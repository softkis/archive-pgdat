       AVANT-6.
           IF REG-PERSON NOT = END-NUMBER
           ACCEPT DIV
             LINE 11 POSITION 25 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE 
             MOVE 0 TO DIV
           END-IF.
       APRES-6.
           MOVE DIV TO DIV-NUMBER.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-DIV" USING LINK-V
                    IF LNK-VAL > 0
                       MOVE LNK-VAL TO DIV
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   2 CALL "2-DIV" USING LINK-V DIV-RECORD
                    IF DIV-NUMBER > 0
                       MOVE DIV-NUMBER TO DIV
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-DIV
                    MOVE DIV-NUMBER TO DIV
           END-EVALUATE.
           PERFORM DIS-HE-06.

       NEXT-DIV.
           CALL "6-DIV" USING LINK-V DIV-RECORD SAVE-KEY.
