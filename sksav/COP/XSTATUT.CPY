       AVANT-STAT.
           IF REG-PERSON NOT = END-NUMBER
           ACCEPT STATUT 
             LINE 9 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE
           MOVE 0 TO STAT-CODE.

       APRES-STAT.
           MOVE STATUT TO STAT-CODE.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-STATUT" USING LINK-V
                    MOVE LNK-VAL TO STATUT
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   2 CALL "2-STATUT" USING LINK-V STAT-RECORD
                    MOVE STAT-CODE TO STATUT
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-STATUT
                    MOVE STAT-CODE TO STATUT
           END-EVALUATE.
           IF STAT-CODE > 5
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-STAT.

       NEXT-STATUT.
           CALL "6-STATUT" USING LINK-V STAT-RECORD EXC-KEY.

