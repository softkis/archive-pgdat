       READ-FORM.
           INITIALIZE FORMULAIRE.
           OPEN INPUT FORM.
           MOVE IMPL-MAX-LINE TO IDX.
           PERFORM LECT-FORM TEST BEFORE VARYING LIN-IDX FROM 1 BY 1 
           UNTIL LIN-IDX > IMPL-MAX-LINE.
           COMPUTE LIN-IDX = IDX.
           ADD 1 TO IDX.
           CLOSE FORM.

       LECT-FORM.
           INITIALIZE REC-FORM.
           READ FORM INTO FORM-LINE(LIN-IDX)
               AT END MOVE LIN-IDX TO IDX
               COMPUTE LIN-IDX = 1 + IMPL-MAX-LINE
           END-READ.

