       AVANT-5.
           IF REG-PERSON NOT = END-NUMBER
           ACCEPT COUT
             LINE 10 POSITION 25 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE 
             MOVE 0 TO COUT
           END-IF.
       APRES-5.
           MOVE COUT TO COUT-NUMBER.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-COUT" USING LINK-V
                    IF LNK-VAL > 0
                       MOVE LNK-VAL TO COUT
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   2 CALL "2-COUT" USING LINK-V COUT-RECORD
                    IF COUT-NUMBER > 0
                       MOVE COUT-NUMBER TO COUT
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-COUT
                    MOVE COUT-NUMBER TO COUT
           END-EVALUATE.
           PERFORM DIS-HE-05.

       NEXT-COUT.
           CALL "6-COUT" USING LINK-V COUT-RECORD SAVE-KEY.
