       IDENTIFICATION DIVISION.
       PROGRAM-ID. "recovery".
      *
      * Title:  recovery.cbl
      *        RM/COBOL-85 Recover Index Files
      *
      * The within material is an unpublished copyrighted work containing
      * trade secret and other proprietary information of Liant Software
      * Corporation (Liant). This copy has been provided on the basis of
      * strict confidentiality and on the express understanding that it
      * may not be reproduced or revealed to any person, in whole or in
      * part, without express written permission from Liant, which is the
      * sole owner of copyright and all other rights therein.
      *
      * This file obtained from $Archive:   /cobol85/pvcs/rmc85/util/code/recovery.cbv  $ on $Date:   28 Apr 1994 20:21:26  $
      *
      * Module History:
      *
      * $Log:   /cobol85/pvcs/rmc85/util/code/recovery.cbv  $
      *    
      *       Rev 6.0   28 Apr 1994 20:21:26   randy
      *    No change.
      *    
      *       Rev 1.5   18 Mar 1994 15:10:00   ROBERT
      *    Project: RM8560
      *    Changed version to 6.0.
      *    
      *       Rev 1.4   10 Mar 1994 15:53:24   ANTHONY
      *    Project: RM8560
      *    Don't erase the screen.
      *
      *       Rev 1.3   09 Mar 1994 11:11:10   ANTHONY
      *    Project: RM8560
      *    Set RETURN-CODE to 1 if indexed file does not exist.
      *
      *       Rev 1.2   04 Mar 1994 10:40:40   ANTHONY
      *    Project: RM8560
      *    Correct sizes of DISPLAY-BUFFER.
      *
      *       Rev 1.1   04 Mar 1994 08:56:06   ANTHONY
      *    Project: RM8560
      *    Clean up display statements.
      *    Change call to recover2 to lower case for UNIX compatibility.
      *
      *       Rev 1.0   03 Mar 1994 14:19:48   ANTHONY
      *    Initial revision
      *
      ******************************************************************
      *
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. RMCOBOL-85.
       OBJECT-COMPUTER. RMCOBOL-85.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT INDEX-FILE, ASSIGN INPUT INDEX-FILE-NAME,
               ORGANIZATION BINARY SEQUENTIAL,
               FILE STATUS INDEX-FILE-STATUS.
       DATA DIVISION.

       FILE SECTION.
       FD  INDEX-FILE
           RECORD CONTAINS 1 CHARACTERS.
       01  INDEX-RECORD                PIC X.

       WORKING-STORAGE SECTION.
       01  RETURN-KEY                  PIC X.
       01  INDEX-FILE-STATUS           PIC X(2).
       01  ACCEPT-STATUS               PIC 99.
       01  SYSTEM-RETURN-CODE          PIC S9(4) BINARY VALUE ZERO.
       01  INDEX-FILE-NAME             PIC X(96) VALUE SPACES.
       01  DROP-FILE-NAME              PIC X(96) VALUE SPACES.
       01  DATA-RECOVERY-FILE-NAME     PIC X(96) VALUE SPACES.
       01  COMMAND-LINE                PIC X(192).
       01  DISPLAY-BUFFER-SIZE         PIC 9(4) BINARY.
       01  DISPLAY-BUFFER.
           02                          PIC X OCCURS 1 TO 80
               DEPENDING ON DISPLAY-BUFFER-SIZE.

       01  CALL-ARGUMENT.
           02  CALL-ARGUMENT-SIZE      PIC 9(4) BINARY.
           02  CALL-ARGUMENT-VALUE.
               03  PIC X OCCURS 1 TO 192
                   DEPENDING ON CALL-ARGUMENT-SIZE.

       01  TMP-SIZE                    PIC 9(4) BINARY.

       01  TITLE-LINE.
           02                          PIC X(49)
             VALUE " RM/COBOL-85 Recover Indexed Files Utility - 6.0".

       LINKAGE SECTION.
       01  MAIN-ARGUMENT.
           02  MAIN-ARGUMENT-SIZE      PIC 9(4) BINARY.
           02  MAIN-ARGUMENT-VALUE.
               03  PIC X OCCURS 1 TO 100
                   DEPENDING ON MAIN-ARGUMENT-SIZE.
      /
       PROCEDURE DIVISION USING MAIN-ARGUMENT.
       DECLARATIVES.
       INDEX-FILE-ERROR SECTION. USE AFTER ERROR PROCEDURE
               ON INDEX-FILE.
       INDEX-FILE-ERROR-PROCEDURE.
           CONTINUE.
       END DECLARATIVES.

       THE-MAIN SECTION.
       A.

           DISPLAY TITLE-LINE.

       GET-FILE-NAMES.
           IF MAIN-ARGUMENT-SIZE NOT = 0
               MOVE SPACES TO INDEX-FILE-NAME
               MOVE SPACES TO DROP-FILE-NAME
               MOVE SPACES TO DATA-RECOVERY-FILE-NAME
               UNSTRING MAIN-ARGUMENT-VALUE DELIMITED BY ","
                   INTO INDEX-FILE-NAME, DROP-FILE-NAME,
                        DATA-RECOVERY-FILE-NAME
               IF INDEX-FILE-NAME NOT EQUAL SPACES
                  AND DROP-FILE-NAME NOT EQUAL SPACES
                  GO OPEN-INDEX-FILE
               END-IF
           END-IF.

       GET-INDEX-FILE-NAME.
           DISPLAY "Indexed File: ".

           IF INDEX-FILE-NAME EQUAL SPACES
               ACCEPT INDEX-FILE-NAME POSITION 0, ECHO, TAB
                       ON EXCEPTION ACCEPT-STATUS CONTINUE
               END-ACCEPT
               IF INDEX-FILE-NAME EQUAL SPACES
                   GO TERMINATE-RECOVERY
               END-IF
           ELSE
               DISPLAY INDEX-FILE-NAME POSITION 0.

       GET-DROP-FILE-NAME.
           DISPLAY "Drop File: ".

           IF DROP-FILE-NAME EQUAL SPACES
               ACCEPT DROP-FILE-NAME POSITION 0, ECHO, TAB,
                   ON EXCEPTION ACCEPT-STATUS CONTINUE
               END-ACCEPT
               IF DROP-FILE-NAME EQUAL SPACES
                   GO GET-DROP-FILE-NAME
               END-IF
           ELSE
               DISPLAY DROP-FILE-NAME POSITION 0.

       OPEN-INDEX-FILE.

           OPEN INPUT INDEX-FILE.

           IF INDEX-FILE-STATUS EQUAL "35"
               MOVE 1 TO TMP-SIZE
               MOVE 80 TO DISPLAY-BUFFER-SIZE
               STRING "File " DELIMITED BY SIZE
                   INDEX-FILE-NAME DELIMITED BY SPACE
                   " does not exist - execution terminated"
                       DELIMITED BY SIZE
                   INTO DISPLAY-BUFFER
                   POINTER TMP-SIZE
               COMPUTE DISPLAY-BUFFER-SIZE = TMP-SIZE - 1
               DISPLAY DISPLAY-BUFFER
               MOVE 1 TO RETURN-CODE
               GO TERMINATE-RECOVERY.

           IF INDEX-FILE-STATUS EQUAL "00"
               CLOSE INDEX-FILE.

       RUN-RECOVER1.
           MOVE 1 TO TMP-SIZE
           MOVE 80 TO DISPLAY-BUFFER-SIZE
           STRING "Attempting to recover " DELIMITED BY SIZE
               INDEX-FILE-NAME DELIMITED BY SPACE
               " in place." DELIMITED BY SIZE
               INTO DISPLAY-BUFFER
               POINTER TMP-SIZE
           COMPUTE DISPLAY-BUFFER-SIZE = TMP-SIZE - 1
           DISPLAY DISPLAY-BUFFER.
           PERFORM WAIT-FOR-RETURN.
           MOVE LOW-VALUES TO COMMAND-LINE.
           STRING "recover1" DELIMITED BY SIZE
               SPACE DELIMITED BY SIZE
               INDEX-FILE-NAME DELIMITED BY SPACES
               SPACE DELIMITED BY SIZE
               DROP-FILE-NAME DELIMITED BY SPACES
               INTO COMMAND-LINE.
           CALL "SYSTEM" USING COMMAND-LINE,
               SYSTEM-RETURN-CODE.

           MOVE SYSTEM-RETURN-CODE TO RETURN-CODE.

           IF SYSTEM-RETURN-CODE EQUAL 0
               MOVE 1 TO TMP-SIZE
               MOVE 80 TO DISPLAY-BUFFER-SIZE
               STRING  "Index file " DELIMITED BY SIZE
                   INDEX-FILE-NAME DELIMITED BY SPACE
                   " was recovered in place." DELIMITED BY SIZE
                   INTO DISPLAY-BUFFER
                   POINTER TMP-SIZE
               COMPUTE DISPLAY-BUFFER-SIZE = TMP-SIZE - 1
               DISPLAY DISPLAY-BUFFER
               MOVE 1 TO TMP-SIZE
               MOVE 80 TO DISPLAY-BUFFER-SIZE
               STRING
               "Records with invalid duplicate keys can be found in "
                   DELIMITED BY SIZE
                   DROP-FILE-NAME DELIMITED BY SPACE
                   "." DELIMITED BY SIZE
                   INTO DISPLAY-BUFFER
                   POINTER TMP-SIZE
               COMPUTE DISPLAY-BUFFER-SIZE = TMP-SIZE - 1
               DISPLAY DISPLAY-BUFFER
               GO TERMINATE-RECOVERY
           END-IF.

           IF SYSTEM-RETURN-CODE EQUAL 2
               GO OPERATOR-TERMINATION.

       RUN-RECOVER2-NOSUB.
           MOVE 1 TO TMP-SIZE
           MOVE 80 TO DISPLAY-BUFFER-SIZE
           STRING "Attempting to extract data records from "
               DELIMITED BY SIZE
               INDEX-FILE-NAME DELIMITED BY SPACE
               " (file structure" DELIMITED BY SIZE
               INTO DISPLAY-BUFFER
               POINTER TMP-SIZE
           COMPUTE DISPLAY-BUFFER-SIZE = TMP-SIZE - 1
           DISPLAY DISPLAY-BUFFER
           DISPLAY "  will be retrieved from the original index file):"
           PERFORM WAIT-FOR-RETURN.
           MOVE 1 TO TMP-SIZE.
           MOVE 192 TO CALL-ARGUMENT-SIZE.
           STRING INDEX-FILE-NAME DELIMITED BY SPACE
              "," DELIMITED BY SIZE
              DATA-RECOVERY-FILE-NAME DELIMITED BY SPACE
              ",NOSUB" DELIMITED BY SIZE
              INTO CALL-ARGUMENT-VALUE
              WITH POINTER TMP-SIZE.
           COMPUTE CALL-ARGUMENT-SIZE = TMP-SIZE - 1.

           CALL "recover2" USING CALL-ARGUMENT.

           IF RETURN-CODE EQUAL 0
               GO RECOVER2-SUCCESS.

       RUN-RECOVER2-SUB.
           MOVE 1 TO TMP-SIZE
           MOVE 80 TO DISPLAY-BUFFER-SIZE
           STRING "Attempting to extract data records from "
               DELIMITED BY SIZE
               INDEX-FILE-NAME DELIMITED BY SPACE
               INTO DISPLAY-BUFFER
               POINTER TMP-SIZE
           COMPUTE DISPLAY-BUFFER-SIZE = TMP-SIZE - 1
           DISPLAY DISPLAY-BUFFER
           DISPLAY "  (file structure must be supplied by user):"
           PERFORM WAIT-FOR-RETURN.
           MOVE 1 TO TMP-SIZE.
           MOVE 192 TO CALL-ARGUMENT-SIZE.
           STRING INDEX-FILE-NAME DELIMITED BY SPACE
              "," DELIMITED BY SIZE
              DATA-RECOVERY-FILE-NAME DELIMITED BY SPACE
              ",SUB" DELIMITED BY SIZE
              INTO CALL-ARGUMENT-VALUE
              WITH POINTER TMP-SIZE.
           COMPUTE CALL-ARGUMENT-SIZE = TMP-SIZE - 1.

           CALL "recover2" USING CALL-ARGUMENT.

           IF RETURN-CODE EQUAL 0
               GO RECOVER2-SUCCESS.

       RECOVERY-FAILED.
           MOVE 1 TO TMP-SIZE
           MOVE 80 TO DISPLAY-BUFFER-SIZE
           STRING "Recovery failed.  A portion of the data records"
               DELIMITED BY SIZE
               " from index file " DELIMITED BY SIZE
               INDEX-FILE-NAME DELIMITED BY SPACE
               " may" DELIMITED BY SIZE
               INTO DISPLAY-BUFFER
               POINTER TMP-SIZE
           COMPUTE DISPLAY-BUFFER-SIZE = TMP-SIZE - 1
           DISPLAY DISPLAY-BUFFER
           DISPLAY "  exist in the data recovery file specified for"
               " recover2."
           DISPLAY "  See the Utilities Appendix of the RM/COBOL-85"
               " User's Guide"
           DISPLAY "  for more information."
           GO TERMINATE-RECOVERY.

       WAIT-FOR-RETURN.
           DISPLAY "Press RETURN when ready..."
           ACCEPT RETURN-KEY TAB ON EXCEPTION ACCEPT-STATUS CONTINUE.

       RECOVER2-SUCCESS.
           DISPLAY "The data recovery file specified for recover2"
               " contains the data records"
           MOVE 1 TO TMP-SIZE
           MOVE 80 TO DISPLAY-BUFFER-SIZE
           STRING "  from index file " DELIMITED BY SIZE
               INDEX-FILE-NAME DELIMITED BY SPACE
               "." DELIMITED BY SIZE
               INTO DISPLAY-BUFFER
               POINTER TMP-SIZE
           COMPUTE DISPLAY-BUFFER-SIZE = TMP-SIZE - 1
           DISPLAY DISPLAY-BUFFER
           DISPLAY "  See the Utilities Appendix of the RM/COBOL-85"
               " User's Guide"
           DISPLAY "  for information on how to rebuild the"
               " index file."
           GO TERMINATE-RECOVERY.

       OPERATOR-TERMINATION.
           DISPLAY "Recovery process canceled by operator.".

       TERMINATE-RECOVERY.
           CANCEL "recover2".
           CANCEL "C$RERR".
           CANCEL "C$FILE".
           STOP RUN.
