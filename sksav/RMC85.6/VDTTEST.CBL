       IDENTIFICATION DIVISION.
       PROGRAM-ID.  "vdttest".
      *
      * Title: vdttest.cbl
      *     RM/COBOL-85 Low Volume I/O Test
      *
      * The information contained herein is proprietary to Liant Software
      * Corporation, and provided for maintenance purposes only.
      * No other use of this material is authorized or permitted without
      * specific authorization, in writing, from Liant Software Corporation.
      *
      * Version Identification:
      *   $Revision:   6.0  $
      *   $Date:   28 Apr 1994 20:22:12  $
      *   $Author:   randy  $
      *   $Logfile:   /cobol85/pvcs/rmc85/verify/code/vdttest.cbv  $
      *
      *    Module History:
      *     $Log:   /cobol85/pvcs/rmc85/verify/code/vdttest.cbv  $
      *    
      *       Rev 6.0   28 Apr 1994 20:22:12   randy
      *    No change.
      *    
      *       Rev 5.6   18 Mar 1994 13:31:54   ROBERT
      *    Project: RM8560
      *    Remove unneeded RMFILT directive.
      *    
      *       Rev 5.5   17 Mar 1994 15:24:58   ROBERT
      *    Project: RM8560
      *    Changed version to 6.0.
      *    
      *       Rev 5.4   22 May 1992 13:31:06   MIKE
      *    Added tests for ACCEPT BEFORE TIME.
      *    
      *       Rev 5.3   19 Jan 1992 17:13:02   BILL
      *    Avoid X/Open reserved words BACKGROUND-COLOR, FOREGROUND-COLOR,
      *    and REVERSE-VIDEO.
      *
      *       Rev 5.2   22 Mar 1991  9:40:52   RANDY
      *    Update version.
      *    Always include color test.
      *
      *       Rev 5.1   12 Mar 1991 10:15:00   BILL
      *    Correct title.
      *
      *       Rev 5.0   15 Sep 1990  1:44:28   BILL
      *    No change.
      *
      *       Rev 1.3   29 Aug 1990 14:53:02   BILL
      *    Change program name to lower case for UNIX program library.
      *
      *       Rev 1.2   15 Aug 1990 10:59:46   BILL
      *    Change displayed text to mixed case.
      *
      *       Rev 1.1   14 Aug 1990 17:39:42   BILL
      *    Improve spelling of program name.
      *    Remove color test from UNIX version.
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL-85.
       OBJECT-COMPUTER.  RMCOBOL-85.
      /
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  TITLE        PIC X(60) VALUE
           "RM/COBOL-85 Verify terminal I/O functions - Version 6.0".

       01  COMMAND-NUMBER          PIC 99    VALUE ZERO.
       01  LAST-COMMAND            PIC 99    VALUE ZERO.
       01  LAST-COMEDIT            PIC ZZ    VALUE ZERO.
       01  EXC-NUM                 PIC 99    VALUE ZERO.
       01  ONE-CHAR                PIC X     VALUE SPACE.
       01  R                       PIC 99    VALUE 24.
       01  C                       PIC 999   VALUE 79.
       01  C-DISPLAY               PIC ZZ9   VALUE ZERO.
       01  O                       PIC 999   VALUE ZERO.
       01  N                       PIC 999   VALUE ZERO.
       01  NAME                    PIC X(10) VALUE SPACES.
       01  LINE-NO                 PIC 99    VALUE ZERO.
       01  LINE-NO-EDIT            PIC Z9    VALUE ZERO.
       01  DATE-FLD                PIC 9(6)  VALUE ZERO.
       01  DAY-FLD                 PIC 9(5)  VALUE ZERO.
       01  TIME-FLD                PIC 9(8)  VALUE ZERO.
       01  OPERAND                 PIC S9(9)V9(9)  VALUE ZERO.
       01  RESULT                  PIC -(9)9.9(9)  VALUE ZERO.
       01  COL-ACC                 PIC X(150)      VALUE SPACES.
       01  TIME-OUT-5-SECONDS      PIC 999         VALUE 500.
       01  TIME-OUT-IMMEDIATE      PIC 999         VALUE 0.
       01  TIME-OUT-INDEFINITE     PIC 9(10)       VALUE 9999999999.

       01  CONTROL-VALUE.
           02                          PIC X(4).
               88  C-INTENSITY-HIGH    VALUE "HIGH".
               88  C-INTENSITY-LOW     VALUE "LOW".
               88  C-INTENSITY-OFF     VALUE "OFF".
           02                          PIC X(12).
               88  C-REVERSE-VIDEO       VALUE ", REVERSE" FALSE SPACES.
               88  C-NO-REVERSE-VIDEO    VALUE ", NO REVERSE".
           02                          PIC X(10).
               88  C-BLINKING            VALUE ", BLINK".
               88  C-NO-BLINKING         VALUE ", NO BLINK".
           02  COLOR-CONTROL.
               03                      PIC X(11).
                   88  FCOLOR-EQUALS   VALUE ", FCOLOR = "
                                       FALSE SPACES.
               03  FCOLOR-VALUE        PIC X(7).
                   88  FOREGROUND-IS-BLACK VALUE "BLACK".
               03                      PIC X(11).
                   88  BCOLOR-EQUALS   VALUE ", BCOLOR = "
                                       FALSE SPACES.
               03  BCOLOR-VALUE        PIC X(7).
               03                      PIC X(11).
                   88  BORDER-EQUALS   VALUE ", BORDER = "
                                       FALSE SPACES.
               03  BORDER-VALUE        PIC X(7).

       01  I                           PIC 99 BINARY.

       01  COLOR-TABLE.
           02  PIC X(7) VALUE "Black  ".
           02  PIC X(7) VALUE "Blue   ".
           02  PIC X(7) VALUE "Green  ".
           02  PIC X(7) VALUE "Cyan   ".
           02  PIC X(7) VALUE "Red    ".
           02  PIC X(7) VALUE "Magenta".
           02  PIC X(7) VALUE "Brown  ".
           02  PIC X(7) VALUE "White  ".
       01  REDEFINES COLOR-TABLE.
           02  COLOR-NAME              PIC X(7) OCCURS 8.

       01  INTENSITY                   PIC X.
           88  LOW-INTENSITY-DESIRED   VALUES "N", "n".
           88  HIGH-INTENSITY-DESIRED  VALUES "Y", "y", SPACE.
       01  REVERSE-VID                 PIC X.
           88  REVERSE-VIDEO-DESIRED   VALUES "Y", "y".
           88  REVERSE-VIDEO-UNDESIRED VALUES "N", "n", SPACE.

       01  STANDARD-CONTROL.
           02  PIC X(13) VALUE "BORDER=BLACK,".
           02  SUBSTANDARD-CONTROL     PIC X(47) VALUE
                   "LOW,FCOLOR=WHITE,BCOLOR=BLACK".

       01  MAIN-MENU-VALUES.
           02  PIC X(51) VALUE " 1.  ACCEPT with and without PROMPT".
           02  PIC X(51) VALUE
               " 2.  ACCEPT and DISPLAY value with and without BEEP".
           02  PIC X(51) VALUE " 3.  ACCEPT with and without TAB".
           02  PIC X(51) VALUE
               " 4.  ACCEPT and DISPLAY value with default values".
           02  PIC X(51) VALUE " 5.  Check screen dimensions".
           02  PIC X(51) VALUE " 6.  ACCEPT with SECURE".
           02  PIC X(51) VALUE
               " 7.  ACCEPT and DISPLAY value with REVERSE".
           02  PIC X(51) VALUE " 8.  ACCEPT DATE and TIME".
           02  PIC X(51) VALUE " 9.  ACCEPT HIGH and LOW intensity".
           02  PIC X(51) VALUE "10.  ACCEPT with BLINK and REVERSE".
           02  PIC X(51) VALUE "11.  ACCEPT with ON EXCEPTION clause".
           02  PIC X(51) VALUE "12.  ACCEPT with UPDATE".
           02  PIC X(51) VALUE "13.  ACCEPT with BEFORE TIME".
           02  PIC X(51) VALUE "14.  Erase to end of line".
           02  PIC X(51) VALUE "15.  Erase to end of screen".
          02  PIC X(51) VALUE "16.  Change colors".
          02  PIC X(51) VALUE "17.  Exit program".
       01  REDEFINES MAIN-MENU-VALUES.
          02  MAIN-MENU-LINE          PIC X(51) OCCURS 17.
      /
      *
      *
       PROCEDURE DIVISION.
       ENTER-COMMAND.
           DISPLAY TITLE LINE 1 ERASE CONTROL STANDARD-CONTROL.
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > 17
               ADD I, 2 GIVING LINE-NO
               DISPLAY MAIN-MENU-LINE (I)
                   LINE LINE-NO, POSITION 3, CONTROL STANDARD-CONTROL
           END-PERFORM.
           DISPLAY "Enter command number" LOW, POSITION 12, LINE 21
           DISPLAY "Last command = " LOW, POSITION 15,
                   COMMAND-NUMBER, CONVERT, LOW.
           DISPLAY "<Enter> goes to next test." LOW, POSITION 12.
           MOVE COMMAND-NUMBER TO LAST-COMMAND.
      /

       ACCEPT-COMMAND-NUMBER.
           ACCEPT COMMAND-NUMBER LINE 21 POSITION 34
                   NO BEEP CONVERT TAB ON EXCEPTION EXC-NUM
               DISPLAY SPACES SIZE 2 LINE 21 POSITION 34 BEEP
               GO TO ACCEPT-COMMAND-NUMBER.
           IF COMMAND-NUMBER EQUAL ZERO
               ADD 1, LAST-COMMAND GIVING LAST-COMMAND, COMMAND-NUMBER.

       VECTOR-ON-COMMAND-NUMBER.
           IF COMMAND-NUMBER > 0 AND < 17
               DISPLAY MAIN-MENU-LINE (COMMAND-NUMBER),
                       LINE 1, POSITION 10, ERASE, HIGH.
           GO TO TEST-1-PARA, TEST-2-PARA, TEST-3-PARA, TEST-4-PARA,
                 TEST-5-PARA, TEST-6-PARA, TEST-7-PARA, TEST-8-PARA,
                 TEST-9-PARA, TEST-10-PARA, TEST-11-PARA, TEST-12-PARA,
                 TEST-13-PARA, TEST-14-PARA, TEST-15-PARA, TEST-16-PARA,
                 EXIT-PARA
                                       DEPENDING ON COMMAND-NUMBER.
           DISPLAY SPACES SIZE 2 LINE 21 POSITION 34 BEEP.
           GO TO ACCEPT-COMMAND-NUMBER.

       RETURN-TO-MENU.
           MOVE SPACE TO ONE-CHAR.

           DISPLAY 'Press "Enter" to return to menu.  < >'
                       LINE 21 POSITION 26 CONTROL SUBSTANDARD-CONTROL
                   'Enter an "R" to repeat this test.'
                       LINE 22 POSITION 26 CONTROL SUBSTANDARD-CONTROL.

           ACCEPT ONE-CHAR LINE 21 POSITION 61 TAB NO BEEP
                        CONTROL SUBSTANDARD-CONTROL.

           IF ONE-CHAR = SPACE GO TO ENTER-COMMAND.
           IF ONE-CHAR NOT EQUAL "R" AND "r" GO TO RETURN-TO-MENU.
           GO VECTOR-ON-COMMAND-NUMBER.
      /
      *
       TEST-1-PARA.
           DISPLAY "A.  ACCEPT with no prompt" LINE 3.
           DISPLAY "    Enter your name:  " LINE 4.
           ACCEPT NAME LINE 4 POSITION 23 NO BEEP TAB.
           DISPLAY NAME LINE 5 POSITION 23.
      *
           DISPLAY "B.  ACCEPT with default prompt character" LINE 7.
           DISPLAY "    Enter your name:  " LINE 8.
           ACCEPT NAME LINE 8 POSITION 23 NO BEEP TAB PROMPT.
           DISPLAY NAME LINE 9 POSITION 23.
      *
           DISPLAY
              'C.  ACCEPT with user-specified prompt character: "H"'
                      LINE 11.
           DISPLAY "    Enter your name:  " LINE 12.
           ACCEPT NAME LINE 12 POSITION 23 NO BEEP TAB PROMPT "H"
           DISPLAY NAME LINE 13 POSITION 23.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-2-PARA.
           DISPLAY 'A. ACCEPT with BEEP (Press "Enter")'  LINE 3.
           ACCEPT ONE-CHAR POSITION 0 NO BEEP.
           DISPLAY "Enter your name (You should have heard a beep):  "
                       LINE 4 POSITION 4.
           ACCEPT NAME POSITION 0 TAB PROMPT.
      *
           DISPLAY 'B. ACCEPT without BEEP (Press "Enter")' LINE 6.
           ACCEPT ONE-CHAR POSITION 0 NO BEEP.
           DISPLAY
                "Enter your name (You should not have heard a beep):  "
                       LINE 7 POSITION 4.
           ACCEPT NAME POSITION 0 TAB PROMPT NO BEEP.
      *
           DISPLAY 'C. DISPLAY with BEEP (Press "Enter")'
                       LINE 9.
           ACCEPT ONE-CHAR POSITION 0 NO BEEP.
           DISPLAY "This DISPLAY should have beeped."
                       LINE 10 POSITION 4 BEEP.
      *
           DISPLAY 'D. DISPLAY without BEEP (Press "Enter")'
                       LINE 12.
           ACCEPT ONE-CHAR POSITION 0 NO BEEP.
           DISPLAY "This DISPLAY should not have beeped."
                       LINE 13 POSITION 4.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-3-PARA.
           DISPLAY "Automatic tab is where input is terminated when"
              LINE 3 POSITION 5.
           DISPLAY "end of the input field is encountered."
              LINE 4 POSITION 5.
           DISPLAY
               "Enter a string of 10 chars.  An automatic tab should"
                       LINE 6 POSITION 5.
           DISPLAY "occur after the last character has been typed."
                       LINE 7 POSITION 5.
           ACCEPT NAME LINE 10 POSITION 15 PROMPT NO BEEP ECHO.
           DISPLAY NAME LINE 11 POSITION 15.
      *
           DISPLAY
               "Enter a string of 10 chars.  An automatic tab should"
                       LINE 13 POSITION 5.
           DISPLAY "not occur after the last character is typed."
                       LINE 14 POSITION 5.
           DISPLAY "You must press Enter to exit from the field"
           LINE 15 POSITION 5.
           ACCEPT NAME LINE 17 POSITION 15
                       NO BEEP PROMPT TAB ECHO.
           DISPLAY NAME LINE 18 POSITION 15.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-4-PARA.
           MOVE 4 TO LINE-NO.
           SUBTRACT 4 FROM R GIVING O.
           SUBTRACT 3 FROM C GIVING N.
           PERFORM DISPLAY-LINE-NO O TIMES.
           DISPLAY ALL "1234567890" LINE 3 SIZE 30.
           DISPLAY "This line should start on line 4 position 1."
           DISPLAY "This line should start on line 5 position 15."
                       POSITION 15.
           DISPLAY "This line should start on line 6 position 1."
                       LINE 6 POSITION 1.
           DISPLAY "This line should start on line 7 position 1."
                       LINE 7.
           DISPLAY "This line should appear on line 24 position 10."
                       LINE 24 POSITION 10.
           DISPLAY
           'This should be line 12  ****Press "Enter" to continue: '
                       LINE 12.
           ACCEPT ONE-CHAR POSITION 0 NO BEEP TAB.
      *
           DISPLAY "This line should appear on line 13 position 5."
                       POSITION 5.
           DISPLAY "ACCEPT and DISPLAY default test (POSITION 0) "
                       LINE 15 POSITION 5.
           DISPLAY "Enter field " POSITION 0.
           ACCEPT NAME POSITION 0 PROMPT NO BEEP TAB.
           DISPLAY " ACCEPT again " POSITION 0.
           ACCEPT NAME POSITION 0 PROMPT NO BEEP TAB.
           DISPLAY " One more time " POSITION 0.
           ACCEPT NAME POSITION 0 PROMPT NO BEEP TAB.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-5-PARA.
           DISPLAY "How many rows are there on your crt? "
                       LINE 3 POSITION 5.
           ACCEPT R POSITION 0 TAB NO BEEP CONVERT UPDATE.
           IF R = 0 MOVE 24 TO R.
           DISPLAY "How many columns? "  LINE 5 POSITION 5.
           ACCEPT C POSITION 0 TAB NO BEEP CONVERT UPDATE.
           IF C = 0 MOVE 79 TO C.
           DISPLAY "You should see an asterisk in each corner"
                   LINE 7 POSITION 5.
           DISPLAY "of screen (after pressing ""Enter"")"
                   LINE 8 POSITION 5.
           ACCEPT ONE-CHAR POSITION 0 NO BEEP.
           DISPLAY "*" LINE 1 POSITION 1 ERASE
              "*" LINE R POSITION 1
                   "*" LINE 1 POSITION C
                   "*" LINE R POSITION C.
      *
           DISPLAY 'Enter "0" to display line nos, otherwise "Enter"'
               LINE 3 POSITION 5.
           ACCEPT ONE-CHAR POSITION 0 NO BEEP TAB.
           IF ONE-CHAR NOT = "0" GO TO COL-DISPLAY.
           MOVE 1 TO LINE-NO.
           SUBTRACT 2 FROM C GIVING N.
           PERFORM DISPLAY-LINE-NO R TIMES.
      *
       COL-DISPLAY.
           DISPLAY 'Enter "1" to display ALL "1234567890", '
                           LINE 5 POSITION 5
                   'otherwise "Enter"'
           ACCEPT ONE-CHAR POSITION 0 NO BEEP TAB.
           IF ONE-CHAR NOT = "1" GO TO ENTER-LINE.
           DISPLAY ALL "1234567890" SIZE C LINE 1 POSITION 1.
           DISPLAY ALL "1234567890" SIZE C LINE R POSITION 1.
      *
       ENTER-LINE.
           DISPLAY "Enter an " LINE 7 POSITION 5
                   C, CONVERT
                   " character field to test accept limits"
                       POSITION 0.
           ACCEPT COL-ACC LINE 9 POSITION 1 SIZE C NO BEEP PROMPT TAB.
           DISPLAY COL-ACC LINE 10 SIZE C REVERSE.
      *
           GO RETURN-TO-MENU.
      *
       DISPLAY-LINE-NO.
           DISPLAY LINE-NO, LINE LINE-NO, POSITION 1, CONVERT,
                   LINE-NO, LINE LINE-NO, POSITION N, CONVERT.
           ADD 1 TO LINE-NO.
      /
      *
       TEST-6-PARA.
           DISPLAY "Enter your code name: " LINE 5 POSITION 5.
           ACCEPT NAME POSITION 0 PROMPT NO BEEP TAB OFF.
           DISPLAY "  Your code name was: " LINE 7 POSITION 5.
           DISPLAY NAME POSITION 0.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-7-PARA.
           DISPLAY "DISPLAY with extra spaces    "
                       LINE 5 POSITION 5 REVERSE.
           DISPLAY "DISPLAY HIGH and REVERSE     "  POSITION 5
                REVERSE HIGH LINE 7.
           DISPLAY "DISPLAY LOW and REVERSE      " POSITION 5
                LINE 9 LOW REVERSE.
      *
           DISPLAY "ACCEPT in REVERSE LOW:            "
                LINE 11 POSITION 5.
           ACCEPT NAME POSITION 0 REVERSE LOW  NO BEEP TAB.
           DISPLAY "ACCEPT in REVERSE HIGH:           "
                LINE 13 POSITION 5.
           ACCEPT NAME POSITION 0 REVERSE HIGH NO BEEP TAB.
      *
           DISPLAY "ACCEPT with prompt character ""-""  "
                LINE 15 POSITION 5.
           ACCEPT NAME POSITION 0 PROMPT "-"
                REVERSE NO BEEP TAB.
      *
           DISPLAY "ACCEPT with default prompt:       "
                LINE 17 POSITION 5.
           ACCEPT NAME POSITION 0 PROMPT REVERSE NO BEEP TAB.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-8-PARA.
           ACCEPT DATE-FLD FROM DATE.
           ACCEPT DAY-FLD FROM DAY.
           ACCEPT TIME-FLD FROM TIME.
           DISPLAY " " LINE 2.
           DISPLAY "    Today's date is   (yymmdd): ", DATE-FLD.
           DISPLAY " ".
           DISPLAY "    The Julian form is (yyddd): ", DAY-FLD.
           DISPLAY " ".
           DISPLAY "    The time is     (hhmmsscc): ", TIME-FLD.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-9-PARA.
           DISPLAY "Enter numeric data for operand" LINE 3
                    POSITION 1 HIGH.
           DISPLAY "DISPLAY LOW, ACCEPT LOW" LINE 4 POSITION 50.
           DISPLAY "DISPLAY LOW, ACCEPT HIGH" LINE 6 POSITION 50.
           DISPLAY "with default positioning" LINE 8 POSITION 50.
      *
           DISPLAY "Operand   = " LINE 4 LOW.
           ACCEPT OPERAND LINE 4 POSITION 13 SIZE 9
                                       TAB NO BEEP PROMPT CONVERT LOW.
           MOVE OPERAND TO RESULT.
           DISPLAY RESULT LINE 4 POSITION 13 LOW.
      *
           DISPLAY "Operand   = " LINE 6 LOW.
           ACCEPT OPERAND LINE 6 POSITION 13 SIZE 9
                                       TAB NO BEEP PROMPT CONVERT HIGH.
           MOVE OPERAND TO RESULT.
           DISPLAY RESULT LINE 6 POSITION 13 LOW.
      *
           DISPLAY "Operand   = " LINE 8 LOW.
           ACCEPT OPERAND LINE 8 POSITION 13 SIZE 9
                                       TAB NO BEEP PROMPT CONVERT HIGH.
           MOVE OPERAND TO RESULT.
           DISPLAY RESULT POSITION 0 LOW.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-10-PARA.
           DISPLAY " DISPLAY with extra spaces    "
                       LINE 3 POSITION 5 BLINK REVERSE.
           DISPLAY "BLINK and REVERSE video" LINE 3 POSITION 43.
           DISPLAY " DISPLAY with extra spaces    "
                             LINE 5 POSITION 5 LOW BLINK.
           DISPLAY "LOW and BLINK" LINE 5 POSITION 43.
           DISPLAY " DISPLAY with extra spaces    "
                           LINE 7 POSITION 5 HIGH BLINK.
           DISPLAY "HIGH and BLINK" LINE 7 POSITION 43.
           DISPLAY " DISPLAY with extra spaces    "
                 LINE 9 POSITION 5 HIGH BLINK REVERSE.
           DISPLAY "HIGH and BLINK and REVERSE" LINE 9
                 POSITION 43.
           DISPLAY " We are testing all the "
                       LINE 11 POSITION 5 LOW REVERSE.
           DISPLAY "LOW and REVERSE video" LINE 11 POSITION 43.
           DISPLAY " display options "
                       LINE 13 POSITION 5 LOW BLINK REVERSE.
           DISPLAY "LOW and REVERSE and BLINK" LINE 13 POSITION 43.
      *
           DISPLAY "ACCEPT with BLINK:                     "
                   LINE 16 POSITION 5.
           ACCEPT NAME POSITION 0 NO BEEP TAB BLINK.
      *
           DISPLAY "LOW and REVERSE with prompt character ""-""  "
               LINE 18 POSITION 5.
           ACCEPT
               NAME POSITION 0 NO BEEP TAB PROMPT "-" LOW REVERSE.
      *
           DISPLAY "BLINK and REVERSE with default prompt:     "
               LINE 20  POSITION 5.
           ACCEPT NAME POSITION 0 NO BEEP TAB PROMPT BLINK REVERSE.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-11-PARA.
           MOVE ZEROS TO OPERAND, RESULT.
           DISPLAY
               "Enter a number and terminate with field termination key"
                       LINE 4 POSITION 5.
           DISPLAY
           "Field termination keys will show nonzero termination code"
                    LINE 5 POSITION 5.
           DISPLAY
               '(Nonnumeric entry will show convert error "98")'
                       LINE 6 POSITION 5.
           DISPLAY SPACES SIZE 40 LINE 12 POSITION 20.
           ACCEPT OPERAND LINE 8 POSITION 25 SIZE 9
                       PROMPT CONVERT NO BEEP TAB
                       ON EXCEPTION EXC-NUM
                               DISPLAY
                                       "*** Exception branch taken ***"
                                       LINE 12 POSITION 29 BEEP.
           MOVE OPERAND TO RESULT.
           DISPLAY "As it was accepted:" LINE 9 POSITION 5.
           DISPLAY RESULT LINE 9 POSITION 24.
           DISPLAY "Termination code <  >" LINE 13 POSITION 33.
           DISPLAY EXC-NUM LINE 13 POSITION 51, CONVERT.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-12-PARA.
           DISPLAY "Update initial field, then <Enter>"
                       LINE 3 POSITION 5.
           MOVE "Init-field" TO NAME.
           ACCEPT NAME LINE 5 POSITION 20 UPDATE NO BEEP TAB.
           DISPLAY
              "update current field, then <Erase-right-key>"
                       LINE 7 POSITION 5
           ACCEPT NAME LINE 9 POSITION 20 UPDATE NO BEEP TAB.
           DISPLAY "(Trailing characters should be blank)"
                       LINE 11 POSITION 5.
           DISPLAY NAME LINE 13 POSITION 20.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-13-PARA.
           MOVE ZEROS TO OPERAND, RESULT.
           DISPLAY "This ACCEPT will terminate itself in 5 seconds"
               LINE 3 POSITION 3 LOW
               "ACCEPT BEFORE TIME " LINE 4 POSITION 5 LOW
               TIME-OUT-5-SECONDS LOW.
           DISPLAY "This ACCEPT will terminate itself immediately"
               LINE 7 POSITION 3 LOW
               "ACCEPT BEFORE TIME " LINE 8 POSITION 5 LOW
               TIME-OUT-IMMEDIATE LOW.
           DISPLAY
               "This ACCEPT will not terminate itself "
               LINE 11 POSITION 3 LOW
               "(Press <RETURN> when ready)" LOW
               "ACCEPT BEFORE TIME " LINE 12 POSITION 5 LOW
               TIME-OUT-INDEFINITE LOW.
           IF NAME = SPACES
               MOVE "ABCD" TO NAME.

           MOVE 0 to EXC-NUM.
           ACCEPT NAME LINE 4 POSITION 28 UPDATE PROMPT
               BEFORE TIME TIME-OUT-5-SECONDS
               ON EXCEPTION EXC-NUM
                   DISPLAY "*** Exception branch taken ***"
                       LINE 4 POSITION 39.
           DISPLAY "Returned " LINE 5 POSITION 5 LOW
               NAME " Termination code <  >" LOW.
           DISPLAY EXC-NUM LINE 5 POSITION 43, CONVERT.
          
           MOVE 0 to EXC-NUM.
           ACCEPT NAME LINE 8 POSITION 28 UPDATE PROMPT
               BEFORE TIME TIME-OUT-IMMEDIATE
               ON EXCEPTION EXC-NUM
                   DISPLAY "*** Exception branch taken ***"
                       LINE 8 POSITION 39.
           DISPLAY "Returned " LINE 9 POSITION 5 LOW
               NAME " Termination code <  >" LOW.
           DISPLAY EXC-NUM LINE 9 POSITION 43, CONVERT.

           MOVE 0 to EXC-NUM.
           ACCEPT NAME LINE 12 POSITION 35 UPDATE PROMPT
               BEFORE TIME TIME-OUT-INDEFINITE
               ON EXCEPTION EXC-NUM
                   DISPLAY "*** Exception branch taken ***"
                       LINE 12 POSITION 46.
           DISPLAY "Returned " LINE 13 POSITION 5 LOW
               NAME " Termination code <  >" LOW.
           DISPLAY EXC-NUM LINE 13 POSITION 43, CONVERT.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-14-PARA.
           DISPLAY "(Screen dimensions taken from test 5.)"
                       LINE 3 POSITION 15.
      *
           DISPLAY "ACCEPT with erase to end-of-line."
                           LINE 5 POSITION 5.
           DISPLAY "This should erase from position 5 to end of line"
                   LINE 6 POSITION 5.
           DISPLAY ALL "1234567890" SIZE C LINE 9.
           DISPLAY ALL "1234567890" SIZE C LINE 10.
           DISPLAY "  This line must remain unchanged.  "
                       LINE 10 POSITION 3.
           DISPLAY "Press ""Enter"" to enter your name."
                       LINE 7 POSITION 5.
           ACCEPT ONE-CHAR POSITION 0 NO BEEP.
           ACCEPT NAME LINE 9 POSITION 5 PROMPT
                                       NO BEEP ERASE EOL TAB.
      *
           DISPLAY
              "DISPLAY with erase to end-of-line." LINE 12 POSITION 5.
           DISPLAY ALL "1234567890" SIZE C LINE 16.
           DISPLAY ALL "1234567890" SIZE C LINE 17.
           DISPLAY "  This line must remain unchanged.  "
                       LINE 17 POSITION 3.
           DISPLAY
           "Press ""Enter"" to erase from position 5 to end of line."
                       LINE 14 POSITION 5.
           ACCEPT ONE-CHAR POSITION 0 NO BEEP.
           DISPLAY NAME LINE 16 POSITION 5 ERASE EOL.
      *
           GO RETURN-TO-MENU.
      /
      *
       TEST-15-PARA.
           SUBTRACT 1 FROM C.
           DISPLAY "(Screen dimensions taken from test 5.)"
                       LINE 3 POSITION 15.
      *
           DISPLAY
            "ACCEPT with erase to end-of-screen." LINE 5 POSITION 5.
           DISPLAY "This should erase to end-of-screen" LINE 6
                 POSITION 5.
           DISPLAY ALL "1234567890" SIZE C LINE 9.
           MOVE 10 TO N.
           PERFORM THIS-LINE.
           DISPLAY "Press ""Enter"" to enter your name"
                       LINE 7 POSITION 5.
           ACCEPT ONE-CHAR POSITION 0 NO BEEP.
           ACCEPT NAME LINE 9 POSITION 5
                                       NO BEEP PROMPT ERASE EOS TAB.
      *
           DISPLAY "DISPLAY with erase to end-of-screen"
                       LINE 12 POSITION 5.
           DISPLAY ALL "1234567890" SIZE C LINE 16.
           MOVE 17 TO N.
           PERFORM THIS-LINE.
           DISPLAY "Press ""Enter"" to display erase to end-of-screen"
                       LINE 14 POSITION 5.
           ACCEPT ONE-CHAR POSITION 0 NO BEEP.
           DISPLAY NAME LINE 16 POSITION 5 ERASE EOS.
      *
           ADD 1 TO C.
           GO RETURN-TO-MENU.
      *
       THIS-LINE.
           DISPLAY ALL "1234567890" SIZE C LINE N.
           DISPLAY " This line must be erased. " LINE N POSITION 3.
           ADD 1 TO N.
           IF N NOT GREATER THAN R GO TO THIS-LINE.
      /
      *
       TEST-16-PARA.
           DISPLAY "Foreground" LINE 13 POSITION 20 HIGH.
           DISPLAY "Valid color          If              If"
                       LINE 14 POSITION 2.
           DISPLAY "   names        HIGH intensity     REVERSE"
                       LINE 15 POSITION 2.

           MOVE SPACES TO CONTROL-VALUE.
           SET FCOLOR-EQUALS, BCOLOR-EQUALS, BORDER-EQUALS TO TRUE.
           MOVE "White" TO BCOLOR-VALUE.
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > 8
               ADD I, 15 GIVING LINE-NO
               MOVE COLOR-NAME (I) TO FCOLOR-VALUE, BORDER-VALUE
               SET C-INTENSITY-LOW TO TRUE
               DISPLAY COLOR-NAME (I) LINE LINE-NO POSITION 2
                   SIZE 10 CONTROL CONTROL-VALUE
               SET C-REVERSE-VIDEO TO TRUE
               DISPLAY COLOR-NAME (I) LINE LINE-NO POSITION 37
                   SIZE 10 CONTROL CONTROL-VALUE
               SET C-NO-REVERSE-VIDEO TO TRUE
               SET C-INTENSITY-HIGH TO TRUE
               DISPLAY COLOR-NAME (I) LINE LINE-NO POSITION 18
                   SIZE 16 CONTROL CONTROL-VALUE
               MOVE "Black" TO BCOLOR-VALUE
           END-PERFORM.

           MOVE SPACES TO CONTROL-VALUE.
           SET FCOLOR-EQUALS, BCOLOR-EQUALS, BORDER-EQUALS TO TRUE.
           MOVE SPACE TO INTENSITY.
           PERFORM UNTIL C-INTENSITY-HIGH OR C-INTENSITY-LOW
               DISPLAY "HIGH intensity (default Y) Y/N?: "
                       LINE 3, CONTROL STANDARD-CONTROL
               ACCEPT INTENSITY
                       POSITION 0 TAB UPDATE CONTROL STANDARD-CONTROL
               EVALUATE TRUE
                   WHEN LOW-INTENSITY-DESIRED
                       SET C-INTENSITY-LOW TO TRUE
                   WHEN HIGH-INTENSITY-DESIRED
                       SET C-INTENSITY-HIGH TO TRUE
               END-EVALUATE
           END-PERFORM.

           MOVE SPACE TO REVERSE-VID.
           PERFORM UNTIL C-REVERSE-VIDEO OR C-NO-REVERSE-VIDEO
               DISPLAY "REVERSE (default N)  Y/N?: "
                       LINE 4, CONTROL STANDARD-CONTROL
               ACCEPT REVERSE-VID
                       POSITION 0 TAB UPDATE CONTROL STANDARD-CONTROL
               EVALUATE TRUE
                   WHEN REVERSE-VIDEO-DESIRED
                       SET C-REVERSE-VIDEO TO TRUE
                   WHEN REVERSE-VIDEO-UNDESIRED
                       SET C-NO-REVERSE-VIDEO TO TRUE
               END-EVALUATE
           END-PERFORM.

           MOVE "Black" TO BCOLOR-VALUE, BORDER-VALUE.
           MOVE "White" TO FCOLOR-VALUE.
           DISPLAY "Enter foreground color name (default White): "
                   CONTROL STANDARD-CONTROL.
           ACCEPT FCOLOR-VALUE POSITION 0 TAB UPDATE
                   CONTROL STANDARD-CONTROL.
           DISPLAY "Enter background color name (default Black): "
                   CONTROL STANDARD-CONTROL.
           ACCEPT BCOLOR-VALUE POSITION 0 TAB UPDATE
                   CONTROL STANDARD-CONTROL.
           DISPLAY "Enter border color name (default Black): "
                   CONTROL STANDARD-CONTROL.
           ACCEPT BORDER-VALUE POSITION 0 TAB UPDATE
                   CONTROL STANDARD-CONTROL.
           IF FCOLOR-VALUE = SPACES MOVE "White" TO FCOLOR-VALUE
           END-IF.
           IF BCOLOR-VALUE = SPACES MOVE "Black" TO BCOLOR-VALUE
           END-IF.
           IF BORDER-VALUE = SPACES MOVE "Black" TO BORDER-VALUE
           END-IF.

           DISPLAY CONTROL-VALUE LINE 1 ERASE CONTROL CONTROL-VALUE.
      *
           GO RETURN-TO-MENU.
      *
      *
       EXIT-PARA.
           EXIT PROGRAM.
           STOP RUN.
