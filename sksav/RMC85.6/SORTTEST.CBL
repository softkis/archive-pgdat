       IDENTIFICATION DIVISION.
       PROGRAM-ID.  "sorttest".
      *
      *    Title:  sorttest.cbl
      *        RM/COBOL-85 Sort Test
      *
      * The information contained herein is proprietary to Liant Software
      *    Corporation, and provided for maintenance purposes only.
      *    No other use of this material is authorized or permitted without
      *    specific authorization, in writing, from Liant Software Corporation.
      *
      * Version Identification:
      *   $Revision:   6.0  $
      *   $Date:   28 Apr 1994 20:22:10  $
      *   $Author:   randy  $
      *   $Logfile:   /cobol85/pvcs/rmc85/verify/code/sorttest.cbv  $
      *
      *    Module History:
      *     $Log:   /cobol85/pvcs/rmc85/verify/code/sorttest.cbv  $
      *    
      *       Rev 6.0   28 Apr 1994 20:22:10   randy
      *    No change.
      *    
      *       Rev 5.5   17 Mar 1994 15:24:08   ROBERT
      *    Project: RM8560
      *    Changed version to 6.0.
      *    
      *       Rev 5.4   04 Mar 1993 16:38:44   MIKE
      *    Project: RM8553
      *    Removed tabs.
      *    Changed version to 5.30.
      *
      *       Rev 5.3   16 Apr 1992 17:40:50   MIKE
      *    Change version number to 5.20.
      *
      *       Rev 5.2   02 Apr 1992 11:06:58   MIKE
      *    Fixup copyright notices.
      *
      *       Rev 5.1   22 Mar 1991  9:40:40   RANDY
      *    Update version.
      *
      *       Rev 5.0   15 Sep 1990  1:44:30   BILL
      *    No change.
      *
      *       Rev 1.3   30 Aug 1990  9:57:50   BILL
      *    Change program name to lower case for UNIX program library.
      *
      *       Rev 1.2   24 Aug 1990 13:32:54   BILL
      *    Add DELETE FILE statements to remove temporary output files.
      *
      *       Rev 1.1   14 Aug 1990 16:36:14   BILL
      *    Correct spelling of program name.
      *    Do not use line 25 of display.
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL-85.
       OBJECT-COMPUTER.  RMCOBOL-85.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT SORT-FILE ASSIGN TO DISK "sortwork".
           SELECT MERGE-1 ASSIGN TO DISK "merge1fl".
           SELECT MERGE-2 ASSIGN TO DISK "merge2fl".
      /
       DATA DIVISION.
       FILE SECTION.
       SD  SORT-FILE.
       01  SORT-RECORD.
           02  RECORD-NUMBER            PIC 99.
           02                           PIC X(04).
           02  NAME                     PIC X(10).
           02                           PIC X(4).
           02  ZIP                      PIC 9(5).
           02                           PIC X(04).
           02  FILE-NUMBER              PIC 9.

       FD  MERGE-1
           BLOCK CONTAINS 8 RECORDS
           DATA RECORD IS MERGE1-OUT-RECORD.
       01  MERGE1-OUT-RECORD            PIC X(30).

       FD  MERGE-2
           BLOCK CONTAINS 8 RECORDS
           DATA RECORD IS MERGE2-OUT-RECORD.
       01  MERGE2-OUT-RECORD            PIC X(30).
      /
       WORKING-STORAGE SECTION.
       01  TITLE                 PIC X(60) VALUE
           "RM/COBOL-85 Verify SORT/MERGE functions - Version 6.0".

       01  DATA-AREA.
           02  DATA-FILLER.
               03  PIC X(15) VALUE "SMITH     01023".
               03  PIC X(15) VALUE "JONES     90274".
               03  PIC X(15) VALUE "SMITH     90024".
               03  PIC X(15) VALUE "BROWN     02345".
               03  PIC X(15) VALUE "WONG      01456".
               03  PIC X(15) VALUE "JONES     45729".
               03  PIC X(15) VALUE "SMITH     78945".
               03  PIC X(15) VALUE "ADAMS     38217".
               03  PIC X(15) VALUE "SMITH     38217".
               03  PIC X(15) VALUE "MORRIS    86721".
               03  PIC X(15) VALUE "SMITH     01023".
               03  PIC X(15) VALUE "ROBERTS   46575".
               03  PIC X(15) VALUE "YOUNG     72365".
               03  PIC X(15) VALUE "SMITH     58724".
               03  PIC X(15) VALUE "STEVENSON 24345".
               03  PIC X(15) VALUE "DAVID     89431".
               03  PIC X(15) VALUE "SMITH     68752".
               03  PIC X(15) VALUE "JACKSON   31547".
           02  TEST-DATA  REDEFINES DATA-FILLER OCCURS 18 TIMES.
               03  TEST-NAME           PIC X(10).
               03  TEST-ZIP            PIC 9(5).

       01  COUNT-WK                    PIC 99 BINARY VALUE ZEROS.

       01                              PIC X(1).
           88  SORT-AT-END             VALUE "Y" WHEN FALSE "N".

       01                              PIC X(1).
           88  ALTERNATING-FLAG        VALUE "Y" WHEN FALSE "N".

       01  TEMP-X                      PIC X.

       01  CRT-LINE                    PIC 99 BINARY.
      /
       PROCEDURE DIVISION.
       MAIN-PGM.
           DISPLAY TITLE, LOW, LINE 1, ERASE.

           DISPLAY "This is a demonstration of the SORT/MERGE feature.",
                       LOW, LINE 4, POSITION 10.
           DISPLAY "Press any key to start SORT demonstration ...",
                       LOW, LINE 24, BLINK.
           ACCEPT TEMP-X, LOW, POSITION 0.

           DISPLAY "Sort by ZIP and name in ascending order and ",
                       LOW, LINE 1, ERASE,
                   "record number in descending order.", LOW,

                   "These are the unsorted records",
                       LOW, LINE 2, POSITION 1,
                   "These are the sorted records",
                       LOW, LINE 2, POSITION 41,
                   "Rec #   Name         ZIP",
                       LOW, LINE 3, POSITION 1,
                   "Rec #   Name         ZIP",
                       LOW, LINE 3, POSITION 41.

           SORT SORT-FILE
                   ON ASCENDING KEY ZIP, NAME
                   ON DESCENDING KEY RECORD-NUMBER
                   INPUT PROCEDURE IS INPUT-SORT
                   OUTPUT PROCEDURE IS OUTPUT-SORT.


           DISPLAY "Press any key to start MERGE demonstration ...",
                       LOW, LINE 24, BLINK.
           ACCEPT TEMP-X, LOW, POSITION 0.
      /
           DISPLAY "Merge by ZIP and name in ascending order and ",
                       LOW, LINE 1, ERASE
                   "record number in descending order", LOW.

           DISPLAY "This is a list of file number 1", LOW, LINE 2.
           DISPLAY "Rec #   Name         ZIP  File #", LOW, LINE 3.
           DISPLAY "This is a list of file number 2", LOW, LINE 14.
           DISPLAY "Rec #   Name         ZIP  File #", LOW, LINE 15.
           DISPLAY "This is a list of the merged files",
                       LOW, LINE 2, POSITION 41,
                   "Rec #   Name         ZIP  File #",
                       LOW, LINE 3, POSITION 41.

           OPEN INPUT MERGE-1 WITH LOCK.
           MOVE 4 TO CRT-LINE.
           SET SORT-AT-END TO FALSE.
           PERFORM UNTIL SORT-AT-END
               READ MERGE-1 AT END SET SORT-AT-END TO TRUE END-READ
               IF NOT SORT-AT-END
                   DISPLAY MERGE1-OUT-RECORD, LOW, LINE CRT-LINE
                   ADD 1 TO CRT-LINE
               END-IF
           END-PERFORM.
           CLOSE MERGE-1.

           OPEN INPUT MERGE-2 WITH LOCK.
           MOVE 16 TO CRT-LINE.
           SET SORT-AT-END TO FALSE.
           PERFORM UNTIL SORT-AT-END
               READ MERGE-2 AT END SET SORT-AT-END TO TRUE END-READ
               IF NOT SORT-AT-END
                   DISPLAY MERGE2-OUT-RECORD, LOW, LINE CRT-LINE
                   ADD 1 TO CRT-LINE
               END-IF
           END-PERFORM.
           CLOSE MERGE-2.

           MERGE SORT-FILE
                   ON ASCENDING KEY ZIP, NAME
                   ON DESCENDING KEY RECORD-NUMBER
                   USING MERGE-1 MERGE-2
                   OUTPUT PROCEDURE OUTPUT-MERGE.

           DISPLAY "End of demonstration.  ", HIGH, LINE 23, POSITION 50,
                   "Press any key to continue ...", HIGH,
                       LINE 24, POSITION 50.
           ACCEPT TEMP-X, LOW, POSITION 0.

           DELETE FILE MERGE-1.
           DELETE FILE MERGE-2.
           EXIT PROGRAM.
           STOP RUN.
      /

       INPUT-SORT.
           MOVE SPACES TO SORT-RECORD.
           PERFORM VARYING COUNT-WK FROM 1 BY 1 UNTIL COUNT-WK > 18
               MOVE COUNT-WK TO RECORD-NUMBER
               MOVE TEST-NAME (COUNT-WK) TO NAME
               MOVE TEST-ZIP  (COUNT-WK) TO ZIP
               ADD 4, COUNT-WK GIVING CRT-LINE
               DISPLAY SORT-RECORD, LOW, LINE CRT-LINE
               RELEASE SORT-RECORD
           END-PERFORM.


       OUTPUT-SORT.
           OPEN OUTPUT MERGE-1 WITH LOCK.
           OPEN OUTPUT MERGE-2 WITH LOCK.
           SET SORT-AT-END TO FALSE.
           SET ALTERNATING-FLAG TO FALSE.
           MOVE 5 TO CRT-LINE.
           PERFORM UNTIL SORT-AT-END
               RETURN SORT-FILE RECORD AT END SET SORT-AT-END TO TRUE
               END-RETURN
               IF NOT SORT-AT-END
                   DISPLAY SORT-RECORD
                               LOW, LINE CRT-LINE, POSITION 41
                   ADD 1 TO CRT-LINE
                   IF ALTERNATING-FLAG
                       MOVE 1 TO FILE-NUMBER
                       WRITE MERGE1-OUT-RECORD FROM SORT-RECORD
                       SET ALTERNATING-FLAG TO FALSE
                   ELSE
                       MOVE 2 TO FILE-NUMBER
                       WRITE MERGE2-OUT-RECORD FROM SORT-RECORD
                       SET ALTERNATING-FLAG TO TRUE
                   END-IF
               END-IF
           END-PERFORM.
           CLOSE MERGE-1 MERGE-2.
      /

       OUTPUT-MERGE.
           SET SORT-AT-END TO FALSE.
           MOVE 4 TO CRT-LINE.
           PERFORM UNTIL SORT-AT-END
               RETURN SORT-FILE RECORD AT END SET SORT-AT-END TO TRUE
               END-RETURN
               IF NOT SORT-AT-END
                   DISPLAY SORT-RECORD
                               LOW, LINE CRT-LINE, POSITION 41
                   ADD 1 TO CRT-LINE
               END-IF
           END-PERFORM.
