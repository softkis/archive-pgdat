       01  LINK-V.
           02 LNK-USER       PIC X(10).
           02 LNK-LANGUAGE   PIC XX.
           02 LG-IDX         PIC 99.
           02 LNK-AREA       PIC XXXX.
           02 LNK-COMPETENCE PIC 9.
           02 LNK-DATE.
              03 LNK-ANNEE  PIC 9(4).
              03 LNK-ANNEE-R REDEFINES LNK-ANNEE.
                 04 LNK-MILLE   PIC 9.
                 04 LNK-SUFFIX  PIC 999.
              03 LNK-ANNEE-S REDEFINES LNK-ANNEE.
                 04 LNK-SIECLE   PIC 99.
                 04 LNK-DECENNIE PIC 99.
              03 LNK-MOIS   PIC 99.
              03 LNK-JOUR   PIC 99.
           02 LNK-DATE-DEVIANT.
              03 LNK-ANNEE-D PIC 9(4).
              03 LNK-ANNEE-DR REDEFINES LNK-ANNEE-D.
                 04 LNK-MILLE-D  PIC 9.
                 04 LNK-SUFFIX-D PIC 999.
              03 LNK-MOIS-D  PIC 99.
           02 LNK-SUITE      PIC 99.
           02 S-I.
              03 SEM-MOIS OCCURS 12.
                 04 SEM-IDX     PIC  9 OCCURS 31.
                 04 SEM-NUM     PIC 99 OCCURS 31.
                 04 SEM-LINE    PIC  9 OCCURS 31.
           02 MOIS-ANNEE        PIC X(24).
           02 MOIS-JOURS REDEFINES MOIS-ANNEE.
              03 MOIS-JRS PIC 99 OCCURS 12.
           02 INDEXES.
              03 MOIS-IDX PIC 999V9(8) OCCURS 12.
              03 MOIS-SALMIN PIC 9(5)V9(4) OCCURS 12.
           02 INDEX-F REDEFINES INDEXES.
              03 MOIS-TX PIC 99999V9(6) OCCURS 12.
              03 MOIS-SALMINR PIC 9(5)V9(4) OCCURS 12.

           02 LNK-MATRICULE  PIC 9(11).
           02 LNK-KEY        PIC 9(8).
           02 LNK-PERSON     PIC 9(8).
           02 LNK-NOM        PIC X(60).
           02 LNK-NOM-R      REDEFINES LNK-NOM.       
              03 LNK-TEXT       PIC X(50).
              03 LNK-NUM        PIC 9(6).
              03 LNK-CALL-1     PIC 99.    
              03 LNK-CALL-2     PIC 99.
           02 LNK-STATUS     PIC 99.
           02 LNK-VAL        PIC S9(8)V9(4) COMP-3.
           02 LNK-VAL-2      PIC S9(8)V9(4) COMP-3.
           02 LNK-INDEX      PIC 99.
           02 LNK-CHOIX      PIC 99.
           02 LNK-YN         PIC X.
           02 LNK-POSITION   PIC 9(8).
           02 LNK-POSITION-R REDEFINES LNK-POSITION.
              03 LNK-LINE    PIC 99.
              03 LNK-COL     PIC 99.
              03 LNK-SIZE    PIC 99.
              03 LNK-SPACE   PIC 99.
           02 LNK-LOW        PIC X.
           02 LNK-EXC-KEY    PIC 9(4) COMP-1.
           02 LNK-A-N        PIC X.
           02 LNK-PRESENCE   PIC 9.
ANNEE      02 LNK-LIMITE     PIC 9999.
           02 LNK-COLOR1     PIC 9.
           02 LNK-COLOR2     PIC 9.
           02 LNK-COLOR3     PIC 9.
           02 LNK-FIRME      PIC 9(6).
           02 LNK-REGISTRE   PIC 9(8).
           02 LNK-FILLER     PIC X(28).
           02 LNK-SQL        PIC X.

      *    Copie du fichier menu 
      
           02 MENU-RECORD.
              03 MENU-KEY.
                 04 MENU-LEVEL.
                    05 MENU-LEV      PIC 99 OCCURS 5.

              03 MENU-BODY.
                 04 MENU-PROG-NAME   PIC X(8).
                 04 MENU-PROG-NAME-R REDEFINES MENU-PROG-NAME.
                    05 MENU-PROG-BEG PIC XX.
                    05 MENU-PROG-END PIC X(6).
                 04 MENU-COMPETENCE  PIC 9.
                 04 MENU-CALL        PIC X(8).
                 04 MENU-PASSWORD    PIC X(22).
                 04 MENU-DESCRIPTION PIC X(40).
                 04 MENU-HELP-TEXT   PIC 9(8).
                 04 MENU-PROG-NUMBER PIC 9(8).
                 04 MENU-PROG-NUMBERS REDEFINES MENU-PROG-NUMBER.
                    05 MENU-PROG-NUM PIC 9 OCCURS 8.
                 04 MENU-EXTENSION-1 PIC X(8).
                 04 MENU-EXTENSION-2 PIC X(8).
                 04 MENU-TEST        PIC 9.
                 04 MENU-BATCH       PIC 9.
                 04 MENU-LOG         PIC 9.
                 04 MENU-FILLER      PIC X(20).
               

           COPY "V-ERR.CPY".      
           COPY "FIRME.LNK".      

           