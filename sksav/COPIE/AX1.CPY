      ****************************
      * parameter for PRINT-PAGE *
      ****************************
       01  AX1-FIELDS.
           05 AX1-HEADER.
               10  AX1-ENTETE            PIC X(25).
               10  AX1-ANNEE             PIC X(10).
               10  AX1-MOIS              PIC X(10).
               10  AX1-NOMFIRME          PIC X(30).
               10  AX1-MATRICULE-FIRME   PIC X(13).
               10  AX1-TITRE             PIC X(80).
               10  AX1-COMMENTAIRE       PIC X(25).
           05 AX1-BODY.
               10  FILLER OCCURS 35.
                   15  AX1-NUM           PIC X(3).
               10  FILLER OCCURS 35.
                   15  AX1-MATRICULE     PIC X(11).
               10  FILLER OCCURS 35.
                   15  AX1-NOM           PIC X(20).
               10  FILLER OCCURS 35.
                   15  AX1-PRENOM        PIC X(20).
               10  FILLER OCCURS 35.
                   15  AX1-FONCTION      PIC X(14).
               10  FILLER OCCURS 35.
                   15  AX1-NATION        PIC X(3).
               10  FILLER OCCURS 35.
                   15  AX1-RES           PIC X(3).
               10  AX1-TOTEMPLOYE        PIC Z(5).
               10  AX1-TOTCHAUFFEURD     PIC Z(5).
               10  AX1-TOTCHAUFFEUR      PIC Z(5).
               10  AX1-TOTOUVRIER        PIC Z(5).
               10  AX1-TOTGERANT         PIC Z(5).
           05 AX1-BOTTOM.
               10  AX1-NOMGERANT         PIC X(45).
               10  AX1-LIEU              PIC X(20).
               10  AX1-DATE              PIC X(12).
