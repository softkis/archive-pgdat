       01  INPUT-ERROR           PIC 9.
       01  NOT-FOUND             PIC 9.
       01  EXC-KEY               PIC 9(4) COMP-1.
       01  ESC-KEY               PIC 9(4) COMP-1.
       01  NUL-KEY               PIC 9(4) COMP-1 VALUE 0.
       01  SAVE-KEY              PIC 9(4) COMP-1 VALUE 0.
       01  FAKE-KEY              PIC 9(4) COMP-1 VALUE 13.
       01  DEL-KEY               PIC 9(4) COMP-1 VALUE 98.
       01  WR-KEY                PIC 9(4) COMP-1 VALUE 99.
       01  PV-KEY                PIC 9(4) COMP-1 VALUE 65.
       01  NX-KEY                PIC 9(4) COMP-1 VALUE 66.
       01  IDX                   PIC 9(4)  VALUE 0.
       01  IDX-1                 PIC 9(4)  VALUE 0.
       01  IDX-2                 PIC 9(4)  VALUE 0.
       01  IDX-3                 PIC 9(4)  VALUE 0.
       01  IDX-4                 PIC 9(4)  VALUE 0.
       01  STORE                 PIC 9(4)  VALUE 0.
       01  STATUT                PIC 9 VALUE 0.
       01  LAST-PERSON           PIC 9(8) VALUE 0.
       01  COUT                  PIC 9(8) VALUE 0.
       01  COUT-DEBUT            PIC 9(8) VALUE 0.
       01  COUT-FIN              PIC 9(8) VALUE 0.
       01  COD-SAL               PIC 9(4) VALUE 0.
       01  CS-DEBUT              PIC 9(4) VALUE 0.
       01  CS-FIN                PIC 9(4) VALUE 0.
       01  ACC-IDX PIC 9(6).
       01  ACC-I REDEFINES ACC-IDX.
           02 LIN-IDX            PIC 99.
           02 COL-IDX            PIC 99.
           02 SIZ-IDX            PIC 99.
       01  SH-00                 PIC S9(10)V999999 COMP-3.
       01  SH-01                 PIC S9(10)V999999 COMP-3.
       01  ECRAN-IDX             PIC 99 VALUE 1.
       01  INDICE-ZONE           PIC 999 VALUE 1.
       01  DECISION              PIC 999.
       01  ACTION                PIC X VALUE " ".
       01  A-N                   PIC X VALUE "N".
       01  SAVE-LANGUAGE         PIC XX.
       01  SAVE-DATE.
           02 SAVE-ANNEE         PIC 9999.
           02 SAVE-MOIS          PIC 99.
           02 SAVE-JOUR          PIC 99.

       01  EXC-KEY-CTL.
           02 EXC-KEY-FUN     PIC 99  OCCURS 70.
       01  EXC-KEY-CTL-R REDEFINES EXC-KEY-CTL.
           02 EXC-KFR         PIC 9(10)  OCCURS 14.

       01  TODAY.
           02 TODAY-DATE   PIC 9(8).
           02 TODAY-DATE-R REDEFINES TODAY-DATE.
              03 TODAY-ANNEE  PIC 9999.
              03 TD-ANNEE-R REDEFINES TODAY-ANNEE.
                 04 TD-SIECLE PIC 99.
                 04 TD-ANNEE  PIC 99.
              03 TODAY-MJ  PIC 9999.
              03 TD-MJ-R REDEFINES TODAY-MJ.
                 04 TODAY-MOIS   PIC 99.
                 04 TODAY-JOUR   PIC 99.
           02 TODAY-TEMPS.
              03 TODAY-HEURE  PIC 99.
              03 TODAY-MIN    PIC 99.
              03 TODAY-SECS   PIC 9999.
       01  TODAY-A REDEFINES TODAY.
           02 TODAY-TIME.
              03 TODAY-DATE-A    PIC 9(8).
              03 TODAY-TEMPS-A   PIC 9(4).
           02 TODAY-FILLER.
              03 TODAY-FILL   PIC 9999.
       01  TODAY-B REDEFINES TODAY.
           02 TODAY-TIME-B.
              03 TODAY-DATE-B    PIC 9(8).
              03 TODAY-TEMPS-B   PIC 9(6).
           02 TODAY-FILLER.
              03 TODAY-FILL   PIC 99.
      

