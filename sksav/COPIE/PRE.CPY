      ****************************
      * parameter for PRINT-PAGE *
      ****************************
       01  PRE-CONVERSE-DATA.
           05  PRE-RET-CODE       
                                       PIC S9(4) COMP-1.
           05  PRE-LENS.
               10  PRE-LEN-LEN    
                                       PIC S9(4) COMP-1 VALUE +20.
               10  PRE-IP-NUM-LEN 
                                       PIC S9(4) COMP-1 VALUE +40.
               10  PRE-IP-CHAR-LEN
                                       PIC S9(4) COMP-1 VALUE +104.
               10  PRE-OP-NUM-LEN 
                                       PIC S9(4) COMP-1 VALUE +6.
               10  PRE-OP-CHAR-LEN  
                                       PIC S9(4) COMP-1 VALUE +2.
               10  PRE-FIELD-LEN  
                                       PIC S9(4) COMP-1 VALUE +628.
               10  PRE-COLR-LEN   
                                       PIC S9(4) COMP-1 VALUE +24.
               10  FILLER          
                                       PIC S9(4) COMP-1 VALUE +0.
               10  FILLER           
                                       PIC S9(4) COMP-1 VALUE +0.
               10  FILLER          
                                       PIC S9(4) COMP-1 VALUE +0.
           05  PRE-DATA.
      ******** PRE-IP-NUM-DATA ********
               10  PRE-KEY        
                                       PIC S9(4) COMP-1.
               10  FILLER              PIC X(38).
      ******** PRE-IP-CHAR-DATA *******
               10  PRE-NEXT-PANEL 
                                       PIC X(8).
               10  FILLER              PIC X(94).
               10  PRE-WAIT-SW    
                                       PIC X.
               10  FILLER              PIC X.
      ******** PRE-OP-NUM-DATA ********
               10  PRE-PAN-POS-SW 
                                       PIC S9(4) COMP-1.
               10  PRE-PAN-ROW    
                                       PIC S9(4) COMP-1.
               10  PRE-PAN-COL    
                                       PIC S9(4) COMP-1.
      ******** PRE-OP-CHAR-DATA ********
               10  FILLER              PIC X(2).
      ******** PRE-OP-VAR-DATA ********
           05  PRE-FIELDS.
               10  PRE-MOIS-ANNEE 
                                       PIC X(0020).
               10  PRE-PAT-NOM 
                                       PIC X(0050).
               10  PRE-PAT-ADRESS 
                                       PIC X(0040).
               10  PRE-PAT-MATRICULE 
                                       PIC X(0015).
               10  PRE-CHOIX-A 
                                       PIC X(0001).
               10  PRE-CHOIX-B 
                                       PIC X(0001).
               10  PRE-CHOIX-C 
                                       PIC X(0001).
               10  PRE-SAL-NOM 
                                       PIC X(0050).
               10  PRE-SAL-PRENOM 
                                       PIC X(0050).
               10  PRE-SAL-ADRESS 
                                       PIC X(0050).
               10  PRE-SAL-MATRICULE 
                                       PIC X(0015).
               10  PRE-LV.
                   15  PRE-INDEX-ACT
                                       PIC X(0015).
                   15  PRE-BRUT
                                       PIC X(0015).
                   15  PRE-COTIS-1
                                       PIC X(0015).
                   15  PRE-ASS-MAL
                                       PIC X(0015).
                   15  PRE-ASS-PEN
                                       PIC X(0015).
                   15  PRE-TOTAL
                                       PIC X(0015).
               10  PRE-BRUT-100 
                                       PIC X(0015).
               10  PRE-SOMME 
                                       PIC X(0015).
               10  PRE-SOMME-INDEX 
                                       PIC X(0015).
               10  PRE-TOUT-LETTRE 
                                       PIC X(0100).
               10  PRE-SOUSSIGNE 
                                       PIC X(0050).
               10  PRE-LIEU 
                                       PIC X(0030).
               10  PRE-DATE 
                                       PIC X(0020).
           05  PRE-COLRS.
               10  PRE-MOIS-ANNEE-C      
                                       PIC X.
               10  PRE-PAT-NOM-C      
                                       PIC X.
               10  PRE-PAT-ADRESS-C      
                                       PIC X.
               10  PRE-PAT-MATRICULE-C      
                                       PIC X.
               10  PRE-CHOIX-A-C      
                                       PIC X.
               10  PRE-CHOIX-B-C      
                                       PIC X.
               10  PRE-CHOIX-C-C      
                                       PIC X.
               10  PRE-SAL-NOM-C      
                                       PIC X.
               10  PRE-SAL-PRENOM-C      
                                       PIC X.
               10  PRE-SAL-ADRESS-C      
                                       PIC X.
               10  PRE-SAL-MATRICULE-C      
                                       PIC X.
               10  PRE-INDEX-ACT-C      
                                       PIC X.
               10  PRE-BRUT-C      
                                       PIC X.
               10  PRE-COTIS-1-C      
                                       PIC X.
               10  PRE-ASS-MAL-C      
                                       PIC X.
               10  PRE-ASS-PEN-C      
                                       PIC X.
               10  PRE-TOTAL-C      
                                       PIC X.
               10  PRE-BRUT-100-C      
                                       PIC X.
               10  PRE-SOMME-C      
                                       PIC X.
               10  PRE-SOMME-INDEX-C      
                                       PIC X.
               10  PRE-TOUT-LETTRE-C      
                                       PIC X.
               10  PRE-SOUSSIGNE-C      
                                       PIC X.
               10  PRE-LIEU-C      
                                       PIC X.
               10  PRE-DATE-C      
                                       PIC X.
      ********************************************
      * field ids - use for field identification *
      ********************************************
       01  PRE-IDS.
               05  PRE-MOIS-ANNEE-I
                                   PIC S9(4) COMP-1 VALUE +1.
               05  PRE-PAT-NOM-I
                                   PIC S9(4) COMP-1 VALUE +4.
               05  PRE-PAT-ADRESS-I
                                   PIC S9(4) COMP-1 VALUE +6.
               05  PRE-PAT-MATRICULE-I
                                   PIC S9(4) COMP-1 VALUE +7.
               05  PRE-CHOIX-A-I
                                   PIC S9(4) COMP-1 VALUE +5.
               05  PRE-CHOIX-B-I
                                   PIC S9(4) COMP-1 VALUE +2.
               05  PRE-CHOIX-C-I
                                   PIC S9(4) COMP-1 VALUE +3.
               05  PRE-SAL-NOM-I
                                   PIC S9(4) COMP-1 VALUE +8.
               05  PRE-SAL-PRENOM-I
                                   PIC S9(4) COMP-1 VALUE +9.
               05  PRE-SAL-ADRESS-I
                                   PIC S9(4) COMP-1 VALUE +10.
               05  PRE-SAL-MATRICULE-I
                                   PIC S9(4) COMP-1 VALUE +11.
               05  PRE-INDEX-ACT-I
                                   PIC S9(4) COMP-1 VALUE +12.
               05  PRE-BRUT-100-I
                                   PIC S9(4) COMP-1 VALUE +13.
               05  PRE-BRUT-I
                                   PIC S9(4) COMP-1 VALUE +14.
               05  PRE-COTIS-1-I
                                   PIC S9(4) COMP-1 VALUE +15.
               05  PRE-ASS-MAL-I
                                   PIC S9(4) COMP-1 VALUE +16.
               05  PRE-ASS-PEN-I
                                   PIC S9(4) COMP-1 VALUE +17.
               05  PRE-TOTAL-I
                                   PIC S9(4) COMP-1 VALUE +18.
               05  PRE-SOMME-I
                                   PIC S9(4) COMP-1 VALUE +19.
               05  PRE-SOMME-INDEX-I
                                   PIC S9(4) COMP-1 VALUE +20.
               05  PRE-TOUT-LETTRE-I
                                   PIC S9(4) COMP-1 VALUE +21.
               05  PRE-SOUSSIGNE-I
                                   PIC S9(4) COMP-1 VALUE +23.
               05  PRE-LIEU-I
                                   PIC S9(4) COMP-1 VALUE +22.
               05  PRE-DATE-I
                                   PIC S9(4) COMP-1 VALUE +24.
