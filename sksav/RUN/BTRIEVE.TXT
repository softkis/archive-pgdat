

				   README Contents

	  This document contains the following sections:

	       Changes and Enhancements


			      Changes and Enhancements

          Converting from RBADAPTR for DOS to RBADAPTR for Windows

               It is assumed that the user is familiar with using Btrieve
               files on DOS with the RBADAPTR.EXE TSR program.  For
               Windows, RBADAPTR is a dynamic-link library (DLL) and is
               included with this release in the file RBADAPTR.DLL.  It is
               also assumed that the user is somewhat familiar with running
               RM/COBOL-85 programs under Windows.  Prior to accessing
               Btrieve files from the RM/COBOL-85 for Windows RUNCOBOL,
               Btrieve for Windows (which is available from Novell, Inc.)
               must be installed.  If only remote Btrieve files will be
               accessed, then only the Windows and DOS Requester packages
               are needed on the PC.  If local Btrieve files will be
               accessed, then the client-based Btrieve Record Manager
               package is needed.

               Follow the instructions in the "Btrieve for Windows
               Installation and Operation" manual to install Btrieve for
               Windows on your PC.  If both local Btrieve files (client-
               based) and remote Btrieve files (server-based) are to be
               accessed simultaneously, pay particular attention to
               installing the Requester DLL as WBTRCALL.DLL and the local
               Record Manager DLL as WBTRLOCL.DLL as described in the
               manual.  Note that for version 6.10 of the Requester, the
               "local=yes" line under the "[brequestDPMI]" section must be
               in the NOVDB.INI file, not the WIN.INI file as stated in the
               manual.  You will probably want to specify the same
               parameter values (such as /m:64 /p:4096 /u:4) on the
               "options=" line under the "[btrieve]" section as you
               specified on the BTRIEVE.EXE command line for DOS.  Note
               that for version 5.10 of Btrieve for Windows, the
               "[btrieve]" section must be in the WIN.INI file.  Also note
               that the Requester TSR, BREQUEST.EXE, must be started before
               starting Windows.

               After installing Btrieve for Windows, verify access to an
               existing local Btrieve file and to an existing remote
               Btrieve file using the BTRTOOLS.EXE Btrieve Toolbox utility.
               For example, run BTRTOOLS from Windows and do a Stat
               operation specifying the pathname of an existing local file
               (on your local disk) and then do another Stat operation
               specifying the pathname of an existing remote file (on your
               Novell server).  If no errors occur and the information
               displayed for both files looks correct, then Btrieve for
               Windows is probably installed and configured correctly.

               RBADAPTR for Windows, which consists of the RBADAPTR.DLL
               file, was installed into your main Windows directory by the
               setup procedure for RM/COBOL-85 if you chose the option to
               do so.  If you did not initially choose to install it, you
               should run the setup procedure again and choose only the
               option box to install "Btrieve interface (RBADAPTR)".
               This should be the same directory where WBTRCALL.DLL was
               installed.  Use a text editor to add an EXTERNAL-ACCESS-
               METHOD configuration record to the RM/COBOL-85 Configuration
               file(s) used by each of the programs that will access
               Btrieve files.  The EXTERNAL-ACCESS-METHOD record is
               described below and a sample record is shown below.

               RM/COBOL-85 Configuration files and the configuration
               records they contain are described in Chapter 10 of the
               "RM/COBOL-85 Version 6 User's Guide" provided by Liant
               Software Corp. with your Windows RUNCOBOL.  A new
               configuration record, EXTERNAL-ACCESS-METHOD, has been added
               which must be specified in order to access foreign file
               formats such as Btrieve files.  The EXTERNAL-ACCESS-METHOD
               record has three keyword parameters:  NAME, CREATE-FILES,
               and OPTIONS.  To access Btrieve files, NAME=RBADAPTR must be
               specified.  This tells RUNCOBOL to load and call
               RBADAPTR.DLL to provide the interface between RUNCOBOL's
               File Management system and the Btrieve DLLs.  CREATE-FILES
               is specified as a YES or NO value.  If CREATE-FILES=YES is
               specified, RUNCOBOL will call RBADAPTR to create new Btrieve
               files.  If CREATE-FILES=NO is specified, RUNCOBOL will not
               call RBADAPTR to create new Btrieve files, which means that
               new files will be created by some other external access
               method or by the RM/COBOL-85 File Management system as
               RM/COBOL-85 indexed files.  The CREATE-FILES parameter is
               the same as the C parameter on the DOS RBADAPTR.EXE command
               line.  The default value is also the same:  YES.  The
               OPTIONS parameter specifies a quoted string of parameters to
               be passed to the RBADAPTR DLL and has the form:
                    OPTIONS='<parameter-string>'

               This is where the remaining parameters (other than C) from
               the DOS RBADAPTR.EXE command line are specified.
               Specifically, the parameters applicable to the Windows
               RBADAPTR are:  P=<page-size>, B=<Btrieve-page-size>,
               M=<mode>, L=<lock>, and O=<owner>.  A typical record might
               be the following:

               EXTERNAL-ACCESS-METHOD  NAME=RBADAPTR CREATE-FILES=YES
                  OPTIONS='P=512 B=4096 M=N L=I O=XYZZY'

               The K, S, and T parameters from the DOS RBADAPTR.EXE command
               line are not applicable to the Windows RBADAPTR DLL.  A
               complete description of the P, B, M, L, and O parameters is
               found in chapter 4 of the User's Guide. Note that different
               Windows applications can specify different RBADAPTR
               parameters via different RM/COBOL-85 configuration files.

               The following is a summary of the changes needed to access
               Btrieve files from Windows rather than DOS:

                    The client-based Btrieve Record Manager,
                    BTRIEVE.EXE, is now WBTRCALL.DLL, which is
                    converted to WBTRLOCL.DLL when remote files are
                    also accessed.  The parameters that were specified
                    on the command line are now specified in the
                    [btrieve] section in WIN.INI (or maybe NOVDB.INI,
                    depending on the version number).

                    In addition to the DOS Requester BREQUEST.EXE,
                    which remains the same and must be started before
                    starting Windows, there is a Windows Requester DLL
                    named WBTRCALL.DLL (not to be confused with the
                    Record Manager DLL which is named WBTRLOCL.DLL
                    when remote files are also accessed).  Some
                    parameters are specified in the [brequestDPMI]
                    section in NOVDB.INI (or maybe WIN.INI, depending
                    on the version number).

                    BTRTOOLS.EXE, which runs under Windows, provides
                    utility operations similar to those which
                    BUTIL.EXE provides on DOS.

                    The Adapter Program, RBADAPTR.EXE, is now
                    RBADAPTR.DLL.  The parameters that were specified
                    on the command line are now specified on an
                    EXTERNAL-ACCESS-METHOD configuration record:

                    DOS                 Windows
                    ------------------- -----------------------

                    RBADAPTR P=nnnn     EXTERNAL-ACCESS-METHOD
                                        & NAME=RBADAPTR
                                        & OPTIONS='P=nnnn'

                    RBADAPTR B=nnnn     EXTERNAL-ACCESS-METHOD
                                        & NAME=RBADAPTR
                                        & OPTIONS='B=nnnn'

                    RBADAPTR M=x        EXTERNAL-ACCESS-METHOD
                                        & NAME=RBADAPTR
                                        & OPTIONS='M=x'
                    RBADAPTR L=x        EXTERNAL-ACCESS-METHOD
                                        & NAME=RBADAPTR
                                        & OPTIONS='L=x'

                    RBADAPTR O=cccccccc EXTERNAL-ACCESS-METHOD
                                        & NAME=RBADAPTR
                                        & OPTIONS='O=cccccccc'

                    RBADAPTR C=N        EXTERNAL-ACCESS-METHOD
                                        & NAME=RBADAPTR
                                        & CREATE-FILES=NO

                    RBADAPTR K          (not applicable)

                    RBADAPTR S          (not applicable)

                    RBADAPTR T          (not applicable)

               A new feature has been added to RBADAPTR for Windows ,
               which writes a trace file of open requests to a user-
               specified filename.  This feature does not exist in RBADAPTR
               for DOS.  This feature is intended to be used when the user
               feels there is a problem with a Btrieve file not being
               successfully opened by a COBOL program.  It is NOT intended
               to be used in a production environment since it does degrade
               performance somewhat.  Also, the trace file can become quite
               large, which might exhaust disk space.  To turn on the trace
               feature, edit the RM/COBOL-85 Configuration file for the
               program in question and add a T=<trace-file-name> parameter
               to the OPTIONS keyword on the EXTERNAL-ACCESS-NAME record.
               For example, the following record will cause trace
               information to be written to the file C:\TEST\TRACE.FIL:

               EXTERNAL-ACCESS-METHOD  NAME=RBADAPTR
                  OPTIONS='T=C:\TEST\TRACE.FIL'

               The trace file will contain a "Begin open" and "End open"
               pair of records for every open request that RBADAPTR
               receives.  This includes all open operations that RUNCOBOL
               does for files, such as its message file, and the COBOL
               program file as well as every OPEN statement executed by the
               COBOL program.  The "End open" line will show the COBOL
               status code returned to the File Management System.  Between
               the "Begin" and "End" lines will be zero or more "BTRV
               Create" or "BTRV Open" lines showing the full pathname of
               the file and the exact Btrieve status code returned by the
               Windows Btrieve DLLs.  A sample trace file is shown in
               chapter 4 of the User's Guide.

               After diagnosing the problem, be sure to edit the
               configuration file again and remove the T=<trace-file-name>
               parameter from the OPTIONS keyword on the EXTERNAL-ACCESS-
               METHOD record.

               Windows Btrieve also has a trace feature which can be
               enabled using the "tracefile=" and "traceops=" keywords
               in the NOVDB.INI (or WIN.INI) file.  See Btrieve
               documentation for information on using its trace facility.

          Btrieve version 6 for Windows

               Btrieve Technologies, Inc. recently released version 6.15
               for Windows and, even more recently, released version 6.15
               for NetWare.

               Version 6.15 for Windows comes with a new version of
               BREQUEST.EXE, the DOS requester, which must be loaded
               prior to starting Windows.

               Version 6.15 for NetWare comes with an all-DLL requester
               for Windows which includes several new DLLs and requires
               Novell's NWIPXSPX.DLL and NWCALLS.DLL.

               Version 6.15 for Windows reads configuration information
               from the file BTI.INI in the main Windows directory rather
               than from NOVDB.INI or WIN.INI.  The sections and options
               have changed.  For example, the "options=" line is now
               in the [Btrieve Client] section and the "local=yes|no"
               line is now in the [Btrieve] section along with a
               "requester=yes|no" line.  Refer to the documentation and
               readme that come with Btrieve 6.15 for Windows for
               installation and configuration information.

               During Btrieve 6.15 installation, a "Btrieve Technologies
               Database" group is created and several program item icons
               are added to it.  In particular, a "Btrieve File Manager"
               program item is added that runs WBMANAGE.EXE; this is the
               equivalent of BTRTOOLS.EXE from earlier versions of
               Btrieve for Windows.  After Btrieve 6.15 for Windows is
               successfully installed and configured, verify access to
               existing local and remote Btrieve files using the "Btrieve
               File Manager" before attempting to use RM/COBOL-85 and
               RBADAPTR.DLL to access Btrieve files.  Btrieve is normally
               installed in C:\BTI.  In this, case it is best to include
               C:\BTI\WIN\BIN in your PATH before starting Windows.

               Contact Btrieve Technologies, Inc.
                       8834 Capital of Texas Highway North
                       Suite 300
                       Austin, Texas 78759
                       U.S.A.
                       1-800-BTRIEVE or 512-794-1719
               for further information about Btrieve products.

          RBADAPTR version 6.08 for Windows

               RBADAPTR.DLL shipped with RM/COBOL-85 version 6.08 now
               supports the following features of RM/COBOL-85 version 6
               for Windows:  split keys, duplicate prime keys, multiple
               record locks, and START with FIRST or LAST.
               In addition, RBADAPTR now supports the RUN-INDEX-FILES
               DATA-COMPRESSION and BLOCK-SIZE keywords and RBADAPTR now
               returns expanded error codes for better error reporting.
               The above enhancements are all in the category of support
               for enhancements already made to RM/COBOL-85.  In addition,
               RBADAPTR now supports selected features of Btrieve version
               6 and 6.1 files as well as Btrieve version 6.1 MicroKernel
               Database Engines.  RBADAPTR supports the Btrieve maximum
               of 119 key segments, repeating duplicates, and the no
               currency change (NCC) option on Inserts and Updates.

               Using repeating duplicates along with the NCC option
               should eliminate the possible position lost errors that
               could occur when a second user deleted records as the
               first user was reading through them.  RBADAPTR now allows
               Btrieve files that have multiple alternate collating
               sequences (ACS) defined, although all Btrieve keys that
               map to RM/COBOL-85 keys must use ACS number zero since
               COBOL defines one ACS per file.


               RBADAPTR has several parameters that are specified on
               an EXTERNAL-ACCESS-METHOD configuration record which
               is of the form:
                  EXTERNAL-ACCESS-METHOD
                     NAME=RBADAPTR
                     CREATE-FILES=<YES|NO>
                     OPTIONS='<parameter-string>'
               Two new option parameters have been added in version
               6.08:  D=<duplicate-type> and I=<yes-or-no>.

               D=L specifies that Linked Duplicatable keys are to be
               used for files created by RBADAPTR.  This is the default.

               D=R specifies that Repeating Duplicatable keys are to be
               used for files created by RBADAPTR.

               Linked duplicates mean that only one copy of the duplicated
               key value is stored in index pages. The data records with
               the duplicated key value are linked together with pointers
               in a doubly linked list.  Repeating duplicates mean that the
               duplicated key value is repeated in the index pages for each
               data record with that value. The data records are not
               linked together.  Using repeating duplicates uses more space
               in index pages but, saves space in data pages and also helps
               avoid position lost errors when files are shared.

               I=Y specifies that an initial message box should be displayed
               when RBADAPTR.DLL is first loaded.  This message box shows
               the RBADAPTR version number and the option parameter string
               that was passed to it from the EXTERNAL-ACCESS-METHOD
               configuration record.  I=Y should be used only in a
               non-production environment.

               I=N specifies no display of the initial Message Box.  This
               is the default and should be used in a production environment.

               The other option parameters that may be specified are:
               P=<page-size>, B=<Btrieve-page-size>, M=<mode>, L=<lock>,
               O=<owner>, and T=<trace-file-name>.  A typical record might
               be the following:

               EXTERNAL-ACCESS-METHOD  NAME=RBADAPTR  CREATE-FILES=YES
               &  OPTIONS='P=1024,B=4096,D=R,L=A,M=N,O=XYZZY'

               The ampersand (&) beginning the second line above is the
               configuration file record continuation character.  More
               information about configuration is found in Chapter 10 of
               the RM/COBOL-85 Version 6 User's Guide.  A complete
               description of the B, L, M, O, P, and T parameters is
               found in Chapter 4 of the User's Guide.  Note that different
               Windows applications can specify different RBADAPTR
               parameters via different RM/COBOL-85 configuration files.

               Two RUN-INDEX-FILES keywords now have meaning for RBADAPTR
               (see Chapter 10 in the User's Guide).

               Specifying DATA-COMPRESSION=NO causes RBADAPTR to create
               uncompressed Btrieve files.  The default is to create
               compressed Btrieve files.  (Note that Btrieve does not
               support key compression.)

               Specifying BLOCK-SIZE=nnnn causes RBADAPTR to create files
               with a page size of nnnn according to the following:
                  RBADAPTR first computes the minimum allowable page size
                  for the file based on the record size, number of key
                  segments, type of duplicates, etc.  It then uses the
                  first valid value greater than or equal to the computed
                  minimum in the following order:

                     1) from the BLOCK CONTAINS clause in the program's
                        file description entry,
                     2) from the P=<page-size> option parameter on the
                        EXTERNAL-ACCESS-METHOD record,
                     3) from the RUN-INDEX-FILES BLOCK-SIZE=<size>
                        configuration record.

                  If none of the above three values are present or
                  acceptable, then RBADAPTR uses the computed minimum.

               RBADAPTR.DLL version 6.08 for Windows has been tested
               with RM/COBOL-85 version 6.08 for Windows using the
               following Btrieve versions:
                  For local Btrieve files:
                     Btrieve 6.15 for Windows
                  For remote Btrieve files:
                     Btrieve 6.15  for NetWare on NetWare 3.12
                     Btrieve 6.10c for NetWare on NetWare 4.10
                     Btrieve 5.15  for NetWare on NetWare 3.11
               RBADAPTR no longer supports versions of Btrieve prior
               to Btrieve version 5.  It is recommended that users
               upgrade to a version 6.1 or later Btrieve engine.


