       IDENTIFICATION DIVISION.
       PROGRAM-ID. "wintest".
      *
      * Title:  wintest.cbl
      *         RM/COBOL Windows Test Driver
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *AUTHOR: RAH.
      *DATE WRITTEN: 2/22/90
      *PROGRAM DESCRIPTION.
      *This is a main menu program that calls eight subroutines
      *based on the selection made.  It tests the windows.
      *
      *INPUT-FILE - None
      *OPERATOR-RESPONSE - Enter selection number from terminal.
      *OUTPUT-FILE - None
      *
      * Version Identification:
      *   $Revision:   6.2.1.2  $
      *   $Date:   21 Sep 1999 13:22:46  $
      *
       ENVIRONMENT DIVISION.
       DATA DIVISION.

       WORKING-STORAGE SECTION.
       COPY "version.cpy".

       01  TITLE.
           05  PIC X(40) VALUE
           "RM/COBOL Verification Program - Version ".
           05  PIC X(4)  VALUE CURRENT-VERSION.

       01  SCREEN-NUM-ROWS              PIC 999.
       01  SCREEN-NUM-COLS              PIC 999.
       77  X                            PIC X.

       01  ESCAPE-KEY   GLOBAL          PIC 99 VALUE 0.
       01  SELECT-NUM   GLOBAL          PIC 99 VALUE 1.
       01  SELECT-HOLD                  PIC 99 VALUE 0.

       COPY "win.cpy".

       SCREEN SECTION.
       01  MENU-SCREEN.
           05 BLANK SCREEN.
           05  LINE 3 COL 10
               "Window Screen ".
           05  LINE 3 COL 43
               "Window status = ".
           05  LINE 3 COL 61
               PIC 999 USING WINDOW-STATUS.
           05  LINE 8 COL 5
               " 1.  Display window attributes ".
           05  LINE 9 COL 5
               " 2.  Display title positions ".
           05  LINE 10 COL 5
               " 3.  Display border types ".
           05  LINE 11 COL 5
               " 4.  Display relative positions".
           05  LINE 12 COL 5
               " 5.  Display status codes".
           05  LINE 13 COL 5
               " 6.  Display colors".
           05  LINE 14 COL 5
               " 7.  Exit program".
           05  LINE 18 COL 15
               "Select command number".
           05  LINE 19 COL 15
               "   Last command = ".
           05  LINE 21 COL 15
               "<Enter> goes to next test".

       01  SELECT-SCREEN  COL 37 LINE 18  PIC 9 USING SELECT-NUM.
       01  DISP-LAST-SCRN COL 37 LINE 19  PIC 9 FROM SELECT-HOLD.

       01  SCREEN-SIZE-SCREEN.
           05  BLANK SCREEN.
           05  LINE 2 COL 5
               "How many rows are there on your crt? ".
           05  PIC 999 TO SCREEN-NUM-ROWS.
           05  LINE 3 COL 5
               "How many columns are there on your crt? ".
           05  PIC 999 TO SCREEN-NUM-COLS.

       01  NO-WINDOW-MGR-SCREEN.
           05  BLANK SCREEN.
           05  LINE 5 COL 15
               "Window Manager is not available.".
           05  PIC X TO X.
      /
       PROCEDURE DIVISION.
       BEGIN-MAIN.
           DISPLAY SCREEN-SIZE-SCREEN.
           ACCEPT SCREEN-SIZE-SCREEN.

       DISPLAY-MENU.
           DISPLAY SPACE ERASE.
      *Define and create window.
           COMPUTE WCB-NUM-ROWS = SCREEN-NUM-ROWS - 2.
           COMPUTE WCB-NUM-COLS = SCREEN-NUM-COLS - 2.
           MOVE "S" TO WCB-LOCATION-REFERENCE.
           MOVE "Y" TO WCB-BORDER-SWITCH.
           MOVE  2  TO WCB-BORDER-TYPE.
           MOVE "*" TO WCB-BORDER-CHAR.
           MOVE "T" TO WCB-TITLE-LOCATION.
           MOVE "C" TO WCB-TITLE-POSITION.
           MOVE  TITLE TO WCB-TITLE.
           MOVE 0 TO WCB-TITLE-LENGTH.
           DISPLAY WCB HIGH ERASE
               LINE 2 POSITION 2 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           IF WINDOW-STATUS = 305
               DISPLAY NO-WINDOW-MGR-SCREEN
               ACCEPT NO-WINDOW-MGR-SCREEN
               GO END-PROGRAM.

      *Display the section menu for screen tests.
           DISPLAY MENU-SCREEN.

      *Display the number of the last test executed.
           DISPLAY DISP-LAST-SCRN.
           DISPLAY SELECT-SCREEN.
           ACCEPT SELECT-SCREEN.
       GOTO-PARA.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".
           GO TO COMMAND-1, COMMAND-2, COMMAND-3, COMMAND-4, COMMAND-5,
               COMMAND-6, COMMAND-7,
               DEPENDING ON SELECT-NUM.
           GO TO DISPLAY-MENU.
       COMMAND-1.
           CALL "winattrb" USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
           CANCEL "winattrb".
           GO TO CHECK-STATUS.
       COMMAND-2.
           CALL "wintitle" USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
           CANCEL "wintitle".
           GO TO CHECK-STATUS.
       COMMAND-3.
           CALL "winbordr" USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
           CANCEL "winbordr".
           GO TO CHECK-STATUS.
       COMMAND-4.
           CALL "winreltv" USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
           CANCEL "winreltv".
           GO TO CHECK-STATUS.
       COMMAND-5.
           CALL "winstat" USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
           CANCEL "winstat".
           GO TO CHECK-STATUS.
       COMMAND-6.
           CALL "wincolor" USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
           CANCEL "wincolor".
           GO TO CHECK-STATUS.
       COMMAND-7.
           GO TO END-WINDOW-TEST.

      *Determine how program terminated and appropriately stop
      *the run, begin again or rerun the last test.
       CHECK-STATUS.
           ACCEPT ESCAPE-KEY FROM ESCAPE KEY.
           EVALUATE ESCAPE-KEY
               WHEN 13 MOVE SELECT-NUM TO SELECT-HOLD
                   ADD 1 TO SELECT-NUM
                   GO TO DISPLAY-MENU
               WHEN 01 GO TO GOTO-PARA
               WHEN 27 GO TO END-WINDOW-TEST
               WHEN OTHER MOVE SELECT-NUM TO SELECT-HOLD
                   ADD 1 TO SELECT-NUM
                   GO TO DISPLAY-MENU.

       END-WINDOW-TEST.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".
       END-PROGRAM.
           EXIT PROGRAM.
           STOP RUN.
