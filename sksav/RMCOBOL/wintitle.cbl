       IDENTIFICATION DIVISION.
       PROGRAM-ID. "wintitle".
      *
      * Title:  wintitle.cbl
      *         RM/COBOL Test Windows Titles
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *AUTHOR: RAH.
      *DATE WRITTEN: 2/23/90
      *PROGRAM DESCRIPTION.
      *This program tests window titles fill characters.
      *
      *INPUT-FILE - None
      *OPERATOR-RESPONSE - Enter selection number from terminal.
      *OUTPUT-FILE - None
      *
      * Version Identification:
      *   $Revision:   6.1.1.1  $
      *   $Date:   21 Sep 1999 13:23:52  $
      *
       ENVIRONMENT DIVISION.
       DATA DIVISION.

       WORKING-STORAGE SECTION.
       77  X                            PIC X.

       COPY "win.cpy".

       LINKAGE SECTION.
       01 SCREEN-NUM-ROWS               PIC 999.
       01 SCREEN-NUM-COLS               PIC 999.

       PROCEDURE DIVISION USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
       BEGIN-MAIN.
      *Define and create window.
           COMPUTE WCB-NUM-ROWS = SCREEN-NUM-ROWS - 3.
           COMPUTE WCB-NUM-COLS = SCREEN-NUM-COLS - 2.
           MOVE "S" TO WCB-LOCATION-REFERENCE.
           MOVE "Y" TO WCB-BORDER-SWITCH.
           MOVE  2  TO WCB-BORDER-TYPE.
           MOVE "*" TO WCB-BORDER-CHAR.
           MOVE "T" TO WCB-TITLE-LOCATION.
           MOVE "C" TO WCB-TITLE-POSITION.
           MOVE  " WINDOWS:  RM/COBOL  Window Titles "
                   TO WCB-TITLE.
           MOVE 35 TO WCB-TITLE-LENGTH.

      *Display TOP CENTER TITLE.
           DISPLAY WCB HIGH ERASE  LINE  2 POSITION  2
                                   CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Window status = "              LINE  3 POSITION 58,
                   WINDOW-STATUS                   LINE  3 POSITION 74,
                   "The title for this window    " LINE 12 POSITION 25,
                   "should begin top center      " LINE 13 POSITION 25,
                   "Press <Return> to continue "
                        LINE WCB-NUM-ROWS POSITION 25.
           ACCEPT  X LINE WCB-NUM-ROWS POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display TOP LEFT TITLE.
           MOVE "L" TO WCB-TITLE-POSITION.
           DISPLAY WCB HIGH LINE 2 POSITION 2 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Window status = "              LINE  3 POSITION 58,
                   WINDOW-STATUS                   LINE  3 POSITION 74,
                   "The title for this window    " LINE 12 POSITION 25,
                   "should begin top left        " LINE 13 POSITION 25,
                   "Press <Return> to continue "
                        LINE WCB-NUM-ROWS POSITION 25.
           ACCEPT  X LINE WCB-NUM-ROWS POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display TOP RIGHT TITLE.
           MOVE "R" TO WCB-TITLE-POSITION.
           DISPLAY WCB HIGH LINE 2 POSITION 2 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Window status = "              LINE  3 POSITION 58,
                   WINDOW-STATUS                   LINE  3 POSITION 74,
                   "The title for this window    " LINE 12 POSITION 25,
                   "should begin top right       " LINE 13 POSITION 25,
                   "Press <Return> to continue "
                        LINE WCB-NUM-ROWS POSITION 25.
           ACCEPT  X LINE WCB-NUM-ROWS POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display BOTTOM CENTER TITLE.
           MOVE "B" TO WCB-TITLE-LOCATION.
           MOVE "C" TO WCB-TITLE-POSITION.
           DISPLAY WCB HIGH LINE 2 POSITION 2 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Window status = "              LINE  3 POSITION 58,
                   WINDOW-STATUS                   LINE  3 POSITION 74,
                   "The title for this window    " LINE 12 POSITION 25,
                   "should begin bottom center   " LINE 13 POSITION 25,
                   "Press <Return> to continue "
                        LINE WCB-NUM-ROWS POSITION 25.
           ACCEPT  X LINE WCB-NUM-ROWS POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display BOTTOM LEFT TITLE.
           MOVE "L" TO WCB-TITLE-POSITION.
           DISPLAY WCB HIGH LINE 2 POSITION 2 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Window status = "              LINE  3 POSITION 58,
                   WINDOW-STATUS                   LINE  3 POSITION 74,
                   "The title for this window  "   LINE 12 POSITION 25,
                   "should begin bottom left   "   LINE 13 POSITION 25,
                   "Press <Return> to continue "
                        LINE WCB-NUM-ROWS POSITION 25.
           ACCEPT  X LINE WCB-NUM-ROWS POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display BOTTOM RIGHT TITLE.
           MOVE "R" TO WCB-TITLE-POSITION.
           DISPLAY WCB HIGH LINE 2 POSITION 2 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Window status = "              LINE  3 POSITION 58,
                   WINDOW-STATUS                   LINE  3 POSITION 74,
                   "The title for this window  "   LINE 12 POSITION 25,
                   "should begin bottom right  "   LINE 13 POSITION 25,
                   "Press <Return> to continue "
                        LINE WCB-NUM-ROWS POSITION 25.
           ACCEPT  X LINE WCB-NUM-ROWS POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display NO TITLE.
           MOVE " " TO WCB-TITLE-LOCATION.
           MOVE " " TO WCB-TITLE-POSITION.
           MOVE  0  TO WCB-TITLE-LENGTH.
           MOVE SPACES TO WCB-TITLE.
           DISPLAY WCB HIGH LINE 2 POSITION 2 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Window status = "              LINE  3 POSITION 58,
                   WINDOW-STATUS                   LINE  3 POSITION 74,
                   "There should be no title   "   LINE 12 POSITION 25,
                   "    for this window        "   LINE 13 POSITION 25,
                   "Press <Return> to continue "
                        LINE WCB-NUM-ROWS POSITION 25.
           ACCEPT  X LINE WCB-NUM-ROWS POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display FILL CHAR.
           MOVE "T" TO WCB-TITLE-LOCATION.
           MOVE "C" TO WCB-TITLE-POSITION.
           MOVE  0  TO WCB-TITLE-LENGTH.
           MOVE  " WINDOWS:  RM/COBOL  Fill Characters "
                   TO WCB-TITLE.
           MOVE 39 TO WCB-TITLE-LENGTH.
           MOVE "+" TO WCB-FILL-CHAR.
           MOVE "Y" TO WCB-FILL-SWITCH.
           DISPLAY WCB HIGH LINE 2 POSITION 2 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Window status = "              LINE  3 POSITION 58,
                   WINDOW-STATUS                   LINE  3 POSITION 74,
                   "  The title for this window  " LINE 12 POSITION 25,
                   "  should begin top center    " LINE 13 POSITION 25,
                   "     fill character is +     " LINE 14 POSITION 25,
                   "Press <Return> to continue "
                        LINE WCB-NUM-ROWS POSITION 25.
           ACCEPT  X LINE WCB-NUM-ROWS POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

           EXIT PROGRAM.
           STOP RUN.
       END PROGRAM "wintitle".
