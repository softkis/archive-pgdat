       IDENTIFICATION DIVISION.
       PROGRAM-ID.     "rmcrl2l".
      *
      * Title:  rmcrl2l.cbl
      *        RM/COBOL Relative File Conversion
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      * ==============
      *
      *    This is the RELATIVE FILE CONVERSION example program
      *    to be used to convert:
      *          RM/COBOL 2.n ----> RM/COBOL 6.n
      *          Record size greater than 512 bytes.
      *
      *    This is an example program that must be edited, compiled
      *    using the RM/COBOL RMCOBOL command, and executed using
      *    the RM/COBOL RUNCOBOL command.
      *
      *    1. Edit the maximum record size value at the ^^^^ points
      *       indicated below.
      *
      *    2. Compile the edited source using the RM/COBOL RMCOBOL
      *       command.
      *
      *    3. Use the DOS SET command to indicated the input (2.n) and
      *       output (85) relative files' DOS pathname, filename and
      *       extension as follows:
      *          SET INPUT=<input file's pathname\filename.ext>
      *          SET OUTPUT=<output file's pathname\filename.ext>
      *
      *    4. Execute the object program produced by step 2 to perform
      *       the file conversion.
      *
      *    5. Use the DOS SET command to remove the input and output
      *       files' pathname, filename and extension from the DOS
      *       environment table as follows:
      *          SET INPUT=
      *          SET OUTPUT=
      *
      * ===========
      *
      *  VERSION IDENTIFICATION:
      *    $Revision:   6.3.1.2  $
      *    $Date:   21 Sep 1999 12:59:42  $
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.    RMC.
       OBJECT-COMPUTER.    RMC.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT OUT-FILE ASSIGN TO RANDOM "OUTPUT"
           ORGANIZATION IS RELATIVE ACCESS IS RANDOM
           RELATIVE KEY IS RELKEY.

           SELECT IN-FILE ASSIGN TO RANDOM "INPUT"
           ORGANIZATION IS BINARY SEQUENTIAL
           FILE STATUS IS IN-STATUS.
       I-O-CONTROL.
           SAME RECORD AREA FOR IN-FILE, OUT-FILE.

       DATA DIVISION.
       FILE SECTION.

       FD  OUT-FILE.
       01  OUT-RECORD                  PIC X(1000).
      * >>> Change to maximum record size    ^^^^

       FD  IN-FILE.
       01  IN-RECORD.
           02                                     PIC X(0512).
               88  DELETED-RECORD      VALUE LOW-VALUES.
           02                                     PIC X(0488).
      * >>> Change to (maximum record size minus 512)   ^^^^

       WORKING-STORAGE SECTION.
       COPY "version.cpy".

       01  TITLE.
           05  PIC X(55) VALUE
           "RM/COBOL 2.n to 6.n Relative file conversion - Version ".
           05  PIC X(4)  VALUE CURRENT-VERSION.

       01  NUMBER-OF-RECORDS           PIC 9(10) VALUE IS ZERO.

       01  DELETED-RECORDS             PIC 9(10) VALUE IS ZERO.

       01  RESPONSE-INDICATOR          PIC X.
           88  AFFIRMATIVE-RESPONSE    VALUES "Y", "y".
           88  NEGATIVE-RESPONSE       VALUES "N", "n".

       01  RELKEY                      PIC 9(10).

       01  IN-STATUS                   PIC X(2).
           88  END-OF-INPUT-FILE       VALUES "04", "10", "46", "97".

       PROCEDURE DIVISION.
       DECLARATIVES.
       IN-FILE-ERROR SECTION.
           USE AFTER STANDARD ERROR PROCEDURE ON IN-FILE.
       A.
           IF NOT END-OF-INPUT-FILE THEN
               DISPLAY "Input file error status ", IN-STATUS,
                       " caused premature termination." LINE 20
               EXIT PROGRAM
               STOP RUN
           END-IF.
       END DECLARATIVES.

       MAIN SECTION.
       START-UP.
           DISPLAY TITLE ERASE LINE 2
           DISPLAY " You must edit and compile this program and set the"
                       LINE 5.
           DISPLAY " appropriate environment variables before running"
           DISPLAY " this program.  Have you done this? (Y/N) " LINE 7.
           SET NEGATIVE-RESPONSE TO TRUE.
           ACCEPT RESPONSE-INDICATOR UPDATE POSITION 0 TAB.
           IF NOT (AFFIRMATIVE-RESPONSE OR NEGATIVE-RESPONSE) THEN
               GO TO START-UP
           END-IF.
           IF NOT AFFIRMATIVE-RESPONSE THEN
               DISPLAY "Program terminated by negative response."
               EXIT PROGRAM
               STOP RUN
           END-IF.

           SET NEGATIVE-RESPONSE TO TRUE.
           OPEN OUTPUT OUT-FILE WITH LOCK.
           OPEN INPUT IN-FILE WITH LOCK.
           MOVE 1 TO RELKEY.
           DISPLAY "Files opened."               LINE 9.
           DISPLAY "Valid data records found:"   LINE 10.
           DISPLAY "Deleted records noted:"      LINE 12.
           DISPLAY "Record number:"              LINE 14.

           READ IN-FILE.
           PERFORM UNTIL END-OF-INPUT-FILE
               IF DELETED-RECORD
                   ADD 1 TO DELETED-RECORDS
                   DISPLAY DELETED-RECORDS LINE 12 POSITION 28
               ELSE
                   WRITE OUT-RECORD
                   ADD 1 TO NUMBER-OF-RECORDS
                   DISPLAY NUMBER-OF-RECORDS LINE 10 POSITION 28
               END-IF
               DISPLAY RELKEY LINE 14 POSITION 28
               ADD 1 TO RELKEY
               READ IN-FILE END-READ
           END-PERFORM.

           CLOSE OUT-FILE.
           CLOSE IN-FILE.
           DISPLAY "Files closed.  Conversion complete." LINE 20.
           EXIT PROGRAM.
           STOP RUN.
       END PROGRAM "rmcrl2l".
