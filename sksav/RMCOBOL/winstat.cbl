       IDENTIFICATION DIVISION.
       PROGRAM-ID. "winstat".
      *
      * Title:  winstat.cbl
      *         RM/COBOL Test Windows Status Codes
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *AUTHOR: RAH.
      *DATE WRITTEN: 3/01/90
      *PROGRAM DESCRIPTION.
      *This program tests window status codes.
      *
      *INPUT-FILE - None
      *OPERATOR-RESPONSE - Enter selection number from terminal.
      *OUTPUT-FILE - None
      *
      * Version Identification:
      *   $Revision:   6.1.1.1  $
      *   $Date:   21 Sep 1999 13:21:52  $
      *
       ENVIRONMENT DIVISION.
       DATA DIVISION.

       WORKING-STORAGE SECTION.
       77  X                            PIC X.

       COPY "win.cpy".

       LINKAGE SECTION.
       01 SCREEN-NUM-ROWS               PIC 999.
       01 SCREEN-NUM-COLS               PIC 999.

       PROCEDURE DIVISION USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
       BEGIN-MAIN.
      *Clear screen.
           DISPLAY SPACE LINE 1 ERASE EOS.

      *Define and create window.
           COMPUTE WCB-NUM-ROWS = SCREEN-NUM-ROWS - 3.
           COMPUTE WCB-NUM-COLS = SCREEN-NUM-COLS - 2.
           MOVE "S" TO WCB-LOCATION-REFERENCE.
           MOVE "Y" TO WCB-BORDER-SWITCH.
           MOVE  2  TO WCB-BORDER-TYPE.
           MOVE "*" TO WCB-BORDER-CHAR.
           MOVE "T" TO WCB-TITLE-LOCATION.
           MOVE "C" TO WCB-TITLE-POSITION.
           MOVE  " WINDOWS:  RM/COBOL  Window Status Codes "
                   TO WCB-TITLE.
           MOVE 41 TO WCB-TITLE-LENGTH.

      *Display NORMAL WINDOW STATUS = 0.
           DISPLAY WCB HIGH ERASE LINE 2 POSITION 2
                   CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Normal window status"        LINE 10 POSITION 30,
                   "Window status = "            LINE 13 POSITION 30,
                   WINDOW-STATUS                 LINE 13 POSITION 46,
                   "STATUS should = 000"         LINE 14 POSITION 30,
                   "Press <Return> to continue " LINE 22 POSITION 27
           ACCEPT  X LINE 22 POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display window title will not fit on screen.
           DISPLAY SPACE LINE 1 ERASE EOS.
           MOVE "N" TO WCB-BORDER-SWITCH.
           MOVE "T" TO WCB-TITLE-LOCATION.
           DISPLAY WCB HIGH LINE 1 POSITION 2 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Title will not fit on screen" LINE 10 POSITION 26,
                   "Window status = "             LINE 13 POSITION 30,
                   WINDOW-STATUS                  LINE 13 POSITION 46,
                   "Status should = 301"          LINE 14 POSITION 30,
                   "Press <Return> to continue "  LINE 22 POSITION 27
           ACCEPT  X LINE 22 POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display window border will not fit on screen.
           DISPLAY SPACE LINE 1 ERASE EOS.
           MOVE "Y" TO WCB-BORDER-SWITCH.
           MOVE  2  TO WCB-BORDER-TYPE.
           MOVE "*" TO WCB-BORDER-CHAR.
           MOVE "T" TO WCB-TITLE-LOCATION.
           DISPLAY WCB HIGH LINE 4 POSITION 2 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Border will not fit on screen" LINE 10 POSITION 26,
                   "Window status = "              LINE 13 POSITION 31,
                   WINDOW-STATUS                   LINE 13 POSITION 47,
                   "Status should = 301"           LINE 14 POSITION 31,
                   "Press <Return> to continue "   LINE 22 POSITION 27
           ACCEPT  X LINE 22 POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display window title is too long for window.
           DISPLAY SPACE LINE 1 ERASE EOS.
           COMPUTE WCB-NUM-ROWS = SCREEN-NUM-ROWS - 3.
           MOVE 41 TO WCB-NUM-COLS.
           DISPLAY WCB HIGH LINE 2 POSITION 20 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Title is too long for window" LINE 7  POSITION 8,
                   "Window status = "             LINE 12 POSITION 12,
                   WINDOW-STATUS                  LINE 12 POSITION 28,
                   "Status should = 302"          LINE 13 POSITION 12,
                   "Press <Return> to continue "  LINE 22 POSITION 8.
           ACCEPT  X LINE 21 POSITION 35 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display window will not fit on screen.
           DISPLAY SPACE LINE 1 ERASE EOS.
           MOVE SCREEN-NUM-ROWS TO WCB-NUM-ROWS.
           MOVE SCREEN-NUM-COLS TO WCB-NUM-COLS.
           DISPLAY WCB HIGH LINE 2 POSITION 2 CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "Window will not fit on screen" LINE 10 POSITION 26,
                   "Window status = "              LINE 13 POSITION 31,
                   WINDOW-STATUS                   LINE 13 POSITION 47,
                   "Status should = 303"           LINE 14 POSITION 31,
                   "Press <Return> to continue "   LINE 22 POSITION 27
           ACCEPT  X LINE 22 POSITION 55 NO BEEP.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".

      *Display no windows active.
           DISPLAY SPACE LINE 1 ERASE EOS.
           DISPLAY WCB CONTROL "WINDOW-REMOVE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "No windows are active "      LINE 10 POSITION 30,
                   "Window status = "            LINE 13 POSITION 31,
                   WINDOW-STATUS                 LINE 13 POSITION 47,
                   "Status should = 304"         LINE 14 POSITION 31,
                   "Press <Return> to continue " LINE 22 POSITION 27
           ACCEPT  X LINE 22 POSITION 55 NO BEEP.
           DISPLAY SPACE LINE 1 ERASE EOS.
           EXIT PROGRAM.
           STOP RUN.
       END PROGRAM "winstat".
