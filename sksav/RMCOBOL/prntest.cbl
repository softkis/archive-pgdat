       IDENTIFICATION DIVISION.
       PROGRAM-ID.  "prntest".
      *
      * Title:  prntest.cbl
      *        RM/COBOL-85 Printer Test
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      * Version Identification:
      *   $Revision:   6.2.1.2  $
      *   $Date:   21 Sep 1999 13:14:14  $
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PFILE ASSIGN TO PRINT, "PRINTER".
      *
       DATA DIVISION.
       FILE SECTION.
       FD  PFILE.
       01  FD-RECORD       PIC X(132).
      *
       WORKING-STORAGE SECTION.
       COPY "version.cpy".

       01  TITLE.
           05  PIC X(44) VALUE
           "RM/COBOL Verify printer operation - Version ".
           05  PIC X(4)  VALUE CURRENT-VERSION.

       01  WS-NEW-PAGE               PIC X(132) VALUE "NEW PAGE".
       01  WS-NEXT-LINE              PIC X(132) VALUE
               "          NEXT LINE".
       01  WS-FIVE-LINES             PIC X(132) VALUE
               "                    5 LINES".
       01  WS-SAME-LINE              PIC X(132) VALUE
               "                              SAME LINE".
       01  WS-NEW-PAGE-B             PIC X(132) VALUE
               "                                      NEW PAGE NEXT".
       01  WS-NEXT-LINE-B            PIC X(132) VALUE
               "          LINE NEXT".
       01  WS-FIVE-LINES-B           PIC X(132) VALUE
               "                    5 NEXT".
       01  RETURN-KEY                PIC X.
      /
       PROCEDURE DIVISION.
       PARA-1.
           DISPLAY TITLE, LOW, LINE 1, ERASE.
           OPEN OUTPUT PFILE WITH LOCK.
           DISPLAY "This section tests printer configuration."
                       LOW, LINE 10, POSITION 20.
           DISPLAY "Make sure that the printer is attached and online.",
                       LOW, LINE 12, POSITION 20.
           DISPLAY "Type any key to continue...",
                       LOW, LINE 15, POSITION 30.
           ACCEPT RETURN-KEY POSITION 0.

       PARA-2.
           WRITE FD-RECORD FROM WS-NEW-PAGE
               AFTER ADVANCING PAGE.
           WRITE FD-RECORD FROM WS-SAME-LINE
               AFTER ADVANCING ZERO LINES.
           WRITE FD-RECORD FROM WS-FIVE-LINES
               AFTER ADVANCING 5 LINES.
           WRITE FD-RECORD FROM WS-SAME-LINE AFTER ADVANCING 0 LINES.
           WRITE FD-RECORD FROM WS-NEXT-LINE.
           WRITE FD-RECORD FROM WS-NEXT-LINE.
           WRITE FD-RECORD FROM WS-NEW-PAGE-B
               BEFORE ADVANCING PAGE.
           WRITE FD-RECORD FROM WS-SAME-LINE AFTER ADVANCING 0 LINES.
           WRITE FD-RECORD FROM WS-FIVE-LINES-B
               BEFORE ADVANCING 5 LINES.
           WRITE FD-RECORD FROM WS-SAME-LINE AFTER ADVANCING 0 LINES.
           WRITE FD-RECORD FROM WS-NEXT-LINE-B
               BEFORE ADVANCING 1 LINE.
           WRITE FD-RECORD FROM WS-NEXT-LINE-B
               BEFORE ADVANCING 1 LINE.
       PROGRAM-END.
           CLOSE PFILE.
