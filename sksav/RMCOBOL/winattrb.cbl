       IDENTIFICATION DIVISION.
       PROGRAM-ID. "winattrb".
      *
      * Title:  winattrb.cbl
      *         RM/COBOL Test Window Attributes
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *AUTHOR: RAH.
      *DATE WRITTEN: 2/23/90
      *PROGRAM DESCRIPTION.
      *This program test attributes displayed with windows.
      *
      *INPUT-FILE - None
      *OPERATOR-RESPONSE - Enter selection number from terminal.
      *OUTPUT-FILE - None
      *
      * Version Identification:
      *   $Revision:   6.1.1.1  $
      *   $Date:   21 Sep 1999 13:18:30  $
      *
       ENVIRONMENT DIVISION.
       DATA DIVISION.

       WORKING-STORAGE SECTION.
       77  X                            PIC X.

       COPY "win.cpy".

       77  NESTED-INDEX                 PIC 99 BINARY VALUE 0.
       01  NESTED-WINDOWS.
           03  NESTED-WCB               PIC X(80) OCCURS 9.

       LINKAGE SECTION.
       01 SCREEN-NUM-ROWS               PIC 999.
       01 SCREEN-NUM-COLS               PIC 999.

       SCREEN SECTION.
       01  MENU-SCREEN.
           05  BLANK SCREEN.
           05  LINE 3 COL 25 HIGHLIGHT
               "Window status = ".
           05  LINE 3 COL 43 HIGHLIGHT
               PIC 999 USING WINDOW-STATUS.

       01  RETURN-SCREEN.
           05  LINE 21 COL 17 HIGHLIGHT
               "Press <Return> for next test ".
           05  LINE 21 COL 51  PIC X USING X.
      /
       PROCEDURE DIVISION USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
       BEGIN-MAIN.
      *Define and create window.
           COMPUTE WCB-NUM-ROWS = SCREEN-NUM-ROWS - 3.
           COMPUTE WCB-NUM-COLS = SCREEN-NUM-COLS - 2.
           MOVE "S" TO WCB-LOCATION-REFERENCE.
           MOVE "Y" TO WCB-BORDER-SWITCH.
           MOVE  2  TO WCB-BORDER-TYPE.
           MOVE "*" TO WCB-BORDER-CHAR.
           MOVE "T" TO WCB-TITLE-LOCATION.
           MOVE "C" TO WCB-TITLE-POSITION.
           MOVE  " WINDOWS:  RM/COBOL  Window Attributes "
                   TO WCB-TITLE.
           MOVE 39 TO WCB-TITLE-LENGTH.
           MOVE 0 TO NESTED-INDEX.

      *Display BEEP window.
           ADD 1 TO NESTED-INDEX.
           MOVE WCB TO NESTED-WCB (NESTED-INDEX).
           DISPLAY NESTED-WCB (NESTED-INDEX) BEEP LINE 2 POSITION 2
                   CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY MENU-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 25,
                   "should begin on line 1 col 1  " LINE  6 POSITION 25,
                   "         BEEP = ON            " LINE  7 POSITION 25.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Display HIGH window.
           ADD 1 TO NESTED-INDEX.
           MOVE WCB TO NESTED-WCB (NESTED-INDEX).
           DISPLAY NESTED-WCB (NESTED-INDEX) HIGH LINE 2 POSITION 2
                   CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY MENU-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 25,
                   "should begin on line 1 col 1  " LINE  6 POSITION 25,
                   "       BORDER = HIGH          " LINE  7 POSITION 25.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Display HIGH REVERSE window.
           SUBTRACT 2 FROM WCB-NUM-ROWS.
           SUBTRACT 2 FROM WCB-NUM-COLS.
           ADD 1 TO NESTED-INDEX.
           MOVE WCB TO NESTED-WCB (NESTED-INDEX).
           DISPLAY NESTED-WCB (NESTED-INDEX) LINE 3 POSITION 3
                   HIGH REVERSE CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY MENU-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 25,
                   "should begin on line 2 col 2  " LINE  6 POSITION 25,
                   "   BORDER = HIGH/REVERSE      " LINE  7 POSITION 25.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Display HIGH BLINK window.
           SUBTRACT 2 FROM WCB-NUM-ROWS.
           SUBTRACT 2 FROM WCB-NUM-COLS.
           ADD 1 TO NESTED-INDEX.
           MOVE WCB TO NESTED-WCB (NESTED-INDEX).
           DISPLAY NESTED-WCB (NESTED-INDEX) LINE 4 POSITION 4
                   HIGH BLINK CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY MENU-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 24,
                   "should begin on line 3 col 3  " LINE  6 POSITION 24,
                   "    BORDER = HIGH/BLINK       " LINE  7 POSITION 24.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Display HIGH BLINK REVERSE window.
           SUBTRACT 2 FROM WCB-NUM-ROWS.
           SUBTRACT 2 FROM WCB-NUM-COLS.
           ADD 1 TO NESTED-INDEX.
           MOVE WCB TO NESTED-WCB (NESTED-INDEX).
           DISPLAY NESTED-WCB (NESTED-INDEX) LINE 5 POSITION 5
                   HIGH BLINK REVERSE CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY MENU-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 23,
                   "should begin on line 4 col 4  " LINE  6 POSITION 23,
                   "BORDER = HIGH/BLINK/REVERSE   " LINE  7 POSITION 23.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Display LOW window.
           SUBTRACT 2 FROM WCB-NUM-ROWS.
           SUBTRACT 2 FROM WCB-NUM-COLS.
           ADD 1 TO NESTED-INDEX.
           MOVE WCB TO NESTED-WCB (NESTED-INDEX).
           DISPLAY NESTED-WCB (NESTED-INDEX) LOW LINE 6 POSITION 6
                   CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY MENU-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 22,
                   "should begin on line 5 col 5  " LINE  6 POSITION 22,
                   "        BORDER = LOW          " LINE  7 POSITION 22.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Display LOW BLINK window.
           SUBTRACT 2 FROM WCB-NUM-ROWS.
           SUBTRACT 2 FROM WCB-NUM-COLS.
           ADD 1 TO NESTED-INDEX.
           MOVE WCB TO NESTED-WCB (NESTED-INDEX).
           DISPLAY NESTED-WCB (NESTED-INDEX) LINE 7 POSITION 7
                   LOW BLINK CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY MENU-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 21,
                   "should begin on line 6 col 6  " LINE  6 POSITION 21,
                   "     BORDER = LOW/BLINK       " LINE  7 POSITION 21.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Display LOW/BLINK window.
           SUBTRACT 2 FROM WCB-NUM-ROWS.
           SUBTRACT 2 FROM WCB-NUM-COLS.
           ADD 1 TO NESTED-INDEX.
           MOVE WCB TO NESTED-WCB (NESTED-INDEX).
           DISPLAY NESTED-WCB (NESTED-INDEX) LINE 8 POSITION 8
                   LOW BLINK REVERSE CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY MENU-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 20,
                   "should begin on line 7 col 7  " LINE  6 POSITION 20,
                   " BORDER = LOW/BLINK/REVERSE   " LINE  7 POSITION 20.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Display FILL CHAR window.
           SUBTRACT 2 FROM WCB-NUM-ROWS.
           SUBTRACT 2 FROM WCB-NUM-COLS.
           MOVE "Y" TO WCB-FILL-SWITCH.
           MOVE "%" TO WCB-FILL-CHAR.
           ADD 1 TO NESTED-INDEX.
           MOVE WCB TO NESTED-WCB (NESTED-INDEX).
           DISPLAY NESTED-WCB (NESTED-INDEX) LOW LINE 9 POSITION 9
                   CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY "The border for this window    " LINE  2 POSITION 19,
                   "should begin on line 8 col 8  " LINE  3 POSITION 19,
                   "        BORDER = LOW          " LINE  4 POSITION 19,
                   "     Fill character is %      " LINE  5 POSITION 19.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Remove windows.
           PERFORM UNTIL NESTED-INDEX < 1
               DISPLAY NESTED-WCB (NESTED-INDEX) CONTROL "WINDOW-REMOVE"
               SUBTRACT 1 FROM NESTED-INDEX
           END-PERFORM.
           EXIT PROGRAM.
           STOP RUN.
       END PROGRAM "winattrb".
