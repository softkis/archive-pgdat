       IDENTIFICATION DIVISION.
       PROGRAM-ID. DOVERIFY.
      *
      * Title: doverify.cbl
      *        RM/COBOL Verify Suite driver for compilation and execution
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      * Version Identification:
      *   $Revision:   6.7.1.1  $
      *   $Date:   21 Sep 1999 13:08:16  $
      *
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT CBL-FILE ASSIGN TO RANDOM CBL-NAME
             ORGANIZATION IS LINE SEQUENTIAL.
           SELECT COB-FILE ASSIGN TO RANDOM COB-NAME.

       DATA DIVISION.
       FILE SECTION.
       FD  CBL-FILE.
       01  CBL-RECORD PIC X(72).

       FD  COB-FILE.
       01  COB-RECORD PIC X(256).

       WORKING-STORAGE SECTION.
       01  COB-NAME        PIC X(14).
       01  CBL-NAME        PIC X(8) VALUE "null.cbl".

       01  VERIFY-SUITE.
           02              PIC X(8) VALUE "filetest".
           02              PIC X(8) VALUE "nuctest".
           02              PIC X(8) VALUE "prntest".
           02              PIC X(8) VALUE "sorttest".
           02              PIC X(8) VALUE "vdttest".
           02              PIC X(8) VALUE "verify".
           02              PIC X(8) VALUE "wintest".
           02              PIC X(8) VALUE "winattrb".
           02              PIC X(8) VALUE "winbordr".
           02              PIC X(8) VALUE "wincolor".
           02              PIC X(8) VALUE "winreltv".
           02              PIC X(8) VALUE "winstat".
           02              PIC X(8) VALUE "wintitle".
       01  VERIFY-ARRAY REDEFINES VERIFY-SUITE.
           02  VERIFY-NAME PIC X(8) OCCURS 13 TIMES.

       01  COMPILING.
           02              PIC X(10) VALUE "Compiling ".
           02  TARGET-NAME PIC X(8).
           02              PIC X(3) VALUE "...".

       01  COMPILER.
           02              PIC X(8) VALUE "rmcobol ".
           02  PRG-NAME    PIC X(8) VALUE SPACES.
           02  OPTIONS     PIC X(8) VALUE SPACES.

       01  COMP-OF         PIC X(15) VALUE "Compilation of ".

       01  INTEGER-GROUP   USAGE BINARY.
           02  I               PIC S9(4).
           02  STAT            PIC S9(4).
       01  DISPLAY-GROUP   USAGE DISPLAY.
           02  X               PIC X.
           02  PRESENT         PIC X VALUE "F".

       PROCEDURE DIVISION.
       MAIN.
      * Clear the screen, but don't use ERASE as it would
      * clear the logo bitmap.
           DISPLAY SPACE LINE 1 POSITION 1 ERASE EOS.

       MAIN-LOOP.
           DISPLAY "       RM/COBOL Verification Suite      "
             LINE 1 REVERSE.
           DISPLAY "Press 1 to Compile, 2 to execute, 3 to exit"
             LINE 2 ERASE EOL.
           ACCEPT X PROMPT LINE 0 POSITION 0.
           IF X = "1" THEN GO TO COMPILER.
           IF X = "2" THEN GO TO RUNTIME.
           IF X = "3" THEN GO TO Z.
           GO TO MAIN-LOOP.

       COMPILER.
      * Test for compiler existence.
      * Compile a null program.  If the returned status is -1
      * rmcobol was not executed.

           IF PRESENT = "F" THEN
               DISPLAY "Testing for RM/COBOL Compiler..."
                 LINE 3 ERASE EOL
               OPEN OUTPUT CBL-FILE
               MOVE '       IDENTIFICATION DIVISION.'
                 TO CBL-RECORD
               WRITE CBL-RECORD
               MOVE '       PROGRAM-ID. "null".'
                 TO CBL-RECORD
               WRITE CBL-RECORD
               MOVE '       END PROGRAM "null".'
                 TO CBL-RECORD
               WRITE CBL-RECORD
               CLOSE CBL-FILE
               MOVE CBL-NAME TO PRG-NAME OF COMPILER
      * option N - no object.
               MOVE " N K" TO OPTIONS OF COMPILER
               DISPLAY "_" LINE 4 POSITION 1
               DISPLAY SPACE LINE 4 POSITION 1
               CALL "C$FORGET"
               CALL "SYSTEM" USING COMPILER "N" STAT
               DELETE FILE CBL-FILE

               IF STAT NOT EQUAL ZERO THEN
                   DISPLAY "not located" LINE 3 POSITION 34
                   GO TO MAIN-LOOP
               END-IF
      * compile and execute.
               DISPLAY "located" LINE 3 POSITION 34
               MOVE "T" TO PRESENT
           END-IF.

      * compile the verification suite.

      * option K - kill banner.
           MOVE " K A Y=3" TO OPTIONS OF COMPILER.
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > 13
               MOVE VERIFY-NAME(I) TO TARGET-NAME
               MOVE TARGET-NAME TO PRG-NAME OF COMPILER

      * build .cob file name and delete old file.
               STRING TARGET-NAME DELIMITED BY SPACE
                 ".cob" DELIMITED BY SIZE
                 SPACES DELIMITED BY SIZE
                 INTO COB-NAME
               DELETE FILE COB-FILE

      * compile module
               DISPLAY COMPILING LINE 3 ERASE EOL
               DISPLAY "_" LINE 4 POSITION 1
               DISPLAY SPACE LINE 4 POSITION 1
               CALL "C$FORGET"
               CALL "SYSTEM" USING COMPILER "N" STAT
               IF STAT = 0 THEN
                   DISPLAY " OK" LINE 3 POSITION 22
               ELSE
                   DISPLAY " errors encountered" LINE 3 POSITION 22
                   GO TO MAIN-LOOP
               END-IF
           END-PERFORM.
           GO TO MAIN-LOOP.

       RUNTIME.
           CALL "verify".
           DISPLAY SPACE ERASE.
           GO TO MAIN-LOOP.

       Z.
           EXIT PROGRAM.
           STOP RUN.

       END PROGRAM DOVERIFY.
