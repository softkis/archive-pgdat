       ID DIVISION.
       PROGRAM-ID.  IXVERIFY.
      *
      * Title: ixverify.cbl
      *        RM/COBOL-85 Verify Program for InfoExpress
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      * Version Identification:
      *   $Revision:   1.3.1.1  $
      *   $Date:   21 Sep 1999 13:10:02  $
      *
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  SPX-OR-TCP      PIC X VALUE "T".
           88  SPX         VALUES "S", "s".
           88  TCP         VALUES "T", "t".
       01  NOTHING         PIC X.
       LINKAGE SECTION.
       01  MAIN-ARGUMENT.
           02  ARGUMENT-SIZE           PIC 9(4) BINARY.
           02  ARGUMENT-STRING.
               03    ARGUMENT-CHAR     PIC X(1) OCCURS 0 TO 100
                                       DEPENDING ON ARGUMENT-SIZE.
       PROCEDURE DIVISION USING MAIN-ARGUMENT.
       MAIN SECTION.
       A.
           IF ARGUMENT-SIZE > ZERO
               MOVE ARGUMENT-STRING TO SPX-OR-TCP.
           IF SPX PERFORM RMCLNSPX
             ELSE PERFORM RMCLNTCP.
           STOP RUN.
       RMCLNTCP SECTION.
       A.
           DISPLAY "A Message Box titled 'RM/InfoExpress Client"
                   LINE 10 POSITION 20 ERASE
                   "WinSock Info' should already have been"
                   LINE 11 POSITION 20
                   "displayed in the center of the screen."
                   LINE 12 POSITION 20
                   "If it was not displayed, then RMTCP32.DLL"
                   LINE 13 POSITION 20
                   "or your Windows Sockets implementation is"
                   LINE 14 POSITION 20
                   "not installed or running properly."
                   LINE 15 POSITION 20
                   "Press any key to continue ... "
                   LINE 17 POSITION 20.
           ACCEPT NOTHING LINE 0 POSITION 0.
       RMCLNSPX SECTION.
       A.
           DISPLAY "A Message Box titled 'RM/InfoExpress Client"
                   LINE 10 POSITION 20 ERASE
                   "SPX Info' should already have been"
                   LINE 11 POSITION 20
                   "displayed in the center of the screen."
                   LINE 12 POSITION 20
                   "If it was not displayed, then RMSPX32.DLL"
                   LINE 13 POSITION 20
                   "or your Novell SPX implementation is"
                   LINE 14 POSITION 20
                   "not installed or running properly."
                   LINE 15 POSITION 20
                   "Press any key to continue ... "
                   LINE 17 POSITION 20.
           ACCEPT NOTHING LINE 0 POSITION 0.
