       IDENTIFICATION DIVISION.
       PROGRAM-ID.  PEXAMPLE.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION:  Printing Examples using P$ Routines
      *
      *  Routines Used:  C$SetDevelopmentMode
      *                  C$ClearDevelopmentMode
      *                  P$EnableDialog
      *                  P$ClearDialog
      *                  P$SetFont
      *                  P$ClearFont
      *                  P$SetTextColor
      *                  P$TextOut
      *                  P$SetPen
      *                  P$SetBoxShade
      *                  P$MoveTo
      *                  P$LineTo
      *                  P$DrawBox
      *                  P$DrawLine
      *                  P$GetTextMetrics
      *                  P$SetTextPosition
      *                  P$GetPosition
      *                  P$SetPosition
      *                  P$GetTextExtent
      *                  P$SetDialog
      *                  P$SetPitch
      *                  P$ChangeDeviceModes
      *                  P$GetDeviceCapabilities
      *                  P$SetLineExtendMode
      *
      * Version Identification:
      *   $Revision:   1.2.1.2  $
      *   $Date:   27 Sep 1999 14:51:56  $
      *
      *********************************************************************

       DATE-WRITTEN.     01/25/1999
       REMARKS.          Printer Example Code Fragments from Appendix E
                         in the RM/COBOL Version 7.00 User's Guide.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PFILE  ASSIGN TO PRINT, "PRINTER?".
           SELECT PFILE1 ASSIGN TO PRINT, "PRINTER?".
           SELECT PFILE2 ASSIGN TO PRINT, "PRINTER?".
           SELECT PFILE3 ASSIGN TO PRINT, "PRINTER?".
       DATA DIVISION.
       FILE SECTION.
       FD  PFILE.
       01  FD-RECORD       PIC X(132).
       FD  PFILE1.
       01  FD-RECORD1       PIC X(80).
       FD  PFILE2.
       01  FD-RECORD2       PIC X(80).
       FD  PFILE3.
       01  FD-RECORD3       PIC X(80).
       WORKING-STORAGE SECTION.
       01  Ws-Fields.
           03  X-POS          Pic 9(10)      Value Zero.
           03  Y-POS          Pic 9(10)      Value Zero.
           03  Ws-X           PIC 9(2)V9(4)  Value Zero.
           03  Ws-Y           PIC 9(2)V9(4)  Value Zero.
           03  Ws-X2          PIC 9(2)V9(4)  Value Zero.
           03  Ws-Y2          PIC 9(2)V9(4)  Value Zero.
           03  Ws-X-M         Pic 9(10)V9(3) Value Zero.
           03  Ws-Y-M         Pic 9(10)V9(3) Value Zero.
           03  Ws-X-D         Pic 9(10)      Value Zero.
           03  Ws-XP-D        Pic 9(10)      Value Zero.
           03  Ws-Dummy       Pic 9(10)      Value Zero.
           03  Ws-XB-D        Pic 9(10)      Value Zero.
           03  Ws-TOP         Pic 9(10)      Value Zero.
           03  Ws-BASE        Pic 9(10)      Value Zero.
           03  Ws-BOTTOM      Pic 9(10)      Value Zero.
           03  Ws-SizeWidth   Pic 9(10)      Value Zero.
           03  Ws-SizeHeight  Pic 9(10)      Value Zero.
           03  Ws-Text-Height Pic 9(10)V9(3) Value Zero.
           03  Ws-Text-Width  Pic 9(10)V9(3) Value Zero.
           03  Width          Pic 9(10)      Value Zero.
           03  Height         Pic 9(10)      Value Zero.
           03  Ws-Handle1     Pic 9(10)      Value Zero.
           03  Ws-Handle2     Pic 9(10)      Value Zero.
           03  Ws-Handle3     Pic 9(10)      Value Zero.
           03  Point-Size     Pic 9(2)       Value Zero.

       01  Ws-Accept          Pic 9(2)       Value Zero.
           88 Example1        Value 1.
           88 Example2        Value 2.
           88 Example3        Value 3.
           88 Example4        Value 4.
           88 Example5        Value 5.
           88 Example6        Value 6.
           88 Example7        Value 7.
           88 Example8        Value 8.
           88 Example9        Value 9.
           88 Example10       Value 10.
           88 Example11       Value 11.
           88 Example12       Value 12.
           88 Example13       Value 13.
           88 Example14       Value 14.
           88 Example15       Value 15.
           88 Exit-Pgm        Value 16.

       01  Ws-YN              Pic X(1) Value Spaces.
           88 WS-Yes          Value "Y".
           88 WS-No           Value "N".
       01  Ws-Ex6             Pic 9(1) Value Zero.
           88 Valid-Printer   Value 1.
           88 Invalid-Printer Value 2.
           88 Valid-Values    Values 1 thru 2.
       01  Validity-Flag      Pic 9(1) Value Zero.
       01  Dialog-Return      Pic 9(1) Value Zero.
           88 Dialog-OK                Value 0.
           88 Dialog-Cancel            Value 1.
           88 Dialog-Error             Value 2.

       78  WS-Size-Inches               Value 7.500.
       78  WS-Size-Centimeters          Value 19.000.

       78  One-Inch                     Value 1.0000.
       78  Half-Inch                    Value 0.5000.
       78  Qtr-Inch                     Value 0.2500.
       78  Eighth-Inch                  Value 0.1250.
       78  Sixteenth-Inch               Value 0.0625.

       78  One-Centimeter               Value 1.0000.
       78  One-Millimeter               Value 0.1000.

       78  One-Inch-Line                Value 0.3750.
       78  Half-Inch-Line               Value 0.3125.
       78  Qtr-Inch-Line                Value 0.2500.
       78  Eighth-Inch-Line             Value 0.1875.
       78  Sixteenth-Inch-Line          Value 0.0625.

       78  One-Centimeter-Line          Value 0.7000.
       78  Five-Millimeter-Line         Value 0.4000.
       78  One-Millimeter-Line          Value 0.2000.

       01  WS-RULER-VARS.
           03  WS-N                     PIC 9(2)V9(4) Value Zero.
           03  WS-COUNT                 PIC 9(1) Value 0.
           03  WS-COUNT-OUT      Redefines WS-Count PIC X(1).
           03  WS-Cent-COUNT            PIC 9(2) Value 0.
           03  WS-Cent-COUNT-OUT Redefines WS-Cent-Count PIC X(2).

       01  WS-TABLE.
           03                     PIC V9(4) VALUE Sixteenth-Inch-Line.
           03                     PIC V9(4) VALUE Eighth-Inch-Line.
           03                     PIC V9(4) VALUE Sixteenth-Inch-Line.
           03                     PIC V9(4) VALUE Qtr-Inch-Line.
           03                     PIC V9(4) VALUE Sixteenth-Inch-Line.
           03                     PIC V9(4) VALUE Eighth-Inch-Line.
           03                     PIC V9(4) VALUE Sixteenth-Inch-Line.
           03                     PIC V9(4) VALUE Half-Inch-Line.
           03                     PIC V9(4) VALUE Sixteenth-Inch-Line.
           03                     PIC V9(4) VALUE Eighth-Inch-Line.
           03                     PIC V9(4) VALUE Sixteenth-Inch-Line.
           03                     PIC V9(4) VALUE Qtr-Inch-Line.
           03                     PIC V9(4) VALUE Sixteenth-Inch-Line.
           03                     PIC V9(4) VALUE Eighth-Inch-Line.
           03                     PIC V9(4) VALUE Sixteenth-Inch-Line.
           03                     PIC V9(4) VALUE One-Inch-Line.
       01  WS-TABLE-RED REDEFINES WS-TABLE.
           03 WS-LINE-LENGTH      PIC V9(4) Occurs 16 Times
                                            Indexed By IDX1.
       copy "windefs.cpy".

       copy "devcaps.cpy".

       copy "printdlg.cpy".

       copy "txtmtric.cpy".

       copy "logfont.cpy".

       PROCEDURE DIVISION.
       MAIN-START.
           CALL "C$SetDevelopMentMode".
           CALL "C$Title" Using
               "RM/COBOL P$ Routine Examples from Appendix E".
           Perform Display-Menu Until Exit-Pgm.
           CALL "C$ClearDevelopmentMode".
           STOP RUN.

       DISPLAY-MENU.
           Display " Example Printing Code Fragments " line 2 column 23
               Control "Reverse,Erase,FColor=White,BColor=Blue".
           Display "1. Printing a Watermark             " Line  6 col 3
               Low "2. Drawing Shaded Boxes with Colors " Line  7 col 3
               Low "3. Drawing a Box Around Text        " Line  8 col 3
               Low "4. Drawing a Ruler                  " Line  9 col 3
               Low "5. Presetting the Printer Dialog Box" Line 10 col 3
               Low "6. Checking the Dialog Return Code  " Line 11 col 3
               Low "7. Drawing a Bitmap                 " Line 12 col 3
               Low "8. Changing a Font While Printing   " Line 13 col 3

               Low " 9. Multiple Text on Same Line      " Line  6 col 43
               Low "10. Changing Orientation and Pitch  " Line  7 col 43
               Low "11. Multiple Printers               " Line  8 col 43
               Low "12. Printing Text Top Center of Page" Line  9 col 43
               Low "13. Printing Text Corners of Page   " Line 10 col 43
               Low "14. Setting the Point Size for Font " Line 11 col 43
               Low "15. Setting Text Position           " Line 12 col 43
               Low "16. Exit Program                    " Line 13 col 43
               Low "Enter Selection:  " Line 18 Column 24.

           Accept Ws-Accept Col 0 Prompt No Beep.
           Evaluate True
               When Example1
                   Perform Example1
               When Example2
                   Perform Example2
               When Example3
                   Perform Example3
               When Example4
                   Perform Example4
               When Example5
                   Perform Example5
               When Example6
                   Perform Example6
               When Example7
                   Perform Example7
               When Example8
                   Perform Example8
               When Example9
                   Perform Example9
               When Example10
                   Perform Example10
               When Example11
                   Perform Example11
               When Example12
                   Perform Example12
               When Example13
                   Perform Example13
               When Example14
                   Perform Example14
               When Example15
                   Perform Example15
               When Other
                   Continue
           End-Evaluate.
           CALL "P$EnableDialog".

       Example1. *> Printing a Watermark

           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
                "Example 1: Printing a Watermark".

           CALL "P$ClearFont".
           Move 48 To Point-Size.
           CALL "P$GetDeviceCapabilities" Using DC-LogicalPixelsYParam,
                                                DC-LogPixelsY.
           Compute LF-Height Rounded = (Point-Size * Dc-LogPixelsY)
                                       / 72.
           Move "Times New Roman" To LF-FaceName.
           CALL "P$SetFont" Using LF-HeightParam,     Lf-Height,
                                  LF-EscapementParam, 450.
           CALL "P$SetTextColor" Using ColorDarkGray.
           CALL "P$TextOut" USING  "Example", 5.08, 17.78,
                                   "Absolute",
                                   "Metric".
           CALL "P$ClearFont".
           CLOSE PFILE.
           CALL "P$ClearDialog".

       Example2. *> Drawing Shaded Boxes with color.

           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
                "Example 2:  Drawing Shaded Boxes with color".

           CALL "P$SetPen"  USING PenStyleSolid, 5.
           CALL "P$SetBoxShade" USING ColorBlack, 10.
           CALL "P$DrawBox" USING 2.00, 2.00, "Absolute", "Inches",
                                  1.00, 1.00, "Inches", WithShading.
           CALL "P$SetPen"  USING PenStyleDash, 1, ColorRed.
           CALL "P$DrawBox" USING 0, 0, "Relative", "Inches",
               1.50, 1.50, "Inches".
           CLOSE PFILE.
           CALL "P$ClearDialog".

       Example3. *> Drawing Box around Text.

           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
                "Example 3:  Drawing Box Around Text".


           CALL "P$GetTextMetrics" USING TextMetrics.
           CALL "P$SetPosition" USING .5, 5.50,
               "Absolute", "Inches".
           CALL "P$GetTextPosition" USING X-POS, Y-POS,
                                  "Top", "Device-Units".
           CALL "P$GetTextExtent" USING "AAaaGGggYYyyTTttH",
               WIDTH, HEIGHT, "Device-Units".
      *
           IF TM-ExternalLeading Equal Zero
               Subtract 2 from Y-POS
               Add 2 To TM-ExternalLeading.

           COMPUTE X-POS  = X-POS - (TM-AveCharWidth / 2).
           COMPUTE WIDTH  = WIDTH + TM-AveCharWidth.
           COMPUTE HEIGHT = TM-ExternalLeading + TM-Height.

           CALL "P$DrawBox" USING X-POS, Y-POS, "Absolute",
                "Device-Units", WIDTH, HEIGHT, "Device-Units".
           CALL "P$TextOut" USING "AAaaGGggYYyyTTttH",
               .5, 5.50, "Absolute", "Inches".

      * The following example does the same thing but lets the runtime
      * compute the size and position of the box as well as draw the box.

           CALL "P$TextOut" USING "AAaaGGggYYyyTTttH",
               .5, 6.00, "Absolute", "Inches", WithBox.

           CLOSE PFILE.
           CALL "P$ClearDialog".

       Example4. *> Drawing a Ruler in Centimeters.
           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
                "Example 4: Drawing a Ruler in Centimeters.".

           Move 0.0 To Ws-X.
           Move 3.0 To Ws-Y, WS-Y2.
           Move 5.0 To Ws-X2.
           Move 0   To Ws-Cent-Count.
           Call "P$SetDefaultUnits" Using UnitsAreMetric.

      * Drawing a Line 5 Centimeters Long.
           Call "P$DrawLine" Using Ws-X,  WS-Y,  "Absolute", "Metric",
                                   Ws-X2, WS-Y2, "Absolute", "Metric".
           Perform Varying Ws-X From 0 By 0.1 Until Ws-X > 5
               Evaluate  True
                 When Ws-Cent-Count = 5
                     Move 3.4 To Ws-Y
                 When Ws-Cent-Count = 10
                     Move 3.7 To Ws-Y
                 When Other
                     Move 3.2 To Ws-Y
               End-Evaluate
               CALL "P$MoveTo" USING Ws-X,  WS-Y
               CALL "P$LineTo" USING Ws-X,  WS-Y2
               If Ws-Cent-Count = 10
                   Move 1 to Ws-Cent-Count
               Else
                   Add 1 to Ws-Cent-Count
               End-If
           End-Perform.

           Call "P$SetDefaultUnits" Using UnitsAreInches.
      *
           WRITE FD-RECORD FROM "Example 4A: Drawing a Ruler in Inches/C
      -"entimeters." After 6.

      *
      *
           CALL "P$GetDeviceCapabilities" Using DC-LogicalPixelsYParam,
                                                DC-LogPixelsY.
           CALL "P$ClearFont".
      * Compute Font Height for desired Point-Size.
           Compute LF-Height Rounded = (16 * Dc-LogPixelsY)  / 72.
           CALL "P$SetFont" Using LF-HeightParam LF-Height,
                                  LF-WeightParam LF-WeightBold.

           Call "P$TextOut" Using "RM/COBOL Ruler" 3, 3.633, "A", "I".
           CALL "P$ClearFont".
           Perform Para-Inches.
           Perform Para-Centimeters.

           CLOSE PFILE.
           CALL "P$ClearDialog".

       Para-Inches.
           Move 0.0 To Ws-X.
           Move 3.0 TO Ws-Y WS-Y2.
           Move Ws-Size-Inches TO Ws-X2.
           Call "P$DrawLine" USING Ws-X,  Ws-Y,  "A", "I",
                                   Ws-X2, Ws-Y2, "A", "I".
           Move 0.0 To Ws-X2.
           CALL "P$SetPosition" Using Ws-X,  Ws-Y,  "A", "I".
           Compute LF-Height Rounded = (12 * Dc-LogPixelsY)  / 72.
           CALL "P$SetFont" Using LF-HeightParam, LF-Height.
           Add One-Inch-Line To Ws-Y  Giving WS-Y2.
           Call "P$DrawLine" Using Ws-X,  Ws-Y,  "A", "I",
                                   Ws-X,  Ws-Y2, "A", "I".
           ADD Sixteenth-Inch to Ws-X.
           Set IDX1 To 1.
           Move Zero to Ws-count.
           Perform Varying Ws-X From Ws-X By Sixteenth-Inch Until
              Ws-X > WS-Size-Inches
                   Add Ws-Line-Length(idx1) To Ws-Y GIVING Ws-Y2
                   Call "P$DrawLine" Using Ws-X,  Ws-Y,  "A", "I",
                                           Ws-X,  Ws-Y2, "A", "I"
                   If IDX1 = 16
                       Add 1 To Ws-Count
                       Subtract Sixteenth-Inch From Ws-Y2
                       Compute Ws-N = Ws-X - (3 * Sixteenth-Inch)
                       Call "P$TextOut" Using Ws-Count-Out Ws-N, Ws-Y2
                       Set IDX1 To 1
                   Else
                       Set IDX1 Up By 1
                   End-If
           End-Perform.
           Compute LF-Height Rounded = (10 * Dc-LogPixelsY)  / 72.
           CALL "P$SetFont" Using LF-HeightParam, LF-Height.
           Call "P$TextOut" Using "INCHES" 0.125, 3.5 "A", "I".

       Para-Centimeters.
           Move 0.0 to Ws-X.
           Move 11.0 TO Ws-Y Ws-Y2.
           Move Ws-Size-Centimeters To Ws-X2.
           Call "P$DrawLine" Using Ws-X,  Ws-Y,  "A", "M",
                                   Ws-X2, Ws-Y2, "A", "M".

           Move 10 To  Ws-Y2.
           Call "P$SetPosition" Using Ws-X, Ws-Y2,  "A", "M".
           Compute LF-Height Rounded = (10 * Dc-LogPixelsY)  / 72.
           Call "P$SetFont" Using LF-HeightParam      LF-Height,
                                  LF-EscapementParam, 1800.
           Call "P$TextOut" Using "CENTIMETERS" 18.8, 9.6, "A", "M".
           Move Zero to Ws-Cent-count.
           Compute WS-X = WS-Size-Centimeters - 1.
           Perform Varying ws-x from Ws-X By -1
              until Ws-X = Zero
               Add 1 to Ws-Cent-Count
               Compute Ws-X2 = Ws-x + (2 * One-Millimeter)
               If Ws-Cent-Count < 10
                   Call "P$TextOut" Using Ws-Cent-Count-OUT(2:1)
                                          Ws-X2, Ws-Y2, "A", "M"
               Else
                   Call "P$TextOut" Using Ws-Cent-Count-Out
                                           Ws-X2, Ws-Y2, "A", "M"
               End-If
           End-Perform.

           Move 0 to Ws-Cent-Count.
           Move 0.0 to Ws-X.
           Move 11.0 TO Ws-Y Ws-Y2.
           Call "P$SetPosition" Using Ws-X, Ws-Y2, "A", "M".
           Subtract One-Centimeter-Line From Ws-Y Giving Ws-Y2.
           Call "P$DrawLine" Using Ws-X,  WS-Y,  "A", "M",
                                   Ws-X,  WS-Y2, "A", "M".
           Add One-Millimeter To Ws-x.
           Perform Varying Ws-X From Ws-X By One-Millimeter Until
              Ws-X > Ws-Size-Centimeters
                 Add 1 TO Ws-Cent-Count
                 Evaluate  True
                   When Ws-Cent-Count = 5
                       Subtract Five-Millimeter-Line from Ws-Y
                           Giving Ws-Y2
                   When Ws-Cent-Count = 10
                       Subtract One-Centimeter-Line from Ws-Y
                           Giving Ws-Y2
                   When Other
                       Subtract One-Millimeter-Line from Ws-Y
                           Giving Ws-Y2
                   End-Evaluate
                   CALL "P$DRAWLINE" USING Ws-X,  WS-Y,  "A", "M",
                                           Ws-X,  WS-Y2, "A", "M"
                   If Ws-Cent-Count = 10
                      Move 0 to Ws-Cent-Count
                   End-If
           End-Perform.

       Example5. *> Presetting the Printer Dialog Box.

           INITIALIZE PrintDialog.
           CALL "P$ClearDialog".
           SET PD-AllPagesFlag        TO TRUE.  *> Sets All Pages option on
           SET PD-NoPageNumbersFlag   TO TRUE.  *> Disable Pages option button
                                                *> and associated edit controls
           SET PD-NoSelectionFlag     TO TRUE.  *> Disables Selection button
           SET PD-CollateFlag         TO FALSE. *> Don't check Collate check box
           SET PD-HidePrintToFileFlag TO TRUE.  *> Hides PrintToFile checkbox

           SET  DM-CopiesField           TO TRUE.
           MOVE 2                        TO DM-Copies.
           SET DM-OrientationField       TO TRUE.
           SET DM-OrientationIsLandscape TO TRUE.
           SET DM-PrintQualityField      TO TRUE.
           SET DM-ResolutionIsHigh       TO TRUE.

           CALL "P$SetDialog" USING PrintDialog.
           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
                "Example 5:  Presetting the Printer Dialog Box Page 1"
                 AFTER PAGE.
           WRITE FD-RECORD FROM
                "Example 5:  Text After CALLING P$SETDIALOG  ".
           WRITE FD-RECORD FROM
                "Example 5:  This should be printed landscape mode.".
           WRITE FD-RECORD FROM
                "Example 5:  There should be two copies printed.".
           CLOSE PFILE.
           CALL "P$ClearDialog".

       Example6.  *> Checking the Return Code After Displaying
                  *> the Print Dialog Box
           Display Space Erase.
           Display "Example 6:  Print Dialog Return Code"  Line 2 Col 24.
           Display " Modify Option 1 for your Printer"     Line 3 Col 24.
           Initialize PrintDialog.
           Display "1.  Default Printer      "  Line 5 Col 27.
           Display "2.  Invalid Printer XYZZY " Line 6 Col 27.
           Display "Enter Printer Device: "     Line 8 Col 27.
           Accept Ws-Ex6 Line 8 col 0 Prompt No Beep.
           Evaluate True
             When Valid-Printer
               Move Spaces                     To DM-DeviceName
             When InValid-Printer
               Move "Printer XYZZY Series II"  To DM-DeviceName
             When Other
               Go to Example6
           End-Evaluate.

           Set   PD-NoWarningFlag       To True. *> Disable Windows Error Box
           CALL "P$SetDialog" Using DM-DeviceNameParam,
                                    DM-DeviceName,
                                    PD-NoWarningFlagParam,
                                    PD-NoWarningFlagValue.

           CALL "P$DisplayDialog" Giving Dialog-Return.

           Evaluate True
             When Dialog-OK                               *> Value Zero
               Display "OK Button Pressed"  Line 13 Col 1
               Perform Open-Printer-Para
             When Dialog-Cancel                           *> Value One
               Display "Cancel Button Pressed"  Line 13 Col 1
               Perform Printer-Canceled-Para
             When Dialog-Error                            *> Value Two
               Display "Error in Dialog"  Line 13 Col 1
               Perform Printer-Error-Para
             When Other                                   *> Value Other
               Display "Invalid Value: " Line 13 Col 1 Dialog-Return
               Accept Ws-Ex6
           End-Evaluate.

           Display "Try it again? (Y/N): " Line 20 Col 1.
           Accept Ws-YN Col 0 No Beep Control "UPPER".
           If Ws-Yes
               Go To Example6.

       Open-Printer-Para.
           Display "Printer Dialog OK." Line 22 Col 3 Reverse Erase Eol.
           Accept Ws-Ex6 no beep.
           Open Output Pfile.
           Write Fd-Record From
               "Example 6:  Checking the Printer Dialog Box" After Page.
           Close Pfile.

       Printer-Canceled-Para.
           Display "Printer Dialog Canceled" Line 23 Col 3 Reverse
               Erase Eol.
           Accept Ws-Ex6 no beep.

       Printer-Error-Para.
           CALL "P$GetDialog" Using PD-ExtendedErrorParam,
                                    PD-ExtendedErrorValue,
                                    Validity-Flag.

           If Validity-Flag Equal Zero  *> Check the Validty Flag
               Display "Call Returned Zero Value!" Line 21 Col 3 Reverse
               Validity-Flag.

           If PD-ExtErrIsPrinterNotFound
               Display "Printer Not Found! " Line 22 Col 3 Reverse
                   Erase Eol
               Display "Printer Error:     " Line 22 Col 23 Reverse
                   PD-ExtendedErrorValue Convert Erase Eol
           Else
               Display "Printer Dialog Had Error" Line 22 Col 3 Reverse
                   PD-ExtendedErrorValue Convert Erase Eol
           End-If.
           Accept Ws-Ex6 No Beep.


       Example7.  *> Drawing a Bitmap

           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
                "Example 7:  Drawing a Bitmap".
           CALL "P$DrawBitmap" USING "rmlogo.bmp", 2, 2, "Absolute",
                                     "Inches", 2, 0, "Inches".
           CLOSE PFILE.
           CALL "P$ClearDialog".

       Example8. *> Changing a Font While Printing.

           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
                "Example 8:  Changing a Font While Printing ".

           CALL "P$TextOut" USING "original FONT", 0.5, 3.50,
                                  "Absolute", "Inches".
           CALL "P$ClearFont".
           CALL "P$SetFont" USING LF-FaceNameParam, "Monotype Corsiva",
                                  LF-ItalicParam, "Y".
           CALL "P$TextOut" USING "Monotype Corsiva FONT Italic",
                                  0.5, 4.50, "Absolute", "Inches".

           CALL "P$ClearFont".
           CLOSE PFILE.
           CALL "P$ClearDialog".

       Example9. *>  Printing Multiple Outputs on the Same Line

           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
               "Example 9:  Printing Multiple Outputs on the Same Line".
           WRITE FD-RECORD FROM SPACES AFTER 2.
           CALL "P$GetDeviceCapabilities" Using DC-LogicalPixelsYParam,
                                                DC-LogPixelsY.
           Move 10 To Point-Size.

           CALL "P$ClearFont".
           INITIALIZE LogicalFont.

           Compute LF-Height Rounded = (Point-Size * Dc-LogPixelsY)
                                       / 72.

           CALL "P$SetFont" USING LogicalFont.

           WRITE FD-RECORD FROM "Printing a word in".
           CALL "P$SetLineExtendMode" USING 1, "Characters". *> one character

           CALL "P$SetFont" USING LF-ItalicParam, "Y".
           WRITE FD-RECORD FROM "Italic," AFTER ADVANCING ZERO.
           CALL "P$SetLineExtendMode" USING 1, "Characters". *> one character

           CALL "P$SetFont" USING LF-ItalicParam,    "N",
                                  LF-UnderlineParam, "Y".
           WRITE FD-RECORD FROM "Underline," AFTER ADVANCING ZERO.
           CALL "P$SetLineExtendMode" USING 1, "Characters". *> one character

           CALL "P$SetFont" USING LF-WeightParam, LF-WeightBold,
                                  LF-UnderlineParam, "N".
           WRITE FD-RECORD FROM "or Bold" AFTER ADVANCING ZERO.
           CALL "P$SetLineExtendMode" USING 1, "C". *> one character
           CALL "P$SetFont" USING LF-WeightParam, LF-WeightNormal.
           WRITE FD-RECORD FROM
               "on the same line is no problem with RM/COBOL."
                AFTER ADVANCING ZERO.

           CALL "P$ClearFont".
           CLOSE PFILE.
           CALL "P$ClearDialog".

       Example10. *> Changing Orientation and Pitch

           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
               "Example 10: Changing Orientation and Pitch".

           CALL "P$TextOut" USING
               "This is Orientation Portrait Normal Pitch"
               0, 1.50, "Absolute", "Inches".

           CALL "P$SetPitch" USING PitchCompressed.

           CALL "P$ChangeDeviceModes" USING DM-OrientationParam,
                                            DM-OrientationLandscape.
           WRITE FD-RECORD FROM SPACES AFTER PAGE.
           CALL "P$TextOut" USING
              "This is printed with the Orientation Landscape with a Com
      -"pressed Type Size", 0, 1.50, "Absolute", "Inches".
           CALL "P$SetPitch" USING PitchNormal.

           CALL "P$ChangeDeviceModes" USING DM-OrientationParam,
                                            DM-OrientationPortrait.
           WRITE FD-RECORD FROM SPACES AFTER PAGE.
           CALL "P$TextOut" USING
               "This is back to Portrait using the Normal Pitch Size",
               0, 1.50, "Absolute", "Inches".
           CLOSE PFILE.
           CALL "P$ClearDialog".

       Example11. *> Opening and Writing to Separate Printers

           OPEN OUTPUT PFILE1.
           CALL "P$GetHandle" USING WS-Handle1.
           CALL "P$EnableDialog".

           OPEN OUTPUT PFILE2.
           CALL "P$GetHandle" USING WS-Handle2.
           CALL "P$EnableDialog".

           OPEN OUTPUT PFILE3.
           CALL "P$GetHandle" USING Ws-Handle3.

           CALL "P$SetHandle" USING Ws-Handle3.
           CALL "P$TextOut" USING
               "Example 11: Text out written to PRINTER 3".

           CALL "P$SetHandle" USING Ws-Handle2.
           CALL "P$TextOut" USING
               "Example 11: Text out written to PRINTER 2".

           CALL "P$SetHandle" USING Ws-Handle1.
           CALL "P$TextOut" USING
               "Example 11: Text out written to PRINTER 1".

           CLOSE PFILE1, PFILE2, PFILE3.
           CALL "P$ClearDialog".

       Example12. *> Printing Text at Top Center of Page

           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
              "Example 12: Printing Text at Top Center of Page" AFTER 3.


           CALL "P$GetDeviceCapabilities" Using DC-HorizontalSizeParam,
                                                DC-HorzSize.
           CALL "P$GetTextExtent" Using "Top Center",
                                         Ws-Text-Width,
                                         Ws-Text-Height,
                                         UnitsAreMetric.
      * Divide by 10 to convert millimeters to centimeters.
           Compute Ws-X-M = ((DC-HorzSize / 10) / 2) -
                            (Ws-Text-Width / 2).
           Move Zero to Ws-Y-M.
           CALL "P$SetTextPosition" USING Ws-X-M, Ws-Y-M,
                                          PositionIsTop,
                                          ModeIsAbsolute,
                                          UnitsAreMetric.
           CALL "P$TextOut" Using "Top Center".
           CLOSE PFILE.
           CALL "P$ClearDialog".


       Example13. *> Printing Text at Corners of Page
           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
              "Example 13: Printing Text at Corners of Page" AFTER 3.

      * Position Text Top Left.
           CALL "P$SetTextPosition" Using 0, 0, PositionIsTop,
                                                ModeIsAbsolute,
                                                UnitsAreDeviceUnits.
           CALL "P$TextOut" Using "Top Left".

      * Position Text Bottom Right.
           CALL "P$GetTextExtent" Using "Bottom Right",
                                         Ws-Text-Width,
                                         Ws-Text-Height,
                                         UnitsAreMetric.

           CALL "P$GetDeviceCapabilities" Using DC-HorizontalSizeParam,
                                                DC-HorzSize,
                                                DC-VerticalSizeParam,
                                                DC-VertSize.

      * Divide by 10 to convert millimeters to centimeters.
           Compute Ws-X-M = (DC-HorzSize / 10) - Ws-Text-Width.
           Compute Ws-Y-M = (DC-VertSize / 10).
           CALL "P$SetTextPosition" Using Ws-X-M, Ws-Y-M,
                                          PositionIsBottom,
                                          ModeIsAbsolute,
                                          UnitsAreMetric.

           CALL "P$TextOut" Using "Bottom Right".

           CLOSE PFILE.
           CALL "P$ClearDialog".

       Example14. *> Setting the Point Size for a Font
           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
              "Example 14: Setting the Point Size for a Font".
           WRITE FD-RECORD FROM
              "Example 14: Times New Roman font Point Size set to 72".

           CALL "P$ClearFont".
           Initialize LogicalFont.

           Move 72 To Point-Size.

           CALL "P$GetDeviceCapabilities" Using DC-LogicalPixelsYParam,
                                                DC-LogPixelsY.
      * Compute Font Height for desired Point-Size.
      * If Point-Size is 72, this produces a 1 inch high font
      * including the internal leading.

           Compute LF-Height Rounded = (Point-Size * Dc-LogPixelsY)
                                       / 72.

           Move "Times New Roman" To LF-FaceName.
           CALL "P$SetFont" Using    Lf-FaceNameParam,  Lf-FaceName,
                                     Lf-HeightParam,    Lf-Height.
           CALL "P$GetTextMetrics" Using TextMetrics.

      * Add bias for Internal Leading.
      * If Point-Size is 72, this produces a 1 inch high font
      * excluding the internal leading.

           Compute LF-Height = (LF-Height * TM-Height) /
                               (TM-Height - TM-InternalLeading).

      * Set Font with computed Lf-Height
           CALL "P$SetFont" Using Lf-HeightParam, Lf-Height.

           Write FD-Record From "Greetings" After Advancing 1 Line.

           CALL "P$ClearFont".
           CLOSE PFILE.
           CALL "P$ClearDialog".

       Example15. *> Setting Text Position
           OPEN OUTPUT PFILE.
           WRITE FD-RECORD FROM
              "Example 15:  Setting Text Position".
           CALL "P$ClearFont".

      * Compute font height for point size 72
           Move 72 To Point-Size.
           CALL "P$GetDeviceCapabilities" Using DC-LogicalPixelsYParam,
                                                DC-LogPixelsY.
           Compute LF-Height Rounded = (Point-Size * Dc-LogPixelsY)
                                       / 72.
           Move "Times New Roman" To LF-FaceName.
           CALL "P$SetFont" Using    Lf-FaceNameParam,  Lf-FaceName,
                                     Lf-HeightParam,    Lf-Height.
           CALL "P$GetTextMetrics" Using TextMetrics.
      * Add bias for Internal Leading.
           Compute LF-Height = (LF-Height * TM-Height) /
                               (TM-Height - TM-InternalLeading).

      * Set Font with computed Lf-Height
           CALL "P$SetFont" Using Lf-HeightParam, Lf-Height.

      * Print initial Greetings in a 72 Point Font Size.

           CALL "P$TextOut"         Using "Greetings", 0.25, 2,
                                          ModeIsAbsolute,
                                          UnitsAreInches.
      * Get the Top Text Position.
           CALL "P$GetTextPosition" Using Ws-X-D, Ws-Top,
                                          PositionIsTop,
                                          UnitsAreDeviceUnits.
      * Get the Bottom Text Position.
           CALL "P$GetTextPosition" Using Ws-XB-D, Ws-Bottom,
                                          PositionIsBottom,
                                          UnitsAreDeviceUnits.
      * Get the Base Line Position.
           CALL "P$GetPosition"     Using Ws-XP-D, Ws-Base,
                                          UnitsAreDeviceUnits.

           Move 10 To Point-Size.
           Compute LF-Height Rounded = (Point-Size * Dc-LogPixelsY)
                                       / 72.
           CALL "P$SetFont"         Using LF-HeightParam, LF-Height.

      * Set the Top Text Position and print, get next X Position.
           CALL "P$SetTextPosition" Using Ws-X-D, Ws-Top,
                                          PositionIsTop,
                                          ModeIsAbsolute,
                                          UnitsAreDeviceUnits.

           CALL "P$TextOut"         Using "Greetings".
           CALL "P$GetPosition"     Using Ws-X-D, Ws-Dummy,
                                          UnitsAreDeviceUnits.

      * Set the Base Line Position and print, get next X Position.
           CALL "P$SetPosition"     Using Ws-X-D, Ws-Base,
                                          ModeIsAbsolute,
                                          UnitsAreDeviceUnits.
           CALL "P$TextOut"         Using "Greetings".
           CALL "P$GetPosition"     Using Ws-X-D, Ws-Dummy,
                                          UnitsAreDeviceUnits.

      * Set the Bottom Position and print.
           CALL "P$SetTextPosition" Using Ws-X-D, Ws-Bottom,
                                          PositionIsBottom,
                                          ModeIsAbsolute,
                                          UnitsAreDeviceUnits.
           CALL "P$TextOut"         Using "Greetings".

           CALL "P$ClearFont".
           CLOSE PFILE.
           CALL "P$ClearDialog".
       END PROGRAM PEXAMPLE.
