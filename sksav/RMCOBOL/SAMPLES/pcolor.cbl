       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PCOLOR.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION:
      *
      * PCOLOR -  Test CALLs to P$SetColor and P$TextOut
      *              This program will print out text in various colors.
      *
      * Version Identification:
      *   $Revision:   1.1.1.2  $
      *   $Date:   27 Sep 1999 14:47:08  $
      *
      *********************************************************************

       DATE-WRITTEN.     01/07/1999.
       REMARKS.     This program will print out text in various colors.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PRINTER ASSIGN TO PRINT, "PRINTER?".
       DATA DIVISION.
       FILE SECTION.
       FD  PRINTER.
       01  PRINT-RECORD                PIC X(80).
       WORKING-STORAGE SECTION.
       77  WS-SUB                      PIC 99 VALUE 0.

       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(15) VALUE SPACES.
           05  FILLER                  PIC X(31) VALUE
           "RUNTIME RESULTS - PCOLOR   ".

      *  Modify the following output line to describe the test.
       01  DESCRIP-LINE.
           05  FILLER                  PIC X(24)  VALUE
           "**   PCOLOR  - Test For ".
           05  FILLER                  PIC X(29) VALUE
           "Printing Text With Colors  **".
      *     1234567890123456789012345678901234567890

       77  DATE-CONVERT                PIC 9(5)V9(4) VALUE 10000.0001.
       78  DATE-COMP                   VALUE IS DATE-COMPILED.
       01  DOW-SUB                    PIC 9(1)  VALUE ZERO.
       01  DAY-OF-WEEK-TAB            PIC X(21) VALUE
           "MonTueWedThuFriSatSun".
       01  DAY-OF-WEEK-RED REDEFINES DAY-OF-WEEK-TAB.
           05  DAY-OF-THE-WEEK        PIC X(3) OCCURS 7 TIMES.

       01  TIME-DATE-LINE.
           05  FILLER                 PIC X(6)  VALUE "Date: ".
           05  MM                     PIC XX.
           05  SLASH-1                PIC X     VALUE "/".
           05  DD                     PIC XX.
           05  SLASH-2                PIC X     VALUE "/".
           05  YY                     PIC X(5).
           05  FILLER                 PIC X     VALUE "(".
           05  DOW                    PIC X(3).
           05  FILLER                 PIC X     VALUE ")".
           05  FILLER                 PIC X(13) VALUE "   Run Time: ".
           05  HH                     PIC XX.
           05  DIV-1                  PIC X     VALUE ":".
           05  MN                     PIC XX.
           05  DIV-2                  PIC X     VALUE ":".
           05  SS                     PIC XX.
           05  FILLER                 PIC X(5).
           05  FILLER                 PIC X(15) VALUE "Date Compiled: ".
           05  DATE-COMPILE           PIC 99/99/9999.

       COPY "rminfo.cpy".
       COPY "sysinfo.cpy".
       COPY "printinf.cpy".

       01  RM-LINE.
           05  RM-USER                 PIC X(9)  VALUE "User:    ".
           05  RM-SUB-LINE.
               10  FILLER              PIC X(16) VALUE
               " using RM/COBOL ".
               10  RM-LINE-VersionNumber
                                       PIC X(8).
               10  FILLER              PIC X(1)  VALUE "(".
               10  RM-Line-RegistrationNumber PIC X(18).
               10  FILLER              PIC X(1)  VALUE ")".
               10  FILLER              PIC X(12) VALUE SPACES.

       01  PRT-LINE.
           05  FILLER                 PIC X(9)  VALUE "Printer: ".
           05  Prt-Line-PrinterName   PIC X(71).


       01  UNDER-LINE.
           05  FILLER                  PIC X(80) VALUE ALL "-".

       01 WS-ARGS.
           03 WS-X                     PIC 9(7)V9(2) VALUE ZERO.
           03 WS-Y                     PIC 9(7)V9(2) VALUE ZERO.

       01  WS-TABLE.
           03              PIC X(21)  VALUE "Black       000000000".
           03              PIC X(21)  VALUE "Dark Blue   000000127".
           03              PIC X(21)  VALUE "Dark Green  000127000".
           03              PIC X(21)  VALUE "Dark Cyan   000127127".
           03              PIC X(21)  VALUE "Dark Red    127000000".
           03              PIC X(21)  VALUE "Dark Magenta127000127".
           03              PIC X(21)  VALUE "Brown       127127000".
           03              PIC X(21)  VALUE "Dark Gray   085085085".
           03              PIC X(21)  VALUE "Light Gray  192192192".
           03              PIC X(21)  VALUE "Blue        000000255".
           03              PIC X(21)  VALUE "Green       000255000".
           03              PIC X(21)  VALUE "Cyan        000255255".
           03              PIC X(21)  VALUE "Red         255000000".
           03              PIC X(21)  VALUE "Magenta     255000255".
           03              PIC X(21)  VALUE "Yellow      255255000".
           03              PIC X(21)  VALUE "White       255255255".
       01  WS-TAB REDEFINES WS-TABLE.
           03 WS-COLOR-TAB  OCCURS 16 TIMES.
              05  WS-COLOR PIC X(12).
              05  WS-RED   PIC 9(3).
              05  WS-GREEN PIC 9(3).
              05  WS-BLUE  PIC 9(3).

       COPY "windefs.cpy".
       01  WS-DEF-TABLE.
           03                  PIC X(12) Value ColorBlack.
           03                  PIC X(12) Value ColorDarkBlue.
           03                  PIC X(12) Value ColorDarkGreen.
           03                  PIC X(12) Value ColorDarkCyan.
           03                  PIC X(12) Value ColorDarkRed.
           03                  PIC X(12) Value ColorDarkMagenta.
           03                  PIC X(12) Value ColorBrown.
           03                  PIC X(12) Value ColorDarkGray.
           03                  PIC X(12) Value ColorLightGray.
           03                  PIC X(12) Value ColorBlue.
           03                  PIC X(12) Value ColorGreen.
           03                  PIC X(12) Value ColorCyan.
           03                  PIC X(12) Value ColorRed.
           03                  PIC X(12) Value ColorMagenta.
           03                  PIC X(12) Value ColorYellow.
           03                  PIC X(12) Value ColorWhite.
       01  WS-DEF-TAB REDEFINES WS-DEF-TABLE.
           03 WS-COLOR-DEF-TAB  OCCURS 16 TIMES.
              05  WS-COLOR-DEF PIC X(12).

       01 WS-LINE-OUT.
           03 FILLER           PIC X(17) VALUE "Color Print Test ".
           03 RGB-Group.
              05 WS-COLOR-OUT  PIC X(12).
              05 RGB-TEXT      PIC X(20).
              05 WS-RED-OUT    PIC ZZ9.
              05 FILLER        PIC X.
              05 WS-GREEN-OUT  PIC ZZ9.
              05 FILLER        PIC X.
              05 WS-BLUE-OUT   PIC ZZ9.
              05 FILLER        PIC X(6).
           03 COLORS-Value Redefines RGB-Group.
              05 FILLER        PIC X(1).
              05 COLORS-OUT    PIC X(12).
              05 COLORS-DEFS   PIC X(18).
              05 FILLER        PIC X(18).
           03 FILLER           PIC X(1).

       PROCEDURE DIVISION.
       MAIN-START SECTION.
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 2 LINES.
           CLOSE PRINTER.
           STOP RUN.

      ** Prepare heading information **
       INITIAL-SETUP.
           OPEN OUTPUT PRINTER.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           ACCEPT DOW-SUB       FROM DAY-OF-WEEK.
           MOVE DAY-OF-THE-WEEK(DOW-SUB)   TO DOW.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.
           COMPUTE DATE-COMPILE = DATE-COMP * DATE-CONVERT.

      ** Print heading **
       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1      AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2      AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE   AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "C$SetDevelopmentMode".
           CALL "C$GetRMInfo"      USING RMInformation.
           CALL "C$GetSysInfo"     USING SystemInformation.
           CALL "P$GetPrinterInfo" USING PrinterInformation.

           MOVE  RM-VersionNumber      TO RM-Line-VersionNumber.
           MOVE  RM-RegistrationNumber TO RM-Line-RegistratioNnumber.
           MOVE  PI-PrinterName        TO Prt-Line-PrinterName.

           STRING RM-USER       Delimited by Size,
                  Sys-UserName  Delimited by Spaces,
                  RM-SUB-LINE   Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           STRING "Machine: "   Delimited by Size,
                  Sys-NodeName  Delimited by Spaces,
                  " running "   Delimited by Size,
                  Sys-Name      Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM PRT-LINE   AFTER 1 LINES.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

       MAIN SECTION.
       TEST-1.

      * Using Three Color Combination

           WRITE PRINT-RECORD FROM
                "Setting colors using three color combination " after 2.
           WRITE PRINT-RECORD FROM SPACES AFTER 2.
           MOVE 1 TO WS-X.
           MOVE 7 TO WS-Y.
           MOVE " RGB Values =    " TO RGB-TEXT.
           PERFORM VARYING WS-SUB FROM 1 BY 1 UNTIL WS-SUB > 16
               CALL "P$SetTextColor"  USING WS-RED(WS-SUB),
                                            WS-GREEN(WS-SUB),
                                            WS-BLUE(WS-SUB)
               MOVE WS-RED(WS-SUB)   TO WS-RED-OUT
               MOVE WS-GREEN(WS-SUB) TO WS-GREEN-OUT
               MOVE WS-BLUE(WS-SUB)  TO WS-BLUE-OUT
               MOVE WS-COLOR(WS-SUB) TO WS-COLOR-OUT
               CALL "P$TextOut" USING  WS-LINE-OUT, WS-X, WS-Y,
                                       ModeisAbsolute,
                                       UnitsareMetric,
                                       TextOutWithBox,
                                       TextOutWithOutShading
               ADD 1 TO WS-Y
           END-PERFORM.
           CALL "P$SetTextColor"  USING  0, 0, 0.

      * Using Windefs Colors

           WRITE PRINT-RECORD FROM SPACES AFTER PAGE.
           WRITE PRINT-RECORD FROM
                "Setting colors using Windefs Colors " after 2.
           WRITE PRINT-RECORD FROM SPACES AFTER 2.
           MOVE 1 TO WS-X.
           MOVE 7 TO WS-Y.
           MOVE SPACES TO COLORS-VALUE.
           MOVE " using windefs.cpy" TO COLORS-DEFS.
           PERFORM VARYING WS-SUB FROM 1 BY 1 UNTIL WS-SUB > 16
               CALL "P$SettextColor"  USING WS-COLOR-DEF(WS-SUB),
               MOVE WS-COLOR-DEF(WS-SUB) TO COLORS-OUT
               CALL "P$TextOut" USING  WS-LINE-OUT, WS-X, WS-Y,
                                       ModeisAbsolute,
                                       UnitsareMetric,
                                       TextOutWithBox,
                                       TextOutWithOutShading
               ADD 1 TO WS-Y
           END-PERFORM.

      * Reset the color
           CALL "P$SetTextColor"  USING  0, 0, 0.

       END PROGRAM  PCOLOR.
