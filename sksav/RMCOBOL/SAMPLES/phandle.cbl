       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PHANDLE.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION:
      *
      * PDRAW -  Test P$SETHANDLE, P$GETHANDLE, P$SETPOSITION.
      *
      * Version Identification:
      *   $Revision:   1.1.1.1  $
      *   $Date:   27 Sep 1999 14:54:00  $
      *
      *********************************************************************

       DATE-WRITTEN.     01/25/1999

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PFILE1 ASSIGN TO PRINT, "PRINTER?".
           SELECT PFILE2 ASSIGN TO PRINT, "PRINTER?".
           SELECT PFILE3 ASSIGN TO PRINT, "PRINTER?".
       DATA DIVISION.
       FILE SECTION.
       FD  PFILE1.
       01  FD-RECORD1       PIC X(80).
       FD  PFILE2.
       01  FD-RECORD2       PIC X(80).
       FD  PFILE3.
       01  FD-RECORD3       PIC X(80).
       WORKING-STORAGE SECTION.
       77  WS-SUB                    PIC 9(3)  VALUE 0.
       01  WS-FIELDS.
           03  YY                    PIC S9(3) VALUE 0.
           03  XX                    PIC S9(3) VALUE 0.
           03  HANDLE                PIC S9(10) OCCURS 3 TIMES.

       PROCEDURE DIVISION.
       MAIN-START.

           CALL "C$SETDEVELOPMENTMODE".
           CALL "P$DISPLAYDIALOG".
           OPEN OUTPUT PFILE1.
           ADD 1 TO WS-SUB.
           CALL "P$GETHANDLE" USING HANDLE(WS-SUB).
           CALL "P$ENABLEDIALOG".
           OPEN OUTPUT PFILE2.
           ADD 1 TO WS-SUB.
           CALL "P$GETHANDLE" USING HANDLE(WS-SUB).
           CALL "P$ENABLEDIALOG".
           OPEN OUTPUT PFILE3.
           ADD 1 TO WS-SUB.
           CALL "P$GETHANDLE" USING HANDLE(WS-SUB).

           WRITE  FD-RECORD1 FROM "RM/COBOL Printer Test " AFTER 1.
           WRITE  FD-RECORD1 FROM "Using Version 7.0     " AFTER 1.
           WRITE  FD-RECORD1 FROM "Using Printer 1       " AFTER 1.

           WRITE  FD-RECORD2 FROM "RM/COBOL Printer Test " AFTER 1.
           WRITE  FD-RECORD2 FROM "Using Version 7.0     " AFTER 1.
           WRITE  FD-RECORD2 FROM "Using Printer 2       " AFTER 1.

           WRITE  FD-RECORD3 FROM "RM/COBOL Printer Test " AFTER 1.
           WRITE  FD-RECORD3 FROM "Using Version 7.0     " AFTER 1.
           WRITE  FD-RECORD3 FROM "Using Printer 3       " AFTER 1.

           CALL "P$SETHANDLE" USING HANDLE(WS-SUB).
           CALL "P$SETPOSITION" USING 1, 5, "R", "Inches".
           CALL "P$TEXTOUT" USING "Text written to PRINTER 3".
           SUBTRACT 1 FROM WS-SUB.
           CALL "P$SETHANDLE" USING HANDLE(WS-SUB).
           CALL "P$SETPOSITION" USING 1, 5, "R", "Inches".
           CALL "P$TEXTOUT" USING "Text written to PRINTER 2".
           SUBTRACT 1 FROM WS-SUB.
           CALL "P$SETHANDLE" USING HANDLE(WS-SUB).
           CALL "P$SETPOSITION" USING 1, 5, "R", "Inches".
           CALL "P$TEXTOUT" USING "Text written to PRINTER 1".

           CLOSE PFILE1, PFILE2, PFILE3.

           STOP RUN.

