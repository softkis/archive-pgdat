       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PRULER.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION: Prints a Ruler in Inches and Centimeters
      *
      * PRULER - This program uses the routines P$DrawLine, P$SetFont
      *                         P$ClearFont, P$SetPosition, P$TextOut
      *          to draw a Seven-Inch Ruler and a 19 Centimeter Ruler
      *
      * Version Identification:
      *   $Revision:   1.1.1.1  $
      *   $Date:   27 Sep 1999 14:59:50  $
      *
      *********************************************************************

       DATE-WRITTEN.     03/12/1999.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL-85.
       OBJECT-COMPUTER.  RMCOBOL-85.
       SPECIAL-NAMES.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT PRINTER ASSIGN TO PRINT, "PRINTER?".

       DATA DIVISION.
       FILE SECTION.

       FD  PRINTER.
       01  PRINT-RECORD                 PIC X(132).

       WORKING-STORAGE SECTION.
       78  WS-Size-Inches               Value  7.500.
       78  WS-Size-Centimeters          Value 19.000.

       78  One-Inch                     Value 1.0000.
       78  Half-Inch                    Value 0.5000.
       78  Qtr-Inch                     Value 0.2500.
       78  Eighth-Inch                  Value 0.1250.
       78  Sixteenth-Inch               Value 0.0625.

       78  One-Centimeter               Value 1.0000.
       78  One-Millimeter               Value 0.1000.

      * These are the lengths of the ruler marks
       78  One-Inch-Line                Value 0.3750.
       78  Half-Inch-Line               Value 0.3125.
       78  Qtr-Inch-Line                Value 0.2500.
       78  Eighth-Inch-Line             Value 0.1875.
       78  Sixteenth-Inch-Line          Value 0.0625.

       78  One-Centimeter-Line          Value 0.7000.
       78  Five-Millimeter-Line         Value 0.4000.
       78  One-Millimeter-Line          Value 0.2000.

       01  WS-VARS.
           03  Ws-X                     PIC 9(2)V9(4) Value Zero.
           03  Ws-Y                     PIC 9(2)V9(4) Value Zero.
           03  Ws-X2                    PIC 9(2)V9(4) Value Zero.
           03  Ws-Y2                    PIC 9(2)V9(4) Value Zero.
           03  Ws-N                     PIC 9(2)V9(4) Value Zero.
           03  Ws-Count                 PIC 9(1) Value 0.
           03  Ws-Count-Out Redefines WS-Count PIC X(1).
           03  Ws-Cent-Count            PIC 9(2) Value 0.
           03  Ws-Cent-Count-Out Redefines WS-Cent-Count PIC X(2).

       01  WS-TABLE.
           03                     PIC V9(4) Value Sixteenth-Inch-Line.
           03                     PIC V9(4) Value Eighth-Inch-Line.
           03                     PIC V9(4) Value Sixteenth-Inch-Line.
           03                     PIC V9(4) Value Qtr-Inch-Line.
           03                     PIC V9(4) Value Sixteenth-Inch-Line.
           03                     PIC V9(4) Value Eighth-Inch-Line.
           03                     PIC V9(4) Value Sixteenth-Inch-Line.
           03                     PIC V9(4) Value Half-Inch-Line.
           03                     PIC V9(4) Value Sixteenth-Inch-Line.
           03                     PIC V9(4) Value Eighth-Inch-Line.
           03                     PIC V9(4) Value Sixteenth-Inch-Line.
           03                     PIC V9(4) Value Qtr-Inch-Line.
           03                     PIC V9(4) Value Sixteenth-Inch-Line.
           03                     PIC V9(4) Value Eighth-Inch-Line.
           03                     PIC V9(4) Value Sixteenth-Inch-Line.
           03                     PIC V9(4) Value One-Inch-Line.
       01  WS-TABLE-RED REDEFINES WS-TABLE.
           03 WS-LINE-LENGTH      PIC V9(4) Occurs 16 Times
                                            Indexed By IDX1.
       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(15) VALUE SPACES.
           05  FILLER                  PIC X(31) VALUE
           "RUNTIME RESULTS - PRULER   ".

      *  Modify the following output line to describe the test.
       01  DESCRIP-LINE.
           05  FILLER                  PIC X(24)  VALUE
           "**   PRULER  - Test For ".
           05  FILLER                  PIC X(18) VALUE
           "Drawing Rulers  **".
      *     1234567890123456789012345678901234567890

       77  DATE-CONVERT                PIC 9(5)V9(4) VALUE 10000.0001.
       78  DATE-COMP                   VALUE IS DATE-COMPILED.
       01  DOW-SUB                    PIC 9(1)  VALUE ZERO.
       01  DAY-OF-WEEK-TAB            PIC X(21) VALUE
           "MonTueWedThuFriSatSun".
       01  DAY-OF-WEEK-RED REDEFINES DAY-OF-WEEK-TAB.
           05  DAY-OF-THE-WEEK        PIC X(3) OCCURS 7 TIMES.

       01  TIME-DATE-LINE.
           05  FILLER                 PIC X(6)  VALUE "Date: ".
           05  MM                     PIC XX.
           05  SLASH-1                PIC X     VALUE "/".
           05  DD                     PIC XX.
           05  SLASH-2                PIC X     VALUE "/".
           05  YY                     PIC X(5).
           05  FILLER                 PIC X     VALUE "(".
           05  DOW                    PIC X(3).
           05  FILLER                 PIC X     VALUE ")".
           05  FILLER                 PIC X(13) VALUE "   Run Time: ".
           05  HH                     PIC XX.
           05  DIV-1                  PIC X     VALUE ":".
           05  MN                     PIC XX.
           05  DIV-2                  PIC X     VALUE ":".
           05  SS                     PIC XX.
           05  FILLER                 PIC X(5).
           05  FILLER                 PIC X(15) VALUE "Date Compiled: ".
           05  DATE-COMPILE           PIC 99/99/9999.

       COPY "rminfo.cpy".
       COPY "sysinfo.cpy".
       COPY "printinf.cpy".

       01  RM-LINE.
           05  RM-USER                 PIC X(9)  VALUE "User:    ".
           05  RM-SUB-LINE.
               10  FILLER              PIC X(16) VALUE
               " using RM/COBOL ".
               10  RM-LINE-VersionNumber
                                       PIC X(8).
               10  FILLER              PIC X(1)  VALUE "(".
               10  RM-Line-RegistrationNumber PIC X(18).
               10  FILLER              PIC X(1)  VALUE ")".
               10  FILLER              PIC X(12) VALUE SPACES.

       01  PRT-LINE.
           05  FILLER                 PIC X(9)  VALUE "Printer: ".
           05  Prt-Line-PrinterName   PIC X(71).

       01  UNDER-LINE.
           05  FILLER                  PIC X(80) VALUE ALL "-".

       COPY "logfont.cpy".

       COPY "windefs.cpy".

       COPY "devcaps.cpy".
      *
        01 Save-Font.
           02  Save-Height                        Pic 9(5) Binary(2).
           02  Save-Width                         Pic 9(5) Binary(2).
           02  Save-Escapement                    Pic 9(5) Binary(2).
           02  Save-Orientation                   Pic 9(5) Binary(2).
           02  Save-WeightValue                   Pic 9(3) Binary(2).
           02  Save-ItalicValue                   Pic X.
           02  Save-UnderlineValue                Pic X.
           02  Save-StrikeoutValue                Pic X.
           02  Save-CharSetValue                  Pic 9(3) Binary(2).
           02  Save-OutPrecisValue                Pic 9 Binary(2).
           02  Save-ClipPrecisValue               Pic 9(3) Binary(2).
           02  Save-QualityValue                  Pic 9 Binary(2).
           02  Save-PitchValue                    Pic 9 Binary(2).
           02  Save-FamilyValue                   Pic 9 Binary(2).
           02  Save-FaceName                      Picture X(31).

       PROCEDURE DIVISION.
       DRIVER-SECTION SECTION.
      ** Main driver paragraph **
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 2 LINES.
           CLOSE PRINTER.
           STOP RUN.

      ** Prepare heading information **
       INITIAL-SETUP.
           OPEN OUTPUT PRINTER.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           ACCEPT DOW-SUB       FROM DAY-OF-WEEK.
           MOVE DAY-OF-THE-WEEK(DOW-SUB)   TO DOW.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.
           COMPUTE DATE-COMPILE = DATE-COMP * DATE-CONVERT.

      ** Print heading **
       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1      AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2      AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE   AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "C$SetDevelopmentMode".
           CALL "C$GetRMInfo"      USING RMInformation.
           CALL "C$GetSysInfo"     USING SystemInformation.
           CALL "P$GetPrinterInfo" USING PrinterInformation.

           MOVE  RM-VersionNumber      TO RM-Line-VersionNumber.
           MOVE  RM-RegistrationNumber TO RM-Line-RegistratioNnumber.
           MOVE  PI-PrinterName        TO Prt-Line-PrinterName.

           STRING RM-USER       Delimited by Size,
                  Sys-UserName  Delimited by Spaces,
                  RM-SUB-LINE   Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           STRING "Machine: "   Delimited by Size,
                  Sys-NodeName  Delimited by Spaces,
                  " running "   Delimited by Size,
                  Sys-Name      Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM PRT-LINE   AFTER 1 LINES.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

       MAIN SECTION.
       TEST-1.
      *
           CALL "P$GetFont" Using Save-Font.
           CALL "P$GetDeviceCapabilities" Using DC-LogicalPixelsYParam,
                                                DC-LogPixelsY.
      * Compute Font Height for desired Point-Size.
           Compute LF-Height Rounded = (16 * Dc-LogPixelsY)  / 72.
           CALL "P$SetFont" Using LF-HeightParam LF-Height,
                                  LF-WeightParam LF-WeightBold.

           Call "P$TextOut" Using "RM/COBOL Ruler" 3, 3.625,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$ClearFont".
           Perform Para-Inches.
           Perform Para-Centimeters.
           CALL "P$ClearFont".
           CALL "P$SetFont" Using Save-Font.
           MOVE SPACES TO PRINT-RECORD.
           WRITE PRINT-RECORD AFTER 3.
      *
       RULER SECTION.
       Para-Inches.
           Move 0.0 To Ws-X.
           Move 3.0 TO Ws-Y WS-Y2.
           Move Ws-Size-Inches TO Ws-X2.
           Call "P$DrawLine" USING Ws-X, Ws-Y,
                                   ModeIsAbsolute,
                                   UnitsAreInches,
                                   Ws-X2, Ws-Y2,
                                   ModeIsAbsolute,
                                   UnitsAreInches.
           Move 0.0 To Ws-X2.
           CALL "P$SetPosition" Using Ws-X,  Ws-Y,
                                      ModeIsAbsolute,
                                      UnitsAreInches.
           Compute LF-Height Rounded = (12 * Dc-LogPixelsY)  / 72.
           Add One-Inch-Line To Ws-Y  Giving WS-Y2.
           CALL "P$SetFont" Using LF-HeightParam, LF-Height.
           Call "P$DrawLine" Using Ws-X, Ws-Y,
                                   ModeIsAbsolute,
                                   UnitsAreInches,
                                   Ws-X,  Ws-Y2,
                                   ModeIsAbsolute,
                                   UnitsAreInches.
           ADD Sixteenth-Inch to Ws-X.
           Set IDX1 To 1.
           Move Zero to Ws-count.
           Perform Varying Ws-X From Ws-X By Sixteenth-Inch Until
              Ws-X > WS-Size-Inches
                   Add Ws-Line-Length(idx1) To Ws-Y GIVING Ws-Y2
                   Call "P$DrawLine" Using Ws-X, Ws-Y,
                                           ModeIsAbsolute,
                                           UnitsAreInches,
                                           Ws-X, Ws-Y2,
                                           ModeIsAbsolute,
                                           UnitsAreInches
                   If IDX1 = 16
                       Add 1 To Ws-Count
                       Subtract Sixteenth-Inch From Ws-Y2
                       Compute Ws-N = Ws-X - (3 * Sixteenth-Inch)
                       Call "P$TextOut" Using Ws-Count-Out Ws-N, Ws-Y2
                       Set IDX1 To 1
                   Else
                       Set IDX1 Up By 1
                   End-If
           End-Perform.
           Compute LF-Height Rounded = (10 * Dc-LogPixelsY)  / 72.
           CALL "P$SetFont" Using LF-HeightParam, LF-Height.
           Call "P$TextOut" Using "INCHES" 0.125, 3.5,
                                   ModeIsAbsolute,
                                   UnitsAreInches.

       Para-Centimeters.
           Move 0.0 to Ws-X.
           Move 11.0 TO Ws-Y Ws-Y2.
           Move Ws-Size-Centimeters To Ws-X2.
           Call "P$DrawLine" Using Ws-X, Ws-Y,
                                   ModeIsAbsolute,
                                   UnitsAreMetric,
                                   Ws-X2, Ws-Y2,
                                   ModeIsAbsolute,
                                   UnitsAreMetric.

           Move 10 To  Ws-Y2.
           Call "P$SetPosition" Using Ws-X, Ws-Y2,
                                      ModeIsAbsolute,
                                      UnitsAreMetric.
           Compute LF-Height Rounded = (10 * Dc-LogPixelsY)  / 72.
           Call "P$SetFont" Using LF-HeightParam      LF-Height,
                                  LF-EscapementParam, 1800.    *> Upside Down
           Call "P$TextOut" Using "CENTIMETERS", 18.8, 9.6,
                                  ModeIsAbsolute,
                                  UnitsAreMetric.
           Move Zero to Ws-Cent-count.
           Compute WS-X = WS-Size-Centimeters - 1.
           Perform Varying ws-x from Ws-X By -1
              until Ws-X = Zero
               Add 1 to Ws-Cent-Count
               Compute Ws-X2 = Ws-x + (2 * One-Millimeter)
               If Ws-Cent-Count < 10
                   Call "P$TextOut" Using Ws-Cent-Count-OUT(2:1),
                                          Ws-X2, Ws-Y2,
                                          ModeIsAbsolute,
                                          UnitsAreMetric
               Else
                   Call "P$TextOut" Using Ws-Cent-Count-Out,
                                          Ws-X2, Ws-Y2,
                                          ModeIsAbsolute,
                                          UnitsAreMetric
               End-If
           End-Perform.

           Move 0 to Ws-Cent-Count.
           Move 0.0 to Ws-X.
           Move 11.0 TO Ws-Y Ws-Y2.
           Call "P$SetPosition" Using Ws-X, Ws-Y2,
                                      ModeIsAbsolute,
                                      UnitsAreMetric.
           Subtract One-Centimeter-Line From Ws-Y Giving Ws-Y2.
           Call "P$DrawLine" Using Ws-X,  WS-Y,
                                   ModeIsAbsolute,
                                   UnitsAreMetric,
                                   Ws-X,  WS-Y2,
                                   ModeIsAbsolute,
                                   UnitsAreMetric.
           Add One-Millimeter To Ws-x.
           Perform Varying Ws-X From Ws-X By One-Millimeter Until
              Ws-X > Ws-Size-Centimeters
                 Add 1 TO Ws-Cent-Count
                 Evaluate  True
                   When Ws-Cent-Count = 5
                       Subtract Five-Millimeter-Line from Ws-Y
                           Giving Ws-Y2
                   When Ws-Cent-Count = 10
                       Subtract One-Centimeter-Line from Ws-Y
                           Giving Ws-Y2
                   When Other
                       Subtract One-Millimeter-Line from Ws-Y
                           Giving Ws-Y2
                   End-Evaluate
                   CALL "P$DrawLine" USING Ws-X, WS-Y,
                                           ModeIsAbsolute,
                                           UnitsAreMetric,
                                           Ws-X, WS-Y2,
                                           ModeIsAbsolute,
                                           UnitsAreMetric
                   If Ws-Cent-Count = 10
                      Move 0 to Ws-Cent-Count
                   End-If
           End-Perform.

       END PROGRAM  PRULER.
