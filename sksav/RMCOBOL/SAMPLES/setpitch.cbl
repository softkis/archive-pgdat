       IDENTIFICATION DIVISION.
       PROGRAM-ID.       SETPITCH.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION:
      *
      * SETPITCH -  Test the P$SETPITCH
      *
      * Version Identification:
      *   $Revision:   1.1.1.3  $
      *   $Date:   27 Sep 1999 15:01:52  $
      *
      *********************************************************************

       DATE-WRITTEN.     04/20/1999

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL-85.
       OBJECT-COMPUTER.  RMCOBOL-85.
       SPECIAL-NAMES.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT PRINTER ASSIGN TO PRINT, "PRINTER?".

       DATA DIVISION.
       FILE SECTION.

       FD  PRINTER.
       01  PRINT-RECORD                    PIC X(132).

       WORKING-STORAGE SECTION.
       77  DATE-CONVERT                PIC 9(5)V9(4) VALUE 10000.0001.
       78  DATE-COMP                   VALUE IS DATE-COMPILED.

       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(31) VALUE
           "RUNTIME RESULTS - SETPITCH   ".

      *  Modify the following output line to describe the test.
       01  DESCRIP-LINE.
           05  FILLER                  PIC X(26)  VALUE
           "**   SETPITCH  - Test For ".
           05  FILLER                  PIC X(22) VALUE
           "Call To P$SetPitch  **".
      *     1234567890123456789012345678901234567890

       01  DOW-SUB                    PIC 9(1)  VALUE ZERO.
       01  DAY-OF-WEEK-TAB            PIC X(21) VALUE
           "MonTueWedThuFriSatSun".
       01  DAY-OF-WEEK-RED REDEFINES DAY-OF-WEEK-TAB.
           05  DAY-OF-THE-WEEK        PIC X(3) OCCURS 7 TIMES.

       01  TIME-DATE-LINE.
           05  FILLER                 PIC X(6)  VALUE "Date: ".
           05  MM                     PIC XX.
           05  SLASH-1                PIC X     VALUE "/".
           05  DD                     PIC XX.
           05  SLASH-2                PIC X     VALUE "/".
           05  YY                     PIC X(5).
           05  FILLER                 PIC X     VALUE "(".
           05  DOW                    PIC X(3).
           05  FILLER                 PIC X     VALUE ")".
           05  FILLER                 PIC X(13) VALUE "   Run Time: ".
           05  HH                     PIC XX.
           05  DIV-1                  PIC X     VALUE ":".
           05  MN                     PIC XX.
           05  DIV-2                  PIC X     VALUE ":".
           05  SS                     PIC XX.
           05  FILLER                 PIC X(5).
           05  FILLER                 PIC X(15) VALUE "Date Compiled: ".
           05  DATE-COMPILE           PIC 99/99/9999.

       COPY "rminfo.cpy".
       COPY "sysinfo.cpy".
       COPY "printinf.cpy".
       COPY "logfont.cpy".

       01  RM-LINE.
           05  RM-USER                 PIC X(9)  VALUE "User:    ".
           05  RM-SUB-LINE.
               10  FILLER              PIC X(16) VALUE
               " using RM/COBOL ".
               10  RM-LINE-VersionNumber
                                       PIC X(8).
               10  FILLER              PIC X(1)  VALUE "(".
               10  RM-Line-RegistrationNumber PIC X(18).
               10  FILLER              PIC X(1)  VALUE ")".
               10  FILLER              PIC X(12) VALUE SPACES.

       01  PRT-LINE.
           05  FILLER                 PIC X(9)  VALUE "Printer: ".
           05  Prt-Line-PrinterName   PIC X(71).

       01  Font-LINE.
           05  FILLER                 PIC X(6)  VALUE "Font: ".
           05  Font-Face-Name         PIC X(31).
           05  FILLER                 PIC X(8)  VALUE "Height: ".
           05  Font-Height            PIC ----9.
           05  FILLER                 PIC X(6)  VALUE SPACES.

       01  UNDER-LINE.
           05  FILLER                  PIC X(80) VALUE ALL "-".

       COPY "windefs.cpy".
      *
      *    Define Device Information Definition
      *
       77 I                                    Picture 99 Value 80.
       01 DefineDeviceInformation.
          02  DDI-DeviceName                   Picture X(80).
          02  DDI-PortName                     Picture X(10).
          02  DDI-FontName                     Picture X(80).
          02  DDI-PointSize                    Picture 9(5) BINARY (2).
          02  DDI-RawModeValue                 Picture X.
              88  DDI-RawMode                    Value 'Y'
                                            When False 'N'.
          02  DDI-EscapeModeValue              Picture X.
              88  DDI-EscapeMode                 Value 'Y'
                                            When False 'N'.

       01  Ws-DefineDevice-Stat                Picture 9(1) Value 0.
           88  No-Define-Device                  Value 0.

       01  Define-Pointer                      Picture 9(3) Value 1.
       01  Ws-Overflow-Flag                    Picture X Value 'N'.
           88 Overflow-Flag                       Value 'Y'.
       01  Ws-Character-Found                  Picture X Value 'N'.
           88 Character-Found                     Value 'Y'
                                             When False 'N'.
       01  Ws-Pointsize                         Picture Z9.

       PROCEDURE DIVISION.
       DRIVER-SECTION SECTION.
      ** Main driver paragraph **
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.
           CLOSE PRINTER.
           STOP RUN.

      ** Prepare heading information **
       INITIAL-SETUP.
           OPEN OUTPUT PRINTER.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           ACCEPT DOW-SUB       FROM DAY-OF-WEEK.
           MOVE DAY-OF-THE-WEEK(DOW-SUB)   TO DOW.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.
           COMPUTE DATE-COMPILE = DATE-COMP * DATE-CONVERT.

      ** Print heading **
       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1      AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2      AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE   AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "C$SetDevelopmentMode".
           CALL "C$GetRMInfo"      USING RMInformation.
           CALL "C$GetSysInfo"     USING SystemInformation.
           CALL "P$GetPrinterInfo" USING PrinterInformation.
           CALL "P$GetFont"        USING LogicalFont.

           MOVE  RM-VersionNumber      TO RM-Line-VersionNumber.
           MOVE  RM-RegistrationNumber TO RM-Line-RegistratioNnumber.
           MOVE  PI-PrinterName        TO Prt-Line-PrinterName.

           STRING RM-USER       Delimited by Size,
                  Sys-UserName  Delimited by Spaces,
                  RM-SUB-LINE   Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           STRING "Machine: "   Delimited by Size,
                  Sys-NodeName  Delimited by Spaces,
                  " running "   Delimited by Size,
                  Sys-Name      Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM PRT-LINE   AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           IF RM-VersionNumber EQUAL "7.00.00 "
              Next Sentence
           Else
              CALL "P$GetDefineDeviceInfo" Using DefineDeviceInformation
                                          Giving Ws-DefineDevice-Stat
              END-CALL
              If No-Define-Device
                  Write Print-Record from "Define-Device: None "
              Else
                  Perform Define-Device-Line
              End-If
           End-If.

           Move Lf-FaceName to Font-Face-Name.
           Move Lf-Height   to Font-Height.
           WRITE PRINT-RECORD FROM Font-LINE AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

       DEFINE-DEVICE-LINE.
           Move "Define-Device: "  To Print-Record.
           Move 16                 To Define-Pointer.

           If DDI-DeviceName Not Equal Spaces
               Set Character-Found To False
               Perform Varying I from 80 by -1 Until Character-Found
                   If DDI-DeviceName (I:1) Not Equal Spaces
                       Set Character-Found To True
                   End-If
               End-Perform
               If I < 80
                   Add 1 To I
               End-If
               String DDI-DeviceName(1:I) Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow DeviceName " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           If Not OverFlow-Flag
               Perform String-Comma.

           If DDI-PortName Not Equal Spaces And Not OverFlow-Flag
               String "Port="         Delimited by Size,
                      DDI-PortName    Delimited by Spaces,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow PortName " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           If Not OverFlow-Flag
               Perform String-Comma.

           If DDI-FontName Not Equal Spaces And Not OverFlow-Flag
               Set Character-Found To False
               Perform Varying I from 80 by -1 Until Character-Found
                   If DDI-FontName (I:1) Not Equal Spaces
                       Set Character-Found To True
                   End-If
               End-Perform
               If I < 80
                   Add 1 To I
               End-If
               String "Font="                Delimited by Size,
                       DDI-FontName(1:I)     Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow FontName " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           If Not OverFlow-Flag
               Perform String-Comma.

           If DDI-PointSize Not Equal Zeros And Not OverFlow-Flag
               Move DDI-PointSize to Ws-PointSize
               String "Size="         Delimited by Size,
                      Ws-PointSize    Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow PointSize " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           IF  DDI-RawMode And Not OverFlow-Flag
               String ",RAW"         Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow RAW " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           IF  DDI-EscapeMode And Not OverFlow-Flag
               String ",ESC"         Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow ESC " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           Write Print-Record.
           Move Spaces To Print-Record.

       STRING-COMMA.
           String ","             Delimited by Size
               Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow Comma " Define-Pointer
                   Set Overflow-Flag To True
           End-String.

       WRAP-UP-SUMMARY.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER ADVANCING 2 LINES.
           DISPLAY "END OF  PTEXTMET TEST"
               LINE 5 POSITION 10 ERASE.

       MAIN SECTION.
       TEST-1.
      *
           CALL "P$SetPitch" USING PitchCompressed.
           WRITE PRINT-RECORD FROM
               "This should be Compressed   Line 1".
           WRITE PRINT-RECORD FROM
               "This should be Compressed   Line 2".
           CALL "P$SetPitch" USING PitchNormal.
           WRITE PRINT-RECORD FROM
               "This should be Normal       Line 3" After 2.
           CALL "P$SetPitch" USING PitchExpanded.
           WRITE PRINT-RECORD FROM
               "This should be Expanded     Line 4".
           CALL "P$SetPitch" USING PitchNormal.
           WRITE PRINT-RECORD  FROM
               "This should be Normal       Line 5" After 2.


       END PROGRAM  SETPITCH.
