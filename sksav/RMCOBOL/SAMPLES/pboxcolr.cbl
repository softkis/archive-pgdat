       IDENTIFICATION DIVISION.
       PROGRAM-ID.      PBOXCOLR.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION:
      *
      * PBOXCOLR -  Test CALLs to P$SetBoxShade, P$DrawBox and P$TextOut
      *             This program will print out boxes in various colors
      *             and shades.
      *
      * Version Identification:
      *   $Revision:   1.1.1.1  $
      *   $Date:   27 Sep 1999 14:41:02  $
      *
      *********************************************************************

       DATE-WRITTEN.    03/23/1999.
       REMARKS.     This program will print out boxes in various colors.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL-85.
       OBJECT-COMPUTER.  RMCOBOL-85.
       SPECIAL-NAMES.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT PRINTER ASSIGN TO PRINT, "PRINTER?".

       DATA DIVISION.
       FILE SECTION.

       FD  PRINTER.
       01  PRINT-RECORD                    PIC X(80).

       WORKING-STORAGE SECTION.
       77  WS-Percent-Sub     PIC 99 VALUE 0.
       77  WS-Color-Sub       PIC 99 VALUE 0.
       78  Eighth-Inch        VALUE   .125.
       78  Qtr-Inch           VALUE   .250.
       78  Half-Inch          VALUE   .500.
       78  One-Inch           VALUE  1.000.

       copy "windefs.cpy".

       01 WS-ARGS.
           03 WS-X                     PIC 9(9)V9(3)   VALUE ZERO.
           03 WS-Y                     PIC 9(9)V9(3)   VALUE ZERO.

       01  WS-Percent-Table.
           03              PIC 9(03)  VALUE 100.
           03              PIC 9(03)  VALUE 090.
           03              PIC 9(03)  VALUE 080.
           03              PIC 9(03)  VALUE 070.
           03              PIC 9(03)  VALUE 050.
           03              PIC 9(03)  VALUE 020.
           03              PIC 9(03)  VALUE 010.
           03              PIC 9(03)  VALUE 000.
       01  WS-Percent-TAB REDEFINES WS-percent-TABLE.
           03 WS-Percent   PIC 9(03)  OCCURS 08 TIMES.

       01  WS-Color-Table.
           03              PIC X(13)  VALUE ColorBlack.
           03              PIC X(13)  VALUE ColorDarkBlue.
           03              PIC X(13)  VALUE ColorDarkGreen.
           03              PIC X(13)  VALUE ColorDarkCyan.
           03              PIC X(13)  VALUE ColorDarkRed.
           03              PIC X(13)  VALUE ColorDarkMagenta.
           03              PIC X(13)  VALUE ColorBrown.
           03              PIC X(13)  VALUE ColorLightGray.
           03              PIC X(13)  VALUE ColorDarkGray.
           03              PIC X(13)  VALUE ColorBlue.
           03              PIC X(13)  VALUE ColorGreen.
           03              PIC X(13)  VALUE ColorCyan.
           03              PIC X(13)  VALUE ColorRed.
           03              PIC X(13)  VALUE ColorMagenta.
           03              PIC X(13)  VALUE ColorYellow.
           03              PIC X(13)  VALUE ColorWhite.
       01  WS-Color-TAB REDEFINES WS-Color-TABLE.
           03 WS-COLOR     PIC X(13) OCCURS 16 TIMES.

       01  WS-Text-Out.
           03 Ws-Text-Out-Pct  PIC ZZ9.
           03                  PIC X(2) VALUE " %".
       01  WS-Text-Line.
           03                  PIC X(80) VALUE
           "This Test will print out boxes of each color with different
      -"degrees of shading".

       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(31) VALUE
           "RUNTIME RESULTS - PBOXCOLR   ".

      *  Modify the following output line to describe the test.
       01  DESCRIP-LINE.
           05  FILLER                  PIC X(26)  VALUE
           "**   PBOXCOLR  - Test For ".
           05  FILLER                  PIC X(27) VALUE
           "Boxes With Shaded Colors **".
      *     1234567890123456789012345678901234567890

       77  DATE-CONVERT                PIC 9(5)V9(4) VALUE 10000.0001.
       78  DATE-COMP                   VALUE IS DATE-COMPILED.
       01  DOW-SUB                    PIC 9(1)  VALUE ZERO.
       01  DAY-OF-WEEK-TAB            PIC X(21) VALUE
           "MonTueWedThuFriSatSun".
       01  DAY-OF-WEEK-RED REDEFINES DAY-OF-WEEK-TAB.
           05  DAY-OF-THE-WEEK        PIC X(3) OCCURS 7 TIMES.

       01  TIME-DATE-LINE.
           05  FILLER                 PIC X(6)  VALUE "Date: ".
           05  MM                     PIC XX.
           05  SLASH-1                PIC X     VALUE "/".
           05  DD                     PIC XX.
           05  SLASH-2                PIC X     VALUE "/".
           05  YY                     PIC X(5).
           05  FILLER                 PIC X     VALUE "(".
           05  DOW                    PIC X(3).
           05  FILLER                 PIC X     VALUE ")".
           05  FILLER                 PIC X(13) VALUE "   Run Time: ".
           05  HH                     PIC XX.
           05  DIV-1                  PIC X     VALUE ":".
           05  MN                     PIC XX.
           05  DIV-2                  PIC X     VALUE ":".
           05  SS                     PIC XX.
           05  FILLER                 PIC X(5).
           05  FILLER                 PIC X(15) VALUE "Date Compiled: ".
           05  DATE-COMPILE           PIC 99/99/9999.

       COPY "rminfo.cpy".
       COPY "sysinfo.cpy".
       COPY "printinf.cpy".

       01  RM-LINE.
           05  RM-USER                 PIC X(9)  VALUE "User:    ".
           05  RM-SUB-LINE.
               10  FILLER              PIC X(16) VALUE
               " using RM/COBOL ".
               10  RM-LINE-VersionNumber
                                       PIC X(8).
               10  FILLER              PIC X(1)  VALUE "(".
               10  RM-Line-RegistrationNumber PIC X(18).
               10  FILLER              PIC X(1)  VALUE ")".
               10  FILLER              PIC X(12) VALUE SPACES.

       01  PRT-LINE.
           05  FILLER                 PIC X(9)  VALUE "Printer: ".
           05  Prt-Line-PrinterName   PIC X(71).

       01  UNDER-LINE.
           05  FILLER                  PIC X(80) VALUE ALL "-".

       PROCEDURE DIVISION.
       DRIVER-SECTION SECTION.
      ** Main driver paragraph **
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 2 LINES.
           CLOSE PRINTER.
           STOP RUN.

      ** Prepare heading information **
       INITIAL-SETUP.
           OPEN OUTPUT PRINTER.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           ACCEPT DOW-SUB       FROM DAY-OF-WEEK.
           MOVE DAY-OF-THE-WEEK(DOW-SUB)   TO DOW.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.
           COMPUTE DATE-COMPILE = DATE-COMP * DATE-CONVERT.

      ** Print heading **
       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1      AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2      AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE   AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "C$SetDevelopmentMode".
           CALL "C$GetRMInfo"      USING RMInformation.
           CALL "C$GetSysInfo"     USING SystemInformation.
           CALL "P$GetPrinterInfo" USING PrinterInformation.

           MOVE  RM-VersionNumber      TO RM-Line-VersionNumber.
           MOVE  RM-RegistrationNumber TO RM-Line-RegistratioNnumber.
           MOVE  PI-PrinterName        TO Prt-Line-PrinterName.

           STRING RM-USER       Delimited by Size,
                  Sys-UserName  Delimited by Spaces,
                  RM-SUB-LINE   Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           STRING "Machine: "   Delimited by Size,
                  Sys-NodeName  Delimited by Spaces,
                  " running "   Delimited by Size,
                  Sys-Name      Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM PRT-LINE   AFTER 1 LINES.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

       MAIN SECTION.
       TEST-1.
           Write print-record from WS-Text-Line after 2.
           Move Spaces To Print-Record.
           Perform Heading-1.

           PERFORM VARYING WS-Color-Sub FROM 1 BY 1
             UNTIL WS-Color-Sub > 16
               PERFORM VARYING WS-Percent-Sub FROM 1 BY 1
                 UNTIL Ws-Percent-Sub > 8
                   CALL "P$SetBoxShade" Using Ws-Color (Ws-Color-Sub),
                                              Ws-Percent(Ws-Percent-Sub)
                   CAll "P$DrawBox" Using Ws-X, Ws-Y, "A", "I",
                                   Half-Inch,  Half-Inch,
                                   "Inches",
                                   DrawBoxWithShading
                   Add One-Inch To WS-X
               End-Perform

      * Print color text.
               Move Zeros       To WS-X
               Add Half-Inch    To Ws-Y
               Add Eighth-Inch  To Ws-Y
               Call "P$TextOut" Using Ws-Color (Ws-Color-Sub) Ws-X, Ws-Y

               Move Qtr-Inch    To WS-X
               Add  Qtr-Inch    To Ws-Y
               Add  Eighth-Inch To Ws-Y

      * New Page after first eight.
               If WS-Color-Sub Equal 8
                   Perform Heading-1
               End-If

           END-PERFORM.

       PERCENT-HEADING SECTION.
       Heading-1.
           Write Print-Record After Page.
           Move Eighth-Inch to WS-Y.
           Move Qtr-Inch    to WS-X.
           PERFORM VARYING WS-Percent-Sub FROM 1 BY 1
              Until Ws-Percent-Sub > 8
                Move Ws-Percent (Ws-Percent-Sub) to Ws-Text-Out-Pct
                Call "P$TextOut" Using Ws-Text-Out, Ws-X, Ws-Y
                Add One-Inch To Ws-X
           End-Perform.
           Move Qtr-Inch  to WS-X.
           Move Half-Inch to WS-Y.

       END PROGRAM  PBOXCOLR.
