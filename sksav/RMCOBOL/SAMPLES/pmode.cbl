       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PMODE.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION:
      *
      * PMODE -  Test P$SetDefaultMode and P$SetDefaultUnits
      *          Also a Test for P$SetLineExtendMode
      *
      * Version Identification:
      *   $Revision:   1.1.1.1  $
      *   $Date:   27 Sep 1999 14:58:36  $
      *
      *********************************************************************

       DATE-WRITTEN.     03/25/1999.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL-85.
       OBJECT-COMPUTER.  RMCOBOL-85.
       SPECIAL-NAMES.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT PRINTER ASSIGN TO PRINT, "PRINTER?".

       DATA DIVISION.
       FILE SECTION.

       FD  PRINTER.
       01  PRINT-RECORD                    PIC X(80).

       WORKING-STORAGE SECTION.

       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(15) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(17) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "RUNTIME RESULTS - PMODE   ".

      *  Modify the following output line to describe the test.
       01  DESCRIP-LINE.
           05  FILLER                  PIC X(23)  VALUE
           "**   PMODE  - Test For ".
           05  FILLER                  PIC X(26) VALUE
           "Set Default Mode/Units  **".
      *     1234567890123456789012345678901234567890

       77  DATE-CONVERT                PIC 9(5)V9(4) VALUE 10000.0001.
       78  DATE-COMP                   VALUE IS DATE-COMPILED.
       01  DOW-SUB                    PIC 9(1)  VALUE ZERO.
       01  DAY-OF-WEEK-TAB            PIC X(21) VALUE
           "MonTueWedThuFriSatSun".
       01  DAY-OF-WEEK-RED REDEFINES DAY-OF-WEEK-TAB.
           05  DAY-OF-THE-WEEK        PIC X(3) OCCURS 7 TIMES.

       01  TIME-DATE-LINE.
           05  FILLER                 PIC X(6)  VALUE "Date: ".
           05  MM                     PIC XX.
           05  SLASH-1                PIC X     VALUE "/".
           05  DD                     PIC XX.
           05  SLASH-2                PIC X     VALUE "/".
           05  YY                     PIC X(5).
           05  FILLER                 PIC X     VALUE "(".
           05  DOW                    PIC X(3).
           05  FILLER                 PIC X     VALUE ")".
           05  FILLER                 PIC X(13) VALUE "   Run Time: ".
           05  HH                     PIC XX.
           05  DIV-1                  PIC X     VALUE ":".
           05  MN                     PIC XX.
           05  DIV-2                  PIC X     VALUE ":".
           05  SS                     PIC XX.
           05  FILLER                 PIC X(5).
           05  FILLER                 PIC X(15) VALUE "Date Compiled: ".
           05  DATE-COMPILE           PIC 99/99/9999.

       COPY "rminfo.cpy".
       COPY "sysinfo.cpy".
       COPY "printinf.cpy".

       01  RM-LINE.
           05  RM-USER                 PIC X(9)  VALUE "User:    ".
           05  RM-SUB-LINE.
               10  FILLER              PIC X(16) VALUE
               " using RM/COBOL ".
               10  RM-LINE-VersionNumber
                                       PIC X(8).
               10  FILLER              PIC X(1)  VALUE "(".
               10  RM-Line-RegistrationNumber PIC X(18).
               10  FILLER              PIC X(1)  VALUE ")".
               10  FILLER              PIC X(12) VALUE SPACES.

       01  PRT-LINE.
           05  FILLER                 PIC X(9)  VALUE "Printer: ".
           05  Prt-Line-PrinterName   PIC X(71).

       01  UNDER-LINE.
           05  FILLER                  PIC X(80) VALUE ALL "-".

       copy "windefs.cpy".
       PROCEDURE DIVISION.
       DRIVER-SECTION SECTION.
      ** Main driver paragraph **
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 2 LINES.
           CLOSE PRINTER.
           STOP RUN.

      ** Prepare heading information **
       INITIAL-SETUP.
           OPEN OUTPUT PRINTER.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           ACCEPT DOW-SUB       FROM DAY-OF-WEEK.
           MOVE DAY-OF-THE-WEEK(DOW-SUB)   TO DOW.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.
           COMPUTE DATE-COMPILE = DATE-COMP * DATE-CONVERT.

      ** Print heading **
       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1      AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2      AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE   AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "C$SetDevelopmentMode".
           CALL "C$GetRMInfo"      USING RMInformation.
           CALL "C$GetSysInfo"     USING SystemInformation.
           CALL "P$GetPrinterInfo" USING PrinterInformation.

           MOVE  RM-VersionNumber      TO RM-Line-VersionNumber.
           MOVE  RM-RegistrationNumber TO RM-Line-RegistratioNnumber.
           MOVE  PI-PrinterName        TO Prt-Line-PrinterName.

           STRING RM-USER       Delimited by Size,
                  Sys-UserName  Delimited by Spaces,
                  RM-SUB-LINE   Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           STRING "Machine: "   Delimited by Size,
                  Sys-NodeName  Delimited by Spaces,
                  " running "   Delimited by Size,
                  Sys-Name      Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM PRT-LINE   AFTER 1 LINES.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

       MAIN SECTION.
      *
       MAIN-START.
           CALL "P$SetDefaultUnits" Using "Inches".
           CALL "P$TEXTOUT" using
               "Set Text Position to:  0, 2.5  Absolute Inches" 0, 2.5.
           CALL "P$SetTextPosition" Using 0, 2.5, PositionIsTop "A", "I".

           CALL "P$SetDefaultMode" Using "Relative".
      * Inches
           CALL "P$TextOut" Using
               "This Text is Positioned at Relative Inches 1, 1"  1, 1.

           CALL "P$TextOut" using
               "Set Text Position to: 0, 4  Absolute Inches"
               0, 4, "A", "I".
           CALL "P$SetTextPosition" Using 0, 4, PositionIsTop "A" "I".

      * Centimeters
           CALL "P$SetDefaultUnits" Using "M".
           CALL "P$TextOut" Using
               "This Text is Positioned at Relative Centimeters 1, 1"
                1, 1.

           CALL "P$TextOut" using
               "Set Text Position to: 0, 5  Absolute Inches"
               0, 5, "A", "I".
           CALL "P$SetTextPosition" Using 0, 5, PositionIsTop "A" "I".

      * Characters
           CALL "P$SetDefaultUnits" Using "C".
           CALL "P$TextOut" Using
               "This Text is Positioned at Relative Characters 5, 3"
                  5, 3.

           CALL "P$TextOut" Using
               "Set Text Position to: 0, 6  Absolute Inches"
               0, 6, "A", "I".
           CALL "P$SetTextPosition" Using 0, 6, PositionIsTop "A" "I".

      * Device Units
           CALL "P$SetDefaultUnits" Using "D".
           CALL "P$TextOut" Using
               "This Text is Positioned at Relative Units 75, 75"
                  75, 75.

      * Set Line ExtendMode
           WRITE Print-Record From "This text should all" after 3.
           CALL "P$SetLineExtendMode".
           WRITE Print-Record From " be on the same line." AFTER 0.
           CALL "P$SetLineExtendMode".
           WRITE Print-Record From
               " Testing P$SetLineExtendMode." After 0.
           WRITE Print-Record From spaces after 2.

       END PROGRAM  PMODE.
