       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PSETFONT.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION:
      *
      * PSETFONT - Test P$SetFont Subroutine. Tests calling by group and pairs.
      *            Tests all values of all 88 levels in LOGFONT.CPY.
      *            Also CALLs P$SetLineExtendMode and P$ClearFont
      *                       P$SetLeftMargin and P$SetTopMargin
      *
      * Version Identification:
      *   $Revision:   1.1.1.2  $
      *   $Date:   27 Sep 1999 15:00:56  $
      *
      *********************************************************************

       DATE-WRITTEN.     01/26/1999

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PRINTER ASSIGN TO PRINT, "PRINTER?".
       DATA DIVISION.
       FILE SECTION.
       FD  PRINTER.
       01  FD-RECORD          PIC X(80).
       01  PRINT-RECORD       PIC X(80).
       WORKING-STORAGE SECTION.
       77  DATE-CONVERT                PIC 9(5)V9(4) VALUE 10000.0001.
       01  LF-OUT                      PIC ZZ9       VALUE 0.
       78  DATE-COMP                   VALUE IS DATE-COMPILED.
       78  WS-ALPHABET     VALUE "ABCDEFGHIJ abcdefghij 0123456789".
       78  ONE-HALF        VALUE .5.
       78  TWO             VALUE 2.

       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "RUNTIME RESULTS - PSETFONT".

      *  Modify the following output line to describe the test.
       01  DESCRIP-LINE.
           05  FILLER                  PIC X(26)  VALUE
           "**   PSETFONT  - Test For ".
           05  FILLER                  PIC X(20) VALUE
           "Call To P$SetFont **".
      *     1234567890123456789012345678901234567890

       01  DOW-SUB                    PIC 9(1)  VALUE ZERO.
       01  DAY-OF-WEEK-TAB            PIC X(21) VALUE
           "MonTueWedThuFriSatSun".
       01  DAY-OF-WEEK-RED REDEFINES DAY-OF-WEEK-TAB.
           05  DAY-OF-THE-WEEK        PIC X(3) OCCURS 7 TIMES.

       01  TIME-DATE-LINE.
           05  FILLER                 PIC X(6)  VALUE "Date: ".
           05  MM                     PIC XX.
           05  SLASH-1                PIC X     VALUE "/".
           05  DD                     PIC XX.
           05  SLASH-2                PIC X     VALUE "/".
           05  YY                     PIC X(5).
           05  FILLER                 PIC X     VALUE "(".
           05  DOW                    PIC X(3).
           05  FILLER                 PIC X     VALUE ")".
           05  FILLER                 PIC X(13) VALUE "   Run Time: ".
           05  HH                     PIC XX.
           05  DIV-1                  PIC X     VALUE ":".
           05  MN                     PIC XX.
           05  DIV-2                  PIC X     VALUE ":".
           05  SS                     PIC XX.
           05  FILLER                 PIC X(5).
           05  FILLER                 PIC X(15) VALUE "Date Compiled: ".
           05  DATE-COMPILE           PIC 99/99/9999.

       COPY "rminfo.cpy".
       COPY "sysinfo.cpy".
       COPY "printinf.cpy".

       01  RM-LINE.
           05  RM-USER                 PIC X(9)  VALUE "User:    ".
           05  RM-SUB-LINE.
               10  FILLER              PIC X(16) VALUE
               " using RM/COBOL ".
               10  RM-LINE-VersionNumber
                                       PIC X(8).
               10  FILLER              PIC X(1)  VALUE "(".
               10  RM-Line-RegistrationNumber PIC X(18).
               10  FILLER              PIC X(1)  VALUE ")".
               10  FILLER              PIC X(12) VALUE SPACES.

       01  PRT-LINE.
           05  FILLER                 PIC X(9)  VALUE "Printer: ".
           05  Prt-Line-PrinterName   PIC X(71).

       01  UNDER-LINE.
           05  FILLER                  PIC X(80) VALUE ALL "-".

       01 WS-ARGS.
           03 WS-X                     PIC 9(9)V9(3) VALUE ZERO.
           03 WS-Y                     PIC 9(9)V9(3) VALUE ZERO.
           03 WS-X-SIZE                PIC 9(9)V9(3) VALUE ZERO.
           03 WS-Y-SIZE                PIC 9(9)V9(3) VALUE ZERO.
           03 Point-Size               Pic 9(2)      Value 10.
           03 WS-MODE                  PIC X.
              88 WS-MODE-ABSOLUTE      VALUE "A".
              88 WS-MODE-RELATIVE      VALUE "R".
           03 WS-UNITS                 PIC X.
              88 WS-UNITS-INCHES       VALUE "I".
              88 WS-UNITS-CHARACTERS   VALUE "C".
              88 WS-UNITS-METRIC       VALUE "M".
              88 WS-UNITS-DEVICE       VALUE "D".
           03 WS-BOX                   PIC X.
              88 WS-DRAW-BOX           VALUE "Y" WHEN FALSE "N".
           03 WS-SHADE                 PIC X     VALUE "N".
              88 WS-SHADE-BOX          VALUE "Y" WHEN FALSE "N".
           03 WS-CLEAR                 PIC X.
              88 WS-CLEAR-FLAG         VALUE "Y" WHEN FALSE "N".
           03 WS-INCREMENT             PIC 9(3)  VALUE ZERO.
           03 WS-PERCENT               PIC 9(3)  VALUE ZERO.
           03 WS-RETURN                PIC S9(4) COMP-1 VALUE ZERO.

       COPY "devcaps.cpy".
       COPY "logfont.cpy".

       PROCEDURE DIVISION.
       DRIVER-SECTION SECTION.
      ** Main driver paragraph **
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER TWO LINES.
           CLOSE PRINTER.
           STOP RUN.

      ** Prepare heading information **
       INITIAL-SETUP.
           OPEN OUTPUT PRINTER.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           ACCEPT DOW-SUB       FROM DAY-OF-WEEK.
           MOVE DAY-OF-THE-WEEK(DOW-SUB)   TO DOW.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.
           COMPUTE DATE-COMPILE = DATE-COMP * DATE-CONVERT.

      ** Print heading **
       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1      AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2      AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE   AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "C$SetDevelopmentMode".
           CALL "C$GetRMInfo"      USING RMInformation.
           CALL "C$GetSysInfo"     USING SystemInformation.
           CALL "P$GetPrinterInfo" USING PrinterInformation.

           MOVE  RM-VersionNumber      TO RM-Line-VersionNumber.
           MOVE  RM-RegistrationNumber TO RM-Line-RegistratioNnumber.
           MOVE  PI-PrinterName        TO Prt-Line-PrinterName.

           STRING RM-USER       Delimited by Size,
                  Sys-UserName  Delimited by Spaces,
                  RM-SUB-LINE   Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           STRING "Machine: "   Delimited by Size,
                  Sys-NodeName  Delimited by Spaces,
                  " running "   Delimited by Size,
                  Sys-Name      Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM PRT-LINE   AFTER 1 LINES.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

       MAIN SECTION.
       TEST-1.
      *
           INITIALIZE LogicalFont.

           CALL "P$SetLeftMargin" USING ONE-HALF.
           CALL "P$GetDeviceCapabilities" Using DC-LogicalPixelsYParam,
                                                DC-LogPixelsY.

           WRITE FD-RECORD FROM
               "Left Margin for this page should be 1/2 inch" AFTER 1.
           WRITE FD-RECORD FROM SPACES AFTER 5.

           SET WS-UNITS-CHARACTERS TO TRUE.
           Compute LF-Height Rounded = (Point-Size * Dc-LogPixelsY)
                                       / 72.

           MOVE "Arial" TO LF-FaceName.
           CALL "P$SetFont" USING LogicalFont.

           Write FD-RECORD FROM "Arial" AFTER 1.
           CALL "P$SetLineExtendMode" USING TWO, WS-UNITS.

           SET LF-Italic          To True.
           CALL "P$SETFONT" USING LogicalFont.
           WRITE FD-RECORD FROM "Italic" AFTER ADVANCING 0.
           CALL "P$SetLineExtendMode" USING TWO, WS-UNITS.

           SET LF-Italic          To False.
           SET LF-Underline       To True.
           CALL "P$SETFONT"  using LogicalFont.
           Write fd-record from "Underline" AFTER ADVANCING 0.
           CALL "P$SetLineExtendMode" USING TWO, WS-UNITS.

           SET LF-Underline       To False.
           SET LF-Strikeout       To True.
           CALL "P$SetFont"  using LogicalFont.
           Write fd-record from "Strikeout" AFTER ADVANCING 0.
           CALL "P$SetLineExtendMode" USING TWO, WS-UNITS.

           SET LF-Strikeout       To False.
           Move 450               To LF-Escapement.
           CALL "P$SETFONT"  using LogicalFont.
           Write FD-record from "Escapement 450" AFTER ADVANCING 0.
           CALL "P$SetLineExtendMode" USING TWO, WS-UNITS.

           Move 000               To LF-Escapement.
           Move 550               To LF-Orientation.
           CALL "P$SetFont"  using LogicalFont.
           Write FD-record from "Orientation 550" AFTER ADVANCING 0.
           CALL "P$ClearFont".

           PERFORM LF-WEIGHT.
           PERFORM LF-CHARSET.
           PERFORM LF-OUTPRECIS.
           PERFORM LF-CLIPPRECIS.
           PERFORM LF-QUALITY.
           PERFORM LF-PITCH.
           PERFORM LF-FAMILY.

       LF-SECTION SECTION 66.

       LF-WEIGHT.
           Move spaces to fd-record.
           Write fd-record from "Testing LF-Weight Values" after TWO.
           Move spaces to fd-record.
           WRITE FD-RECORD AFTER 1.
           SET  LF-WeightIsDontCare          To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsThin              To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsExtraLight        To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsUltraLight        To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsLight             To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsNormal            To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsRegular           To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsMedium            To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsSemiBold          To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsDemiBold          To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsBold              To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsExtraBold         To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsUltraBold         To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsHeavy             To True.
           PERFORM WRITE-LF-WEIGHT.
           SET  LF-WeightIsBlack             To True.
           PERFORM WRITE-LF-WEIGHT.
           CALL "P$CLEARFONT".

       LF-CHARSET.
           Move spaces to fd-record.
           Write fd-record from "Testing LF-Charset Values" after TWO.
           Move spaces to fd-record.
           WRITE FD-RECORD AFTER 1.
           SET LF-CharsetIsAnsi To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsDefault          To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsSymbol           To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsShiftJIS         To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsHangeul          To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsChineseBig5      To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsOEM              To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsJohab            To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsHebrew           To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsArabic           To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsGreek            To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsTurkish          To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsThai             To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsEastEurope       To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsRussian          To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsMAC              To True.
           Perform WRITE-LF-CHARSET.
           SET  LF-CharSetIsBaltic           To True.
           Perform WRITE-LF-CHARSET.
           CALL "P$CLEARFONT".

       LF-OUTPRECIS.
           MOVE SPACES TO FD-RECORD.
           CALL "P$SETTOPMARGIN" USING TWO, "I".
           WRITE FD-RECORD FROM
               "Top Margin for this page should be 2 inches" AFTER PAGE.
           MOVE SPACES TO FD-RECORD.
           WRITE FD-RECORD FROM "Testing LF-OutPrecis Values" AFTER 1.
           Move spaces to fd-record.
           WRITE FD-RECORD AFTER 1.
           SET  LF-OutPrecisIsDefault        To True.
           PERFORM WRITE-LF-OUTPRECIS.
           SET  LF-OutPrecisIsString         To True.
           PERFORM WRITE-LF-OUTPRECIS.
           SET  LF-OutPrecisIsStroke         To True.
           PERFORM WRITE-LF-OUTPRECIS.
           SET  LF-OutPrecisIsTrueType       To True.
           PERFORM WRITE-LF-OUTPRECIS.
           SET  LF-OutPrecisIsDevice         To True.
           PERFORM WRITE-LF-OUTPRECIS.
           SET  LF-OutPrecisIsRaster         To True.
           PERFORM WRITE-LF-OUTPRECIS.
           SET  LF-OutPrecisIsTruTypeOnly    To True.
           PERFORM WRITE-LF-OUTPRECIS.
           SET  LF-OutPrecisIsOutline        To True.
           PERFORM WRITE-LF-OUTPRECIS.
           CALL "P$CLEARFONT".

       LF-CLIPPRECIS.
           Move spaces to fd-record.
           Write fd-record from "Testing LF-ClipPrecis Values" after 2.
           Move spaces to fd-record.
           WRITE FD-RECORD AFTER 1.
           SET LF-ClipPrecisIsDefault       To True.
           PERFORM WRITE-LF-CLIPPRECIS.
           SET LF-ClipPrecisIsStroke        To True.
           PERFORM WRITE-LF-CLIPPRECIS.
           SET LF-ClipPrecisIsLHAngles      To True.
           PERFORM WRITE-LF-CLIPPRECIS.
           SET LF-ClipPrecisIsEmbedded      To True.
           PERFORM WRITE-LF-CLIPPRECIS.
           CALL "P$CLEARFONT".

       LF-QUALITY.
           Move spaces to fd-record.
           Write fd-record from "Testing LF-Quality Values" after TWO.
           Move spaces to fd-record.
           WRITE FD-RECORD AFTER 1.
           SET LF-QualityIsDefault          To True.
           PERFORM WRITE-LF-QUALITY.
           SET LF-QualityIsDraft            To True.
           PERFORM WRITE-LF-QUALITY.
           SET LF-QualityIsProof            To True.
           PERFORM WRITE-LF-QUALITY.
           CALL "P$CLEARFONT".

       LF-PITCH.
           Move spaces to fd-record.
           Write fd-record from "Testing LF-Pitch Values" after TWO.
           Move spaces to fd-record.
           WRITE FD-RECORD AFTER 1.
           SET LF-PitchIsDefault            To True.
           PERFORM WRITE-LF-PITCH.
           SET LF-PitchIsFixed              To True.
           PERFORM WRITE-LF-PITCH.
           SET LF-PitchIsVariable           To True.
           PERFORM WRITE-LF-PITCH.
           CALL "P$CLEARFONT".

       LF-FAMILY.
           Move spaces to fd-record.
           Write fd-record from "Testing LF-Family Values" after TWO.
           Move spaces to fd-record.
           WRITE FD-RECORD AFTER 1.
           SET  LF-FamilyIsDontCare          To True.
           PERFORM WRITE-LF-FAMILY.
           SET  LF-FamilyIsRoman             To True.
           PERFORM WRITE-LF-FAMILY.
           SET  LF-FamilyIsSwiss             To True.
           PERFORM WRITE-LF-FAMILY.
           SET  LF-FamilyIsModern            To True.
           PERFORM WRITE-LF-FAMILY.
           SET  LF-FamilyIsScript            To True.
           PERFORM WRITE-LF-FAMILY.
           SET  LF-FamilyIsDecorative        To True.
           PERFORM WRITE-LF-FAMILY.
           CALL "P$CLEARFONT".

       WRITE-LF-WEIGHT.
           CALL "P$SETFONT" using LF-WeightParam LF-WeightValue
           move lf-WeightValue to lf-out.
           String lf-WeightParam delimited by size, space,
                  lf-out delimited by size,  space
                  ws-alphabet delimited by size
                  into fd-record.
           Write fd-record After 1.
           Move spaces to Fd-record.

       WRITE-LF-CHARSET.
           CALL "P$SETFONT" using LF-CharSetParam LF-CharsetValue
           move lf-charsetvalue to lf-out.
           String lf-charsetparam delimited by size, space,
                  lf-out delimited by size,  space
                  ws-alphabet delimited by size
                  into fd-record.
           Write fd-record After 1.
           Move spaces to Fd-record.

       WRITE-LF-OUTPRECIS.
           CALL "P$SETFONT" using LF-OutPrecisionParam LF-OutPrecisValue
           move lf-OutPrecisValue to lf-out.
           String lf-OutPrecisionParam delimited by size, space,
                  lf-out delimited by size,  space
                  ws-alphabet delimited by size
                  into fd-record.
           Write fd-record After 1.
           Move spaces to Fd-record.

       WRITE-LF-CLIPPRECIS.
           CALL "P$SETFONT" using LF-ClipPrecisionParam,
                                  LF-ClipPrecisValue
           move lf-ClipPrecisValue to lf-out.
           String lf-ClipPrecisionParam delimited by size, space,
                  lf-out delimited by size,  space
                  ws-alphabet delimited by size
                  into fd-record.
           Write fd-record After 1.

       WRITE-LF-QUALITY.
           CALL "P$SETFONT" using LF-QualityParam LF-QualityValue
           move lf-QualityValue to lf-out.
           String lf-QualityParam delimited by size, space,
                  lf-out delimited by size,  space
                  ws-alphabet delimited by size
                  into fd-record.
           Write fd-record After 1.

       WRITE-LF-PITCH.
           CALL "P$SETFONT" using LF-PitchParam LF-PitchValue
           move lf-PitchValue to lf-out.
           String lf-PitchParam delimited by size, space,
                  lf-out delimited by size,  space
                  ws-alphabet delimited by size
                  into fd-record.
           Write fd-record After 1.

       WRITE-LF-FAMILY.
           CALL "P$SETFONT" using LF-FamilyParam LF-FamilyValue
           move lf-FamilyValue to lf-out.
           String lf-FamilyParam delimited by size, space,
                  lf-out delimited by size,  space
                  ws-alphabet delimited by size
                  into fd-record.
           Write fd-record After 1.

       END PROGRAM  PSETFONT.
