       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PESCAPE.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION: This program will test RM Specific Escape Seq.
      *
      * PESCAPE -  This program will test RM Specific Escape Sequences.
      *
      * Version Identification:
      *   $Revision:   1.1.1.2  $
      *   $Date:   27 Sep 1999 14:50:24  $
      *
      *********************************************************************

       DATE-WRITTEN.     03/28/1999.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL-85.
       OBJECT-COMPUTER.  RMCOBOL-85.
       SPECIAL-NAMES.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT PFILE ASSIGN TO PRINT, "PRINTER?".

       DATA DIVISION.
       FILE SECTION.

       FD  PFILE.
       01  PRINT-RECORD                    PIC X(132).

       WORKING-STORAGE SECTION.

       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(15) VALUE SPACES.
           05  FILLER                  PIC X(31) VALUE
           "RUNTIME RESULTS - PESCAPE   ".

      *  Modify the following output line to describe the test.
       01  DESCRIP-LINE.
           05  FILLER                  PIC X(25)  VALUE
           "**   PESCAPE  - Test For ".
           05  FILLER                  PIC X(31) VALUE
           "RM Specific Escape Sequences **".
      *     1234567890123456789012345678901234567890

       77  DATE-CONVERT                PIC 9(5)V9(4) VALUE 10000.0001.
       78  DATE-COMP                   VALUE IS DATE-COMPILED.
       01  DOW-SUB                    PIC 9(1)  VALUE ZERO.
       01  DAY-OF-WEEK-TAB            PIC X(21) VALUE
           "MonTueWedThuFriSatSun".
       01  DAY-OF-WEEK-RED REDEFINES DAY-OF-WEEK-TAB.
           05  DAY-OF-THE-WEEK        PIC X(3) OCCURS 7 TIMES.

       01  TIME-DATE-LINE.
           05  FILLER                 PIC X(6)  VALUE "Date: ".
           05  MM                     PIC XX.
           05  SLASH-1                PIC X     VALUE "/".
           05  DD                     PIC XX.
           05  SLASH-2                PIC X     VALUE "/".
           05  YY                     PIC X(5).
           05  FILLER                 PIC X     VALUE "(".
           05  DOW                    PIC X(3).
           05  FILLER                 PIC X     VALUE ")".
           05  FILLER                 PIC X(13) VALUE "   Run Time: ".
           05  HH                     PIC XX.
           05  DIV-1                  PIC X     VALUE ":".
           05  MN                     PIC XX.
           05  DIV-2                  PIC X     VALUE ":".
           05  SS                     PIC XX.
           05  FILLER                 PIC X(5).
           05  FILLER                 PIC X(15) VALUE "Date Compiled: ".
           05  DATE-COMPILE           PIC 99/99/9999.

       COPY "rminfo.cpy".
       COPY "sysinfo.cpy".
       COPY "printinf.cpy".

       01  RM-LINE.
           05  RM-USER                 PIC X(9)  VALUE "User:    ".
           05  RM-SUB-LINE.
               10  FILLER              PIC X(16) VALUE
               " using RM/COBOL ".
               10  RM-LINE-VersionNumber
                                       PIC X(8).
               10  FILLER              PIC X(1)  VALUE "(".
               10  RM-Line-RegistrationNumber PIC X(18).
               10  FILLER              PIC X(1)  VALUE ")".
               10  FILLER              PIC X(12) VALUE SPACES.

       01  PRT-LINE.
           05  FILLER                 PIC X(9)  VALUE "Printer: ".
           05  Prt-Line-PrinterName   PIC X(71).

       01  UNDER-LINE.
           05  FILLER                  PIC X(80) VALUE ALL "-".

       01  WS-HEADING-TABS.
           03  FILLER          PIC X(15)   VALUE SPACES.
           03  FILLER          PIC X(19)   VALUE "TAB STOPS set to 10".
           03  FILLER          PIC X(05)   VALUE SPACES.
           03  FILLER          PIC X(26)   VALUE
                                           "TOP MARGIN set to 6 lines".
           03  FILLER          PIC X(10)   VALUE SPACES.
       01  WS-HEADING-STYLES.
           03  FILLER          PIC X(15)   VALUE "RM STYLE ".
           03  FILLER          PIC X(07)   VALUE " Style".
           03  FILLER          PIC X(01)   VALUE SPACES.
           03  FILLER          PIC X(06)   VALUE "Normal".
           03  FILLER          PIC X(42)   VALUE SPACES.

       78 RM-Style-Normal      VALUE x'1B28730053'.
       78 RM-Style-Italic      VALUE x'1B28730153'.
       78 RM-Style-Bold        VALUE x'1B28730253'.
       78 RM-Style-Underline   VALUE x'1B28730453'.
       78 RM-Style-Compressed  VALUE x'1B28730853'.
       78 RM-Style-Expanded    VALUE x'1B28731053'.

       01  RM-ESCAPE-SEQUENCES.
           03  RM-RESET       PIC X(2) VALUE x'1B45'.
           03  RM-PAPER-SOURCE.
               05             PIC X(3) VALUE x'1B266C'.
               05             PIC X(1).
                  88 DMBIN-ONLYONE     VALUE x'01'.
                  88 DMBIN-LOWER       VALUE x'02'.
                  88 DMBIN-MIDDLE      VALUE x'03'.
                  88 DMBIN-MANUAL      VALUE x'04'.
                  88 DMBIN-ENVELOPE    VALUE x'05'.
                  88 DMBIN-ENVMANUAL   VALUE x'06'.
                  88 DMBIN-AUTO        VALUE x'07'.
                  88 DMBIN-TRACTOR     VALUE x'08'.
                  88 DMBIN-SMALLFMT    VALUE x'09'.
                  88 DMBIN-LARGEFMT    VALUE x'0A'.
                  88 DMBIN-LARGECAPACITY VALUE x'0B'.
                  88 DMBIN-CASSETTE    VALUE x'0E'.
                  88 DMBIN-FORMSOURCE  VALUE x'0F'.
               05             PIC X(1) VALUE x'48'.
           03  RM-ORIENTATION.
               05             PIC X(3) VALUE x'1B266C'.
               05             PIC X(1).
                  88 OrientationIsPortrait  VALUE x'00'.
                  88 OrientationIsLandscape VALUE x'01'.
               05             PIC X(1) VALUE x'4F'.
           03  RM-CLEAR-MARGINS        PIC X(2) VALUE x'1B39'.
           03  RM-TOP-MARGIN.
               05             PIC X(3) VALUE x'1B266C'.
               05             PIC X(1).
                  88 4-Lines           VALUE x'04'.
                  88 5-Lines           VALUE x'05'.
                  88 6-Lines           VALUE x'06'.
               05             PIC X(1) VALUE x'45'.
           03  RM-TEXT-LENGTH.
               05             PIC X(3) VALUE x'1B266C'.
               05             PIC X(1).
                  88 18-Text-Lines     VALUE x'18'.
                  88 20-Text-Lines     VALUE x'20'.
                  88 36-Text-Lines     VALUE x'24'.
                  88 40-Text-Lines     VALUE x'28'.
               05             PIC X(1) VALUE x'46'.
           03  RM-LEFT-MARGIN.
               05             PIC X(3) VALUE x'1B2661'.
               05             PIC X(1).
                  88  8-Columns        VALUE x'08'.
                  88 10-Columns        VALUE x'0A'.
                  88 12-Columns        VALUE x'0C'.
                  88 20-Columns        VALUE x'14'.
               05             PIC X(1) VALUE x'4C'.
           03  RM-LINE-SPACING.
               05             PIC X(3) VALUE x'1B266C'.
               05             PIC X(1).
                  88 4-LPI             VALUE x'04'.
                  88 6-LPI             VALUE x'06'.
                  88 8-LPI             VALUE x'08'.
               05             PIC X(1) VALUE x'44'.
           03  RM-PRINT-PITCH.
               05             PIC X(3) VALUE x'1B2873'.
               05             PIC X(1).
                  88 10-CPI            VALUE x'0A'.
                  88 12-CPI            VALUE x'0C'.
                  88 16-CPI            VALUE x'10'.
                  88 20-CPI            VALUE x'14'.
               05             PIC X(1) VALUE x'48'.
           03  RM-STYLE.
               05             PIC X(3) VALUE x'1B2873'.
               05             PIC X(1).
                  88 Style-IsNormal      VALUE x'00'.
                  88 Style-IsItalic      VALUE x'01'.
                  88 Style-IsBold        VALUE x'02'.
                  88 Style-IsUnderline   VALUE x'04'.
                  88 Style-IsCompressed  VALUE x'08'.
                  88 Style-IsExpanded    VALUE x'10'.
               05             PIC X(1) VALUE x'53'.
           03  RM-NULL        PIC X(1) VALUE x'00'.
           03  RM-TAB-STOPS.
               05             PIC X(3) VALUE x'1B266B'.
               05             PIC X(1).
                  88 3-Tab             VALUE x'03'.
                  88 5-Tab             VALUE x'05'.
                  88 10-Tab            VALUE x'0A'.
                  88 11-Tab            VALUE x'0B'.
               05             PIC X(1) VALUE x'48'.
           03  RM-HOZ-TAB     PIC x(1) VALUE x'09'.
           03  RM-SHIFT-OUT   PIC x(1) VALUE x'0E'.
           03  RM-SHIFT-IN    PIC x(1) VALUE x'0F'.

       01 WS-RULER            PIC X(80)  VALUE ALL "1234567890".
       01 WS-ABC              PIC X(5)   VALUE ALL "ABCDE".
       01 WS-RULER132         PIC X(132) VALUE ALL "1234567890".
      *
      *    Define Device Information Definition
      *
       77 I                                    Picture 99 Value 80.
       01 DefineDeviceInformation.
          02  DDI-DeviceName                   Picture X(80).
          02  DDI-PortName                     Picture X(10).
          02  DDI-FontName                     Picture X(80).
          02  DDI-PointSize                    Picture 9(5) BINARY (2).
          02  DDI-RawModeValue                 Picture X.
              88  DDI-RawMode                    Value 'Y'
                                            When False 'N'.
          02  DDI-EscapeModeValue              Picture X.
              88  DDI-EscapeMode                 Value 'Y'
                                            When False 'N'.

       01  Ws-DefineDevice-Stat                Picture 9(1) Value 0.
           88  No-Define-Device                  Value 0.

       01  Define-Pointer                      Picture 9(3) Value 1.
       01  Ws-Overflow-Flag                    Picture X Value 'N'.
           88 Overflow-Flag                       Value 'Y'.
       01  Ws-Character-Found                  Picture X Value 'N'.
           88 Character-Found                     Value 'Y'
                                             When False 'N'.
       01  Ws-Pointsize                         Picture Z9.

       PROCEDURE DIVISION.
       DRIVER-SECTION SECTION.
      ** Main driver paragraph **
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 2 LINES.
           CLOSE PFILE.
           STOP RUN.

      ** Prepare heading information **
       INITIAL-SETUP.
           OPEN OUTPUT PFILE.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           ACCEPT DOW-SUB       FROM DAY-OF-WEEK.
           MOVE DAY-OF-THE-WEEK(DOW-SUB)   TO DOW.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.
           COMPUTE DATE-COMPILE = DATE-COMP * DATE-CONVERT.

      ** Print heading **
       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1      AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2      AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE   AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "C$SetDevelopmentMode".
           CALL "C$GetRMInfo"      USING RMInformation.
           CALL "C$GetSysInfo"     USING SystemInformation.
           CALL "P$GetPrinterInfo" USING PrinterInformation.

           MOVE  RM-VersionNumber      TO RM-Line-VersionNumber.
           MOVE  RM-RegistrationNumber TO RM-Line-RegistratioNnumber.
           MOVE  PI-PrinterName        TO Prt-Line-PrinterName.

           STRING RM-USER       Delimited by Size,
                  Sys-UserName  Delimited by Spaces,
                  RM-SUB-LINE   Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           STRING "Machine: "   Delimited by Size,
                  Sys-NodeName  Delimited by Spaces,
                  " running "   Delimited by Size,
                  Sys-Name      Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM PRT-LINE   AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "P$GetDefineDeviceInfo" Using DefineDeviceInformation
                                        Giving Ws-DefineDevice-Stat.
           If No-Define-Device
               Write Print-Record from "Define-Device: None "
           Else
               Perform Define-Device-Line
           End-If.

           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

       DEFINE-DEVICE-LINE.
           Move "Define-Device: "  To Print-Record.
           Move 16                 To Define-Pointer.

           If DDI-DeviceName Not Equal Spaces
               Set Character-Found To False
               Perform Varying I from 80 by -1 Until Character-Found
                   If DDI-DeviceName (I:1) Not Equal Spaces
                       Set Character-Found To True
                   End-If
               End-Perform
               If I < 80
                   Add 1 To I
               End-If
               String DDI-DeviceName(1:I) Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow DeviceName " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           If Not OverFlow-Flag
               Perform String-Comma.

           If DDI-PortName Not Equal Spaces And Not OverFlow-Flag
               String "Port="         Delimited by Size,
                      DDI-PortName    Delimited by Spaces,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow PortName " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           If Not OverFlow-Flag
               Perform String-Comma.

           If DDI-FontName Not Equal Spaces And Not OverFlow-Flag
               Set Character-Found To False
               Perform Varying I from 80 by -1 Until Character-Found
                   If DDI-FontName (I:1) Not Equal Spaces
                       Set Character-Found To True
                   End-If
               End-Perform
               If I < 80
                   Add 1 To I
               End-If
               String "Font="                Delimited by Size,
                       DDI-FontName(1:I)     Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow FontName " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           If Not OverFlow-Flag
               Perform String-Comma.

           If DDI-PointSize Not Equal Zeros And Not OverFlow-Flag
               Move DDI-PointSize to Ws-PointSize
               String "Size="         Delimited by Size,
                      Ws-PointSize    Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow PointSize " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           IF  DDI-RawMode And Not OverFlow-Flag
               String ",RAW"         Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow RAW " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           IF  DDI-EscapeMode And Not OverFlow-Flag
               String ",ESC"         Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow ESC " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           Write Print-Record.
           Move Spaces To Print-Record.

       STRING-COMMA.
           String ","             Delimited by Size
               Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow Comma " Define-Pointer
                   Set Overflow-Flag To True
           End-String.

       MAIN SECTION.
       TEST-1.
      *
           CALL "P$EnableEscapeSequences".

           PERFORM TAB-TESTS.
           PERFORM STYLE-TESTS.
           PERFORM LEFT-MARGIN-TESTS.
           PERFORM ORIENTATION-SHIFT-TESTS.
           WRITE PRINT-RECORD FROM RM-RESET.

       ESCAPE-TEST SECTION.
       TAB-TESTS.
           SET 6-LINES        TO TRUE.
           SET DMBIN-ONLYONE  TO TRUE.
           SET 40-TEXT-LINES  TO TRUE.
           STRING RM-PAPER-SOURCE Delimited by Size,
                  RM-TOP-MARGIN   Delimited by Size,
                  RM-TEXT-LENGTH  Delimited by Size
           INTO PRINT-RECORD.
           WRITE PRINT-RECORD AFTER 1.

           WRITE PRINT-RECORD FROM WS-HEADING-TABS AFTER PAGE.
           WRITE PRINT-RECORD FROM WS-RULER AFTER 1.
           MOVE SPACES TO PRINT-RECORD.
           SET 10-TAB TO TRUE.
           WRITE PRINT-RECORD FROM RM-TAB-STOPS AFTER 1.
           String  "TAB STOP"            Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   "TAB STOP"            Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   "TAB STOP"            Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   "TAB STOP"            Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   "TAB STOP"            Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   "TAB STOP"            Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   "TAB STOP"            Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   "TAB STOP"            Delimited by size
                   RM-HOZ-TAB            Delimited by size
           into print-record.
           WRITE PRINT-RECORD AFTER 1.

       STYLE-TESTS.
           WRITE PRINT-RECORD FROM WS-HEADING-STYLES AFTER 3.
           MOVE SPACES TO PRINT-RECORD.
           SET 5-TAB TO TRUE.
           WRITE PRINT-RECORD FROM RM-TAB-STOPS AFTER 1.
           MOVE SPACES TO PRINT-RECORD.
           String "Italic    "           Delimited by size
                   RM-Style-Italic       Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   WS-ABC                Delimited by size
                   RM-Style-Normal       Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   WS-ABC                Delimited by size
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD AFTER 1.

           MOVE SPACES TO PRINT-RECORD.
           String "Bold      "           Delimited by size
                   RM-Style-Bold         Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   WS-ABC                Delimited by size
                   RM-Style-Normal       Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   WS-ABC                Delimited by size
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD AFTER 1.

           MOVE SPACES TO PRINT-RECORD.
           String "Underline"            Delimited by size
                   RM-Style-Underline    Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   WS-ABC                Delimited by size
                   RM-Style-Normal       Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   WS-ABC                Delimited by size
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD AFTER 1.

           MOVE SPACES TO PRINT-RECORD.
           String "Compressed  "         Delimited by size
                   RM-Style-Compressed   Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   WS-ABC                Delimited by size
                   RM-Style-Normal       Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   WS-ABC                Delimited by size
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD AFTER 1.

           MOVE SPACES TO PRINT-RECORD.
           String "Expanded   "          Delimited by size
                   RM-Style-Expanded     Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   WS-ABC                Delimited by size
                   RM-Style-Normal       Delimited by size
                   RM-HOZ-TAB            Delimited by size
                   WS-ABC                Delimited by size
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD AFTER 1.

           MOVE SPACES TO PRINT-RECORD.
           WRITE PRINT-RECORD FROM RM-RESET.


       LEFT-MARGIN-TESTS.
           WRITE PRINT-RECORD FROM WS-RULER AFTER 2.
           SET 12-Columns TO TRUE.
           WRITE PRINT-RECORD FROM RM-LEFT-MARGIN.
           WRITE PRINT-RECORD FROM "LEFT-MARGIN Should be column 12".
           MOVE SPACES TO PRINT-RECORD.

           SET 16-CPI TO TRUE.
           WRITE PRINT-RECORD FROM RM-PRINT-PITCH.
           WRITE PRINT-RECORD FROM "PRINT PITCH IS 16-CPI ".
           MOVE SPACES TO PRINT-RECORD.

           SET 20-CPI TO TRUE.
           WRITE PRINT-RECORD FROM RM-PRINT-PITCH.
           WRITE PRINT-RECORD FROM "PRINT PITCH IS 20-CPI ".
           MOVE SPACES TO PRINT-RECORD.

           SET 10-CPI TO TRUE.
           WRITE PRINT-RECORD FROM RM-PRINT-PITCH.
           WRITE PRINT-RECORD FROM "PRINT PITCH IS 10-CPI ".
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM RM-CLEAR-MARGINS.
           WRITE PRINT-RECORD FROM "MARGINS RESET ".
           MOVE SPACES TO PRINT-RECORD.

           SET 4-LPI TO TRUE.
           WRITE PRINT-RECORD FROM RM-LINE-SPACING.
           PERFORM 5 TIMES
               WRITE PRINT-RECORD FROM "4 Lines Per Inch "
           END-PERFORM.

       ORIENTATION-SHIFT-TESTS.
           SET OrientationIsLandscape TO TRUE.
           WRITE PRINT-RECORD FROM RM-ORIENTATION.
           WRITE PRINT-RECORD FROM WS-RULER132 AFTER PAGE.
           MOVE SPACES TO PRINT-RECORD.
           WRITE PRINT-RECORD FROM "This page is oriented landscape ".
           MOVE SPACES TO PRINT-RECORD.

           STRING "Shift out (Expanded) Line " Delimited by Size,
                  RM-Shift-Out Delimited by Size,
                  WS-RULER     Delimited by Size
           INTO PRINT-RECORD.
           Write print-record after 2.
           Write print-record from "Normal Line " after 1.
           STRING "Shift In (Compressed) Line " Delimited by Size,
                  RM-Shift-In Delimited by Size,
                  WS-RULER   Delimited by Size
           INTO PRINT-RECORD.
           Write print-record after 1.
           Write print-record from "Normal Line " after 1.

           Move Spaces To Print-Record.
           STRING "Normal" Delimited by Size,
                  RM-Shift-Out Delimited by Size,
                  "Shift Out Expanded" delimited by Size,
                  RM-Shift-In Delimited by Size,
                  "Shift In Normal" delimited by Size,
                  RM-Shift-In Delimited by Size,
                  "Shift In Compressed" delimited by Size,
                  RM-Shift-Out Delimited by Size,
                  "Shift Out Normal" delimited by Size,
           INTO PRINT-RECORD.
           Write print-record after 2.

           SET OrientationIsPortrait TO TRUE.
           WRITE PRINT-RECORD FROM RM-ORIENTATION.
           WRITE PRINT-RECORD FROM WS-RULER AFTER PAGE.
           MOVE SPACES TO PRINT-RECORD.
           WRITE PRINT-RECORD FROM "This page is oriented Portrait ".
           MOVE SPACES TO PRINT-RECORD.

       END PROGRAM  PESCAPE.
