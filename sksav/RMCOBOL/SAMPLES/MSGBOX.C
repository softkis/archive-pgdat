/*
 *	Title:	msgbox.c
 *		RM/COBOL Message Box Sample DLL
 *
 *	Copyright (c) 1999 Liant Software Corporation.
 *
 *	You have a royalty-free right to use, modify, reproduce, and
 *	distribute this C source file (and/or any modified version)
 *	in any way you find useful, provided that you retain this notice
 *	and agree that Liant has no warranty, obligations, or liability
 *	for any such use of the source file.
 *
 *	Version = @(#) $Revision:   6.6.1.0  $ $Date:   23 Sep 1999 15:53:50  $
 */

#include <windows.h>

#define RMLittleEndian 1

#include "rmc85cal.h"

int __declspec(dllexport)  __cdecl
MsgBox (char *Name, WORD ArgCount, ARGUMENT_ENTRY *ArgEntry, WORD State)

{
    short sButton;
    long lButton;
    char Buf[64];
    short i;
    char *p;
    short n;

    if (ArgCount != 2)
	return (RM_STOP);

    /* -- check arguments */
    switch (ArgEntry[0].a_type)
    {
	/* -- various displayable types */
	case RM_ANS:	case RM_ANSR:
	case RM_ABS:	case RM_ABSR:
	case RM_NSE:
	case RM_GRPF:
	    break;

	default:
	    return (RM_STOP);
    }

    switch (ArgEntry[1].a_type)
    {
	/* -- only return binary types */
	case RM_NBS: case RM_NBU:
	    break;

	default:
	    return (RM_STOP);
    }

    p = ArgEntry[0].a_address;
    n = (short) ArgEntry[0].a_length;
    for (i = 0; i < n; i++)
	Buf[i] = *p++;
    Buf[i] = '\0';

    lButton =
    sButton = MessageBox (0, Buf, NULL, MB_YESNO |
					MB_ICONQUESTION |
					MB_SETFOREGROUND);

    /* -- return value in second argument */
    p = ArgEntry[1].a_address;
    if (ArgEntry[1].a_length > sizeof(short))
	STLONG (lButton, p);
    else
	STSHORT (sButton, p);
    return (RM_FND);
}
