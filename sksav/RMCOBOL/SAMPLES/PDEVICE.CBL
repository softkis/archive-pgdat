       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PDEVICE.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION:
      *
      * PDEVICE -  Calls P$GetDeviceCapabilities with individual and
      *            group calls.
      *
      * Version Identification:
      *   $Revision:   1.2.1.1  $
      *   $Date:   27 Sep 1999 17:13:46  $
      *
      *********************************************************************

       DATE-WRITTEN.     01/22/1999

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
       SPECIAL-NAMES.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PRINTER  ASSIGN TO PRINT, "PRINTER?".
       DATA DIVISION.
       FILE SECTION.
       FD  PRINTER.
       01  FD-RECORD       PIC X(80).
       01  PRINT-RECORD    PIC X(80).

       WORKING-STORAGE SECTION.
       77  DATE-CONVERT                PIC 9(5)V9(4) VALUE 10000.0001.
       77  WS-SUB                      PIC 99        VALUE 0.
       77  Units-Per-Inch              PIC 9(5)      VALUE 0.
       77  Millimeters-Per-Inch        PIC 9(5)V9(2) VALUE 25.4.
       78  DATE-COMP                   VALUE IS DATE-COMPILED.

       01  CHECK-FOR-ERRS              PIC 9  VALUE 0.
           88  NO-ERRS-ENCOUNTERED     VALUE 0.
           88  ERRS-ENCOUNTERED        VALUE 1.

       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(31) VALUE
           "RUNTIME RESULTS - PDEVICE   ".

      *  Modify the following output line to describe the test.
       01  DESCRIP-LINE.
           05  FILLER                  PIC X(25)  VALUE
           "**   PDEVICE  - Test For ".
           05  FILLER                  PIC X(34) VALUE
           "Call To P$GetDeviceCapabilities **".
      *     1234567890123456789012345678901234567890

       01  DOW-SUB                    PIC 9(1)  VALUE ZERO.
       01  DAY-OF-WEEK-TAB            PIC X(21) VALUE
           "MonTueWedThuFriSatSun".
       01  DAY-OF-WEEK-RED REDEFINES DAY-OF-WEEK-TAB.
           05  DAY-OF-THE-WEEK        PIC X(3) OCCURS 7 TIMES.

       01  TIME-DATE-LINE.
           05  FILLER                 PIC X(6)  VALUE "Date: ".
           05  MM                     PIC XX.
           05  SLASH-1                PIC X     VALUE "/".
           05  DD                     PIC XX.
           05  SLASH-2                PIC X     VALUE "/".
           05  YY                     PIC X(5).
           05  FILLER                 PIC X     VALUE "(".
           05  DOW                    PIC X(3).
           05  FILLER                 PIC X     VALUE ")".
           05  FILLER                 PIC X(13) VALUE "   Run Time: ".
           05  HH                     PIC XX.
           05  DIV-1                  PIC X     VALUE ":".
           05  MN                     PIC XX.
           05  DIV-2                  PIC X     VALUE ":".
           05  SS                     PIC XX.
           05  FILLER                 PIC X(5).
           05  FILLER                 PIC X(15) VALUE "Date Compiled: ".
           05  DATE-COMPILE           PIC 99/99/9999.

       COPY "rminfo.cpy".
       COPY "sysinfo.cpy".
       COPY "printinf.cpy".

       01  RM-LINE.
           05  RM-USER                 PIC X(9)  VALUE "User:    ".
           05  RM-SUB-LINE.
               10  FILLER              PIC X(16) VALUE
               " using RM/COBOL ".
               10  RM-LINE-VersionNumber
                                       PIC X(8).
               10  FILLER              PIC X(1)  VALUE "(".
               10  RM-Line-RegistrationNumber PIC X(18).
               10  FILLER              PIC X(1)  VALUE ")".
               10  FILLER              PIC X(12) VALUE SPACES.

       01  PRT-LINE.
           05  FILLER                 PIC X(9)  VALUE "Printer: ".
           05  Prt-Line-PrinterName   PIC X(71).

       01  UNDER-LINE.
           05  FILLER                  PIC X(80) VALUE ALL "-".

       01  ERROR-LINE.
           05  FILLER                  PIC X(61) VALUE SPACES.
           05  FILLER                  PIC X(19) VALUE
           " ERRORS ENCOUNTERED".

       01  NO-ERROR-LINE.
           05  FILLER                  PIC X(59) VALUE SPACES.
           05  FILLER                  PIC X(21) VALUE
           "NO ERRORS ENCOUNTERED".

       01  WS-INDIVIDUAL-LINE          PIC X(80) VALUE
           "Calling P$GetDeviceCapabilities with Individual parameters".
       01  WS-GROUP-LINE               PIC X(80) VALUE
           "Calling P$GetDeviceCapabilities with Group parameter".
       01  WS-HEAD-LINE.
           05                          PIC X(40) VALUE SPACES.
           05                          PIC X(40) VALUE
           "Device Units   Inches     Millimeters   ".

       01 WS-A-LINE.
          03  WS-A-PARM              PIC X(21).
          03                         PIC X(3)  VALUE " = ".
          03  WS-A-VALUE             PIC Z(9)9.
          03                         PIC X(2).
          03  WS-A-TEXT              PIC X(44).
          03  WS-A-CONVERT REDEFINES WS-A-TEXT.
              05 WS-A-ERROR          PIC X(2).
              05                     PIC X(1).
              05 WS-A-UNITS          PIC Z(9)9.
              05                     PIC X(5).
              05 WS-A-INCHES         PIC Z(3).9(3).
              05                     PIC X(6).
              05 WS-A-MILLIMETERS    PIC Z(5).9(3).
              05                     PIC X(4).

       copy "devcaps.cpy".
      *
      *    Parameter Name Values
      *
       01 WS-DEVCAP-PARAMETERS.
         02 WS-DC-NUMERIC  PIC 9(10) BINARY VALUE 0.
         02 WS-DC-NUMERICS.
            03             PIC X(21) Value DC-DriverVersionParam.
            03             PIC X(21) Value DC-TechnologyParam.
            03             PIC X(21) Value DC-HorizontalSizeParam.
            03             PIC X(21) Value DC-VerticalSizeParam.
            03             PIC X(21) Value DC-HorizontalResolutionParam.
            03             PIC X(21) Value DC-VerticalResolutionParam.
            03             PIC X(21) Value DC-LogicalPixelsXParam.
            03             PIC X(21) Value DC-LogicalPixelsYParam.
            03             PIC X(21) Value DC-AspectXParam.
            03             PIC X(21) Value DC-AspectYParam.
            03             PIC X(21) Value DC-AspectXYParam.
            03             PIC X(21) Value DC-PhysicalWidthParam.
            03             PIC X(21) Value DC-PhysicalHeightParam.
            03             PIC X(21) Value DC-PhysicalOffsetXParam.
            03             PIC X(21) Value DC-PhysicalOffsetYParam.
            03             PIC X(21) Value DC-ScalingFactorXParam.
            03             PIC X(21) Value DC-ScalingFactorYParam.
         02 WS-DC-NUM REDEFINES WS-DC-NUMERICS OCCURS 17 TIMES.
           03 WS-DC-N      PIC X(21).
         02 WS-DC-TEXT-DESCRIPTION.
           03 PIC X(42) Value "Device driver version                  ".
           03 PIC X(42) Value "Device technology                      ".
           03 PIC X(42) Value "Width of printable area in millimeters ".
           03 PIC X(42) Value "Height of printable area in millimeters".
           03 PIC X(42) Value "Width of printable area in dots        ".
           03 PIC X(42) Value "Height of printable area in dots       ".
           03 PIC X(42) Value "Horizontal dots-per-inch               ".
           03 PIC X(42) Value "Vertical dots-per-inch                 ".
           03 PIC X(42) Value "Length of horizontal sides of a square ".
           03 PIC X(42) Value "Length of vertical sides of same square".
           03 PIC X(42) Value "Length of diagonal line of same square ".
           03 PIC X(42) Value "Width of page area in device units     ".
           03 PIC X(42) Value "Height of page area in device units    ".
           03 PIC X(42) Value
                           "Length of unprintable area in device units".
           03 PIC X(42) Value
                           "Height of unprintable area in device units".
           03 PIC X(42) Value "X-axis scale factor                    ".
           03 PIC X(42) Value "Y-axis scale factor                    ".
         02 WS-DC-TXT REDEFINES WS-DC-TEXT-DESCRIPTION OCCURS 17 TIMES.
           03 WS-DC-TEXT   PIC X(42).

        01 WS-DC-Verify.
           02  DC-Verify           PIC 9(10) Binary(4) Occurs 17 Times.
      *
       PROCEDURE DIVISION.
       DRIVER-SECTION SECTION.
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           PERFORM WRAP-UP-SUMMARY.
           CLOSE PRINTER,
           STOP RUN.

      ** Prepare heading information **
       INITIAL-SETUP.
           OPEN OUTPUT PRINTER.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           ACCEPT DOW-SUB       FROM DAY-OF-WEEK.
           MOVE DAY-OF-THE-WEEK(DOW-SUB)   TO DOW.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.
           COMPUTE DATE-COMPILE = DATE-COMP * DATE-CONVERT.

      ** Print heading **
       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1      AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2      AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE   AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "C$SetDevelopmentMode".
           CALL "C$GetRMInfo"      USING RMInformation.
           CALL "C$GetSysInfo"     USING SystemInformation.
           CALL "P$GetPrinterInfo" USING PrinterInformation.

           MOVE  RM-VersionNumber      TO RM-Line-VersionNumber.
           MOVE  RM-RegistrationNumber TO RM-Line-RegistratioNnumber.
           MOVE  PI-PrinterName        TO Prt-Line-PrinterName.

           STRING RM-USER       Delimited by Size,
                  Sys-UserName  Delimited by Spaces,
                  RM-SUB-LINE   Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           STRING "Machine: "   Delimited by Size,
                  Sys-NodeName  Delimited by Spaces,
                  " running "   Delimited by Size,
                  Sys-Name      Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM PRT-LINE   AFTER 1 LINES.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

      ** Write last summary line **
       WRAP-UP-SUMMARY.
           IF  NO-ERRS-ENCOUNTERED
               WRITE PRINT-RECORD FROM NO-ERROR-LINE AFTER 2 LINES
               PERFORM DISPLAY-END
           ELSE
               WRITE PRINT-RECORD FROM ERROR-LINE AFTER 2 LINES
               PERFORM DISPLAY-ABEND.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER ADVANCING 1 LINES.

      ** Display no errors message **
       DISPLAY-END.
           DISPLAY "END OF  PDEVICE TEST"
               LINE 5 POSITION 10 ERASE.
           DISPLAY "NO ERRORS ENCOUNTERED"
               LINE 7 POSITION 10.

      ** Display errors message **
       DISPLAY-ABEND.
           DISPLAY "END OF PDEVICE TEST"
               LINE 5 POSITION 10 ERASE.
           DISPLAY "ERRORS ENCOUNTERED" LINE 7 POSITION 13.

       MAIN SECTION.
       TEST-1.
      *
           WRITE PRINT-RECORD FROM WS-INDIVIDUAL-LINE AFTER 2.
           WRITE PRINT-RECORD FROM SPACES AFTER 2.

           PERFORM VARYING WS-SUB FROM 1 BY 1 UNTIL WS-SUB > 17
               CALL "P$GetDeviceCapabilities" USING WS-DC-N(WS-SUB),
                                                    WS-DC-NUMERIC
               END-CALL
               MOVE WS-DC-N(WS-SUB)        TO WS-A-PARM
               MOVE WS-DC-NUMERIC          TO WS-A-VALUE,
                                              DC-VERIFY(WS-SUB)
               MOVE WS-DC-TEXT(WS-SUB)     TO WS-A-TEXT
               WRITE FD-RECORD FROM WS-A-LINE
           END-PERFORM.
           MOVE SPACES TO WS-A-TEXT.

           WRITE PRINT-RECORD FROM WS-GROUP-LINE AFTER 2.
           WRITE PRINT-RECORD FROM SPACES AFTER 1.
           WRITE PRINT-RECORD FROM WS-HEAD-LINE.

           MOVE 1 TO WS-SUB.
           CALL "P$GetDeviceCapabilities" USING DeviceCapabilities.
           Move  DC-LogPixelsX                  To Units-Per-Inch.
           Move  DC-DriverVersionParam          To WS-A-Parm.
           Move  DC-DriverVersion               To WS-A-VALUE.
           IF DC-DriverVersion Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           WRITE FD-RECORD FROM WS-A-LINE.
           MOVE SPACES TO WS-A-CONVERT.

           Move  DC-TechnologyParam             To WS-A-Parm.
           Move  DC-TechnologyValue             To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-TechnologyValue Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           WRITE FD-RECORD FROM WS-A-LINE.
           MOVE SPACES TO WS-A-CONVERT.

           Move  DC-HorizontalSizeParam         To WS-A-Parm.
           Move  DC-HorzSize                    To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-HorzSize Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-HorzSize                    To WS-A-Millimeters.
           Divide DC-HorzSize by Millimeters-Per-Inch Giving WS-A-Inches.
           Compute Ws-A-Units = (DC-HorzSize / Millimeters-Per-Inch)
                                * Units-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-VerticalSizeParam           To WS-A-Parm.
           Move  DC-VertSize                    To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-VertSize Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-VertSize                    To WS-A-Millimeters.
           Divide Dc-VertSize by Millimeters-Per-Inch Giving WS-A-Inches.
           Compute Ws-A-Units = (DC-VertSize / Millimeters-Per-Inch)
                                * Units-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-HorizontalResolutionParam   To WS-A-Parm.
           Move  DC-HorzRes                     To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-HorzRes Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-HorzRes                     To WS-A-Units.
           Divide DC-HorzRes By Units-Per-Inch Giving WS-A-Inches.
           Compute WS-A-Millimeters = (DC-HorzRes / Units-Per-Inch) *
                                      Millimeters-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-VerticalResolutionParam     To WS-A-Parm.
           Move  DC-VertRes                     To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-VertRes Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-VertRes                     To WS-A-Units.
           Divide DC-VertRes By Units-Per-Inch Giving WS-A-Inches.
           Compute WS-A-Millimeters = (DC-VertRes / Units-Per-Inch) *
                                      Millimeters-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-LogicalPixelsXParam         To WS-A-Parm.
           Move  DC-LogPixelsX                  To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-LogPixelsX Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-LogPixelsX                  To WS-A-Units.
           Divide DC-LogPixelsX By Units-Per-Inch Giving WS-A-Inches.
           Compute WS-A-Millimeters = (DC-LogPixelsX / Units-Per-Inch) *
                                      Millimeters-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-LogicalPixelsYParam         To WS-A-Parm.
           Move  DC-LogPixelsY                  To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-LogPixelsY Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-LogPixelsY                  To WS-A-Units.
           Divide DC-LogPixelsY By Units-Per-Inch Giving WS-A-Inches.
           Compute WS-A-Millimeters = (DC-LogPixelsY / Units-Per-Inch) *
                                      Millimeters-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-AspectXParam                To WS-A-Parm.
           Move  DC-AspectX                     To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-AspectX Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-AspectX                  To WS-A-Units.
           Divide DC-AspectX By Units-Per-Inch Giving WS-A-Inches.
           Compute WS-A-Millimeters = (DC-AspectX / Units-Per-Inch) *
                                      Millimeters-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-AspectYParam                To WS-A-Parm.
           Move  DC-AspectY                     To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-AspectY Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-AspectY                  To WS-A-Units.
           Divide DC-AspectY By Units-Per-Inch Giving WS-A-Inches.
           Compute WS-A-Millimeters = (DC-AspectY / Units-Per-Inch) *
                                      Millimeters-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-AspectXYParam               To WS-A-Parm.
           Move  DC-AspectXY                    To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-AspectXY Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-AspectXY                  To WS-A-Units.
           Divide DC-AspectXY By Units-Per-Inch Giving WS-A-Inches.
           Compute WS-A-Millimeters = (DC-AspectXY / Units-Per-Inch) *
                                      Millimeters-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-PhysicalWidthParam          To WS-A-Parm.
           Move  DC-PhysicalWidth               To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-PhysicalWidth Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-PhysicalWidth              To WS-A-Units.
           Divide DC-PhysicalWidth By Units-Per-Inch
               Giving WS-A-Inches.
           Compute WS-A-Millimeters =
               (DC-PhysicalWidth / Units-Per-Inch) *
                Millimeters-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-PhysicalHeightParam         To WS-A-Parm.
           Move  DC-PhysicalHeight              To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-PhysicalHeight Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-PhysicalHeight              To WS-A-Units.
           Divide DC-PhysicalHeight By Units-Per-Inch
               Giving WS-A-Inches.
           Compute WS-A-Millimeters =
               (DC-PhysicalHeight / Units-Per-Inch) *
                Millimeters-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-PhysicalOffsetXParam        To WS-A-Parm.
           Move  DC-PhysicalOffsetX             To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-PhysicalOffsetX Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-PhysicalOffsetX              To WS-A-Units.
           Divide DC-PhysicalOffsetX By Units-Per-Inch
               Giving WS-A-Inches.
           Compute WS-A-Millimeters =
               (DC-PhysicalOffsetX / Units-Per-Inch) *
                Millimeters-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-PhysicalOffsetYParam        To WS-A-Parm.
           Move  DC-PhysicalOffsetY             To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-PhysicalOffsetY Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           Move  DC-PhysicalOffsetY              To WS-A-Units.
           Divide DC-PhysicalOffsetY By Units-Per-Inch
               Giving WS-A-Inches.
           Compute WS-A-Millimeters =
               (DC-PhysicalOffsetY / Units-Per-Inch) *
                Millimeters-Per-Inch.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-ScalingFactorXParam         To WS-A-Parm.
           Move  DC-ScalingFactorX              To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-ScalingFactorX Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           Move  DC-ScalingFactorYParam         To WS-A-Parm.
           Move  DC-ScalingFactorY              To WS-A-VALUE.
           ADD 1 TO WS-SUB.
           IF DC-ScalingFactorY Not Equal DC-Verify(Ws-SUB)
               Set Errs-Encountered To True
               MOVE "**" TO WS-A-ERROR.
           WRITE FD-RECORD FROM WS-A-LINE.
           Move Spaces To WS-A-CONVERT.

           If Ws-DC-Verify Not Equal DeviceCapabilities
               Set Errs-Encountered To True.

       END PROGRAM  PDEVICE.
