       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PTEXTMET.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION:
      *
      * PTEXTMET -  Test CALL to P$GetTextMetrics
      *
      * Version Identification:
      *   $Revision:   1.4.1.2  $
      *   $Date:   27 Sep 1999 17:19:12  $
      *
      *********************************************************************

       DATE-WRITTEN.     01/22/1999

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PRINTER ASSIGN TO PRINT, "PRINTER?".
       DATA DIVISION.
       FILE SECTION.
       FD  PRINTER.
       01  PRINT-RECORD                PIC X(80).
       WORKING-STORAGE SECTION.
       77  DATE-CONVERT                PIC 9(5)V9(4) VALUE 10000.0001.
       77  WS-SUB                      PIC 99        VALUE 0.
       77  UNITS-PER-INCH              PIC 9(5)      VALUE 0.
       78  DATE-COMP                   VALUE IS DATE-COMPILED.

       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(31) VALUE
           "RUNTIME RESULTS - PTEXTMET   ".

       01  DESCRIP-LINE.
           05  FILLER                  PIC X(26) VALUE
           "**   PTEXTMET  - Test For ".
           05  FILLER                  PIC X(27) VALUE
           "Call To P$GetTextMetrics **".

       01  DOW-SUB                    PIC 9(1)  VALUE ZERO.
       01  DAY-OF-WEEK-TAB            PIC X(21) VALUE
           "MonTueWedThuFriSatSun".
       01  DAY-OF-WEEK-RED REDEFINES DAY-OF-WEEK-TAB.
           05  DAY-OF-THE-WEEK        PIC X(3) OCCURS 7 TIMES.

       01  TIME-DATE-LINE.
           05  FILLER                 PIC X(6)  VALUE "Date: ".
           05  MM                     PIC XX.
           05  SLASH-1                PIC X     VALUE "/".
           05  DD                     PIC XX.
           05  SLASH-2                PIC X     VALUE "/".
           05  YY                     PIC X(5).
           05  FILLER                 PIC X     VALUE "(".
           05  DOW                    PIC X(3).
           05  FILLER                 PIC X     VALUE ")".
           05  FILLER                 PIC X(13) VALUE "   Run Time: ".
           05  HH                     PIC XX.
           05  DIV-1                  PIC X     VALUE ":".
           05  MN                     PIC XX.
           05  DIV-2                  PIC X     VALUE ":".
           05  SS                     PIC XX.
           05  FILLER                 PIC X(5).
           05  FILLER                 PIC X(15) VALUE "Date Compiled: ".
           05  DATE-COMPILE           PIC 99/99/9999.

       COPY "rminfo.cpy".
       COPY "sysinfo.cpy".
       COPY "printinf.cpy".

       01  RM-LINE.
           05  RM-USER                 PIC X(9)  VALUE "User:    ".
           05  RM-SUB-LINE.
               10  FILLER              PIC X(16) VALUE
               " using RM/COBOL ".
               10  RM-LINE-VersionNumber
                                       PIC X(8).
               10  FILLER              PIC X(1)  VALUE "(".
               10  RM-Line-RegistrationNumber PIC X(18).
               10  FILLER              PIC X(1)  VALUE ")".
               10  FILLER              PIC X(12) VALUE SPACES.

       01  PRT-LINE.
           05  FILLER                 PIC X(9)  VALUE "Printer: ".
           05  Prt-Line-PrinterName   PIC X(71).

       01  UNDER-LINE.
           05  FILLER                 PIC X(80) VALUE ALL "-".

       01 WS-A-LINE.
          03  WS-A-PARM              PIC X(23).
          03                         PIC X(4)  VALUE " =  ".
          03                         PIC X(8).
          03  WS-A-VALUE             PIC X(1).
          03                         PIC X(15) VALUE " Return Code = ".
          03  WS-A-RETURN-OUT        PIC ZZZ9.

       01 WS-X-LINE.
          03  WS-X-PARM              PIC X(23).
          03                         PIC X(3)  VALUE " = ".
          03                         PIC X(7)  VALUE "      x".
          03  WS-X-VALUE             PIC 9(3).
          03                         PIC X(15) VALUE " Return Code = ".
          03  WS-X-RETURN-OUT        PIC ZZZ9.

       01 WS-X-VALUE-GRP.
          03  WS-X-VALUE-1            PIC X VALUE x'00'.
          03  WS-X-VALUE-2            PIC X.
       01 WS-X-VALUE-RED REDEFINES WS-X-VALUE-GRP PIC 9(3) COMP-1.

       01 WS-B-LINE.
          03  WS-B-PARM              PIC X(23).
          03                         PIC X(3)  VALUE " = ".
          03  WS-B-VALUE             PIC Z(9)9.
          03                         PIC X(15) VALUE " Return Code = ".
          03  WS-B-RETURN-OUT        PIC ZZZ9.

       01  WS-GROUP-LINE.
           03  FILLER                  PIC X(49)  VALUE
           "P$GetTextMetrics Group Calls.  Greetings Font is ".
           03  WS-FONT-OUT             PIC X(31)  VALUE
           "Times New Roman".

       01  WS-INDIVIDUAL-LINE.
           03  FILLER                  PIC X(49)  VALUE
           "P$GetTextMetrics Individual Calls. Font reset to ".
           03  WS-RESET-FONT-OUT       PIC X(31)  VALUE SPACES.

       01  WS-LINE-OUT.
           03  WS-TEXT-OUT             PIC X(23) VALUE SPACES.
           03  FILLER                  PIC X(2)  VALUE " =".
           03  WS-VALUE-OUT            PIC Z(9)9.
           03  WS-VALUE-OUT-X REDEFINES WS-VALUE-OUT.
               05 FILLER               PIC X(9).
               05 WS-YN-VALUE-OUT      PIC X(1).
           03  WS-VALUE-OUT-R REDEFINES WS-VALUE-OUT.
               05 FILLER               PIC X(6).
               05 WS-HEX-VALUE-X       PIC X(1).
               05 WS-HEX-VALUE-OUT     PIC 9(3).
           03  FILLER                  PIC X(1)  VALUE SPACES.
           03  WS-DESC-OUT             PIC X(44) VALUE SPACES.

       01 WS-VARIABLES.
           03 WS-TOP                  PIC 9(10)      VALUE ZERO.
           03 WS-TOP-Less-Ext-Lead    PIC 9(10)      VALUE ZERO.
           03 WS-BASE                 PIC 9(10)      VALUE ZERO.
           03 WS-BOTTOM               PIC 9(10)      VALUE ZERO.
           03 WS-DUMMY                PIC 9(10)      VALUE ZERO.
           03 WS-XP-D                 PIC 9(10)      VALUE ZERO.
           03 WS-X-D                  PIC 9(10)      VALUE ZERO.
           03 WS-XB-D                 PIC 9(10)      VALUE ZERO.
           03 WS-Line-End             PIC 9(10)      VALUE ZERO.
           03 WS-Line-Beg             PIC 9(10)      VALUE ZERO.
           03 WS-SizeWidth            PIC 9(10)      VALUE ZERO.
           03 WS-SizeHeight           PIC 9(10)      VALUE ZERO.

       01  WS-Save-Logical-Font.
           03                         PIC X(25).
           03  WS-Save-FaceName       PIC X(31).

       COPY "windefs.cpy".
       COPY "devcaps.cpy".
       COPY "logfont.cpy".
       COPY "txtmtric.cpy".
      *
      *    Parameter Name Values
      *
       01 WS-TEXTMETRIC-PARAMETERS.
          02 WS-TM-ALPHANUMERICS.
             03              PIC X(23) Value TM-ItalicParam.
             03              PIC X(23) Value TM-UnderlinedParam.
             03              PIC X(23) Value TM-StruckOutParam.
             03              PIC X(23) Value TM-FirstCharacterParam.
             03              PIC X(23) Value TM-LastCharacterParam.
             03              PIC X(23) Value TM-DefaultCharacterParam.
             03              PIC X(23) Value TM-BreakCharacterParam.
          02 WS-TM-ALP REDEFINES WS-TM-ALPHANUMERICS OCCURS 7 TIMES.
             03 WS-TM-ALPHN  PIC X(23).
      *
          02 WS-TM-NUMERICS.
             03           PIC X(23) Value TM-HeightParam.
             03           PIC X(23) Value TM-AscentParam.
             03           PIC X(23) Value TM-DescentParam.
             03           PIC X(23) Value TM-InternalLeadingParam.
             03           PIC X(23) Value TM-ExternalLeadingParam.
             03           PIC X(23) Value TM-AverageCharacterWidthParam.
             03           PIC X(23) Value TM-MaximumCharacterWidthParam.
             03           PIC X(23) Value TM-WeightParam.
             03           PIC X(23) Value TM-OverhangParam.
             03           PIC X(23) Value TM-DigitizedAspectXParam.
             03           PIC X(23) Value TM-DigitizedAspectYParam.
             03           PIC X(23) Value TM-PitchParam.
             03           PIC X(23) Value TM-FamilyParam.
             03           PIC X(23) Value TM-CharacterSetParam.
          02 WS-TM-NUM REDEFINES WS-TM-NUMERICS OCCURS 14 TIMES.
             03 WS-TM-N   PIC X(23).
          02 WS-TM-ALPHN-VALUE      PIC X(01)  VALUE SPACES.
          02 WS-TM-NUM-VALUE        PIC 9(10)  VALUE ZEROS.
          02 WS-TM-RET-VALUE        PIC 9(01)  VALUE ZEROS.
      *
      *    Define Device Information Definition
      *
       77 I                                    Picture 99 Value 80.
       01 DefineDeviceInformation.
          02  DDI-DeviceName                   Picture X(80).
          02  DDI-PortName                     Picture X(10).
          02  DDI-FontName                     Picture X(80).
          02  DDI-PointSize                    Picture 9(5) BINARY (2).
          02  DDI-RawModeValue                 Picture X.
              88  DDI-RawMode                    Value 'Y'
                                            When False 'N'.
          02  DDI-EscapeModeValue              Picture X.
              88  DDI-EscapeMode                 Value 'Y'
                                            When False 'N'.

       01  Ws-DefineDevice-Stat                Picture 9(1) Value 0.
           88  No-Define-Device                  Value 0.

       01  Define-Pointer                      Picture 9(3) Value 1.
       01  Ws-Overflow-Flag                    Picture X Value 'N'.
           88 Overflow-Flag                       Value 'Y'.
       01  Ws-Character-Found                  Picture X Value 'N'.
           88 Character-Found                     Value 'Y'
                                             When False 'N'.
       01  Ws-Pointsize                         Picture Z9.

       PROCEDURE DIVISION.
       DRIVER-SECTION SECTION.
      ** Main driver paragraph **
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           PERFORM WRAP-UP-SUMMARY.
           CLOSE PRINTER.
           STOP RUN.

      ** Prepare heading information **
       INITIAL-SETUP.
           OPEN OUTPUT PRINTER.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           ACCEPT DOW-SUB       FROM DAY-OF-WEEK.
           MOVE DAY-OF-THE-WEEK(DOW-SUB)   TO DOW.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.
           COMPUTE DATE-COMPILE = DATE-COMP * DATE-CONVERT.

      ** Print heading **
       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1      AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2      AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE   AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "C$SetDevelopmentMode".
           CALL "C$GetRMInfo"      USING RMInformation.
           CALL "C$GetSysInfo"     USING SystemInformation.
           CALL "P$GetPrinterInfo" USING PrinterInformation.

           MOVE  RM-VersionNumber      TO RM-Line-VersionNumber.
           MOVE  RM-RegistrationNumber TO RM-Line-RegistratioNnumber.
           MOVE  PI-PrinterName        TO Prt-Line-PrinterName.

           STRING RM-USER       Delimited by Size,
                  Sys-UserName  Delimited by Spaces,
                  RM-SUB-LINE   Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           STRING "Machine: "   Delimited by Size,
                  Sys-NodeName  Delimited by Spaces,
                  " running "   Delimited by Size,
                  Sys-Name      Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM PRT-LINE   AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "P$GetDefineDeviceInfo" Using DefineDeviceInformation
                                        Giving Ws-DefineDevice-Stat.
           If No-Define-Device
               Write Print-Record from "Define-Device: None "
           Else
               Perform Define-Device-Line
           End-If.

           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.

       DEFINE-DEVICE-LINE.
           Move "Define-Device: "  To Print-Record.
           Move 16                 To Define-Pointer.

           If DDI-DeviceName Not Equal Spaces
               Set Character-Found To False
               Perform Varying I from 80 by -1 Until Character-Found
                   If DDI-DeviceName (I:1) Not Equal Spaces
                       Set Character-Found To True
                   End-If
               End-Perform
               If I < 80
                   Add 1 To I
               End-If
               String DDI-DeviceName(1:I) Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow DeviceName " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           If Not OverFlow-Flag
               Perform String-Comma.

           If DDI-PortName Not Equal Spaces And Not OverFlow-Flag
               String "Port="         Delimited by Size,
                      DDI-PortName    Delimited by Spaces,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow PortName " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           If Not OverFlow-Flag
               Perform String-Comma.

           If DDI-FontName Not Equal Spaces And Not OverFlow-Flag
               Set Character-Found To False
               Perform Varying I from 80 by -1 Until Character-Found
                   If DDI-FontName (I:1) Not Equal Spaces
                       Set Character-Found To True
                   End-If
               End-Perform
               If I < 80
                   Add 1 To I
               End-If
               String "Font="                Delimited by Size,
                       DDI-FontName(1:I)     Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow FontName " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           If Not OverFlow-Flag
               Perform String-Comma.

           If DDI-PointSize Not Equal Zeros And Not OverFlow-Flag
               Move DDI-PointSize to Ws-PointSize
               String "Size="         Delimited by Size,
                      Ws-PointSize    Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow PointSize " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           IF  DDI-RawMode And Not OverFlow-Flag
               String ",RAW"         Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow RAW " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           IF  DDI-EscapeMode And Not OverFlow-Flag
               String ",ESC"         Delimited by Size,
                  Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow ESC " Define-Pointer
                   Set Overflow-Flag To True
               End-String.

           Write Print-Record.
           Move Spaces To Print-Record.

       STRING-COMMA.
           String ","             Delimited by Size
               Into Print-Record
                  With Pointer Define-pointer
               On Overflow
                   Display " Overflow Comma " Define-Pointer
                   Set Overflow-Flag To True
           End-String.

       WRAP-UP-SUMMARY.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER ADVANCING 2 LINES.
           DISPLAY "END OF  PTEXTMET TEST"
               LINE 5 POSITION 10 ERASE.

       MAIN SECTION.
       TEST-1.
           INITIALIZE TEXTMETRICS.

      * Group Call
           PERFORM PRINT-SAMPLE.

      * Individual Calls
           MOVE SPACES TO PRINT-RECORD.
           WRITE PRINT-RECORD AFTER PAGE.
           MOVE WS-SAVE-FACENAME TO WS-RESET-FONT-OUT.
           WRITE PRINT-RECORD FROM WS-INDIVIDUAL-LINE AFTER 2 LINES.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINE.
           WRITE PRINT-RECORD FROM SPACES AFTER 1 LINE.

           INITIALIZE TEXTMETRICS.

           PERFORM VARYING WS-SUB FROM 1 BY 1 UNTIL WS-SUB > 14
               CALL "P$GetTextMetrics" USING WS-TM-N(WS-SUB),
                                       WS-TM-NUM-VALUE,
                                       GIVING WS-TM-RET-VALUE
               END-CALL
               MOVE WS-TM-N(WS-SUB)            TO WS-B-PARM
               MOVE WS-TM-NUM-VALUE            TO WS-B-VALUE
               MOVE WS-TM-RET-VALUE            TO WS-B-RETURN-OUT
               WRITE PRINT-RECORD FROM WS-B-LINE
           END-PERFORM.

           WRITE PRINT-RECORD FROM SPACES AFTER 2.

           PERFORM VARYING WS-SUB FROM 1 BY 1 UNTIL WS-SUB > 3
               CALL "P$GetTextMetrics" USING WS-TM-ALPHN(WS-SUB)
                                       WS-TM-ALPHN-VALUE
                                       GIVING WS-TM-RET-VALUE
               END-CALL
               MOVE WS-TM-ALPHN(WS-SUB)        TO WS-A-PARM
               MOVE WS-TM-ALPHN-VALUE          TO WS-A-VALUE
               MOVE WS-TM-RET-VALUE            TO WS-A-RETURN-OUT
               WRITE PRINT-RECORD FROM WS-A-LINE
           END-PERFORM.

           PERFORM VARYING WS-SUB FROM 4 BY 1 UNTIL WS-SUB > 7
               CALL "P$GetTextMetrics" USING WS-TM-ALPHN(WS-SUB)
                                       WS-TM-ALPHN-VALUE
                                       GIVING WS-TM-RET-VALUE
               END-CALL
               MOVE WS-TM-ALPHN(WS-SUB)        TO WS-X-PARM
               MOVE WS-TM-ALPHN-VALUE          TO WS-X-VALUE-2
               MOVE WS-X-VALUE-RED             TO WS-X-VALUE
               MOVE WS-TM-RET-VALUE            TO WS-X-RETURN-OUT
               WRITE PRINT-RECORD FROM WS-X-LINE
           END-PERFORM.

       PRINT-SAMPLE SECTION 88.
       PRINT-TEXT.
           CALL "P$GetFont" USING WS-Save-Logical-Font.
           CALL "P$ClearFont".

           CALL "P$GetDeviceCapabilities" Using DC-LogicalPixelsYParam,
                                                DC-LogPixelsY.
           Move DC-LogPixelsY  To Units-Per-Inch.
           Compute WS-Line-End = 5.25 * Units-Per-Inch.
           Compute WS-Line-Beg =  .25 * Units-Per-Inch.

      * Set the font to one inch, Times New Roman.
           CALL "P$SetFont" Using LF-HeightParam    DC-LogPixelsY,
                                  LF-WeightParam    LF-WeightNormal,
                                  LF-FaceNameParam  Ws-Font-Out.

           CALL "P$SetPosition" Using .25, 3.25, ModeIsAbsolute,
                                                UnitsAreInches.

           CALL "P$TextOut" Using "Greetings" Omitted, Omitted,
                                   ModeIsAbsolute,
                                   UnitsAreInches,
                                   TextOutWithBox,
                                   TextOutWithoutShading.

      * Get the text metrics for the current font.
           CALL "P$GetTextMetrics"  Using TextMetrics.

           CALL "P$GetTextExtent"   Using "Greetings",
                                          Ws-SizeWidth,
                                          Ws-SizeHeight,
                                          UnitsAreDeviceUnits.
           CALL "P$GetTextPosition" Using Ws-X-D, Ws-TOP,
                                          PositionIsTop,
                                          UnitsAreDeviceUnits.
           CALL "P$GetTextPosition" Using Ws-XB-D, Ws-BOTTOM,
                                          PositionIsBottom,
                                          UnitsAreDeviceUnits.
           CALL "P$GetPosition"     Using Ws-XP-D, Ws-BASE,
                                          UnitsAreDeviceUnits.

           Compute LF-Height Rounded = (9 * DC-LogPixelsY) / 72.

           CALL "P$SetFont" Using LF-HeightParam    LF-Height,
                                  LF-WeightParam    LF-WeightNormal,
                                  LF-FaceNameParam  "Times New Roman".

           CALL "P$SetPen"  Using PenStyleDash.

           CALL "P$DrawLine" Using Ws-Line-Beg, Ws-ToP,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits,
                                   Ws-Line-End,
                                   Ws-Top
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.

           CALL "P$DrawLine" Using Ws-Line-Beg, Ws-Base,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits,
                                   Ws-Line-End,
                                   Ws-Base
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.

           CALL "P$DrawLine" Using Ws-Line-Beg, Ws-Bottom,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits,
                                   Ws-Line-End,
                                   Ws-Bottom
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.

           Compute Ws-Line-End = 6 * Units-Per-Inch.
           Compute Ws-Top-Less-Ext-Lead = Ws-Top +
                                          TM-ExternalLeading.

           CALL "P$DrawLine" Using Ws-Line-Beg,
                                   Ws-Top-Less-Ext-Lead,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits,
                                   Ws-Line-End,
                                   Ws-Top-Less-Ext-Lead
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.
           Add 40 to Ws-X-D.
           CALL "P$SetTextPosition" Using Ws-X-D, Ws-Top,
                                    PositionIsBottom,
                                    ModeIsAbsolute,
                                    UnitsAreDeviceUnits.
           CALL "P$TextOut" Using "Top Line".

           CALL "P$GetPosition" Using Ws-XP-D, Ws-Dummy,
                                      UnitsAreDeviceUnits.
           CALL "P$SetPosition" Using Ws-XP-D, Ws-Base,
                                      ModeIsAbsolute,
                                      UnitsAreDeviceUnits.
           CALL "P$TextOut" Using "Base Line".

           CALL "P$GetPosition" Using Ws-X-D, Ws-dummy,
                                      UnitsAreDeviceUnits.
           CALL "P$SetTextPosition" Using Ws-X-D, Ws-Bottom,
                                      PositionIsBottom,
                                      ModeIsAbsolute,
                                      UnitsAreDeviceUnits.
           CALL "P$TextOut" Using "Bottom Line".

           CALL "P$SetPosition" Using Ws-Line-End,
                                      Ws-Top-Less-Ext-Lead,
                                      ModeIsAbsolute,
                                      UnitsAreDeviceUnits.
           CALL "P$TextOut" Using "Top + ExtLeading".

      *    If Ws-Save-FaceName Equal Spaces
      *        Move "Courier New" To Ws-Save-FaceName.

           CALL "P$SetFont" USING WS-Save-Logical-Font
                                  Giving WS-TM-Ret-Value.

           WRITE PRINT-RECORD FROM WS-GROUP-LINE AFTER 11.

           Move  TM-HeightParam                To WS-Text-Out.
           Move  TM-Height                     To WS-Value-Out.
           Move  "Ascender line to descender line             "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 2 lines.
           Move Spaces to print-record.
           Move  TM-AscentParam                To Ws-Text-Out.
           Move  TM-Ascent                     To Ws-Value-Out.
           Move  "Ascender line to baseline                   "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move Spaces to print-record.
           Move  TM-DescentParam               To Ws-Text-Out.
           Move  TM-Descent                    To Ws-Value-Out.
           Move  "Baseline to descender line                  "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move Spaces to print-record.
           Move  TM-InternalLeadingParam       To Ws-Text-Out.
           Move  TM-InternalLeading            To Ws-Value-Out.
           Move  "Point size of font minus physical font size "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move Spaces to print-record.
           Move  TM-ExternalLeadingParam       To Ws-Text-Out.
           Move  TM-ExternalLeading            To Ws-Value-Out.
           Move  "Extra space to be added between lines       "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move Spaces to print-record.
           Move  TM-AverageCharacterWidthParam To Ws-Text-Out.
           Move  TM-AveCharWidth               To Ws-Value-Out.
           Move  "Average character width for a font          "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move Spaces to print-record.
           Move  TM-MaximumCharacterWidthParam To Ws-Text-Out.
           Move  TM-MaxCharWidth               To Ws-Value-Out.
           Move  "Width of the widest character               "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move Spaces to print-record.
           Move  TM-WeightParam                To Ws-Text-Out.
           Move  TM-WeightValue                To Ws-Value-Out.
           Move  "Font weight                                 "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move  TM-OverhangParam              To Ws-Text-Out.
           Move  TM-Overhang                   To Ws-Value-Out.
           Move  "Extra width added to some synthesized fonts "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move Spaces to print-record.
           Move  TM-DigitizedAspectXParam      To Ws-Text-Out.
           Move  TM-DigitizedAspectX           To Ws-Value-Out.
           Move  "Horz device aspect which font was designed  "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move Spaces to print-record.
           Move  TM-DigitizedAspectYParam      To Ws-Text-Out.
           Move  TM-DigitizedAspectY           To Ws-Value-Out.
           Move  "Vert device aspect which font was designed  "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move  TM-PitchParam                 To Ws-Text-Out.
           Move  TM-PitchValue                 To Ws-Value-Out.
           Move  "Pitch of the font                           "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move Spaces to print-record.
           Move  TM-FamilyParam                To Ws-Text-Out.
           Move  TM-FamilyValue                To Ws-Value-Out.
           Move  "Family of the font                          "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move Spaces to print-record.
           Move  TM-CharacterSetParam          To Ws-Text-Out.
           Move  TM-CharSetValue               To Ws-Value-Out.
           Move  "Character set of the font                   "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.

           Move  TM-ItalicParam                To Ws-Text-Out.
           Move  TM-ItalicValue                To Ws-YN-Value-Out.
           Move  "Specifies an italic font if Y               "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 2 lines.
           Move  TM-UnderlinedParam            To Ws-Text-Out.
           Move  TM-UnderlinedValue            To Ws-YN-Value-Out.
           Move  "Specifies an underlined font if Y           "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move  TM-StruckOutParam             To Ws-Text-Out.
           Move  TM-StruckOutValue             To Ws-YN-Value-Out.
           Move  "Specifies an strikeout font if Y            "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.

           Move  "x"                           To Ws-Hex-Value-X.
           Move  TM-FirstCharacterParam        To Ws-Text-Out.
           Move  TM-FirstChar                  To Ws-X-Value-2.
           Move  WS-X-Value-Red                To Ws-Hex-Value-Out.
           Move  "First character defined in the font         "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move  TM-LastCharacterParam         To Ws-Text-Out.
           Move  TM-LastChar                   To Ws-X-Value-2.
           Move  WS-X-Value-Red                To Ws-Hex-Value-Out.
           Move  "Last character defined in the font          "
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move  TM-DefaultCharacterParam      To Ws-Text-Out.
           Move  TM-DefaultChar                To Ws-X-Value-2.
           Move  WS-X-Value-Red                To Ws-Hex-Value-Out.
           Move  "Character substituted for omitted characters"
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.
           Move  TM-BreakCharacterParam        To Ws-Text-Out.
           Move  TM-BreakChar                  To Ws-X-Value-2.
           Move  WS-X-Value-Red                To Ws-Hex-Value-Out.
           Move  "Character used for word breaks in text just."
                                               To Ws-Desc-Out.
           Write Print-Record from Ws-line-out after advancing 1 lines.

           Write Print-Record from Under-Line after advancing 2 lines.

       END PROGRAM  PTEXTMET.
