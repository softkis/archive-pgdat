       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PINVOICE.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION: Prints a sample Invoice
      *
      * Version Identification:
      *   $Revision:   1.1.1.1  $
      *   $Date:   27 Sep 1999 14:55:40  $
      *
      *********************************************************************

       DATE-WRITTEN.     01/12/1999.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PFILE ASSIGN TO PRINT, "PRINTER?".
       DATA DIVISION.
       FILE SECTION.
       FD  PFILE.
       01  FD-RECORD       PIC X(132).
       WORKING-STORAGE SECTION.
       COPY "windefs.cpy".
       COPY "logfont.cpy".
       COPY "devcaps.cpy".
       PROCEDURE DIVISION.
       MAIN-START.

           CALL "C$SetDevelopmentMode".
           OPEN OUTPUT PFILE.
           CALL "P$DRAWBITMAP" USING "RMLOGO.BMP",
                                     0, 0,
                                     ModeIsAbsolute,
                                     UnitsAreInches,
                                     1.5, 1.0,
                                     UnitsAreInches.

           CALL "P$DRAWBOX" USING 4, .5,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  3, 1.

           CALL "P$SETPEN" USING PenStyleDash, 1.

           CALL "P$DRAWBOX" USING 4.5, 1.80,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  2, .5.

           CALL "P$SETPOSITION" USING 0, 0, "A" "I".
           CALL "P$CLEARFONT".

           CALL "P$GetDeviceCapabilities" Using DC-LogicalPixelsYParam,
                                                DC-LogPixelsY.
      * Compute Font Height for desired Point-Size.
           Compute LF-Height Rounded = (28 * Dc-LogPixelsY)  / 72.
           CALL "P$SetFont" Using LF-HeightParam LF-Height,
                                  LF-WeightParam LF-WeightBold.

           CALL "P$TEXTOUT" USING "INVOICE", 4.25, .35,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$CLEARFONT".


           Set LF-Underline To True.
           CALL "P$SETFONT" USING LF-UnderlineParam, LF-UnderlineValue.
           CALL "P$TEXTOUT" USING "Terms: NET 30 DAYS", 4.55, 2,
                                  ModeIsAbsolute,
                                  UnitsAreInches.

           CALL "P$CLEARFONT".
           WRITE  FD-RECORD FROM "RM/COBOL Printer Test " AFTER 1.

           CALL "P$SETPEN" USING PenStylesolid, 5.

           CALL "P$DRAWBOX" USING 0, 2.5,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  7.5, 2.5.

           CALL "P$SetTextPosition" Using 0.25, 2.75,
                                  PositionIsTop,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$TextOut" USING "Order".

           CALL "P$DRAWLINE" USING 1, 2.5,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  1, 3.15.
           CALL "P$SetTextPosition" Using 1.15, 2.75,
                                  PositionIsTop,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$TextOut" USING "Customer ID".

           CALL "P$DRAWLINE" USING 2.25, 2.5,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  2.25, 3.15.
           CALL "P$SetTextPosition" Using 2.40, 2.75,
                                  PositionIsTop,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$TextOut" USING "Purchase No.".

           CALL "P$DRAWLINE" USING 3.8, 2.5,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  3.8, 3.15.
           CALL "P$SetTextPosition" Using 4.00, 2.75,
                                  PositionIsTop,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$TextOut" USING "Sales Person".

           CALL "P$DRAWLINE" USING 5.25, 2.5,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  5.25, 6.
           CALL "P$SetTextPosition" Using 5.40, 2.75,
                                  PositionIsTop,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$TextOut" USING "Ship Date".

           CALL "P$DRAWLINE" USING 6.5, 2.5,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  6.5, 6.
           CALL "P$SetTextPosition" Using 6.75, 2.75,
                                  PositionIsTop,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$TextOut" USING "Method".

           CALL "P$DRAWLINE" USING 0, 3.15,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  7.5, 3.15.

           CALL "P$DRAWLINE" USING 0, 3.50,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  7.5, 3.50.

           CALL "P$SetTextPosition" Using 0.25, 3.25,
                                  PositionIsTop,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$TextOut" USING "Part No.".
           CALL "P$DRAWLINE" USING 1.3, 3.15,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  1.3, 5.00.

           CALL "P$SetTextPosition" Using 1.35, 3.25,
                                  PositionIsTop,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$TextOut" USING "Qty.".
           CALL "P$DRAWLINE" USING 2.0, 3.15,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  2.0, 5.00.

           CALL "P$SetTextPosition" Using 2.45, 3.25,
                                  PositionIsTop,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$TextOut" USING " Description ".
           CALL "P$DRAWLINE" USING 7.5, 5.0,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  7.5, 6.00.
           CALL "P$SetTextPosition" Using 6.65, 3.25,
                                  PositionIsTop,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$TextOut" USING "Price".
           CALL "P$SetTextPosition" Using 5.45, 3.25,
                                  PositionIsTop,
                                  ModeIsAbsolute,
                                  UnitsAreInches.
           CALL "P$TextOut" USING "Total".

           CALL "P$DRAWLINE" USING 5.25, 6.0,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  7.5, 6.00.

           CALL "P$SetTextColor" Using ColorDarkGray.
           Compute LF-Height Rounded = (72 * Dc-LogPixelsY)  / 72.
           Move 450 To Lf-Escapement.
           CALL "P$SetFont" Using LF-HeightParam,  LF-Height,
                                  LF-EscapementParam, LF-Escapement.

           CALL "P$TEXTOUT" USING "Example", 5, 17,
                                  ModeIsAbsolute,
                                  UnitsAreMetric.

           CLOSE PFILE.
           STOP RUN.

