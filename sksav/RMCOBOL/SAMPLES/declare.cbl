       ID DIVISION.
       PROGRAM-ID.  DECLARATIVES-EXAMPLE.
      *
      * Title:  declare.cbl
      *         RM/COBOL Declaratives Example
      *
      * This program provides an example of calling
      * several C$ routines in the DECLARATIVES section
      * to obtain more information about an I/O error.
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      * Version Identification:
      *   $Revision:   1.0.1.0  $
      *   $Date:   23 Sep 1999 14:43:10  $
      *

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT THE-FILE ASSIGN TO RANDOM FILE-NAME.

       DATA DIVISION.
       FILE SECTION.
       FD  THE-FILE.
       01  THE-REC                      PIC X(80).
       WORKING-STORAGE SECTION.
       01  FILE-NAME                    PIC X(80)
                        VALUE "c:\Liant.Nonexistent.File".
       01  ERROR-STATUS                 PIC X(11).
       01  LAST-FILE-NAME               PIC X(30).
       01  LAST-PATH-NAME               PIC X(80).
       01  LAST-OPERATION               PIC X(20).
       01  LAST-LINE-NUM                PIC Z(5)9.
       01  LAST-INTRALINE-NUM           PIC Z9.

       PROCEDURE DIVISION.
       DECLARATIVES.

       THE-FILE-ERROR SECTION.
           USE AFTER ERROR PROCEDURE ON THE-FILE.
       THE-FILE-ERR.
           CALL "C$RERR"            USING ERROR-STATUS.
           CALL "C$GetLastFileOp"   USING LAST-OPERATION,
                                          LAST-LINE-NUM,
                                          LAST-INTRALINE-NUM.
           CALL "C$GetLastFileName" USING LAST-FILE-NAME,
                                          LAST-PATH-NAME.
           IF LAST-PATH-NAME = SPACES
               MOVE "<no path name available>" TO LAST-PATH-NAME.
           DISPLAY "The last operation was:  "
                                LAST-OPERATION.
           DISPLAY "which returned error  :  "
                                ERROR-STATUS.
           DISPLAY "on COBOL file         :  "
                                LAST-FILE-NAME.
           DISPLAY "file path name        :  "
                                LAST-PATH-NAME.
           DISPLAY "at line               :  "
                                LAST-LINE-NUM "+"
                                LAST-INTRALINE-NUM CONVERT.
           DISPLAY "in program            :  "
                                PROGRAM-ID.

       END DECLARATIVES.

       THE-MAIN SECTION.
       A.
      * The following OPEN should get an error.
           OPEN INPUT THE-FILE.
           STOP RUN.
