       ID DIVISION.
       PROGRAM-ID.       SYSINFO.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION: SYSINFO
      *
      *  SYSINFO - Call C$GETSYSINFO and prints values.
      *            Version 7 Feature
      *
      * Version Identification:
      *   $Revision:   1.2.1.1  $
      *   $Date:   27 Sep 1999 17:21:00  $
      *
      *********************************************************************

       DATE-WRITTEN.     01/27/1999
       REMARKS.          CALLS C$GETSYSINFO.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PRINTER ASSIGN TO PRINT, "PRINTER1".

       DATA DIVISION.
       FILE SECTION.
       FD  PRINTER.
       01  PRINT-RECORD                PIC X(80).

       WORKING-STORAGE SECTION.
       01  WS-RECORD.
           03  WS-TEXT                 PIC X(25)  VALUE SPACES.
           03  WS-TEXT-VALUE           PIC X(20).
           03  WS-NUM-VALUE-RED REDEFINES WS-TEXT-VALUE.
                05  WS-NUM-VALUE       PIC Z(9)9.
                05  FILLER             PIC X(10).
           03  FILLER                  PIC X(15) VALUE SPACES.

       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(15) VALUE SPACES.
           05  FILLER                  PIC X(31) VALUE
           "RUNTIME RESULTS - SYSINFO   ".

       01  DESCRIP-LINE.
           05  FILLER                  PIC X(25)  VALUE
           "**   SYSINFO  - Test For ".
           05  FILLER                  PIC X(24) VALUE
           "Call To C$GetSysInfo  **".

       01  TIME-DATE-LINE.
           05  FILLER                  PIC X(6)  VALUE "DATE: ".
           05  MM                      PIC XX.
           05  SLASH-1                 PIC X     VALUE "/".
           05  DD                      PIC XX.
           05  SLASH-2                 PIC X     VALUE "/".
           05  YY                      PIC X(4).
           05  FILLER                  PIC X(14) VALUE "    RUN-TIME: ".
           05  HH                      PIC XX.
           05  DIV-1                   PIC X     VALUE ":".
           05  MN                      PIC XX.
           05  DIV-2                   PIC X     VALUE ":".
           05  SS                      PIC XX.
           05  DIV-3                   PIC X     VALUE ".".
           05  CC                      PIC XX.

       01  UNDER-LINE.
           05  FILLER                  PIC X(80) VALUE ALL "-".

       COPY "sysinfo.cpy".

       PROCEDURE DIVISION.
       DRIVER-SECTION SECTION.
      ** Main driver paragraph **
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 2 LINES.
           CLOSE PRINTER.
           STOP RUN.

      ** Prepare heading information **
       INITIAL-SETUP.
           OPEN OUTPUT PRINTER.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.

      ** Print heading **
       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1 AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2 AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

       MAIN SECTION.
       A.
           INITIALIZE SystemInformation.
           CALL "C$GETSYSINFO" USING SystemInformation
               ON EXCEPTION
                   WRITE PRINT-RECORD FROM "Error Calling C$GetSysInfo"
                   MOVE SPACES TO WS-RECORD
               NOT ON EXCEPTION
                   PERFORM PRINT-SYSINFO
           END-CALL.

       PRINT-SYSINFO SECTION.
       B.

           MOVE  "Sys-Name: " TO WS-TEXT.
           MOVE  SYS-NAME     TO WS-TEXT-VALUE.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 2.
           MOVE SPACES TO WS-RECORD.

           MOVE  "Sys-MajorVersion: " TO WS-TEXT.
           MOVE  SYS-MAJORVERSION     TO WS-NUM-VALUE.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 1.
           MOVE SPACES TO WS-RECORD.

           MOVE  "Sys-MinorVersion: " TO WS-TEXT.
           MOVE   SYS-MINORVERSION    TO WS-NUM-VALUE.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 1.
           MOVE SPACES TO WS-RECORD.

           MOVE "Sys-NodeName: " TO WS-TEXT.
           MOVE  SYS-NODENAME    TO WS-TEXT-VALUE.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 1.
           MOVE  SPACES TO WS-RECORD.

           MOVE "Sys-Machine: " TO WS-TEXT.
           MOVE  SYS-MACHINE    TO WS-TEXT-VALUE.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 1.
           MOVE SPACES TO WS-RECORD.

           MOVE "Sys-UserName: " TO WS-TEXT.
           MOVE  SYS-USERNAME    TO WS-TEXT-VALUE.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 1.
           MOVE SPACES TO WS-RECORD.

           MOVE "Sys-UserID: " TO WS-TEXT.
           MOVE  SYS-USERID    TO WS-NUM-VALUE.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 1.
           MOVE SPACES TO WS-RECORD.

           MOVE "Sys-GroupName: " TO WS-TEXT.
           MOVE  SYS-GROUPNAME    TO WS-TEXT-VALUE.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 1.
           MOVE SPACES TO WS-RECORD.

           MOVE "Sys-GroupID: " TO WS-TEXT.
           move  SYS-GROUPID    TO WS-NUM-VALUE.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 1.
           MOVE SPACES TO WS-RECORD.

           MOVE "Sys-StationName: " TO WS-TEXT.
           MOVE  SYS-STATIONNAME    TO WS-TEXT-VALUE.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 1.
           MOVE SPACES TO WS-RECORD.

           MOVE "Sys-ProcessID: " TO WS-TEXT.
           MOVE  SYS-PROCESSID    TO WS-NUM-VALUE.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 1.
           MOVE SPACES TO WS-RECORD.

           MOVE "Sys-IsMultiUserValue: " TO WS-TEXT.
           IF SYS-IsMULTIUSER
               MOVE "Multi User Mode" TO WS-TEXT-VALUE
           ELSE
               MOVE "Single User Mode" TO WS-TEXT-VALUE
           END-If.
           WRITE PRINT-RECORD FROM WS-RECORD AFTER 1.
           MOVE SPACES TO WS-RECORD.

       END PROGRAM SYSINFO.
