       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PCALENDR.

      *************************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *************************************************************************
      *
      * PROGRAM DESCRIPTION: Prints a Calendar Month for any Month/Year entered
      *
      * PRULER - This program uses the routines P$DrawLine, P$SetFont
      *                         P$ClearFont, P$SetPosition, P$TextOut
      *                         P$GetDeviceCapabilities,
      *
      * Version Identification:
      *   $Revision:   1.2.1.0  $
      *   $Date:   27 Sep 1999 14:43:38  $
      *
      *************************************************************************

       DATE-WRITTEN.     02/02/1999.
       REMARKS.     This program will print out a calendar for the
                    month/year entered.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT PFILE ASSIGN TO PRINT, "PRINTER?".
       DATA DIVISION.
       FILE SECTION.
       FD  PFILE.
       01  FD-RECORD               PIC X(80).
       WORKING-STORAGE SECTION.
       77  x pic x.

       78  ONE-SECOND               VALUE 1.
       01  WS-DELAY                 PIC 9(5) BINARY VALUE ONE-SECOND.

       01  WS-VARIABLES.
           03  DAY-COUNTER          PIC 99   VALUE 0.
           03  DAY-COUNTER-X        PIC XX.
           03  Point-Size           Pic 9(2)       Value Zero.
           03  WS-TITLE-OUT         PIC X(18)  VALUE SPACES.
           03  WS-TITLE-TEXT        PIC X(35)  VALUE SPACES.
           03  WS-WEEKDAY-X         PIC 9(10) VALUE 0.
           03  WS-WEEKDAY-X1        PIC 9(10) VALUE 0.
           03  WS-WEEKDAY-SUB       PIC 9(1)  VALUE 0.
           03  WS-SUB               PIC 99V99  VALUE 0.
           03  WS-SUB-C             PIC 99     VALUE 0.
           03  WS-RESULT            PIC 9999   VALUE 0.
           03  WS-REMAINDER         PIC 9999   VALUE 0.
           03  WS-CENTER            PIC 9(6)   VALUE 0.
           03  WS-TITLE-X           PIC 9(10)  VALUE 0.
           03  X-SIZE               PIC 9(10)  VALUE 0.
           03  Y-SIZE               PIC 9(10)  VALUE 0.
           03  UNITS-PER-INCH       PIC 9(10)  VALUE 0.
           03  Ws-Text-Height Pic 9(10)V9(3) Value Zero.
           03  Ws-Text-Width  Pic 9(10)V9(3) Value Zero.
           03  Ws-X-M         Pic 9(10)V9(3) Value Zero.
           03  Ws-Y-M         Pic 9(10)V9(3) Value Zero.

       01  WS-DAYS-OF-WEEK.
           03                       PIC X(9)  VALUE "SUNDAY   ".
           03                       PIC X(9)  VALUE "MONDAY   ".
           03                       PIC X(9)  VALUE "TUESDAY  ".
           03                       PIC X(9)  VALUE "WEDNESDAY".
           03                       PIC X(9)  VALUE "THURSDAY ".
           03                       PIC X(9)  VALUE "FRIDAY   ".
           03                       PIC X(9)  VALUE "SATURDAY ".
       01  WS-DAYS-OF-WEEK-RED REDEFINES WS-DAYS-OF-WEEK.
           03  WS-DAY-OF-WEEK       PIC X(9) OCCURS 7 TIMES.

       01 WS-ARGS.
          03 WS-U-X                 PIC 9(6)       VALUE ZERO.
          03 WS-U-X-OFFSET          PIC 9(6)       VALUE ZERO.
          03 WS-U-X2                PIC 9(6)       VALUE ZERO.
          03 WS-U-X3                PIC 9(6)       VALUE ZERO.
          03 WS-U-Y                 PIC 9(6)       VALUE ZERO.
          03 WS-U-Y2                PIC 9(6)       VALUE ZERO.
          03 WS-X                   PIC 9(9)V9(3)  VALUE ZERO.
          03 WS-Y                   PIC 9(9)V9(3)  VALUE ZERO.
          03 WS-DAY-X               PIC 9(5)V9(3)  VALUE ZERO.
          03 WS-DAY-Y               PIC 9(9)V9(3)  VALUE ZERO.
          03 WS-BITMAP1             PIC X(12) VALUE "RMLOGO.BMP".
          03 WS-BITMAP2             PIC X(12) VALUE "LIANT.BMP".

       01 WS-EXC-NUM                PIC 99           VALUE ZERO.
       01 WS-RETURN                 PIC S9(4) COMP-1 VALUE ZERO.
       01 WS-NEXT-DATE.
          03 WS-NEXT-YY             PIC 9(4).
          03 WS-NEXT-MM             PIC 9(2).
          03 WS-NEXT-DD             PIC 9(2) VALUE 01.
       01 WS-PREV-DATE.
          03 WS-PREV-YY             PIC 9(4).
          03 WS-PREV-MM             PIC 9(2).
          03 WS-PREV-DD             PIC 9(2) VALUE 01.
       01 WS-TEMP-DATE.
          03 WS-TEMP-YY             PIC 9(4).
          03 WS-TEMP-MM             PIC 9(2).
          03 WS-TEMP-DD             PIC 9(2) VALUE 01.
       01 WS-ENTER-DATE.
          03 WS-ENTER-YY            PIC 9(4).
          03 WS-ENTER-MM            PIC 9(2).
             88 VALID-VALUES        VALUE 01 THRU 12.
          03 WS-ENTER-DD            PIC 9(2) VALUE 01.
       01 WS-LEAP-YEAR              PIC X.
          88 LEAP-YEAR              VALUE "Y" WHEN FALSE "N".

       01 WS-MONTH-TABLE.
          03                        PIC X(9) VALUE "JANUARY  ".
          03                        PIC X(9) VALUE "FEBRUARY ".
          03                        PIC X(9) VALUE "MARCH    ".
          03                        PIC X(9) VALUE "APRIL    ".
          03                        PIC X(9) VALUE "MAY      ".
          03                        PIC X(9) VALUE "JUNE     ".
          03                        PIC X(9) VALUE "JULY     ".
          03                        PIC X(9) VALUE "AUGUST   ".
          03                        PIC X(9) VALUE "SEPTEMBER".
          03                        PIC X(9) VALUE "OCTOBER  ".
          03                        PIC X(9) VALUE "NOVEMBER ".
          03                        PIC X(9) VALUE "DECEMBER ".
       01 WS-MONTH-TAB REDEFINES WS-MONTH-TABLE.
          03  WS-MONTH              PIC X(9) OCCURS 12 TIMES.

       01 WS-DAYS-IN-MONTH-TABLE.
          03 YEAR-TAB        PIC X(24) VALUE "312831303130313130313031".
          03 WS-MONTH-DAYS REDEFINES YEAR-TAB
                                    PIC 9(2) OCCURS 12 TIMES.

        01 DATE-VARS.
           03  WS-DAYS-IN-MONTH     PIC 99     VALUE 0.
           03  WS-YEAR-DAYS-AREA.
               05  WS-Y-LEAPS       PIC 9(4) VALUE 0.
               05  WS-Y-CENTURIES   PIC 9(4) VALUE 0.
               05  WS-Y-400S        PIC 9(4) VALUE 0.
               05  WS-A             PIC 9(4) VALUE 0.
               05  WS-M             PIC 9(4) VALUE 0.
               05  WS-YY            PIC 9(4) VALUE 0.
               05  WS-D             PIC 9(4) VALUE 0.
            03  WS-INTERNAL         PIC 9(6) VALUE 0.
            03  WS-DAY-NO.
               05  DA-DAY-NO        PIC 9.
                   88  DA-SUNDAY    VALUE 0.
                   88  DA-MONDAY    VALUE 1.
                   88  DA-TUESDAY   VALUE 2.
                   88  DA-WEDNESDAY VALUE 3.
                   88  DA-THURSDAY  VALUE 4.
                   88  DA-FRIDAY    VALUE 5.
                   88  DA-SATURDAY  VALUE 6.

       COPY "devcaps.cpy".
       COPY "logfont.cpy".
       COPY "windefs.cpy".
       COPY "txtmtric.cpy".

       PROCEDURE DIVISION.
       MAIN-START.

           DISPLAY "  RM/COBOL Calendar Program  "
               LINE 1 COLUMN 25 REVERSE ERASE.

           CALL "C$SetDevelopMentMode"
               ON EXCEPTION
                   Display "This program must be run with Version 7.0"
                      Line 4 Col 1 Reverse.
                    CONTINUE.

           PERFORM ACCEPT-MONTH-YEAR.
           PERFORM GET-DATE-INFO.
           OPEN OUTPUT PFILE.
           PERFORM WRITE-TITLE.
           PERFORM WRITE-DAYS-OF-WEEK.
           PERFORM DRAW-CALENDAR.
           CLOSE PFILE.
           STOP RUN.

       WRITE-TITLE.
           CALL "P$GetDeviceCapabilities" Using DeviceCapabilities.
           MOVE DC-LogPixelsY TO Units-Per-Inch.

           CALL "P$ClearFont".
      * Compute Font Height for desired Point-Size.
           Compute LF-Height Rounded = (36 * Dc-LogPixelsY)  / 72.
           CALL "P$SetFont" Using LF-HeightParam LF-Height,
                                  LF-WeightParam LF-WeightBold.


           MOVE SPACES TO WS-TITLE-OUT.
           STRING  WS-MONTH(WS-TEMP-MM) DELIMITED BY SPACES,
                   SPACE,
                   WS-TEMP-YY DELIMITED BY SIZE
               INTO WS-TITLE-OUT
           ON OVERFLOW
               DISPLAY "  OVERFLOW ERROR!  " LINE 23 COL 22 REVERSE
               CALL "C$DELAY" USING WS-DELAY
               DISPLAY SPACE LINE 23 COL 22 ERASE EOL
           END-STRING.
      *Find the size of the month/year title.

           CALL "P$GetTextExtent" Using  Ws-Title-Out,
                                         Ws-Text-Width,
                                         Ws-Text-Height,
                                         UnitsAreMetric.
      * Divide by 10 to convert millimeters to centimeters.
           Compute Ws-X-M = ((DC-HorzSize / 10) / 2) -
                            (Ws-Text-Width / 2).
           Move Zero to Ws-Y-M.
           CALL "P$SetTextPosition" USING Ws-X-M, Ws-Y-M,
                                          PositionIsTop,
                                          ModeIsAbsolute,
                                          UnitsAreMetric.
           CALL "P$TextOut" Using Ws-Title-Out.


           Compute LF-Height Rounded = (16 * Dc-LogPixelsY)  / 72.
           CALL "P$SetFont" Using LF-HeightParam LF-Height,
                                  LF-WeightParam LF-WeightNormal,
                                  LF-FaceNameParam "Arial",
                                  LF-PitchParam LF-PitchVariable.

           CALL "P$GetTextExtent" Using  Ws-Title-Text,
                                         Ws-Text-Width,
                                         Ws-Text-Height,
                                         UnitsAreMetric.
      * Divide by 10 to convert millimeters to centimeters.
           Compute Ws-X-M = ((DC-HorzSize / 10) / 2) -
                            (Ws-Text-Width / 2).
           Move 1.27 to Ws-Y-M.
           CALL "P$SetTextPosition" USING Ws-X-M, Ws-Y-M,
                                          PositionIsTop,
                                          ModeIsAbsolute,
                                          UnitsAreMetric.
           CALL "P$TextOut" Using Ws-Title-Text.


           CALL "P$ClearFont".

           CALL "P$DrawBitmap" USING WS-BITMAP1 0, 0,
                                     ModeIsAbsolute,
                                     UnitsAreInches,
                                     .75, .75,
                                     UnitsAreInches.

           CALL "P$DrawBitmap" USING WS-BITMAP2 7, 0,
                                     ModeIsAbsolute,
                                     UnitsAreInches,
                                     .75, .75,
                                     UnitsAreInches.

       WRITE-DAYS-OF-WEEK.
      *    CALL "P$SetDefaultUnits" USING UnitsAreDeviceUnits.
           Compute LF-Height Rounded = (6 * Dc-LogPixelsY)  / 72.
           CALL "P$SetFont" Using LF-HeightParam LF-Height,
                                  LF-WeightParam LF-WeightNormal,
                                  LF-FaceNameParam "Arial",
                                  LF-PitchParam LF-PitchVariable.

           COMPUTE WS-WEEKDAY-X1 = UNITS-PER-INCH * .5.
           COMPUTE WS-U-Y = UNITS-PER-INCH * .93.
           PERFORM VARYING WS-WEEKDAY-SUB FROM 1 BY 1 UNTIL
              WS-WEEKDAY-SUB > 7
               CALL "P$GetTextExtent" USING
                                          WS-DAY-OF-WEEK(WS-WEEKDAY-SUB),
                                          X-SIZE, Y-SIZE,
                                          UnitsAreDeviceUnits
               COMPUTE WS-WEEKDAY-X = WS-WEEKDAY-X1
                                    + (UNITS-PER-INCH * .5)
                                    - (X-SIZE / 2)
               CALL "P$TextOut" USING WS-DAY-OF-WEEK(WS-WEEKDAY-SUB),
                                      WS-WEEKDAY-X, WS-U-Y,
                                      ModeIsAbsolute,
                                      UnitsAreDeviceUnits
               COMPUTE WS-WEEKDAY-X1 = WS-WEEKDAY-X1 + UNITS-PER-INCH
           END-PERFORM.
           CALL "P$ClearFont".

       DRAW-CALENDAR.
           ADD .50 TO WS-SUB.
           CALL "P$SetPen" USING PenStyleSolid 2.
           PERFORM VARYING WS-Y FROM 1 BY 1.5
               UNTIL DAY-COUNTER > WS-DAYS-IN-MONTH
               PERFORM VARYING WS-X FROM WS-SUB BY 1 UNTIL WS-X > 6.50
                   OR DAY-COUNTER > WS-DAYS-IN-MONTH
                    CALL "P$DrawBox" USING WS-X, WS-Y,
                                           ModeIsAbsolute,
                                           UnitsAreInches,
                                           1, 1.5,
                                           UnitsAreInches,
                                           DrawBoxWithoutShading
                    ADD .050 TO WS-X GIVING WS-DAY-X
                    ADD .150 TO WS-Y GIVING WS-DAY-Y
                    IF DAY-COUNTER < 10
                       MOVE DAY-COUNTER(2:1) TO DAY-COUNTER-X(1:1)
                    ELSE
                       MOVE DAY-COUNTER TO DAY-COUNTER-X
                    END-IF
                    CALL "P$TextOut" USING DAY-COUNTER-X, WS-DAY-X,
                                           WS-DAY-Y,
                                           ModeIsAbsolute,
                                           UnitsAreInches
                    ADD 1 TO DAY-COUNTER
               END-PERFORM
               MOVE 0.50 TO WS-SUB
           END-PERFORM.
           CALL "P$SetPen" USING PenStyleSolid 0.
           PERFORM PREVIOUS-MONTH.
           PERFORM NEXT-MONTH.

      * Previous Month
       PREVIOUS-MONTH.
           COMPUTE WS-U-X = UNITS-PER-INCH * 3.
           COMPUTE WS-U-Y = UNITS-PER-INCH * 8.75.
           CALL "P$DrawBox" USING  WS-U-X, WS-U-Y,
                                           ModeIsAbsolute,
                                           UnitsAreDeviceUnits,
                                           2.00, 1.25,
                                           UnitsAreInches,
                                           DrawBoxWithoutShading.
           MOVE WS-PREV-DATE TO WS-TEMP-DATE.
           PERFORM PRINT-HEADINGS.

           CALL "P$SetPen" USING PenStyleSolid 0.
           COMPUTE WS-U-X  = UNITS-PER-INCH * 3.02.
           COMPUTE WS-U-X2 = UNITS-PER-INCH * 4.95.
           CALL "P$DrawLine" USING WS-U-X, WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits,
                                   WS-U-X2, WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.

           MOVE SPACES TO DAY-COUNTER-X.
           COMPUTE WS-U-X-OFFSET = UNITS-PER-INCH * 3.1.
           COMPUTE WS-U-Y = WS-U-Y + (UNITS-PER-INCH * .15).
           COMPUTE WS-U-X = (WS-SUB * UNITS-PER-INCH * .28)
                          + WS-U-X-OFFSET.
           COMPUTE WS-U-Y2 = UNITS-PER-INCH * .15.
           COMPUTE WS-U-X2 = UNITS-PER-INCH * .28.
           COMPUTE WS-U-X3 = UNITS-PER-INCH * 5.
           PERFORM DRAW-SMALL-CALENDAR.
      * Draw Shadow
           CALL "P$SetPen" USING PenStyleSolid 10.
           COMPUTE WS-U-X = UNITS-PER-INCH * 5.01.
           COMPUTE WS-U-Y = UNITS-PER-INCH * 8.81.
           CALL "P$MoveTo" USING   WS-U-X, WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.
           COMPUTE WS-U-Y = UNITS-PER-INCH * 10.
           CALL "P$LineTo" USING   WS-U-X, WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.

           COMPUTE WS-U-X = UNITS-PER-INCH * 3.05.
           COMPUTE WS-U-Y = UNITS-PER-INCH * 10.01.
           CALL "P$MoveTo" USING   WS-U-X, WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.
           COMPUTE WS-U-X = UNITS-PER-INCH * 5.01.
           CALL "P$LineTo" USING   WS-U-X, WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.
           CALL "P$SetPen" USING PenStyleSolid 0.

      * Next Month
       NEXT-MONTH.
           COMPUTE WS-U-X = UNITS-PER-INCH * 5.50.
           COMPUTE WS-U-Y = UNITS-PER-INCH * 8.75.
           CALL "P$DrawBox" USING  WS-U-X, WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits,
                                   2.00, 1.25,
                                   UnitsAreInches,
                                   DrawBoxWithoutShading.
           MOVE WS-NEXT-DATE TO WS-TEMP-DATE.
           PERFORM PRINT-HEADINGS.

           CALL "P$SetPen" USING PenStyleSolid 0.
           COMPUTE WS-U-X  = UNITS-PER-INCH * 5.55.
           COMPUTE WS-U-X2 = UNITS-PER-INCH * 7.45.
           CALL "P$DrawLine" USING WS-U-X,  WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits,
                                   WS-U-X2, WS-U-Y
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.

           COMPUTE WS-U-X-OFFSET = UNITS-PER-INCH * 5.6.
           COMPUTE WS-U-Y = WS-U-Y + (UNITS-PER-INCH * .15).
           COMPUTE WS-U-X = (WS-SUB * UNITS-PER-INCH * .28)
                          + WS-U-X-OFFSET.
           MOVE SPACES TO DAY-COUNTER-X.
           COMPUTE WS-U-Y2 = UNITS-PER-INCH * .15
           COMPUTE WS-U-X2 = UNITS-PER-INCH * .28
           COMPUTE WS-U-X3 = UNITS-PER-INCH * 7.50
           PERFORM DRAW-SMALL-CALENDAR.
      * Draw Shadow
           CALL "P$SetPen" USING PenStyleSolid 10.
           COMPUTE WS-U-X = UNITS-PER-INCH * 7.51.
           COMPUTE WS-U-Y = UNITS-PER-INCH * 8.81.
           CALL "P$MoveTo" USING   WS-U-X, WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.
           COMPUTE WS-U-Y = UNITS-PER-INCH * 10.
           CALL "P$LineTo" USING   WS-U-X, WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.

           COMPUTE WS-U-X = UNITS-PER-INCH * 5.56.
           COMPUTE WS-U-Y = UNITS-PER-INCH * 10.01.
           CALL "P$MoveTo" USING   WS-U-X, WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.
           COMPUTE WS-U-X = UNITS-PER-INCH * 7.51.
           CALL "P$Lineto" USING   WS-U-X, WS-U-Y,
                                   ModeIsAbsolute,
                                   UnitsAreDeviceUnits.

           CALL "P$SetPen" USING PenStyleSolid 0.

       ACCEPT-MONTH-YEAR.
           DISPLAY "Enter Month: " LINE 6 COLUMN 1 ERASE EOS.
           DISPLAY "Enter Year:  " LINE 7 COLUMN 1.
      *    DISPLAY "Title Text:  " LINE 8 COLUMN 1.
           ACCEPT WS-ENTER-MM NO BEEP TAB PROMPT LINE 6 COLUMN 14
               ON EXCEPTION WS-EXC-NUM
               DISPLAY "  INVALID MONTH ENTERED!" LINE 23 COL 22 REVERSE
               CALL "C$Delay" USING WS-DELAY
               GO TO ACCEPT-MONTH-YEAR.

           IF NOT VALID-VALUES
               DISPLAY "  INVALID MONTH ENTERED!" LINE 23 COL 22 REVERSE
               CALL "C$DELAY" USING WS-DELAY
               GO TO ACCEPT-MONTH-YEAR.

           ACCEPT  WS-ENTER-YY NO BEEP TAB PROMPT LINE 7 COL 14
               ON EXCEPTION WS-EXC-NUM
                   DISPLAY " NONNUMERIC DATA ENTERED! " LINE 23 COL 22
                      REVERSE
               CALL "C$DELAY" USING WS-DELAY
               GO TO ACCEPT-MONTH-YEAR.

      *    ACCEPT  WS-TITLE-TEXT NO BEEP TAB PROMPT LINE 8 COL 14.
           Move "RM/COBOL Calendar Example Program" TO  WS-TITLE-TEXT.

           MOVE WS-ENTER-DATE TO WS-TEMP-DATE.

      * Set the next month and the previous month
           IF WS-ENTER-MM = 12
               MOVE 01 TO WS-NEXT-MM
               ADD 1 TO WS-ENTER-YY GIVING WS-NEXT-YY
           ELSE
               ADD 1 TO WS-ENTER-MM GIVING WS-NEXT-MM
               MOVE     WS-ENTER-YY TO  WS-NEXT-YY
           END-IF.

           IF WS-ENTER-MM = 01
               MOVE 12 TO WS-PREV-MM
               SUBTRACT 1 FROM WS-ENTER-YY GIVING WS-PREV-YY
           ELSE
               SUBTRACT 1 FROM WS-ENTER-MM GIVING WS-PREV-MM
               MOVE     WS-ENTER-YY TO  WS-PREV-YY
           END-IF.

      * Get first day of month, determine if month is leap month
       GET-DATE-INFO.
           PERFORM COMPUTE-LEAP-YEAR.
           PERFORM COMPUTE-DAY-OF-WEEK.
           MOVE DA-DAY-NO    TO WS-SUB.
           MOVE 1 TO DAY-COUNTER.

       COMPUTE-LEAP-YEAR.
           SET LEAP-YEAR TO FALSE.
           DIVIDE WS-TEMP-YY BY 100 GIVING WS-RESULT REMAINDER
                                                  WS-REMAINDER
           IF WS-REMAINDER = ZERO
               DIVIDE WS-TEMP-YY BY 400 GIVING WS-RESULT REMAINDER
                                                      WS-REMAINDER
               IF WS-REMAINDER = ZERO
                   SET LEAP-YEAR TO TRUE
               ELSE
                   SET LEAP-YEAR TO FALSE
               END-IF
           ELSE
               DIVIDE WS-TEMP-YY BY 4 GIVING WS-RESULT REMAINDER
                                                    WS-REMAINDER
               IF WS-REMAINDER = ZERO
                   SET LEAP-YEAR TO TRUE
               ELSE
                   SET LEAP-YEAR TO FALSE
              END-IF
           END-IF.

           IF LEAP-YEAR
               MOVE 29                 TO WS-MONTH-DAYS(2)
           ELSE
               MOVE 28                 TO WS-MONTH-DAYS(2)
           END-IF.
           MOVE WS-MONTH-DAYS(WS-TEMP-MM) TO WS-DAYS-IN-MONTH.

       COMPUTE-DAY-OF-WEEK.
      *
      * Returns day of week for First day of the month.
      * 0=Sunday, 1=Monday, etc..
      *
           COMPUTE WS-A  = (14 - WS-TEMP-MM) / 12
           COMPUTE WS-YY = (WS-TEMP-YY - WS-A)
           COMPUTE WS-M  = (WS-TEMP-MM + (12 * WS-A)) - 2.

           COMPUTE WS-INTERNAL    = ((31 * WS-M) / 12).
           COMPUTE WS-Y-LEAPS     =  WS-YY / 4.
           COMPUTE WS-Y-CENTURIES =  WS-YY / 100.
           COMPUTE WS-Y-400S      =  WS-YY / 400.
           COMPUTE WS-D = 1 + WS-YY + WS-Y-LEAPS - WS-Y-CENTURIES
                            + WS-Y-400S + WS-INTERNAL.
           DIVIDE WS-D BY 7 GIVING WS-RESULT REMAINDER DA-DAY-NO.

       PRINT-HEADINGS.
           PERFORM GET-DATE-INFO.

           Compute LF-Height Rounded = (8 * Dc-LogPixelsY) / 72.
           CALL "P$SetFont" Using LF-HeightParam LF-Height,
                                  LF-WeightParam LF-WeightNormal,
                                  LF-FaceNameParam "Arial",
                                  LF-PitchParam LF-PitchVariable.

           PERFORM CENTER-TITLE.

           COMPUTE WS-TITLE-X = WS-U-X + UNITS-PER-INCH - (X-SIZE / 2).

           CALL "P$GetTextMetrics" Using TextMetrics.
           COMPUTE WS-U-Y = WS-U-Y - (TM-Height / 2).

           CALL "P$SetTextPosition" USING WS-TITLE-X, WS-U-Y,
                                          PositionIsTop,
                                          ModeIsAbsolute,
                                          UnitsAreDeviceUnits.

           CALL "P$SetPen"  USING PenStyleNull.
           CALL "P$SetBoxShade" USING ColorBlack, 10.
           CALL "P$TextOut" USING WS-TITLE-OUT, Omitted, Omitted,
                                          ModeIsAbsolute,
                                          UnitsAreDeviceUnits,
                                          TextOutWithBox,
                                          TextOutWithShading.

           COMPUTE WS-U-Y = WS-U-Y + (TM-Height * 2.5).
           COMPUTE WS-U-X = WS-U-X + (UNITS-PER-INCH * .12).
           PERFORM VARYING WS-WEEKDAY-SUB FROM 1 BY 1 UNTIL
              WS-WEEKDAY-SUB > 7
               CALL "P$TextOut" USING
                                WS-DAY-OF-WEEK(WS-WEEKDAY-SUB) (1:1),
                                WS-U-X, WS-U-Y,
                                ModeIsAbsolute,
                                UnitsAreDeviceUnits
               COMPUTE WS-U-X = WS-U-X + UNITS-PER-INCH * .28
           END-PERFORM.

      *    COMPUTE WS-U-Y  = WS-U-Y + (UNITS-PER-INCH * .10).

       CENTER-TITLE.
           MOVE SPACES TO WS-TITLE-OUT.
           STRING  SPACE      DELIMITED BY SIZE,
                   WS-MONTH(WS-TEMP-MM) DELIMITED BY SPACES,
                   SPACE      DELIMITED BY SIZE,
                   WS-TEMP-YY DELIMITED BY SIZE,
                   SPACE      DELIMITED BY SIZE,
               INTO WS-TITLE-OUT
           ON OVERFLOW
               DISPLAY "  OVERFLOW ERROR!  " LINE 23 COL 22 REVERSE
               CALL "C$DELAY" USING WS-DELAY
               DISPLAY SPACE LINE 23 COL 22 ERASE EOL
           END-STRING.
      *Find the size of the month/year title.
           CALL "P$GetTextExtent" USING WS-TITLE-OUT,
                                        X-SIZE, Y-SIZE,
                                        UnitsAreDeviceUnits.


       DRAW-SMALL-CALENDAR.
           PERFORM VARYING WS-U-Y FROM WS-U-Y BY WS-U-Y2
               UNTIL DAY-COUNTER > WS-DAYS-IN-MONTH
               PERFORM VARYING WS-U-X FROM WS-U-X BY WS-U-X2
                   UNTIL WS-U-X > WS-U-X3
                   OR DAY-COUNTER > WS-DAYS-IN-MONTH
                      IF DAY-COUNTER < 10
                         MOVE DAY-COUNTER(2:1) TO DAY-COUNTER-X(2:1)
                      ELSE
                         MOVE DAY-COUNTER TO DAY-COUNTER-X
                      END-IF
                      CALL "P$TextOut" USING DAY-COUNTER-X,
                                             WS-U-X, WS-U-Y,
                                             ModeIsAbsolute,
                                             UnitsAreDeviceUnits
                     ADD 1 TO DAY-COUNTER
               END-PERFORM
               MOVE WS-U-X-OFFSET TO WS-U-X
           END-PERFORM.
