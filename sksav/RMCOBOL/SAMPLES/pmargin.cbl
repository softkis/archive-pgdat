       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PMARGIN.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION:
      *
      * PMARGIN - This will test P$SetLeftMargin and P$SetTopMargin.
      *
      * Version Identification:
      *   $Revision:   1.1.1.1  $
      *   $Date:   27 Sep 1999 14:57:42  $
      *
      *********************************************************************

       DATE-WRITTEN.     03/22/1999.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL-85.
       OBJECT-COMPUTER.  RMCOBOL-85.
       SPECIAL-NAMES.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT PRINTER ASSIGN TO PRINT, "PRINTER?".
       DATA DIVISION.
       FILE SECTION.
       FD  PRINTER.
       01  FD-RECORD                       PIC X(80).
       01  PRINT-RECORD                    PIC X(80).

       WORKING-STORAGE SECTION.

       01  CHECK-FOR-ERRS              PIC 9  VALUE 0.
           88  NO-ERRS-ENCOUNTERED     VALUE 0.
           88  ERRS-ENCOUNTERED        VALUE 1.

       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(15) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(15) VALUE SPACES.
           05  FILLER                  PIC X(31) VALUE
           "RUNTIME RESULTS - PMARGIN   ".

      *  Modify the following output line to describe the test.
       01  DESCRIP-LINE.
           05  FILLER                  PIC X(25)  VALUE
           "**   PMARGIN  - Test For ".
           05  FILLER                  PIC X(27) VALUE
           "Set Left and Top Margins **".
      *     1234567890123456789012345678901234567890

       77  DATE-CONVERT                PIC 9(5)V9(4) VALUE 10000.0001.
       78  DATE-COMP                   VALUE IS DATE-COMPILED.
       01  DOW-SUB                    PIC 9(1)  VALUE ZERO.
       01  DAY-OF-WEEK-TAB            PIC X(21) VALUE
           "MonTueWedThuFriSatSun".
       01  DAY-OF-WEEK-RED REDEFINES DAY-OF-WEEK-TAB.
           05  DAY-OF-THE-WEEK        PIC X(3) OCCURS 7 TIMES.

       01  TIME-DATE-LINE.
           05  FILLER                 PIC X(6)  VALUE "Date: ".
           05  MM                     PIC XX.
           05  SLASH-1                PIC X     VALUE "/".
           05  DD                     PIC XX.
           05  SLASH-2                PIC X     VALUE "/".
           05  YY                     PIC X(5).
           05  FILLER                 PIC X     VALUE "(".
           05  DOW                    PIC X(3).
           05  FILLER                 PIC X     VALUE ")".
           05  FILLER                 PIC X(13) VALUE "   Run Time: ".
           05  HH                     PIC XX.
           05  DIV-1                  PIC X     VALUE ":".
           05  MN                     PIC XX.
           05  DIV-2                  PIC X     VALUE ":".
           05  SS                     PIC XX.
           05  FILLER                 PIC X(5).
           05  FILLER                 PIC X(15) VALUE "Date Compiled: ".
           05  DATE-COMPILE           PIC 99/99/9999.

       COPY "rminfo.cpy".
       COPY "sysinfo.cpy".
       COPY "printinf.cpy".

       01  RM-LINE.
           05  RM-USER                 PIC X(9)  VALUE "User:    ".
           05  RM-SUB-LINE.
               10  FILLER              PIC X(16) VALUE
               " using RM/COBOL ".
               10  RM-LINE-VersionNumber
                                       PIC X(8).
               10  FILLER              PIC X(1)  VALUE "(".
               10  RM-Line-RegistrationNumber PIC X(18).
               10  FILLER              PIC X(1)  VALUE ")".
               10  FILLER              PIC X(12) VALUE SPACES.

       01  PRT-LINE.
           05  FILLER                 PIC X(9)  VALUE "Printer: ".
           05  Prt-Line-PrinterName   PIC X(71).

       01  UNDER-LINE.
           05  FILLER                  PIC X(80) VALUE ALL "-".

       01 WS-ARGS.
           03 Ws-Line       PIC 9(2)                  VALUE 00.
           03 Ws-SizeWidth  PIC 9(4) Binary           VALUE 00.
           03 Ws-SizeHeight PIC 9(4) Packed-Decimal   VALUE 00.
       01  WS-RECORD.
           03 WS-Text       PIC X(15) value "Left Margin at ".
           03 Ws-Width-Out  PIC Z9.
           03               PIC X(1) Value Spaces.
           03 WS-Units      PIC X(13).
              88  Flag-Inches       value "Inches ".
              88  Flag-Metric       value "Centimeters ".
              88  Flag-Characters   value "Characters ".
              88  Flag-Device-Units value "Device Units ".
           03               PIC X(6) Value "Line ".
           03 Ws-Line-Out   PIC Z9.

       PROCEDURE DIVISION.
       DRIVER-SECTION SECTION.
      ** Main driver paragraph **
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 2 LINES.
           CLOSE PRINTER.
           STOP RUN.

      ** Prepare heading information **
       INITIAL-SETUP.
           OPEN OUTPUT PRINTER.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           ACCEPT DOW-SUB       FROM DAY-OF-WEEK.
           MOVE DAY-OF-THE-WEEK(DOW-SUB)   TO DOW.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.
           COMPUTE DATE-COMPILE = DATE-COMP * DATE-CONVERT.

      ** Print heading **
       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1      AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2      AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE   AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "C$SetDevelopmentMode".
           CALL "C$GetRMInfo"      USING RMInformation.
           CALL "C$GetSysInfo"     USING SystemInformation.
           CALL "P$GetPrinterInfo" USING PrinterInformation.

           MOVE  RM-VersionNumber      TO RM-Line-VersionNumber.
           MOVE  RM-RegistrationNumber TO RM-Line-RegistratioNnumber.
           MOVE  PI-PrinterName        TO Prt-Line-PrinterName.

           STRING RM-USER       Delimited by Size,
                  Sys-UserName  Delimited by Spaces,
                  RM-SUB-LINE   Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           STRING "Machine: "   Delimited by Size,
                  Sys-NodeName  Delimited by Spaces,
                  " running "   Delimited by Size,
                  Sys-Name      Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM PRT-LINE   AFTER 1 LINES.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

       MAIN SECTION.
       MAIN-PARA.
           CALL "C$SETDEVELOPMENTMODE".

           WRITE Print-RECORD FROM "Default Left Margin " After 2.
           WRITE Print-RECORD FROM Spaces After 1.

           Move 4 to Ws-SizeWidth.
           Set Flag-Inches to True.
           CALL "P$SetLeftMargin"  using WS-SizeWidth, "Inches".
           perform print-start 5 times.

           Set Flag-Metric to True.
           Move 6 To Ws-SizeWidth.
           CALL "P$SetLeftMargin"  using WS-SizeWidth, "M".
           Perform Print-Start 5 times.

           Set Flag-Characters to True.
           Move 10 to Ws-SizeWidth.
           CALL "P$SetLeftMargin"  using WS-SizeWidth, "C".
           Perform print-start 5 times.

           Set Flag-Device-Units to True.
           Move 75 to Ws-SizeWidth.
           CALL "P$SetLeftMargin"  using WS-SizeWidth, "D".
           Perform print-start 5 times.

           Move 0 to Ws-SizeWidth.
           CALL "P$SetLeftMargin"  using WS-SizeWidth.
           WRITE Print-RECORD FROM "Left Margin Set to 0 " After 2.

           Move 3 to Ws-SizeHeight.
           CALL "P$SetTopMargin"  using Ws-SizeHeight, "Inches".
           WRITE Print-RECORD FROM "Top Margin 3 inches Left Margin 0"
               AFTER PAGE.

           WRITE PRINT-RECORD FROM Spaces After 1.

       PRINT-IT SECTION 66.
       PRINT-START.
           Add 1 To Ws-Line
           Move Ws-Line To Ws-Line-Out.
           Move Ws-SizeWidth To Ws-Width-Out.
           Write Print-Record From WS-Record After 1.

       END PROGRAM  PMARGIN.
