       IDENTIFICATION DIVISION.
       PROGRAM-ID.       PDRAW.

      *********************************************************************
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *********************************************************************
      *
      * PROGRAM DESCRIPTION:
      *
      * PDRAW -  Test P$DrawBitmap, P$DrawBox, P$DrawLine.
      *
      * Version Identification:
      *   $Revision:   1.1.1.1  $
      *   $Date:   27 Sep 1999 14:48:56  $
      *
      *********************************************************************

       DATE-WRITTEN.     01/25/1999

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           SELECT PRINTER ASSIGN TO PRINT, "PRINTER?".

       DATA DIVISION.
       FILE SECTION.
       FD  PRINTER.
       01  PRINT-RECORD       PIC X(132).
       WORKING-STORAGE SECTION.

       01  TIME-AND-DATE.
           05  CURRENT-DATE.
               10  YY                  PIC X(4).
               10  MM                  PIC XX.
               10  DD                  PIC XX.
           05  CURRENT-TIME.
               10  HH                  PIC XX.
               10  MN                  PIC XX.
               10  SS                  PIC XX.
               10  CC                  PIC XX.

       01  HEADING-1.
           05  FILLER                  PIC X(14) VALUE SPACES.
           05  FILLER                  PIC X(26) VALUE
           "LIANT SOFTWARE CORPORATION".

       01  HEADING-2.
           05  FILLER                  PIC X(15) VALUE SPACES.
           05  FILLER                  PIC X(31) VALUE
           "RUNTIME RESULTS - PDRAW   ".

       01  DESCRIP-LINE.
           05  FILLER                  PIC X(23)  VALUE
           "**   PDRAW  - Test For ".
           05  FILLER                  PIC X(29) VALUE
           "P$Draw Bitmap, Line & Box  **".

       77  DATE-CONVERT                PIC 9(5)V9(4) VALUE 10000.0001.
       78  DATE-COMP                   VALUE IS DATE-COMPILED.
       01  DOW-SUB                     PIC 9(1)  VALUE ZERO.
       01  DAY-OF-WEEK-TAB             PIC X(21) VALUE
           "MonTueWedThuFriSatSun".
       01  DAY-OF-WEEK-RED REDEFINES DAY-OF-WEEK-TAB.
           05  DAY-OF-THE-WEEK        PIC X(3) OCCURS 7 TIMES.

       01  TIME-DATE-LINE.
           05  FILLER                 PIC X(6)  VALUE "Date: ".
           05  MM                     PIC XX.
           05  SLASH-1                PIC X     VALUE "/".
           05  DD                     PIC XX.
           05  SLASH-2                PIC X     VALUE "/".
           05  YY                     PIC X(5).
           05  FILLER                 PIC X     VALUE "(".
           05  DOW                    PIC X(3).
           05  FILLER                 PIC X     VALUE ")".
           05  FILLER                 PIC X(13) VALUE "   Run Time: ".
           05  HH                     PIC XX.
           05  DIV-1                  PIC X     VALUE ":".
           05  MN                     PIC XX.
           05  DIV-2                  PIC X     VALUE ":".
           05  SS                     PIC XX.
           05  FILLER                 PIC X(5).
           05  FILLER                 PIC X(15) VALUE "Date Compiled: ".
           05  DATE-COMPILE           PIC 99/99/9999.

       COPY "rminfo.cpy".
       COPY "sysinfo.cpy".
       COPY "printinf.cpy".

       01  RM-LINE.
           05  RM-USER                 PIC X(9)  VALUE "User:    ".
           05  RM-SUB-LINE.
               10  FILLER              PIC X(16) VALUE
               " using RM/COBOL ".
               10  RM-LINE-VersionNumber
                                       PIC X(8).
               10  FILLER              PIC X(1)  VALUE "(".
               10  RM-Line-RegistrationNumber PIC X(18).
               10  FILLER              PIC X(1)  VALUE ")".
               10  FILLER              PIC X(12) VALUE SPACES.

       01  PRT-LINE.
           05  FILLER                 PIC X(9)  VALUE "Printer: ".
           05  Prt-Line-PrinterName   PIC X(71).

       01  UNDER-LINE.
           05  FILLER                  PIC X(80) VALUE ALL "-".

       copy "windefs.cpy".

       PROCEDURE DIVISION.
       DRIVER-SECTION SECTION.
       CONTROL-MODULE.
           PERFORM INITIAL-SETUP.
           PERFORM PRINT-HEADINGS.
           PERFORM MAIN.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 2 LINES.
           CLOSE PRINTER.
           STOP RUN.

       INITIAL-SETUP.
           OPEN OUTPUT PRINTER.
           ACCEPT TIME-AND-DATE FROM DATE-AND-TIME.
           ACCEPT DOW-SUB       FROM DAY-OF-WEEK.
           MOVE DAY-OF-THE-WEEK(DOW-SUB)   TO DOW.
           MOVE CORRESPONDING CURRENT-DATE TO TIME-DATE-LINE.
           MOVE CORRESPONDING CURRENT-TIME TO TIME-DATE-LINE.
           COMPUTE DATE-COMPILE = DATE-COMP * DATE-CONVERT.

       PRINT-HEADINGS.
           WRITE PRINT-RECORD FROM HEADING-1      AFTER PAGE.
           WRITE PRINT-RECORD FROM HEADING-2      AFTER 1 LINES.
           WRITE PRINT-RECORD FROM DESCRIP-LINE   AFTER 2 LINES.
           WRITE PRINT-RECORD FROM TIME-DATE-LINE AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           CALL "C$SetDevelopmentMode".
           CALL "C$GetRMInfo"      USING RMInformation.
           CALL "C$GetSysInfo"     USING SystemInformation.
           CALL "P$GetPrinterInfo" USING PrinterInformation.

           MOVE  RM-VersionNumber      TO RM-Line-VersionNumber.
           MOVE  RM-RegistrationNumber TO RM-Line-RegistratioNnumber.
           MOVE  PI-PrinterName        TO Prt-Line-PrinterName.

           STRING RM-USER       Delimited by Size,
                  Sys-UserName  Delimited by Spaces,
                  RM-SUB-LINE   Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 2 LINES.
           MOVE SPACES TO PRINT-RECORD.

           STRING "Machine: "   Delimited by Size,
                  Sys-NodeName  Delimited by Spaces,
                  " running "   Delimited by Size,
                  Sys-Name      Delimited by Size,
               INTO PRINT-RECORD.
           WRITE PRINT-RECORD                 AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

           WRITE PRINT-RECORD FROM PRT-LINE   AFTER 1 LINES.
           WRITE PRINT-RECORD FROM UNDER-LINE AFTER 1 LINES.
           MOVE SPACES TO PRINT-RECORD.

       MAIN SECTION.
       TEST-1.
      *
           WRITE PRINT-RECORD FROM
               "Test will draw bitmap in upper right corner." AFTER 2.
           WRITE PRINT-RECORD FROM "Set PEN type and draw an 8-inch line
      -" and 2 quarter-inch boxes normal and shaded." after 1.

           CALL "P$DrawBitmap" USING "RMLOGO.BMP",
                                     6, 0,
                                     ModeIsAbsolute,
                                     UnitsAreInches,
                                     4.0, 2.5,
                                     UnitsAreMetric.

           CALL "P$SetBoxShade" USING ColorLightGray, 50.

           CALL "P$SetPen"  USING PenStyleSolid.
           CALL "P$TextOut" USING "Solid line  " 0, 3.0.
           CALL "P$DrawBox" USING 2.5, 3.0,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithoutShading.
           CALL "P$DrawBox" USING 3.5, 3.0,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithShading.
           CALL "P$DrawLine" USING 0, 3.25,
                                   ModeIsAbsolute,
                                   UnitsAreInches,
                                   8, 3.25.

           CALL "P$SetPen"  USING PenStyleDash.
           CALL "P$TextOut" USING "Dash line  " 0, 3.5.
           CALL "P$DrawBox" USING 2.5, 3.5,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithoutShading.
           CALL "P$DrawBox" USING 3.5, 3.5,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithShading.
           CALL "P$DrawLine" USING 0, 3.75,
                                   ModeIsAbsolute,
                                   UnitsAreInches,
                                   8, 3.75.

           CALL "P$SetPen"  USING PenStyleDot.
           CALL "P$TextOut" USING "Dot Line "  0, 4.0.
           CALL "P$DrawBox" USING 2.5, 4.0,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithoutShading.
           CALL "P$DrawBox" USING 3.5, 4.0,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithShading.
           CALL "P$DrawLine" USING 0, 4.25,
                                   ModeIsAbsolute,
                                   UnitsAreInches,
                                   8, 4.25.

           CALL "P$SetPen"  USING PenStyleDashDot.
           CALL "P$TextOut" USING "DashDot Line  "  0, 4.50.
           CALL "P$DrawBox" USING 2.5, 4.50,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithoutShading.
           CALL "P$DrawBox" USING 3.5, 4.50,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithShading.
           CALL "P$DrawLine" USING 0, 4.75,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  8, 4.75.

           CALL "P$SetPen"  USING PenStyleDashDotDot.
           CALL "P$TextOut" USING "DashDotDot Line  "  0, 5.0.
           CALL "P$DrawBox" USING 2.5, 5.0,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithoutShading.
           CALL "P$DrawBox" USING 3.5, 5.0,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithShading.
           CALL "P$DrawLine" USING 0, 5.25,
                                   ModeIsAbsolute,
                                   UnitsAreInches,
                                   8, 5.25.

           CALL "P$SetPen"  USING PenStyleNull.
           CALL "P$TextOut" USING "Null Line  "  0, 5.50.
           CALL "P$DrawBox" USING 2.5, 5.50,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithoutShading.
           CALL "P$DrawBox" USING 3.5, 5.50,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  .25, .25,
                                  UnitsAreInches,
                                  DrawBoxWithShading.
           CALL "P$DrawLine" USING 0, 5.75,
                                  ModeIsAbsolute,
                                  UnitsAreInches,
                                  8, 5.75.

       END PROGRAM PDRAW.
