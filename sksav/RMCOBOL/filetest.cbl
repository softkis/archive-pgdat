       IDENTIFICATION DIVISION.
       PROGRAM-ID.  "filetest".
      *
      * Title:  filetest.cbl
      *        RM/COBOL File System Test
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      * Version Identification:
      *   $Revision:   6.3.1.2  $
      *   $Date:   21 Sep 1999 13:09:02  $
      *
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.

           SELECT SEQ-FILE
              ASSIGN TO RANDOM, "seqfl".

           SELECT REL-FILE
               ASSIGN TO RANDOM, "relfl"
               ORGANIZATION IS RELATIVE,
               ACCESS MODE IS DYNAMIC,
               RELATIVE KEY IS REL-KEY.

           SELECT INX-FILE
               ASSIGN TO RANDOM, "inxfl"
               ORGANIZATION IS INDEXED,
               ACCESS MODE IS DYNAMIC,
               RECORD KEY IS INX-KEY,
               ALTERNATE RECORD KEY IS ALT-KEY.
      /
       DATA DIVISION.

       FILE SECTION.

       FD  SEQ-FILE                    BLOCK CONTAINS 8 RECORDS
                                       DATA RECORD IS SEQ-RECORD.
       01  SEQ-RECORD                  PIC X(30).

       FD  REL-FILE                    BLOCK CONTAINS 8 RECORDS
                                       DATA RECORD IS REL-RECORD.
       01  REL-RECORD                  PIC X(30).

       FD  INX-FILE                    BLOCK CONTAINS 8 RECORDS
                                       DATA RECORD IS INX-RECORD.
       01  INX-RECORD.
           02  INX-KEY                 PIC X(3).
           02                          PIC X(6).
           02  ALT-KEY                 PIC X(3).
           02  INX-DAT                 PIC X(3).
           02                          PIC X(15).

       WORKING-STORAGE SECTION.

       COPY "version.cpy".

       01  TITLE.
           05  PIC X(46) VALUE
           "RM/COBOL Verify file I/O operations - Version ".
           05  PIC X(4)  VALUE CURRENT-VERSION.

       01  REL-KEY                     PIC 999.

       01  KEY-NUM                     PIC 999.

       01  RETURN-KEY                  PIC X.

       01  FUNCTION-NUMBER             PIC 9.

       01  COUNTER                     PIC 999.

       01  ALT-COUNT                   PIC 999.
      /
       PROCEDURE DIVISION.

       SECTION-1 SECTION 1.

       DISPLAY-MENU.
           DISPLAY TITLE, HIGH, LINE 1, ERASE.

           DISPLAY "1. Sequential File Test", LOW, LINE 4 POSITION 10.
           DISPLAY "2. Relative File Test",   LOW,        POSITION 10.
           DISPLAY "3. Indexed File Test",    LOW,        POSITION 10.
           DISPLAY "4. Exit Program",         LOW,        POSITION 10.

           DISPLAY "Each of these tests will write 100 records",
                                              LOW, LINE 10,
                   " and then read the records.", LOW.

       ACCEPT-FUNCTION-NUMBER.
           DISPLAY "Enter test number", LOW, LINE 16, POSITION 40.
           ACCEPT FUNCTION-NUMBER, LINE 16, POSITION 66, TAB.
           DISPLAY SPACES, ERASE EOL, LINE 16, POSITION 40.
           GO TO PARA-3,
                 PARA-2,
                 PARA-1,
                 EXIT-PARA DEPENDING ON FUNCTION-NUMBER.
           GO TO ACCEPT-FUNCTION-NUMBER.

       DISPLAY-MENU-LOOP.
           DISPLAY "Type any key to continue....", HIGH,
                   LINE 22 POSITION 40.
           ACCEPT RETURN-KEY POSITION 0.
           GO TO DISPLAY-MENU.

       RESET-COUNTER.
           MOVE ZERO TO COUNTER.
      /
       SECTION-2 SECTION 2.

       WRITE-INX-RECORD.
           ADD 1 TO COUNTER.
           COMPUTE ALT-COUNT = 101 - COUNTER.
           MOVE COUNTER TO INX-DAT, INX-KEY.
           MOVE ALT-COUNT TO ALT-KEY.
           DISPLAY "Writing by prime key with alternate. Record ",
                       LOW, LINE 12 POSITION 10,
                   COUNTER, LOW, CONVERT.
           WRITE INX-RECORD.

       READ-BY-INX-KEY.
           ADD 1 TO KEY-NUM.
           MOVE KEY-NUM TO INX-KEY.
           DISPLAY "Reading by prime key. Record ",
                       LOW, LINE 13 POSITION 10,
                   KEY-NUM, LOW, CONVERT.
           READ INX-FILE RECORD KEY IS INX-KEY
               INVALID KEY ADD 1 TO COUNTER.
           IF INX-DAT NOT = KEY-NUM ADD 1 TO COUNTER.

       READ-BY-ALT-KEY.
           MOVE KEY-NUM TO ALT-KEY.
           COMPUTE ALT-COUNT = 101 - KEY-NUM.
           DISPLAY "Reading by alternate key. Record ",
                       LOW, LINE 14 POSITION 10,
                   ALT-COUNT, LOW, CONVERT.
           READ INX-FILE RECORD KEY IS ALT-KEY
               INVALID KEY ADD 1 TO COUNTER.
           IF INX-DAT NOT = ALT-COUNT ADD 1 TO COUNTER.
           SUBTRACT 1 FROM KEY-NUM.

       READ-PREV-INX-RECORD.
           SUBTRACT 1 FROM KEY-NUM.
           DISPLAY "Reading previous by prime key. Record ",
                       LOW, LINE 15 POSITION 10,
                   KEY-NUM, LOW, CONVERT.
           READ INX-FILE PREVIOUS RECORD
               AT END ADD 1 TO COUNTER.
           IF INX-DAT NOT = KEY-NUM ADD 1 TO COUNTER.
      /
       WRITE-REL-RECORD.
           MOVE SPACES TO REL-RECORD.
           MOVE COUNTER TO REL-KEY.
           MOVE COUNTER TO REL-RECORD.
           DISPLAY "Writing relative record ",
                       LOW, LINE 12, POSITION 10,
                   COUNTER, LOW, CONVERT.
           WRITE REL-RECORD.
           ADD 2 TO COUNTER.

       READ-REL-RECORD.
           ADD 1 TO KEY-NUM.
           MOVE KEY-NUM TO REL-KEY.
           DISPLAY "Reading relative record "
                       LOW, LINE 13, POSITION 10,
                   KEY-NUM, LOW, CONVERT.
           READ REL-FILE RECORD
               INVALID KEY ADD 1 TO COUNTER.
           IF REL-RECORD NOT = REL-KEY ADD 1 TO COUNTER.

       READ-PREV-REL-RECORD.
           SUBTRACT 1 FROM KEY-NUM.
           DISPLAY "Reading previous relative record ",
                       LOW, LINE 14 POSITION 10,
                   KEY-NUM, LOW, CONVERT.
           READ REL-FILE PREVIOUS RECORD
               AT END ADD 1 TO COUNTER.
           IF REL-RECORD NOT = REL-KEY ADD 1 TO COUNTER.

       WRITE-SEQ-RECORD.
           ADD 1 TO KEY-NUM.
           MOVE KEY-NUM TO SEQ-RECORD.
           DISPLAY "Writing sequential record ",
                       LOW, LINE 12, POSITION 10,
                   KEY-NUM, LOW, CONVERT.
           WRITE SEQ-RECORD.

       READ-SEQ-RECORD.
           ADD 1 TO KEY-NUM.
           DISPLAY "Reading sequential record "
                       LOW, LINE 13, POSITION 10,
                   KEY-NUM, LOW, CONVERT.
           READ SEQ-FILE RECORD.
           IF SEQ-RECORD NOT = KEY-NUM
               ADD 1 TO COUNTER.
      /
       SECTION-51 SECTION 4.

       PARA-1.
           PERFORM RESET-COUNTER.
           OPEN OUTPUT INX-FILE WITH LOCK.
           PERFORM WRITE-INX-RECORD 100 TIMES.
           CLOSE INX-FILE.
           OPEN INPUT INX-FILE WITH LOCK.
           PERFORM RESET-COUNTER.
           MOVE 0 TO KEY-NUM.
           PERFORM READ-BY-INX-KEY 100 TIMES.
           PERFORM READ-BY-ALT-KEY 100 TIMES.
           MOVE 101 TO INX-KEY, KEY-NUM.
           START INX-FILE KEY NOT GREATER THAN INX-KEY
               INVALID KEY
                   DISPLAY "Invalid Start", HIGH LINE 18 POSITION 10.
           PERFORM READ-PREV-INX-RECORD 100 TIMES.
           CLOSE INX-FILE.
           DELETE FILE INX-FILE.
           IF COUNTER = 0
               DISPLAY "Passed!", HIGH, LINE 17, POSITION 10
           ELSE
               DISPLAY "Failed!", HIGH, BLINK, LINE 17, POSITION 10.
           GO TO DISPLAY-MENU-LOOP.

       SECTION-52 SECTION 5.

       PARA-2.
           OPEN OUTPUT REL-FILE WITH LOCK.
           MOVE 1 TO COUNTER.
           PERFORM WRITE-REL-RECORD 50 TIMES.
           MOVE 2 TO COUNTER.
           PERFORM WRITE-REL-RECORD 50 TIMES.
           PERFORM RESET-COUNTER.
           CLOSE REL-FILE.
           OPEN INPUT REL-FILE WITH LOCK.
           MOVE 0 TO KEY-NUM.
           PERFORM READ-REL-RECORD 100 TIMES.
           MOVE 101 TO KEY-NUM, REL-KEY.
           START REL-FILE KEY NOT GREATER THAN REL-KEY
               INVALID KEY
                   DISPLAY "Invalid Start", HIGH LINE 18 POSITION 10.
           PERFORM READ-PREV-REL-RECORD 100 TIMES.
           CLOSE REL-FILE.
           DELETE FILE REL-FILE.
           IF COUNTER = 0
               DISPLAY "Passed!", HIGH, LINE 16, POSITION 10
           ELSE
               DISPLAY "Failed!", HIGH, BLINK, LINE 16, POSITION 10.
           GO TO DISPLAY-MENU-LOOP.
      /
       SECTION-53 SECTION 6.

       PARA-3.
           OPEN OUTPUT SEQ-FILE WITH LOCK.
           MOVE 0 TO KEY-NUM.
           PERFORM WRITE-SEQ-RECORD 100 TIMES.
           PERFORM RESET-COUNTER.
           CLOSE SEQ-FILE.
           OPEN INPUT SEQ-FILE WITH LOCK.
           MOVE 0 TO KEY-NUM.
           PERFORM READ-SEQ-RECORD 100 TIMES.
           CLOSE SEQ-FILE.
           DELETE FILE SEQ-FILE.
           IF COUNTER = 0
               DISPLAY "Passed!", HIGH, LINE 15, POSITION 10
           ELSE
               DISPLAY "Failed!", HIGH, BLINK, LINE 15, POSITION 10.
           GO TO DISPLAY-MENU-LOOP.

       EXIT-PARA.
           EXIT PROGRAM.
           STOP RUN.
