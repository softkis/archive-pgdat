      *
      *    Print Dialog Definitions
      *
        01  PrintDialog.
            02  PD-ReturnValue                Picture X.
                88  PD-OKReturn                 Value 'Y'
                                           When False 'N'.
            02  PD-ExtendedErrorValue         Picture 9(5) Binary(2).
                88  PD-ExtErrIsCanceled         Value 0.

                88  PD-ExtErrIsStructSize       Value 1.
                88  PD-ExtErrIsInitialization   Value 2.
                88  PD-ExtErrIsNoTemplate       Value 3.
                88  PD-ExtErrIsNoHInstance      Value 4.
                88  PD-ExtErrIsLoadStrFailure   Value 5.
                88  PD-ExtErrIsFindResFailure   Value 6.
                88  PD-ExtErrIsLoadResFailure   Value 7.
                88  PD-ExtErrIsLockResFailure   Value 8.
                88  PD-ExtErrIsMemAllocFailure  Value 9.
                88  PD-ExtErrIsMemLockFailure   Value 10.
                88  PD-ExtErrIsNoHook           Value 11.
                88  PD-ExtErrIsRegisterMsgFail  Value 12.
 
                88  PD-ExtErrIsSetupFailure     Value 4097.
                88  PD-ExtErrIsParseFailure     Value 4098.
                88  PD-ExtErrIsRetDefFailure    Value 4099.
                88  PD-ExtErrIsLoadDrvFailure   Value 4100.
                88  PD-ExtErrIsGetDevModeFail   Value 4101.
                88  PD-ExtErrIsInitFailure      Value 4102.
                88  PD-ExtErrIsNoDevices        Value 4103.
                88  PD-ExtErrIsNoDefaultPrn     Value 4104.
                88  PD-ExtErrIsDNDMMismatch     Value 4105.
                88  PD-ExtErrIsCreateICFailure  Value 4106.
                88  PD-ExtErrIsPrinterNotFound  Value 4107.
                88  PD-ExtErrIsDefaultDifferent Value 4108.
 
                88  PD-ExtErrIsDialogFailure    Value 65535.
 
            02  PD-Flags.
                03  PD-AllPagesFlagValue      Picture X.
                    88  PD-AllPagesFlag         Value 'Y'
                                           When False 'N'.
                03  PD-SelectionFlagValue     Picture X.
                    88  PD-SelectionFlag        Value 'Y'
                                           When False 'N'.
                03  PD-PageNumbersFlagValue   Picture X.
                    88  PD-PageNumbersFlag      Value 'Y'
                                           When False 'N'.
                03  PD-NoSelectionFlagValue   Picture X.
                    88  PD-NoSelectionFlag      Value 'Y'
                                           When False 'N'.
                03  PD-NoPageNumbersFlagValue Picture X.
                    88  PD-NoPageNumbersFlag    Value 'Y'
                                           When False 'N'.
                03  PD-CollateFlagValue       Picture X.
                    88  PD-CollateFlag          Value 'Y'
                                           When False 'N'.
                03  PD-PrintSetupFlagValue       Picture X.
                    88  PD-PrintSetupFlag       Value 'Y'
                                           When False 'N'.
                03  PD-PrintToFileFlagValue   Picture X.
                    88  PD-PrintToFileFlag      Value 'Y'
                                           When False 'N'.
                03  PD-NoWarningFlagValue     Picture X.
                    88  PD-NoWarningFlag        Value 'Y'
                                           When False 'N'.
                03  PD-UseDevModeCopiesFlagValue Picture X.
                    88  PD-UseDevModeCopiesFlag Value 'Y'
                                           When False 'N'.
                03  PD-DisablePrintToFileFlagValue Picture X.
                    88  PD-DisablePrintToFileFlag Value 'Y'
                                            When False 'N'.
                03  PD-HidePrintToFileFlagValue Picture X.
                    88  PD-HidePrintToFileFlag  Value 'Y'
                                           When False 'N'.
                03  PD-NoNetworkButtonFlagValue Picture X.
                    88  PD-NoNetworkButtonFlag  Value 'Y'
                                           When False 'N'.
            02  PD-FromPage                   Picture 9(5) Binary(2).
            02  PD-ToPage                     Picture 9(5) Binary(2).
            02  PD-MinPage                    Picture 9(5) Binary(2).
            02  PD-MaxPage                    Picture 9(5) Binary(2).
            02  PD-Copies                     Picture 9(5) Binary(2).
 
            02  DM-DeviceName                 Picture X(31).
            02  DM-Fields.
                03  DM-OrientationFieldValue  Picture X.
                    88  DM-OrientationField     Value 'Y'
                                           When False 'N'.
                03  DM-PaperSizeFieldValue    Picture X.
                    88  DM-PaperSizeField       Value 'Y'
                                           When False 'N'.
                03  DM-PaperLengthFieldValue  Picture X.
                    88  DM-PaperLengthField     Value 'Y'
                                           When False 'N'.
                03  DM-PaperWidthFieldValue   Picture X.
                    88  DM-PaperWidthField      Value 'Y'
                                           When False 'N'.
                03  DM-ScaleFieldValue        Picture X.
                    88  DM-ScaleField           Value 'Y'
                                           When False 'N'.
                03  DM-CopiesFieldValue       Picture X.
                    88  DM-CopiesField          Value 'Y'
                                           When False 'N'.
                03  DM-PaperSourceFieldValue  Picture X.
                    88  DM-PaperSourceField     Value 'Y'
                                           When False 'N'.
                03  DM-PrintQualityFieldValue Picture X.
                    88  DM-PrintQualityField    Value 'Y'
                                           When False 'N'.
                03  DM-ColorFieldValue        Picture X.
                    88  DM-ColorField           Value 'Y'
                                           When False 'N'.
                03  DM-DuplexFieldValue       Picture X.
                    88  DM-DuplexField          Value 'Y'
                                           When False 'N'.
                03  DM-YResolutionFieldValue  Picture X.
                    88  DM-YResolutionField     Value 'Y'
                                           When False 'N'.
                03  DM-TrueTypeOptionFieldValue Picture X.
                    88  DM-TrueTypeOptionField  Value 'Y'
                                           When False 'N'.
                03  DM-CollateFieldValue      Picture X.
                    88  DM-CollateField         Value 'Y'
                                           When False 'N'.
                03  DM-ICMMethodFieldValue    Picture X.
                    88  DM-ICMMethodField       Value 'Y'
                                           When False 'N'.
                03  DM-ICMIntentFieldValue    Picture X.
                    88  DM-ICMIntentField       Value 'Y'
                                           When False 'N'.
                03  DM-MediaTypeFieldValue    Picture X.
                    88  DM-MediaTypeField       Value 'Y'
                                           When False 'N'.
                03  DM-DitherTypeFieldValue   Picture X.
                    88  DM-DitherTypeField      Value 'Y'
                                           When False 'N'.
            02  DM-OrientationValue           Picture 9 Binary(2).
                88  DM-OrientationIsPortrait    Value 1.
                88  DM-OrientationIsLandscape   Value 2.
            02  DM-PaperSizeValue             Picture 9(2) Binary(2).
                88  DM-PaperSizeIsLetter        Value 1.
                88  DM-PaperSizeIsLetterSmall   Value 2.
                88  DM-PaperSizeIsTabloid       Value 3.
                88  DM-PaperSizeIsLedger        Value 4.
                88  DM-PaperSizeIsLegal         Value 5.
                88  DM-PaperSizeIsStatement     Value 6.
                88  DM-PaperSizeIsExecutive     Value 7.
                88  DM-PaperSizeIsA3            Value 8.
                88  DM-PaperSizeIsA4            Value 9.
                88  DM-PaperSizeIsA4Small       Value 10.
                88  DM-PaperSizeIsA5            Value 11.
                88  DM-PaperSizeIsB4            Value 12.
                88  DM-PaperSizeIsB5            Value 13.
                88  DM-PaperSizeIsFolio         Value 14.
                88  DM-PaperSizeIsQuarto        Value 15.
                88  DM-PaperSizeIs10x14         Value 16.
                88  DM-PaperSizeIs11x17         Value 17.
                88  DM-PaperSizeIsNote          Value 18.
                88  DM-PaperSizeIsEnv9          Value 19.
                88  DM-PaperSizeIsEnv10         Value 20.
                88  DM-PaperSizeIsEnv11         Value 21.
                88  DM-PaperSizeIsEnv12         Value 22.
                88  DM-PaperSizeIsEnv14         Value 23.
                88  DM-PaperSizeIsCSheet        Value 24.
                88  DM-PaperSizeIsDSheet        Value 25.
                88  DM-PaperSizeIsESheet        Value 26.
                88  DM-PaperSizeIsEnvDl         Value 27.
                88  DM-PaperSizeIsEnvC5         Value 28.
                88  DM-PaperSizeIsEnvC3         Value 29.
                88  DM-PaperSizeIsEnvC4         Value 30.
                88  DM-PaperSizeIsEnvC6         Value 31.
                88  DM-PaperSizeIsEnvC65        Value 32.
                88  DM-PaperSizeIsEnvB4         Value 33.
                88  DM-PaperSizeIsEnvB5         Value 34.
                88  DM-PaperSizeIsEnvB6         Value 35.
                88  DM-PaperSizeIsEnvItaly      Value 36.
                88  DM-PaperSizeIsEnvMonarch    Value 37.
                88  DM-PaperSizeIsEnvPersonal   Value 38.
                88  DM-PaperSizeIsFanFoldUS     Value 39.
                88  DM-PaperSizeIsFanFoldStdGerman Value 40.
                88  DM-PaperSizeIsFanFoldLglGerman Value 41.
            02  DM-PaperLength                Picture 9(5) Binary(2).
            02  DM-PaperWidth                 Picture 9(5) Binary(2).
            02  DM-Scale                      Picture 9(5) Binary(2).
            02  DM-Copies                     Picture 9(5) Binary(2).
            02  DM-PaperSourceValue           Picture 9(2) Binary(2).
                88  DM-PaperSourceIsUpper       Value 1.
                88  DM-PaperSourceIsOnlyOne     Value 1.
                88  DM-PaperSourceIsLower       Value 2.
                88  DM-PaperSourceIsMiddle      Value 3.
                88  DM-PaperSourceIsManual      Value 4.
                88  DM-PaperSourceIsEnvelope    Value 5.
                88  DM-PaperSourceIsEnvManual   Value 6.
                88  DM-PaperSourceIsAuto        Value 7.
                88  DM-PaperSourceIsTractor     Value 8.
                88  DM-PaperSourceIsSmallFmt    Value 9.
                88  DM-PaperSourceIsLargeFmt    Value 10.
                88  DM-PaperSourceIsLargeCapacity Value 11.
                88  DM-PaperSourceIsCassette    Value 14.
                88  DM-PaperSourceIsFormSource  Value 15.
            02  DM-ResolutionValue            Picture S9 Binary(2).
                88  DM-ResolutionIsDraft        Value -1.
                88  DM-ResolutionIsLow          Value -2.
                88  DM-ResolutionIsMedium       Value -3.
                88  DM-ResolutionIsHigh         Value -4.
            02  DM-ColorValue                 Picture 9 Binary(2).
                88  DM-ColorIsMonochrome        Value 1.
                88  DM-ColorIsColor             Value 2.
            02  DM-DuplexValue                Picture 9 Binary(2).
                88  DM-DuplexIsSimplex          Value 1.
                88  DM-DuplexIsVertical         Value 2.
                88  DM-DuplexIsHorizontal       Value 3.
            02  DM-YResolution                Picture 9(5) Binary(2).
            02  DM-TrueTypeValue              Picture 9 Binary(2).
                88  DM-TrueTypeIsBitmap         Value 1.
                88  DM-TrueTypeIsDownload       Value 2.
                88  DM-TrueTypeIsSubDev         Value 3.
            02  DM-CollateValue               Picture 9 Binary(2).
                88  DM-CollateIsFalse           Value 0.
                88  DM-CollateIsTrue            Value 1.
            02  DM-ICMMethodValue             Picture 9 Binary(4).
                88  DM-ICMMethodIsNone          Value 1.
                88  DM-ICMMethodIsSystem        Value 2.
                88  DM-ICMMethodIsDriver        Value 3.
                88  DM-ICMMethodIsDevice        Value 4.
            02  DM-ICMIntentValue             Picture 9 Binary(4).
                88  DM-ICMIntentIsSaturate      Value 1.
                88  DM-ICMIntentIsContrast      Value 2.
                88  DM-ICMIntentIsColorMetric   Value 3.
            02  DM-MediaTypeValue             Picture 9 Binary(4).
                88  DM-MediaTypeIsStandard      Value 1.
                88  DM-MediaTypeIsTransparency  Value 2.
                88  DM-MediaTypeIsGlossy        Value 3.
            02  DM-DitherTypeValue            Picture 99 Binary(4).
                88  DM-DitherTypeIsNone         Value 1.
                88  DM-DitherTypeIsCoarse       Value 2.
                88  DM-DitherTypeIsFine         Value 3.
                88  DM-DitherTypeIsLineArt      Value 4.
                88  DM-DitherTypeIsErrorDiffusion Value 5.
                88  DM-DitherTypeIsGrayScale    Value 10.
      *
      *    Print Dialog Extended Error Values
      *
       78  PD-ExtErrCanceled                    Value 0.
 
       78  PD-ExtErrStructSize                  Value 1.
       78  PD-ExtErrInitialization              Value 2.
       78  PD-ExtErrNoTemplate                  Value 3.
       78  PD-ExtErrNoHInstance                 Value 4.
       78  PD-ExtErrLoadStrFailure              Value 5.
       78  PD-ExtErrFindResFailure              Value 6.
       78  PD-ExtErrLoadResFailure              Value 7.
       78  PD-ExtErrLockResFailure              Value 8.
       78  PD-ExtErrMemAllocFailure             Value 9.
       78  PD-ExtErrMemLockFailure              Value 10.
       78  PD-ExtErrNoHook                      Value 11.
       78  PD-ExtErrRegisterMsgFail             Value 12.

       78  PD-ExtErrSetupFailure                Value 4097.
       78  PD-ExtErrParseFailure                Value 4098.
       78  PD-ExtErrRetDefFailure               Value 4099.
       78  PD-ExtErrLoadDrvFailure              Value 4100.
       78  PD-ExtErrGetDevModeFail              Value 4101.
       78  PD-ExtErrInitFailure                 Value 4102.
       78  PD-ExtErrNoDevices                   Value 4103.
       78  PD-ExtErrNoDefaultPrn                Value 4104.
       78  PD-ExtErrDNDMMismatch                Value 4105.
       78  PD-ExtErrCreateICFailure             Value 4106.
       78  PD-ExtErrPrinterNotFound             Value 4107.
       78  PD-ExtErrDefaultDifferent            Value 4108.

       78  PD-ExtErrDialogFailure               Value 65535.
      *
      *    Device Mode Orientation Values
      *
       78  DM-OrientationPortrait               Value 1.
       78  DM-OrientationLandscape              Value 2.
      *
      *    Device Mode Paper Size Values
      *
       78  DM-PaperSizeLetter                   Value 1.
       78  DM-PaperSizeLetterSmall              Value 2.
       78  DM-PaperSizeTabloid                  Value 3.
       78  DM-PaperSizeLedger                   Value 4.
       78  DM-PaperSizeLegal                    Value 5.
       78  DM-PaperSizeStatement                Value 6.
       78  DM-PaperSizeExecutive                Value 7.
       78  DM-PaperSizeA3                       Value 8.
       78  DM-PaperSizeA4                       Value 9.
       78  DM-PaperSizeA4Small                  Value 10.
       78  DM-PaperSizeA5                       Value 11.
       78  DM-PaperSizeB4                       Value 12.
       78  DM-PaperSizeB5                       Value 13.
       78  DM-PaperSizeFolio                    Value 14.
       78  DM-PaperSizeQuarto                   Value 15.
       78  DM-PaperSize10x14                    Value 16.
       78  DM-PaperSize11x17                    Value 17.
       78  DM-PaperSizeNote                     Value 18.
       78  DM-PaperSizeEnv9                     Value 19.
       78  DM-PaperSizeEnv10                    Value 20.
       78  DM-PaperSizeEnv11                    Value 21.
       78  DM-PaperSizeEnv12                    Value 22.
       78  DM-PaperSizeEnv14                    Value 23.
       78  DM-PaperSizeCSheet                   Value 24.
       78  DM-PaperSizeDSheet                   Value 25.
       78  DM-PaperSizeESheet                   Value 26.
       78  DM-PaperSizeEnvDl                    Value 27.
       78  DM-PaperSizeEnvC5                    Value 28.
       78  DM-PaperSizeEnvC3                    Value 29.
       78  DM-PaperSizeEnvC4                    Value 30.
       78  DM-PaperSizeEnvC6                    Value 31.
       78  DM-PaperSizeEnvC65                   Value 32.
       78  DM-PaperSizeEnvB4                    Value 33.
       78  DM-PaperSizeEnvB5                    Value 34.
       78  DM-PaperSizeEnvB6                    Value 35.
       78  DM-PaperSizeEnvItaly                 Value 36.
       78  DM-PaperSizeEnvMonarch               Value 37.
       78  DM-PaperSizeEnvPersonal              Value 38.
       78  DM-PaperSizeFanFoldUS                Value 39.
       78  DM-PaperSizeFanFoldStdGerman         Value 40.
       78  DM-PaperSizeFanFoldLglGerman         Value 41.
      *
      *    Device Mode Paper Source Values
      *
       78  DM-PaperSourceUpper                  Value 1.
       78  DM-PaperSourceOnlyOne                Value 1.
       78  DM-PaperSourceLower                  Value 2.
       78  DM-PaperSourceMiddle                 Value 3.
       78  DM-PaperSourceManual                 Value 4.
       78  DM-PaperSourceEnvelope               Value 5.
       78  DM-PaperSourceEnvManual              Value 6.
       78  DM-PaperSourceAuto                   Value 7.
       78  DM-PaperSourceTractor                Value 8.
       78  DM-PaperSourceSmallFmt               Value 9.
       78  DM-PaperSourceLargeFmt               Value 10.
       78  DM-PaperSourceLargeCapacity          Value 11.
       78  DM-PaperSourceCassette               Value 14.
       78  DM-PaperSourceFormSource             Value 15.
      *
      *    Device Mode Resolution Values
      *
       78  DM-ResolutionDraft                   Value -1.
       78  DM-ResolutionLow                     Value -2.
       78  DM-ResolutionMedium                  Value -3.
       78  DM-ResolutionHigh                    Value -4.
      *
      *    Device Mode Color Values
      *
       78  DM-ColorMonochrome                   Value 1.
       78  DM-ColorColor                        Value 2.
      *
      *    Device Mode Duplex Values
      *
       78  DM-DuplexSimplex                     Value 1.
       78  DM-DuplexVertical                    Value 2.
       78  DM-DuplexHorizontal                  Value 3.
      *
      *    Device Mode True Type Values
      *
       78  DM-TrueTypeBitmap                    Value 1.
       78  DM-TrueTypeDownload                  Value 2.
       78  DM-TrueTypeSubDev                    Value 3.
      *
      *    Device Mode Collate Values
      *
       78  DM-CollateFalse                      Value 0.
       78  DM-CollateTrue                       Value 1.
      *
      *    Device ICM Method Values
      *
       78  DM-ICMMethodNone                     Value 1.
       78  DM-ICMMethodSystem                   Value 2.
       78  DM-ICMMethodDriver                   Value 3.
       78  DM-ICMMethodDevice                   Value 4.
      *
      *    Device ICM Type Values
      *
       78  DM-ICMTypeSaturate                   Value 1.
       78  DM-ICMTypeContrast                   Value 2.
       78  DM-ICMTypeColorMetric                Value 3.
      *
      *    Device Mode Media Type Values
      *
       78  DM-MediaTypeStandard                 Value 1.
       78  DM-MediaTypeTransparency             Value 2.
       78  DM-MediaTypeGlossy                   Value 3.
      *
      *    Device Mode Dither Type Values
      *
       78  DM-DitherTypeNone                    Value 1.
       78  DM-DitherTypeCoarse                  Value 2.
       78  DM-DitherTypeFine                    Value 3.
       78  DM-DitherTypeLineArt                 Value 4.
       78  DM-DitherTypeErrorDiffusion          Value 5.
       78  DM-DitherTypeGrayScale               Value 10.
      *
      *    P$DisplayDialog return values
      *
       78  PD-ReturnOK                          Value 0.
       78  PD-ReturnCancelled                   Value 1.
       78  PD-ReturnError                       Value 2.
      *
      *    Parameter Name Values
      *
       78  PD-ReturnParam                       Value "Return".
       78  PD-ExtendedErrorParam                Value "Extended Error".

       78  PD-AllPagesFlagParam                 Value "All Pages Flag".
       78  PD-SelectionFlagParam                Value "Selection Flag".
       78  PD-PageNumbersFlagParam              Value
                                          "Page Numbers Flag".
       78  PD-NoSelectionFlagParam              Value
                                          "No Selection Flag".
       78  PD-NoPageNumbersFlagParam            Value
                                          "No Page Numbers Flag".
       78  PD-CollateFlagParam                  Value "Collate Flag".
       78  PD-PrintSetupFlagParam               Value
                                          "Print Setup Flag".
       78  PD-PrintToFileFlagParam              Value
                                          "Print To File Flag".
       78  PD-NoWarningFlagParam                Value "No Warning Flag".
       78  PD-UseDevModeCopiesFlagParam         Value
                                          "Use Device Mode Copies Flag".
       78  PD-DisablePrintToFileFlagParam       Value
                                          "Disable Print To File Flag".
       78  PD-HidePrintToFileFlagParam          Value
                                          "Hide Print To File Flag".
       78  PD-NoNetworkButtonFlagParam          Value
                                          "No Network Button Flag".

       78  PD-FromPageParam                     Value "From Page".
       78  PD-ToPageParam                       Value "To Page".
       78  PD-MinPageParam                      Value "Min Page".
       78  PD-MaxPageParam                      Value "Max Page".
       78  PD-PrintDialogCopiesParam            Value
                                              "Print Dialog Copies".

       78  DM-DeviceNameParam                   Value "Device Name".
       78  DM-OrientationParam                  Value "Orientation".
       78  DM-PaperSizeParam                    Value "Paper Size".
       78  DM-PaperLengthParam                  Value "Paper Length".
       78  DM-PaperWidthParam                   Value "Paper Width".
       78  DM-ScaleParam                        Value "Scale".
       78  DM-DeviceModeCopiesParam             Value
                                              "Device Mode Copies".
       78  DM-DefaultSourceParam                Value "Default Source".
       78  DM-PrintQualityParam                 Value "Print Quality".
       78  DM-ColorParam                        Value "Color".
       78  DM-DuplexParam                       Value "Duplex".
       78  DM-YResolutionParam                  Value "Y Resolution".
       78  DM-TrueTypeOptionParam               Value
                                              "True Type Option".
       78  DM-CollateParam                      Value "Collate".
       78  DM-ICMMethodParam                    Value "ICM Method".
       78  DM-ICMIntentParam                    Value "ICM Intent".
       78  DM-MediaTypeParam                    Value "Media Type".
       78  DM-DitherTypeParam                   Value "Dither Type".
