      *
      *    Miscellaneous Windows Definitions
      *
      *
      *    C$PlaySound Options
      *
       78  SoundSync                    Value 0.
       78  SoundAsync                   Value 2 ** 0.
       78  SoundNoDefault               Value 2 ** 1.
       78  SoundNoStop                  Value 2 ** 4.
       78  SoundPurge                   Value 2 ** 6.
       78  SoundApplication             Value 2 ** 7.
       78  SoundNoWait                  Value 2 ** 13.
       78  SoundAlias                   Value 2 ** 16.
       78  SoundFilename                Value 2 ** 17.
       78  SoundAliasId                 Value (2 ** 16) + (2 ** 20).
      *
      *    Pen Style Values
      *
       78  PenStyleSolid                Value 0.
       78  PenStyleDash                 Value 1.
       78  PenStyleDot                  Value 2.
       78  PenStyleDashDot              Value 3.
       78  PenStyleDashDotDot           Value 4.
       78  PenStyleNull                 Value 5.
      *
      *    Position Alignment Argument Values
      *
       78  PositionIsTop                Value "Top".
       78  PositionIsBottom             Value "Bottom".
      *
      *    Position Mode Argument Values
      *
       78  ModeIsAbsolute               Value "Absolute".
       78  ModeIsRelative               Value "Relative".
      *
      *    Position Unit Argument Values
      *
       78  UnitsAreInches               Value "Inches".
       78  UnitsAreMetric               Value "Metric".
       78  UnitsAreCharacters           Value "Characters".
       78  UnitsAreDeviceUnits          Value "Device Units".
      *
      *    Yes/No Argument Values
      *
       78  DrawBoxWithShading           Value "Yes".
       78  DrawBoxWithoutShading        Value "No".
       78  DrawBoxNoShading             Value "No".
       78  TextOutWithBox               Value "Yes".
       78  TextOutWithoutBox            Value "No".
       78  TextOutNoBox                 Value "No".
       78  TextOutWithShading           Value "Yes".
       78  TextOutWithoutShading        Value "No".
       78  TextOutNoShading             Value "No".
       78  WithBox                      Value "Yes".
       78  WithoutBox                   Value "No".
       78  NoBox                        Value "No".
       78  WithShading                  Value "Yes".
       78  WithoutShading               Value "No".
       78  NoShading                    Value "No".
      *
      *    Color Argument Values
      *
       78  ColorBlack                   Value "Black".
       78  ColorDarkBlue                Value "Dark Blue".
       78  ColorDarkGreen               Value "Dark Green".
       78  ColorDarkCyan                Value "Dark Cyan".
       78  ColorDarkRed                 Value "Dark Red".
       78  ColorDarkMagenta             Value "Dark Magenta".
       78  ColorBrown                   Value "Brown".
       78  ColorDarkGray                Value "Dark Gray".
       78  ColorLightGray               Value "Light Gray".
       78  ColorBlue                    Value "Blue".
       78  ColorGreen                   Value "Green".
       78  ColorCyan                    Value "Cyan".
       78  ColorRed                     Value "Red".
       78  ColorMagenta                 Value "Magenta".
       78  ColorYellow                  Value "Yellow".
       78  ColorWhite                   Value "White".
      *
      *    Pitch Name Values
      *
       78  PitchNormal                  Value "Normal".
       78  PitchExpanded                Value "Expanded".
       78  PitchCompressed              Value "Compressed".
      *
      *    Orientation Values
      *
       78  OrientationPortrait          Value "Portrait".
       78  OrientationLandscape         Value "Landscape".
