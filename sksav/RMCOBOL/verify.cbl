       IDENTIFICATION DIVISION.
       PROGRAM-ID.  "verify".
      *
      * Title:  verify.cbl
      *        RM/COBOL Verify Test Suite Driver
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      * Version Identification:
      *   $Revision:   6.3.1.2  $
      *   $Date:   21 Sep 1999 13:16:58  $
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
      *
      *
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
       COPY "version.cpy".

       01  TITLE.
           05  PIC X(40) VALUE
           "RM/COBOL Verification Program - Version ".
           05  PIC X(4)  VALUE CURRENT-VERSION.

       01  FUNCTION-NUMBER     PIC 9.
       01  RETURN-KEY          PIC X.
       01  FILLER VALUE
           "vdttest filetestnuctest prntest sorttestwintest ".
           02  TEST-NAME       PIC X(8) OCCURS 6.
      /
       PROCEDURE DIVISION.
       MAIN-START.
           DISPLAY "The RM/COBOL Verification Suite",
                       HIGH, LINE 1, POSITION 20, ERASE.

           DISPLAY "The RM/COBOL Verification Suite tests the following
      -            "aspects of RM/COBOL:", LINE 3, LOW.
           DISPLAY "  -  the terminal configuration",    LOW, LINE 5.
           DISPLAY "  -  the I-O subsystem",             LOW, LINE 7.
           DISPLAY "  -  the nucleus",                   LOW, LINE 9.
           DISPLAY "  -  the printer configuration",     LOW, LINE 11.
           DISPLAY "  -  the SORT/MERGE subsystem",      LOW, LINE 13.
           DISPLAY "  -  the WINDOWS subsystem (if enabled)",
                                LOW, LINE 15.

           DISPLAY "The test selection menus are designed to be self-exp
      -            "lanatory.  Where a", LINE 18, LOW.

           DISPLAY "specific field or type of data is required, a prompt
      -            " will appear on the", LINE 19, LOW.

           DISPLAY "screen so indicating.", LOW, LINE 20.

           DISPLAY " Press any key to continue...",
                       HIGH, LINE 22, POSITION 20.
           ACCEPT RETURN-KEY, POSITION 0.
      /
       DISPLAY-MAIN-MENU.
           DISPLAY TITLE, LINE 1, ERASE, LOW.
           DISPLAY " ", LOW.
           DISPLAY "   1) Screen Configuration Test", LOW.
           DISPLAY "   2) File System Test", LOW.
           DISPLAY "   3) Nucleus Test", LOW.
           DISPLAY "   4) Printer Test", LOW.
           DISPLAY "   5) Sort/Merge Test", LOW.
           DISPLAY "   6) Windows Test", LOW.
           DISPLAY "   7) Exit Program", LOW.
           DISPLAY " ", LOW.
           DISPLAY "      Enter desired test number       ", LOW.

       ACCEPT-MAIN-MENU.
           ACCEPT FUNCTION-NUMBER LINE 11 POSITION 37 TAB, CONVERT.
           IF FUNCTION-NUMBER GREATER THAN 0 AND LESS THAN 7 THEN
               CALL TEST-NAME (FUNCTION-NUMBER)
               GO DISPLAY-MAIN-MENU
           END-IF.
           IF FUNCTION-NUMBER NOT EQUAL 7 THEN
               DISPLAY "  " LINE 11 POSITION 37
               GO TO ACCEPT-MAIN-MENU
           END-IF.
           EXIT PROGRAM.
           STOP RUN.
