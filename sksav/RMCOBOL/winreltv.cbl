       IDENTIFICATION DIVISION.
       PROGRAM-ID. "winreltv".
      *
      * Title:  winreltv.cbl
      *         RM/COBOL Test Windows Relative Options
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *AUTHOR: RAH.
      *DATE WRITTEN: 2/28/90
      *PROGRAM DESCRIPTION.
      *This program tests SCREEN or WINDOW relative of overlayed windows.
      *
      *INPUT-FILE - None
      *OPERATOR-RESPONSE - Enter selection number from terminal.
      *OUTPUT-FILE - None
      *
      * Version Identification:
      *   $Revision:   6.1.1.1  $
      *   $Date:   21 Sep 1999 13:21:04  $
      *
       ENVIRONMENT DIVISION.
       DATA DIVISION.

       WORKING-STORAGE SECTION.
       77  X                            PIC X.
       77  I                            PIC 999.
       01  RULER                        PIC X(150)
                                        VALUE ALL "1234567890".

       01  RELATIVE-WINDOWS.
           02  RELATIVE-WCB             PIC X(80) OCCURS 4 TIMES.

       COPY "win.cpy".

       LINKAGE SECTION.
       01 SCREEN-NUM-ROWS               PIC 999.
       01 SCREEN-NUM-COLS               PIC 999.

       SCREEN SECTION.

       01  STATUS-SCREEN.
           05 BLANK SCREEN.
           05  LINE 3 COL 27 HIGHLIGHT
               "Window status = ".
           05  LINE 3 COL 45 HIGHLIGHT
               PIC 999 USING WINDOW-STATUS.

       01  RETURN-SCREEN.
           05  LINE 21 COL 15 HIGHLIGHT
                 "Press <Return> for next test ".
           05  LINE 21 COL 53  PIC X USING X.
      /
       PROCEDURE DIVISION USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
       BEGIN-MAIN.
           PERFORM DISPLAY-RULERS.

      *Define and create window.
           COMPUTE WCB-NUM-ROWS = SCREEN-NUM-ROWS - 4.
           COMPUTE WCB-NUM-COLS = SCREEN-NUM-COLS - 11.
           MOVE "S" TO WCB-LOCATION-REFERENCE.
           MOVE "Y" TO WCB-BORDER-SWITCH.
           MOVE  0  TO WCB-BORDER-TYPE.
           MOVE "*" TO WCB-BORDER-CHAR.
           MOVE "T" TO WCB-TITLE-LOCATION.
           MOVE "C" TO WCB-TITLE-POSITION.
           MOVE  " WINDOWS:  RM/COBOL  Relative Location "
               TO WCB-TITLE.
           MOVE 39 TO WCB-TITLE-LENGTH.

      *Display SCREEN relative window.
           MOVE WCB TO RELATIVE-WCB (1).
           DISPLAY RELATIVE-WCB (1) LINE 3 POSITION 11
               CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY STATUS-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 35,
                   "should begin on line 2 col 10 " LINE  6 POSITION 35,
                   "       BORDER = HIGH          " LINE  7 POSITION 35,
                   "      screen relative         " LINE  8 POSITION 35.
           DISPLAY RULER LINE 1, POSITION 1 SIZE, WCB-NUM-COLS.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Display WINDOW relative window.
           MOVE "W" TO WCB-LOCATION-REFERENCE.
           MOVE  1  TO WCB-BORDER-TYPE.
           SUBTRACT 6 FROM WCB-NUM-ROWS.
           SUBTRACT 16 FROM WCB-NUM-COLS.
           MOVE WCB TO RELATIVE-WCB (2).
           DISPLAY RELATIVE-WCB (2) REVERSE LINE 6 POSITION 16
                       CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY STATUS-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 20,
                   "should begin on line 5 col 15 " LINE  6 POSITION 20,
                   "       BORDER = REVERSE       " LINE  7 POSITION 20,
                   "       window relative        " LINE  8 POSITION 20.
           DISPLAY RULER LINE 1, POSITION 1, SIZE WCB-NUM-COLS.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Display SCREEN relative window.
           MOVE "S" TO WCB-LOCATION-REFERENCE.
           MOVE  2  TO WCB-BORDER-TYPE.
           COMPUTE WCB-NUM-ROWS = SCREEN-NUM-ROWS - 12.
           COMPUTE WCB-NUM-COLS = SCREEN-NUM-COLS - 22.
           MOVE WCB TO RELATIVE-WCB (3).
           DISPLAY RELATIVE-WCB (3) LOW LINE 5 POSITION 15
                       CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY STATUS-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 25,
                   "should begin on line 4 col 14 " LINE  6 POSITION 25,
                   "       BORDER = LOW           " LINE  7 POSITION 25,
                   "      screen relative         " LINE  8 POSITION 25.
           DISPLAY RULER LINE 1, POSITION 1 SIZE, WCB-NUM-COLS.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.

      *Display SCREEN relative window.
           MOVE "S" TO WCB-LOCATION-REFERENCE.
           MOVE  3  TO WCB-BORDER-TYPE.
           COMPUTE WCB-NUM-ROWS = SCREEN-NUM-ROWS / 2.
           COMPUTE WCB-NUM-COLS = SCREEN-NUM-COLS / 2 + 6.
           MOVE WCB TO RELATIVE-WCB (4).
           DISPLAY RELATIVE-WCB (4) HIGH LINE 10 POSITION 20
                       CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY STATUS-SCREEN.
           DISPLAY "The border for this window    " LINE  5 POSITION 5,
                   "should begin on line 9 col 19 " LINE  6 POSITION 5,
                   "       BORDER = HIGH          " LINE  7 POSITION 5,
                   "      screen relative         " LINE  8 POSITION 5.
           DISPLAY RULER LINE 1, POSITION 1 SIZE, WCB-NUM-COLS.

      *Remove windows.
           PERFORM REMOVE-WINDOW
               VARYING I FROM 4 BY -1 UNTIL I IS EQUAL TO 0.
           DISPLAY SPACE LINE 1 ERASE EOS.
           EXIT PROGRAM.
           STOP RUN.

       DISPLAY-RULERS.
           DISPLAY SPACE ERASE.
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > SCREEN-NUM-ROWS
             DISPLAY "Line " LINE I POSITION 1
                     I       POSITION 0
           END-PERFORM.
           DISPLAY RULER LINE SCREEN-NUM-ROWS POSITION 1
                SIZE SCREEN-NUM-COLS.

       REMOVE-WINDOW.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.
           DISPLAY RELATIVE-WCB (I) CONTROL "WINDOW-REMOVE".
