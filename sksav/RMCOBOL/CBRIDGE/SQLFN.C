#include <string.h>

void string2string(char *pInput, char *pOutput)
{   (void)strcpy(pOutput, pInput);
}

void cint2integer(void *pInput, short sInput, short tInput, __int64 *pOutput)
{   char TypeTable[] = {1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0};
    char SignType = 0;

    if (tInput < sizeof(TypeTable))
	SignType = TypeTable[tInput];
    if (sInput == sizeof(char))
    {	if (SignType == 0)
	{   unsigned char *pTemp = (unsigned char*)pInput;
	    *pOutput = (__int64)*pTemp;
	}
	else
	{   signed char *pTemp = (signed char*)pInput;
	    *pOutput = (__int64)*pTemp;
	}
    }
    else if ((sizeof(char) != sizeof(short)) && (sInput == sizeof(short)))
    {	if (SignType == 0)
	{   unsigned short *pTemp = (unsigned short*)pInput;
	    *pOutput = (__int64)*pTemp;
	}
	else
	{   signed short *pTemp = (signed short*)pInput;
	    *pOutput = (__int64)*pTemp;
	}
    }
    else if((sizeof(short) != sizeof(int)) && (sInput == sizeof(int)))
    {	if (SignType == 0)
	{   unsigned int *pTemp = (unsigned int*)pInput;
	    *pOutput = (__int64)*pTemp;
	}
	else
	{   signed int *pTemp = (signed int*)pInput;
	    *pOutput = (__int64)*pTemp;
	}
    }
    else if ((sizeof(int) != sizeof(long)) && (sInput == sizeof(long)))
    {	if (SignType == 0)
	{   unsigned long *pTemp = (unsigned long*)pInput;
	    *pOutput = (__int64)*pTemp;
	}
	else
	{   signed long *pTemp = (signed long*)pInput;
	    *pOutput = (__int64)*pTemp;
	}
    }
    else /* (sInput == sizeof(__int64)) */
    {	__int64 *pTemp = (__int64*)pInput;
	*pOutput = (__int64)*pTemp;
    }
}

void integer2cint(__int64 *pInput, void *pOutput, short sOutput, short tOutput)
{   char TypeTable[] = {1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0};
    char SignType = 0;

    if (tOutput < sizeof(TypeTable))
	SignType = TypeTable[tOutput];
    if (sOutput == sizeof(char))
    {	if (SignType == 0)
	{   unsigned char *pTemp = (unsigned char*)pOutput;
	    *pTemp = *pInput;
	}
	else
	{   signed char *pTemp = (signed char*)pOutput;
	    *pTemp = *pInput;
	}
    }
    else if ((sizeof(char) != sizeof(short)) && (sOutput == sizeof(short)))
    {	if (SignType == 0)
	{   unsigned short *pTemp = (unsigned short*)pOutput;
	    *pTemp = *pInput;
	}
	else
	{   signed short *pTemp = (signed short*)pOutput;
	    *pTemp = *pInput;
	}
    }
    else if ((sizeof(short) != sizeof(int)) && (sOutput == sizeof(int)))
    {	if (SignType == 0)
	{   unsigned int *pTemp = (unsigned int*)pOutput;
	    *pTemp = *pInput;
	}
	else
	{   signed int *pTemp = (signed int*)pOutput;
	    *pTemp = *pInput;
	}
    }
    else if ((sizeof(int) != sizeof(long)) && (sOutput == sizeof(long)))
    {	if (SignType == 0)
	{   unsigned long *pTemp = (unsigned long*)pOutput;
	    *pTemp = *pInput;
	}
	else
	{   signed long *pTemp = (signed long*)pOutput;
	    *pTemp = *pInput;
	}
    }
    else /* (sOutput == sizeof(__int64)) */
    {	__int64 *pTemp = (__int64*)pOutput;
	*pTemp = *pInput;
    }
}

void cfloat2float(void *pInput, short sInput, double *pOutput)
{   if (sInput == sizeof(double))
    {	double *pTemp = (double*)pInput;
	*pOutput = (double)*pTemp;
    }
    else /* sInput == sizeof(float) */
    {	float *pTemp = (float*)pInput;
       *pOutput = (float)*pTemp;
    }
}

void float2cfloat(double *pInput, void *pOutput, short sOutput)
{   if (sOutput == sizeof(double))
    {	double *pTemp = (double*)pOutput;
	*pTemp = *pInput;
    }
    else /* sOutput == sizeof(float) */
    {	float *pTemp = (float*)pOutput;
       *pTemp = *pInput;
    }
}
