#include <math.h>
[[float out rounded]] double cos ([[float in rounded]] double x);
[[float out rounded]] double sin ([[float in rounded]] double x);
[[float out rounded]] double tan ([[float in rounded]] double x);
