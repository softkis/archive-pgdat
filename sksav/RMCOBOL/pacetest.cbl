       IDENTIFICATION DIVISION.
       PROGRAM-ID.  "pacetest".
      *
      * Title:  pacetest.cbl
      *         RM/COBOL Multi-user Test
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      * Version Identification:
      *   $Revision:   6.0.1.1  $
      *   $Date:   21 Sep 1999 13:13:18  $
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMC.
       OBJECT-COMPUTER.  RMC.
      *
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT INDEX-FILE
               ASSIGN TO RANDOM "pinxfl"
               ORGANIZATION INDEXED
               ACCESS MODE DYNAMIC
               RECORD KEY IS I-KEY
               FILE STATUS IS I-STATUS.

           SELECT REG-FILE
               ASSIGN TO RANDOM "pinx"
               ORGANIZATION INDEXED
               ACCESS MODE RANDOM
               RECORD KEY IS R-KEY
               FILE STATUS IS R-STATUS.
      *
       DATA DIVISION.
      *
       FILE SECTION.

       FD INDEX-FILE
           LABEL RECORD IS STANDARD.
       01 INDEX-RECORD.
           03 I-KEY    PIC X(5).
           03 I-DATA   PIC X(20).

       FD REG-FILE
           LABEL RECORD IS STANDARD.
       01 REG-RECORD.
           03 R-KEY    PIC X.
           03 R-NUM    PIC 9(5).
      *
       WORKING-STORAGE SECTION.

       77 I-STATUS     PIC XX.
       77 R-STATUS     PIC XX.
       77 RECKOUNT     PIC 99999.
       77 KEYKOUNT     PIC 99999.
       77 KOUNT        PIC 99999.
       77 DUMNUM       PIC 99.
       77 MENUNUM      PIC 99.
       77 TERM-ID      PIC 99.
       01 WS-RECORD.
           03 WS-KEY   PIC 9(5).
           03 FILLER   PIC X(16)  VALUE  "   TERMINAL ID= ".
           03 WS-ID    PIC 99.
           03 FILLER   PIC XX VALUE SPACES.
      *
      *
       PROCEDURE DIVISION.
      *
       BEGINING.
           MOVE 0 TO MENUNUM.
       MENU.
           DISPLAY "****    PACETEST  MENU    ****"
                   LINE 3 POSITION 24 ERASE.
           IF MENUNUM LESS THAN 1
              DISPLAY
              "1.  Initialization (only one of you run this)."
                   LINE 5 POSITION 14.
           IF MENUNUM LESS THAN 2
              DISPLAY "2.  Simultaneous Extension (both of you)."
                   LINE 7 POSITION 14.
           IF MENUNUM LESS THAN 3
              DISPLAY
              "3.  Verification Listing (only one need run this)."
                   LINE 9 POSITION 14.
           DISPLAY "4.  Start ""PACETEST"" all over."
                   LINE 11 POSITION 14.
           DISPLAY "5.  Exit ""PACETEST""." LINE 13 POSITION 14.
           DISPLAY "< >  Press the number of the desired section."
                   LINE 16 POSITION 13.
           ACCEPT DUMNUM LINE 16 POSITION 14 SIZE 1 CONVERT NO BEEP.
           IF DUMNUM GREATER THAN 5 GO TO MENU.
           IF DUMNUM EQUAL TO MENUNUM OR
              DUMNUM LESS THAN MENUNUM GO TO MENU.
           MOVE DUMNUM TO MENUNUM.
           GO TO INITIALIZ,
                 SIMEXTEND,
                 VERALIST,
                 BEGINING,
                 EXITING   DEPENDING ON MENUNUM.
      *
      *
       INITIALIZ.
           DISPLAY "*    PACETEST  INITIALIZATION    *"
                   LINE 3 POSITION 23 ERASE.
           DISPLAY
           "Now initializing ""pinxfl"" and ""pinx"" files."
           LINE 8 POSITION 20.
           OPEN OUTPUT REG-FILE.
           MOVE 0 TO R-NUM.
           MOVE "X" TO R-KEY.
           WRITE REG-RECORD.
           OPEN OUTPUT INDEX-FILE.
           CLOSE INDEX-FILE.
           CLOSE REG-FILE.
           DISPLAY
           "Initialization complete, press ""RETURN"" < >."
           LINE 13 POSITION 19.
           ACCEPT DUMNUM POSITION 60 LINE 13 SIZE 1 NO BEEP.
           GO TO MENU.
      *
      *
       SIMEXTEND.
           MOVE ZERO TO KOUNT, TERM-ID.
           DISPLAY "**    PACETEST  SIMULTANEOUS  EXTENSION    **"
                   LINE 3 POSITION 18 ERASE.
           DISPLAY
           "Enter a number for TERMINAL-ID. (like 1 or 2) <  >"
                   LINE 6 POSITION 15.
           DISPLAY
           "(both of you press ""RETURN"" simultaneously)"
                   LINE 7 POSITION 16.
           ACCEPT TERM-ID LINE 6 POSITION 62
                          NO BEEP TAB CONVERT ECHO.
           MOVE TERM-ID TO WS-ID.
           DISPLAY "Now extending ""pinxfl"" with a key value of : "
                   LINE 10 POSITION 15.
           OPEN I-O REG-FILE.
           OPEN I-O INDEX-FILE.
           PERFORM LOOP UNTIL KOUNT EQUAL TO 100.
           CLOSE REG-FILE.
           CLOSE INDEX-FILE.
           DISPLAY "Extension has finished, press ""RETURN"" < >."
                   LINE 13 POSITION 20.
           ACCEPT DUMNUM LINE 13 POSITION 60 SIZE 1 NO BEEP.
           GO TO MENU.
       LOOP.
           MOVE "X" TO R-KEY.
           READ REG-FILE KEY IS R-KEY
                         INVALID KEY STOP "REG-KEY-BAD".
           ADD 1 TO R-NUM.
           MOVE R-NUM TO WS-KEY.
           REWRITE REG-RECORD INVALID KEY STOP "REG-REWRITE-BAD".
           DISPLAY WS-KEY LINE 10 POSITION 60.
           ADD 1 TO KOUNT.
           WRITE INDEX-RECORD FROM WS-RECORD
               INVALID KEY STOP "INDEX-KEY-BAD".
      *
      *
       VERALIST.
           DISPLAY "***    PACETEST  VERIFICATION  LISTING    ***"
                   LINE 1 POSITION 17 ERASE.
           OPEN INPUT INDEX-FILE.
           MOVE 0 TO I-KEY, RECKOUNT, KEYKOUNT, KOUNT.
           PERFORM READ-LOOP UNTIL KOUNT = 999.
           DISPLAY "Press ""RETURN"" to return to menu. < >"
                   LINE 23 POSITION 22.
           ACCEPT DUMNUM LINE 23 POSITION 57 SIZE 1 NO BEEP.
           GO TO MENU.
       READ-LOOP.
           ADD 1 TO RECKOUNT.
           READ INDEX-FILE NEXT RECORD AT END
                                       CLOSE INDEX-FILE
                                       MOVE 999 TO KOUNT.
           IF KOUNT EQUAL 999 GO TO DONE.
           MOVE I-KEY TO KEYKOUNT.
           ADD 1 TO KOUNT.
           MOVE KOUNT TO DUMNUM.
           ADD 2 TO DUMNUM.
           DISPLAY INDEX-RECORD LINE DUMNUM POSITION 25.
           IF RECKOUNT NOT EQUAL TO KEYKOUNT
              DISPLAY "*** error ***" LINE DUMNUM POSITION 59
                      BLINK, BEEP.
           IF DUMNUM EQUAL 22 MOVE 0 TO KOUNT.
           IF KOUNT EQUAL 0
              DISPLAY "Press ""RETURN"" to continue. < >"
                      LINE 24 POSITION 25
              ACCEPT DUMNUM SIZE 1 LINE 24 POSITION 54 NO BEEP
              DISPLAY
              "***    PACETEST  VERIFICATION  LISTING    ***"
              LINE 1 POSITION 17 ERASE.
       DONE.
           DISPLAY
           """pinxfl"" exhausted. Press ""RETURN"" to exit. < >"
           LINE 24 POSITION 16.
           ACCEPT DUMNUM SIZE 1 LINE 24 POSITION 61 NO BEEP.
           GO TO MENU.
      *
      *
       EXITING.
           EXIT PROGRAM.
       STOPING.
           STOP RUN.
       END PROGRAM.
