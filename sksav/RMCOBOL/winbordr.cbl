       IDENTIFICATION DIVISION.
       PROGRAM-ID. "winbordr".
      *
      * Title:  winbordr.cbl
      *         RM/COBOL Test Window Borders
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *AUTHOR: RAH.
      *DATE WRITTEN: 2/27/90
      *PROGRAM DESCRIPTION.
      *This program tests the 9 different border types.
      *
      *INPUT-FILE - None
      *OPERATOR-RESPONSE - Enter selection number from terminal.
      *OUTPUT-FILE - None
      *
      * Version Identification:
      *   $Revision:   6.2.1.1  $
      *   $Date:   21 Sep 1999 13:19:30  $
      *
       ENVIRONMENT DIVISION.
       DATA DIVISION.

       WORKING-STORAGE SECTION.
       77  X                            PIC X.
       77  L                            PIC 99.
       77  P                            PIC 99.
       77  I                            PIC 99 BINARY.

       01  BORDER-WINDOWS.
           03  BORDER-WCB               PIC X(80) OCCURS 5.

       COPY "win.cpy".

       LINKAGE SECTION.
       01 SCREEN-NUM-ROWS               PIC 999.
       01 SCREEN-NUM-COLS               PIC 999.

       SCREEN SECTION.
       01  MENU-SCREEN.
           05  BLANK SCREEN.
           05  LINE 3 COL 21 HIGHLIGHT
               "Window status = ".
           05  LINE 3 COL 38 HIGHLIGHT
               PIC 999 USING WINDOW-STATUS.

       01  RETURN-SCREEN.
           05  LINE 9  COL 17 HIGHLIGHT
               "Press <Return> for next test ".
           05  LINE 9  COL 54  PIC X USING X.
      /
       PROCEDURE DIVISION USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
       BEGIN-MAIN.
           DISPLAY SPACE LINE 1 POSITION 1 ERASE EOS.
      *Define and create window.
           MOVE 11 TO WCB-NUM-ROWS.
           MOVE 60 TO WCB-NUM-COLS.
           MOVE "S" TO WCB-LOCATION-REFERENCE.
           MOVE "Y" TO WCB-BORDER-SWITCH.
           MOVE "T" TO WCB-TITLE-LOCATION.
           MOVE "C" TO WCB-TITLE-POSITION.
           MOVE  " WINDOWS:  RM/COBOL  Border Types "
                   TO WCB-TITLE.
           MOVE 34 TO WCB-TITLE-LENGTH.
           MOVE  9 TO L.
           MOVE 10 TO P.
           MOVE 0 TO WCB-BORDER-TYPE.
           PERFORM DISPLAY-BORDERS VARYING I FROM 1 BY 1
                   UNTIL I > 5.
           PERFORM REMOVE-WINDOWS VARYING I FROM 5 BY -1
                   UNTIL I < 1.
           EXIT PROGRAM.
           STOP RUN.

      *Display window WCB-BORDER-TYPE 0 TO 5.
       DISPLAY-BORDERS.
           MOVE WCB TO BORDER-WCB (I).
           DISPLAY BORDER-WCB (I) LINE L POSITION P
                   CONTROL "WINDOW-CREATE".
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY MENU-SCREEN.
           DISPLAY "The border type for this window" LINE 5 POSITION 13,
                   " = "                             LINE 5 POSITION 0,
                   WCB-BORDER-TYPE                   LINE 5 POSITION 0.
           DISPLAY RETURN-SCREEN.
           ACCEPT  RETURN-SCREEN.
           ADD 1 TO WCB-BORDER-TYPE.

      *Remove windows.
       REMOVE-WINDOWS.
           DISPLAY BORDER-WCB (I) CONTROL "WINDOW-REMOVE".
