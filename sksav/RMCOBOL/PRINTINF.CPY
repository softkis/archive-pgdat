      *
      *    Printer Information Definitions
      *
        01 PrinterInformation.
           02  PI-ServerName                  Picture X(80).
           02  PI-PrinterName                 Picture X(80).
           02  PI-ShareName                   Picture X(80).
           02  PI-PortName                    Picture X(80).
           02  PI-DriverName                  Picture X(80).
           02  PI-Comment                     Picture X(80).
           02  PI-Location                    Picture X(80).
           02  PI-SepFile                     Picture X(80).
           02  PI-PrintProcessor              Picture X(80).
           02  PI-DataType                    Picture X(80).
           02  PI-Parameters                  Picture X(80).
           02  PI-Attribute.
               03  PI-QueuedAttributeValue    Picture X.
                   88  PI-QueuedAttribute       Value 'Y'
                                           When False 'N'.
               03  PI-DirectAttributeValue    Picture X.
                   88  PI-DirectAttribute       Value 'Y'
                                           When False 'N'.
               03  PI-DefaultAttributeValue   Picture X.
                   88  PI-DefaultAttribute      Value 'Y'
                                           When False 'N'.
               03  PI-SharedAttributeValue    Picture X.
                   88  PI-SharedAttribute       Value 'Y'
                                           When False 'N'.
               03  PI-NetworkAttributeValue   Picture X.
                   88  PI-NetworkAttribute      Value 'Y'
                                           When False 'N'.
               03  PI-HiddenAttributeValue    Picture X.
                   88  PI-HiddenAttribute       Value 'Y'
                                           When False 'N'.
               03  PI-LocalAttributeValue     Picture X.
                   88  PI-LocalAttribute        Value 'Y'
                                           When False 'N'.
               03  PI-EnableDEVQAttributeValue Picture X.
                   88  PI-EnableDEVQAttribute   Value 'Y'
                                           When False 'N'.
               03  PI-KeepPrintedAttributeValue Picture X.
                   88  PI-KeepPrintedJobsAttribute Value 'Y'
                                              When False 'N'.
               03  PI-DoComplete1stAttributeValue Picture X.
                   88  PI-DoCompleteFirstAttribute Value 'Y'
                                              When False 'N'.
               03  PI-WorkOfflineAttributeValue Picture X.
                   88  PI-WorkOfflineAttribute  Value 'Y'
                                           When False 'N'.
               03  PI-EnableBIDIAttributeValue Picture X.
                   88  PI-EnableBIDIAttribute   Value 'Y'
                                           When False 'N'.
           02  PI-Priority                    Picture 9(10) Binary(4).
           02  PI-DefaultPriority             Picture 9(10) Binary(4).
           02  PI-StartTime                   Picture 9(10) Binary(4).
           02  PI-UntilTime                   Picture 9(10) Binary(4).
           02  PI-Status.
               03  PI-PausedStatusValue            Picture X.
                   88  PI-PausedStatus          Value 'Y'
                                           When False 'N'.
               03  PI-ErrorStatusValue        Picture X.
                   88  PI-ErrorStatus           Value 'Y'
                                           When False 'N'.
               03  PI-PendingDeletionStatusValue Picture X.
                   88  PI-PendingDeletionStatus Value 'Y'
                                           When False 'N'.
               03  PI-PaperJamStatusValue     Picture X.
                   88  PI-PaperJamStatus        Value 'Y'
                                           When False 'N'.
               03  PI-PaperOutStatusValue     Picture X.
                   88  PI-PaperOutStatus        Value 'Y'
                                           When False 'N'.
               03  PI-ManualFeedStatusValue   Picture X.
                   88  PI-ManualFeedStatus      Value 'Y'
                                           When False 'N'.
               03  PI-PaperProblemStatusValue Picture X.
                   88  PI-PaperProblemStatus    Value 'Y'
                                           When False 'N'.
               03  PI-OfflineStatusValue      Picture X.
                   88  PI-OfflineStatus         Value 'Y'
                                           When False 'N'.
               03  PI-IOActiveStatusValue     Picture X.
                   88  PI-IOActiveStatus        Value 'Y'
                                           When False 'N'.
               03  PI-BusyStatusValue         Picture X.
                   88  PI-BusyStatus            Value 'Y'
                                           When False 'N'.
               03  PI-PrintingStatusValue     Picture X.
                   88  PI-PrintingStatus        Value 'Y'
                                           When False 'N'.
               03  PI-OutputBinFullStatusValue Picture X.
                   88  PI-OutputBinFullStatus   Value 'Y'
                                           When False 'N'.
               03  PI-NotAvailableStatusValue Picture X.
                   88  PI-NotAvailableStatus    Value 'Y'
                                           When False 'N'.
               03  PI-WaitingStatusValue      Picture X.
                   88  PI-WaitingStatus         Value 'Y'
                                           When False 'N'.
               03  PI-ProcessingStatusValue   Picture X.
                   88  PI-ProcessingStatus      Value 'Y'
                                           When False 'N'.
               03  PI-IntializingStatusValue  Picture X.
                   88  PI-InitializingStatus    Value 'Y'
                                           When False 'N'.
               03  PI-WarmingUpStatusValue    Picture X.
                   88  PI-WarmingUpStatus       Value 'Y'
                                           When False 'N'.
               03  PI-ToneLowStatusValue      Picture X.
                   88  PI-TonerLowStatus        Value 'Y'
                                           When False 'N'.
               03  PI-NoTonerStatusValue      Picture X.
                   88  PI-NoTonerStatus         Value 'Y'
                                           When False 'N'.
               03  PI-PagePuntStatusValue     Picture X.
                   88  PI-PagePuntStatus        Value 'Y'
                                           When False 'N'.
               03  PI-UserInterventionStatusValue Picture X.
                   88  PI-UserInterventionStatus Value 'Y'
                                            When False 'N'.
               03  PI-OutOfMemoryStatusValue  Picture X.
                   88  PI-OutOfMemoryStatus     Value 'Y'
                                           When False 'N'.
               03  PI-DoorOpenStatusValue     Picture X.
                   88  PI-DoorOpenStatus        Value 'Y'
                                           When False 'N'.
               03  PI-ServerUnknownStatusValue Picture X.
                   88  PI-ServerUnknownStatus   Value 'Y'
                                           When False 'N'.
               03  PI-PowerSaveStatusValue    Picture X.
                   88  PI-PowerSaveStatus       Value 'Y'
                                           When False 'N'.
           02  PI-Jobs                        Picture 9(10) Binary(4).
           02  PI-AveragePPM                  Picture 9(10) Binary(4).
      *
      *    Parameter Name Values
      *
       78  PI-ServerNameParam                   Value "Server Name".
       78  PI-PrinterNameParam                  Value "Printer Name".
       78  PI-ShareNameParam                    Value "Share Name".
       78  PI-PortNameParam                     Value "Port Name".
       78  PI-DriverNameParam                   Value "Driver Name".
       78  PI-CommentParam                      Value "Comment".
       78  PI-LocationParam                     Value "Location".
       78  PI-SepFileParam                      Value "Sep File".
       78  PI-PrintProcessorParam               Value "Print Processor".
       78  PI-DataTypeParam                     Value "Data Type".
       78  PI-ParametersParam                   Value "Parameters".
       78  PI-PriorityParam                     Value "Priority".
       78  PI-DefaultPriorityParam              Value
                                              "Default Priority".
       78  PI-StartTimeParam                    Value "Start Time".
       78  PI-UntilTimeParam                    Value "Until Time".
       78  PI-JobsParam                         Value "Jobs".
       78  PI-AveragePPMParam                   Value "Average PPM".
