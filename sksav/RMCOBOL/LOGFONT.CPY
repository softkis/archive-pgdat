      *
      *    Logical Font Definitions
      *
        01 LogicalFont.
           02  LF-Height                        Picture S9(5) Binary(2).
           02  LF-Width                         Picture 9(5) Binary(2).
           02  LF-Escapement                    Picture 9(5) Binary(2).
           02  LF-Orientation                   Picture 9(5) Binary(2).
           02  LF-WeightValue                   Picture 9(3) Binary(2).
               88  LF-WeightIsDontCare            Value 0.
               88  LF-WeightIsThin                Value 100.
               88  LF-WeightIsExtraLight          Value 200.
               88  LF-WeightIsUltraLight          Value 200.
               88  LF-WeightIsLight               Value 300.
               88  LF-WeightIsNormal              Value 400.
               88  LF-WeightIsRegular             Value 400.
               88  LF-WeightIsMedium              Value 500.
               88  LF-WeightIsSemiBold            Value 600.
               88  LF-WeightIsDemiBold            Value 600.
               88  LF-WeightIsBold                Value 700.
               88  LF-WeightIsExtraBold           Value 800.
               88  LF-WeightIsUltraBold           Value 800.
               88  LF-WeightIsHeavy               Value 900.
               88  LF-WeightIsBlack               Value 900.
           02  LF-ItalicValue                   Picture X.
               88  LF-Italic                      Value 'Y'
                                             When False 'N'.
           02  LF-UnderlineValue                Picture X.
               88  LF-Underline                   Value 'Y'
                                             When False 'N'.
           02  LF-StrikeoutValue                Picture X.
               88  LF-Strikeout                   Value 'Y'
                                             When False 'N'.
           02  LF-CharSetValue                  Picture 9(3) Binary(2).
               88  LF-CharSetIsANSI               Value 0.
               88  LF-CharSetIsDefault            Value 1.
               88  LF-CharSetIsSymbol             Value 2.
               88  LF-CharSetIsMAC                Value 77.
               88  LF-CharSetIsShiftJIS           Value 128.
               88  LF-CharSetIsHangeul            Value 129.
               88  LF-CharSetIsJohab              Value 130.
               88  LF-CharSetIsChineseBig5        Value 136.
               88  LF-CharSetIsGreek              Value 161.
               88  LF-CharSetIsTurkish            Value 162.
               88  LF-CharSetIsHebrew             Value 177.
               88  LF-CharSetIsArabic             Value 178.
               88  LF-CharSetIsBaltic             Value 186.
               88  LF-CharSetIsRussian            Value 204.
               88  LF-CharSetIsThai               Value 222.
               88  LF-CharSetIsEastEurope         Value 238.
               88  LF-CharSetIsOEM                Value 255.
           02  LF-OutPrecisValue                Picture 9 Binary(2).
               88  LF-OutPrecisIsDefault          Value 0.
               88  LF-OutPrecisIsString           Value 1.
               88  LF-OutPrecisIsStroke           Value 3.
               88  LF-OutPrecisIsTrueType         Value 4.
               88  LF-OutPrecisIsDevice           Value 5.
               88  LF-OutPrecisIsRaster           Value 6.
               88  LF-OutPrecisIsTruTypeOnly      Value 7.
               88  LF-OutPrecisIsOutline          Value 8.
           02  LF-ClipPrecisValue               Picture 9(3) Binary(2).
               88  LF-ClipPrecisIsDefault         Value 0.
               88  LF-ClipPrecisIsStroke          Value 2.
               88  LF-ClipPrecisIsLHAngles        Value 16.
               88  LF-ClipPrecisIsEmbedded        Value 128.
           02  LF-QualityValue                  Picture 9 Binary(2).
               88  LF-QualityIsDefault            Value 0.
               88  LF-QualityIsDraft              Value 1.
               88  LF-QualityIsProof              Value 2.
           02  LF-PitchValue                    Picture 9 Binary(2).
               88  LF-PitchIsDefault              Value 0.
               88  LF-PitchIsFixed                Value 1.
               88  LF-PitchIsVariable             Value 2.
           02  LF-FamilyValue                   Picture 9 Binary(2).
               88  LF-FamilyIsDontCare            Value 0.
               88  LF-FamilyIsRoman               Value 1.
               88  LF-FamilyIsSwiss               Value 2.
               88  LF-FamilyIsModern              Value 3.
               88  LF-FamilyIsScript              Value 4.
               88  LF-FamilyIsDecorative          Value 5.
           02  LF-FaceName                      Picture X(31).
      *
      *    Font Weight Values
      *
       78  LF-WeightDontCare                      Value 0.
       78  LF-WeightThin                          Value 100.
       78  LF-WeightExtraLight                    Value 200.
       78  LF-WeightUltraLight                    Value 200.
       78  LF-WeightLight                         Value 300.
       78  LF-WeightNormal                        Value 400.
       78  LF-WeightRegular                       Value 400.
       78  LF-WeightMedium                        Value 500.
       78  LF-WeightSemiBold                      Value 600.
       78  LF-WeightDemiBold                      Value 600.
       78  LF-WeightBold                          Value 700.
       78  LF-WeightExtraBold                     Value 800.
       78  LF-WeightUltraBold                     Value 800.
       78  LF-WeightHeavy                         Value 900.
       78  LF-WeightBlack                         Value 900.
      *
      *    Font Character Set Values
      *
       78  LF-CharSetANSI                        Value 0.
       78  LF-CharSetDefault                     Value 1.
       78  LF-CharSetSymbol                      Value 2.
       78  LF-CharSetMAC                         Value 77.
       78  LF-CharSetShiftJIS                    Value 128.
       78  LF-CharSetHangeul                     Value 129.
       78  LF-CharSetJohab                       Value 130.
       78  LF-CharSetChineseBig5                 Value 136.
       78  LF-CharSetGreek                       Value 161.
       78  LF-CharSetTurkish                     Value 162.
       78  LF-CharSetHebrew                      Value 177.
       78  LF-CharSetArabic                      Value 178.
       78  LF-CharSetBaltic                      Value 186.
       78  LF-CharSetRussian                     Value 204.
       78  LF-CharSetThai                        Value 222.
       78  LF-CharSetEastEurope                  Value 238.
       78  LF-CharSetOEM                         Value 255.
      *
      *    Font Output Precision Values
      *
       78  LF-OutPrecisDefault                   Value 0.
       78  LF-OutPrecisString                    Value 1.
       78  LF-OutPrecisStroke                    Value 3.
       78  LF-OutPrecisTrueType                  Value 4.
       78  LF-OutPrecisDevice                    Value 5.
       78  LF-OutPrecisRaster                    Value 6.
       78  LF-OutPrecisTruTypeOnly               Value 7.
       78  LF-OutPrecisOutline                   Value 8.
      *
      *    Font Clipping Precision Values
      *
       78  LF-ClipPrecisDefault                  Value 0.
       78  LF-ClipPrecisStroke                   Value 2.
       78  LF-ClipPrecisLHAngles                 Value 16.
       78  LF-ClipPrecisEmbedded                 Value 128.
      *
      *    Font Quality Values
      *
       78  LF-QualityDefault                     Value 0.
       78  LF-QualityDraft                       Value 1.
       78  LF-QualityProof                       Value 2.
      *
      *    Font Pitch Values
      *
       78  LF-PitchDefault                       Value 0.
       78  LF-PitchFixed                         Value 1.
       78  LF-PitchVariable                      Value 2.
      *
      *    Font Family Values
      *
       78  LF-FamilyDontCare                     Value 0.
       78  LF-FamilyRoman                        Value 1.
       78  LF-FamilySwiss                        Value 2.
       78  LF-FamilyModern                       Value 3.
       78  LF-FamilyScript                       Value 4.
       78  LF-FamilyDecorative                   Value 5.
      *
      *    Parameter Name Values
      *
       78  LF-HeightParam                        Value "Height".
       78  LF-WidthParam                         Value "Width".
       78  LF-EscapementParam                    Value "Escapement".
       78  LF-OrientationParam                   Value "Orientation".
       78  LF-WeightParam                        Value "Weight".
       78  LF-ItalicParam                        Value "Italic".
       78  LF-UnderlineParam                     Value "Underline".
       78  LF-StrikeOutParam                     Value "Strike Out".
       78  LF-CharSetParam                       Value "Char Set".
       78  LF-OutPrecisionParam                  Value "Out Precision".
       78  LF-ClipPrecisionParam                 Value "Clip Precision".
       78  LF-QualityParam                       Value "Quality".
       78  LF-PitchParam                         Value "Pitch".
       78  LF-FamilyParam                        Value "Family".
       78  LF-FaceNameParam                      Value "Face Name".
