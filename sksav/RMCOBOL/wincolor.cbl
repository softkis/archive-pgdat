       IDENTIFICATION DIVISION.
       PROGRAM-ID. "wincolor".
      *
      * Title:  wincolor.cbl
      *         RM/COBOL-85 Test WIndow Colors
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      *AUTHOR: RAH.
      *DATE WRITTEN: 3/01/90
      *PROGRAM DESCRIPTION.
      *This program test color attributes displayed with windows.
      *
      *INPUT-FILE - None
      *OPERATOR-RESPONSE - Enter selection number from terminal.
      *OUTPUT-FILE - None
      *
      * Version Identification:
      *   $Revision:   6.0.1.1  $
      *   $Date:   21 Sep 1999 13:20:10  $
      *
       ENVIRONMENT DIVISION.
       DATA DIVISION.

       WORKING-STORAGE SECTION.
       77  X                            PIC X.
       77  S                            PIC 99 BINARY VALUE 0.
       77  A                            PIC 99 BINARY VALUE 0.
       77  L                            PIC 999.
       77  P                            PIC 999.

       COPY "win.cpy".

       01  COLOR-TABLE.
          03  FILLER                    PIC X(109) VALUE
              "FCOLOR=BLUE   ,BCOLOR=GREEN  ".
          03  FILLER                    PIC X(109) VALUE
              "FCOLOR=GREEN  ,BCOLOR=CYAN   ".
          03  FILLER                    PIC X(109) VALUE
              "FCOLOR=CYAN   ,BCOLOR=RED    ".
          03  FILLER                    PIC X(109) VALUE
              "FCOLOR=RED    ,BCOLOR=MAGENTA".
          03  FILLER                    PIC X(109) VALUE
              "FCOLOR=MAGENTA,BCOLOR=BROWN  ".
          03  FILLER                    PIC X(109) VALUE
              "FCOLOR=BROWN  ,BCOLOR=WHITE  ".
          03  FILLER                    PIC X(109) VALUE
              "FCOLOR=WHITE  ,BCOLOR=BLACK  ".
          03  FILLER                    PIC X(109) VALUE
              "FCOLOR=BLACK  ,BCOLOR=BLUE   ".
       01  COLOR-TAB REDEFINES COLOR-TABLE.
          03  COLOR-VALUES OCCURS 8 TIMES.
              05  COLOR-CONTROL         PIC X(29).
              05  COLOR-WCB             PIC X(80).

       01  ATTRB-TABLE.
           03  FILLER                   PIC X(8)  VALUE "HIGH   ,".
           03  FILLER                   PIC X(8)  VALUE "LOW    ,".
           03  FILLER                   PIC X(8)  VALUE "OFF    ,".
           03  FILLER                   PIC X(8)  VALUE "REVERSE,".
           03  FILLER                   PIC X(8)  VALUE "BLINK  ,".
           03  FILLER                   PIC X(8)  VALUE "BEEP   ,".
           03  FILLER                   PIC X(8)  VALUE "ERASE  ,".
       01  ATTRB-TAB REDEFINES ATTRB-TABLE.
           03  ATTRB                    OCCURS 7 TIMES PIC X(8).

       01  WIN-CONTROL                  PIC X(80) VALUE SPACES.

       LINKAGE SECTION.
       01 SCREEN-NUM-ROWS               PIC 999.
       01 SCREEN-NUM-COLS               PIC 999.

       SCREEN SECTION.
       01  STATUS-SCREEN.
           05 BLANK SCREEN.
           05  LINE 2 COL 2 HIGHLIGHT
               " Status = ".
           05  LINE 2 COL 14 HIGHLIGHT
               PIC 999 USING WINDOW-STATUS.
      /
       PROCEDURE DIVISION USING SCREEN-NUM-ROWS, SCREEN-NUM-COLS.
       BEGIN-MAIN.
           DISPLAY SPACE LINE 1 ERASE EOS.
      *Define and create window.
           COMPUTE WCB-NUM-ROWS = ( SCREEN-NUM-ROWS - 4 ) / 2.
           COMPUTE WCB-NUM-COLS = ( SCREEN-NUM-COLS - 8 ) / 4.
           MOVE "S" TO WCB-LOCATION-REFERENCE.
           MOVE "Y" TO WCB-BORDER-SWITCH.
           MOVE  0  TO WCB-BORDER-TYPE.
           MOVE "*" TO WCB-BORDER-CHAR.
           MOVE "T" TO WCB-TITLE-LOCATION.
           MOVE "C" TO WCB-TITLE-POSITION.
           MOVE  " Window Color "
                   TO WCB-TITLE.
           MOVE 14 TO WCB-TITLE-LENGTH.

      *Display 8 Window color combinations with 7 attributes.
           PERFORM VARYING A FROM 1 BY 1 UNTIL A > 7
               MOVE 2 TO L, P
               PERFORM VARYING S FROM 1 BY 1 UNTIL S > 8
                   PERFORM DISPLAY-WINDOWS
                   COMPUTE P = P + WCB-NUM-COLS + 2
                   IF S = 4 THEN
                       COMPUTE L = L + WCB-NUM-ROWS + 2
                       MOVE 2 TO P
                   END-IF
               END-PERFORM
               ACCEPT X LINE WCB-NUM-ROWS POSITION WCB-NUM-COLS
                       NO BEEP
               PERFORM REMOVE-WINDOWS VARYING S
                       FROM 8 BY -1 UNTIL S < 1
           END-PERFORM.

      *Display same window with 7 different attributes.
           MOVE 1 TO S.
           MOVE SPACES TO WIN-CONTROL.
           MOVE 2 TO L, P.
           MOVE 1 TO A.
           PERFORM VARYING S FROM 1 BY 1 UNTIL S > 7
               PERFORM DISPLAY-WINDOWS
               ADD 1 TO A
               COMPUTE P = P + WCB-NUM-COLS + 2
               IF S = 4 THEN
                   COMPUTE L = L + WCB-NUM-ROWS + 2
                   MOVE 2 TO P
               END-IF
           END-PERFORM.
           ACCEPT X LINE WCB-NUM-ROWS POSITION WCB-NUM-COLS NO BEEP
           PERFORM REMOVE-WINDOWS VARYING S
                   FROM 7 BY -1 UNTIL S < 1.
           EXIT PROGRAM.
           STOP RUN.
      /
      *Display WINDOW COLORS.
       DISPLAY-WINDOWS.
           STRING "WINDOW-CREATE,", ATTRB(A), COLOR-CONTROL(S)
                   DELIMITED BY SIZE INTO WIN-CONTROL.
           MOVE WCB TO COLOR-WCB (S).
           DISPLAY COLOR-WCB (S) LINE L POSITION P
                   CONTROL WIN-CONTROL.
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY STATUS-SCREEN.
           DISPLAY COLOR-CONTROL(S) (1:14)  LINE 4 POSITION 3,
                   COLOR-CONTROL(S) (16:14) LINE 5 POSITION 3,
                   "ATTRIB="                LINE 6 POSITION 3,
                   ATTRB(A) (1:7)           LINE 6 POSITION 0.

      *Display WINDOW Attributes.
       DISPLAY-ATTRIBS.
           STRING "WINDOW-CREATE,", ATTRB(A),
                  "FCOLOR=RED,", "BCOLOR=BLUE"
                  DELIMITED BY SIZE INTO WIN-CONTROL.
           MOVE WCB TO COLOR-WCB (A).
           DISPLAY COLOR-WCB (A) LINE L POSITION P
                   CONTROL WIN-CONTROL.
           ACCEPT WINDOW-STATUS FROM EXCEPTION STATUS.
           DISPLAY STATUS-SCREEN.
           DISPLAY "FCOLOR=RED"             LINE 4 POSITION 3,
                   "BCOLOR=BLUE"            LINE 5 POSITION 3,
                   "ATTRIB="                LINE 6 POSITION 3,
                   ATTRB(A) (1:7)           LINE 6 POSITION 0.

      *Remove windows.
       REMOVE-WINDOWS.
           DISPLAY COLOR-WCB (S) CONTROL "WINDOW-REMOVE".
