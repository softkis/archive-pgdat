       IDENTIFICATION DIVISION.
       PROGRAM-ID.  "nuctest".
      *
      * Title:  nuctest.cbl
      *        RM/COBOL-85 Nucleus Test
      *
      * Copyright (c) 1999 Liant Software Corporation.
      *
      * You have a royalty-free right to use, modify, reproduce, and
      * distribute this COBOL source file (and/or any modified version)
      * in any way you find useful, provided that you retain this notice
      * and agree that Liant has no warranty, obligations, or liability
      * for any such use of the source file.
      *
      * Version Identification:
      *   $Revision:   6.2.1.2  $
      *   $Date:   21 Sep 1999 13:10:58  $
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.  RMCOBOL.
       OBJECT-COMPUTER.  RMCOBOL.
      *
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY "version.cpy".

       01  TITLE.
           05  PIC X(44) VALUE
           "RM/COBOL Verify nucleus functions - Version ".
           05  PIC X(4)  VALUE CURRENT-VERSION.

       01  NUM-1               PIC 999.
       01  NUM-2               PIC 999.
       01  GROUP-A.
           03  SUB-A1          PIC 9.
           03  SUB-A2          PIC 9.
           03  SUB-A3          PIC 9.
       01  GROUP-B         USAGE COMP-3.
           03  SUB-B1          PIC S9(2)V99.
           03  SUB-B2          PIC S9(5)V99.
           03  SUB-B3          PIC S9(4).
           03  SUB-B4          PIC 9(4).
       01  CORRECT             PIC X(4)    VALUE "PASS".
       01  INCORRECT           PIC X(4)    VALUE "FAIL".
       01  RETURN-KEY          PIC X.
       01  CALC-VALUE          PIC 999.
       01  EDITED-VALUE            PIC 9(9)V9(9).
       01  COUNTER             PIC 9.
      /
      *
       PROCEDURE DIVISION.
       INIT-PARA.
           MOVE 100 TO NUM-1.
           MOVE 200 TO NUM-2.
           MOVE 005 TO GROUP-A.
           MOVE .00 TO SUB-B1.
           MOVE 42.00 TO SUB-B2.
           MOVE 5.00 TO SUB-B3.
           MOVE 1 TO SUB-B4.
           MOVE ZERO TO COUNTER.
      *
       PARA-1.
           DISPLAY TITLE, LOW, LINE 1, ERASE.
           DISPLAY "This section tests Runtime nucleus functions."
                       LOW, LINE 4, POSITION 16.
           DISPLAY "OPERATION", LOW, LINE 8, POSITION 2.
           DISPLAY "DISPLAY           CALCULATED           PASS/FAIL"
                       LOW, LINE 8, POSITION 15.
      *
       TEST-1.
           COMPUTE CALC-VALUE = (SUB-A3 * 60) - NUM-1.
           DISPLAY "COMPUTE", LOW, LINE 10, POSITION 3.
           DISPLAY CALC-VALUE, LOW, LINE 10, POSITION 36.
           DISPLAY NUM-2, LOW, LINE 10, POSITION 17.
           IF CALC-VALUE = NUM-2 THEN
               DISPLAY CORRECT, LOW, LINE 10, POSITION 57
           ELSE
               DISPLAY INCORRECT, LOW, LINE 10, POSITION 57
               ADD 1 TO COUNTER
           END-IF.
      *
       TEST-2.
           COMPUTE CALC-VALUE ROUNDED = (NUM-1 / 6) / SUB-A3.
           ADD 2 TO CALC-VALUE.
           DISPLAY "COMPUTE", LOW, LINE 11, POSITION 3.
           DISPLAY CALC-VALUE, LOW, LINE 11, POSITION 36.
           DISPLAY GROUP-A, LOW, LINE 11, POSITION 17.
           IF CALC-VALUE = GROUP-A THEN
               DISPLAY CORRECT, LOW, LINE 11, POSITION 57
           ELSE
               DISPLAY INCORRECT, LOW, LINE 11, POSITION 57
               ADD 1 TO COUNTER
           END-IF.
      *
       TEST-3.
           DISPLAY "COMP-EDIT", LOW, LINE 12, POSITION 2.
           COMPUTE EDITED-VALUE = ((100 - SUB-B1) / 100 *
               SUB-B2 * SUB-B3 / SUB-B4)
               ON SIZE ERROR GO TO ERROR-MESSAGE.
           DISPLAY NUM-1, LOW, LINE 12, POSITION 17.
           DISPLAY NUM-1, LOW, LINE 12, POSITION 36.
           DISPLAY CORRECT, LOW, LINE 12, POSITION 57.
      /
      *
       TEST-4.
           MOVE NUM-1 TO CALC-VALUE.
           MULTIPLY SUB-A3 BY NUM-1.
           MOVE NUM-1 TO NUM-2.
           DIVIDE 5 INTO NUM-2.
           MOVE CALC-VALUE TO NUM-1.
           DISPLAY "CALC-MOVE", LOW, LINE 13, POSITION 2.
           DISPLAY NUM-1, LOW, LINE 13, POSITION 17.
           DISPLAY NUM-2, LOW, LINE 13, POSITION 36.
           IF NUM-1 = NUM-2 THEN
               DISPLAY CORRECT, LOW, LINE 13, POSITION 57
           ELSE
               DISPLAY INCORRECT, LOW, LINE 13, POSITION 57
               ADD 1 TO COUNTER
           END-IF.
      *
       TEST-5.
           ADD 11 TO NUM-2.
           MOVE NUM-2 TO GROUP-A.
           ADD SUB-A1, SUB-A2, SUB-A3 TO NUM-1.
           DIVIDE 3 INTO NUM-1 ROUNDED.
           ADD 77 TO NUM-1.
           DISPLAY "CALC-MOVE", LOW, LINE 14, POSITION 2.
           DISPLAY NUM-1, LOW, LINE 14, POSITION 17.
           DISPLAY NUM-2, LOW, LINE 14, POSITION 36.
           IF NUM-1 = NUM-2 THEN
               DISPLAY CORRECT, LOW, LINE 14, POSITION 57
           ELSE
               DISPLAY INCORRECT, LOW, LINE 14, POSITION 57
               ADD 1 TO COUNTER
           END-IF.
      *
       TEST-6.
           SUBTRACT SUB-A2 FROM NUM-1.
           COMPUTE CALC-VALUE = (NUM-1 / 10) - SUB-A3.
           DIVIDE 10 INTO NUM-2 ROUNDED.
           SUBTRACT SUB-B4 FROM NUM-2.
           DISPLAY "ROUNDED", LOW, LINE 15, POSITION 3.
           DISPLAY NUM-2, LOW, LINE 15, POSITION 17.
           DISPLAY CALC-VALUE, LOW, LINE 15, POSITION 36.
           IF NUM-2 = CALC-VALUE THEN
               DISPLAY CORRECT, LOW, LINE 15, POSITION 57
           ELSE
               DISPLAY INCORRECT, LOW, LINE 15, POSITION 57
               ADD 1 TO COUNTER
           END-IF.
      /
      *
       TEST-7.
           DISPLAY "OVER-FLOW", LOW, LINE 16, POSITION 2.
           COMPUTE CALC-VALUE = (101 * (SUB-B4 + 9)) / SUB-B4
               ON SIZE ERROR
                   DISPLAY "OVERFLOW", LOW, LINE 16, POSITION 15
                   DISPLAY "OVERFLOW", LOW, LINE 16, POSITION 34
                   DISPLAY CORRECT, LOW, LINE 16, POSITION 57
                   GO TO TEST-8.
           DISPLAY "SHOULD'VE HAD OVERFLOW", LOW, LINE 16, POSITION 26.
           DISPLAY INCORRECT, LOW, LINE 16, POSITION 57.
           ADD 1 TO COUNTER.
      *
       TEST-8.
           MOVE 2 TO CALC-VALUE.
           SUBTRACT 1 FROM NUM-2.
           PERFORM TEST-8-LOOP NUM-2 TIMES.
           MULTIPLY 57 BY NUM-2.
           DISPLAY "PERFORM", LOW, LINE 17, POSITION 3.
           DISPLAY NUM-2, LOW, LINE 17, POSITION 17.
           DISPLAY CALC-VALUE, LOW, LINE 17, POSITION 36.
           IF CALC-VALUE = NUM-2 THEN
               DISPLAY CORRECT, LOW, LINE 17, POSITION 57
           ELSE
               DISPLAY INCORRECT, LOW, LINE 17, POSITION 57
               ADD 1 TO COUNTER
           END-IF.
      *
       TEST-9.
           COMPUTE CALC-VALUE ROUNDED = (NUM-2 - 13) / 450.
           MOVE 001 TO GROUP-A.
           DISPLAY "ROUNDED", LOW, LINE 18, POSITION 3.
           DISPLAY GROUP-A, LOW, LINE 18, POSITION 17.
           DISPLAY CALC-VALUE, LOW, LINE 18, POSITION 36.
           IF CALC-VALUE = SUB-B4 THEN
               DISPLAY CORRECT, LOW, LINE 18, POSITION 57
           ELSE
               DISPLAY INCORRECT, LOW, LINE 18, POSITION 57
               ADD 1 TO COUNTER
           END-IF.
      /
      *
       DISPLAY-RESULTS.
           DISPLAY COUNTER, LOW, LINE 20, POSITION 40.
           IF COUNTER NOT = 1 THEN DISPLAY " errors encountered",
           LOW, POSITION 0.
           IF COUNTER = 1 THEN DISPLAY "error encountered",
           LOW, POSITION 0.
           DISPLAY "Press any key to continue ...",
                       BLINK, HIGH, LINE 24.
           ACCEPT RETURN-KEY, POSITION 0, LOW.
       EXIT-PARA.
           EXIT PROGRAM.
       STOP-PARA.
           STOP RUN.
      *
       ERROR-MESSAGE.
           DISPLAY "SIZE ERROR !", LOW, LINE 12, POSITION 26.
           DISPLAY INCORRECT, LOW, LINE 12, POSITION 57.
           ADD 1 TO COUNTER.
           GO TO TEST-4.
      *
       TEST-8-LOOP.
           MULTIPLY 2 BY CALC-VALUE.
           SUBTRACT SUB-A3 FROM CALC-VALUE.
