      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-I2008 CALCUL DES IMPOTS - SALAIRES        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-I2008.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  MINIMUM               PIC 9(6) VALUE 10335.
PM     01  MAXIMUM               PIC 9(6) VALUE 36570.
       01  ABAT-1                PIC 9(6) VALUE  1380.
       01  ABAT-2                PIC 9(6) VALUE   636.
       01  STEPS                 PIC 9(6) VALUE  1749.
       01  RONN                  PIC 9(10).
       01  CENTS                 PIC 9(6)V99.
       01  HE-Z8                 PIC -Z(8),ZZ.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       LINKAGE SECTION.
      *----------------
       01 DONNEES-BASE.
           02 IMPOSABLE       PIC 9(8)V99.
           02 JOURS-IMPOSABLE PIC 999.
           02 IMPOT           PIC 9(8)V9.
           02 IMPOT-R REDEFINES IMPOT.
              03 IMPOT-A         PIC 9(8).
              03 IMPOT-B         PIC 9.
           02 CLASSE-IMPOT.
              03 CLASSE    PIC 9.
              03 GROUPE    PIC X.
              03 ENFANTS   PIC 99.
           02 REVENU       PIC X.

       PROCEDURE DIVISION USING DONNEES-BASE. 

       START-CALC SECTION.
             
       START-PROGRAMME-0-I2008.

           MOVE 0 TO IMPOT.
           IF GROUPE = "A"
              COMPUTE MINIMUM = 10335 * 2
2/3 de        MOVE 1166 TO STEPS
1749          MOVE 9 TO IDX-2
              MOVE 3 TO IDX-3
              MOVE 9 TO IDX-4
           ELSE
              COMPUTE MINIMUM = 10335 
              MOVE 1749 TO STEPS
              MOVE  6 TO IDX-2
              MOVE  2 TO IDX-3
              MOVE 15 TO IDX-4
           END-IF.

           IF JOURS-IMPOSABLE = 300
              COMPUTE RONN  = IMPOSABLE / 50 
              COMPUTE SH-00 = RONN * 50 
           ELSE
              COMPUTE SH-00 = IMPOSABLE * 300 / JOURS-IMPOSABLE
              COMPUTE RONN  = SH-00 / 60 
              COMPUTE SH-00 = RONN * 60 
           END-IF.
           COMPUTE SH-00 = SH-00 / CLASSE.

           COMPUTE SH-00 = SH-00 - MINIMUM - (ABAT-1 / CLASSE).

           IF REVENU NOT = "P"
              COMPUTE SH-00 = SH-00 - (ABAT-2 / CLASSE)
           END-IF.

           IF SH-00 <= 0
              EXIT PROGRAM.

           DIVIDE SH-00 BY STEPS GIVING IDX-1 REMAINDER SH-01.

           IF IDX-1 > IDX-4
              COMPUTE SH-01 = SH-01 + (STEPS * (IDX-1 - IDX-4))
              MOVE IDX-4 TO IDX-1
           END-IF.
           MOVE 0 TO SH-00.
           IF IDX-1 > 0
              PERFORM PALIERS VARYING IDX FROM 1 BY 1 UNTIL IDX > IDX-1.

           IF IDX-2 < 38
              ADD IDX-3 TO IDX-2
           IF IDX-2 > 38
              MOVE 38 TO IDX-2.

           COMPUTE SH-00 = SH-00 + (SH-01 / 100 * IDX-2).

           IF JOURS-IMPOSABLE = 300
              COMPUTE RONN = SH-00 * CLASSE
              COMPUTE SH-00 = RONN * 1,025 
           ELSE
              COMPUTE IMPOT = SH-00 * CLASSE / 12
              IF JOURS-IMPOSABLE = 25
                 COMPUTE SH-00 = IMPOT * 1,025 
              ELSE
                 COMPUTE CENTS = IMPOT / 25
                 COMPUTE CENTS = CENTS * 1,025 
                 IF CENTS < ,04
                    MOVE 0 TO CENTS
                 END-IF
                 COMPUTE IMPOT = CENTS * JOURS-IMPOSABLE 
                 COMPUTE SH-00 = IMPOT 
              END-IF
           END-IF.
           COMPUTE SH-01 = JOURS-IMPOSABLE * ,04.
           IF SH-00 < SH-01
              MOVE 0 TO SH-00
           END-IF.     
           MOVE SH-00 TO IMPOT.
           IF JOURS-IMPOSABLE = 300
              MOVE 0 TO IMPOT-B.
           EXIT PROGRAM.
          
       PALIERS.
           ADD IDX-3 TO IDX-2.
           COMPUTE SH-00 = SH-00 + (STEPS / 100 * IDX-2).

