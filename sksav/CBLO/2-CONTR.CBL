      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-CONTR RECHERCHE TYPES CONTRAT             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-CONTR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CONTRAT.REC".
           COPY "CONTYPE.REC".
           COPY "MOTDEP.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.

       01  CHOIX                 PIC 9(8).
       01  COMPTEUR              PIC 99.

       01 HE-CONTR.
          02 H-C OCCURS 20.
             03 H-A PIC 9(4).
             03 H-M PIC 99.
             03 H-J PIC 99.


       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ.
           02 HE-Z3 PIC ZZZ.
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z4Z2 PIC Z(4),ZZ BLANK WHEN ZERO.
           02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8).
           02 HE-ACCEPT.
              03 HE-A     PIC ZZZZ.
              03 HE-M     PIC ZZ.
           02 HE-DATE.
              03 HE-JJ    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-MM    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-AA    PIC ZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "CONTRAT.LNK".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD PR-RECORD REG-RECORD.

       START-DISPLAY SECTION.
             
       START-2-CONTR.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE CON-RECORD IDX-1 EXC-KEY COMPTEUR CHOIX HE-CONTR.
           MOVE 4 TO LIN-IDX.
           MOVE 9999 TO CON-DEBUT-A.
           PERFORM READ-CAR THRU READ-CONTR-END.
           IF EXC-KEY = 66 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-CAR.
           MOVE 65 TO EXC-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD EXC-KEY.
           IF CON-FIRME = 0
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 INITIALIZE HE-CONTR IDX-1
                 GO READ-CAR
              END-IF
              GO READ-CONTR-END.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 INITIALIZE HE-CONTR IDX-1
                 IF EXC-KEY = 65 
                    MOVE 9999 TO CON-DEBUT-A
                 END-IF
                 GO READ-CAR
              END-IF
              IF CHOIX NOT = 0
                 GO READ-CONTR-END
              END-IF
           END-IF.
           GO READ-CAR.
       READ-CONTR-END.
           IF CHOIX = 0
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82 AND NOT = 68
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX COMPTEUR IDX-1.

           MOVE CON-DEBUT-A TO HE-AA H-A(IDX-1).
           MOVE CON-DEBUT-M TO HE-MM H-M(IDX-1).
           MOVE CON-DEBUT-J TO HE-JJ H-J(IDX-1).
           DISPLAY HE-DATE LINE LIN-IDX POSITION 1.

           MOVE CON-FIN-A TO HE-AA.
           MOVE CON-FIN-M TO HE-MM.
           MOVE CON-FIN-J TO HE-JJ.
           DISPLAY HE-DATE LINE LIN-IDX POSITION 10 LOW.

           MOVE CON-TYPE TO CT-CODE.
           CALL "6-CONTYP" USING LINK-V CT-RECORD FAKE-KEY.
           DISPLAY CT-NOM LINE LIN-IDX POSITION 19 SIZE 19. 

           MOVE CON-MOTIF-DEPART TO MD-CODE.
           CALL "6-MOTDEP" USING LINK-V MD-RECORD FAKE-KEY.
           DISPLAY MD-NOM LINE LIN-IDX POSITION 38 SIZE 18. 
           MOVE CON-MOTIF-INTERNE TO MD-CODE.
           CALL "6-MOTDEP" USING LINK-V MD-RECORD FAKE-KEY.
           DISPLAY MD-NOM LINE LIN-IDX POSITION 56 SIZE 17 LOW.

           MOVE CON-PREAVIS-A TO HE-AA.
           MOVE CON-PREAVIS-M TO HE-MM.
           MOVE CON-PREAVIS-J TO HE-JJ.
           DISPLAY HE-DATE LINE LIN-IDX POSITION 73. 

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0000530000 TO EXC-KFR (11).
           MOVE 0000000065 TO EXC-KFR (13).
           MOVE 6667000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE CHOIX.
           ACCEPT CHOIX 
              LINE 3 POSITION 2 SIZE 1
              TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF CHOIX NOT = 0
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IDX-1.

       AFFICHAGE-ECRAN.
           MOVE 2205 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 10.
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 20 SIZE 50.

       END-PROGRAM.
           INITIALIZE CON-RECORD.
           MOVE CHOIX TO CON-DATE-DEBUT.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD FAKE-KEY.
           IF CON-DEBUT-A NOT = 0
              MOVE CON-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR (1).
           MOVE 0000000000 TO EXC-KFR (2).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IDX-1 + 4.
           MOVE H-J(IDX-1) TO HE-JJ.
           ACCEPT HE-JJ
             LINE  LIN-IDX POSITION 1 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           MOVE H-J(IDX-1) TO HE-JJ.
           DISPLAY HE-JJ LINE LIN-IDX POSITION 1.
           IF EXC-KEY = 18 
              MOVE H-C(IDX-1) TO CON-DATE-DEBUT
              CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD DEL-KEY
              EXIT PROGRAM
           END-IF.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN OTHER MOVE H-C(IDX-1) TO CHOIX
                     PERFORM END-PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-A(IDX-1) = 0
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.
                        