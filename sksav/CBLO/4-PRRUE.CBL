      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-PRRUE STRING RUE PERSONNE                 �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-PRRUE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

       01  POINT-IDX      PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "PERSON.REC".

       PROCEDURE DIVISION USING LINK-V PR-RECORD.

       START-DISPLAY SECTION.
             
       START-4-PRRUE.
           INITIALIZE LNK-TEXT.
           MOVE 1 TO POINT-IDX.
           STRING PR-MAISON DELIMITED BY "   "  INTO LNK-TEXT 
           WITH POINTER POINT-IDX.

           ADD 1 TO POINT-IDX.
           STRING PR-RUE DELIMITED BY SIZE INTO LNK-TEXT 
           WITH POINTER POINT-IDX.
           EXIT PROGRAM.


