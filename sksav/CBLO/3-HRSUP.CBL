      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-HRSUP IMPRESSION                          �
      *  � RELEVE DES HEURES SUPPLEMENTAIRES PRESTEES            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-HRSUP.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".
           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  CHOIX-MAX             PIC 99 VALUE 6.
       01  PRECISION             PIC 9 VALUE 1.
       01  JOB-STANDARD          PIC X(10) VALUE "80        ".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "OCCUP.REC".
           COPY "STATUT.REC".
           COPY "COUT.REC".
           COPY "LIVRE.REC".
           COPY "V-VAR.CPY".
           COPY "IMPRLOG.REC".

       01  END-NUMBER            PIC  9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  COMPTEUR-SEMAINE      PIC 999V99.
       01  COMPTEUR              PIC 999.

       01  TOTAL-GLOBAL.
           02 T-G OCCURS 12.
              03 TOT-GLOBAL  PIC 9999V99 COMP-3 OCCURS 7.

       01  TOTAL-HEURES.
           02 TOT-HRS        PIC 999V99 COMP-3 OCCURS 32.

       77  MOIS   PIC 99.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".


       01  FORM-NAME             PIC X(8) VALUE "FORM.RHS".

       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM JOURS.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-HRSUP.

           MOVE 0 TO COMPTEUR.
           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN INPUT JOURS.

           CALL "0-TODAY" USING TODAY.

           INITIALIZE TOTAL-GLOBAL.
           MOVE LNK-MOIS TO SAVE-MOIS.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-PERSON
                    PERFORM READ-PERSON THRU READ-PERSON-END
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-PERSON.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
              GO READ-PERSON-END
           END-IF.
           IF LNK-VAL > 1 
              GO READ-PERSON
           END-IF.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF STATUT > 0 AND NOT = CAR-STATUT  
              GO READ-PERSON
           END-IF.
           IF COUT > 0 AND NOT = CAR-COUT
              GO READ-PERSON
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM LIVRE-ANNEE.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
               GO READ-PERSON
           END-IF.
       READ-PERSON-END.
           IF COMPTEUR > 0
              PERFORM FILL-ALL
              PERFORM TRANSMET.
           PERFORM END-PROGRAM.
                
       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           MOVE 1 TO INPUT-ERROR.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.

       LIVRE-ANNEE.
           PERFORM ANNEE THRU ANNEE-END
           VARYING MOIS FROM 1 BY 1 UNTIL MOIS > LNK-MOIS.

       ANNEE.
           INITIALIZE L-RECORD.
           MOVE MOIS TO L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           COMPUTE VH-00 = L-UNI-HRS-SUP + L-UNI-RECUP-PLUS.
           IF VH-00 = 0
              GO ANNEE-END
           END-IF.
           IF COUT > 0 AND COUT NOT = L-COUT  
              GO ANNEE-END
           END-IF.
           ADD 1 TO TOT-GLOBAL(MOIS, 1) COMPTEUR.
           ADD VH-00 TO TOT-GLOBAL(MOIS, 2).
           IF VH-00 > TOT-GLOBAL(MOIS, 4)
              MOVE VH-00 TO TOT-GLOBAL(MOIS, 4)
           END-IF.
           PERFORM INIT-HEURES THRU INIT-HEURES-END.
       ANNEE-END.
           EXIT.

       INIT-HEURES.
           INITIALIZE TOTAL-HEURES JRS-RECORD.
           MOVE FR-KEY TO JRS-FIRME-3.
           MOVE MOIS TO JRS-MOIS-3.
           MOVE REG-PERSON TO JRS-PERSON-3.
           START JOURS KEY >= JRS-KEY-3 INVALID GO INIT-HEURES-END.
       INIT-HEURES-1.
           READ JOURS NEXT NO LOCK AT END GO INIT-HEURES-END.
           IF JRS-FIRME  NOT = FR-KEY 
           OR JRS-MOIS   NOT = MOIS  
           OR JRS-PERSON NOT = REG-PERSON 
              GO INIT-HEURES-END.
           IF JRS-COMPLEMENT > 0
           OR JRS-POSTE      > 0
              GO INIT-HEURES-1.
           MOVE JRS-OCCUPATION TO OCC-KEY.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY.
           IF OCC-HRS-SUPPL = 0
              GO INIT-HEURES-1.
           PERFORM ADD-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.
           GO INIT-HEURES-1.
       INIT-HEURES-END.
           MOVE 0 TO COMPTEUR-SEMAINE.
           PERFORM TEST-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.
           IF COMPTEUR-SEMAINE > TOT-GLOBAL(MOIS, 5)
              MOVE COMPTEUR-SEMAINE TO TOT-GLOBAL(MOIS, 5)
           END-IF.

       ADD-HRS.
           IF JRS-HRS(IDX) > 0
              ADD JRS-HRS(IDX) TO TOT-HRS(IDX)
           END-IF.

       TEST-HRS.
           IF TOT-HRS(IDX) > TOT-GLOBAL(MOIS, 6)
              MOVE TOT-HRS(IDX) TO TOT-GLOBAL(MOIS, 6)
           END-IF.
           IF SEM-IDX(MOIS, IDX) = 1 
              IF COMPTEUR-SEMAINE > TOT-GLOBAL(MOIS, 5)
                 MOVE COMPTEUR-SEMAINE TO TOT-GLOBAL(MOIS, 5)
              END-IF
              MOVE TOT-HRS(IDX) TO COMPTEUR-SEMAINE
           ELSE
              ADD  TOT-HRS(IDX) TO COMPTEUR-SEMAINE
           END-IF.
           IF SEM-IDX(MOIS, IDX) > 5 
           IF TOT-HRS(IDX) > TOT-GLOBAL(MOIS, 7)
              MOVE TOT-HRS(IDX) TO TOT-GLOBAL(MOIS, 7)
           END-IF.

       FILL-ALL.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              PERFORM READ-FORM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE LIN-NUM =  5.
           MOVE 9 TO COL-NUM.
           MOVE LNK-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
      * DONNEES FIRME
           MOVE 23 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM =  7.
           MOVE  2 TO COL-NUM.
           MOVE COUT-NOM TO ALPHA-TEXTE.
           IF COUT-NUMBER NOT = 0
              PERFORM FILL-FORM.

      * DATE EDITION
           COMPUTE LIN-NUM =  5.
           MOVE 68 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 71 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 74 TO COL-NUM.
           MOVE TODAY-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           PERFORM FILL VARYING MOIS FROM 1 BY 1 UNTIL MOIS > LNK-MOIS.

       FILL.
           COMPUTE LIN-NUM = 14 + MOIS * 2.
           COMPUTE VH-00 = TOT-GLOBAL(MOIS, 2) / TOT-GLOBAL(MOIS, 1).
           MOVE VH-00 TO TOT-GLOBAL(MOIS, 3).
           COMPUTE VH-00 = TOT-GLOBAL(MOIS, 5) - 40.
           IF VH-00 > 0
              MOVE VH-00 TO TOT-GLOBAL(MOIS, 5)
           ELSE
              MOVE TOT-GLOBAL(MOIS, 4) TO TOT-GLOBAL(MOIS, 5)
           END-IF.
           COMPUTE VH-00 = TOT-GLOBAL(MOIS, 6) - 8.
           IF VH-00 > 0
              MOVE VH-00 TO TOT-GLOBAL(MOIS, 6)
           ELSE
              MOVE TOT-GLOBAL(MOIS, 7) TO TOT-GLOBAL(MOIS, 6)
           END-IF.

           PERFORM FILL-LINE VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 6.

       FILL-LINE.
           COMPUTE COL-NUM = 2 + IDX-1 * 11.
           COMPUTE VH-00 = TOT-GLOBAL(MOIS, IDX-1).
           MOVE 6 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

       AFFICHAGE-ECRAN.
           MOVE 1203 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           CLOSE JOURS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XACTION.CPY".

