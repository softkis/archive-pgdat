      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-GRAT IMPRESSION / CREATION SALES          �
      *  � GRATIFICATION MOYENNE SALAIRE DE BASE DE L'ANNEE      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-GRAT.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".

       WORKING-STORAGE SECTION.

       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  JOB-STANDARD          PIC X(10) VALUE "80       ".
       01  TIRET-TEXTE           PIC X VALUE "�".
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "STATUT.REC".
           COPY "LIVRE.REC".
           COPY "IMPRLOG.REC".
           COPY "CODPAR.REC".
           COPY "CODPAIE.REC".
           COPY "CONTRAT.REC".
           COPY "PARMOD.REC".
           COPY "LIVREX.REC".

           COPY "V-VAR.CPY".

       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  PERIODE               PIC 99 VALUE 0.    
       01  PRIME                 PIC 9(6)V99.
       01  CREATE-CODE           PIC X VALUE "N".
       01  COMPTEUR              PIC 999 VALUE 0.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".GRT".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ.
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).
           02 HE-Z3Z2 PIC ZZZ,ZZ. 

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-GRAT.
       

           INITIALIZE PARMOD-RECORD.
           INITIALIZE PARMOD-SETTINGS.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY         TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           IF PARMOD-PRIME = 0
              MOVE "Y" TO PM-SET(100)
              MOVE 100 TO PARMOD-PRIME
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

           PERFORM AFFICHAGE-ECRAN.
           
           MOVE 1 TO INPUT-ERROR.

           INITIALIZE LIN-NUM LIN-IDX.
           CALL "6-GCP" USING LINK-V CP-RECORD.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           CALL "0-TODAY" USING TODAY.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2  MOVE 0063640400 TO EXC-KFR(1)
                   MOVE 0000000065 TO EXC-KFR(13)
                   MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7  MOVE 0000000025 TO EXC-KFR(1)
                   MOVE 1700000000 TO EXC-KFR(2)
                   MOVE 0052000000 TO EXC-KFR(11)
                   MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5
           WHEN  6 PERFORM AVANT-6
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-5.
           MOVE PARMOD-PRIME TO HE-Z3Z2.
           ACCEPT HE-Z3Z2
             LINE 14 POSITION 30 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.
           INSPECT HE-Z3Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z3Z2 TO PARMOD-PRIME.

       AVANT-6.
           ACCEPT CREATE-CODE
             LINE 16 POSITION 35 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-5.
           IF PARMOD-PRIME = 0
              MOVE 100 TO PARMOD-PRIME.
           PERFORM DIS-HE-05.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       APRES-6.
           IF CREATE-CODE = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM TRAITEMENT
            WHEN 68 CALL "5-SELLIV" USING LINK-V PARMOD-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-PERSON-END.
           PERFORM END-PROGRAM.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-PERSON.
           PERFORM NEXT-REGIS
           PERFORM DIS-HE-01.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-PERSON-END
           END-IF.

      * D굋art en cours d'ann괻
           IF LNK-MOIS < 12
              PERFORM READ-CONTRAT
              IF  CON-FIN-A = LNK-ANNEE
              AND CON-FIN-M = LNK-MOIS
                 CONTINUE
              ELSE
                 GO READ-PERSON-2
              END-IF
           END-IF.
       READ-PERSON-1.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF STATUT > 0 AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           PERFORM FILL-FILES.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
               GO READ-PERSON.
       READ-PERSON-END.
           EXIT.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           MOVE "AA" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           MOVE 1 TO INPUT-ERROR.

       DIS-HE-01.
           DISPLAY A-N LINE 4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           IF A-N = "N"
              DISPLAY LNK-TEXT LINE 6 POSITION 27 SIZE 27
           ELSE
              DISPLAY SPACES   LINE 6 POSITION 37 SIZE 1
              DISPLAY LNK-TEXT LINE 6 POSITION 38 SIZE 18.
       DIS-HE-STAT.
           DISPLAY STAT-NOM  LINE 9 POSITION 35 SIZE 15.
       DIS-HE-05.
           MOVE PARMOD-PRIME TO HE-Z3Z2.
           DISPLAY HE-Z3Z2  LINE 14 POSITION 30.
       DIS-HE-END.
           EXIT.

       FILL-FIRME.

      *    PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  3 TO CAR-NUM.
           MOVE  6 TO LIN-NUM.
           MOVE 118 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

           MOVE  2 TO LIN-NUM.
           MOVE 80 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.

           PERFORM FILL-FORM.
           MOVE  4 TO LIN-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE 111 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 114 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 117 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE 4 TO LIN-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE 8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           MOVE 15 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 5 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 6 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-CODE-POST TO VH-00.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       FILL-FILES.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE IDX = LIN-NUM + 3.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM FILL-FIRME
              MOVE LIN-IDX TO LIN-NUM
           END-IF.

      * DONNEES PERSONNE
           ADD 1 TO LIN-NUM.
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  6 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           PERFORM CUMUL-LIVRE.

           COMPUTE VH-00 = SH-00.
           MOVE 45 TO COL-NUM.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = SH-00 * PARMOD-PRIME / 1200 + ,005.
           COMPUTE PRIME = VH-00 .
           MOVE 67 TO COL-NUM.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.
           IF CREATE-CODE NOT = "N" 
              IF COD-PAR(197, 1)  > 0
              OR MENU-PROG-NUMBER > 0
                 PERFORM WRITE-CS.

       WRITE-CS.
           INITIALIZE CSP-RECORD.
           MOVE COD-PAR(197, 1) TO CSP-CODE. 
           IF MENU-PROG-NUMBER > 0
              MOVE MENU-PROG-NUMBER TO CSP-CODE. 
           MOVE PRIME TO CSP-TOTAL.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.
           CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       CUMUL-LIVRE.
           INITIALIZE L-RECORD SH-00 LNK-SUITE.
           PERFORM CUM-AN VARYING IDX FROM 1 BY 1 UNTIL IDX > LNK-MOIS.
       CUM-AN.
           MOVE IDX TO L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           PERFORM DEBIT VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 100.

       DEBIT.
           IF PM-SET(IDX-1) NOT = "N" AND NOT = " "
              ADD L-DEB(IDX-1) TO SH-00.

       AFFICHAGE-ECRAN.
           MOVE 515  TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE 0 TO COMPTEUR.
           PERFORM DIS-LEX VARYING IDX FROM 1 BY 1 UNTIL IDX > 100.

       DIS-LEX.
           IF PM-SET(IDX) NOT = "N" AND NOT = " "
              ADD 1 TO COMPTEUR 
              COMPUTE LIN-IDX = COMPTEUR + 2
              MOVE IDX TO HE-Z4 LEX-NUMBER
              CALL "6-LEX" USING LINK-V LEX-RECORD FAKE-KEY
              IF COMPTEUR < 22
                 DISPLAY HE-Z4 LINE LIN-IDX POSITION 53
                 DISPLAY LEX-DESCRIPTION LINE LIN-IDX POSITION 58 
                 SIZE 23 LOW
              END-IF
           END-IF.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.
           

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           CANCEL "5-SELLIV".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XACTION.CPY".


