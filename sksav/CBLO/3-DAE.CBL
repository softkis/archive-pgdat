      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-DAE IMPRESSION DEMANDE D'AIDES A          �
      *  � L'EMBAUCHE DE CHOMEURS AGES ET DE LONGUE DUREE        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-DAE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 5.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "LIVRE.REC".
           COPY "CODPAIE.REC".
           COPY "BANQF.REC".
           COPY "BANQUE.REC".
           COPY "PARMOD.REC".
           COPY "CONTRAT.REC".
           COPY "MOTDEP.REC".
           COPY "TXACCID.REC".

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  COUNTER               PIC 9999 VALUE 0.
       01  TRIMESTER             PIC 9 VALUE 0.
       01  ARR                   PIC 9(8)V99.

       01 HELP-CUMUL.
          02 HELP-TOTAL      PIC 9(8)V99.
          02 HELP-ACTIF      PIC 9.
          02 HELP-INDIVIDUEL OCCURS 13.
             03 HELP-PA              PIC 9(8)V99.
             03 HELP-PP              PIC 9(8)V99.

           COPY "V-VAR.CPY".

       01  LINK-ECRIT.
           02  TOTAL-CHIFFRE     PIC X(100).
           02  REDEF-CHIFFRE REDEFINES TOTAL-CHIFFRE.
               03 CHIFFRE   PIC X OCCURS 100.
           02  TOTAL-VAL         PIC 9(9)V99.
           02  CHIFFRE-POINT     PIC 999 COMP-4.
        
       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z2Z2 PIC ZZ,ZZ.
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6Z2 PIC Z(6),ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       01  DAE-RECORD.
           10  DAE-ENTREPRISE    PIC X(0080).
           10  DAE-MONTANT       PIC X(0012).
           10  DAE-TOUTLETT      PIC X(0080).
           10  DAE-NOM           PIC X(0080).
           10  DAE-TAUX-ACC      PIC X(0005).
           10  DAE-MOIS1-ALPHA   PIC X(0020).
           10  DAE-MOIS1-PPAT    PIC X(0012).
           10  DAE-MOIS1-PASS    PIC X(0012).
           10  DAE-MOIS2-ALPHA   PIC X(0020).
           10  DAE-MOIS2-PPAT    PIC X(0012).
           10  DAE-MOIS2-PASS    PIC X(0012).
           10  DAE-MOIS3-ALPHA   PIC X(0020).
           10  DAE-MOIS3-PPAT    PIC X(0012).
           10  DAE-MOIS3-PASS    PIC X(0012).
           10  DAE-TOTAL-PPAT    PIC X(0012).
           10  DAE-TOTAL-PPASS   PIC X(0012).
           10  DAE-LIEU-DATE     PIC X(0040).


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-DAE.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-PATH TO BQF-CODE.
           MOVE LNK-MOIS TO SAVE-MOIS.
           DIVIDE LNK-MOIS BY 3 GIVING TRIMESTER REMAINDER IDX-2.
           IF IDX-2 > 0
              ADD 1 TO TRIMESTER
           END-IF.
           COMPUTE MOIS-FIN = 3 * TRIMESTER.
           COMPUTE MOIS-DEBUT = MOIS-FIN - 2.
           CALL "0-TODAY" USING TODAY.
           INITIALIZE TXA-RECORD.
           CALL "6-ASSACC" USING LINK-V TXA-RECORD.
           IF FR-ACCIDENT = 0
              MOVE 1 TO FR-ACCIDENT 
           END-IF.
           INITIALIZE HELP-CUMUL.

           PERFORM AFFICHAGE-ECRAN .
           MOVE 66 TO SAVE-KEY.
           IF BQF-CODE = SPACES
              PERFORM NEXT-BANQF.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
      *    WHEN  4 PERFORM AVANT-4
           WHEN 5  PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
      *    WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-4.
           ACCEPT BQF-CODE
             LINE 15 POSITION 25 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-4.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-BANQF" USING LINK-V BQF-RECORD
                   IF BQF-CODE NOT = SPACES
                      MOVE BQF-CODE TO BQ-CODE
                   END-IF
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER MOVE EXC-KEY TO SAVE-KEY
                      PERFORM NEXT-BANQF
                      MOVE BQF-CODE TO BQ-CODE
           END-EVALUATE.
           IF BQF-KEY = SPACES MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0
              MOVE BQF-CODE TO PARMOD-PATH
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
      *    PERFORM DIS-HE-04.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-PERSON 
                    PERFORM READ-PERSON THRU READ-EXIT
                    PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM DIS-HE-01.
           PERFORM CAR-RECENTE.
           INITIALIZE HELP-CUMUL DAE-RECORD.
           PERFORM CUMUL-LIVRE.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.
           EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           MOVE 27 TO COL-IDX.
           IF A-N = "A"
              ADD 11 TO COL-IDX
              DISPLAY SPACES LINE 6 POSITION 37 SIZE 1.
           COMPUTE IDX = 80 - COL-IDX.
           MOVE 0 TO SIZ-IDX IDX-1.
           INSPECT PR-NOM TALLYING  SIZ-IDX FOR CHARACTERS BEFORE "  ".
           INSPECT PR-NOM-JF TALLYING IDX-1 FOR CHARACTERS BEFORE "  ".

           IF FR-NOM-JF = SPACES
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX  = COL-IDX + SIZ-IDX + 1
              IF PR-NOM-JF NOT = SPACES
                 COMPUTE IDX = 80 - COL-IDX
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
           ELSE
              IF PR-NOM-JF NOT = SPACES
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX = COL-IDX + SIZ-IDX + 1
           END-IF.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 6 POSITION COL-IDX SIZE IDX LOW.
       DIS-HE-END.
           EXIT.

       READ-BANQUE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD EXC-KEY.
       NEXT-BANQF.
           CALL "6-BANQF" USING LINK-V BQF-RECORD SAVE-KEY.

       CUMUL-LIVRE.
           PERFORM GET-CS VARYING LNK-MOIS
           FROM MOIS-DEBUT BY 1 UNTIL LNK-MOIS > MOIS-FIN.
           COMPUTE HELP-TOTAL = HELP-PA(13) + HELP-PP(13).
           MOVE HELP-PA(13) TO HE-Z6Z2.
           MOVE HE-Z6Z2 TO DAE-TOTAL-PPASS.
           MOVE HELP-PP(13) TO HE-Z6Z2.
           MOVE HE-Z6Z2 TO DAE-TOTAL-PPAT.
qq         IF HELP-ACTIF > 0 
              PERFORM TRANSMET
           END-IF.

       GET-CS.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY     TO CSP-FIRME.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE 9011       TO CSP-CODE.
           MOVE LNK-MOIS   TO CSP-MOIS.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           IF CSP-UNITE NOT = 0
              MOVE 1 TO HELP-ACTIF  
              PERFORM NOTER
           END-IF.

       NOTER.
           INITIALIZE L-RECORD
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           ADD L-CAISSE-MAL-SAL TO HELP-PA(LNK-MOIS) HELP-PA(13) 
           ADD L-CAISSE-PEN-SAL TO HELP-PA(LNK-MOIS) HELP-PA(13).
           ADD L-CAISSE-MAL-PAT TO HELP-PP(LNK-MOIS) HELP-PP(13).
           ADD L-CAISSE-PEN-PAT TO HELP-PP(LNK-MOIS) HELP-PP(13).
           ADD L-CAISSE-ACC-PAT TO HELP-PP(LNK-MOIS) HELP-PP(13).
           MOVE HELP-PA(LNK-MOIS) TO HE-Z6Z2.
           EVALUATE L-MOIS 
               WHEN  1 MOVE HE-Z6Z2 TO DAE-MOIS1-PASS
               WHEN  2 MOVE HE-Z6Z2 TO DAE-MOIS2-PASS
               WHEN  3 MOVE HE-Z6Z2 TO DAE-MOIS3-PASS
               WHEN  4 MOVE HE-Z6Z2 TO DAE-MOIS1-PASS
               WHEN  5 MOVE HE-Z6Z2 TO DAE-MOIS2-PASS
               WHEN  6 MOVE HE-Z6Z2 TO DAE-MOIS3-PASS
               WHEN  7 MOVE HE-Z6Z2 TO DAE-MOIS1-PASS
               WHEN  8 MOVE HE-Z6Z2 TO DAE-MOIS2-PASS
               WHEN  9 MOVE HE-Z6Z2 TO DAE-MOIS3-PASS
               WHEN 10 MOVE HE-Z6Z2 TO DAE-MOIS1-PASS
               WHEN 11 MOVE HE-Z6Z2 TO DAE-MOIS2-PASS
               WHEN 12 MOVE HE-Z6Z2 TO DAE-MOIS3-PASS
           END-EVALUATE.
           MOVE HELP-PP(LNK-MOIS) TO HE-Z6Z2.
           EVALUATE L-MOIS 
               WHEN  1 MOVE HE-Z6Z2 TO DAE-MOIS1-PPAT
               WHEN  2 MOVE HE-Z6Z2 TO DAE-MOIS2-PPAT
               WHEN  3 MOVE HE-Z6Z2 TO DAE-MOIS3-PPAT
               WHEN  4 MOVE HE-Z6Z2 TO DAE-MOIS1-PPAT
               WHEN  5 MOVE HE-Z6Z2 TO DAE-MOIS2-PPAT
               WHEN  6 MOVE HE-Z6Z2 TO DAE-MOIS3-PPAT
               WHEN  7 MOVE HE-Z6Z2 TO DAE-MOIS1-PPAT
               WHEN  8 MOVE HE-Z6Z2 TO DAE-MOIS2-PPAT
               WHEN  9 MOVE HE-Z6Z2 TO DAE-MOIS3-PPAT
               WHEN 10 MOVE HE-Z6Z2 TO DAE-MOIS1-PPAT
               WHEN 11 MOVE HE-Z6Z2 TO DAE-MOIS2-PPAT
               WHEN 12 MOVE HE-Z6Z2 TO DAE-MOIS3-PPAT
           END-EVALUATE.
           MOVE L-MOIS TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           EVALUATE L-MOIS 
               WHEN  1 MOVE LNK-TEXT TO DAE-MOIS1-ALPHA
               WHEN  2 MOVE LNK-TEXT TO DAE-MOIS2-ALPHA
               WHEN  3 MOVE LNK-TEXT TO DAE-MOIS3-ALPHA
               WHEN  4 MOVE LNK-TEXT TO DAE-MOIS1-ALPHA
               WHEN  5 MOVE LNK-TEXT TO DAE-MOIS2-ALPHA
               WHEN  6 MOVE LNK-TEXT TO DAE-MOIS3-ALPHA
               WHEN  7 MOVE LNK-TEXT TO DAE-MOIS1-ALPHA
               WHEN  8 MOVE LNK-TEXT TO DAE-MOIS2-ALPHA
               WHEN  9 MOVE LNK-TEXT TO DAE-MOIS3-ALPHA
               WHEN 10 MOVE LNK-TEXT TO DAE-MOIS1-ALPHA
               WHEN 11 MOVE LNK-TEXT TO DAE-MOIS2-ALPHA
               WHEN 12 MOVE LNK-TEXT TO DAE-MOIS3-ALPHA
           END-EVALUATE.


       X.
           PERFORM READ-CONTRAT.


       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.


       TRANSMET.
           MOVE 1 TO IDX.
           STRING FR-ETAB-A DELIMITED BY SIZE INTO DAE-ENTREPRISE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-ETAB-N DELIMITED BY SIZE INTO DAE-ENTREPRISE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-SNOCS  DELIMITED BY SIZE INTO DAE-ENTREPRISE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-EXTENS DELIMITED BY SIZE INTO DAE-ENTREPRISE
           WITH POINTER IDX.
           ADD 3 TO IDX.
           STRING FR-NOM DELIMITED BY SIZE INTO DAE-ENTREPRISE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           MOVE 1 TO IDX.
           STRING PR-NAISS-A DELIMITED BY SIZE INTO DAE-NOM 
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-NAISS-M DELIMITED BY SIZE INTO DAE-NOM 
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-NAISS-J DELIMITED BY SIZE INTO DAE-NOM 
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-SNOCS DELIMITED BY SIZE INTO DAE-NOM 
           WITH POINTER IDX.
           ADD 3 TO IDX.
           STRING PR-NOM DELIMITED BY "  " INTO DAE-NOM 
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-PRENOM DELIMITED BY SIZE INTO DAE-NOM 
           WITH POINTER IDX.

           MOVE TXA-VALUE(FR-ACCIDENT) TO HE-Z2Z2.
           MOVE HE-Z2Z2 TO DAE-TAUX-ACC.
           MOVE FR-LOCALITE TO DAE-LIEU-DATE.

           MOVE HELP-TOTAL TO HE-Z6Z2 TOTAL-VAL.
           CALL "0-ECRIT" USING LINK-ECRIT.
           MOVE TOTAL-CHIFFRE TO DAE-TOUTLETT.
           MOVE HE-Z6Z2 TO DAE-MONTANT.

           MOVE  0 TO LNK-VAL.
           CALL "DAEL" USING LINK-V DAE-RECORD.
           ADD 1 TO COUNTER.

       AFFICHAGE-ECRAN.
           MOVE 1301 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           DISPLAY SPACES LINE 15 POSITION 1 SIZE 80.

           DISPLAY "CS 9011" LINE 4 POSITION 70 LOW.
           DISPLAY "TRIMESTER" LINE 10 POSITION 5 LOW.
           DISPLAY TRIMESTER LINE 10 POSITION 17.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
           
       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "DAEL" USING LINK-V DAE-RECORD.
           CANCEL "DAEL".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


