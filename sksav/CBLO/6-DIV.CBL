      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-DIV MODULE GENERAL LECTURE DIVISION       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-DIV.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "DIVISION.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "DIVISION.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "DIVISION.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON DIVIS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-DIV.
       
           IF NOT-OPEN = 0
              OPEN I-O DIVIS
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO DIV-RECORD.
           MOVE FR-KEY TO DIV-FIRME.
           MOVE 0 TO LNK-VAL.

           IF EXC-KEY = 99
              IF LNK-SQL = "Y" 
                 CALL "9-DIVIS" USING LINK-V DIV-RECORD EXC-KEY
              END-IF
              WRITE DIV-RECORD INVALID REWRITE DIV-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ DIVIS PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ DIVIS NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ DIVIS NO LOCK INVALID INITIALIZE DIV-REC-DET
                      END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY NOT = DIV-FIRME 
              GO EXIT-1
           END-IF.
           MOVE DIV-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START DIVIS KEY < DIV-KEY INVALID GO EXIT-1.
       START-2.
           START DIVIS KEY > DIV-KEY INVALID GO EXIT-1.


               