      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-RECAL RECALCUL SALAIRES                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-RECAL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 12.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CCOL.REC".
           COPY "CARRIERE.REC".
           COPY "FICHE.REC".
           COPY "LIVRE.REC".
           COPY "LIVRE.CUM".
           COPY "V-BASES.REC".
           COPY "PAREX.REC".
           COPY "HORSEM.REC".
           COPY "POSE.REC".

       01  SAL-ACT      PIC 9(6)V99.
       01  HRS-MOIS     PIC 9(3)V99.

       01  LAST-FIRME            PIC 9(6) VALUE 0.
       01  LAST-MOIS             PIC 9(2) VALUE 0.
       01  LAST-ANNEE            PIC 9(4) VALUE 0.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".

       PROCEDURE DIVISION USING LINK-V REG-RECORD PRESENCES.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-RECAL .
       
           CALL "6-PARAM" USING LINK-V PAREX-RECORD.
           PERFORM HISTORIQUE.
           PERFORM CALCUL.

       CALCUL.     
           INITIALIZE L-RECORD.
           MOVE 13 TO SAVE-KEY.
           PERFORM NXLP.
           INITIALIZE L-REC-DETAIL.
           CALL "4-CSADD" USING LINK-V REG-RECORD L-RECORD.
           CANCEL "4-CSADD".
           IF CAR-HOR-MEN = 1
           AND LNK-SUITE = 0
              PERFORM SAL-MOIS.
           CALL "4-CALCUL" USING LINK-V 
                                 REG-RECORD
                                 CAR-RECORD
                                 FICHE-RECORD
                                 CCOL-RECORD
                                 PRESENCES 
                                 L-RECORD
                                 LCUM-RECORD.
           IF LNK-YN = "E"
              EXIT PROGRAM.
           MOVE 99 TO SAVE-KEY.
           MOVE CAR-STATUT   TO L-STATUT.
           MOVE CAR-HOR-MEN  TO L-HOR-MEN. 
           MOVE CAR-STATEC   TO L-STATEC.
           MOVE CAR-REGIME   TO L-REGIME.
<          MOVE CAR-COUT     TO L-COUT.
           MOVE REG-SEXE     TO L-CODE-SEXE.
           COMPUTE SH-00 = CCOL-DUREE-JOUR * 21,667 - 1
           IF CAR-HRS-MOIS < SH-00
              MOVE 1 TO L-TEMPS-PARTIEL
           END-IF.
           MOVE FICHE-CLASSE TO L-CLASSE-I.
           MOVE FICHE-TAUX   TO L-TAUX.
           MOVE BAS-HORAIRE  TO L-SAL-HOR.
           MOVE BAS-PRORATA  TO L-SAL-HOR-P.
           MOVE BAS-MOIS     TO L-SAL-MOIS.
           MOVE BAS-CONGE-MOYENNE   TO L-SAL-HOR-CONGE.
<          MOVE BAS-MALADIE-MOYENNE TO L-SAL-MOY-MAL.

           PERFORM NXLP.
           IF LNK-SUITE > 0
              CALL "4-CSPMAL" USING LINK-V REG-RECORD L-RECORD
              MOVE 0 TO LNK-SUITE
              GO CALCUL
           ELSE
              CALL "5-J" USING LINK-V REG-RECORD L-RECORD
              IF FR-CCM = "X"
                CALL "5-CCM" USING LINK-V REG-RECORD L-RECORD CAR-RECORD
              END-IF
           END-IF.
           EXIT PROGRAM.

       NXLP.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD SAVE-KEY.

       HISTORIQUE.
           INITIALIZE CAR-RECORD FICHE-RECORD LCUM-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD NUL-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           CALL "4-LPCUM" USING LINK-V 
                                REG-RECORD
                                LCUM-RECORD.
           CALL "4-SALBAS" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 POSE-RECORD 
                                 HJS-RECORD 
                                 BASES-REMUNERATION.
           CALL "4-SALMOY" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 HJS-RECORD 
                                 BASES-REMUNERATION
                                 LCUM-RECORD.

        
       SAL-MOIS.
           MOVE L-UNIT(100) TO HRS-MOIS.
           MOVE 0 TO IDX-2.
           PERFORM OCCUP VARYING IDX-1 FROM 11 BY 1 UNTIL IDX-1 > 50.
           MOVE 100 TO IDX-3.
           IF IDX-2 > 0
              MOVE L-DEB(100) TO SH-00
              IF L-UNIT(100) = 0
                 MOVE L-DEB(100) TO L-DEB(IDX-2)
                 MOVE 0 TO L-DEB(100) 
                 MOVE IDX-2 TO IDX-3
                 SUBTRACT 1 FROM IDX-2
              END-IF
              PERFORM OCCUP-1 VARYING IDX-1 FROM 11 BY 1 UNTIL IDX-1 
                 > IDX-2
           END-IF.

       OCCUP.
           IF PAREX-OCC(IDX-1) > 0
           AND L-DEB(IDX-1) = 0 
           AND L-UNIT(IDX-1) > 0 
              ADD L-UNIT(IDX-1) TO HRS-MOIS
              MOVE IDX-1 TO IDX-2
           END-IF.

       OCCUP-1.
           IF PAREX-OCC(IDX-1) > 0
           AND L-DEB(IDX-1) = 0 
              COMPUTE SAL-ACT = SH-00 / HRS-MOIS * L-UNIT(IDX-1) + ,005
              MOVE SAL-ACT TO L-DEB(IDX-1)
              SUBTRACT SAL-ACT FROM L-DEB(IDX-3)
           END-IF.
                  