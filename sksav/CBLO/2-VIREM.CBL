      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-VIREM CONTROLE + MODIFICATION VIREMENTS   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  2-VIREM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "VIREMENT.FC".
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "VIREMENT.FDE".
           COPY "FORM80.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  ESPACES               PIC 99 VALUE 36.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "V-VAR.CPY".
           COPY "BANQUE.REC".
           COPY "BANQF.REC".
           COPY "BANQP.REC".
           COPY "IMPRLOG.REC".
           COPY "SYNDICAT.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".

       01  VIR-IDX               PIC 99 COMP-1.
       01  VIR-CUMUL             PIC 9(8)V99 COMP-3.
       01  HELP-1                PIC 9(6).
       01  SUITE-IDX             PIC 99 VALUE 2.
       01  POINT-IDX             PIC 99.
       01  IBAN                  PIC X(40).
       01  IBAN-1 REDEFINES IBAN.
           02 IBAN-TEST          PIC X.
           02 IBAN-REST          PIC X(39).
       01  IBAN-2 REDEFINES IBAN.
           02 IBAN-BLOCK         PIC XXXX OCCURS 8.

       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIR          PIC 999.

       01  FORM-NAME             PIC X(8) VALUE "FORM.VIR".

       01  ECR-DISPLAY.
            02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
            02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
            02 HE-Z8 PIC Z(7),ZZ BLANK WHEN ZERO.
            02 HE-Z6Z2R REDEFINES HE-Z8.
               03 HE-Z6A2 PIC Z(7).
               03 HE-Z6B2 PIC ZZZ.
            02 HE-DATE .
               03 HE-JJ PIC ZZ.
               03 FILLER PIC X VALUE ".".
               03 HE-MM PIC ZZ.
               03 FILLER PIC X VALUE ".".
               03 HE-AA PIC 99.

       01 HE-VIR.
          02 H-V OCCURS 40.
             03 HE-VIR-BANQF              PIC X(10).
             03 HE-VIR-BANQ               PIC X(10).
             03 HE-VIR-TYPE               PIC 9.
             03 HE-VIR-SUITE              PIC 99.
             03 HE-VIR-COMPTE             PIC X(25).
             03 HE-VIR-LIBELLE            PIC X(50).
             03 HE-VIR-BENEFIC.
                04  HE-VIR-BENEFIC-NOM    PIC X(40).
                04  HE-VIR-BENEFIC-RUE    PIC X(40).
                04  HE-VIR-BENEFIC-LOC    PIC X(40).
             03 HE-VIR-BENEFIC-R REDEFINES HE-VIR-BENEFIC.
                04  HE-VIR-BENEFIC-IDX    PIC X(40) OCCURS 3.
             03 HE-VIR-TOTAL              PIC 9(7)V99 COMP-3.
             03 HE-VIR-VENTIL             PIC 9(6) COMP-6.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                VIREMENT
                FORM.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-VIREM .

           CALL "0-TODAY" USING TODAY.
       
           MOVE LNK-SUFFIX TO ANNEE-VIR.
           OPEN I-O   VIREMENT.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 


           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.


      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN OTHER MOVE 0076002007 TO EXC-KFR (1)
                      MOVE 1700080000 TO EXC-KFR (2).
           IF INDICE-ZONE = CHOIX-MAX
                      MOVE 0052000000 TO EXC-KFR (11).


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  OTHER PERFORM AVANT-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  OTHER PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           MOVE FR-KEY TO REG-FIRME.
           IF LNK-PERSON NOT = 0 
               MOVE LNK-PERSON TO REG-PERSON 
               MOVE 13 TO EXC-KEY
               MOVE 0 TO LNK-PERSON
           ELSE    
           ACCEPT REG-PERSON 
             LINE  3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 
                PERFORM CHANGE-MOIS
                MOVE 27 TO EXC-KEY
           END-EVALUATE.

       AVANT-2.            
           ACCEPT REG-MATCHCODE
             LINE  3 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PAGE.
           MOVE 0000000000 TO EXC-KFR(1) EXC-KFR(2) 
           MOVE 0000680000 TO EXC-KFR(14) 
           PERFORM DISPLAY-F-KEYS
           ACCEPT ACTION 
             LINE  24 POSITION 70 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
           COMPUTE VIR-IDX = INDICE-ZONE - 2.
           COMPUTE LIN-IDX = VIR-IDX + 6.
           MOVE HE-VIR-SUITE(VIR-IDX) TO SUITE-IDX.
           PERFORM DIS-HE-ALL.
           ACCEPT SUITE-IDX
             LINE  LIN-IDX POSITION 4 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "REVERSE"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE HE-VIR-SUITE(VIR-IDX) TO HE-Z2.
           DISPLAY HE-Z2  LINE  LIN-IDX POSITION 4.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE FR-KEY TO REG-FIRME
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-HE-01 THRU DIS-HE-02
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE
              IF PRES-TOT(LNK-MOIS) = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           PERFORM DIS-HE-01 THRU DIS-HE-02.
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-NUM LNK-PERSON
           ELSE
              IF REG-PERSON NOT = 0
              AND EXC-KEY NOT = 53
                 PERFORM TOTAL-VIREMENT  THRU TOTAL-VIREMENT-END.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   PERFORM DIS-HE-01 THRU DIS-HE-02
                   MOVE 6 TO LIN-IDX
                   PERFORM TOTAL-VIREMENT  THRU TOTAL-VIREMENT-END
           END-EVALUATE.                     
           
       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-END.

       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS THRU SUBTRACT-END
             WHEN 58 PERFORM ADD-MOIS THRU ADD-END
           END-EVALUATE.
           PERFORM AFFICHAGE-ECRAN.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-DETAIL.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0 
           OR REG-PERSON = 0
              GO SUBTRACT-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO SUBTRACT-END.
           GO SUBTRACT-MOIS.
       SUBTRACT-END.
           IF LNK-MOIS = 0
              IF REG-PERSON = 0
                 MOVE 1 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.
       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
           OR REG-PERSON = 0
              GO ADD-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO ADD-END.
           GO ADD-MOIS.
       ADD-END.
           IF LNK-MOIS > 12
              IF REG-PERSON = 0
                 MOVE 12 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.
           PERFORM DIS-HE-01.
           IF REG-PERSON > 0
              PERFORM TOTAL-VIREMENT  THRU TOTAL-VIREMENT-END.
           
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           MOVE FR-KEY TO VIR-FIRME.
           MOVE REG-PERSON TO VIR-PERSON.
           MOVE LNK-MOIS  TO VIR-MOIS.
           MOVE HE-VIR-TYPE(VIR-IDX)  TO VIR-TYPE.
           MOVE HE-VIR-SUITE(VIR-IDX) TO VIR-SUITE.
           EVALUATE EXC-KEY 
            WHEN  2 READ VIREMENT INVALID CONTINUE
                    NOT INVALID
                    IF VIR-ANNEE-SS > 0
                       INITIALIZE VIR-DATE-SUSPENSION
                    ELSE 
                       MOVE TODAY-ANNEE TO VIR-ANNEE-SS
                       MOVE TODAY-MOIS  TO VIR-MOIS-SS
                       MOVE TODAY-JOUR  TO VIR-JOUR-SS
                    END-IF
                    IF LNK-SQL = "Y" 
                       CALL "9-VIREM" USING LINK-V VIR-RECORD WR-KEY 
                    END-IF
                    IF LNK-LOG = "Y" 
                       CALL "6-AVIR" USING LINK-V VIR-RECORD WR-KEY 
                    END-IF
                    REWRITE VIR-RECORD INVALID CONTINUE END-REWRITE   
                    END-READ
                    PERFORM TOTAL-VIREMENT THRU TOTAL-VIREMENT-END
            WHEN  5 READ VIREMENT INVALID CONTINUE
                    NOT INVALID
                    MOVE TODAY-ANNEE TO VIR-ANNEE-E
                    MOVE TODAY-MOIS  TO VIR-MOIS-E
                    MOVE TODAY-JOUR  TO VIR-JOUR-E
                    IF LNK-SQL = "Y" 
                       CALL "9-VIREM" USING LINK-V VIR-RECORD WR-KEY 
                    END-IF
                    IF LNK-LOG = "Y" 
                       CALL "6-AVIR" USING LINK-V VIR-RECORD WR-KEY 
                    END-IF
                    REWRITE VIR-RECORD INVALID CONTINUE 
                    NOT INVALID PERFORM FULL-PROCESS END-REWRITE   
                    END-READ
                    PERFORM TOTAL-VIREMENT THRU TOTAL-VIREMENT-END
            WHEN  4 READ VIREMENT INVALID CONTINUE
                    NOT INVALID
                        INITIALIZE VIR-DATE-EDITION VIR-DATE-VIREMENT 
                        WRITE VIR-RECORD INVALID REWRITE VIR-RECORD
                        END-WRITE   
                    END-READ
                    MOVE 2 TO DECISION
                    PERFORM TOTAL-VIREMENT THRU TOTAL-VIREMENT-END
            WHEN  8 MOVE 2 TO DECISION
                    IF LNK-SQL = "Y" 
                       CALL "9-VIREM" USING LINK-V VIR-RECORD DEL-KEY 
                    END-IF
                    IF LNK-LOG = "Y" 
                       CALL "6-AVIR" USING LINK-V VIR-RECORD DEL-KEY 
                    END-IF
                    DELETE VIREMENT INVALID CONTINUE END-DELETE
                    PERFORM TOTAL-VIREMENT THRU TOTAL-VIREMENT-END
             END-EVALUATE.


      *    HISTORIQUE DES VIREMENTS
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       TOTAL-VIREMENT.
           MOVE 0 to VIR-CUMUL NOT-FOUND.
           MOVE 6 TO LIN-IDX.
           INITIALIZE VIR-RECORD VIR-IDX.
           MOVE REG-FIRME  TO VIR-FIRME-N.
           MOVE REG-PERSON TO VIR-PERSON-N.
           MOVE LNK-MOIS  TO VIR-MOIS-N.
           START VIREMENT KEY >= VIR-KEY-PERSON INVALID 
                MOVE 1 TO NOT-FOUND.
           PERFORM READ-VIREM THRU READ-VIR-END.
           COMPUTE CHOIX-MAX = 2 + VIR-IDX.
       TOTAL-VIREMENT-END.

       READ-VIREM.
           IF NOT-FOUND = 1
              GO READ-VIR-END.
           READ VIREMENT NEXT AT END 
              GO READ-VIR-END.
           IF FR-KEY     NOT = VIR-FIRME
           OR REG-PERSON NOT = VIR-PERSON
           OR LNK-MOIS   NOT = VIR-MOIS 
              GO READ-VIR-END.
           ADD 1 TO VIR-IDX.
           ADD VIR-A-PAYER TO VIR-CUMUL.
           PERFORM READ-BANQP.
           PERFORM DIS-HIS-LIGNE.
           IF LIN-IDX > 21
              ADD 1 TO INDICE-ZONE
              PERFORM AVANT-PAGE
              IF EXC-KEY NOT = 68
                 GO READ-VIR-END
              ELSE
                 INITIALIZE HE-VIR VIR-IDX
                 PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM 7
                                   BY 1 UNTIL LIN-IDX > 24
                 MOVE 6 TO LIN-IDX
                 MOVE 0 TO INDICE-ZONE
              END-IF
           END-IF.
           GO READ-VIREM.
       READ-VIR-END.
           ADD 1 TO LIN-IDX .
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 24.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       READ-BANQP.
           INITIALIZE BQP-RECORD.
           MOVE FR-KEY TO BQP-FIRME.
           MOVE REG-PERSON TO BQP-PERSON.
           MOVE VIR-TYPE  TO BQP-TYPE.
           MOVE VIR-SUITE TO BQP-SUITE.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD FAKE-KEY.
           MOVE VIR-BANQUE-D TO HE-VIR-BANQF(VIR-IDX).
           MOVE VIR-BANQUE-C TO HE-VIR-BANQ(VIR-IDX).
           MOVE VIR-TYPE    TO HE-VIR-TYPE(VIR-IDX).
           MOVE VIR-SUITE   TO HE-VIR-SUITE(VIR-IDX).
           MOVE BQP-LIBELLE TO HE-VIR-LIBELLE(VIR-IDX).
           IF VIR-TYPE < 9
              MOVE BQP-COMPTE  TO HE-VIR-COMPTE(VIR-IDX)
              MOVE BQP-BENEFIC-NOM TO HE-VIR-BENEFIC-NOM(VIR-IDX)
              MOVE BQP-BENEFIC-RUE TO HE-VIR-BENEFIC-RUE(VIR-IDX)
              MOVE BQP-BENEFIC-LOC TO HE-VIR-BENEFIC-LOC(VIR-IDX)
           ELSE
              MOVE VIR-COMPTE-C TO HE-VIR-COMPTE(VIR-IDX).
           MOVE VIR-A-PAYER TO HE-VIR-TOTAL(VIR-IDX).


       DIS-HIS-LIGNE.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES  LINE  LIN-IDX POSITION  1 SIZE 80.
           MOVE VIR-TYPE  TO HE-Z2.
           DISPLAY HE-Z2  LINE  LIN-IDX POSITION  1.
           MOVE VIR-SUITE TO HE-Z2.
           DISPLAY HE-Z2  LINE  LIN-IDX POSITION  4.
           MOVE VIR-MOIS  TO HE-Z2.
           DISPLAY HE-Z2  LINE  LIN-IDX POSITION  8.
           MOVE VIR-JOUR-E  TO HE-JJ.
           MOVE VIR-MOIS-E  TO HE-MM.
           MOVE VIR-ANNEE-E TO HE-AA.
           IF HE-AA > 0
              DISPLAY HE-DATE LINE LIN-IDX POSITION 13
           ELSE
              DISPLAY SPACES LINE  LIN-IDX POSITION 13 SIZE 8.
           MOVE VIR-JOUR-V TO HE-JJ.
           MOVE VIR-MOIS-V TO HE-MM.
           MOVE VIR-ANNEE-V TO HE-AA.
           IF HE-AA > 0
              DISPLAY HE-DATE LINE LIN-IDX POSITION 23
           ELSE
              DISPLAY SPACES LINE  LIN-IDX POSITION 23 SIZE 8.
           MOVE VIR-JOUR-SS TO HE-JJ.
           MOVE VIR-MOIS-SS TO HE-MM.
           MOVE VIR-ANNEE-SS TO HE-AA.
           IF HE-AA > 0
              DISPLAY HE-DATE LINE LIN-IDX POSITION 32
           ELSE
              DISPLAY SPACES LINE  LIN-IDX POSITION 32 SIZE 8.
           DISPLAY VIR-BANQUE-D  LINE  LIN-IDX POSITION 41.
           DISPLAY VIR-BANQUE-C  LINE  LIN-IDX POSITION 51.
           MOVE VIR-A-PAYER TO HE-Z8.
           IF MENU-PROG-NUMBER > 0
              PERFORM LUF.
           IF HE-Z6B2 = ",00"
              DISPLAY HE-Z6A2 LINE LIN-IDX POSITION 61
           ELSE
              DISPLAY HE-Z8   LINE LIN-IDX POSITION 61.
           MOVE VIR-CUMUL   TO HE-Z8.
           IF MENU-PROG-NUMBER > 0
              PERFORM LUF.
           IF HE-Z6B2 = ",00"
              DISPLAY HE-Z6A2 LINE LIN-IDX POSITION 71 LOW
           ELSE
              DISPLAY HE-Z8   LINE LIN-IDX POSITION 71 LOW.

       DIS-HE-ALL.
           DISPLAY HE-VIR-BANQ(VIR-IDX)        LINE 23 POSITION  2.
           DISPLAY HE-VIR-COMPTE(VIR-IDX)      LINE 23 POSITION 12.
           DISPLAY HE-VIR-LIBELLE(VIR-IDX) LINE 23 POSITION 40 SIZE 40.
           DISPLAY HE-VIR-BENEFIC-NOM(VIR-IDX) LINE 24 POSITION  2.
      *    DISPLAY HE-VIR-BENEFIC-LOC(VIR-IDX) LINE 24 POSITION 39.
           DISPLAY VIR-USER LINE 24 POSITION 39
           DISPLAY VIR-ST-JOUR  LINE 24 POSITION 49.
           DISPLAY VIR-ST-MOIS  LINE 24 POSITION 52.
           DISPLAY VIR-ST-ANNEE LINE 24 POSITION 55.
           DISPLAY VIR-ST-HEURE LINE 24 POSITION 60.
           DISPLAY VIR-ST-MIN   LINE 24 POSITION 63.

       FULL-PROCESS.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
           END-IF.
           PERFORM READ-FORM.
           MOVE HE-VIR-BANQF(VIR-IDX) TO VIR-BANQUE-D.
           PERFORM READ-BANQ.
           PERFORM READ-BANQP.
           MOVE 0 TO ESPACES.
           PERFORM FILL-FILES.
           MOVE 36 TO ESPACES.
           PERFORM FILL-FILES.
           MOVE 70 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE 70 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           PERFORM TRANSMET.
           MOVE 0 TO COUNTER.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.

       READ-BANQ.
           INITIALIZE BQF-RECORD.
           MOVE FR-KEY TO BQF-FIRME.
           MOVE VIR-BANQUE-D TO BQF-CODE.
           CALL "6-BANQF" USING LINK-V BQF-RECORD FAKE-KEY.

       FILL-FILES.
           COMPUTE LIN-NUM = ESPACES + 22.
           MOVE 3 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 23.
           MOVE 3  TO COL-NUM.
           MOVE FR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 24.
           MOVE  3 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  6 TO CAR-NUM.
           MOVE FR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           ADD  1  TO COL-NUM.
           MOVE FR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 9.
           MOVE  9 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 60 TO COL-NUM.
           MOVE  1 TO POINTS.
           COMPUTE VH-00 = VIR-A-PAYER.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 21.
           MOVE  3 TO COL-NUM.
           MOVE BQF-CODE  TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE BQF-COMPTE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.



           COMPUTE LIN-NUM = ESPACES + 18.
           MOVE  3 TO COL-NUM.
           MOVE BQP-LIBELLE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           PERFORM MOIS-NOM.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE LNK-ANNEE TO HE-Z4.
           MOVE HE-Z4 TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           PERFORM FILL-FORM.
           IF BQP-BENEFIC = SPACES
              IF BQP-TYPE NOT = 9
                 PERFORM FILL-BENEFIC
              ELSE
                 PERFORM FILL-SYNDIC
                 IF BQP-COMPTE = SPACES
                    MOVE SYN-COMPTE TO BQP-COMPTE
                 END-IF
              END-IF
           END-IF.

           COMPUTE LIN-NUM = ESPACES + 9.
           MOVE  3 TO COL-NUM.
           MOVE VIR-BANQUE-C TO BQ-CODE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD FAKE-KEY.
           MOVE BQ-SWIFT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           ADD 1 TO LIN-NUM
           MOVE 3 TO COL-NUM.
           MOVE BQP-COMPTE TO ALPHA-TEXTE IBAN.
           IF IBAN-TEST NUMERIC
              PERFORM FILL-FORM
           ELSE 
              PERFORM IBAN.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 13.
           MOVE  3 TO COL-NUM.
           MOVE BQP-BENEFIC-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = ESPACES + 14.
           MOVE  3 TO COL-NUM.
           MOVE BQP-BENEFIC-RUE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = ESPACES + 15.
           MOVE  3 TO COL-NUM.
           MOVE BQP-BENEFIC-LOC TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 20.
           MOVE 57 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 63 TO COL-NUM.
           MOVE TODAY-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

       IBAN. 
           MOVE "IBAN" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           PERFORM BLOCS VARYING IDX FROM 1 BY 1 UNTIL IDX > 8.

       BLOCS.
           MOVE IBAN-BLOCK(IDX) TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

       FILL-SYNDIC.
           MOVE BQP-SYNDICAT TO SYN-KEY.
           CALL "6-SYNDIC" USING LINK-V SYN-RECORD FAKE-KEY.
           MOVE SYN-NOM TO BQP-BENEFIC-NOM.
           MOVE 1 TO POINT-IDX.
           STRING SYN-MAISON DELIMITED BY "  "
           INTO BQP-BENEFIC-RUE WITH POINTER POINT-IDX.
           ADD 1 TO POINT-IDX.
           STRING SYN-RUE DELIMITED BY "  "
              INTO BQP-BENEFIC-RUE WITH POINTER POINT-IDX.

           MOVE 1 TO POINT-IDX.
           STRING SYN-PAYS DELIMITED BY "  "
              INTO BQP-BENEFIC-LOC WITH POINTER POINT-IDX.
           ADD 1 TO POINT-IDX.
           MOVE SYN-CODE-POST TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE
           INTO BQP-BENEFIC-LOC WITH POINTER POINT-IDX.
           ADD 1 TO POINT-IDX.
           STRING SYN-LOCALITE DELIMITED BY SIZE
           INTO BQP-BENEFIC-LOC WITH POINTER POINT-IDX.

       FILL-BENEFIC.
           MOVE 1 TO POINT-IDX.
           STRING PR-NOM DELIMITED BY "  "
           INTO BQP-BENEFIC-NOM WITH POINTER POINT-IDX
           ADD 1 TO POINT-IDX.
           STRING PR-PRENOM DELIMITED BY "  "
              INTO BQP-BENEFIC-NOM WITH POINTER POINT-IDX.
           MOVE 1 TO POINT-IDX.
           STRING PR-MAISON DELIMITED BY "  "
           INTO BQP-BENEFIC-RUE WITH POINTER POINT-IDX.
           ADD 1 TO POINT-IDX.
           STRING PR-RUE DELIMITED BY "  "
              INTO BQP-BENEFIC-RUE WITH POINTER POINT-IDX.

           MOVE 1 TO POINT-IDX.
           STRING PR-PAYS DELIMITED BY "  "
              INTO BQP-BENEFIC-LOC WITH POINTER POINT-IDX.
           ADD 1 TO POINT-IDX.
           MOVE PR-CODE-POST TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE
           INTO BQP-BENEFIC-LOC WITH POINTER POINT-IDX.
           ADD 1 TO POINT-IDX.
           STRING PR-LOCALITE DELIMITED BY SIZE
           INTO BQP-BENEFIC-LOC WITH POINTER POINT-IDX.

        LUF.
           MOVE HE-Z8 TO SH-00.
           COMPUTE HELP-1 = SH-00 * 40,3399 + ,5.
           MOVE HELP-1 TO HE-Z8. 

       AFFICHAGE-ECRAN.
           MOVE 2521 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           MOVE 99 TO LNK-VAL
           CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".

       END-PROGRAM.
           CLOSE VIREMENT.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".


