      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-FORMUL FORMULES CODES SALAIRES            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-FORMUL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  SAL-ACT              PIC S9(8)V9(5).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02  SAL-INT          PIC S9(8).
           02  SAL-DEC          PIC V9(5).
       01  SH-00                PIC S9(8)V9(5).
       01  IDX-1                PIC 9(4).
       01  ARRONDI              PIC 9.
       01  ACTION               PIC 9.
       01  ESC-KEY              PIC 9(4) COMP-1.

       01  H-Z.
           03 ZZ-05          PIC Z(8),Z(5).
       01  H-Z0 REDEFINES H-Z.
           03 ZZ-00          PIC Z(8).
           03 FILLER         PIC Z(6).
       01  H-Z1 REDEFINES H-Z.
           03 ZZ-01          PIC Z(8),Z.
           03 FILLER         PIC Z(4).
       01  H-Z2 REDEFINES H-Z.
           03 ZZ-02          PIC Z(8),ZZ.
           03 FILLER         PIC ZZZ.
       01  H-Z3 REDEFINES H-Z.
           03 ZZ-03          PIC Z(8),ZZZ.
           03 FILLER         PIC ZZ.
       01  H-Z4 REDEFINES H-Z.
           03 ZZ-04          PIC Z(8),ZZZZ.
           03 FILLER         PIC Z.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "CODPAIE.REC".
           COPY "CODSAL.REC".

       PROCEDURE DIVISION USING LINK-V
                                CS-RECORD
                                CSP-RECORD.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-FORMUL.
       
       BASES-SALAIRE.
             MOVE 0 TO ARRONDI SH-00 SAL-ACT.


       FORMULES. 
           EVALUATE CS-FORMULE(LNK-NUM)
             WHEN 1 COMPUTE SAL-ACT = CSP-UNITAIRE * CSP-UNITE
             WHEN 2 COMPUTE SAL-ACT = CSP-UNITAIRE * CSP-UNITE / 100
             WHEN 3 COMPUTE SAL-ACT = CSP-UNITAIRE * CSP-UNITE 
                                    * CSP-POURCENT 
             WHEN 4 COMPUTE SAL-ACT = CSP-UNITAIRE * CSP-UNITE 
                                    * CSP-POURCENT / 100   
             WHEN 5 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-POURCENT
             WHEN 6 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-POURCENT / 100
             WHEN 7 COMPUTE SAL-ACT = CSP-DONNEE-1 * MOIS-IDX(LNK-MOIS)
             WHEN 8 COMPUTE SAL-ACT = CSP-DONNEE-2 * MOIS-IDX(LNK-MOIS)
             WHEN 9 COMPUTE SAL-ACT = CSP-POURCENT * MOIS-IDX(LNK-MOIS)
             WHEN 10 COMPUTE SAL-ACT = CSP-UNITAIRE * CSP-UNITE
                     IF SAL-ACT = 0
                        MOVE CSP-TOTAL TO SAL-ACT 
                     END-IF
             WHEN 11 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-UNITAIRE
             WHEN 12 COMPUTE SAL-ACT = CSP-DONNEE-2 * CSP-UNITAIRE
             WHEN 13 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-DONNEE-2
             WHEN 14 COMPUTE SAL-ACT = CSP-DONNEE-1 / CSP-DONNEE-2
             WHEN 15 COMPUTE SAL-ACT = CSP-DONNEE-1 + CSP-UNITE
             WHEN 16 COMPUTE SAL-ACT = CSP-DONNEE-2 + CSP-UNITE
             WHEN 17 COMPUTE SAL-ACT = CSP-DONNEE-1 - CSP-UNITE
             WHEN 18 COMPUTE SAL-ACT = CSP-DONNEE-2 - CSP-UNITE
             WHEN 19 COMPUTE SAL-ACT = CSP-UNITE + CSP-UNITAIRE 
                     MOVE 1 TO ARRONDI
             WHEN 20 COMPUTE SAL-ACT = CSP-UNITE - CSP-UNITAIRE 
                     MOVE 1 TO ARRONDI

             WHEN 21 COMPUTE SAL-ACT = CSP-DONNEE-1 + CSP-DONNEE-2
                     MOVE 1 TO ARRONDI
             WHEN 22 COMPUTE SAL-ACT = CSP-DONNEE-1 - CSP-DONNEE-2
                     MOVE 1 TO ARRONDI
             WHEN 23 COMPUTE SAL-ACT = CSP-DONNEE-2 - CSP-DONNEE-1
                     MOVE 1 TO ARRONDI
             WHEN 24 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-DONNEE-2
             WHEN 25 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-POURCENT
             WHEN 26 COMPUTE SAL-ACT = CSP-DONNEE-2 * CSP-POURCENT
             WHEN 27 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-DONNEE-2
                                     * CSP-POURCENT 
             WHEN 28 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-DONNEE-2
                                     * CSP-POURCENT / 100
             WHEN 29 COMPUTE SAL-ACT = CSP-DONNEE-1 / CSP-DONNEE-2
                     MOVE 1 TO ARRONDI
             WHEN 30 COMPUTE SAL-ACT = CSP-DONNEE-2 / CSP-DONNEE-1
                     MOVE 1 TO ARRONDI
             WHEN 31 COMPUTE SAL-ACT = CSP-DONNEE-1 / CSP-POURCENT
             WHEN 32 COMPUTE SAL-ACT = CSP-DONNEE-2 / CSP-POURCENT
             WHEN 33 COMPUTE SAL-ACT = CSP-DONNEE-2 * CSP-POURCENT / 10
             WHEN 34 COMPUTE SAL-ACT = CSP-DONNEE-2 * CSP-POURCENT / 100
             WHEN 35 COMPUTE SAL-ACT = CSP-DONNEE-2 * CSP-POURCENT / 1000
             WHEN 36 COMPUTE SAL-ACT = CSP-DONNEE-2 * CSP-POURCENT 
                                     / 1000
             WHEN 37 COMPUTE SAL-ACT = CSP-DONNEE-1 / CSP-DONNEE-2
                                     * CSP-POURCENT 
             WHEN 38 COMPUTE SAL-ACT = CSP-DONNEE-2 / CSP-DONNEE-1
                                     * CSP-POURCENT 
             WHEN 39 COMPUTE SAL-ACT = CSP-DONNEE-1 / CSP-DONNEE-2
                                     * CSP-POURCENT / 100
             WHEN 40 COMPUTE SAL-ACT = CSP-DONNEE-2 / CSP-DONNEE-1
                                     * CSP-POURCENT / 100
             WHEN 41 MOVE CSP-UNITAIRE TO SAL-ACT
                     IF SAL-ACT > CSP-DONNEE-1 
                        MOVE CSP-DONNEE-1 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 42 MOVE CSP-UNITAIRE TO SAL-ACT
                     IF SAL-ACT > CSP-DONNEE-2 
                        MOVE CSP-DONNEE-2 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 43 MOVE CSP-UNITAIRE TO SAL-ACT
                     IF SAL-ACT < CSP-DONNEE-1 
                        MOVE CSP-DONNEE-1 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 44 MOVE CSP-UNITAIRE TO SAL-ACT
                     IF SAL-ACT < CSP-DONNEE-2 
                        MOVE CSP-DONNEE-2 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 45 COMPUTE SAL-ACT = CSP-UNITE * CSP-POURCENT / 100   
             WHEN 46 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-DONNEE-2
                                     / CSP-POURCENT 
             WHEN 47 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-DONNEE-2
                                     / CSP-POURCENT / 100

             WHEN 48 COMPUTE SAL-ACT = CSP-DONNEE-1 * MOIS-IDX(LNK-MOIS)
             WHEN 49 COMPUTE SAL-ACT = CSP-DONNEE-2 * MOIS-IDX(LNK-MOIS)
             WHEN 50 COMPUTE SAL-ACT = CSP-POURCENT * MOIS-IDX(LNK-MOIS)
             WHEN 61 COMPUTE SAL-ACT = CSP-DONNEE-1 + CSP-DONNEE-2
                     MOVE 1 TO ARRONDI
             WHEN 62 COMPUTE SAL-ACT = CSP-DONNEE-1 - CSP-DONNEE-2
                     MOVE 1 TO ARRONDI
             WHEN 63 COMPUTE SAL-ACT = CSP-DONNEE-2 - CSP-DONNEE-1
                     MOVE 1 TO ARRONDI
             WHEN 64 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-DONNEE-2
             WHEN 65 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-DONNEE-2
                                     * CSP-POURCENT 
             WHEN 66 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-DONNEE-2
                                     * CSP-POURCENT / 100

             WHEN 67 COMPUTE SAL-ACT = CSP-DONNEE-1 / CSP-DONNEE-2
             WHEN 68 COMPUTE SAL-ACT = CSP-DONNEE-2 / CSP-DONNEE-1

             WHEN 69 COMPUTE SAL-ACT = CSP-DONNEE-1 / CSP-DONNEE-2
                                     * CSP-POURCENT 
             WHEN 70 COMPUTE SAL-ACT = CSP-DONNEE-2 / CSP-DONNEE-1
                                     * CSP-POURCENT 
             WHEN 71 COMPUTE SAL-ACT = CSP-DONNEE-1 / CSP-DONNEE-2
                                     * CSP-POURCENT / 100
             WHEN 72 COMPUTE SAL-ACT = CSP-DONNEE-2 / CSP-DONNEE-1
                                     * CSP-POURCENT / 100

             WHEN 75 MOVE CSP-UNITE TO SAL-ACT
                     IF SAL-ACT > CSP-DONNEE-1 
                        MOVE CSP-DONNEE-1 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 76 MOVE CSP-UNITE TO SAL-ACT
                     IF SAL-ACT > CSP-DONNEE-2 
                        MOVE CSP-DONNEE-2 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 77 MOVE CSP-UNITE TO SAL-ACT
                     IF SAL-ACT < CSP-DONNEE-1 
                        MOVE CSP-DONNEE-1 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 78 MOVE CSP-UNITE TO SAL-ACT
                     IF SAL-ACT < CSP-DONNEE-2 
                        MOVE CSP-DONNEE-2 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 79 COMPUTE SAL-ACT = CSP-TOTAL / CSP-UNITAIRE

             WHEN 81 COMPUTE SAL-ACT = CSP-TOTAL * CSP-POURCENT / 100 
             WHEN 82 COMPUTE SAL-ACT = CSP-TOTAL / CSP-DONNEE-2
             WHEN 83 COMPUTE SAL-ACT = CSP-UNITE * CSP-POURCENT / 100 
             WHEN 84 COMPUTE SAL-ACT = CSP-UNITE / CSP-DONNEE-2
             WHEN 85 COMPUTE SAL-ACT = CSP-UNITAIRE * CSP-POURCENT / 100 
             WHEN 86 COMPUTE SAL-ACT = CSP-UNITAIRE / CSP-DONNEE-2
             WHEN 87 COMPUTE SAL-ACT = CSP-TOTAL * CSP-POURCENT
             WHEN 88 COMPUTE SAL-ACT = CSP-UNITE * CSP-POURCENT
             WHEN 89 COMPUTE SAL-ACT = CSP-UNITAIRE * CSP-POURCENT

             WHEN 90 IF CSP-DONNEE-1 > 0
                        MOVE 0 TO CSP-UNITAIRE  CSP-UNITE CSP-TOTAL
                     END-IF
             WHEN 96 IF CSP-UNITE = 0
                        MOVE 0 TO CSP-UNITAIRE 
                     END-IF
             WHEN 97 IF CSP-UNITAIRE = 0
                        MOVE 0 TO CSP-UNITE CSP-TOTAL
                     END-IF
             WHEN 98 IF CSP-UNITE = 0
                        MOVE 0 TO CSP-UNITAIRE CSP-TOTAL
                     END-IF
             WHEN 99 IF CSP-TOTAL = 0
                        MOVE 0 TO CSP-UNITAIRE  CSP-UNITE
                     END-IF
             WHEN 100 IF CSP-TOTAL = 0
                        MOVE 0 TO CSP-UNITAIRE  
                     END-IF
             WHEN 101 COMPUTE SAL-ACT = CSP-UNITAIRE * CSP-UNITE
             WHEN 102 COMPUTE SAL-ACT = CSP-DONNEE-1 * CSP-UNITAIRE
             WHEN 103 COMPUTE SAL-ACT = CSP-DONNEE-2 * CSP-UNITAIRE
             WHEN 104 COMPUTE SAL-ACT = CSP-TOTAL    * CSP-POURCENT
             WHEN 105 COMPUTE SAL-ACT = CSP-DONNEE-1 - CSP-TOTAL


            WHEN 121 COMPUTE SAL-ACT = CSP-DONNEE-1 * MOIS-IDX(LNK-MOIS)
             WHEN 141 IF CSP-DONNEE-1 = 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 142 IF CSP-DONNEE-2 = 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 143 IF CSP-POURCENT = 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 144 IF CSP-DONNEE-1 > 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 145 IF CSP-DONNEE-2 > 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 146 IF CSP-POURCENT > 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 161 COMPUTE SAL-ACT = (CSP-DONNEE-1 + CSP-UNITE)
                                      * CSP-UNITAIRE

COMET        WHEN 162 COMPUTE SH-00 = CSP-DONNEE-1 - CSP-DONNEE-2
                                    + CSP-UNITE - CSP-UNITAIRE
                                    IF SH-00 < 0
                                       INITIALIZE CSP-VALEURS
                                    ELSE
                                       COMPUTE SAL-ACT = CSP-COMPL-1
                                               * CSP-COMPL-2 / 100
                                       MOVE SAL-ACT TO CSP-UNITAIRE 
                                       COMPUTE SAL-ACT = CSP-UNITAIRE 
                                       * SH-00
                                    END-IF
pr굍et      WHEN 163 COMPUTE SAL-ACT = CSP-UNITAIRE * MOIS-IDX(LNK-MOIS)
             WHEN 164 COMPUTE SAL-ACT = CSP-UNITE / CSP-UNITAIRE
presta       WHEN 165  MOVE CSP-UNITE TO SAL-ACT 
                       MOVE SAL-INT TO CSP-DONNEE(1)
                       MOVE SAL-DEC TO CSP-DONNEE(2)
             WHEN 171 MOVE CSP-TOTAL TO SAL-ACT
                      IF SAL-ACT > CSP-DONNEE-1 
                         MOVE CSP-DONNEE-1 TO SAL-ACT
                      END-IF
             WHEN 181 COMPUTE SAL-ACT = CSP-COMPL-1 * CSP-POURCENT
           END-EVALUATE.
           IF SAL-ACT < 0
              MOVE 0 TO SAL-ACT
           END-IF.

           IF ARRONDI = 0
              PERFORM ARRONDI
           ELSE
              PERFORM REMISE
           END-IF.


       ARRONDI.
           EVALUATE CS-FORMULE(LNK-NUM)
              WHEN   1 THRU  20 MOVE 6 TO IDX-1
              WHEN  21 THRU  60 MOVE 5 TO IDX-1
              WHEN  61 THRU  80 MOVE 4 TO IDX-1
              WHEN  81 THRU  90 MOVE 1 TO IDX-1
              WHEN  91 THRU 100 MOVE 0 TO IDX-1
              WHEN 101 THRU 120 MOVE 7 TO IDX-1
              WHEN 121 THRU 140 MOVE 2 TO IDX-1
              WHEN 141 THRU 160 MOVE 4 TO IDX-1
              WHEN 161 THRU 180 MOVE 6 TO IDX-1
              WHEN 181 THRU 200 MOVE 8 TO IDX-1
           END-EVALUATE.
           IF IDX-1 > 0
           EVALUATE CS-DECIMALES(IDX-1)
              WHEN 0 ADD ,5      TO SAL-ACT
              WHEN 1 ADD ,05     TO SAL-ACT
              WHEN 2 ADD ,005    TO SAL-ACT
              WHEN 3 ADD ,0005   TO SAL-ACT
              WHEN 4 ADD ,00005  TO SAL-ACT
              WHEN 5 ADD ,000005 TO SAL-ACT
           END-EVALUATE.
           MOVE SAL-ACT TO ZZ-04.
           IF IDX-1 > 0
           EVALUATE CS-DECIMALES(IDX-1)
              WHEN 0 MOVE ZZ-00 TO SAL-ACT
              WHEN 1 MOVE ZZ-01 TO SAL-ACT
              WHEN 2 MOVE ZZ-02 TO SAL-ACT
              WHEN 3 MOVE ZZ-03 TO SAL-ACT
              WHEN 4 MOVE ZZ-04 TO SAL-ACT
              WHEN 5 MOVE ZZ-05 TO SAL-ACT
           END-EVALUATE.


           PERFORM REMISE.
           
       REMISE.
           EVALUATE CS-FORMULE(LNK-NUM)
              WHEN   1 THRU  20 MOVE SAL-ACT TO CSP-TOTAL
              WHEN  21 THRU  60 MOVE SAL-ACT TO CSP-UNITAIRE
              WHEN  61 THRU  80 MOVE SAL-ACT TO CSP-UNITE
              WHEN  81 THRU  90 MOVE SAL-ACT TO CSP-DONNEE-1
              WHEN 101 THRU 120 MOVE SAL-ACT TO CSP-COMPL-1
              WHEN 121 THRU 140 MOVE SAL-ACT TO CSP-DONNEE-2
              WHEN 141 THRU 160 MOVE SAL-ACT TO CSP-UNITE
              WHEN 161 THRU 180 MOVE SAL-ACT TO CSP-TOTAL
              WHEN 181 THRU 200 MOVE SAL-ACT TO CSP-COMPL-2
           END-EVALUATE.
           MOVE 0 TO LNK-NUM.
           EXIT PROGRAM.

           COPY "Xaction.CPY".


