      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-MAL  CONTROLE + MODIFICATION MALADIES     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  1-MAL .

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       DATA DIVISION.


       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  PRECISION             PIC 9 VALUE 0.
       01  TYP                   PIC 9 VALUE 1.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "LIVRE.REC".
           COPY "CODPAR.REC".
           COPY "CODPAIE.REC".
           COPY "DECMAL.REC".
           COPY "V-VAR.CPY".
       01  MAL-IDX               PIC 99 COMP-1.
       01  SUITE-IDX             PIC 99 VALUE 2.
       01  INTERMEDIATE          PIC 999V99.
       01  ARROW                 PIC X VALUE ">".

       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z3Z2 PIC ZZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z4Z2 PIC ZZZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZ.

       01 HE-VIR.
          02 H-V OCCURS 15.
             03 HE-LP-SUITE  PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-MAL.
       
           CALL "6-GCP" USING LINK-V CP-RECORD.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.


      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0163640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0129000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN OTHER MOVE 7538391188 TO EXC-KFR (1)
                      MOVE 8777081410 TO EXC-KFR (2)
                      MOVE 0100000000 TO EXC-KFR (3).
           IF INDICE-ZONE = CHOIX-MAX
                      MOVE 0052000000 TO EXC-KFR (11).


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  OTHER PERFORM AVANT-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  OTHER PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           MOVE FR-KEY TO REG-FIRME.
           IF LNK-PERSON NOT = 0
              MOVE LNK-PERSON TO REG-PERSON
              MOVE  0 TO LNK-PERSON
              MOVE 13 TO EXC-KEY
           ELSE
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 
                PERFORM CHANGE-MOIS
             MOVE 27 TO EXC-KEY
           END-EVALUATE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
           COMPUTE MAL-IDX = INDICE-ZONE - 2.
           MOVE 7538391188 TO EXC-KFR (1)
           MOVE 8777621410 TO EXC-KFR (2).
           MOVE 0100000000 TO EXC-KFR (3).
           IF HE-LP-SUITE(MAL-IDX) = 10
              MOVE 0000008900 TO EXC-KFR (1)
              MOVE 8700001400 TO EXC-KFR (2).
           PERFORM DISPLAY-F-KEYS.
           COMPUTE LIN-IDX = MAL-IDX + 6.
           MOVE HE-LP-SUITE(MAL-IDX) TO SUITE-IDX.
           MOVE ">" TO ARROW.
           ACCEPT ARROW
             LINE  LIN-IDX POSITION 9 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "REVERSE"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           DISPLAY SPACES LINE LIN-IDX POSITION 9 SIZE 1.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 MOVE  504014 TO LNK-VAL
                   PERFORM HELP-SCREEN
                   MOVE 1 TO INPUT-ERROR
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN  OTHER PERFORM NEXT-REGIS.
           PERFORM DIS-HE-01.
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "GN" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE.
           PERFORM AFFICHAGE-DETAIL.
           IF INPUT-ERROR = 0 
           AND PRES-TOT(LNK-MOIS) > 0
              AND EXC-KEY NOT = 53
              PERFORM TOTAL-LIVRE THRU TOTAL-LIVRE-END.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              IF  MAL-IDX = 0 
              AND REG-PERSON NOT = 0
                 GO APRES-1 
              END-IF.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
           END-EVALUATE.                     
           PERFORM DIS-HE-01.
           PERFORM TOTAL-LIVRE THRU TOTAL-LIVRE-END.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              IF  MAL-IDX = 0 
              AND REG-PERSON NOT = 0
                 GO APRES-2
              END-IF.
           
       APRES-DEC.
           INITIALIZE L-RECORD.
           MOVE FR-KEY TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON.
           MOVE LNK-MOIS  TO L-MOIS.
           MOVE HE-LP-SUITE(MAL-IDX) TO L-SUITE LNK-SUITE.
           IF  EXC-KEY > 0
           AND EXC-KEY < 11
               MOVE 13 TO SAVE-KEY
               PERFORM NXLP
           END-IF.
           EVALUATE EXC-KEY 
           WHEN  1 THRU 8
                   INITIALIZE L-DATE-EDITION 
                   PERFORM DECMAL
                   IF L-PERSON NOT = 0
                      PERFORM CREATE-FLAG
                      MOVE 99 TO SAVE-KEY
                      PERFORM NXLP
                   END-IF
           WHEN  9 INITIALIZE L-DATE-EDITION 
                   MOVE 99 TO SAVE-KEY
                   PERFORM NXLP
           WHEN 10 IF L-SUITE NOT = 10
                      PERFORM REMBOURSEMENT
                   END-IF
           WHEN 11 MOVE  504014 TO LNK-VAL
                   PERFORM HELP-SCREEN
                   MOVE 1 TO INPUT-ERROR
           END-EVALUATE.
           IF  EXC-KEY > 0
           AND EXC-KEY < 11
               PERFORM TOTAL-LIVRE THRU TOTAL-LIVRE-END
           END-IF.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DECMAL.
           CALL "6-DECMAL" USING LINK-V REG-RECORD DM-RECORD TYP WR-KEY.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           PERFORM DISPLAY-MESSAGE.

      *    HISTORIQUE DES LIVRES
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       TOTAL-LIVRE.
           MOVE 0 TO MAL-IDX.
           MOVE 6 TO LIN-IDX.
           INITIALIZE L-RECORD.
           MOVE LNK-MOIS  TO L-MOIS.
           MOVE 66 TO SAVE-KEY. 
           PERFORM READ-LIVRE THRU READ-LP-END.
           COMPUTE CHOIX-MAX = 2 + MAL-IDX.
       TOTAL-LIVRE-END.
           EXIT.

       READ-LIVRE.
           PERFORM NXLP.
           IF FR-KEY     NOT = L-FIRME
           OR REG-PERSON NOT = L-PERSON
           OR LNK-MOIS   NOT = L-MOIS 
              GO READ-LP-END.
           IF L-SUITE = 0
              GO READ-LIVRE.
           ADD 1 TO MAL-IDX.
           MOVE L-SUITE TO HE-LP-SUITE(MAL-IDX).
           PERFORM DIS-HIS-LIGNE.
           IF LIN-IDX > 22
              GO READ-LP-END.
           GO READ-LIVRE.
       READ-LP-END.
           ADD 1 TO LIN-IDX .
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 24.

       NXLP.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD SAVE-KEY.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.


       EFFACEMENT.
           INITIALIZE CSP-RECORD LNK-SUITE.
           MOVE FR-KEY     TO CSP-FIRME.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE LNK-MOIS   TO CSP-MOIS.

       CREATE-FLAG.
           PERFORM KEY-CSP.
           COMPUTE CSP-CODE = 700 + EXC-KEY.
           PERFORM TEST-CSP.
           EVALUATE EXC-KEY 
              WHEN 1 IF L-FLAG(6) = 0
                        MOVE 0 TO CSP-UNITE L-FLAG(4) 
                        MOVE 704 TO CSP-CODE
                        PERFORM WRITE-CSP
                        PERFORM RECALCUL
                     END-IF
              WHEN 2 MOVE 0 TO CSP-UNITE L-FLAG(4) L-FLAG(6)
                     MOVE 704 TO CSP-CODE
                     PERFORM WRITE-CSP
                     MOVE 706 TO CSP-CODE
                     PERFORM WRITE-CSP
              WHEN 4 MOVE 0 TO CSP-UNITE L-FLAG(1) L-FLAG(2) L-FLAG(6)
                     MOVE 701 TO CSP-CODE
                     PERFORM WRITE-CSP
                     MOVE 702 TO CSP-CODE
                     PERFORM WRITE-CSP
                     MOVE 706 TO CSP-CODE
                     PERFORM WRITE-CSP
                     PERFORM RECALCUL
              WHEN 6 IF PR-CODE-SEXE = 1
                        MOVE "SL" TO LNK-AREA
                        MOVE 58   TO LNK-NUM
                        MOVE 0 TO LNK-POSITION
                        CALL "0-DMESS" USING LINK-V
                        PERFORM AA
                        MOVE 0 TO CSP-UNITE L-FLAG(6)
                        MOVE 706 TO CSP-CODE
                        PERFORM WRITE-CSP
                     ELSE
                        MOVE CSP-UNITE TO L-FLAG(1)
                        MOVE 701 TO CSP-CODE
                        PERFORM WRITE-CSP
                        MOVE 0 TO CSP-UNITE L-FLAG(2) L-FLAG(4)
                        MOVE 702 TO CSP-CODE
                        PERFORM WRITE-CSP
                        MOVE 704 TO CSP-CODE
                        PERFORM WRITE-CSP
                        PERFORM RECALCUL
                     END-IF
           END-EVALUATE.

       RECALCUL.
           PERFORM RECAL.
           MOVE HE-LP-SUITE(MAL-IDX) TO L-SUITE LNK-SUITE.
           PERFORM NXLP.
           CALL "4-CSPMAL" USING LINK-V REG-RECORD L-RECORD.

       KEY-CSP.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY TO CSP-FIRME.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE LNK-MOIS   TO CSP-MOIS
           MOVE L-SUITE    TO CSP-SUITE.


       WRITE-CSP.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.

       RECAL.
           CANCEL "4-NXLP".
           CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES.

       TEST-CSP.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           IF CSP-UNITE = 0
              MOVE 1 TO CSP-UNITE L-FLAG(EXC-KEY)
           ELSE
              MOVE 0 TO CSP-UNITE L-FLAG(EXC-KEY)
           END-IF.
           PERFORM WRITE-CSP.

       REMBOURSEMENT.
           PERFORM KEY-CSP.
           COMPUTE CSP-CODE = 710.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           IF L-FLAG-AVANCE = 0
              IF CSP-TOTAL > 0 
                 MOVE 0 TO CSP-TOTAL L-REMB-CMO 
              ELSE
                 COMPUTE CSP-TOTAL = L-IMPOT + L-A-PAYER
                 MOVE CSP-TOTAL TO L-REMB-CMO 
              END-IF
              PERFORM WRITE-CSP
              MOVE 99 TO SAVE-KEY
              PERFORM NXLP
           END-IF.


       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS THRU SUBTRACT-END
             WHEN 58 PERFORM ADD-MOIS THRU ADD-END
           END-EVALUATE.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-DETAIL.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0 
           OR REG-PERSON = 0
              GO SUBTRACT-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO SUBTRACT-END.
           GO SUBTRACT-MOIS.
       SUBTRACT-END.
           IF LNK-MOIS = 0
              IF REG-PERSON = 0
                 MOVE 1 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.
       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
           OR REG-PERSON = 0
              GO ADD-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO ADD-END.
           GO ADD-MOIS.
       ADD-END.
           IF LNK-MOIS > 12
              IF REG-PERSON = 0
                 MOVE 12 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.


       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           IF REG-PERSON NOT = 0
              MOVE REG-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 
             LINE 3 POSITION 15 SIZE 6
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-END.
           EXIT.

       DIS-HIS-LIGNE.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           MOVE L-JOUR-DEBUT TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 2 LOW.
           MOVE L-JOUR-FIN TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 5 LOW.
           DISPLAY L-SUITE  LINE LIN-IDX POSITION 11 LOW.
           MOVE L-EDIT-J TO HE-JJ.
           MOVE L-EDIT-M TO HE-MM.
           MOVE L-EDIT-A TO HE-AA.
           DISPLAY HE-DATE LINE  LIN-IDX POSITION 14.
           MOVE L-IMPOSABLE-BRUT  TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z4Z2 LINE LIN-IDX POSITION 24.
           MOVE L-UNI-SALAIRE TO HE-Z3Z2.
           INSPECT HE-Z3Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z3Z2 LINE LIN-IDX POSITION 33.
           EVALUATE LNK-LANGUAGE 
              WHEN "F" PERFORM SIGNE-F
              WHEN "D" PERFORM SIGNE-D
              WHEN "E" PERFORM SIGNE-E
           END-EVALUATE.
           IF L-SUITE = 10 
           IF L-FLAG-FAMILLE NOT = 0
              DISPLAY "Adoption" LINE LIN-IDX POSITION 41.

           MOVE L-SALAIRE-NET  TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z4Z2 LINE LIN-IDX POSITION 48.
           MOVE L-IMPOT TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z4Z2 LINE LIN-IDX POSITION 56.
           MOVE L-REMB-CMO TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z4Z2 LINE LIN-IDX POSITION 65.
           MOVE 0 TO SH-00.
           IF L-FLAG-AVANCE = 0
              COMPUTE SH-00 = L-IMPOT + L-A-PAYER
              SUBTRACT L-REMB-CMO FROM SH-00.
           MOVE SH-00 TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z4Z2 LINE LIN-IDX POSITION 74.

       SIGNE-F.
           IF L-FLAG-ACCIDENT NOT = 0
              DISPLAY "A" LINE LIN-IDX POSITION 40.
           IF L-FLAG-HOPITAL NOT = 0
              DISPLAY "H" LINE LIN-IDX POSITION 41.
           IF L-FLAG-FAMILLE NOT = 0
              DISPLAY "F" LINE LIN-IDX POSITION 42.
           IF L-FLAG-AGREE NOT = 0
              DISPLAY "P" LINE LIN-IDX POSITION 43.
           IF L-FLAG-DISPENSE NOT = 0
              DISPLAY "D" LINE LIN-IDX POSITION 44.
           IF L-FLAG-ACCOMPAGNE NOT = 0
              DISPLAY "A" LINE LIN-IDX POSITION 45.
           IF L-FLAG-EXTERNE  NOT = 0
              DISPLAY "X" LINE LIN-IDX POSITION 46.
       SIGNE-D.
           IF L-FLAG-ACCIDENT NOT = 0
              DISPLAY "U" LINE LIN-IDX POSITION 40.
           IF L-FLAG-HOPITAL NOT = 0
              DISPLAY "H" LINE LIN-IDX POSITION 41.
           IF L-FLAG-FAMILLE NOT = 0
              DISPLAY "F" LINE LIN-IDX POSITION 42.
           IF L-FLAG-AGREE NOT = 0
              DISPLAY "P" LINE LIN-IDX POSITION 43.
           IF L-FLAG-DISPENSE NOT = 0
              DISPLAY "F" LINE LIN-IDX POSITION 44.
           IF L-FLAG-ACCOMPAGNE NOT = 0
              DISPLAY "B" LINE LIN-IDX POSITION 45.
           IF L-FLAG-EXTERNE  NOT = 0
              DISPLAY "X" LINE LIN-IDX POSITION 46.
       SIGNE-E.
           IF L-FLAG-ACCIDENT NOT = 0
              DISPLAY "A" LINE LIN-IDX POSITION 40.
           IF L-FLAG-HOPITAL NOT = 0
              DISPLAY "H" LINE LIN-IDX POSITION 41.
           IF L-FLAG-FAMILLE NOT = 0
              DISPLAY "F" LINE LIN-IDX POSITION 42.
           IF L-FLAG-AGREE NOT = 0
              DISPLAY "P" LINE LIN-IDX POSITION 43.
           IF L-FLAG-DISPENSE NOT = 0
              DISPLAY "E" LINE LIN-IDX POSITION 44.
           IF L-FLAG-ACCOMPAGNE NOT = 0
              DISPLAY "A" LINE LIN-IDX POSITION 45.
           IF L-FLAG-EXTERNE  NOT = 0
              DISPLAY "X" LINE LIN-IDX POSITION 46.

       AFFICHAGE-ECRAN.
           MOVE 2551 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE 0 TO LNK-SUITE.
           CANCEL "4-RECAL".
           CANCEL "6-CODPAI".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
                 PERFORM TOTAL-LIVRE THRU TOTAL-LIVRE-END.
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

