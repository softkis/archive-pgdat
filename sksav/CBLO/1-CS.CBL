      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-CS DETAIL CODES SALAIRES                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-CS.

       ENVIRONMENT DIVISION.
      *袴袴袴袴袴袴袴袴袴袴
       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CSDET.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CSDET.FDE".
       FD  TF-TRANS
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X(1830).

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  HE-INPUT-PARAMETER.
      *    LINE COL SIZE HE-SCREEN    LLCCSZHELP
           02 IP-101 PIC 9(10)  VALUE 0328040000.
           02 IP-102 PIC 9(10)  VALUE 0428040000.
           02 IP-103 PIC 9(10)  VALUE 0530020000.

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
           02 I-P OCCURS 1.
              03 I-PP OCCURS  3.
                 04 IP-LINE PIC 99.
                 04 IP-COL  PIC 99.
                 04 IP-SIZE PIC 99.
                 04 IP-HELP PIC 9999.

       01  ECRAN-SUITE.
           02 ECR-S1             PIC 999 VALUE 51.
           02 ECR-S2             PIC 999 VALUE 52.
           02 ECR-S2             PIC 999 VALUE 53.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S              PIC 999 OCCURS 3.

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1    PIC 99 VALUE 18.
           02  CHOIX-MAX-2    PIC 99 VALUE 21.
           02  CHOIX-MAX-3    PIC 99 VALUE 21.
           02  CHOIX-MAX-4    PIC 99 VALUE 21.
           02  CHOIX-MAX-5    PIC 99 VALUE 21.
           02  CHOIX-MAX-6    PIC 99 VALUE 21.
           02  CHOIX-MAX-7    PIC 99 VALUE 21.
           02  CHOIX-MAX-8    PIC 99 VALUE 21.
           02  CHOIX-MAX-9    PIC 99 VALUE 21.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX PIC 99 OCCURS 9.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "CSDEF.REC".
           COPY "CODTXT.REC".
           COPY "LIVREX.REC".
           COPY "PARMOD.REC".
           COPY "V-VAR.CPY".


       01  ECR-DISPLAY.
           02 HE-Z  PIC Z    BLANK WHEN ZERO.
           02 HE-Z2 PIC ZZ.
           02 HE-Z4 PIC ZZZZ.
           02 HE-Z5 PIC ZZZZZ.
           02 HE-ZT PIC ZZ,ZZZZ.
           02 HE-Z2Z2 PIC ZZ,ZZ.
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6Z2 PIC Z(6),ZZ.
           02 HE-Z5Z2 PIC Z(5),ZZ.
           02 HE-Z5Z5 PIC Z(5),Z(5).
           02 HE-Z6Z4 PIC Z(6),ZZZZ.
           02 HE-Z8Z2 PIC Z(8),ZZ.
           02 HE-Z10 PIC X(10).
           02 HE-Z12 PIC X(12).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CSDET TF-TRANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-CS.
       
           OPEN I-O   CSDET.

           INITIALIZE CS-RECORD.
           MOVE 1 TO ECRAN-IDX CS-NUMBER.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0012130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions
           IF ECRAN-IDX = 1
              MOVE 0000680000 TO EXC-KFR (14) 
           EVALUATE INDICE-ZONE
           WHEN 1  MOVE 0129232216 TO EXC-KFR(1)
                   MOVE 3700000000 TO EXC-KFR(2)
                   MOVE 0000130000 TO EXC-KFR(3)
                   MOVE 0000000065 TO EXC-KFR(13)
                   MOVE 6600000000 TO EXC-KFR(14) 
                   MOVE 0000530000 TO EXC-KFR(11)
           WHEN 2  THRU 3
                   MOVE 0000000005 TO EXC-KFR(1)
                   MOVE 0000080000 TO EXC-KFR(4)
                   MOVE 0000000065 TO EXC-KFR(13)
                   MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 4  THRU 5 
                   MOVE 0100000000 TO EXC-KFR(1)
           WHEN 7  MOVE 0100000000 TO EXC-KFR(1)
           WHEN 10 THRU 17 
                   MOVE 0100000005 TO EXC-KFR(1)
           WHEN 18 MOVE 0000000005 TO EXC-KFR(1)
                   MOVE 0000000078 TO EXC-KFR(2)
                   MOVE 0016000000 TO EXC-KFR(3)
                   MOVE 0000080000 TO EXC-KFR(4)
                   MOVE 0052000000 TO EXC-KFR(11)
           END-IF.        

           IF ECRAN-IDX > 1
              MOVE 0067680000 TO EXC-KFR (14) 
              EVALUATE INDICE-ZONE
              WHEN CHOIX-MAX(ECRAN-IDX)
                   MOVE 0100000005 TO EXC-KFR(1)
                   MOVE 0052000000 TO EXC-KFR(11)
              WHEN OTHER 
                   MOVE 0129000005 TO EXC-KFR(1)
                   MOVE 0000080000 TO EXC-KFR(2)
                   MOVE 0000000065 TO EXC-KFR(13)
                   MOVE 6667680000 TO EXC-KFR(14)
           END-IF.    

           IF ECRAN-IDX = 9
              MOVE 0067000000 TO EXC-KFR(14) 
           END-IF.    
               
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.



           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2
           WHEN  3 PERFORM AVANT-3
           WHEN  4 PERFORM AVANT-4
           WHEN  5 PERFORM AVANT-5
           WHEN  6 PERFORM AVANT-6
           WHEN  7 PERFORM AVANT-7
           WHEN  8 PERFORM AVANT-8
           WHEN  9 PERFORM AVANT-9
           WHEN 10 THRU 17 PERFORM AVANT-ALL THRU AVANT-ALL-END
           WHEN 18 PERFORM AVANT-DEC.

           IF ECRAN-IDX > 1
              IF INDICE-ZONE < CHOIX-MAX(ECRAN-IDX)
                 PERFORM AVANT-LP THRU AVANT-LP-END
              ELSE 
                 PERFORM AVANT-DEC
              END-IF
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           INITIALIZE INPUT-ERROR.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 67 
              SUBTRACT 1 FROM ECRAN-IDX
              PERFORM AFFICHAGE-ECRAN
           END-IF.
           IF EXC-KEY = 68 
              ADD 1 TO ECRAN-IDX
              IF ECRAN-IDX > 1
                 PERFORM AFFICHAGE-ECRAN
              END-IF.
           IF EXC-KEY = 67 
           OR EXC-KEY = 68 
              PERFORM AFFICHAGE-DETAIL
              MOVE 1 TO INDICE-ZONE
              IF ECRAN-IDX = 1
                 MOVE 2 TO INDICE-ZONE
              END-IF
              INITIALIZE DECISION
              GO TRAITEMENT-ECRAN
           END-IF.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3
           WHEN  5 PERFORM APRES-5 
           WHEN  7 PERFORM APRES-7 
           WHEN  8 PERFORM APRES-8 
           WHEN  9 PERFORM APRES-9 
           WHEN 18 PERFORM APRES-DEC.

           IF ECRAN-IDX > 1
             IF INDICE-ZONE = CHOIX-MAX(ECRAN-IDX)
                PERFORM APRES-DEC
             END-IF
           END-IF.

           EVALUATE EXC-KEY
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
           END-EVALUATE.

           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT CD-NUMBER
             LINE  3 POSITION 23 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-2.
           IF CS-ANNEE = 0
           OR CS-ANNEE = 9999
              MOVE 2001 TO CS-ANNEE
           END-IF.
           ACCEPT CS-ANNEE
             LINE  4 POSITION 23 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-3.
           ACCEPT CS-MOIS  
             LINE  5 POSITION 25 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-4.        
           MOVE CS-PERMISSION TO HE-Z10.
           ACCEPT HE-Z10
             LINE   7 POSITION 23 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "NUMERIC"
             ON EXCEPTION  EXC-KEY  CONTINUE.
             MOVE HE-Z10 TO CS-PERMISSION.

       AVANT-5.        
           MOVE CS-VALEURS TO HE-Z10.
           ACCEPT HE-Z10
             LINE   8 POSITION 23 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "NUMERIC"
             ON EXCEPTION  EXC-KEY  CONTINUE.
             MOVE HE-Z10 TO CS-VALEURS.

       AVANT-6.
           MOVE CS-PERIODES TO HE-Z12.
           ACCEPT HE-Z12
             LINE 10 POSITION 23 SIZE 12
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "NUMERIC"
             ON EXCEPTION  EXC-KEY CONTINUE.
           MOVE HE-Z12 TO CS-PERIODES.

       AVANT-7.
           ACCEPT CS-MALADIE
             LINE 11 POSITION 23 SIZE 1
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.
       AVANT-8.
           ACCEPT CS-FRAIS
             LINE 12 POSITION 23 SIZE 1
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "UPPER"
             ON EXCEPTION EXC-KEY CONTINUE.
       AVANT-9.
           ACCEPT CS-NEGATIF
             LINE 12 POSITION 45 SIZE 1
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-ALL.
           COMPUTE IDX = INDICE-ZONE - 9.
           MOVE 1 TO IDX-1.
       AVANT-ALL-1.
           PERFORM AVANT-PARAM.
           IF EXC-KEY = 82 PERFORM END-PROGRAM. 
           IF EXC-KEY-FUN(EXC-KEY) = 0 
              GO AVANT-ALL-1.
           EVALUATE EXC-KEY
                WHEN 5 GO AVANT-ALL-END
                WHEN 54 MOVE 9 TO IDX
                        MOVE 13 TO IDX-1
                WHEN 13 ADD 1 TO IDX-1
                WHEN 27 SUBTRACT 1 FROM IDX-1
                WHEN 52 SUBTRACT 1 FROM IDX
                        IF IDX = 0 
                           MOVE 1 TO IDX
                           MOVE 0 TO IDX-1
                        END-IF
                WHEN 53 ADD 1 TO IDX
                WHEN 68 GO AVANT-ALL-END
           END-EVALUATE.
           MOVE IDX TO IDX-3.
           MOVE IDX-1 TO IDX-4.
           PERFORM APRES-ALL.
           IF EXC-KEY = 1 
              MOVE IDX-3 TO IDX
              MOVE IDX-4 TO IDX-1
              GO AVANT-ALL-1.
           IF IDX-1 > 13
              ADD 1 TO IDX 
              MOVE 1 TO IDX-1
           END-IF.
           IF IDX-1 > 0 
           AND IDX < 9
              GO AVANT-ALL-1.
           COMPUTE INDICE-ZONE = IDX + 9.
           MOVE 0 TO DECISION.
       AVANT-ALL-END.
           IF EXC-KEY = 5 PERFORM APRES-DEC.

       AVANT-PARAM.
           COMPUTE LIN-IDX = IDX + 14.
           IF IDX-1 < 5 OR > 7
              MOVE 0000000000 TO EXC-KFR(2)
           ELSE
              MOVE 0000000073 TO EXC-KFR(2)
           END-IF.
           PERFORM DISPLAY-F-KEYS.

           EVALUATE IDX-1
              WHEN  1 PERFORM ACCEPT-SAISIE
              WHEN  2 PERFORM ACCEPT-IMPRESSION
              WHEN  3 PERFORM ACCEPT-COLON
              WHEN  4 PERFORM ACCEPT-DECIM
              WHEN  5 PERFORM ACCEPT-PREDEF
              WHEN  6 PERFORM ACCEPT-MAX
              WHEN  7 PERFORM ACCEPT-MIN
              WHEN  8 PERFORM ACCEPT-NULL
      *       WHEN  9 PERFORM ACCEPT-TEXTE
              WHEN 10 PERFORM ACCEPT-PROC
              WHEN 11 PERFORM ACCEPT-NOT
              WHEN 12 PERFORM ACCEPT-FORM
           END-EVALUATE.
           PERFORM DISPLAY-DETAIL.


       ACCEPT-SAISIE. 
           ACCEPT CS-SAISIE(IDX)
             LINE LIN-IDX POSITION 2 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       ACCEPT-IMPRESSION.
           ACCEPT CS-IMPRESSION(IDX)
             LINE LIN-IDX POSITION 4 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       ACCEPT-COLON.
           ACCEPT CS-COLONNES(IDX)
             LINE LIN-IDX POSITION 6 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       ACCEPT-DECIM.
           ACCEPT CS-DECIMALES(IDX)
             LINE LIN-IDX POSITION 9 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       ACCEPT-PREDEF.
           MOVE CS-VAL-PREDEFINIE(IDX) TO HE-Z6Z4.
           MOVE 0 TO IDX-3.
           INSPECT HE-Z6Z4 TALLYING IDX-3 FOR ALL " ".
           ADD 1 TO IDX-3.
           IF IDX-3 > 9
              MOVE 1 TO IDX-3
           END-IF.
           ACCEPT HE-Z6Z4
             LINE LIN-IDX POSITION 11 SIZE 11
             TAB UPDATE NO BEEP CURSOR  IDX-3
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM CONV.
           MOVE HE-Z6Z4 TO CS-VAL-PREDEFINIE(IDX).

       ACCEPT-MAX.
           MOVE CS-VAL-MAX(IDX) TO HE-Z6Z4.
           ACCEPT HE-Z6Z4
             LINE LIN-IDX POSITION 22 SIZE 11
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM CONV.
           MOVE HE-Z6Z4 TO CS-VAL-MAX(IDX).

       ACCEPT-MIN.
           MOVE CS-VAL-MIN(IDX) TO HE-Z6Z4.
           ACCEPT HE-Z6Z4
             LINE LIN-IDX POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM CONV.
           MOVE HE-Z6Z4 TO CS-VAL-MIN(IDX).

       CONV.
           INSPECT HE-Z6Z4 REPLACING ALL "." BY ",".
           IF EXC-KEY = 10
              MOVE HE-Z6Z4 TO SH-00
              COMPUTE SH-00 = SH-00 / MOIS-IDX(LNK-MOIS)  
              MOVE SH-00 TO HE-Z6Z4 
              MOVE 13 TO EXC-KEY
           END-IF.


      *ACCEPT-TEXTE.
      *    ACCEPT CS-DESCRIPTION(IDX)
      *      LINE LIN-IDX POSITION 46 SIZE 10
      *      TAB UPDATE NO BEEP CURSOR  1
      *      ON EXCEPTION  EXC-KEY  CONTINUE.

       ACCEPT-PROC.
           ACCEPT CS-PROCEDURE(IDX)
             LINE LIN-IDX POSITION 56 SIZE 5 LOW 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       ACCEPT-FORM.
           ACCEPT CS-FORMULE(IDX)
             LINE LIN-IDX POSITION 77 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       ACCEPT-NOT.
           ACCEPT CS-NOTPOL(IDX)
             LINE LIN-IDX POSITION 62 SIZE 15
             CONTROL "UPPER"
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       ACCEPT-NULL.
           ACCEPT CS-NULL(IDX)
             LINE LIN-IDX POSITION 44 SIZE 1 LOW 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-LP.
           IF INDICE-ZONE = 0
              MOVE 1 TO INDICE-ZONE.
           MOVE INDICE-ZONE TO IDX.
           PERFORM DIS-LP.
           PERFORM DISPLAY-F-KEYS.
           COMPUTE LIN-IDX = IDX + 2.
           COMPUTE IDX-2 = ECRAN-IDX - 1.
           ACCEPT CS-LIVRE(IDX-2, IDX)
             LINE LIN-IDX POSITION 15 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM DIS-LP.
           IF EXC-KEY = 82 PERFORM END-PROGRAM
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-LP.
           EVALUATE EXC-KEY
                WHEN  5 GO AVANT-LP-END
                WHEN  8 MOVE 0 TO CS-LIVRE(IDX-2, IDX)
                        PERFORM DIS-LP
                        ADD 1 TO IDX
                WHEN 65 THRU 66 MOVE CS-LIVRE(IDX-2, IDX) TO LEX-NUMBER 
                        PERFORM NEXT-MESSAGE
                        MOVE LEX-NUMBER TO CS-LIVRE(IDX-2, IDX)
                WHEN  2 CALL "2-LEX" USING LINK-V LEX-RECORD
                        IF LEX-NUMBER > 0
                           MOVE LEX-NUMBER TO CS-LIVRE(IDX-2, IDX)
                        END-IF
                        MOVE IDX TO IDX-3
                        PERFORM AFFICHAGE-ECRAN 
                        PERFORM AFFICHAGE-DETAIL
                        MOVE IDX-3 TO IDX
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO IDX
                WHEN 13 ADD 1 TO IDX
                WHEN 27 SUBTRACT 1 FROM IDX
                WHEN 52 SUBTRACT 1 FROM IDX
                WHEN 53 ADD 1 TO IDX
                WHEN 67 GO AVANT-LP-END
                WHEN 68 GO AVANT-LP-END
           END-EVALUATE.
           COMPUTE INDICE-ZONE = IDX.
           IF IDX < CHOIX-MAX(ECRAN-IDX)
              GO AVANT-LP.
           MOVE 98 TO EXC-KEY.
       AVANT-LP-END.
           IF EXC-KEY = 5 PERFORM APRES-DEC.

       AVANT-PATH.
           MOVE "AA" TO LNK-AREA.
           MOVE 32 TO LNK-NUM.
           MOVE "L" TO LNK-LOW.
           PERFORM DISPLAY-MESSAGE.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 24 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       NEXT-CODSAL.
           CALL "6-CODSAL" USING LINK-V CS-RECORD EXC-KEY.

       NEXT-MESSAGE.
           CALL "6-LEX" USING LINK-V LEX-RECORD EXC-KEY.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-CSDEF" USING LINK-V 
                    MOVE LNK-VAL TO CD-NUMBER 
                    PERFORM AFFICHAGE-ECRAN 
           WHEN   2 CALL "2-CSDEF" USING LINK-V CD-RECORD
                    CANCEL "2-CSDEF"
                    PERFORM AFFICHAGE-ECRAN 
           WHEN  3 CALL "2-CSD" USING LINK-V CTX-RECORD
                   IF CTX-NUMBER > 0
                      MOVE CTX-NUMBER TO CD-NUMBER
                      CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY
                   END-IF
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  OTHER PERFORM NEXT-CSDEF
           END-EVALUATE.
           IF CD-NUMBER = 0
              MOVE 1 TO INPUT-ERROR
           ELSE
              MOVE CD-NUMBER TO CS-NUMBER
              PERFORM CSDET-RECENT
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           EVALUATE EXC-KEY
           WHEN  65 PERFORM CSDET-PRECEDENT
           WHEN  66 PERFORM CSDET-SUIVANT
           WHEN   5 PERFORM APRES-DEC     
           WHEN  18 PERFORM APRES-DEC     
           WHEN OTHER
                    IF CS-MOIS = 0
                       MOVE 1 TO CS-MOIS
                    END-IF
                    READ CSDET INVALID 
                       MOVE "AA" TO LNK-AREA
                       MOVE 13 TO LNK-NUM
                       PERFORM DISPLAY-MESSAGE
                    END-READ
                    IF CS-ANNEE < 2001
                       MOVE 2001 TO CS-ANNEE
                       MOVE 1 TO INPUT-ERROR
                    END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-3.
           IF CS-MOIS < 1
              MOVE 1 TO CS-MOIS
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF CS-MOIS > 12
              MOVE 12 TO CS-MOIS
              MOVE 1 TO INPUT-ERROR
           END-IF.
           EVALUATE EXC-KEY
              WHEN  65 PERFORM CSDET-PRECEDENT
              WHEN  66 PERFORM CSDET-SUIVANT
              WHEN  18 PERFORM APRES-DEC     
              WHEN   5 PERFORM APRES-DEC.
           IF INPUT-ERROR = 0
              READ CSDET INVALID 
              MOVE "AA" TO LNK-AREA
              MOVE 13 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              END-READ.
           PERFORM AFFICHAGE-DETAIL.

       APRES-ALL.
           EVALUATE IDX-1
              WHEN 10 PERFORM APRES-PROC
              WHEN 11 PERFORM APRES-NOTPOL
              WHEN 12 PERFORM APRES-FORM
           END-EVALUATE.

       APRES-1-ORI.
           EVALUATE EXC-KEY
           WHEN  1 MOVE 01001001 TO LNK-VAL
                   PERFORM HELP-SCREEN
                   MOVE 1 TO INPUT-ERROR
           WHEN 65 THRU 66 PERFORM NEXT-CODSAL
           WHEN  4 PERFORM COPY-CODSAL
      *    WHEN  5 PERFORM COPY-CODCOPY
           WHEN  6 CALL "2-CODSLX" USING LINK-V CS-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  2 CALL "2-CODSAL" USING LINK-V CS-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  3 CALL "2-CS" USING LINK-V CS-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN 18 DELETE CSDET INVALID CONTINUE END-DELETE
                   MOVE 66 TO EXC-KEY
                   PERFORM NEXT-CODSAL
           WHEN OTHER PERFORM READ-CODSAL
           END-EVALUATE.
           IF CS-KEY = 0 MOVE 1 TO INPUT-ERROR.

           PERFORM DIS-HE-01 THRU DIS-HE-END.

       READ-CODSAL.
           READ CSDET INVALID INITIALIZE CD-REC-DET END-READ.

       APRES-5.
           EVALUATE EXC-KEY
           WHEN 1 MOVE 01001003 TO LNK-VAL
                  PERFORM HELP-SCREEN
                  MOVE 1 TO INPUT-ERROR.
       APRES-7.
           EVALUATE EXC-KEY
           WHEN 1 MOVE 01001006 TO LNK-VAL
                  PERFORM HELP-SCREEN
                  MOVE 1 TO INPUT-ERROR.

       APRES-8.
           IF  CS-FRAIS NOT = " "
           AND CS-FRAIS NOT = "N"
           AND CS-FRAIS NOT = "O"
           AND CS-FRAIS NOT = "J"
           AND CS-FRAIS NOT = "Y"
               MOVE 1 TO INPUT-ERROR
               MOVE " " TO CS-FRAIS.
       APRES-9.
           IF  CS-NEGATIF NOT = " "
           AND CS-NEGATIF NOT = "-"
               MOVE 1 TO INPUT-ERROR
               MOVE " " TO CS-NEGATIF.

       APRES-PROC.
           EVALUATE EXC-KEY
           WHEN 1 MOVE 01001007 TO LNK-VAL
                  PERFORM HELP-SCREEN
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR
           WHEN 2 CALL "2-LEX" USING LINK-V LEX-RECORD
                  IF LEX-NUMBER > 0
                     MOVE LEX-NUMBER TO CS-PROCEDURE(IDX)
                     ADD 10000 TO CS-PROCEDURE(IDX)
                  END-IF
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
                  PERFORM DISPLAY-F-KEYS
           END-EVALUATE.
                  

       APRES-FORM.
           EVALUATE EXC-KEY
           WHEN 1 MOVE 01001008 TO LNK-VAL
                  PERFORM HELP-SCREEN
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR.


       APRES-NOTPOL.
           EVALUATE EXC-KEY
           WHEN 1 MOVE 01001009 TO LNK-VAL
                  PERFORM HELP-SCREEN
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR.

       COPY-CODSAL.   
           MOVE CS-NUMBER TO IDX-2.
           ACCEPT CS-NUMBER
             LINE  3 POSITION 60 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 4
              MOVE 13 TO EXC-KEY.
           READ CSDET INVALID CONTINUE
                END-READ.
           MOVE IDX-2 TO CS-NUMBER.


       APRES-DEC.
           EVALUATE EXC-KEY 
              WHEN 5 CALL "0-TODAY" USING TODAY
                     MOVE TODAY-TIME TO CD-TIME
                     MOVE LNK-USER TO CD-USER
                     WRITE CS-RECORD INVALID REWRITE CS-RECORD
                     END-WRITE
                     IF LNK-SQL = "Y"
                        CALL "9-CODSAL" USING LINK-V CS-RECORD WR-KEY
                     END-IF
                     IF ECRAN-IDX > 1
                        MOVE 1  TO ECRAN-IDX
                        PERFORM AFFICHAGE-ECRAN
                     END-IF
                     PERFORM AFFICHAGE-DETAIL
                     MOVE 1  TO DECISION INDICE-ZONE
                     MOVE CS-NUMBER TO LNK-VAL
              WHEN 10 PERFORM AVANT-PATH
                      PERFORM TRAITEMENT
              WHEN 12 PERFORM AVANT-PATH
                      PERFORM IMPORT
              WHEN 18 DELETE CSDET INVALID CONTINUE END-DELETE
      *              IF LNK-SQL = "Y"
      *                 CALL "9-CODSAL" USING LINK-V CS-RECORD DEL-KEY
      *              END-IF
                     INITIALIZE CS-REC-DET
                     IF ECRAN-IDX > 1
                        MOVE 1  TO ECRAN-IDX
                        PERFORM AFFICHAGE-ECRAN
                     END-IF
                     PERFORM AFFICHAGE-DETAIL
                     MOVE 1 TO DECISION
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       DIS-HE-01.
           MOVE CD-NUMBER TO HE-Z4 CTX-NUMBER.
           CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           display cTX-nom line 1 position 1.
           DISPLAY HE-Z4 LINE 3 POSITION 23.
           DISPLAY CD-DEFINITION  LINE  3 POSITION 30 SIZE 50.
       DIS-HE-02.
           DISPLAY CS-ANNEE LINE  4 POSITION 23.
       DIS-HE-03.
           DISPLAY CS-MOIS LINE  5 POSITION 25.
       DIS-HE-04.
           DISPLAY CS-PERMISSION  LINE  7 POSITION 23.
       DIS-HE-05.
           DISPLAY CS-VALEURS LINE  8 POSITION 23.
       DIS-HE-06.
           DISPLAY CS-PERIODES LINE 10 POSITION 23.
       DIS-HE-07.
           DISPLAY CS-MALADIE LINE 11 POSITION 23.
       DIS-HE-08.
           DISPLAY CS-FRAIS LINE 12 POSITION 23.
       DIS-HE-09.
           DISPLAY CS-NEGATIF LINE 12 POSITION 45.
       DIS-HE-LINES.
           PERFORM DISPLAY-DETAIL VARYING IDX FROM 1 BY 1
           UNTIL IDX > 8.
       DIS-HE-END.
           EXIT.

       DISPLAY-DETAIL.
           COMPUTE LIN-IDX = IDX + 14.
           MOVE CS-SAISIE(IDX) TO HE-Z. 
           DISPLAY HE-Z LINE LIN-IDX POSITION  2.
           MOVE CS-IMPRESSION(IDX) TO HE-Z. 
           DISPLAY HE-Z LINE LIN-IDX POSITION  4.
           MOVE CS-COLONNES(IDX) TO HE-Z2. 
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 6.
           MOVE CS-DECIMALES(IDX) TO HE-Z. 
           DISPLAY HE-Z LINE LIN-IDX POSITION 9.
           MOVE CS-VAL-PREDEFINIE(IDX) TO HE-Z6Z4.
           DISPLAY HE-Z6Z4 LINE LIN-IDX POSITION 11.
           MOVE CS-VAL-MAX(IDX) TO HE-Z6Z4.
           DISPLAY HE-Z6Z4 LINE LIN-IDX POSITION 22.
           MOVE CS-VAL-MIN(IDX) TO HE-Z6Z4.
           DISPLAY HE-Z6Z4 LINE LIN-IDX POSITION 33.
           MOVE CS-NULL(IDX) TO HE-Z. 
           DISPLAY HE-Z LINE LIN-IDX POSITION 44 LOW.
           DISPLAY CTX-DESCRIPTION(IDX) LINE LIN-IDX POSITION 46.
           MOVE CS-PROCEDURE(IDX) TO HE-Z5. 
           DISPLAY HE-Z5 LINE LIN-IDX POSITION 56 LOW.
           DISPLAY CS-NOTPOL(IDX) LINE LIN-IDX POSITION 62 SIZE 15.
           MOVE CS-FORMULE(IDX)   TO HE-Z4. 
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 77.

       AFFICHAGE-ECRAN.
           IF ECRAN-IDX = 1
              MOVE 36 TO LNK-VAL 
           ELSE
              MOVE 32 TO LNK-VAL 
           END-IF.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           IF ECRAN-IDX > 1
              COMPUTE IDX-1 = (ECRAN-IDX - 1)
              MOVE CS-KEY TO HE-Z4
              DISPLAY HE-Z4  LINE 1 POSITION 2
              DISPLAY CTX-NOM LINE 1 POSITION 7 SIZE 20
              DISPLAY CTX-DESCRIPTION(IDX-1) LINE 1 POSITION 30.
    
       AFFICHAGE-DETAIL.
           EVALUATE ECRAN-IDX
              WHEN 1 PERFORM DIS-HE-01 THRU DIS-HE-END 
              WHEN OTHER 
              PERFORM DIS-LP VARYING IDX FROM 1 BY 1 UNTIL IDX > 20
           END-EVALUATE.

       DIS-LP.
           COMPUTE LIN-IDX = 2 + IDX.
           COMPUTE IDX-2 = ECRAN-IDX - 1.
           MOVE IDX TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 5.
           MOVE IDX-2 TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 10 LOW.
           MOVE CS-LIVRE(IDX-2, IDX) TO LNK-NUM HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 15.

           COMPUTE LNK-POSITION = LIN-IDX * 1000000 + 255000. 
           CALL "0-DLEX" USING LINK-V.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
             IF DECISION = 99 
                DISPLAY CS-USER LINE 24 POSITION 5
                DISPLAY CS-ST-JOUR  LINE 24 POSITION 15
                DISPLAY CS-ST-MOIS  LINE 24 POSITION 18
                DISPLAY CS-ST-ANNEE LINE 24 POSITION 21
                DISPLAY CS-ST-HEURE LINE 24 POSITION 30
                DISPLAY CS-ST-MIN   LINE 24 POSITION 33
                GO AVANT-DEC
             END-IF.
           COPY "XDEC.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

       TRAITEMENT.
           INITIALIZE CS-RECORD.
           OPEN OUTPUT TF-TRANS.
           PERFORM ASCII-DEBUT THRU ASCII-END.
           CLOSE TF-TRANS.

       ASCII-DEBUT.
           CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NX-KEY.
           IF CD-DEFINITION = SPACES
              GO ASCII-END.
           WRITE TF-RECORD FROM CS-RECORD
           GO ASCII-DEBUT.
       ASCII-END.
           EXIT.

       IMPORT.
           INITIALIZE TF-RECORD.
           OPEN INPUT TF-TRANS.
           PERFORM IMPORT-DEBUT THRU IMPORT-END.
           CLOSE TF-TRANS.

       IMPORT-DEBUT.
           READ TF-TRANS AT END GO IMPORT-END.
           MOVE TF-RECORD TO CD-RECORD.
           WRITE CS-RECORD INVALID REWRITE CS-RECORD.
           GO IMPORT-DEBUT.
       IMPORT-END.
           EXIT.

      *?????????????????????????????????????????????????????????

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       NEXT-CSDEF.
           CALL "6-CSDEF" USING LINK-V CD-RECORD EXC-KEY.

       CSDET-RECENT.
           INITIALIZE NOT-FOUND.
           MOVE 9999 TO CS-ANNEE.
           PERFORM CSDET-PRECEDENT.

       CSDET-PRECEDENT.
           MOVE 0 TO NOT-FOUND .
           START CSDET KEY < CS-KEY INVALID 
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ CSDET PREVIOUS AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           PERFORM TEST-REG.

       CSDET-SUIVANT.
           MOVE 0 TO NOT-FOUND .
           START CSDET KEY > CS-KEY INVALID INITIALIZE CS-RECORD
                NOT INVALID 
                   READ CSDET PREVIOUS AT END INITIALIZE CS-RECORD
           END-READ.
           PERFORM TEST-REG.

       TEST-REG.
           IF CD-NUMBER NOT = CS-NUMBER
              INITIALIZE CS-RECORD.

       DIS-E1-1.
           DISPLAY CD-NUMBER
           LINE     IP-LINE (1, 1) POSITION IP-COL  (1, 1).
           CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY.
           DISPLAY CD-DEFINITION LINE 3 POSITION 30 SIZE 50.

       DIS-E1-2.
           DISPLAY CS-ANNEE LINE IP-LINE(1, 2) POSITION IP-COL(1, 2).

       DIS-E1-3.
           MOVE CS-MOIS TO HE-Z2.
           DISPLAY HE-Z2 LINE IP-LINE(1, 3) POSITION IP-COL(1, 3).

       END-PROGRAM.
           CLOSE CSDET.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           
