      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-USER MODULE LECTURE USER                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-USER.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "USER.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "USER.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  ACTION  PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "USER.LNK".
       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON USER.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-USER.
       
           IF NOT-OPEN = 0
              OPEN I-O USER
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO US-RECORD.

           EVALUATE EXC-KEY 
           WHEN 0 READ USER NO LOCK INVALID INITIALIZE US-REC-DET
                      END-READ
                  GO EXIT-0
           WHEN 65 PERFORM START-1
               READ USER PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-0
           WHEN 66 PERFORM START-2
               READ USER NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-0
           WHEN 98 DELETE USER INVALID CONTINUE END-DELETE
               GO EXIT-1
           WHEN 99 WRITE US-RECORD INVALID REWRITE US-RECORD 
                   END-WRITE
               GO EXIT-0
           WHEN OTHER READ USER NO LOCK INVALID GO EXIT-1 END-READ
               GO EXIT-0
           END-EVALUATE.

       EXIT-0.
           IF US-FIRME < 1
              MOVE 0 TO US-FIRME.
           IF US-PERSON < 1
              MOVE 0 TO US-PERSON.
           MOVE US-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE US-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START USER KEY < US-KEY INVALID GO EXIT-1.
       START-2.
           START USER KEY > US-KEY INVALID GO EXIT-1.


               