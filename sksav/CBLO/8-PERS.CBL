      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-PERS TRANSFER ASCII DONNEES PERSONNES     �
      *  � MODULE APPELE PAR 4-LOOP                              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-PERS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           SELECT OPTIONAL TF-PERS ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.

       FD  TF-PERS 
           LABEL RECORD STANDARD
           DATA RECORD TFP-RECORD.

      *  ENREGISTREMENT FICHIER DE TRANSFER ASCII PERSONNE
      
       01  TFP-RECORD.
           02 TFP-FIRME           PIC 999999.
           02 TFP-FILLER-1        PIC X.
           02 TFP-NUMBER          PIC 9(6).
           02 TFP-FILLER-2        PIC X.
           02 TFP-MATCHCODE       PIC X(10).
           02 TFP-FILLER-3        PIC X.
           02 TFP-PAYS            PIC XXXX.
           02 TFP-FILLER-4        PIC X.
           02 TFP-CODE-POST       PIC Z(5).
           02 TFP-FILLER-5        PIC X.
           02 TFP-NOM             PIC X(30).
           02 TFP-FILLER-6        PIC X.
           02 TFP-PRENOM          PIC X(20).
           02 TFP-FILLER-7        PIC X.
           02 TFP-NOM-JF          PIC X(25).
           02 TFP-FILLER-8        PIC X.
           02 TFP-PRENOM-MARI     PIC X(12).
           02 TFP-FILLER-9        PIC X.
           02 TFP-RUE             PIC X(40).
           02 TFP-FILLER-10       PIC X.
           02 TFP-NUMERO          PIC X(6).
           02 TFP-FILLER-11       PIC X.
           02 TFP-LOCALITE        PIC X(40).
           02 TFP-FILLER-12       PIC X.
           02 TFP-CODE-SEXE       PIC 9.
           02 TFP-FILLER-13       PIC X.
           02 TFP-NATIONALITE     PIC XXX.
           02 TFP-FILLER-14       PIC X.
           02 TFP-LANGUAGE        PIC XXXX.
           02 TFP-FILLER-15       PIC X.
           02 TFP-LIEU-NAISSANCE  PIC X(40).
           02 TFP-FILLER-16       PIC X.
           02 TFP-PHONE-PRIVE     PIC X(20).
           02 TFP-FILLER-18       PIC X.
           02 TFP-PHONE-INTERNE   PIC X(20).
           02 TFP-FILLER-19       PIC X.
           02 TFP-PHONE-MOBILE  PIC X(20).
           02 TFP-FILLER-19A      PIC X.
           02 TFP-MALADIE-AGENCE  PIC X(12).
           02 TFP-FILLER-20       PIC X.
           02 TFP-DATE-NAISS.
              04 TFD-NAISS-A      PIC 9999.
              04 TFP-FILLER-21    PIC X.
              04 TFD-NAISS-M      PIC 99.
              04 TFP-FILLER-22    PIC X.
              04 TFD-NAISS-J      PIC 99.
              04 TFP-FILLER-23    PIC X.
              04 TFD-SNOCS        PIC 999.
           02 TFP-FILLER-24       PIC X.
           02 TFP-DATE-ANCIEN.
              04 TFD-ANCIEN-A     PIC 9999.
              04 TFP-FILLER-25    PIC X.
              04 TFD-ANCIEN-M     PIC 99.
              04 TFP-FILLER-26    PIC X.
              04 TFD-ANCIEN-J     PIC 99.
           02 TFP-FILLER-24       PIC X.
           02 TFP-DATE-ENTREE.
              04 TFD-ENTREE-A     PIC 9999.
              04 TFP-FILLER-25    PIC X.
              04 TFD-ENTREE-M     PIC 99.
              04 TFP-FILLER-26    PIC X.
              04 TFD-ENTREE-J     PIC 99.
           02 TFP-FILLER-27       PIC X.
           02 TFP-DATE-DEP.
              04 TFD-SORTIE-A     PIC 9999.
              04 TFP-FILLER-28    PIC X.
              04 TFD-SORTIE-M     PIC 99.
              04 TFP-FILLER-29    PIC X.
              04 TFD-SORTIE-J     PIC 99.
           02 TFP-FILLER-30       PIC X.
           02 TFP-COMPETENCE      PIC 9.
           02 TFP-FILLER-31       PIC X.
           02 TFP-BADGE           PIC 9(8).
           02 TFP-FILLER-32       PIC X.
           02 TFP-CONTRAT         PIC 9999.
           02 TFP-FILLER-33       PIC X.
           02 TFP-BANQUE          PIC X(10).
           02 TFP-FILLER-34       PIC X.
           02 TFP-COMPTE          PIC X(35).
           02 TFP-FILLER-35       PIC X.
           02 TFP-POLITESSE       PIC X(25).
           02 TFP-FILLER-36       PIC X.
           02 TFC-DATE.
              03 TFC-ANNEE       PIC 9999.
              03 TFC-MOIS        PIC 99.
           02 TFC-FILLER         PIC X.
           02 TFC-STATUT         PIC 9.
           02 TFC-FILLER         PIC X.
           02 TFC-DEPARTEMENT    PIC 9(8).
           02 TFC-FILLER         PIC X.
           02 TFC-EQUIPE         PIC 9999.
           02 TFC-FILLER         PIC X.
           02 TFC-METIER         PIC X(10).
           02 TFC-FILLER         PIC X.
           02 TFC-MET-NOM        PIC X(25).
           02 TFC-FILLER         PIC X.
           02 TFC-POSITION       PIC X(60).
           02 TFC-FILLER         PIC X.
           02 TFC-HOR-MEN        PIC 999.
           02 TFC-FILLER         PIC X.
           02 TFC-CONGE          PIC 99.
           02 TFC-FILLER         PIC X.
           02 TFC-TARIF-KM       PIC 99.
           02 TFC-FILLER         PIC X.
           02 TFC-SAL-HOR        PIC Z(4),ZZZZ.
           02 TFC-FILLER         PIC X.
           02 TFC-SAL-MENS       PIC Z(7),ZZ.
           02 TFC-FILLER         PIC X.
           02 TFC-DEPLACEMENT    PIC 9(4).
           02 TFC-FILLER         PIC X.
           02 TFC-JOURS-CONGE    PIC 99.
           02 TFC-FILLER         PIC X.
           02 TFC-HRS-JOUR       PIC 99,99.
           02 TFC-FILLER         PIC X.
           02 TFC-JRS-SEMAINE    PIC 999.
           02 TFC-FILLER         PIC X.
           02 TFC-HRS-MOIS       PIC ZZZ,ZZ.
           02 TFC-FILLER         PIC X.
           02 TFC-BAREME         PIC X(10).
           02 TFC-FILLER         PIC X.
           02 TFC-GRADE          PIC 99999.
           02 TFC-FILLER         PIC X.
           02 TFC-ECHELON        PIC 99999.
           02 TFC-FILLER         PIC X.
           02 TFC-PRIME-1        PIC 9(6).
           02 TFC-FILLER         PIC X.
           02 TFC-PRIME-2        PIC 9(6).
           02 TFC-FILLER         PIC X.
           02 TFC-PRIME-3        PIC 9(6).
           02 TFC-FILLER         PIC X.
           02 TFC-POSTE-FRAIS    PIC 9(8).
           02 TFC-FILLER         PIC X.
           02 TFI-COMMUNE      PIC X(30).
           02 TFI-FILLER       PIC X.
           02 TFI-NUMERO       PIC X(10).
           02 TFI-FILLER       PIC X.
           02 TFI-DATE.
              03 TFI-ANNEE     PIC 9999.
              03 TFI-MOIS      PIC 99.
           02 TFI-FILLER       PIC X.
           02 TFI-IMPOSABLE-YN PIC XXX.
           02 TFI-FILLER       PIC X.
           02 TFI-DECOMPTE-YN  PIC XXX.
           02 TFI-FILLER       PIC X.
           02 TFI-PENSION      PIC XXX.
           02 TFI-FILLER       PIC X.
           02 TFI-ETAT-CIVIL   PIC XXX.
           02 TFI-FILLER       PIC X.
           02 TFI-TAUX         PIC ZZZ,ZZ.
           02 TFI-FILLER       PIC X.
           02 TFI-CLASSE.
              03 TFI-CLASSE-A  PIC 9.
              03 TFI-GROUPE    PIC X.
              03 TFI-CLASSE-B  PIC 99.
           02 TFI-FILLER       PIC X.
           02 TFI-ABAT-1       PIC Z(6),ZZ.
           02 TFI-FILLER       PIC X.
           02 TFI-ABAT-2       PIC Z(6),ZZ.
           02 TFI-FILLER       PIC X.
           02 TFI-ABAT-3       PIC Z(6),ZZ.
           02 TFI-FILLER       PIC X.
           02 TFI-ABAT-4       PIC Z(6),ZZ.
           02 TFI-FILLER       PIC X.
           02 TFI-ABAT-5       PIC Z(6),ZZ.
           02 TFI-FILLER       PIC X.
                 
       WORKING-STORAGE SECTION.

                 
       01  TXT-RECORD.
           02 TXT-FIRME           PIC XXXXXX  VALUE "FIRME".
           02 TXT-FILLER-1        PIC X VALUE ";".
           02 TXT-NUMBER          PIC X(6) VALUE "PERSON".
           02 TXT-FILLER-2        PIC X VALUE ";".
           02 TXT-MATCHCODE       PIC X(10) VALUE "MATCHCODE".
           02 TXT-FILLER-3        PIC X VALUE ";".
           02 TXT-PAYS            PIC XXXX VALUE "PAYS".
           02 TXT-FILLER-4        PIC X VALUE ";".
           02 TXT-CODE-POST       PIC X(5) VALUE "CPOST".
           02 TXT-FILLER-5        PIC X VALUE ";".
           02 TXT-NOM             PIC X(30) VALUE "NOM".
           02 TXT-FILLER-6        PIC X VALUE ";".
           02 TXT-PRENOM          PIC X(20) VALUE "PRENOM".
           02 TXT-FILLER-7        PIC X VALUE ";".
           02 TXT-NOM-JF          PIC X(25) VALUE "NOM JEUNE FILLE".
           02 TXT-FILLER-8        PIC X VALUE ";".
           02 TXT-PRENOM-MARI     PIC X(12) VALUE "PRENOM MARI".
           02 TXT-FILLER-9        PIC X VALUE ";".
           02 TXT-RUE             PIC X(40) VALUE "RUE".
           02 TXT-FILLER-10       PIC X VALUE ";".
           02 TXT-NUMERO          PIC X(6) VALUE "MAISON".
           02 TXT-FILLER-11       PIC X VALUE ";".
           02 TXT-LOCALITE        PIC X(40) VALUE "LOCALITE".
           02 TXT-FILLER-12       PIC X VALUE ";".
           02 TXT-CODE-SEXE       PIC X VALUE "S".
           02 TXT-FILLER-13       PIC X VALUE ";".
           02 TXT-NATIONALITE     PIC XXX VALUE "NAT".
           02 TXT-FILLER-14       PIC X VALUE ";".
           02 TXT-LANGUAGE        PIC XXXX VALUE "LANG".
           02 TXT-FILLER-15       PIC X VALUE ";".
           02 TXT-LIEU-NAISSANCE  PIC X(40) VALUE "LIEU NAISSANCE".
           02 TXT-FILLER-16       PIC X VALUE ";".
           02 TXT-PHONE-PRIVE     PIC X(20) VALUE "TELEPHONE PRIVE".
           02 TXT-FILLER-18       PIC X VALUE ";".
           02 TXT-PHONE-INTERNE   PIC X(20) VALUE "TELEPHONE INTERNE".
           02 TXT-FILLER-19       PIC X VALUE ";".
           02 TXT-PHONE-MOBILE    PIC X(20) VALUE "TELEPHONE MOBILE".
           02 TXT-FILLER-19A      PIC X VALUE ";".
           02 TXT-MALADIE-AGENCE  PIC X(12) VALUE "AGENCE".
           02 TXT-FILLER-20       PIC X VALUE ";".
           02 TXT-DATE-NAISS.
              04 TXD-NAISS-A      PIC XXXX VALUE "NAIS".
              04 TXT-FILLER-21    PIC X VALUE ";".
              04 TXD-NAISS-M      PIC XX VALUE "MO".
              04 TXT-FILLER-22    PIC X VALUE ";".
              04 TXD-NAISS-J      PIC XX VALUE "JO".
              04 TXT-FILLER-23    PIC X VALUE ";".
              04 TXD-SNOCS        PIC XXX VALUE "SNO".
           02 TXT-FILLER-24       PIC X VALUE ";".
           02 TXT-DATE-ANCIEN.
              04 TXD-ANCIEN-A     PIC XXXX VALUE "ANC".
              04 TXT-FILLER-25    PIC X VALUE ";".
              04 TXD-ANCIEN-M     PIC XX VALUE "MO".
              04 TXT-FILLER-26    PIC X VALUE ";".
              04 TXD-ANCIEN-J     PIC XX VALUE "JO".
           02 TXT-FILLER-24       PIC X VALUE ";".
           02 TXT-DATE-ENTREE.
              04 TXD-ENTREE-A     PIC XXXX VALUE "ENTR".
              04 TXT-FILLER-25    PIC X VALUE ";".
              04 TXD-ENTREE-M     PIC XX VALUE "MO".
              04 TXT-FILLER-26    PIC X VALUE ";".
              04 TXD-ENTREE-J     PIC XX VALUE "JO".
           02 TXT-FILLER-27       PIC X VALUE ";".
           02 TXT-DATE-DEP.
              04 TXD-SORTIE-A     PIC XXXX VALUE "SORT".
              04 TXT-FILLER-28    PIC X VALUE ";".
              04 TXD-SORTIE-M     PIC XX VALUE "MO".
              04 TXT-FILLER-29    PIC X VALUE ";".
              04 TXD-SORTIE-J     PIC XX VALUE "JO".
           02 TXT-FILLER-30       PIC X VALUE ";".
           02 TXT-COMPETENCE      PIC X VALUE "C".
           02 TXT-FILLER-31       PIC X VALUE ";".
           02 TXT-BADGE           PIC X(8) VALUE "BADGE".
           02 TXT-FILLER-32       PIC X VALUE ";".
           02 TXT-CONTRAT         PIC XXXX VALUE "CONT".
           02 TXT-FILLER-33       PIC X VALUE ";".
           02 TXT-BANQUE          PIC X(10) VALUE "BANQUE".
           02 TXT-FILLER-34       PIC X VALUE ";".
           02 TXT-COMPTE          PIC X(35) VALUE "COMPTE".
           02 TXT-FILLER-35       PIC X VALUE ";".
           02 TXT-POLITESSE       PIC X(25) VALUE "TITRE".
           02 TXT-FILLER-36       PIC X VALUE ";".
           02 TXC-DATE.
              03 TXC-ANNEE       PIC XXXX VALUE "CARD".
              03 TXC-MOIS        PIC XX VALUE "AT".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-STATUT         PIC X VALUE "S".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-DEPARTEMENT    PIC X(8) VALUE "COUT".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-EQUIPE         PIC XXXX VALUE "EQUI".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-METIER         PIC X(10) VALUE "METIERCODE".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-MET-NOM        PIC X(25) VALUE "METIER NOM".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-POSITION       PIC X(60) VALUE "POSITION".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-HOR-MEN        PIC XXX VALUE "H/M".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-CONGE          PIC XX VALUE "CG".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-TARIF-KM       PIC XX VALUE "KM".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-SAL-HOR        PIC X(9) VALUE "SAL HOR".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-SAL-MENS       PIC X(10) VALUE "SAL MENS".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-DEPLACEMENT    PIC X(4) VALUE "DEPL".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-JOURS-CONGE    PIC XX VALUE "JC".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-HRS-JOUR       PIC XXXXX VALUE "HRSJR".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-JRS-SEMAINE    PIC XXX VALUE "SEM".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-HRS-MOIS       PIC XXXXXX VALUE "MOIS".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-BAREME         PIC X(10) VALUE "BAREME".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-GRADE          PIC XXXXX VALUE "GRADE".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-ECHELON        PIC XXXXX VALUE "ECHEL".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-PRIME-1        PIC X(6) VALUE "PRIME1".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-PRIME-2        PIC X(6) VALUE "PRIME2".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-PRIME-3        PIC X(6) VALUE "PRIME3".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXC-POSTE-FRAIS    PIC X(8) VALUE "POSTE".
           02 TXC-FILLER         PIC X VALUE ";".
           02 TXI-COMMUNE      PIC X(30) VALUE "COMMUNE".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-NUMERO       PIC X(10) VALUE "NUMERO".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-DATE.
              03 TXI-ANNEE     PIC XXXX VALUE "FICH".
              03 TXI-MOIS      PIC XX VALUE "E".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-IMPOSABLE-YN PIC XXX VALUE "IMP".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-DECOMPTE-YN  PIC XXX VALUE "DEC".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-PENSION      PIC XXX VALUE "PEN".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-ETAT-CIVIL   PIC XXX VALUE "CIV".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-TAUX         PIC XXXXXX VALUE "TAUX".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-CLASSE.
              03 TXI-CLASSE-A  PIC XXXX VALUE "CLAS".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-ABAT-1       PIC X(9) VALUE "ABAT-FD".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-ABAT-2       PIC X(9) VALUE "ABAT-AC;".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-ABAT-3       PIC X(9) VALUE "ABAT-FO".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-ABAT-4       PIC X(9) VALUE "ABAT-DS".
           02 TXI-FILLER       PIC X VALUE ";".
           02 TXI-ABAT-4       PIC X(9) VALUE "ABAT-CE".
           02 TXI-FILLER       PIC X VALUE ";".
                              
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "CCOL.REC".
           COPY "PERSON.REC".
           COPY "CONTRAT.REC".
           COPY "CARRIERE.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "BANQP.REC".
           COPY "FICHE.REC".
           COPY "POSE.REC".
           COPY "HORSEM.REC".
           COPY "METIER.REC".
V
           COPY "V-BASES.REC".
           COPY "V-VAR.CPY".

       01  NOT-OPEN              PIC 9 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z6Z4 PIC Z(6),Z(4). 


       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TF-PERS.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-PERS.
       

           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-PERSON-END.
           PERFORM END-PROGRAM.

           
       START-PERSON.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-PERS
              WRITE TFP-RECORD FROM TXT-RECORD.
           MOVE 1 TO NOT-OPEN.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF PRES-TOT(LNK-MOIS) = 0
                 GO READ-PERSON
              END-IF
           ELSE 
              GO READ-PERSON-END
           END-IF.
           PERFORM GET-PERS.
           IF PR-NOM = SPACES
              GO READ-PERSON
           END-IF.
           PERFORM DIS-HE-01.
          
           PERFORM GET-DETAILS.
           IF CAR-STATUT = 0
              GO READ-PERSON
           END-IF.
           IF PARMOD-STATUT > 0 
           AND NOT = CAR-STATUT
              GO READ-PERSON
           END-IF.
           PERFORM FILL-FILES.
           GO READ-PERSON.
       READ-PERSON-END.
           EXIT.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 22 POSITION 1 SIZE 6
           DISPLAY PR-NOM LINE 22 POSITION 10.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 12 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 22 POSITION COL-IDX SIZE IDX LOW.
       DIS-HE-END.  
           EXIT.

        GET-DETAILS.
           INITIALIZE CON-RECORD CAR-RECORD FICHE-RECORD
           BQP-RECORD HJS-RECORD POSE-RECORD CCOL-RECORD SAVE-KEY
           BASES-REMUNERATION.
           CALL "1-GCONTR" USING LINK-V REG-RECORD CON-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           MOVE CAR-METIER TO MET-CODE
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD SAVE-KEY.
           CALL "6-GHJS" USING LINK-V HJS-RECORD  CAR-RECORD PRESENCES.
           MOVE CAR-EQUIPE TO POSE-CODE.
           CALL "6-POSE" USING LINK-V POSE-RECORD FAKE-KEY.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD FAKE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           CALL "4-SALBAS" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 POSE-RECORD 
                                 HJS-RECORD 
                                 BASES-REMUNERATION.


       FILL-FILES.
           INITIALIZE TFP-RECORD.
           INSPECT TFP-RECORD REPLACING ALL " " BY ";".
           MOVE REG-FIRME TO TFP-FIRME.
           MOVE REG-PERSON TO TFP-NUMBER.
           MOVE REG-MATCHCODE TO TFP-MATCHCODE.

           MOVE PR-PAYS TO TFP-PAYS.
           MOVE PR-CODE-POST TO TFP-CODE-POST.
           MOVE PR-NOM TO TFP-NOM.
           MOVE PR-PRENOM TO TFP-PRENOM.
           MOVE PR-NOM-JF TO TFP-NOM-JF.
           MOVE PR-PRENOM-MARI TO TFP-PRENOM-MARI.
           MOVE PR-RUE TO TFP-RUE.
           MOVE PR-MAISON TO TFP-NUMERO.
           MOVE PR-LOCALITE TO TFP-LOCALITE.
           MOVE PR-CODE-SEXE TO TFP-CODE-SEXE.
           MOVE PR-NATIONALITE TO TFP-NATIONALITE.
           MOVE PR-LANGUAGE TO TFP-LANGUAGE.
           MOVE PR-LIEU-NAISSANCE TO TFP-LIEU-NAISSANCE.
           MOVE PR-TELEPHONE TO TFP-PHONE-PRIVE.
           MOVE PR-TELEPHONE-INT TO TFP-PHONE-INTERNE.
           MOVE PR-TELEPHONE-MOB TO TFP-PHONE-MOBILE.
           MOVE PR-MALADIE-AGENCE TO TFP-MALADIE-AGENCE.
           MOVE PR-NAISS-A TO TFD-NAISS-A.
           MOVE PR-NAISS-M TO TFD-NAISS-M.
           MOVE PR-NAISS-J TO TFD-NAISS-J.
           MOVE PR-SNOCS  TO TFD-SNOCS.
           MOVE CON-DEBUT-A TO TFD-ENTREE-A.
           MOVE CON-DEBUT-M TO TFD-ENTREE-M.
           MOVE CON-DEBUT-J TO TFD-ENTREE-J.
           MOVE REG-ANCIEN-A TO TFD-ANCIEN-A.
           MOVE REG-ANCIEN-M TO TFD-ANCIEN-M.
           MOVE REG-ANCIEN-J TO TFD-ANCIEN-J.
           MOVE CON-FIN-A TO TFD-SORTIE-A.
           MOVE CON-FIN-M TO TFD-SORTIE-M.
           MOVE CON-FIN-J TO TFD-SORTIE-J.
           MOVE REG-COMPETENCE  TO TFP-COMPETENCE.
           MOVE REG-BADGE TO TFP-BADGE.
           MOVE CON-TYPE TO TFP-CONTRAT.
           MOVE BQP-BANQUE TO TFP-BANQUE.
           MOVE BQP-COMPTE TO TFP-COMPTE.
           MOVE PR-POLITESSE TO LNK-NUM.
           MOVE "P" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TFP-POLITESSE.
           MOVE CAR-DATE        TO TFC-DATE.
           MOVE CAR-COUT        TO TFC-DEPARTEMENT.
           MOVE CAR-EQUIPE      TO TFC-EQUIPE.
           MOVE CAR-METIER      TO TFC-METIER.
           MOVE MET-NOM(PR-CODE-SEXE) TO TFC-MET-NOM.
           MOVE CAR-POSITION    TO TFC-POSITION.
           MOVE CAR-STATUT      TO TFC-STATUT.
           MOVE CAR-HOR-MEN     TO TFC-HOR-MEN.
           MOVE CAR-CONGE       TO TFC-CONGE.
           MOVE CAR-TARIF-KM    TO TFC-TARIF-KM.
           MOVE BAS-HORAIRE     TO TFC-SAL-HOR.
           MOVE BAS-MOIS        TO TFC-SAL-MENS
           MOVE CAR-DEPLACEMENT TO TFC-DEPLACEMENT.
           MOVE CAR-JOURS-CONGE TO TFC-JOURS-CONGE.
           MOVE CAR-HRS-JOUR    TO TFC-HRS-JOUR.
           MOVE CAR-JRS-SEMAINE TO TFC-JRS-SEMAINE.
           MOVE CAR-HRS-MOIS    TO TFC-HRS-MOIS.
           MOVE CAR-BAREME      TO TFC-BAREME.
           MOVE CAR-GRADE       TO TFC-GRADE.
           MOVE CAR-ECHELON     TO TFC-ECHELON.
           MOVE CAR-PRIME-1 TO  TFC-PRIME-1.
           MOVE CAR-PRIME-2 TO  TFC-PRIME-2.
           MOVE CAR-PRIME-3 TO  TFC-PRIME-3.
           MOVE CAR-POSTE-FRAIS TO TFC-POSTE-FRAIS.
           MOVE FICHE-DATE TO TFI-DATE.
           MOVE FICHE-COMMUNE TO TFI-COMMUNE.
           MOVE FICHE-NUMERO  TO TFI-NUMERO.
           MOVE FICHE-IMPOSABLE-YN TO TFI-IMPOSABLE-YN.
           MOVE FICHE-DECOMPTE-YN  TO TFI-DECOMPTE-YN.
           MOVE FICHE-PENSION TO TFI-PENSION.
           MOVE FICHE-ETAT-CIVIL TO TFI-ETAT-CIVIL.
           MOVE FICHE-TAUX TO TFI-TAUX.
           MOVE FICHE-CLASSE TO TFI-CLASSE.
           MOVE FICHE-ABAT-1 TO TFI-ABAT-1.
           MOVE FICHE-ABAT-2 TO TFI-ABAT-2.
           MOVE FICHE-ABAT-3 TO TFI-ABAT-3.
           MOVE FICHE-ABAT-4 TO TFI-ABAT-4.
           MOVE FICHE-ABAT-5 TO TFI-ABAT-5.
           WRITE TFP-RECORD.
                 

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".

