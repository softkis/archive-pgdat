      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 7-SYNDIC AUGMENTATION COLLECTIVE SALAIRES   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    7-SYNDIC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL .
           COPY "BANQP.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "BANQP.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 12.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "STATUT.REC".
           COPY "COUT.REC".
           COPY "CARRIERE.REC".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  SYNDICAT              PIC X(10).
       01  MONTANT-A             PIC 9999V99 VALUE 0.
       01  MONTANT-B             PIC 9(4)V99 VALUE 0.
       01  ANNULER-YN            PIC X VALUE "N".
       01  REMPLACEMENT          PIC X(10).
       01  LIBELLE               PIC X(50).

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z2Z2 PIC ZZ,ZZ. 
           02 HE-Z4Z2 PIC Z(4),ZZ. 
           02 HE-Z6Z4 PIC Z(6),ZZZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON BANQP.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-7-SYNDIC.
       
           PERFORM AFFICHAGE-ECRAN .

           OPEN I-O   BANQP.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 12    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-10
           WHEN 11 PERFORM AVANT-11
           WHEN 12 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.

           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  9 PERFORM APRES-9
           WHEN 10 PERFORM APRES-10
           WHEN 11 PERFORM APRES-11
           WHEN 12 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT SYNDICAT 
             LINE 12 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           ACCEPT REMPLACEMENT 
             LINE 14 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT LIBELLE
             LINE 16 POSITION 30 SIZE 50
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-9.
           MOVE MONTANT-A TO HE-Z4Z2.
           ACCEPT HE-Z4Z2
             LINE 18 POSITION 30 SIZE 7
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z4Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z4Z2 TO MONTANT-A.

       AVANT-10.
           MOVE MONTANT-B TO HE-Z4Z2.
           ACCEPT HE-Z4Z2
             LINE 19 POSITION 30 SIZE 7
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z4Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z4Z2 TO MONTANT-B.

       AVANT-11.
           ACCEPT ANNULER-YN 
             LINE 21 POSITION 30 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           IF SYNDICAT = SPACES
              MOVE 1 TO INPUT-ERROR.
           INSPECT SYNDICAT CONVERTING
           "abcdefghijklmnopqrstuvwxyz굤닀뀑뙎봺뼏꽇쉸�" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZEEEEAIIOOUUUAOUAE".
           PERFORM DIS-HE-06.
       
       APRES-7.
           INSPECT REMPLACEMENT CONVERTING
           "abcdefghijklmnopqrstuvwxyz굤닀뀑뙎봺뼏꽇쉸�" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZEEEEAIIOOUUUAOUAE".
           PERFORM DIS-HE-07.
       APRES-9.
           PERFORM DIS-HE-09.
       APRES-10.
           PERFORM DIS-HE-10.

       APRES-11.
           MOVE 1 TO INPUT-ERROR.
           IF ANNULER-YN = "J" MOVE 0 TO INPUT-ERROR.
           IF ANNULER-YN = "O" MOVE 0 TO INPUT-ERROR.
           IF ANNULER-YN = "Y" MOVE 0 TO INPUT-ERROR.
           IF ANNULER-YN = "N" MOVE 0 TO INPUT-ERROR.
           PERFORM DIS-HE-11.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-1
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           PERFORM RECTIF.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.



           COPY "XDIS.CPY".
       DIS-HE-06.
           DISPLAY SYNDICAT LINE 12 POSITION 30.
       DIS-HE-07.
           DISPLAY REMPLACEMENT LINE 14 POSITION 30.
       DIS-HE-08.
           DISPLAY LIBELLE LINE 16 POSITION 30.
       DIS-HE-09.
           MOVE MONTANT-A TO HE-Z4Z2.
           DISPLAY HE-Z4Z2  LINE 18 POSITION 30.
       DIS-HE-10.
           MOVE MONTANT-B TO HE-Z4Z2.
           DISPLAY HE-Z4Z2  LINE 19 POSITION 30.
       DIS-HE-11.
           DISPLAY ANNULER-YN LINE 21 POSITION 30.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 281 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


       RECTIF.
           INITIALIZE BQP-RECORD.
           MOVE FR-KEY TO BQP-FIRME.
           MOVE REG-PERSON TO BQP-PERSON.
           MOVE 9 TO BQP-TYPE.
           START BANQP KEY > BQP-KEY INVALID 
                INITIALIZE BQP-REC-DET
                NOT INVALID 
                PERFORM RECTIF-1 THRU RECTIF-END.

       RECTIF-1.
           READ BANQP NEXT NO LOCK AT END 
               GO RECTIF-END
           END-READ.
           IF FR-KEY     NOT = BQP-FIRME
           OR REG-PERSON NOT = BQP-PERSON
           OR BQP-TYPE NOT = 9
              GO RECTIF-END
           END-IF.
           INSPECT BQP-SYNDICAT CONVERTING
           "abcdefghijklmnopqrstuvwxyz굤닀뀑뙎봺뼏꽇쉸�" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZEEEEAIIOOUUUAOUAE".
           IF BQP-SYNDICAT NOT = SYNDICAT
              GO RECTIF-1.
           IF  MONTANT-A > 0
           AND MONTANT-A NOT = BQP-PROPOSITION
              GO RECTIF-1.
           IF  BQP-FIN-A NOT = 0
           AND BQP-FIN-A < LNK-ANNEE 
              GO RECTIF-1
           END-IF.
           IF  BQP-FIN-A = LNK-ANNEE 
           AND BQP-FIN-M < LNK-MOIS 
              GO RECTIF-1
           END-IF.
           IF MONTANT-B > 0
              MOVE MONTANT-B TO BQP-PROPOSITION.
           IF ANNULER-YN NOT = "N" 
              MOVE 0 TO BQP-PROPOSITION.
           IF REMPLACEMENT NOT = SPACES
              MOVE REMPLACEMENT TO BQP-SYNDICAT
           END-IF.
           IF LIBELLE NOT = SPACES
              MOVE LIBELLE TO BQP-LIBELLE
           END-IF.
           INITIALIZE BQP-BENEFIC-NOM BQP-BENEFIC-LOC BQP-BENEFIC-RUE
           BQP-COMPTE.
           IF LNK-SQL = "Y" 
              CALL "9-BANQP" USING LINK-V BQP-RECORD WR-KEY 
           END-IF.
           REWRITE BQP-RECORD INVALID CONTINUE.
           GO RECTIF-1.
       RECTIF-END.
           EXIT.