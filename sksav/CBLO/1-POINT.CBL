      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-POINT VALEUR POINTS                       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-POINT.

       ENVIRONMENT DIVISION.
      *袴袴袴袴袴袴袴袴袴袴
       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "POINTS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "POINTS.FDE".

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  HE-INPUT-PARAMETER.
      *    LINE COL SIZE HE-SCREEN    LLCCSZHELP
           02 IP-101 PIC 9(10)  VALUE 0428040000.
           02 IP-102 PIC 9(10)  VALUE 0530020000.

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
           02 I-P OCCURS 1.
              03 I-PP OCCURS  2.
                 04 IP-LINE PIC 99.
                 04 IP-COL  PIC 99.
                 04 IP-SIZE PIC 99.
                 04 IP-HELP PIC 9999.

       01  ECRAN-SUITE.
           02 ECR-S1             PIC 999 VALUE 6.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S              PIC 999 OCCURS 3.

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE 23.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "INDEX.REC".
           COPY "V-VAR.CPY".


       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ.
           02 HE-ZT PIC ZZ,ZZZ.
           02 HE-Z2Z2 PIC ZZ,ZZ.
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z3Z5 PIC Z(3),Z(5).
           02 HE-Z2Z6 PIC Z(2),Z(6).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON POINTS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-POINT.
       
           OPEN I-O   POINTS.

           MOVE 1 TO ECRAN-IDX.
           INITIALIZE PTS-RECORD.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM POINTS-RECENT.
           PERFORM AFFICHAGE-DETAIL.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions
           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
              WHEN 1 THRU 2
                         MOVE 0000000065 TO EXC-KFR(13)
                         MOVE 6600000000 TO EXC-KFR(14)
              WHEN CHOIX-MAX(1)
                         MOVE 0000000005 TO EXC-KFR (1)
                         MOVE 0000080000 TO EXC-KFR (2)
                         MOVE 0052000000 TO EXC-KFR (11)
                         MOVE 0000680000 TO EXC-KFR (14) 
              WHEN OTHER MOVE 0000000005 TO EXC-KFR (1)
                         MOVE 0000000073 TO EXC-KFR (2)
                         MOVE 0012130000 TO EXC-KFR (3)
                         MOVE 0000000065 TO EXC-KFR (13) 
           END-IF.

           IF IP-HELP(ECRAN-IDX, INDICE-ZONE) > 0
              MOVE 1 TO EXC-KEY-FUN(1) 
           END-IF.
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-1-1 
               WHEN  2 PERFORM AVANT-1-2 
               WHEN  3 THRU 12 PERFORM AVANT-PARAM
               WHEN 13 THRU 22 PERFORM AVANT-100
               WHEN CHOIX-MAX(1) PERFORM AVANT-DEC
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 1 
              MOVE IP-HELP(ECRAN-IDX, INDICE-ZONE) TO LNK-VAL
              PERFORM HELP-SCREEN
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN.
           
           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
                WHEN  1 PERFORM APRES-1-1 
                WHEN  2 PERFORM APRES-1-2 
                WHEN  3 THRU 12 PERFORM APRES-1-ALL
                WHEN 13 THRU 22 PERFORM APRES-1-100
                WHEN CHOIX-MAX(1) PERFORM APRES-DEC
              END-EVALUATE
           END-IF.

           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 52 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 ADD 1 TO INDICE-ZONE
                WHEN 53 ADD 1 TO INDICE-ZONE.

           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.
           IF PTS-ANNEE = 0
              MOVE LNK-ANNEE TO PTS-ANNEE
           END-IF.
           ACCEPT PTS-ANNEE
           LINE     IP-LINE (ECRAN-IDX, INDICE-ZONE) 
           POSITION IP-COL  (ECRAN-IDX, INDICE-ZONE) 
           SIZE     IP-SIZE (ECRAN-IDX, INDICE-ZONE) 
           TAB UPDATE NO BEEP CURSOR  1 REVERSE
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           ACCEPT PTS-MOIS  
           LINE     IP-LINE (ECRAN-IDX, INDICE-ZONE) 
           POSITION IP-COL  (ECRAN-IDX, INDICE-ZONE) 
           SIZE     IP-SIZE (ECRAN-IDX, INDICE-ZONE) 
           TAB UPDATE NO BEEP CURSOR  1 REVERSE
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PARAM.
           COMPUTE IDX = INDICE-ZONE - 2.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE 20 TO COL-IDX.
           MOVE PTS-BASE(IDX) TO HE-Z3Z5.
           ACCEPT HE-Z3Z5 LINE LIN-IDX POSITION COL-IDX SIZE 9
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z3Z5 REPLACING ALL "." BY ",".
           MOVE HE-Z3Z5 TO PTS-BASE(IDX).

       AVANT-100.
           COMPUTE IDX = INDICE-ZONE - 12.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE 40 TO COL-IDX.
           MOVE PTS-I100(IDX) TO HE-Z2Z6.
           ACCEPT HE-Z2Z6 LINE LIN-IDX POSITION COL-IDX SIZE 9
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z2Z6 REPLACING ALL "." BY ",".
           MOVE HE-Z2Z6 TO PTS-I100(IDX).

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           EVALUATE EXC-KEY
           WHEN  65 PERFORM POINTS-PRECEDENT
           WHEN  66 PERFORM POINTS-SUIVANT
           WHEN OTHER
                    READ POINTS INVALID 
                    MOVE "AA" TO LNK-AREA
                    MOVE 13 TO LNK-NUM
                    PERFORM DISPLAY-MESSAGE
                    END-READ.
                    IF PTS-ANNEE < 1950
                       MOVE 1950 TO PTS-ANNEE
                       MOVE 1 TO INPUT-ERROR
                    END-IF
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-2.
           EVALUATE EXC-KEY
           WHEN  65 PERFORM POINTS-PRECEDENT
           WHEN  66 PERFORM POINTS-SUIVANT
           WHEN OTHER
                    IF PTS-MOIS < 1
                       MOVE 1 TO PTS-MOIS
                       MOVE 1 TO INPUT-ERROR
                    END-IF
                    IF PTS-MOIS > 12
                       MOVE 12 TO PTS-MOIS
                       MOVE 1 TO INPUT-ERROR
                    END-IF.
           IF INPUT-ERROR = 0
              READ POINTS INVALID 
              MOVE "AA" TO LNK-AREA
              MOVE 13 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              END-READ.
           MOVE PTS-ANNEE TO INDEX-ANNEE.
           MOVE PTS-MOIS  TO INDEX-MOIS.
           CALL "6-INDEX" USING LINK-V INDEX-RECORD NUL-KEY.
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-ALL.
           IF PTS-BASE(IDX) > 0
           IF PTS-I100(IDX) = 0
              MOVE 10 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM APRES-DEC
            WHEN 10 COMPUTE PTS-I100(IDX) = 
                    PTS-BASE(IDX) / INDEX-FACT
                    PERFORM DIS-E1-ALL
                    MOVE 13 TO EXC-KEY
            WHEN OTHER PERFORM DIS-E1-ALL
           END-EVALUATE.

       APRES-1-100.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM APRES-DEC
            WHEN 10 COMPUTE PTS-BASE(IDX) = 
                    PTS-I100(IDX) * INDEX-FACT + ,00005
                    PERFORM DIS-E1-ALL
                    MOVE 13 TO EXC-KEY
            WHEN OTHER PERFORM DIS-E1-ALL
           END-EVALUATE.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO PTS-TIME
                    MOVE LNK-USER TO PTS-USER
                    WRITE PTS-RECORD INVALID REWRITE PTS-RECORD 
                    END-WRITE
                    MOVE 1 TO ECRAN-IDX
                    PERFORM AFFICHAGE-ECRAN
                    PERFORM AFFICHAGE-DETAIL
                    MOVE 1 TO DECISION  
            WHEN 8  DELETE POINTS INVALID CONTINUE END-DELETE
                    PERFORM POINTS-PRECEDENT
                    MOVE 1 TO ECRAN-IDX
                    PERFORM AFFICHAGE-ECRAN
                    PERFORM AFFICHAGE-DETAIL
                    MOVE 1 TO DECISION  
            WHEN 52 COMPUTE DECISION = CHOIX-MAX(ECRAN-IDX) + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.
           
       POINTS-RECENT.
           INITIALIZE NOT-FOUND.
           MOVE 9999 TO PTS-ANNEE.
           PERFORM POINTS-PRECEDENT.

       POINTS-PRECEDENT.
           MOVE 0 TO NOT-FOUND .
           START POINTS KEY < PTS-KEY INVALID KEY
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ POINTS PREVIOUS AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.

       POINTS-SUIVANT.
           MOVE 0 TO NOT-FOUND .
           START POINTS KEY > PTS-KEY INVALID KEY
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ POINTS PREVIOUS AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.

       DIS-E1-1.
           DISPLAY PTS-ANNEE LINE IP-LINE(1, 1) POSITION IP-COL(1, 1).

       DIS-E1-2.
           MOVE PTS-MOIS TO HE-Z2.
           DISPLAY HE-Z2 LINE IP-LINE(1, 2) POSITION IP-COL(1, 2).

       DIS-E1-VAL.
           PERFORM DIS-E1-ALL VARYING IDX FROM 1 BY 1 UNTIL IDX > 10. 

       DIS-E1-ALL.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE PTS-BASE(IDX) TO HE-Z3Z5.
           DISPLAY HE-Z3Z5 LINE LIN-IDX POSITION 20.
           MOVE PTS-I100(IDX) TO HE-Z2Z6.
           DISPLAY HE-Z2Z6 LINE LIN-IDX POSITION 40.
           COMPUTE SH-00 = PTS-I100(IDX) * MOIS-IDX(LNK-MOIS).
           IF SH-00 > 0
              MOVE SH-00 TO HE-Z3Z5
              DISPLAY HE-Z3Z5 LINE LIN-IDX POSITION 55 REVERSE.


       AFFICHAGE-ECRAN.
           MOVE ECR-S(ECRAN-IDX) TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           MOVE INDICE-ZONE TO DECISION.
           EVALUATE ECRAN-IDX
               WHEN 1 PERFORM ECRAN-1 VARYING INDICE-ZONE FROM
               1 BY 1 UNTIL INDICE-ZONE = CHOIX-MAX(ECRAN-IDX)
           END-EVALUATE.
           MOVE DECISION TO INDICE-ZONE.

       ECRAN-1.
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM DIS-E1-1 
               WHEN  2 PERFORM DIS-E1-2 
               WHEN OTHER PERFORM DIS-E1-VAL 
           END-EVALUATE.

       END-PROGRAM.
           CLOSE POINTS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
