      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-CGS IMPRESSION SOLDE ANNUEL               �
      *  � CONGE FERIE RECUP REPOS HORAIRE MOBILE                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *          display "*" line 1 position 1

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CGS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM215.FC".
           COPY "TRIPR.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM215.FDE".
           COPY "TRIPR.FDE".
       FD  TF-TRANS
           RECORD VARYING DEPENDING ON MAX-LONGEUR
           DATA RECORD TF-RECORD.

       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 300 DEPENDING MAX-LONGEUR.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "215       ".
       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  MAX-LIGNES            PIC 99 VALUE 2.
       01  MAX-LONGEUR           PIC 999 VALUE 273.
       01  PRECISION             PIC 9 VALUE 0.
       01  TIRET-TEXTE           PIC X VALUE "�".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "CCOL.REC".
           COPY "IMPRLOG.REC".
           COPY "LIVRE.REC".
           COPY "LIVRE.CUM".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".
           COPY "CONGE.REC".
           COPY "PARMOD.REC".
           COPY "V-BASES.REC".
           COPY "HORSEM.REC".
           COPY "POSE.REC".
           COPY "TXACCID.REC".
           COPY "TAUXSS.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(215) OCCURS 45.

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  COMPTEUR              PIC 99 VALUE 0.
       01  CONTROLE              PIC S9(6)V99.
       01  SAL-ACT               PIC 9(7)V9(5).
       01  VAL-PP                PIC 99V99.
       01  TOTAL-YN              PIC X VALUE "N".

       01  ASCII-FILE            PIC 9 VALUE 0.
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  AJOUTE                PIC 9 VALUE 0.
       01  TABLEAU               PIC 9 VALUE 0.

       01  SOLDES.
           03 SOLDE-CG OCCURS 5.
              04 SOLDE              PIC S9999V99.

       01  TOTAUX.
           03 TOT-CG OCCURS 5.
              04 TOTAL-H            PIC S9(6)V99.
              04 TOTAL-V            PIC S9(6)V99.
              04 TOTAL-P           PIC S9(6)V99.

       01  TOTAUX-R REDEFINES TOTAUX.
           04 TOTAL-D               PIC S9(6)V99 OCCURS 15.

       01  TF-HELP  PIC X(300).

       01  T1-RECORD.
           02 T1-A.
              03 T1-FIRME  PIC 9(6).
              03 T1-DELIM0 PIC X VALUE ";".
              03 T1-PERSON PIC X(8).
              03 T1-DELIM1 PIC X VALUE ";".
              03 T1-NOM    PIC X(40).
              03 T1-DELIM2 PIC X VALUE ";".

              03 T1-FILLER PIC X(6) VALUE SPACES.
              03 T1-COUT   PIC Z(8).
              03 T1-DELIM3 PIC X VALUE ";".
              03 T1-ANNEE  PIC ZZZZZ.
              03 T1-DELIM4 PIC X VALUE ";".
              03 T1-MOIS   PIC ZZZZZ.
              03 T1-DELIM5 PIC X VALUE ";".

       01  T1-DET.                
              03 T1-HRS  PIC -Z(8),ZZ.
              03 T1-DEL1 PIC X.
              03 T1-VAL  PIC -Z(8),ZZ.
              03 T1-DEL2 PIC X.
              03 T1-PP   PIC -Z(8),ZZ.
              03 T1-DEL3 PIC X.

       01  TXT-RECORD.
           02 TXT-A.
              03 TXT-FIRME  PIC X(6).
              03 TXT-DELIM0 PIC X VALUE ";".
              03 TXT-PERSON PIC X(8).
              03 TXT-DELIM1 PIC X VALUE ";".
              03 TXT-NOM    PIC X(40).
              03 TXT-DELIM2 PIC X VALUE ";".

              03 TXT-COUT   PIC X(14).
              03 TXT-DELIM3 PIC X VALUE ";".
              03 TXT-ANNEE  PIC XXXXX.
              03 TXT-DELIM4 PIC X VALUE ";".
              03 TXT-MOIS   PIC XXXXX.
              03 TXT-DELIM5 PIC X VALUE ";".

       01  TXT-DET.                
           02 TXT-HRS  PIC X(12).
           02 TXT-DEL  PIC X VALUE ";".
           02 TXT-VAL  PIC X(12).
           02 TXT-DEL  PIC X VALUE ";".
           02 TXT-PP   PIC X(12).
           02 TXT-DEL  PIC X VALUE ";".

       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".CGS".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.

       01 HE-SEL.
          02 H-S PIC X       OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TF-TRANS
               TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CGS.
       

           INITIALIZE PARMOD-RECORD SS-RECORD TXA-RECORD.
           IF MENU-PROG-NUMBER = 1
              CALL "6-ASSACC" USING LINK-V TXA-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-SETTINGS TO HE-SEL.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE LNK-MOIS TO MOIS-FIN.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TEST-EXTENSION TOTAUX.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700009278 TO EXC-KFR(2)
                      MOVE 0000009278 TO EXC-KFR(4)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7 
           WHEN  8 PERFORM APRES-SORT
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT TOTAL-YN 
             LINE 13 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.            
           ACCEPT MOIS-FIN
             LINE 20 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PATH.
           MOVE "AA" TO LNK-AREA.
           MOVE 32 TO LNK-NUM.
           MOVE "L" TO LNK-LOW.
           PERFORM DISPLAY-MESSAGE.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 24 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           IF TOTAL-YN = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-06.

--     APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-07.

       APRES-DEC.
           MOVE 0 TO AJOUTE.
           IF EXC-KEY = 19
           OR EXC-KEY = 20
              MOVE 1 TO AJOUTE
              SUBTRACT 10 FROM EXC-KEY
           END-IF.
           IF EXC-KEY = 9 
              MOVE 1 TO TABLEAU
              MOVE 10 TO EXC-KEY
           END-IF.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           WHEN 10 MOVE 1 TO ASCII-FILE
                   PERFORM AVANT-PATH
                   PERFORM TRAITEMENT
                   PERFORM END-PROGRAM
           WHEN 68 MOVE "RR" TO LNK-AREA
                  CALL "5-SEL" USING LINK-V PARMOD-RECORD
                  MOVE PARMOD-SETTINGS TO HE-SEL
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM DISPLAY-F-KEYS
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-2
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           IF PRES-TOT(MOIS-FIN) = 0
              GO READ-PERSON-2
           END-IF.
           IF  CHOIX > 0
           AND CHOIX < 90
           AND TRIPR-CHOIX NOT = TEST-EXTENSION
              IF COUNTER > 0
              OR NOT-OPEN > 0
                 IF ASCII-FILE = 0
                    PERFORM TOT
                    PERFORM TRANSMET
                    MOVE 0 TO LIN-NUM
                 ELSE
                    PERFORM LIGNE-TOTAL
                 END-IF
              END-IF
           END-IF.
           IF CHOIX > 0
              MOVE TRIPR-CHOIX TO TEST-EXTENSION.
           INITIALIZE LAST-PERSON.
           IF ASCII-FILE = 0
              PERFORM DONNEES-PERSONNE
           ELSE
              PERFORM WRITE-TEXT
           END-IF.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY LNK-NUM.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           CALL "6-GHJS" USING LINK-V HJS-RECORD  CAR-RECORD PRESENCES.
           MOVE CAR-EQUIPE TO POSE-CODE.
           CALL "6-POSE" USING LINK-V POSE-RECORD FAKE-KEY.
           CALL "4-LPCUM" USING LINK-V REG-RECORD LCUM-RECORD.
           CALL "4-SALBAS" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 POSE-RECORD 
                                 HJS-RECORD 
                                 BASES-REMUNERATION.
           CALL "4-SALMOY" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 HJS-RECORD 
                                 BASES-REMUNERATION
                                 LCUM-RECORD.
           IF MENU-PROG-NUMBER = 1
           OR ASCII-FILE = 1
              PERFORM TAUX
           END-IF.

       TAUX.
           MOVE CAR-REGIME TO SS-REGIME.
           CALL "6-TSS" USING LINK-V SS-RECORD.
           IF FR-ACCIDENT > 0
           AND CAR-REGIME > 0
           AND CAR-REGIME < 100
              MOVE TXA-VALUE(FR-ACCIDENT) TO VAL-PP
           END-IF.
           ADD SS-PP(1) TO VAL-PP.
           ADD SS-PP(2) TO VAL-PP.
           ADD SS-PP(3) TO VAL-PP.
           ADD SS-PP(4) TO VAL-PP.
           ADD SS-PP(5) TO VAL-PP.
           ADD SS-PP(6) TO VAL-PP.
           ADD SS-PP(7) TO VAL-PP.
           ADD SS-PP(8) TO VAL-PP.
           ADD SS-PP(9) TO VAL-PP.
           ADD SS-PP(10) TO VAL-PP.


       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       ENTETE.
           ADD 1 TO PAGE-NUMBER.
           MOVE 4 TO CAR-NUM.
           MOVE 3 TO LIN-NUM.
           MOVE 123 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  2 TO LIN-NUM.
           MOVE 117 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 120 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 123 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           IF COUT-NUMBER NOT = 0
              MOVE 3 TO LIN-NUM
              MOVE 8 TO COL-NUM
              MOVE COUT-NOM TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE COUT-NUMBER TO VH-00 
              MOVE 4 TO CAR-NUM
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM.

           MOVE  2 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           IF STATUT NOT = 0
              MOVE "-" TO ALPHA-TEXTE
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM
              MOVE STAT-NOM TO ALPHA-TEXTE
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           
           MOVE FR-PAYS TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 2 TO LIN-NUM.
           MOVE 70 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE LNK-ANNEE TO VH-00.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE MOIS-FIN TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           PERFORM FILL-FORM.

           IF CHOIX NOT = 0 
              PERFORM DETAIL-CHOIX.

       DETAIL-CHOIX.
           MOVE  4 TO LIN-NUM.
           MOVE 65 TO COL-NUM.
           MOVE TEST-NUMBER TO LNK-POSITION.
           MOVE TEST-ALPHA  TO LNK-TEXT.
           MOVE CHOIX       TO LNK-NUM.
           CALL "4-CHOIX" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

        DONNEES-PERSONNE.
           PERFORM DIS-HE-01.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE IDX = LIN-NUM + 4.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           ADD 1 TO LIN-NUM.
           MOVE REG-PERSON TO LAST-PERSON.
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  3 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           INITIALIZE CONGE-RECORD SOLDES L-RECORD LNK-SUITE.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD FAKE-KEY.
           PERFORM LOAD-REPORT VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           PERFORM CONGE VARYING IDX FROM 1 BY 1 UNTIL IDX > MOIS-FIN.
           ADD CONGE-AJUSTE TO SOLDE(1).
           PERFORM LP VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > MOIS-FIN.
           PERFORM SOLDES VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.

       SOLDES.
           IF H-S(IDX) NOT = "N"
              MOVE SOLDE(IDX) TO VH-00
              ADD SOLDE(IDX) TO TOTAL-H(IDX)
              COMPUTE COL-NUM = 25 + IDX * 30
              MOVE  4 TO CAR-NUM
              MOVE  2 TO DEC-NUM
              PERFORM FILL-FORM
              IF LNK-COMPETENCE >= REG-COMPETENCE
                 MOVE BAS-SAL(1) TO SAL-ACT
                 IF IDX = 1
                    IF CAR-CONGE = 1 OR 2
                       ADD BAS-SAL(9) TO SAL-ACT
                    END-IF
                 END-IF
                 COMPUTE VH-00 = SOLDE(IDX) * SAL-ACT 
                 MOVE VH-00 TO CONTROLE
                 COMPUTE COL-NUM = 34 + IDX * 30
                 MOVE  6 TO CAR-NUM
                 MOVE  2 TO DEC-NUM
                 COMPUTE SH-00 = VH-00 * VAL-PP / 100
                 PERFORM FILL-FORM
                 ADD CONTROLE TO TOTAL-V(IDX)
                 ADD SH-00    TO TOTAL-P(IDX)
             END-IF
           END-IF.

        TOT.
           ADD 1 TO LIN-NUM.
           PERFORM TOTS VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           IF MENU-PROG-NUMBER = 1
              ADD 1 TO LIN-NUM
              PERFORM PP VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.

           INITIALIZE TOTAUX.

       TOTS.
           IF H-S(IDX) NOT = "N"
              MOVE TOTAL-H(IDX) TO VH-00
              COMPUTE COL-NUM = 23 + IDX * 30
              MOVE  6 TO CAR-NUM
              MOVE  2 TO DEC-NUM
              PERFORM FILL-FORM
              MOVE TOTAL-V(IDX) TO VH-00 
              COMPUTE COL-NUM = 33 + IDX * 30
              MOVE  7 TO CAR-NUM
              MOVE  2 TO DEC-NUM
              PERFORM FILL-FORM
           END-IF.

       PP.
           IF H-S(IDX) NOT = "N"
              MOVE TOTAL-P(IDX) TO VH-00 
              COMPUTE COL-NUM = 33 + IDX * 30
              MOVE  7 TO CAR-NUM
              MOVE  2 TO DEC-NUM
              PERFORM FILL-FORM
              ADD 1 TO LIN-NUM
              COMPUTE VH-00 = TOTAL-P(IDX) + TOTAL-V(IDX)
              COMPUTE COL-NUM = 33 + IDX * 30
              MOVE  7 TO CAR-NUM
              MOVE  2 TO DEC-NUM
              PERFORM FILL-FORM
              SUBTRACT 1 FROM LIN-NUM
           END-IF.

       LOAD-REPORT.
           MOVE CONGE-IDX(IDX) TO SOLDE(IDX) VH-00.
           IF CONGE-IDX(IDX) NOT = 0
           AND H-S(IDX) NOT = "N"
               COMPUTE COL-NUM = 14 + IDX * 30
               MOVE "(" TO ALPHA-TEXTE
               PERFORM FILL-FORM
               MOVE  4 TO CAR-NUM
               MOVE  2 TO DEC-NUM
               PERFORM FILL-FORM
               MOVE ")" TO ALPHA-TEXTE
               PERFORM FILL-FORM
           END-IF.

       CONGE.
           ADD CONGE-MOIS(IDX) TO SOLDE(1).

       LP.
           INITIALIZE L-RECORD LNK-SUITE.
           MOVE IDX-4 TO L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-MOIS NOT = 0
              PERFORM CALCUL VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.

       CALCUL.
           COMPUTE IDX-1 = 10 + IDX.
           COMPUTE IDX-2 = 20 + IDX.
           IF IDX = 1 ADD L-UNI-CONGE-PLUS TO SOLDE(IDX)
                      MOVE 0 TO SH-00
                      SUBTRACT L-UNI-CONGE FROM SH-00
                      SUBTRACT L-UNI-CONGE-MIN FROM SH-00
           ELSE
              COMPUTE SH-00 = L-UNITE-ST(IDX-1) - L-UNITE-ST(IDX-2).
           IF IDX = 2 SUBTRACT L-UNI-FERIE     FROM SH-00
                      SUBTRACT L-UNI-FERIE-R   FROM SH-00.
           IF IDX = 3 SUBTRACT L-UNI-RECUP     FROM SH-00.
           IF IDX = 4 SUBTRACT L-UNI-REPOS-C   FROM SH-00.
           ADD SH-00 TO SOLDE(IDX).

       AVANT-SORT.
           ACCEPT CHOIX
             LINE 22 POSITION 20 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
             PERFORM DIS-SORT.

       APRES-SORT.
           MOVE CHOIX TO MS-NUMBER.
           EVALUATE EXC-KEY
           WHEN   2 MOVE "SO" TO LNK-AREA
                    CALL "2-MESS" USING LINK-V MS-RECORD
                    MOVE LNK-VAL TO CHOIX
                    CANCEL "2-MESS"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN  65 THRU 66 PERFORM NEXT-MESSAGES
           END-EVALUATE.
           MOVE MS-NUMBER TO CHOIX.

       DIS-SORT.
           MOVE CHOIX TO LNK-NUM HE-Z2.
           DISPLAY HE-Z2 LINE 22 POSITION 20.
           MOVE "SO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           DISPLAY LNK-TEXT LINE 22 POSITION 23 SIZE 30.
           MOVE "AA" TO LNK-AREA.

       CALL-SORT.
           MOVE A-N TO LNK-A-N.
           MOVE PRECISION TO LNK-PRESENCE.
           CALL "4-SORT" USING LINK-V
                               REG-RECORD
                               END-NUMBER
                               END-MATCHCODE
                               STATUT
                               CHOIX.
           CANCEL "4-SORT".

       TRAITEMENT.
           IF CHOIX NOT = 0
              PERFORM CALL-SORT 
              MOVE LNK-USER  TO USER-TRIPR
              MOVE FR-KEY TO REG-FIRME FIRME-TRIPR
              OPEN I-O TRIPR
              PERFORM START-TRI
              PERFORM READ-TRI THRU READ-TRI-END
              PERFORM END-PROGRAM
           END-IF. 
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.
           PERFORM END-PROGRAM.

       START-TRI.
           INITIALIZE TRIPR-RECORD.
           START TRIPR KEY >  TRIPR-KEY INVALID
              PERFORM END-PROGRAM.

       READ-TRI.
           READ TRIPR NEXT AT END 
              GO READ-TRI-END
           END-READ.
           MOVE TRIPR-PERSON TO REG-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           PERFORM GET-PERS.
           PERFORM PRESENCE.
           PERFORM READ-PERSON-1 THRU READ-PERSON-2.
           GO READ-TRI.
       READ-TRI-END.
           CLOSE TRIPR.
           DELETE FILE TRIPR.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.


       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L215" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.



       WRITE-TEXT.
           IF NOT-OPEN = 0
              IF AJOUTE = 1
                 OPEN EXTEND TF-TRANS
              ELSE
                 OPEN OUTPUT TF-TRANS
              END-IF
              MOVE 1 TO NOT-OPEN
              IF TABLEAU = 0
              AND AJOUTE = 0
                 PERFORM HEAD-LINE
                 WRITE TF-RECORD FROM TF-HELP
              END-IF
           END-IF.
           PERFORM T-LINE.

       T-LINE.
           MOVE LNK-ANNEE TO T1-ANNEE.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO T1-NOM.
           MOVE CAR-COUT TO T1-COUT.
           MOVE FR-KEY TO T1-FIRME.
           MOVE REG-PERSON TO HE-Z8.
           MOVE HE-Z8 TO T1-PERSON.
           MOVE MOIS-FIN TO T1-MOIS.
           INITIALIZE CONGE-RECORD SOLDES L-RECORD LNK-SUITE.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD FAKE-KEY.
           PERFORM LOAD-REPORT VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           PERFORM CONGE VARYING IDX FROM 1 BY 1 UNTIL IDX > MOIS-FIN.
           ADD CONGE-AJUSTE TO SOLDE(1).
           PERFORM LP VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > MOIS-FIN.
           MOVE 1 TO MAX-LONGEUR.
           STRING T1-RECORD DELIMITED BY SIZE INTO TF-HELP
           POINTER MAX-LONGEUR ON OVERFLOW CONTINUE END-STRING.
           PERFORM LIGNE VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           IF TOTAL-YN = "N"
              WRITE TF-RECORD FROM TF-HELP.


       HEAD-LINE.
           MOVE "AA" TO LNK-AREA.
           MOVE 117 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-ANNEE.
           MOVE "AY" TO LNK-AREA.
           MOVE  5  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-PERSON.
           MOVE "FI" TO LNK-AREA.
           MOVE  1  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-FIRME.
           MOVE "PR" TO LNK-AREA.
           MOVE  3  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-NOM.
           MOVE "FR" TO LNK-AREA.
           MOVE  25 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-COUT.
           MOVE 114 TO LNK-NUM.
           MOVE "AA" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-MOIS.
           MOVE 1 TO MAX-LONGEUR.
           STRING TXT-RECORD DELIMITED BY SIZE INTO TF-HELP
           POINTER MAX-LONGEUR ON OVERFLOW CONTINUE END-STRING.
           PERFORM TEXTE-LIGNE VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.

       TEXTE-LIGNE.
           IF H-S(IDX) NOT = "N"
              MOVE IDX TO LNK-NUM
              MOVE "RR" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO TXT-HRS
              MOVE 0 TO LNK-NUM
              MOVE "VV" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO TXT-VAL
              MOVE 9 TO LNK-NUM
              MOVE "FC" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO TXT-PP
              STRING TXT-DET DELIMITED BY SIZE INTO TF-HELP
              POINTER MAX-LONGEUR ON OVERFLOW CONTINUE END-STRING
           END-IF.

       LIGNE.
           IF H-S(IDX) NOT = "N"
              MOVE SOLDE(IDX) TO T1-HRS
              ADD SOLDE(IDX) TO TOTAL-H(IDX)
              MOVE ";" TO T1-DEL1 T1-DEL2 T1-DEL3
              IF LNK-COMPETENCE >= REG-COMPETENCE
                 MOVE BAS-SAL(1) TO SAL-ACT
                 IF IDX = 1
                    IF CAR-CONGE = 1 OR 2
                       ADD BAS-SAL(9) TO SAL-ACT
                    END-IF
                 END-IF
                 COMPUTE VH-00 = SOLDE(IDX) * SAL-ACT 
                 MOVE VH-00 TO T1-VAL
                 ADD VH-00 TO TOTAL-V(IDX)
                 COMPUTE SH-00 = VH-00 * VAL-PP / 100
                 MOVE SH-00 TO T1-PP
                 ADD SH-00 TO TOTAL-P(IDX)
             END-IF
             STRING T1-DET DELIMITED BY SIZE INTO TF-HELP
             POINTER MAX-LONGEUR ON OVERFLOW CONTINUE END-STRING
           END-IF.

       LIGNE-TOTAL.
           MOVE LNK-ANNEE TO T1-ANNEE.
           MOVE "TOTAL" TO T1-PERSON.
           INITIALIZE T1-COUT T1-NOM.
           MOVE MOIS-FIN TO T1-MOIS.
           IF CHOIX = 1
              MOVE FR-KEY TO COUT-FIRME 
              MOVE TEST-NUMBER TO COUT-NUMBER T1-COUT
              CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY
              MOVE COUT-NOM TO T1-NOM
           END-IF.
           MOVE FR-KEY TO T1-FIRME.
           MOVE 1 TO MAX-LONGEUR.
           STRING T1-RECORD DELIMITED BY SIZE INTO TF-HELP
           POINTER MAX-LONGEUR ON OVERFLOW CONTINUE END-STRING.
           PERFORM LIGNE-SOLDE VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           WRITE TF-RECORD FROM TF-HELP.
           INITIALIZE TOTAUX.

       LIGNE-SOLDE.
           IF H-S(IDX) NOT = "N"
              MOVE TOTAL-H(IDX) TO T1-HRS
              MOVE TOTAL-V(IDX) TO T1-VAL
              MOVE TOTAL-P(IDX) TO T1-PP
              STRING T1-DET DELIMITED BY SIZE INTO TF-HELP
              POINTER MAX-LONGEUR ON OVERFLOW CONTINUE END-STRING
           END-IF.

           COPY "XDIS.CPY".
       DIS-HE-06.
           DISPLAY TOTAL-YN LINE 13 POSITION 32.
       DIS-HE-07.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 35.
           MOVE "MO" TO LNK-AREA.
           MOVE 20401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 1352 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE 13 TO LIN-IDX.
           MOVE 0 TO COMPTEUR.
           PERFORM AFFICHE-D THRU AFFICHE-END.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       AFFICHE-D.
           MOVE COMPTEUR TO LNK-NUM.
           ADD 1 TO LIN-IDX LNK-NUM.
           MOVE "RR" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           IF LNK-TEXT = SPACES
              GO AFFICHE-END.
           ADD 1 TO COMPTEUR.
           IF H-S(COMPTEUR) NOT = "N"
              DISPLAY LNK-TEXT LINE LIN-IDX POSITION 53 SIZE 15 REVERSE
           ELSE
              DISPLAY LNK-TEXT LINE LIN-IDX POSITION 55 SIZE 15 LOW.
           GO AFFICHE-D.
       AFFICHE-END.
           EXIT.
       
       END-PROGRAM.
           IF COUNTER > 0 
              PERFORM TOT
              PERFORM TRANSMET
              MOVE 99 TO LNK-VAL
              CALL "L215" USING LINK-V FORMULAIRE
           END-IF.
           CANCEL "L215".
           IF NOT-OPEN = 1
           AND TABLEAU = 0
           AND AJOUTE  = 0
              PERFORM LIGNE-TOTAL.
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".

