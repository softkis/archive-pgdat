      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-COUT GESTION DES CENTRES DE COUT          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-COUT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "COUT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "COUT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PLANDEF.REC".
           COPY "PARMOD.REC".

       01  CHOIX-MAX             PIC 99 VALUE 9. 

       01   ECR-DISPLAY.
            02 HE-Z4 PIC ZZZZ.
            02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON COUTS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-1-COUT.

           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           OPEN I-O COUTS.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3     MOVE 0000680000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 THRU 8 PERFORM AVANT-PLAN
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 THRU 8 PERFORM APRES-PLAN 
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 52 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT COUT-NUMBER
             LINE  3 POSITION 30 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT COUT-NOM
             LINE  4 POSITION 30 SIZE 50
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           ACCEPT COUT-ANALYTIC
             LINE  5 POSITION 30 SIZE 20
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-RULER.
           ACCEPT PARMOD-SETTINGS
             LINE  6 POSITION 30 SIZE 20
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
           DISPLAY PARMOD-SETTINGS LINE 6 POSITION 30 LOW.

       AVANT-PLAN.
           COMPUTE IDX = INDICE-ZONE - 3.
           COMPUTE LIN-IDX = IDX + 7.
           ACCEPT COUT-PLAN(IDX)
             LINE LIN-IDX POSITION 34 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.



      *AVANT-VAL.
      *    COMPUTE LIN-IDX = 3 + INDICE-ZONE.
      *    COMPUTE IDX = INDICE-ZONE - 4.
      *    ACCEPT COUT-VAL(IDX)
      *      LINE  LIN-IDX POSITION 20 SIZE  8
      *      TAB UPDATE NO BEEP CURSOR  1
      *      ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE FR-KEY TO COUT-FIRME.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-COUT
           WHEN   2 CALL "2-COUT" USING LINK-V COUT-RECORD
                    CANCEL "2-COUT"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM DIS-HE-01 THRU DIS-HE-END
           WHEN   8 INITIALIZE COUT-RECORD.
           PERFORM READ-COUT.
           PERFORM AFFICHAGE-DETAIL.
           IF COUT-NUMBER = 0
              MOVE 1 TO INPUT-ERROR
              IF COUT-FIRME NOT = 0
                 DELETE COUTS INVALID CONTINUE END-DELETE
              END-IF
           END-IF.

       APRES-3.
           EVALUATE EXC-KEY
              WHEN  68 PERFORM AVANT-RULER
              MOVE 1 TO INPUT-ERROR
           END-EVALUATE.

       APRES-PLAN.
           PERFORM DIS-PLAN.

       APRES-DEC.
           MOVE FR-KEY TO COUT-FIRME.
           EVALUATE EXC-KEY 
            WHEN  5 CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO COUT-TIME
                    MOVE LNK-USER TO COUT-USER
                    IF LNK-SQL = "Y" 
                      CALL "9-COUT" USING LINK-V COUT-RECORD WR-KEY 
                    END-IF
                    WRITE COUT-RECORD INVALID 
                        REWRITE COUT-RECORD
                    END-WRITE   
                    MOVE 1 TO DECISION
                    MOVE COUT-NUMBER TO LNK-VAL
            WHEN  8 DELETE COUTS INVALID CONTINUE END-DELETE
                    IF LNK-SQL = "Y" 
                      CALL "9-COUT" USING LINK-V COUT-RECORD DEL-KEY 
                    END-IF
                    INITIALIZE COUT-RECORD
                    PERFORM AFFICHAGE-DETAIL
                    MOVE 1 TO DECISION
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
               MOVE CHOIX-MAX TO INDICE-ZONE.
           

       READ-COUT.
           MOVE EXC-KEY TO SAVE-KEY.
           MOVE 13 TO EXC-KEY.
           PERFORM NEXT-COUT.
           MOVE SAVE-KEY TO EXC-KEY.

       NEXT-COUT.
           CALL "6-COUT" USING LINK-V COUT-RECORD EXC-KEY.

       DIS-HE-01.
           MOVE COUT-NUMBER TO LNK-VAL HE-Z8.
           DISPLAY HE-Z8  LINE  3 POSITION 30.
       DIS-HE-02.
           DISPLAY COUT-NOM  LINE  4 POSITION 30.
       DIS-HE-03.
           DISPLAY COUT-ANALYTIC LINE  5 POSITION 30.
       DIS-HE-04.
           PERFORM DIS-PLAN VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
       DIS-HE-END.
           EXIT.

       DIS-PLAN.
           COMPUTE LIN-IDX = 7 + IDX.
           DISPLAY COUT-PLAN(IDX) LINE LIN-IDX POSITION 34.
           MOVE COUT-PLAN(IDX) TO PLANDEF-KEY.
           CALL "6-PLANDF" USING LINK-V PLANDEF-RECORD "N" FAKE-KEY.
           DISPLAY PLANDEF-NOM LINE LIN-IDX POSITION 40 SIZE 40.

       AFFICHAGE-ECRAN.
           MOVE 130 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY PARMOD-SETTINGS LINE 6 POSITION 30 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE COUTS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
             IF DECISION = 99 
                DISPLAY COUT-USER LINE 24 POSITION 5
                DISPLAY COUT-ST-JOUR  LINE 24 POSITION 15
                DISPLAY COUT-ST-MOIS  LINE 24 POSITION 18
                DISPLAY COUT-ST-ANNEE LINE 24 POSITION 21
                DISPLAY COUT-ST-HEURE LINE 24 POSITION 30
                DISPLAY COUT-ST-MIN   LINE 24 POSITION 33
                GO AVANT-DEC
             END-IF.
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
