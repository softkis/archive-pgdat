      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-SDFAUT SAMEDI DIMANCHE FERIE AUTOMATIQUE  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-SDFAUT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "OCCUP.REC".
           COPY "OCCOM.REC".
             

           COPY "V-VAR.CPY".
        
       01  FLAG-POSTE            PIC 9 VALUE 0.
       01  JOUR                  PIC 99.
       01  NOT-OPEN              PIC 9 VALUE 0.

       01 SAV-RECORD.
          02 SAV-KEY.
             03 SAV-FIRME          PIC 9(6).
             03 SAV-MOIS           PIC 99.
             03 SAV-PERSON         PIC 9(8).
             03 SAV-OCCUPATION     PIC 99.
             03 SAV-POSTE          PIC 9(8).
             03 SAV-EXTENSION      PIC X(50).
             03 SAV-COMPLEMENT     PIC 99.

          02 SAV-KEY-1.
             03 SAV-FIRME-1        PIC 9(6).
             03 SAV-MOIS-1         PIC 99.
             03 SAV-OCCUPATION-1   PIC 99.
             03 SAV-COMPLEMENT-1   PIC 99.
             03 SAV-POSTE-1        PIC 9(8).
             03 SAV-EXTENSION-1    PIC X(50).
             03 SAV-PERSON-1       PIC 9(8).


          02 SAV-KEY-2.
             03 SAV-FIRME-2        PIC 9(6).
             03 SAV-PERSON-2       PIC 9(8).
             03 SAV-MOIS-2         PIC 99.
             03 SAV-OCCUPATION-2   PIC 99.
             03 SAV-POSTE-2        PIC 9(8).
             03 SAV-EXTENSION-2    PIC X(50).
             03 SAV-COMPLEMENT-2   PIC 99.

          02 SAV-KEY-3.
             03 SAV-FIRME-3        PIC 9(6).
             03 SAV-OCCUPATION-3   PIC 99.
             03 SAV-POSTE-3        PIC 9(8).
             03 SAV-EXTENSION-3    PIC X(50).
             03 SAV-COMPLEMENT-3   PIC 99.
             03 SAV-MOIS-3         PIC 99.
             03 SAV-PERSON-3       PIC 9(8).
              
          02 SAV-REC-DET.
             03 SAV-HEURES.
                04 SAV-HRS PIC S9(6)V99 COMP-3 OCCURS 32.
             03 SAV-VALEUR REDEFINES SAV-HEURES.
                04 SAV-VAL PIC S9(8) COMP-3 OCCURS 32.
             03 SAV-JOURS  PIC 99.
             03 SAV-STAMP.
                04 SAV-TIME.
                   05 SAV-ST-ANNEE PIC 9999.
                   05 SAV-ST-MOIS  PIC 99.
                   05 SAV-ST-JOUR  PIC 99.
                   05 SAV-ST-HEURE PIC 99.
                   05 SAV-ST-MIN   PIC 99.
                04 SAV-USER        PIC X(10).

      *    VARIABLES SERVANT DANS LA DEFINITION DES CS PAR CALENDRIER
       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999 VALUE 0.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".
           COPY "CALEN.REC".


       PROCEDURE DIVISION USING LINK-V 
                                REG-RECORD  CAR-RECORD
                                CCOL-RECORD CAL-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-SDFAUT.
       

           IF  CCOL-DIM-AUTOMAT = "N"
           AND CCOL-FER-AUTOMAT = "N"
           AND CCOL-SAM-AUTOMAT = "N"
               EXIT PROGRAM.

           IF ANNEE-JOURS NOT = 0 
           AND LNK-SUFFIX NOT = ANNEE-JOURS
              CLOSE JOURS
              MOVE 0 TO ANNEE-JOURS
           END-IF.

           IF ANNEE-JOURS = 0
              MOVE LNK-SUFFIX TO ANNEE-JOURS
              OPEN I-O JOURS
           END-IF.

           INITIALIZE JRS-RECORD.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE LNK-MOIS   TO JRS-MOIS.
           MOVE REG-PERSON TO JRS-PERSON.
           START JOURS KEY >= JRS-KEY INVALID EXIT PROGRAM
                NOT INVALID
                PERFORM READ-HEURES THRU READ-HEURES-END.


       READ-HEURES.
           READ JOURS NEXT NO LOCK AT END
               GO READ-HEURES-END
           END-READ.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
              GO READ-HEURES-END
           END-IF.
           IF JRS-POSTE > 0
              MOVE 1 TO FLAG-POSTE
           END-IF.
           IF JRS-OCCUPATION = 99
              GO READ-HEURES-END
           END-IF.
           IF JRS-COMPLEMENT = 0
              MOVE JRS-OCCUPATION TO OCC-KEY
              CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY
              PERFORM TESTS
           END-IF.
           GO READ-HEURES.
       READ-HEURES-END.
           IF FLAG-POSTE > 0
              CALL "4-HPADD" USING LINK-V.
           EXIT PROGRAM.


       TESTS.
           MOVE JRS-RECORD TO SAV-RECORD.
           INITIALIZE JRS-REC-DET.
           IF CCOL-FER-AUTOMAT NOT = "N"
           AND CCOL-CALENDRIER > 0
              MOVE 11 TO JRS-COMPLEMENT 
              PERFORM DEL-JOURS
              IF CCOL-TAUX-LIBRE(1) > 0
              AND OCC-FERIE > 0
                 PERFORM FERIES VARYING JOUR FROM 1 BY 1 UNTIL JOUR > 
                 MOIS-JRS(LNK-MOIS)
              END-IF
              IF JRS-HRS(32) > 0
                 MOVE 11 TO JRS-COMPLEMENT   JRS-COMPLEMENT-1 
                            JRS-COMPLEMENT-2 JRS-COMPLEMENT-3 
                 PERFORM WRITE-JOURS
              END-IF
           END-IF.
           INITIALIZE JRS-REC-DET.
           IF CCOL-SAM-AUTOMAT NOT = "N"
              MOVE 19 TO JRS-COMPLEMENT 
              PERFORM DEL-JOURS
              IF CCOL-TAUX-LIBRE(9) > 0
                 PERFORM SAMEDIS VARYING JOUR FROM 1 BY 1 UNTIL JOUR > 
                 MOIS-JRS(LNK-MOIS)
              END-IF
              IF JRS-HRS(32) > 0
                 MOVE 19 TO JRS-COMPLEMENT   JRS-COMPLEMENT-1 
                            JRS-COMPLEMENT-2 JRS-COMPLEMENT-3 
                 PERFORM WRITE-JOURS
              END-IF
           END-IF.
           INITIALIZE JRS-REC-DET.
           IF CCOL-DIM-AUTOMAT NOT = "N"
              MOVE 20 TO JRS-COMPLEMENT 
              PERFORM DEL-JOURS
              IF CCOL-TAUX-LIBRE(10) > 0
              AND OCC-DIM > 0
                 PERFORM DIMANCHES VARYING JOUR FROM 1 BY 1 UNTIL JOUR > 
                 MOIS-JRS(LNK-MOIS)
              END-IF
              IF JRS-HRS(32) > 0
                 MOVE 20 TO JRS-COMPLEMENT   JRS-COMPLEMENT-1 
                            JRS-COMPLEMENT-2 JRS-COMPLEMENT-3 
                 PERFORM WRITE-JOURS
              END-IF
           END-IF.
           
       DIMANCHES.
           IF SEM-IDX(LNK-MOIS, JOUR) = 7
           AND SAV-HRS(JOUR) > 0
              ADD SAV-HRS(JOUR) TO JRS-HRS(JOUR) JRS-HRS(32) 
           END-IF.

       SAMEDIS.
           IF SEM-IDX(LNK-MOIS, JOUR) = 6
           AND SAV-HRS(JOUR) > 0
              ADD SAV-HRS(JOUR) TO JRS-HRS(JOUR) JRS-HRS(32) 
           END-IF.

       FERIES.
           IF CAL-JOUR(LNK-MOIS, JOUR) = 1
           AND SAV-HRS(JOUR) > 0
              ADD SAV-HRS(JOUR) TO JRS-HRS(JOUR) JRS-HRS(32) 
           END-IF.

       DEL-JOURS.
           DELETE JOURS INVALID CONTINUE.
           IF LNK-SQL = "Y"
              CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY
           END-IF.

       WRITE-JOURS.
           WRITE JRS-RECORD INVALID REWRITE JRS-RECORD.
           IF LNK-SQL = "Y"
              CALL "9-JOURS" USING LINK-V JRS-RECORD WR-KEY
           END-IF.
