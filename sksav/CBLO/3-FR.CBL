      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-FR IMPRESSION LISTE FIRMES                �
      *  � AVEC DETAILS                                          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-FR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM130.FDE".
       FD  TF-TRANS
           RECORD VARYING DEPENDING ON IDX-4
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 1500 DEPENDING IDX-4.


       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "130       ".
       01  CHOIX-MAX             PIC 99 VALUE 18.
       01  MAX-LIGNES            PIC 99 VALUE 10.
       01  PRECISION             PIC 9 VALUE 1.
       01  NEXT-LINE             PIC 9 VALUE 0.
       01  TIRET-TEXTE           PIC X VALUE "�".
       01  TXT-LIMIT             PIC XX VALUE ";|".
       01  TXT-SC                PIC X VALUE ";".
       01  TXT-CODE              PIC X(11) VALUE "CODE      ;".
       01  NUM-CODE              PIC X(9) VALUE "CODE    ;".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".
           COPY "ECHEANCE.REC".
           COPY "PARMOD.REC".
           COPY "PAYS.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.
       01  LIMITEUR              PIC X(6).
       01  COMPTEUR              PIC 99.
       01  SEL-ERROR             PIC 9.
       01  PAYS                  PIC X(10) VALUE SPACES.

       01  TXT-RECORD            PIC X(1500).

       01  ASCII-FILE            PIC 9 VALUE 0.
       01  TABLEAU               PIC 9 VALUE 0.
       01  NOT-OPEN              PIC 9 VALUE 0.

       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".FL1".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6)  VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-FIRME            PIC 9(6).
       01  CHOIX                 PIC 99 VALUE 0.

       01  TOUS                  PIC 9 VALUE 0.

       01  HE-SEL.
           02 H-S PIC X       OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-ZM PIC ZZZZZZ,ZZ.
           02 HE-ZJ PIC ZZZZ,ZZZZ.
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TF-TRANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-FR.
       
           MOVE FR-KEY TO SAVE-FIRME.
           MOVE LNK-MOIS TO SAVE-MOIS.
           MOVE LNK-LANGUAGE TO FORM-LANGUE SAVE-LANGUAGE.
           INITIALIZE PARMOD-RECORD TEST-EXTENSION.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-SETTINGS TO HE-SEL.
           MOVE 0 TO FR-KEY.
           MOVE 66 TO EXC-KEY.
      *    MOVE "N" TO A-N.
      *    PERFORM NEXT-FIRME.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN CHOIX-MAX
                      MOVE 0000000025 TO EXC-KFR(1)
      *               MOVE 1700009278 TO EXC-KFR(2)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0000000025 TO EXC-KFR(3)
                      MOVE 1700000000 TO EXC-KFR(4)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN CHOIX-MAX PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 50.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-01 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN CHOIX-MAX PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-PATH.
           MOVE "AA" TO LNK-AREA.
           MOVE 32 TO LNK-NUM.
           MOVE "L" TO LNK-LOW.
           PERFORM DISPLAY-MESSAGE.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 24 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-01.
           IF  A-N NOT = "A"
           AND A-N NOT = "N"
               MOVE 1 TO INPUT-ERROR
               MOVE "N" TO A-N.
           MOVE A-N TO LNK-A-N.
           PERFORM NEXT-FIRME.

       APRES-DEC.
           MOVE 0 TO TOUS.
           IF EXC-KEY = 9 
              MOVE 1 TO TABLEAU
              MOVE 10 TO EXC-KEY
           END-IF.
           IF EXC-KEY = 15
           OR EXC-KEY = 16
              MOVE 1 TO TOUS
              SUBTRACT 10 FROM EXC-KEY
           END-IF.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.

           EVALUATE EXC-KEY 
           WHEN 5  PERFORM TRAITEMENT
                   PERFORM END-PROGRAM
           WHEN 10 MOVE 1 TO ASCII-FILE
                   PERFORM AVANT-PATH
                   PERFORM TRAITEMENT
                   PERFORM END-PROGRAM
           WHEN 68 MOVE "F1" TO LNK-AREA
                   CALL "5-SEL" USING LINK-V PARMOD-RECORD
                   MOVE PARMOD-SETTINGS TO HE-SEL
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-PAYS.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD EXC-KEY.

       TRAITEMENT.
           MOVE 0 TO INPUT-ERROR.
           PERFORM START-FIRME.
           IF INPUT-ERROR = 0 
               PERFORM READ-FIRME THRU READ-FIRME-END.
           PERFORM END-PROGRAM.


       START-FIRME.
           IF FR-KEY > 0 
              SUBTRACT 1 FROM FR-KEY.
           IF A-N = "A"
              MOVE 9999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-FIRME.
           PERFORM NEXT-FIRME.
           IF LNK-COMPETENCE < FR-COMPETENCE
              GO READ-FIRME
           END-IF.
           IF FR-KEY = 0
           OR FR-KEY > END-NUMBER
           OR FR-MATCHCODE > END-MATCHCODE
              GO READ-FIRME-END
           END-IF.
           ACCEPT ACTION TIME 1 LINE 23 POSITION 27 NO BEEP
           ON EXCEPTION ESC-KEY CONTINUE END-ACCEPT.
           IF ESC-KEY = 27 
              EXIT PROGRAM.
           PERFORM DIS-HE-01.
           PERFORM TEST-EXCLUSION.
           IF SEL-ERROR = 1
              GO READ-FIRME
           END-IF.
           PERFORM FILL-FIRME.
           GO READ-FIRME.
       READ-FIRME-END.
           PERFORM END-PROGRAM.

       NEXT-FIRME.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.
           IF FR-KEY NOT = 0
           AND EXC-KEY = 66
              IF FR-DEB-A > 0 AND > LNK-ANNEE
                 GO NEXT-FIRME
              END-IF
              IF FR-FIN-A > 0 AND < LNK-ANNEE
                 GO NEXT-FIRME
              END-IF
           END-IF.

       ENTETE.
           ADD 1 TO PAGE-NUMBER.
           MOVE 4 TO CAR-NUM.
           MOVE 2 TO LIN-NUM.
           MOVE 20 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 112 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 115 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 118 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       FILL-FIRME.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER 
           END-IF.
           COMPUTE IDX = LIN-NUM + MAX-LIGNES.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              COMPUTE LIN-NUM = LIN-IDX 
           END-IF.
           ADD 1 TO LIN-NUM.
           MOVE LIN-NUM TO IDX-4.
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  3 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           IF H-S(1) NOT = "N" AND NOT = " "
              MOVE FR-PAYS TO ALPHA-TEXTE
              MOVE 55 TO COL-NUM
              PERFORM FILL-FORM
              MOVE 57 TO COL-NUM
              IF FR-PAYS = "D" OR "F" OR "CZ" OR "I"
                 MOVE FR-CP5 TO ALPHA-TEXTE
              ELSE
                 MOVE FR-CP-LUX TO ALPHA-TEXTE
              END-IF
              PERFORM FILL-FORM
              MOVE 65 TO COL-NUM
              MOVE FR-MAISON TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD   1 TO COL-NUM
              MOVE FR-RUE TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE 110 TO COL-NUM
              MOVE FR-LOCALITE TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.
           MOVE 0 TO STORE.
           MOVE 10 TO COL-NUM.
           IF H-S(2) NOT = "N" AND NOT = " "
              ADD 1 TO LIN-NUM STORE
              MOVE FR-PHONE TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 5 TO COL-NUM
           END-IF.
           IF H-S(3) NOT = "N" AND NOT = " "
              IF FR-FAX > SPACES
                 IF STORE = 0
                    ADD 1 TO LIN-NUM STORE
                 END-IF
                 MOVE "FAX:" TO ALPHA-TEXTE
                 PERFORM FILL-FORM
                 ADD 2 TO COL-NUM
                 MOVE FR-FAX TO ALPHA-TEXTE
                 PERFORM FILL-FORM
                 ADD 5 TO COL-NUM
              END-IF
           END-IF.
           IF H-S(4) NOT = "N" AND NOT = " "
           AND FR-EMAIL > SPACES
              IF STORE = 0
                 ADD 1 TO LIN-NUM STORE
              END-IF
              MOVE FR-EMAIL TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 5 TO COL-NUM
           END-IF.
           IF H-S(5) NOT = "N" AND NOT = " "
           AND FR-CORRESPONDANT > SPACES
              IF STORE = 0
                 ADD 1 TO LIN-NUM STORE
              END-IF
              MOVE FR-CORRESPONDANT TO ALPHA-TEXTE
              MOVE 55 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.

           IF H-S(6) NOT = "N" AND NOT = " "
           AND FR-COURRIER-1 NOT = SPACES
              ADD 1 TO LIN-NUM 
              MOVE 10 TO COL-NUM
              MOVE FR-COURRIER-1 TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE 55 TO COL-NUM
              MOVE FR-COURRIER-2 TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 5 TO COL-NUM
              MOVE FR-COURRIER-3 TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.

           MOVE 0 TO STORE.
           IF H-S(14) NOT = "N" AND NOT = " "
           AND FR-ACTIVITE > SPACES
              ADD 1 TO LIN-NUM STORE
              MOVE 10 TO COL-NUM
              MOVE FR-ACTIVITE TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.

           IF H-S(15) NOT = "N" AND NOT = " "
              IF STORE = 0
                 ADD 1 TO LIN-NUM STORE
              END-IF
              MOVE 55 TO COL-NUM
              MOVE FR-ACCIDENT TO LNK-NUM VH-00
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE "AC" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.

           MOVE 0 TO STORE.
           MOVE 10 TO COL-NUM.
           IF H-S(10) NOT = "N" AND NOT = " " AND NOT = "-"
              ADD 1 TO LIN-NUM STORE
              MOVE FR-ETAB-A TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE FR-ETAB-N TO ALPHA-TEXTE
              ADD 1 TO COL-NUM
              PERFORM FILL-FORM
              MOVE FR-SNOCS TO ALPHA-TEXTE
              ADD 1 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.

           IF H-S(8) NOT = "N" AND NOT = " "
              IF STORE = 0
                 ADD 1 TO LIN-NUM STORE
              END-IF
              MOVE 40 TO COL-NUM
              MOVE FR-COMMERCE TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.
           IF H-S(9) NOT = "N" AND NOT = " "
              IF STORE = 0
                 ADD 1 TO LIN-NUM 
              END-IF
              MOVE 25 TO COL-NUM
              MOVE FR-TVA TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.

           IF H-S(17) NOT = "N" AND NOT = " " AND NOT = "-"
              MOVE 10 TO COL-NUM
              ADD 1 TO LIN-NUM 
              MOVE FR-FISC-BUREAU TO ALPHA-TEXTE
              PERFORM FILL-FORM
              EVALUATE FR-FISC-CODE  
                WHEN 0 MOVE 104 TO LNK-NUM
                WHEN 1 MOVE 105 TO LNK-NUM
                WHEN 2 MOVE 107 TO LNK-NUM
              END-EVALUATE
              MOVE "AA" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO ALPHA-TEXTE
              MOVE 55 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.

           MOVE 0 TO STORE.
           IF H-S(11) NOT = "N" AND NOT = " " AND NOT = "-"
           AND FR-ECHEANCE > 0
              ADD 1 TO LIN-NUM STORE
              MOVE 10 TO COL-NUM
              MOVE FR-ECHEANCE TO VH-00 ECH-CODE
              MOVE 5 TO CAR-NUM
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              CALL "6-ECHEA" USING LINK-V ECH-RECORD FAKE-KEY
              MOVE ECH-NOM TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 5 TO COL-NUM
           END-IF.
           IF H-S(12) NOT = "N" AND NOT = " " AND NOT = "-"
              IF STORE = 0
                 ADD 1 TO LIN-NUM STORE
              END-IF
              MOVE 55 TO COL-NUM
              EVALUATE FR-FREQ-FACT
                WHEN 0 MOVE 104 TO LNK-NUM
                WHEN 1 MOVE 104 TO LNK-NUM
                WHEN 2 MOVE 108 TO LNK-NUM
                WHEN 3 MOVE 105 TO LNK-NUM
                WHEN 6 MOVE 106 TO LNK-NUM
                WHEN 9 MOVE 99  TO LNK-NUM
              END-EVALUATE
              MOVE "AA" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.

           MOVE 90 TO COL-NUM.
           IF H-S(13) NOT = "N" AND NOT = " "
              IF STORE = 0
                 ADD 1 TO LIN-NUM STORE
              END-IF
              MOVE FR-COMPTA TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 5 TO COL-NUM
           END-IF.

           MOVE 10 TO COL-NUM.
           IF H-S(14) NOT = "N" AND NOT = " "
              IF FR-DATE-DEB NOT = 0
              OR FR-DATE-FIN NOT = 0
                 ADD 1 TO LIN-NUM STORE
                 MOVE FR-DEB-A TO HE-AA
                 MOVE FR-DEB-M TO HE-MM
                 MOVE FR-DEB-J TO HE-JJ
                 MOVE HE-DATE TO ALPHA-TEXTE
                 PERFORM FILL-FORM
                 ADD 5 TO COL-NUM
                 MOVE FR-FIN-A TO HE-AA
                 MOVE FR-FIN-M TO HE-MM
                 MOVE FR-FIN-J TO HE-JJ
                 MOVE HE-DATE TO ALPHA-TEXTE
                 PERFORM FILL-FORM
              END-IF
           END-IF.

           IF H-S(7) NOT = "N" AND NOT = " "
           AND FR-REMARQUE > SPACES
              IF STORE = 0
                 ADD 1 TO LIN-NUM STORE
              END-IF
              MOVE 10 TO COL-NUM
              MOVE FR-REMARQUE TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.

           IF H-S(18) NOT = "N" AND NOT = " " AND NOT = "-"
              MOVE 10 TO COL-NUM
              ADD 1 TO LIN-NUM 
              MOVE "F1" TO LNK-AREA
              MOVE 18 TO LNK-NUM
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE FR-PRIME TO LNK-NUM
              MOVE "PF" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO ALPHA-TEXTE
              ADD  3 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.

           MOVE 10 TO COL-NUM.
           ADD  1  TO LIN-NUM.
           PERFORM TIRET UNTIL COL-NUM > 127.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

       WRITE-TEXT.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-TRANS
              MOVE 1 TO NOT-OPEN IDX-4
              IF TABLEAU = 1
                 PERFORM HEAD-LINE
                 WRITE TF-RECORD FROM TXT-RECORD
              END-IF
              INITIALIZE TXT-RECORD
           END-IF.
           PERFORM LIGNE.
           WRITE TF-RECORD FROM TXT-RECORD
           INITIALIZE TXT-RECORD.


       HEAD-LINE.
           MOVE "FI" TO LNK-AREA.
           MOVE 1 TO LNK-NUM.
           MOVE 9 TO IDX.
FIRME      PERFORM TEXTES.
           MOVE "AA" TO LNK-AREA.
           MOVE 100 TO LNK-NUM.
           MOVE 9 TO IDX.
NUMERO     PERFORM TEXTES.
           MOVE "PR" TO LNK-AREA.
           MOVE  3 TO LNK-NUM.
           MOVE 31 TO IDX.
NOM        PERFORM TEXTES.
           MOVE  4 TO LNK-NUM.
           MOVE 21 TO IDX.
PRENOM     PERFORM TEXTES.

           IF H-S(1) NOT = "N" AND NOT = " "
              MOVE 20 TO LNK-NUM
              MOVE 15 TO IDX
TITRE         PERFORM TEXTES
              MOVE 10 TO LNK-NUM
              MOVE 11 TO IDX
MAISON        PERFORM TEXTES
              MOVE  9 TO LNK-NUM
              MOVE 31 TO IDX
RUE           PERFORM TEXTES
              MOVE 56 TO LNK-NUM
              MOVE 21 TO IDX
BPOST         PERFORM TEXTES
              MOVE  7 TO LNK-NUM
              MOVE  8 TO IDX
PAYS          PERFORM TEXTES
              MOVE  8 TO LNK-NUM
              MOVE 12 TO IDX
CPOST         PERFORM TEXTES
              MOVE 11 TO LNK-NUM
              MOVE 31 TO IDX
LOC           PERFORM TEXTES
           END-IF.

       TEXTES.
           CALL "0-GMESS" USING LINK-V.
           PERFORM STR.

       STR.
           STRING TXT-LIMIT DELIMITED BY SIZE INTO LNK-TEXT 
           POINTER IDX ON OVERFLOW CONTINUE END-STRING.
           STRING LNK-TEXT DELIMITED BY "|" INTO TXT-RECORD 
           POINTER IDX-4 ON OVERFLOW CONTINUE END-STRING.

       SMS.
           STRING TXT-LIMIT DELIMITED BY SIZE INTO MS-DESCRIPTION
           POINTER IDX ON OVERFLOW CONTINUE END-STRING.
           STRING MS-DESCRIPTION DELIMITED BY "|" INTO TXT-RECORD 
           POINTER IDX-4 ON OVERFLOW CONTINUE END-STRING.

       LIGNE.
           MOVE 1 TO IDX-4.
           MOVE FR-KEY     TO HE-Z8.
           STRING HE-Z8 DELIMITED BY SIZE INTO TXT-RECORD 
           POINTER IDX-4 ON OVERFLOW CONTINUE END-STRING.
           STRING TXT-SC DELIMITED BY SIZE INTO TXT-RECORD 
           POINTER IDX-4 ON OVERFLOW CONTINUE END-STRING.
           MOVE FR-NOM TO LNK-TEXT.
           MOVE 31 TO IDX.
           PERFORM STR.

           IF H-S(1) NOT = "N" AND NOT = " "
              MOVE 11 TO IDX
              MOVE FR-MAISON TO LNK-TEXT
              PERFORM STR
              MOVE 31 TO IDX
              MOVE FR-RUE TO LNK-TEXT
              PERFORM STR
              MOVE FR-PAYS TO LNK-TEXT
              MOVE 8 TO IDX
              PERFORM STR
              MOVE 12 TO IDX
              IF FR-PAYS = "D" OR "F" OR "CZ" OR "I"
                 MOVE FR-CP5 TO LNK-TEXT
              ELSE
                 MOVE FR-CP-LUX TO LNK-TEXT
              END-IF
              PERFORM STR
              MOVE 31 TO IDX
              MOVE FR-LOCALITE TO LNK-TEXT
              PERFORM STR
           END-IF.


       TEST-EXCLUSION.
           MOVE 0 TO SEL-ERROR.
           IF H-S(1) = "-"
              IF FR-MAISON   > SPACES
              OR FR-RUE      > SPACES
              OR FR-LOCALITE > SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(1) = "+"
              IF  FR-MAISON   = SPACES
              AND FR-RUE      = SPACES
              AND FR-LOCALITE = SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(2) = "-"
              IF FR-PHONE   > SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(2) = "+"
              IF  FR-PHONE   = SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(3) = "-"
              IF FR-FAX   > SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(3) = "+"
              IF FR-FAX   = SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(6) = "-"
              IF FR-COURRIER-1   > SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(6) = "+"
              IF FR-COURRIER-1   = SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(4) = "-"
              IF FR-EMAIL   > SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(4) = "+"
              IF FR-EMAIL   = SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(5) = "-"
              IF FR-RESPONSABLE > SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(5) = "+"
              IF FR-RESPONSABLE = SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(7) = "-"
              IF FR-REMARQUE > SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(7) = "+"
              IF FR-REMARQUE = SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(8) = "-"
              IF FR-COMMERCE > SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(8) = "+"
              IF FR-COMMERCE = SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(9) = "-"
              IF FR-TVA > SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(9) = "+"
              IF FR-TVA = SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(10) = "-"
              IF FR-MATRICULE > 0
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(10) = "+"
              IF FR-MATRICULE = 0
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(11) = "-"
              IF FR-ECHEANCE > 0
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(11) = "+"
              IF FR-ECHEANCE = 0
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(12) = "-"
              IF FR-FREQ-FACT > 0
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(12) = "+"
              IF FR-FREQ-FACT = 0
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(13) = "-"
              IF FR-COMPTA > SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(13) = "+"
              IF FR-COMPTA = SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(14) = "-"
              IF FR-ACTIVITE > SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(14) = "+"
              IF FR-ACTIVITE = SPACES
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(15) = "-"
              IF FR-DATE-FIN > 0
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(15) = "+"
              IF FR-DATE-FIN = 0
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(18) = "-"
              IF FR-PRIME > 0
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.
           IF H-S(18) = "+"
              IF FR-PRIME = 0
                 MOVE 1 TO SEL-ERROR
              END-IF
           END-IF.

       DIS-HE-01.
           DISPLAY A-N LINE 4 POSITION 32.
           MOVE FR-KEY  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 17 
           IF A-N = "N"
              DISPLAY FR-NOM LINE 6 POSITION 25 SIZE 30
           ELSE
              DISPLAY SPACES   LINE 6 POSITION 25 SIZE 30
              DISPLAY FR-NOM   LINE 6 POSITION 36 SIZE 25.

       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1100 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           MOVE 2 TO LIN-IDX.
           MOVE 0 TO COMPTEUR.
           PERFORM AFFICHE-D THRU AFFICHE-END.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       AFFICHE-D.
           MOVE COMPTEUR TO LNK-NUM.
           ADD 1 TO LNK-NUM.
           MOVE "F1" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           IF LNK-TEXT = SPACES
              GO AFFICHE-END.
           ADD 1 TO COMPTEUR.
           IF H-S(COMPTEUR) = "N" 
              CONTINUE
           ELSE
              ADD  1 TO LIN-IDX 
              MOVE 1 TO IDX
              INSPECT LNK-TEXT TALLYING IDX FOR CHARACTERS BEFORE "   "
             DISPLAY LNK-TEXT LINE LIN-IDX POSITION 55 SIZE IDX REVERSE.
           GO AFFICHE-D.
       AFFICHE-END.
           EXIT.

       END-PROGRAM.
           IF LIN-NUM > LIN-IDX 
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           CANCEL "5-SEL"
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V A-N FAKE-KEY.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXF".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".
