      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 0-DELDAT EFFACEMENT DATES INCOHERENTES PERSONNE  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-DELDAT.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "CODPAIE.FC".
           COPY "LIVRE.FC".
           COPY "JOURS.FC".
           COPY "HEURES.FC".
           COPY "HORSEM.FC".
           COPY "VIREMENT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CODPAIE.FDE".
           COPY "LIVRE.FDE".
           COPY "JOURS.FDE".
           COPY "HEURES.FDE".
           COPY "HORSEM.FDE".
           COPY "VIREMENT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".

       01  RECAL.
           02 RECAL-IDX          PIC 9 OCCURS 12.
           02 RECAL-SUITE OCCURS 12.
              03 RECAL-MAL       PIC 9 OCCURS 10.

       01  CHOIX-MAX             PIC 99 VALUE 3.

       01  HRS-NAME.
           02 FILLER             PIC X(9) VALUE "S-HEURES.".
           02 ANNEE-HEURES        PIC 999.
       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.
       01  LIVRE-NAME.
           02 FILLER             PIC X(8) VALUE "S-LIVRE.".
           02 ANNEE-LIVRE        PIC 999.
       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIR          PIC 999.
       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       01  HE-Z6 PIC Z(6) BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                CODPAIE
                LIVRE
                JOURS
                HEURES
                HORSEM 
                VIREMENT.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-DELDAT.

           MOVE LNK-MOIS TO SAVE-MOIS.
           MOVE LNK-SUFFIX TO ANNEE-PAIE ANNEE-HEURES ANNEE-LIVRE 
           ANNEE-VIR ANNEE-JOURS.

      *  Appel par un autre programme 
           IF LNK-PERSON NOT = 0
              MOVE FR-KEY TO REG-FIRME
              MOVE LNK-PERSON TO REG-PERSON 
              CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY
              MOVE 0 TO LNK-PERSON
              PERFORM EFFACE-REGISTRE
              PERFORM END-PROGRAM
           END-IF.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF ECRAN-IDX = 1 AND INDICE-ZONE < 1
              MOVE 1 TO INDICE-ZONE.
        
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000130000 TO EXC-KFR(3)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
                      MOVE 0000130000 TO EXC-KFR(3)
              WHEN 3  MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-1.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE  3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.
           
      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           PERFORM DIS-HE-01 THRU DIS-HE-02.

       APRES-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
           WHEN  OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           PERFORM DIS-HE-01 THRU DIS-HE-02.
           
       APRES-DEC.
            EVALUATE EXC-KEY 
            WHEN  8 PERFORM EFFACE-REGISTRE
                    MOVE 1 TO DECISION 
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       EFFACE-REGISTRE.
           INITIALIZE
           RECAL
           CSP-RECORD
           JRS-RECORD
           HRS-RECORD
           HJS-RECORD
           L-RECORD
           VIR-RECORD.
       
           PERFORM EFFACE-CSPAI.
           PERFORM EFFACE-JOURS.
           PERFORM EFFACE-HEURES.
           PERFORM EFFACE-HORSEM.
           PERFORM EFFACE-LIVRE.
           PERFORM EFFACE-VIREMENT.
           MOVE LNK-MOIS TO SAVE-MOIS
           PERFORM RECAL VARYING IDX FROM 1 BY 1 UNTIL IDX > 12.
           MOVE SAVE-MOIS TO LNK-MOIS.
           MOVE 0 TO LNK-SUITE.


       EFFACE-CSPAI.
           OPEN I-O CODPAIE.
           MOVE FR-KEY TO CSP-FIRME.
           MOVE REG-PERSON TO CSP-PERSON.
           START CODPAIE KEY > CSP-KEY INVALID CLOSE CODPAIE
                NOT INVALID
           PERFORM DELETE-CSP THRU DELETE-CSP-END.
           
       DELETE-CSP.
           READ CODPAIE NEXT AT END 
                GO DELETE-CSP-END.
           IF FR-KEY NOT = CSP-FIRME
           OR REG-PERSON NOT = CSP-PERSON
              GO DELETE-CSP-END.
           IF PRES-TOT(CSP-MOIS) = 0
              DELETE CODPAIE INVALID CONTINUE END-DELETE
              IF LNK-SQL = "Y"
                 CALL "9-CODPAI" USING LINK-V CSP-RECORD DEL-KEY
              END-IF
           END-IF.
           GO DELETE-CSP.
       DELETE-CSP-END.
           CLOSE CODPAIE.



       EFFACE-JOURS.
           OPEN I-O   JOURS.
           INITIALIZE JRS-RECORD.
           MOVE FR-KEY  TO JRS-FIRME-2.
           MOVE REG-PERSON  TO JRS-PERSON-2.
           START JOURS KEY > JRS-KEY-2 INVALID CLOSE JOURS
                NOT INVALID
           PERFORM READ-JOURS THRU READ-JRS-END.
       READ-JOURS.
           READ JOURS NEXT AT END 
                GO READ-JRS-END.
           IF FR-KEY NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
              GO READ-JRS-END.
           IF PRES-TOT(JRS-MOIS) = 0
              DELETE JOURS INVALID CONTINUE
              IF LNK-SQL = "Y"
                 CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY
              END-IF
           END-IF.
           IF PRES-TOT(JRS-MOIS) < MOIS-JRS(JRS-MOIS)
              MOVE JRS-JOURS TO IDX-2 
              MOVE 0 TO JRS-HRS(32) JRS-JOURS
              PERFORM TEST-OK VARYING IDX-1 FROM 1 BY 1
              UNTIL IDX-1 > MOIS-JRS(JRS-MOIS)
              IF IDX-2 > JRS-JOURS 
                 MOVE 1 TO RECAL-IDX(JRS-MOIS) 
                 IF  JRS-OCCUPATION > 0
                 AND JRS-OCCUPATION < 11
                    MOVE 1 TO RECAL-MAL(JRS-MOIS, JRS-OCCUPATION) 
                 END-IF
                 IF JRS-JOURS > 0
                    REWRITE JRS-RECORD INVALID CONTINUE
                    IF LNK-SQL = "Y"
                       CALL "9-JOURS" USING LINK-V JRS-RECORD WR-KEY
                    END-IF
                 ELSE
                    DELETE JOURS INVALID CONTINUE
                    IF LNK-SQL = "Y"
                       CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY
                    END-IF
                 END-IF
              END-IF
           END-IF.
           GO READ-JOURS.
       READ-JRS-END.
           CLOSE JOURS.


       TEST-OK.
           IF PRES-JOUR(JRS-MOIS, IDX-1) = 0
           AND JRS-HRS(IDX-1) NOT = 0
              MOVE 0 TO JRS-HRS(IDX-1) 
           END-IF.
           IF JRS-HRS(IDX-1) > 0
              ADD JRS-HRS(IDX-1) TO JRS-HRS(32)
              ADD 1 TO JRS-JOURS 
           END-IF.

       EFFACE-HEURES.
           OPEN I-O   HEURES.
           INITIALIZE HRS-RECORD.
           MOVE FR-KEY  TO HRS-FIRME.
           MOVE REG-PERSON  TO HRS-PERSON.
           START HEURES KEY > HRS-KEY INVALID KEY CLOSE HEURES
                NOT INVALID
           PERFORM READ-HEURES THRU READ-HRS-END.
       READ-HEURES.
           READ HEURES NEXT AT END 
                GO READ-HRS-END.
           IF FR-KEY NOT = HRS-FIRME
           OR REG-PERSON NOT = HRS-PERSON
              GO READ-HRS-END.
           IF PRES-JOUR(HRS-MOIS, HRS-JOUR) = 0
              DELETE HEURES INVALID CONTINUE END-DELETE
              IF LNK-SQL = "Y"
                 CALL "9-HEURES" USING LINK-V HRS-RECORD DEL-KEY
              END-IF
              GO READ-HEURES
           END-IF.
           GO READ-HEURES.
       READ-HRS-END.
           CLOSE HEURES.


       EFFACE-HORSEM.
           OPEN I-O   HORSEM.
           INITIALIZE HJS-RECORD.
           MOVE FR-KEY TO HJS-FIRME.
           MOVE REG-PERSON TO HJS-PERSON.
           START HORSEM KEY > HJS-KEY INVALID CLOSE HORSEM
                NOT INVALID
           PERFORM DELETE-HORSEM THRU DELETE-HJS-END.
       DELETE-HORSEM.
           READ HORSEM NEXT AT END 
                GO DELETE-HJS-END.
           IF FR-KEY NOT = HJS-FIRME
           OR REG-PERSON NOT = HJS-PERSON
                GO DELETE-HJS-END.
           IF  HJS-ANNEE = LNK-ANNEE
           AND PRES-TOT(HJS-MOIS) = 0
               DELETE HORSEM INVALID CONTINUE
               NOT INVALID
               IF LNK-SQL = "Y"
                  CALL "9-HORSEM" USING LINK-V HJS-RECORD DEL-KEY
               END-IF
           END-IF.
           GO DELETE-HORSEM.
       DELETE-HJS-END.
           CLOSE HORSEM.

       EFFACE-LIVRE.
           OPEN I-O   LIVRE.
           INITIALIZE L-RECORD.
           MOVE FR-KEY TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON.
           START LIVRE KEY > L-KEY INVALID CLOSE LIVRE
                NOT INVALID
           PERFORM DELETE-LP THRU DELETE-L-END.
       DELETE-LP.
           READ LIVRE NEXT AT END 
                GO DELETE-L-END.
           IF FR-KEY NOT = L-FIRME
           OR REG-PERSON NOT = L-PERSON
              GO DELETE-L-END.
           IF PRES-TOT(L-MOIS) = 0
              DELETE LIVRE INVALID CONTINUE
              NOT INVALID
              IF LNK-SQL = "Y"
                 CALL "9-LIVRE" USING LINK-V L-RECORD DEL-KEY
              END-IF
              IF L-SUITE = 0
              INITIALIZE L-REC-DETAIL L-UNITES CAR-RECORD
              MOVE L-MOIS TO LNK-MOIS
              INITIALIZE CAR-RECORD 
              CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY
              CALL "5-J" USING LINK-V REG-RECORD L-RECORD
              IF FR-CCM = "X"
                CALL "5-CCM" USING LINK-V REG-RECORD L-RECORD CAR-RECORD 
              END-IF
              END-IF
           END-IF.
           GO DELETE-LP.
       DELETE-L-END.
           CLOSE LIVRE.


       EFFACE-VIREMENT.
           OPEN I-O   VIREMENT.
           INITIALIZE VIR-RECORD.
           MOVE FR-KEY  TO VIR-FIRME.
           MOVE REG-PERSON TO VIR-PERSON.
           START VIREMENT KEY > VIR-KEY INVALID CLOSE VIREMENT
                NOT INVALID
               PERFORM DELETE-VIR THRU DELETE-VIR-END.
       DELETE-VIR.
           READ VIREMENT NEXT AT END 
                GO DELETE-VIR-END.
           IF FR-KEY NOT = VIR-FIRME
           OR REG-PERSON NOT = VIR-PERSON
              GO DELETE-VIR-END.
           IF VIR-MOIS = 0
              GO DELETE-VIR.
      *    IF VIR-DATE-EDITION  NOT = 0
      *    OR VIR-DATE-VIREMENT NOT = 0
      *       GO DELETE-VIR.
           IF PRES-TOT(VIR-MOIS) = 0
              DELETE VIREMENT INVALID CONTINUE
              NOT INVALID
              IF LNK-SQL = "Y"
                 CALL "9-VIREM" USING LINK-V VIR-RECORD DEL-KEY
              END-IF
              END-DELETE
           END-IF.
           GO DELETE-VIR.
       DELETE-VIR-END.
           CLOSE VIREMENT.

       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 292 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       RECAL.
           IF RECAL-IDX(IDX) = 1
           AND PRES-TOT(IDX) > 0
              MOVE IDX TO LNK-MOIS
              PERFORM RECMAL VARYING LNK-SUITE FROM 1 BY 1 UNTIL 
              LNK-SUITE > 10
              MOVE 0 TO LNK-SUITE
              CALL "4-A" USING LINK-V PR-RECORD REG-RECORD
           END-IF.

       RECMAL.
           IF RECAL-MAL(IDX, LNK-SUITE) = 1
              CALL "4-A" USING LINK-V PR-RECORD REG-RECORD
              INITIALIZE L-RECORD
              CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY
              CALL "4-CSPMAL" USING LINK-V REG-RECORD L-RECORD
           END-IF.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE 0 TO LNK-PERSON.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

