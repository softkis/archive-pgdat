      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-CSA IMPRESSION CONTROLE CODES SALAIRE     �
      *  � RECAPITULATION ANNUELLE                               �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CSA.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".
           COPY "TRIPR.FC".
           COPY "CODPAIE.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM130.FDE".
           COPY "TRIPR.FDE".
           COPY "CODPAIE.FDE".
       FD  TF-TRANS
           RECORD VARYING DEPENDING ON MAX-LONGUEUR
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 240 DEPENDING MAX-LONGUEUR.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "130       ".
       01  ESPACE                PIC X(9) VALUE "         ".
       01  CHOIX-MAX             PIC 99 VALUE 10.
       01  MAX-LIGNES            PIC 9 VALUE 2.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".
           COPY "METIER.REC".
           COPY "CONTRAT.REC".
           COPY "CODSAL.REC".
           COPY "POCL.REC".
           COPY "PARMOD.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.
       01  LIMITEUR              PIC X(6).

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.

       01  MAX-LONGUEUR          PIC 9999 VALUE 300.
       01  ASCII-FILE            PIC 9 VALUE 0.
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  AJOUTE                PIC 9 VALUE 0.
       01  TABLEAU               PIC 9 VALUE 0.

       01  TOTAL-VALEUR.
           02 TOT-VALEURS.
               04 TOT-DONNEE     PIC 9(8)V9(5) OCCURS 8.
           02 TOT-PERS           PIC 9(4) COMP-3.

       01  TOTAL-PERSONNE.
           02 TP-VALEURS.
               04 TP-DONNEE     PIC 9(8)V9(5) OCCURS 8.
           02 TOT-OCCURENCES     PIC 999.

       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

           COPY "V-VH00.CPY".
       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".CSA".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.

       01  T1-RECORD.
           02 T1-A.
              03 T1-FIRME  PIC 9(6).
              03 T1-DELIM0 PIC X VALUE ";".
              03 T1-PERSON PIC 9(8).
              03 T1-DELIM1 PIC X VALUE ";".
              03 T1-NOM    PIC X(50).
              03 T1-DELIM2 PIC X VALUE ";".

              03 T1-FILLER PIC X(6) VALUE SPACES.
              03 T1-COUT   PIC Z(8).
              03 T1-DELIM3 PIC X VALUE ";".
              03 T1-ANNEE  PIC ZZZZZ.
              03 T1-DELIM4 PIC X VALUE ";".
              03 T1-MOIS  PIC Z(9).
              03 T1-DELIM4 PIC X VALUE ";".

              03 T1-TEXT   PIC X(25).
              03 T1-DELIM5 PIC X VALUE ";".
              03 T1-M OCCURS 6.
                 05 T1-FILL PIC X(8).
                 05 T1-VAL  PIC ZZZZZZZZZ,ZZ.
                 05 T1-DEL  PIC X.

       01  TXT-RECORD.
           02 TXT-A.
              03 TXT-FIRME  PIC X(6).
              03 TXT-DELIM0 PIC X VALUE ";".
              03 TXT-PERSON PIC X(8).
              03 TXT-DELIM1 PIC X VALUE ";".
              03 TXT-NOM    PIC X(50).
              03 TXT-DELIM2 PIC X VALUE ";".

              03 TXT-COUT   PIC X(14).
              03 TXT-DELIM3 PIC X VALUE ";".
              03 TXT-ANNEE  PIC XXXXX.
              03 TXT-DELIM4 PIC X VALUE ";".
              03 TXT-MOIS   PIC X(9).
              03 TXT-DELIM4 PIC X VALUE ";".
              03 TXT-TEXT   PIC X(25).
              03 TXT-DELIM5 PIC X VALUE ";".
              03 TXT-CS OCCURS 6.
                 05 TXT-DES  PIC X(20).
                 05 TXT-DEL  PIC X.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR
               TF-TRANS
               CODPAIE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CSA.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-PROG-NUMBER-1 TO CS-NUMBER.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE LNK-SUFFIX TO ANNEE-PAIE.
           OPEN INPUT CODPAIE.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TOTAL-VALEUR TEST-EXTENSION.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0064230000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7 THRU 8
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 10    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700009278 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR (14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN 10 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-8
           WHEN  9 PERFORM APRES-SORT
           WHEN 10 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.            
           IF MENU-PROG-NUMBER > 0
              MOVE MENU-PROG-NUMBER TO CS-NUMBER
           ELSE
           ACCEPT CS-NUMBER
             LINE 13 POSITION 29 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-7.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PATH.
           MOVE "AA" TO LNK-AREA.
           MOVE 32 TO LNK-NUM.
           MOVE "L" TO LNK-LOW.
           PERFORM DISPLAY-MESSAGE.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 24 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

--     APRES-6.
           MOVE 0 TO INPUT-ERROR.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 2 CALL "2-CODSAL" USING LINK-V CS-RECORD
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR
                  CANCEL "2-CODSAL" 
           WHEN 3 CALL "2-CS" USING LINK-V CS-RECORD
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR
                  CANCEL "2-CS" 
           WHEN OTHER PERFORM NEXT-CS 
           END-EVALUATE.
           IF CS-NOM = SPACES
              MOVE 1 TO INPUT-ERROR
           ELSE       
              PERFORM TEST-CS.
           PERFORM DIS-HE-06.
           MOVE CS-NUMBER TO PARMOD-PROG-NUMBER-1.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       NEXT-CS.
           MOVE 0 TO INPUT-ERROR.
           CALL "6-CODSAL" USING LINK-V CS-RECORD SAVE-KEY.
           IF CS-NOM NOT = SPACES
              PERFORM TEST-CS
              IF INPUT-ERROR = 1
              AND SAVE-KEY NOT = 13
                 GO NEXT-CS
              END-IF
           END-IF.
           IF LNK-LANGUAGE NOT = "F"
              CALL "6-CODTXT" USING LINK-V CS-RECORD.

       TEST-CS.
           MOVE 0 TO LNK-NUM INPUT-ERROR.
           IF CS-MALADIE = 2
              MOVE 34 TO LNK-NUM.
           IF CS-NOM = SPACES
              MOVE 1 TO LNK-NUM.
           IF LNK-NUM NOT = 0 
              IF LNK-NUM = 1 
                 MOVE "AA" TO LNK-AREA
              ELSE
                 MOVE "SL" TO LNK-AREA
              END-IF
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-NUM.

       APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-07.

       APRES-8.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           IF EXC-KEY = 9 
              MOVE 1 TO TABLEAU
              MOVE 10 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           WHEN 10 MOVE 1 TO ASCII-FILE
                   PERFORM AVANT-PATH
                   PERFORM TRAITEMENT
                   PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-2
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           IF  CHOIX > 0
           AND CHOIX < 90
              IF TEST-ALPHA  NOT = SPACES
              OR TEST-NUMBER NOT = 0
                 IF TRIPR-CHOIX NOT = TEST-EXTENSION
                    IF TOT-PERS NOT = 0
                       IF MENU-BATCH = 0
                          PERFORM FILL-TOTAL
                       END-IF
                       PERFORM TRANSMET
                       INITIALIZE TOTAL-VALEUR
                    END-IF
                 END-IF
              END-IF
           END-IF.
           IF CHOIX > 0
              MOVE TRIPR-CHOIX TO TEST-EXTENSION
           END-IF.
           INITIALIZE TOTAL-PERSONNE LAST-PERSON.
           PERFORM RECHERCHE-CSP  VARYING MOIS-COURANT FROM 
                   MOIS-DEBUT BY 1 UNTIL MOIS-COURANT > MOIS-FIN.

           IF TOT-OCCURENCES > 1
           AND MENU-BATCH = 0
              PERFORM FILL-TOTAL-PERS
           END-IF.
           INITIALIZE TOTAL-PERSONNE.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.


       METIER.
           MOVE CAR-METIER TO MET-CODE.
           MOVE "F "       TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           MOVE 20 TO LIN-NUM.
           MOVE 10 TO COL-NUM.
           PERFORM FILL-FORM.

       ENTETE.
           MOVE 2 TO LIN-NUM.
           MOVE 8 TO COL-NUM
           MOVE MENU-DESCRIPTION TO ALPHA-TEXTE
           MOVE 0 TO IDX.
           INSPECT ALPHA-TEXTE TALLYING IDX FOR CHARACTERS BEFORE "(".
           STRING ESPACE DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER IDX ON OVERFLOW CONTINUE.

           PERFORM FILL-FORM.
           ADD 1 TO PAGE-NUMBER.
           MOVE 4 TO CAR-NUM.
           MOVE 120 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  7 TO LIN-NUM.
           MOVE 112 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 115 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 118 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           IF COUT-NUMBER NOT = 0
              MOVE 3 TO LIN-NUM
              MOVE 8 TO COL-NUM
              MOVE COUT-NOM TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE COUT-NUMBER TO VH-00 
              MOVE 4 TO CAR-NUM
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM.

           MOVE  4 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           IF STATUT NOT = 0
              MOVE "-" TO ALPHA-TEXTE
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM
              MOVE STAT-NOM TO ALPHA-TEXTE
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           
           MOVE FR-PAYS TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 2 TO LIN-NUM.
           MOVE 55 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE LNK-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE 4 TO LIN-NUM.
           MOVE 30 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE CS-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE CS-NOM TO ALPHA-TEXTE.
           MOVE 35 TO COL-NUM.
           PERFORM FILL-FORM.

           IF CHOIX NOT = 0 
              PERFORM DETAIL-CHOIX.

           MOVE 9 TO LIN-NUM.
           PERFORM FILL-DESCR VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.

       FILL-DESCR.
           COMPUTE COL-NUM = 33 + (IDX * 14).
           MOVE CS-DESCRIPTION(IDX) TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       DETAIL-CHOIX.
           MOVE  4 TO LIN-NUM.
           MOVE 65 TO COL-NUM.
           MOVE TEST-NUMBER TO LNK-POSITION.
           MOVE TEST-ALPHA  TO LNK-TEXT.
           MOVE CHOIX       TO LNK-NUM.
           CALL "4-CHOIX" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

        DONNEES-PERSONNE.
           PERFORM DIS-HE-01.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
           END-IF.
           
           COMPUTE IDX = LIN-NUM + MAX-LIGNES.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           ADD 1 TO LIN-NUM.
           
           MOVE REG-PERSON TO LAST-PERSON.
           MOVE  0 TO DEC-NUM POINTS.
           MOVE  3 TO COL-NUM.

           IF MENU-EXTENSION-1 = "MATR"
              ADD 2 TO COL-NUM
              MOVE PR-NAISS-A TO  ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE PR-NAISS-M TO  ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE PR-NAISS-J TO  ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE PR-SNOCS TO  ALPHA-TEXTE
              PERFORM FILL-FORM
           ELSE
              MOVE 6 TO CAR-NUM
              MOVE REG-PERSON TO VH-00
              PERFORM FILL-FORM
           END-IF.

           ADD 1 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           IF MENU-EXTENSION-1 = "MATR"
              MOVE 68 TO COL-NUM
              MOVE 6 TO CAR-NUM
              MOVE REG-PERSON TO VH-00
              PERFORM FILL-FORM
           END-IF.
           ADD 1 TO TOT-PERS.

       FILL-CS.
           COMPUTE COL-NUM = 31 + (IDX * 14).
           IF CSP-DONNEE(IDX) < 20000000
              MOVE 1 TO POINTS. 
              MOVE 8 TO CAR-NUM.
           MOVE CS-DECIMALES(IDX) TO DEC-NUM.
           IF DEC-NUM > 4 MOVE 4 TO DEC-NUM.
           MOVE CSP-DONNEE(IDX) TO VH-00.
           IF VH-00 > 0
              PERFORM FILL-FORM
           END-IF.            

       FILL-TOTAL.
           ADD 1 TO LIN-NUM.
           MOVE 13 TO COL-NUM.
           MOVE "TOTAL" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 4 TO CAR-NUM.
           MOVE TOT-PERS TO VH-00.
           ADD   1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE TOT-VALEURS TO CSP-VALEURS.
           PERFORM FILL-CS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.

       FILL-TOTAL-PERS.
           ADD 1 TO LIN-NUM.
           MOVE TP-VALEURS TO CSP-VALEURS.
           PERFORM FILL-CS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.

       RECHERCHE-CSP.
           INITIALIZE CSP-RECORD CAR-NUM.
           MOVE FR-KEY       TO CSP-FIRME.
           MOVE REG-PERSON   TO CSP-PERSON.
           MOVE MOIS-COURANT TO CSP-MOIS.
           MOVE CS-NUMBER    TO CSP-CODE.
           START CODPAIE KEY >= CSP-KEY INVALID CONTINUE
               NOT INVALID PERFORM READ-CSP THRU READ-CSP-END.

       READ-CSP.
           READ CODPAIE NEXT NO LOCK AT END
               GO READ-CSP-END
           END-READ.
           IF FR-KEY       NOT = CSP-FIRME
           OR REG-PERSON   NOT = CSP-PERSON
           OR MOIS-COURANT NOT = CSP-MOIS 
           OR CSP-SUITE    NOT = 0
              GO READ-CSP-END
           END-IF.
           IF CS-NUMBER NOT = CSP-CODE
              GO READ-CSP.
           IF ASCII-FILE = 1
              PERFORM WRITE-TEXT
           ELSE
              PERFORM PRINT-TEXT
           END-IF.
           GO READ-CSP.
       READ-CSP-END.
           EXIT.

       PRINT-TEXT.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE IDX = LIN-NUM + 1.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              PERFORM READ-FORM
              PERFORM ENTETE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              COMPUTE LIN-NUM = LIN-IDX 
           END-IF.
           IF REG-PERSON NOT = LAST-PERSON
           OR LAST-PERSON = 0
              PERFORM DONNEES-PERSONNE.
           ADD 1 TO LIN-NUM.
           PERFORM FILL-FORM.
           MOVE 0 TO DEC-NUM.
           MOVE MOIS-COURANT TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           MOVE 13 TO COL-NUM.
           PERFORM FILL-FORM.
           IF CSP-POSTE > 0
              MOVE CSP-POSTE TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              MOVE 24 TO COL-NUM
              MOVE PC-NOM TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.
           PERFORM FILL-CS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.
           PERFORM ADD-CS  VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.
           ADD 1 TO TOT-OCCURENCES.
           IF CS-NUMBER = 9002
              IF CSP-TEXTE > SPACES
                 MOVE CSP-TEXTE TO ALPHA-TEXTE
              ELSE
                 MOVE CSP-ADRESSE TO ALPHA-TEXTE
              END-IF
              MOVE  0 TO CAR-NUM
              MOVE 80 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.

       ADD-CS.
           ADD CSP-DONNEE(IDX) TO TOT-DONNEE(IDX) TP-DONNEE(IDX).  


       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       WRITE-TEXT.
           IF NOT-OPEN = 0
              IF AJOUTE = 1
                 OPEN EXTEND TF-TRANS
              ELSE
                 OPEN OUTPUT TF-TRANS
              END-IF
              MOVE 1 TO NOT-OPEN
              PERFORM HEAD-LINE
              IF TABLEAU = 1
              AND AJOUTE = 0
                  WRITE TF-RECORD FROM Txt-RECORD
              END-IF
           END-IF.
           MOVE LNK-ANNEE TO T1-ANNEE.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO T1-NOM.
           MOVE CAR-COUT TO T1-COUT.
           MOVE FR-KEY TO T1-FIRME.
           MOVE REG-PERSON TO T1-PERSON.
           MOVE CSP-MOIS TO LNK-NUM T1-MOIS.
           MOVE "MO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO T1-TEXT.
           MOVE 0 TO IDX.
           PERFORM LIGNE VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 6.
           WRITE TF-RECORD FROM T1-RECORD.

       LIGNE.
           IF CS-IMPRESSION(IDX-1) > 0
              ADD 1 TO IDX
              MOVE CSP-DONNEE(IDX-1) TO T1-VAL(IDX)
              MOVE ";" TO T1-DEL(IDX).

       HEAD-LINE.
           MOVE "AA" TO LNK-AREA.
           MOVE 117 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-ANNEE.
           MOVE 114 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-MOIS.

           MOVE "AY" TO LNK-AREA.
           MOVE  5  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-PERSON.
           MOVE "FI" TO LNK-AREA.
           MOVE  1  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-FIRME.
           MOVE "PR" TO LNK-AREA.
           MOVE  3  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-NOM.
           MOVE "FR" TO LNK-AREA.
           MOVE  25 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-COUT.
           MOVE CS-NOM   TO TXT-TEXT.
           MOVE 0 TO IDX.
           PERFORM TX VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 6.
           COMPUTE MAX-LONGUEUR = 124 + IDX * 21.

        TX.
           IF CS-IMPRESSION(IDX-1) > 0
              ADD 1 TO IDX
              MOVE CS-DESCRIPTION(IDX-1) TO TXT-DES(IDX)
              MOVE ";" TO TXT-DEL(IDX).

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE CS-NUMBER TO HE-Z4.
           DISPLAY HE-Z4  LINE 13 POSITION 29.
           DISPLAY CS-NOM LINE 13 POSITION 35.
       DIS-HE-07.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 19 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-08.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1510 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF ASCII-FILE = 0
              IF COUNTER > 0 
                 IF MENU-BATCH = 0
                    PERFORM FILL-TOTAL
                 END-IF
                 PERFORM TRANSMET
              END-IF
              IF COUNTER > 0
                 MOVE 99 TO LNK-VAL
                 CALL "P130" USING LINK-V FORMULAIRE
                 CANCEL "P130"
              END-IF
           END-IF.
           CANCEL "6-CODSAL".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XSORT.CPY".
           COPY "XACTION.CPY".

