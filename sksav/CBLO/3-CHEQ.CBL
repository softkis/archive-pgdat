      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-CHEQ IMPRESSION CHEQUES                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CHEQ.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "VIREMENT.FC".
           COPY "BANQUE.FC".
           COPY "SPOOLMGR.FC".
           COPY "SPOOLQUE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "VIREMENT.FDE".
           COPY "BANQUE.FDE".
           COPY "SPOOLMGR.FDE".
           COPY "SPOOLQUE.FDE".

       WORKING-STORAGE SECTION.
       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "V-VAR.CPY".
           COPY "BANQF.REC".
           COPY "BANQP.REC".

       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".

       01  HELP-0      PIC 9999.
       01  DATE-VALEUR.
           02  VAL-ANNEE             PIC 9999 VALUE 0.
           02  VAL-MOIS              PIC 99.
           02  VAL-JOUR              PIC 99.

       01  TAB-LIN               PIC 99 COMP-6.
       01  TAB-COL               PIC 99 COMP-6.

       01  LINK-ECRIT.
           02  TOTAL-CHIFFRE     PIC X(100).
           02  REDEF-CHIFFRE REDEFINES TOTAL-CHIFFRE.
               03 CHIFFRE   PIC X OCCURS 100.
           02  TOTAL-VAL         PIC 9(9)V99.
           02  CHIFFRE-POINT     PIC 999 COMP-4.

       01  HELP-TEXTE.
           03 HELP-T   PIC X OCCURS 50.
       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".

       01  SPOOLQ-NAME.
           02  FILLER            PIC X(7) VALUE "SPOOLQ.".
           02  SPOOL-Q-NBR       PIC 999  VALUE 0.
       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIR          PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).

       01  HE-DATE.
           02 HE-JJ PIC ZZ BLANK WHEN ZERO.
           02 FILLER PIC X VALUE ".".
           02 HE-MM PIC ZZ BLANK WHEN ZERO.
           02 FILLER PIC X VALUE ".".
           02 HE-AA PIC ZZZZ BLANK WHEN ZERO.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                VIREMENT
                BANQUE
                SPOOLM
                SPOOLQUEUE.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CHEQ.
       
           MOVE LNK-SUFFIX TO ANNEE-VIR.
           OPEN I-O   VIREMENT.
           OPEN INPUT BANQUE.
           OPEN I-O   SPOOLM.
           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN.
           MOVE 66 TO SAVE-KEY.
           PERFORM NEXT-BANQF.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR (1)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5
           WHEN  6 PERFORM AVANT-6
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT 
           WHEN  5 PERFORM APRES-5 
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-5.
           ACCEPT BQF-CODE
             LINE 12 POSITION 25 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-6.
           MOVE 14250000 TO LNK-POSITION
           IF VAL-ANNEE = 0
              MOVE TODAY-ANNEE TO VAL-ANNEE
              MOVE TODAY-MOIS  TO VAL-MOIS
              MOVE TODAY-JOUR  TO VAL-JOUR
           END-IF.
           CALL "0-GDATE" USING DATE-VALEUR
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-5.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-BANQF" USING LINK-V BQF-RECORD
                   IF BQF-CODE NOT = SPACES
                      MOVE BQF-CODE TO BQ-CODE
                   END-IF
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER MOVE EXC-KEY TO SAVE-KEY
                      PERFORM NEXT-BANQF
           END-EVALUATE.
           MOVE BQF-CODE TO BQ-CODE.
           CALL "0-TODAY" USING TODAY.
           
           IF BQF-CODE = SPACES MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-05.

       APRES-DEC.
            EVALUATE EXC-KEY 
            WHEN  1 MOVE 00201000 TO LNK-VAL
                    PERFORM HELP-SCREEN
                    MOVE 1 TO INPUT-ERROR
            WHEN  5 PERFORM TRAITEMENT
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TRAITEMENT.
           IF A-N = "A" 
           OR A-N = "N" 
              PERFORM START-PERSON
              PERFORM READ-PERSON THRU READ-EXIT
              PERFORM END-PROGRAM
           END-IF. 

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-1
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.

           PERFORM READ-VIREMENT.

       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

           
       READ-VIREMENT.
           INITIALIZE VIR-RECORD.
           MOVE FR-KEY  TO VIR-FIRME-N.
           MOVE LNK-MOIS   TO VIR-MOIS-N.
           MOVE REG-PERSON TO VIR-PERSON-N.
           START VIREMENT KEY >= VIR-KEY-PERSON INVALID KEY CONTINUE
               NOT INVALID
               PERFORM READ-VIREM THRU READ-VIR-END.

       READ-VIREM.
           READ VIREMENT NEXT AT END 
                GO READ-VIR-END.
           IF FR-KEY NOT = VIR-FIRME
           OR VIR-MOIS NOT  = LNK-MOIS 
           OR REG-PERSON NOT = VIR-PERSON
                GO READ-VIR-END.
           IF VIR-DATE-EDITION  NOT = 0
           OR VIR-DATE-VIREMENT NOT = 0
           OR VIR-BANQUE-C NOT = "CQ"
              GO READ-VIREM.
       READ-VIR-1.
           PERFORM DIS-HE-01.
           IF SPOOL-READY = 0 
              PERFORM READ-IMPRIMANTE 
              IF IMPL-PAR(1) = 0
                 MOVE 20 TO IMPL-PAR(1)
              END-IF
              IF IMPL-PAR(3) = 0
                 MOVE 10 TO IMPL-PAR(3)
              END-IF
              IF IMPL-PAR(4) = 0
                 MOVE 6  TO IMPL-PAR(4)
              END-IF
              IF IMPL-PAR(5) = 0
                 MOVE 5  TO IMPL-PAR(5)
              END-IF
              IF IMPL-PAR(6) = 0
                 MOVE 10 TO IMPL-PAR(6)
              END-IF
              IF IMPL-PAR(7) = 0
                 MOVE 11 TO IMPL-PAR(7)
              END-IF
              PERFORM START-SPOOL
           END-IF.
           PERFORM FILL-FILES.
           PERFORM WRITE-SPOOL.
           MOVE TODAY-DATE TO VIR-DATE-VIREMENT.
           MOVE BQ-CODE   TO VIR-BANQUE-D.
           IF LNK-SQL = "Y" 
              CALL "9-VIREM" USING LINK-V VIR-RECORD WR-KEY 
           END-IF.
           REWRITE VIR-RECORD INVALID WRITE VIR-RECORD END-REWRITE.
           GO READ-VIREM.
       READ-VIR-END.
           EXIT.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 0 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
                
       READ-BANQUE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD FAKE-KEY.
       NEXT-BANQF.
           CALL "6-BANQF" USING LINK-V BQF-RECORD SAVE-KEY.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 27 SIZE 6
           DISPLAY PR-NOM LINE 6 POSITION 47 SIZE 33.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 48 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 6 POSITION COL-IDX SIZE IDX LOW.
           IF A-N = "N"
              DISPLAY SPACES LINE 6 POSITION 35 SIZE 10.
       DIS-HE-STAT.
           DISPLAY STAT-NOM  LINE 9 POSITION 45.
       DIS-HE-05.
           MOVE BQF-CODE TO BQ-CODE.
           PERFORM READ-BANQUE.
           DISPLAY BQF-CODE   LINE 12 POSITION 25.
           DISPLAY BQ-NOM     LINE 12 POSITION 35 SIZE 35.
           DISPLAY BQF-COMPTE LINE 13 POSITION 35 SIZE 25.
       DIS-HE-06.
           MOVE VAL-JOUR  TO HE-JJ.
           MOVE VAL-MOIS  TO HE-MM.
           MOVE VAL-ANNEE TO HE-AA.
           DISPLAY HE-DATE  LINE 14 POSITION 25.
       DIS-HE-END.
           EXIT.

       FILL-FILES.
           INITIALIZE FORMULAIRE.
           COMPUTE LIN-NUM = IMPL-PAR(4).
           COMPUTE COL-NUM = IMPL-PAR(5).
           MOVE "EUR" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = IMPL-PAR(4).
           COMPUTE COL-NUM = IMPL-PAR(5) + 5.
           MOVE VIR-A-PAYER TO VH-00 TOTAL-VAL.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = IMPL-PAR(8).
           COMPUTE COL-NUM = IMPL-PAR(9).
           CALL "0-ECRIT" USING LINK-ECRIT.
           PERFORM LIMITE TEST BEFORE 
                   VARYING IDX FROM 39 BY -1 UNTIL IDX < 1.
           ADD 1 TO IDX-1.
           MOVE 0 TO IDX-2.
           INITIALIZE HELP-TEXTE.
           PERFORM RESTES VARYING IDX-1 
                   FROM IDX-1 BY 1 UNTIL IDX-2 = 50.

           MOVE 1 TO IDX.
           STRING TOTAL-CHIFFRE DELIMITED BY "*" INTO ALPHA-TEXTE
           WITH POINTER IDX ON OVERFLOW CONTINUE END-STRING.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = IMPL-PAR(8) + 2.
           COMPUTE COL-NUM = IMPL-PAR(9).
           MOVE HELP-TEXTE TO ALPHA-TEXTE.
           IF ALPHA-TEXTE NOT = SPACES
              PERFORM FILL-FORM.

           COMPUTE LIN-NUM = IMPL-PAR(10).
           COMPUTE COL-NUM = IMPL-PAR(11).
           MOVE FR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = IMPL-PAR(6).
           COMPUTE COL-NUM = IMPL-PAR(7).
           MOVE PR-PRENOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = IMPL-PAR(6) + 1.
           COMPUTE COL-NUM = IMPL-PAR(7).
           MOVE PR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = IMPL-PAR(6) + 2.
           COMPUTE COL-NUM = IMPL-PAR(7).
           MOVE FR-KEY  TO BQP-FIRME.
           MOVE VIR-PERSON TO BQP-PERSON.
           MOVE VIR-TYPE   TO BQP-TYPE.
           MOVE VIR-SUITE  TO BQP-SUITE.
           PERFORM READ-BP. 
           IF BQP-PERSON = 0
           AND VIR-TYPE = 1
              MOVE 0 TO BQP-SUITE BQP-TYPE
              PERFORM READ-BP
           END-IF.
           MOVE BQP-LIBELLE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           PERFORM MOIS-NOM.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE LNK-ANNEE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.


           COMPUTE LIN-NUM = IMPL-PAR(12).
           COMPUTE COL-NUM = IMPL-PAR(13).
           MOVE  2 TO CAR-NUM.
           MOVE VAL-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  3 TO CAR-NUM.
           MOVE VAL-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  5 TO CAR-NUM.
           MOVE VAL-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       READ-BP.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD FAKE-KEY.

       LIMITE.
           IF CHIFFRE(IDX) = " "
              MOVE "*" TO CHIFFRE(IDX) 
              MOVE IDX TO IDX-1
              MOVE 0 TO IDX.

       RESTES.
           ADD 1 TO IDX-2.
           MOVE CHIFFRE(IDX-1) TO HELP-T(IDX-2).

       AFFICHAGE-ECRAN.
           MOVE 1522 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE BANQUE.
           CLOSE VIREMENT.
           COPY "XSPOOL.CPY".
           CLOSE SPOOLM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINTER.CPY".
           COPY "XMOISNOM.CPY".
