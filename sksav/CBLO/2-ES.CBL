      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-ES  RAPPORT ENTREES  SORTIES              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-ES .

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "CONTRAT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM80.FDE".
           COPY "CONTRAT.FDE".

       WORKING-STORAGE SECTION.

       01  CHOIX-MAX         PIC 99 VALUE 1. 
       01  JOB-STANDARD      PIC X(10) VALUE "80        ".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "IMPRLOG.REC".
           COPY "REGISTRE.REC".

           COPY "V-VAR.CPY".
       01  JOUR-IDX              PIC 9.
       01  JOUR                  PIC 99.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

       01  HELP-CUMUL.           
           02 H-F-ENTREE OCCURS 2.
              03 ENTREE-TOT PIC 9999 OCCURS 13.
           02 H-F-SORTIE OCCURS 2.
              03 SORTIE-TOT PIC 9999 OCCURS 13.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".E-S".
           
       01  ECR-DISPLAY.
           02 HE-Z2    PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4    PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6    PIC Z(6) BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                FORM 
                CONTRAT.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-ES.
       
           PERFORM AFFICHAGE-ECRAN .
           OPEN INPUT CONTRAT.

           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.
           MOVE 1 TO INDICE-ZONE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
            UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.


      * param둻res sp괹ifiques touches de fonctions
           MOVE 0000000007 TO EXC-KFR(1).
           MOVE 1700000000 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF ECRAN-IDX = 1 AND EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY = 82
              PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           
           PERFORM APRES-DEC.

           IF INDICE-ZONE > CHOIX-MAX
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN.
           IF EXC-KEY = 5 OR 6
              PERFORM EDITION
           END-IF.

       EDITION.
           PERFORM READ-IMPRIMANTE.
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           PERFORM READ-FORM.
           PERFORM FILL-FILES.
           PERFORM TRANSMET.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.

                 
       CUMUL-ENTREE.
           PERFORM GET-PERSON.
           IF PR-CODE-SEXE > 0
           ADD 1 TO ENTREE-TOT(PR-CODE-SEXE, CON-DEBUT-M) 
                    ENTREE-TOT(PR-CODE-SEXE, 13).

       CUMUL-SORTIE.
           PERFORM GET-PERSON.
           IF PR-CODE-SEXE > 0
           ADD 1 TO SORTIE-TOT(PR-CODE-SEXE, CON-FIN-M) 
                    SORTIE-TOT(PR-CODE-SEXE, 13).

       DIS-FIRME.
           DISPLAY FR-NOM  LINE  3 POSITION  3.
           DISPLAY FR-PAYS LINE  5 POSITION  3.
           MOVE FR-CODE-POST   TO HE-Z6.
           DISPLAY HE-Z6  LINE  5 POSITION  5.
           DISPLAY FR-MAISON   LINE  4 POSITION  7.
           DISPLAY FR-RUE      LINE  4 POSITION 12.
           DISPLAY FR-LOCALITE LINE  5 POSITION 12.

       DIS-E1-01.
           PERFORM DIS-LIGNE VARYING IDX FROM 1 BY 1 UNTIL IDX > 13.

       DIS-HE-END.
           EXIT.
        
       DIS-LIGNE.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE ENTREE-TOT(1, IDX) TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 22.
           MOVE ENTREE-TOT(2, IDX) TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 32.
           COMPUTE VH-00 = ENTREE-TOT(1, IDX) + ENTREE-TOT(2, IDX).
           MOVE VH-00 TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 42.

           MOVE SORTIE-TOT(1, IDX) TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 52.
           MOVE SORTIE-TOT(2, IDX) TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 62.
           COMPUTE VH-00 = SORTIE-TOT(1, IDX) + SORTIE-TOT(2, IDX).
           MOVE VH-00 TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 72.
            
       AFFICHAGE-ECRAN.
           MOVE 2230 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           PERFORM DIS-FIRME.

       AFFICHAGE-DETAIL.
           PERFORM DIS-FIRME THRU DIS-HE-END.
           
       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XACTION.CPY".
           
    
       FILL-FILES.
           MOVE 03 TO LIN-NUM.
           MOVE LNK-ANNEE TO ALPHA-TEXTE.
           MOVE 70 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 05 TO LIN-NUM.
           MOVE 20 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           PERFORM FILL-LIGNE VARYING IDX FROM 1 BY 1 UNTIL IDX > 13.

       FILL-LIGNE.
           COMPUTE LIN-NUM = (IDX * 2) + 12.
           MOVE ENTREE-TOT(1, IDX) TO VH-00.
           MOVE 22 TO COL-NUM.
           PERFORM FILL-VAL.
           MOVE ENTREE-TOT(2, IDX) TO VH-00.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-VAL.
           COMPUTE VH-00 = ENTREE-TOT(1, IDX) + ENTREE-TOT(2, IDX).
           MOVE 42 TO COL-NUM.
           PERFORM FILL-VAL.

           MOVE SORTIE-TOT(1, IDX) TO VH-00.
           MOVE 52 TO COL-NUM.
           PERFORM FILL-VAL.
           MOVE SORTIE-TOT(2, IDX) TO VH-00.
           MOVE 62 TO COL-NUM.
           PERFORM FILL-VAL.
           COMPUTE VH-00 = SORTIE-TOT(1, IDX) + SORTIE-TOT(2, IDX).
           MOVE 72 TO COL-NUM.
           PERFORM FILL-VAL.

       FILL-VAL.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.


       AFFICHE-DEBUT.
           INITIALIZE CON-RECORD.
           MOVE FR-KEY TO CON-FIRME.
           START CONTRAT KEY > CON-KEY INVALID GO AFFICHE-END.
           PERFORM READ-CON THRU READ-CON-END.
       AFFICHE-END.
           EXIT.

       READ-CON.
           READ CONTRAT NEXT NO LOCK AT END GO READ-CON-END END-READ.
           IF FR-KEY NOT = CON-FIRME 
              GO READ-CON-END.
           IF LNK-ANNEE = CON-DEBUT-A
              PERFORM CUMUL-ENTREE
           END-IF
           IF LNK-ANNEE = CON-FIN-A
              PERFORM CUMUL-SORTIE
           END-IF
           GO READ-CON.
       READ-CON-END.
           PERFORM DIS-E1-01.

       GET-PERSON.
           MOVE CON-PERSON TO REG-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           MOVE 99 TO LNK-VAL
           CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           MOVE " " TO LNK-YN.
