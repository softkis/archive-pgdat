      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-BQP LISTE BANQUES  PERSONNELLE            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-BQP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".
           COPY "VIREMENT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM130.FDE".
           COPY "VIREMENT.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  JOB-STANDARD          PIC X(10) VALUE "130       ".
       01  PRECISION             PIC 9 VALUE 1.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".
           COPY "BANQP.REC".
           COPY "COUT.REC".
           COPY "IMPRLOG.REC".
           COPY "STATUT.REC".
           COPY "PARMOD.REC".

       01  LANGUE                PIC 9.
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  FEUILLE               PIC X VALUE "N".
       01  COMPTEUR              PIC 99 VALUE 0.
       01  RESULTAT              PIC 99 VALUE 0.

       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIR          PIC 999.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".BQP".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).

       01 HE-SEL.
          02 H-S PIC X     OCCURS 20.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM VIREMENT.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-BQP.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-SETTINGS TO HE-SEL.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           INITIALIZE LIN-NUM LIN-IDX.

           CALL "0-TODAY" USING TODAY.
           MOVE LNK-ANNEE TO SAVE-ANNEE.
           MOVE LNK-MOIS  TO SAVE-MOIS.
           PERFORM AFFICHAGE-ECRAN.
           MOVE LNK-SUFFIX TO ANNEE-VIR.
           OPEN INPUT VIREMENT.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT FEUILLE
             LINE 12 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-6.
           IF FEUILLE = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
              WHEN  5 PERFORM TRAITEMENT
              WHEN 68 MOVE "VV" TO LNK-AREA
                  CALL "5-SEL" USING LINK-V PARMOD-RECORD
                  MOVE PARMOD-SETTINGS TO HE-SEL
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
                  PERFORM DISPLAY-F-KEYS
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-PERSON-END.
           PERFORM END-PROGRAM.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           MOVE SAVE-ANNEE TO LNK-ANNEE.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-PERSON-END
           END-IF.

       READ-PERSON-1.
           PERFORM DIS-HE-01.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD BQP-RECORD NUL-KEY.
           IF STATUT > 0 AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF COUT > 0 AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           MOVE LNK-LANGUAGE TO PR-LANGUAGE.
           PERFORM START-BQP.
           IF FEUILLE NOT = "N"
           AND RESULTAT > 0
              PERFORM TRANSMET
           END-IF.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
               GO READ-PERSON.
       READ-PERSON-END.
           PERFORM END-PROGRAM.

       START-BQP.
           INITIALIZE BQP-RECORD EXC-KEY RESULTAT.
           PERFORM NEXT-BQP THRU NEXT-BQP-END.

       NEXT-BQP.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD EXC-KEY.
           MOVE 66 TO EXC-KEY.
           IF BQP-FIRME = 0
              GO NEXT-BQP-END.
           COMPUTE IDX-1 = BQP-TYPE + 1.
           IF H-S(IDX-1) = "N"
              GO NEXT-BQP
           END-IF.
           IF  BQP-FIN-A > 0
           AND BQP-FIN-A < LNK-ANNEE
              GO NEXT-BQP
           END-IF.
           IF  BQP-FIN-M < LNK-MOIS
           AND BQP-FIN-A = LNK-ANNEE
              GO NEXT-BQP
           END-IF.

           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              ADD 1 TO COUNTER
           END-IF.
           PERFORM TEST-IMPRIMANTE.
           PERFORM FILL-FILES.
           GO NEXT-BQP.
       NEXT-BQP-END.

       TEST-IMPRIMANTE.
           COMPUTE IDX = LIN-NUM + 5.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM LAST-PERSON
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM FILL-FIRME
              MOVE LIN-IDX TO LIN-NUM
              SUBTRACT 2 FROM LIN-NUM
           END-IF.
           IF REG-PERSON NOT = LAST-PERSON
              PERFORM DIS-HE-01
              PERFORM DONNEES-PERSONNE.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 0 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       FILL-FIRME.
      *    PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  2 TO CAR-NUM.
           MOVE  4 TO LIN-NUM.
           MOVE 122 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

           MOVE  2 TO LIN-NUM.
           MOVE 50 TO COL-NUM.
           MOVE SAVE-MOIS  TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE SAVE-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE  2 TO CAR-NUM.
           MOVE 111 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 114 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 117 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE 2 TO LIN-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE 8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           MOVE 15 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 3 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 4 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-CODE-POST TO VH-00.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       DONNEES-PERSONNE.
           ADD 2 TO LIN-NUM.
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  6 TO COL-NUM.
           MOVE REG-PERSON TO VH-00 LAST-PERSON.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.

       FILL-FILES.
           ADD 1 TO LIN-NUM RESULTAT.
           MOVE BQP-TYPE TO LNK-NUM VH-00.
           MOVE "VV" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE 5 TO COL-NUM.
           MOVE 2 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           MOVE 10 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE BQP-SUITE TO VH-00.
           MOVE 25 TO COL-NUM.
           MOVE 2 TO CAR-NUM.
           PERFORM FILL-FORM.

           MOVE BQP-BANQUE TO ALPHA-TEXTE.
           MOVE 30 TO COL-NUM.
           PERFORM FILL-FORM.
           

           MOVE BQP-DEBUT-M  TO VH-00.
           MOVE 60 TO COL-NUM.
           MOVE 2 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE BQP-DEBUT-A TO VH-00.
           MOVE 64 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.

           MOVE BQP-FIN-M  TO VH-00.
           MOVE 70 TO COL-NUM.
           MOVE 2 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE BQP-FIN-A TO VH-00.
           MOVE 74 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.

           MOVE BQP-TOTAL TO VH-00.
           MOVE 80 TO COL-NUM.
           MOVE 6 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.
           MOVE BQP-PROPOSITION TO VH-00.
           MOVE 90 TO COL-NUM.
           MOVE 6 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

           IF BQP-TYPE > 1
           AND BQP-TYPE < 7
              PERFORM AJUST-TOTAL
              MOVE 100 TO COL-NUM
              MOVE 6 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              PERFORM FILL-FORM.

           IF BQP-TYPE > 1
           AND BQP-TYPE < 7
           AND BQP-TOTAL > 0
              COMPUTE VH-00 = BQP-TOTAL - VH-00
              MOVE 113 TO COL-NUM
              MOVE 6 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              PERFORM FILL-FORM.

           IF BQP-TYPE > 0
              PERFORM 2-LIGNE.
           MOVE BQP-COMPTE TO ALPHA-TEXTE.
           MOVE 105 TO COL-NUM.
           PERFORM FILL-FORM.

       2-LIGNE.
           ADD 1 TO LIN-NUM.
           MOVE 10 TO COL-NUM.
           MOVE BQP-LIBELLE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 65 TO COL-NUM.
           MOVE BQP-BENEFIC-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       AJUST-TOTAL.
           INITIALIZE VIR-RECORD VH-00.
           MOVE BQP-FIRME  TO VIR-FIRME.
           MOVE BQP-PERSON TO VIR-PERSON.
           MOVE BQP-TYPE   TO VIR-TYPE.
           MOVE BQP-SUITE  TO VIR-SUITE.
           START VIREMENT KEY >= VIR-KEY INVALID CONTINUE
              NOT INVALID
              PERFORM READ-VIREM THRU READ-VIR-END.

       READ-VIREM.
           READ VIREMENT NEXT AT END 
                GO READ-VIR-END.
           IF VIR-SUITE  NOT = BQP-SUITE
           OR VIR-TYPE   NOT = BQP-TYPE
           OR FR-KEY     NOT = VIR-FIRME
           OR REG-PERSON NOT = VIR-PERSON
           OR VIR-MOIS >= LNK-MOIS 
              GO READ-VIR-END.
           ADD VIR-A-PAYER TO VH-00.
           GO READ-VIREM.
       READ-VIR-END.
           EXIT.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 1207 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           MOVE 11 TO LIN-IDX.
           MOVE 0 TO COMPTEUR.
           PERFORM AFFICHE-D THRU AFFICHE-END.

       AFFICHE-D.
           MOVE COMPTEUR TO LNK-NUM.
           ADD 1 TO LIN-IDX.
           MOVE "VV" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           IF LNK-TEXT = SPACES
              GO AFFICHE-END.
           ADD 1 TO COMPTEUR.
           IF H-S(COMPTEUR) NOT = "N"
              DISPLAY LNK-TEXT LINE LIN-IDX POSITION 60 SIZE 19 REVERSE
           ELSE
              DISPLAY LNK-TEXT LINE LIN-IDX POSITION 62 SIZE 19 LOW.
           GO AFFICHE-D.
       AFFICHE-END.
           EXIT.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           MOVE SAVE-ANNEE TO LNK-ANNEE.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".



