      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-DEDS DECLARATION ENTREE SORTIE      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-DEDS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.

           COPY "CONTRAT.FC".
      *    Fichier DEDS

           SELECT OPTIONAL DEDS ASSIGN TO DISK PARMOD-PATH-REAL
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-HELP.

       DATA DIVISION.


       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CONTRAT.FDE".

       FD  DEDS
           RECORD VARYING DEPENDING IDX-4
           DATA RECORD IS DEDS-REC.
       01  DEDS-REC.
           02  DEDS-X  PIC X OCCURS 1 TO 270 DEPENDING IDX-4.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  PRECISION             PIC 9 VALUE 0.
       01  ADRES                 PIC X(55) 
           VALUE "X:\CETREL\SOFIE\DATA\123456789\TO_CRYPT\DEDS.DTA".


      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "MESSAGE.REC".
           COPY "METIER.REC".
           COPY "POCL.REC".
           COPY "PARMOD.REC".

       01  COMPTEUR              PIC 99 COMP-1.
       01  HEURES                PIC 99.
       01  SAVE-FIRME            PIC 9(6).
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  ANNEE-LIMITE          PIC 9999.

       01  NOT-OPEN              PIC 9 VALUE 0.

       01  REC-MATR.
           02  REC-MAT PIC X(28).
           02  REC-MATR-R REDEFINES REC-MAT.
               10  MATR-ID1         PIC 9(13).
               10  MATR-CONVENTION  PIC X(6).
               10  MATR-SECULINE    PIC 9(9).

       01  HEADER.
           02 HD-TYPE        PIC 9 VALUE 0.
           02 HD-DELIM       PIC X VALUE ";".
           02 HD-MATR        PIC 9(13).
           02 HD-DELIM       PIC X VALUE ";".
           02 HD-CONVENTION  PIC 9(6).

       01  DEDS-ENTREE.
1          02 EN-TYPE           PIC 9 VALUE 1.
           02 EN-DELIM          PIC X VALUE ";".
2          02 EN-MATR           PIC 9(13).
           02 EN-DELIM          PIC X VALUE ";".
3          02 EN-SNOCS          PIC 9(11).
           02 EN-DELIM          PIC X VALUE ";".
4          02 EN-PAYS           PIC XXX.
           02 EN-DELIM          PIC X VALUE ";".
5          02 EN-POST           PIC X(5).
           02 EN-DELIM          PIC X VALUE ";".
6          02 EN-LOC            PIC X(24).
           02 EN-DELIM          PIC X VALUE ";".
7          02 EN-MAISON         PIC X(4).
           02 EN-DELIM          PIC X VALUE ";".
8          02 EN-RUE            PIC X(20).
           02 EN-DELIM          PIC X VALUE ";".
9          02 EN-METIER         PIC X(20).
           02 EN-DELIM          PIC X VALUE ";".
10         02 EN-PROF           PIC 9(4).
           02 EN-DELIM          PIC X VALUE ";".
11         02 EN-REGIME         PIC 99.
           02 EN-DELIM          PIC X VALUE ";".
12         02 EN-HRS            PIC 99.
           02 EN-DELIM          PIC X VALUE ";".
13         02 EN-CONTRAT        PIC X.
           02 EN-DELIM          PIC X VALUE ";".
14         02 EN-ENTREE         PIC 9(8).
           02 EN-DELIM          PIC X VALUE ";".
15         02 EN-SORTIE         PIC 9(8).
           02 EN-DELIM          PIC X VALUE ";".
16         02 EN-DEPART         PIC 99.
           02 EN-DELIM          PIC X VALUE ";".
17         02 EN-PAYS-1         PIC XXX.
           02 EN-DELIM          PIC X VALUE ";".
18         02 EN-PAYS-2         PIC XXX.
           02 EN-DELIM          PIC X VALUE ";".
19         02 EN-PAYS-3         PIC XXX.
           02 EN-DELIM          PIC X VALUE ";".
20         02 EN-PAYS-4         PIC XXX.
           02 EN-DELIM          PIC X VALUE ";".
21         02 EN-PAYS-5         PIC XXX.
           02 EN-DELIM          PIC X VALUE ";".
22         02 EN-PAYS-6         PIC XXX.
           02 EN-DELIM          PIC X VALUE ";".
23         02 EN-PAYS-7         PIC X VALUE SPACES.
           02 EN-DELIM          PIC X VALUE ";".
24         02 EN-POST-1         PIC X(5).
           02 EN-DELIM          PIC X VALUE ";".
25         02 EN-LOC-1          PIC X(24).
           02 EN-DELIM          PIC X VALUE ";".
26         02 EN-AUTOR          PIC X VALUE SPACES.
           02 EN-DELIM          PIC X VALUE ";".
27         02 EN-PARTS          PIC 999.
           02 EN-DELIM          PIC X VALUE ";".
28         02 EN-GERANT         PIC X VALUE SPACES.
           02 EN-DELIM          PIC X VALUE ";".
29         02 EN-PRERET         PIC X VALUE SPACES.
           02 EN-DELIM          PIC X VALUE ";".
30         02 EN-RELIGION       PIC X VALUE SPACES.
           02 EN-DELIM          PIC X VALUE ";".
31         02 EN-ETAT           PIC X(6) VALUE SPACES.
           02 EN-DELIM          PIC X VALUE ";".
32         02 EN-AGAUX          PIC X VALUE SPACES.
           02 EN-DELIM          PIC X VALUE ";".
33         02 EN-BCL            PIC X VALUE SPACES.
           02 EN-DELIM          PIC X VALUE ";".
34         02 EN-FIRME          PIC 9(4).
           02 EN-PERSON         PIC 9(6).
           02 EN-NOM            PIC X(20) VALUE SPACES.
           02 EN-DELIM          PIC X VALUE ";".
35         02 EN-RECTIF         PIC 9(8).


       01  DEDS-SORTIE.
1          02 SO-TYPE           PIC 9 VALUE 2.
           02 SO-DELIM          PIC X VALUE ";".
2          02 SO-MATR           PIC 9(13).
           02 SO-DELIM          PIC X VALUE ";".
3          02 SO-SNOCS          PIC 9(11).
           02 SO-DELIM          PIC X VALUE ";".
4          02 SO-PAYS           PIC XXX.
           02 SO-DELIM          PIC X VALUE ";".
5          02 SO-POST           PIC X(5).
           02 SO-DELIM          PIC X VALUE ";".
6          02 SO-LOC            PIC X(24).
           02 SO-DELIM          PIC X VALUE ";".
7          02 SO-MAISON         PIC X(4).
           02 SO-DELIM          PIC X VALUE ";".
8          02 SO-RUE            PIC X(20).
           02 SO-DELIM          PIC X VALUE ";".
9          02 SO-SORTIE         PIC 9(8).
           02 SO-DELIM          PIC X VALUE ";".
10         02 SO-DEPART         PIC 99.
           02 SO-DELIM          PIC X VALUE ";".
11         02 SO-FIRME          PIC 9(4).
           02 SO-PERSON         PIC 9(6).
           02 SO-NOM            PIC X(20) VALUE SPACES.
           02 SO-DELIM          PIC X VALUE ";".
12         02 SO-RECTIF         PIC 9(8).


           COPY "V-VAR.CPY".
        
       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-X8 PIC 9(8).
           02 HE-XX REDEFINES HE-X8.
              03 HE-XA PIC 9.
              03 HE-XB PIC 9.
              03 HE-XC PIC 9.
              03 HE-XD PIC 9.
              03 HE-XE PIC 9.
              03 HE-XF PIC 9.
              03 HE-XG PIC 9.
              03 HE-XH PIC 9.
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.


           02 HE-DEDS.
              03 HE-A PIC ZZZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HE-M PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HE-J PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HE-S PIC ZZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CONTRAT DEDS.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-DEDS.
       
           IF LNK-ANNEE > 2008
              EXIT PROGRAM.

           INITIALIZE PARMOD-RECORD.
           MOVE "SECULINE" TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-MATR TO REC-MATR.

           MOVE FR-KEY TO SAVE-FIRME.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           IF MENU-PROG-NUMBER = 1 
              MOVE LNK-USER TO PARMOD-USER.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           IF PARMOD-PATH3 = SPACES
              MOVE PARMOD-SETTINGS TO PARMOD-MATR
              MOVE PARMOD-PATH2 TO PARMOD-PATH1
              MOVE SPACES TO PARMOD-PATH2 
           END-IF.

           MOVE LNK-LANGUAGE TO SAVE-LANGUAGE.
           CALL "0-TODAY" USING TODAY.
           OPEN I-O   CONTRAT.
           MOVE LNK-MOIS  TO SAVE-MOIS.
           MOVE LNK-ANNEE TO SAVE-ANNEE ANNEE-LIMITE.
           SUBTRACT 1 FROM ANNEE-LIMITE.
           MOVE 1 TO LNK-PRESENCE.

           INITIALIZE FR-RECORD.
           PERFORM AFFICHAGE-ECRAN .
           MOVE 500 TO IDX-4.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-4
           WHEN  5 PERFORM AVANT-5
           WHEN  6 PERFORM AVANT-PATH
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  6 PERFORM APRES-PATH
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-4.
           IF MATR-ID1 < 1
              MOVE FR-DATE-ETAB TO MATR-ID1.
           ACCEPT MATR-ID1
             LINE 11 POSITION 25 SIZE 13
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "NUMERIC"
             ON EXCEPTION  EXC-KEY  CONTINUE.
             IF EXC-KEY = 12
                MOVE FR-DATE-ETAB TO MATR-ID1.

       AVANT-5.
           ACCEPT MATR-CONVENTION
             LINE 12 POSITION 25 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "NUMERIC"
             ON EXCEPTION  EXC-KEY CONTINUE.
           INSPECT REC-MATR REPLACING ALL " " BY "0".

       AVANT-PATH.
           IF PARMOD-PATH-PROTO = SPACES
              MOVE ADRES TO PARMOD-PATH-PROTO
              INSPECT PARMOD-PATH-PROTO REPLACING ALL "123456789"
              BY MATR-SECULINE
           END-IF.
           MOVE 15176300 TO LNK-POSITION.
           MOVE SPACES TO LNK-LOW.
           CALL "0-GPATH" USING LINK-V PARMOD-RECORD EXC-KEY.
           IF EXC-KEY = 12
              MOVE ADRES TO PARMOD-PATH-PROTO
              INSPECT PARMOD-PATH-PROTO REPLACING ALL "123456789"
              BY MATR-SECULINE
              GO AVANT-PATH
           END-IF.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-PATH.
           IF LNK-LOW = "!" 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF LNK-NUM > 0 
              MOVE  0 TO LNK-POSITION
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE "N" TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       TRAITEMENT.
           PERFORM START-FIRME.
           IF INPUT-ERROR = 0 
              PERFORM READ-FIRME THRU READ-FIRME-END.

       START-FIRME.
           IF FR-KEY > 0 
              SUBTRACT 1 FROM FR-KEY.
           IF A-N = "A"
              MOVE 9999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-FIRME.
           PERFORM NEXT-FIRME.
           IF FR-KEY = 0
           OR FR-KEY > END-NUMBER
           OR FR-MATCHCODE > END-MATCHCODE
              GO READ-FIRME-END
           END-IF.
           IF FR-FIN-A > 0 AND < ANNEE-LIMITE
              GO READ-FIRME
           END-IF.
           IF FR-SNOCS = 0
           OR FR-SNOCS-YN = "N"
              GO READ-FIRME
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM START-PR.
           GO READ-FIRME.
       READ-FIRME-END.
           EXIT.

       NEXT-FIRME.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.
           
       START-PR.
           INITIALIZE REG-RECORD.
           MOVE FR-KEY TO REG-FIRME-A REG-FIRME.
           PERFORM READ-PERSON THRU READ-EXIT.
           MOVE "F" TO LNK-LANGUAGE.

       READ-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY.
           IF  REG-PERSON > 0
           AND REG-SNOCS < 3
               GO READ-PERSON
           END-IF.
           IF REG-PERSON = 0
              GO READ-EXIT.
           IF REG-SNOCS-YN = "N"
              GO READ-PERSON
           END-IF.
           PERFORM CONTRAT THRU CONTRAT-END.
           GO READ-PERSON.
       READ-EXIT.
           EXIT.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE SAVE-ANNEE TO CON-DEBUT-A.
           MOVE 12        TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE FR-KEY TO CON-FIRME.
           MOVE REG-PERSON TO CON-PERSON.
           START CONTRAT KEY < CON-KEY INVALID GO CONTRAT-END.
       CONTRAT-1.
           READ CONTRAT PREVIOUS NO LOCK AT END GO CONTRAT-END END-READ.
           IF FR-KEY     NOT = CON-FIRME 
           OR REG-PERSON NOT = CON-PERSON
              GO CONTRAT-END.

           PERFORM  DIS-HE-00.
           IF  CON-DEBUT-A  > 0
           AND CON-NOTIFE-A > 0
           AND CON-FIN-A    > 0
           AND CON-NOTIFS-A > 0
              GO CONTRAT-END
           END-IF.
           IF CON-NOTIFE-A = 0
           IF CON-DEBUT-A >= ANNEE-LIMITE
              PERFORM ENTREE
           END-IF.
           IF  CON-FIN-A    > 0
           AND CON-NOTIFS-A = 0
              PERFORM SORTIE
           END-IF.
           GO CONTRAT-1.
       CONTRAT-END.
           EXIT.

       ENTREE.
           MOVE CON-DEBUT-A TO LNK-ANNEE.
           MOVE CON-DEBUT-M TO LNK-MOIS.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF CAR-REGIME = 0
              GO CONTRAT-1.
           MOVE CAR-METIER  TO MET-CODE.
           MOVE PR-LANGUAGE TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           IF MET-PROF > 1
           OR CAR-PROF > 1
              PERFORM TEXTE-E.

       SORTIE.
           EVALUATE CON-MOTIF-DEPART 
              WHEN 1 PERFORM TEXTE-S
              WHEN 2 PERFORM TEXTE-S
              WHEN 3 PERFORM TEXTE-S
              WHEN 5 PERFORM TEXTE-S
              WHEN 6 PERFORM TEXTE-S
              WHEN 9 PERFORM TEXTE-S
           END-EVALUATE.
       TEXTE-E.
           MOVE TODAY-DATE TO CON-DATE-NOTIFE.
           PERFORM GET-PERS.
           MOVE 1 TO EN-TYPE.
           MOVE FR-KEY TO EN-FIRME.
           MOVE REG-PERSON TO EN-PERSON.
           MOVE FR-DATE-ETAB TO EN-MATR.
           MOVE PR-MATRICULE TO EN-SNOCS.
           MOVE CAR-REGIME TO EN-REGIME
           MOVE PR-PAYS TO EN-PAYS.
           MOVE PR-NOM  TO EN-NOM.
           MOVE PR-LOCALITE TO EN-LOC.
           MOVE PR-MAISON TO EN-MAISON.
           MOVE PR-RUE    TO EN-RUE.
           MOVE PR-CP5 TO EN-POST.

           COMPUTE HEURES = CAR-HRS-JOUR * CAR-JRS-SEMAINE.
           MOVE HEURES TO EN-HRS.
           MOVE MET-NOM(PR-CODE-SEXE) TO EN-METIER.
           IF EN-METIER = SPACES
              MOVE CAR-POSITION TO EN-METIER
           END-IF.
           MOVE CON-DATE-DEBUT TO EN-ENTREE.
           MOVE CON-DATE-FIN   TO EN-SORTIE.
           IF CON-FIN-A > 0
              MOVE TODAY-DATE TO CON-DATE-NOTIFS
              MOVE CON-MOTIF-DEPART TO EN-DEPART
           END-IF.
           MOVE CAR-PROF TO EN-PROF.
           IF CAR-PROF < 1
              MOVE MET-PROF TO EN-PROF.
           INITIALIZE EN-PARTS EN-GERANT EN-AUTOR EN-PRERET.
           IF REG-AUT-COMMERCE NOT = "N" AND NOT = " "
              MOVE "Y" TO EN-AUTOR
           END-IF.
           IF REG-GERANT NOT = "N" AND NOT = " "
              MOVE "Y" TO EN-GERANT
           END-IF.
           IF REG-PARTS > 0
              MOVE REG-PARTS TO EN-PARTS
           END-IF.
           MOVE REG-PAYS(1) TO EN-PAYS-1.
           MOVE REG-PAYS(2) TO EN-PAYS-2.
           MOVE REG-PAYS(3) TO EN-PAYS-3.
           MOVE REG-PAYS(4) TO EN-PAYS-4.
           MOVE REG-PAYS(5) TO EN-PAYS-5.
           MOVE REG-PAYS(6) TO EN-PAYS-6.
           IF REG-PAYS(7) > SPACES
              MOVE "Y" TO EN-PAYS-7
           END-IF.
           IF REG-PAYS(1) = SPACES
              MOVE "L" TO EN-PAYS-1
           END-IF.
           IF REG-CDPOST > SPACES
              MOVE REG-CDPOST TO EN-POST-1
           END-IF.
           IF REG-LOCALITE > SPACES
              MOVE REG-LOCALITE TO EN-LOC-1
           END-IF.
           IF CAR-POSTE-FRAIS NOT = 0
              MOVE CAR-POSTE-FRAIS TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              IF PC-CODE-POST > 0
              AND PC-LOCALITE > SPACES
                 MOVE PC-CODE-POST TO EN-POST-1
                 MOVE PC-LOCALITE  TO EN-LOC-1
              END-IF
           END-IF.
           MOVE SPACES TO EN-CONTRAT.
           IF CON-FIN-A > 0
              MOVE "D" TO EN-CONTRAT
           END-IF.
           IF CAR-STATEC = 1 
              MOVE "A" TO EN-CONTRAT
           END-IF.
           IF CAR-METIER = "STAGE"
              IF  CAR-SAL-ACT = 0
              AND CAR-INDEXE  = "N"
                 MOVE "N" TO EN-CONTRAT
              ELSE
                 MOVE "S" TO EN-CONTRAT
              END-IF
           END-IF.
           MOVE 0 TO IDX-1.
           INSPECT CAR-METIER TALLYING IDX-1 FOR CHARACTERS BEFORE "ET".
           IF  IDX-1 = 0
           AND CAR-REGIME = 5
              MOVE "E" TO EN-CONTRAT
           END-IF.
           
           IF NOT-OPEN = 0
              PERFORM OPEN-FILE.
           MOVE 254 TO IDX-4.
           IF CON-DATE-RECT1 > 0
              MOVE CON-DATE-RECT1 TO EN-RECTIF
              MOVE 0 TO CON-DATE-RECT1
              MOVE 3 TO EN-TYPE
              MOVE SPACES TO CON-RECT-ENTREE
              MOVE 263 TO IDX-4.
           WRITE DEDS-REC FROM DEDS-ENTREE.
           REWRITE CON-RECORD INVALID CONTINUE.
           IF LNK-SQL = "Y" 
              CALL "9-CONTR" USING LINK-V CON-RECORD WR-KEY 
           END-IF.

       
       OPEN-FILE.
           IF IDX = 1
              OPEN EXTEND DEDS
           ELSE
              OPEN OUTPUT DEDS
              MOVE MATR-ID1 TO HD-MATR
              MOVE MATR-CONVENTION TO HD-CONVENTION
              MOVE 22 TO IDX-4
              WRITE DEDS-REC FROM HEADER
              DISPLAY DEDS-REC LINE 24 POSITION 25 SIZE 35
              IF FS-HELP = "30"
                 MOVE 1 TO INPUT-ERROR
              END-IF
           END-IF.
           MOVE 1 TO NOT-OPEN.

       TEXTE-S.
           MOVE TODAY-DATE TO CON-DATE-NOTIFS.
           PERFORM GET-PERS.
           MOVE 2 TO SO-TYPE.
           MOVE FR-KEY       TO SO-FIRME.
           MOVE REG-PERSON   TO SO-PERSON.
           MOVE FR-DATE-ETAB TO SO-MATR.
           MOVE PR-MATRICULE TO SO-SNOCS.
           MOVE PR-PAYS      TO SO-PAYS.
           MOVE PR-NOM       TO SO-NOM.
           MOVE PR-LOCALITE  TO SO-LOC.
           MOVE PR-MAISON    TO SO-MAISON.
           MOVE PR-RUE       TO SO-RUE.
           MOVE PR-CP5       TO SO-POST.
           MOVE CON-DATE-FIN TO SO-SORTIE.
           MOVE CON-MOTIF-DEPART TO SO-DEPART.

           IF NOT-OPEN = 0
              PERFORM OPEN-FILE.
           MOVE 131 TO IDX-4.
           IF CON-DATE-RECT2 > 0
              MOVE CON-DATE-RECT2 TO SO-RECTIF
              MOVE 0 TO CON-DATE-RECT2
              MOVE 4 TO SO-TYPE
              MOVE SPACES TO CON-RECT-DEPART
              MOVE 140 TO IDX-4.
           WRITE DEDS-REC FROM DEDS-SORTIE.
           REWRITE CON-RECORD INVALID CONTINUE.
           IF LNK-SQL = "Y" 
              CALL "9-CONTR" USING LINK-V CON-RECORD WR-KEY 
           END-IF.

       DIS-HE-00.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 16 POSITION 27 SIZE 6
           MOVE 0 TO COL-IDX
           IF  FR-NOM-JF = SPACES
           AND PR-NOM-JF = SPACES
              DISPLAY PR-NOM LINE 16 POSITION 47 SIZE 33
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "   "
           ELSE
              DISPLAY PR-NOM-JF LINE 16 POSITION 47 SIZE 33
           INSPECT PR-NOM-JF TALLYING COL-IDX FOR CHARACTERS BEFORE "  "
           END-IF.
           ADD 48 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 16 POSITION COL-IDX SIZE IDX LOW.
           IF A-N = "N"
              DISPLAY SPACES LINE 16 POSITION 35 SIZE 10.
       DIS-HE-01.
           MOVE FR-KEY    TO HE-Z6.
           DISPLAY HE-Z6  LINE  6 POSITION 17.
           DISPLAY FR-NOM LINE  6 POSITION 25 SIZE 30.
       DIS-HE-END.

       AFFICHAGE-ECRAN.
           MOVE 706 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE SAVE-LANGUAGE TO LNK-LANGUAGE.
           MOVE SAVE-FIRME TO FR-KEY.
           MOVE SAVE-ANNEE TO LNK-ANNEE.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           CALL "6-FIRME" USING LINK-V A-N FAKE-KEY.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXF".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


