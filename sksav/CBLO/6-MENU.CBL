      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-MENU MODULE GENERAL LECTURE MENU          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-MENU.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MENU.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "MENU.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  ACTION   PIC X.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "MENU.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING  LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MENU.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-MENU.
       
           IF NOT-OPEN = 0
              OPEN I-O MENU
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO MN-RECORD.
           IF EXC-KEY = 99
              WRITE MN-RECORD INVALID REWRITE MN-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.

           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ MENU PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ MENU NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ MENU NO LOCK INVALID INITIALIZE 
           MN-BODY END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE MN-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START MENU KEY < MN-KEY INVALID GO EXIT-1.
       START-2.
           START MENU KEY > MN-KEY INVALID GO EXIT-1.


