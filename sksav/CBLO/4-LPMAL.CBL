      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-LPMAL CUMUL MALADIES PAIEES MOIS EN COURS �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-LPMAL.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

           COPY "LIVRE.REC".
           COPY "V-VAR.CPY".

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "LIVRE.CUM".

       PROCEDURE DIVISION USING 
                          LINK-V 
                          REG-RECORD 
                          LCUM-RECORD.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-LPMAL.

           INITIALIZE L-RECORD.
           MOVE LNK-MOIS TO L-MOIS.
           PERFORM READ-LIVRE THRU READ-LIVRE-END.

       READ-LIVRE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD NX-KEY.
           IF L-FIRME = 0
           OR L-MOIS > LNK-MOIS
      *    OR L-SUITE = 10
              GO READ-LIVRE-END
           END-IF.
           IF L-SUITE = 0
              GO READ-LIVRE
           END-IF.
      *    IF L-FLAG-AVANCE NOT = 0
      *       GO READ-LIVRE
      *    END-IF.

           MOVE 4 TO IDX-1
           PERFORM CUMUL.
           IF L-FLAG-AVANCE = 0
              MOVE 5 TO IDX-1
              PERFORM CUMUL.
           GO READ-LIVRE.
       READ-LIVRE-END.
           EXIT PROGRAM.

       CUMUL.
           PERFORM CUMUL-DC  VARYING IDX FROM 1 BY 1 UNTIL IDX > 300.
           PERFORM CUMUL-UN  VARYING IDX FROM 1 BY 1 UNTIL IDX > 200.
           PERFORM CUMUL-JRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 70.
           PERFORM CUMUL-COT VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.
           PERFORM CUMUL-COT VARYING IDX FROM 31 BY 1 UNTIL IDX > 70.
           IF L-SUITE = 0
              PERFORM CUMUL-COT VARYING IDX FROM 21 BY 1 UNTIL IDX > 30.
           PERFORM CUMUL-ABA VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           PERFORM CUMUL-IMP VARYING IDX FROM 1 BY 1 UNTIL IDX > 40.
           PERFORM CUMUL-SNO VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           PERFORM CUMUL-MOY VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           PERFORM CUMUL-FLA VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.

       CUMUL-DC.
           ADD L-DC(IDX)        TO LCUM-DC(IDX-1, IDX).
       CUMUL-UN.
           ADD L-UNITE(IDX)     TO LCUM-UNITE(IDX-1, IDX).
       CUMUL-JRS.
           ADD L-JR-OCC(IDX)    TO LCUM-JRS(IDX-1, IDX).
       CUMUL-COT.
           ADD L-COT(IDX)       TO LCUM-COT(IDX-1, IDX).
       CUMUL-ABA.
           ADD L-ABAT(IDX)      TO LCUM-ABAT(IDX-1, IDX).
       CUMUL-IMP.
           ADD L-IMPOS(IDX)     TO LCUM-IMPOS(IDX-1, IDX).
       CUMUL-SNO.
           ADD L-SNOCS(IDX)     TO LCUM-SNOCS(IDX-1, IDX).
       CUMUL-MOY.
           ADD L-MOY(IDX)       TO LCUM-MOY(IDX-1, IDX).
       CUMUL-FLA.
           ADD L-FLAG(IDX)      TO LCUM-FLAG(IDX-1, IDX).

      
           COPY "XACTION.CPY".
