      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 7-HRDEL EFFACEMENT BATCH HEURES OCCUP       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    7-HRDEL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  PRECISION             PIC 9 VALUE 2.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "STATUT.REC".
           COPY "OCCUP.REC".
           COPY "COUT.REC".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  HELP-VAL              PIC Z BLANK WHEN ZERO.
       01  JOUR-DEB              PIC 99.
       01  JOUR-FIN              PIC 99.



       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z2Z2 PIC ZZ,ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-7-HRDEL.
       
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-8
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT OCC-KEY
             LINE 11 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-7.
           ACCEPT JOUR-DEB
             LINE 13 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-8.
           IF JOUR-DEB > JOUR-FIN
              MOVE JOUR-DEB TO JOUR-FIN.
           ACCEPT JOUR-FIN
             LINE 14 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           MOVE EXC-KEY TO SAVE-KEY.
           IF OCC-KEY > 0
           AND OCC-KEY < 11
              MOVE 0 TO OCC-KEY 
              MOVE 1 TO INPUT-ERROR
           END-IF.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-OCCUP" USING LINK-V OCC-RECORD 
                   CANCEL "2-OCCUP"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-HE-01 THRU DIS-HE-END
                   PERFORM DISPLAY-F-KEYS
           WHEN OTHER PERFORM NEXT-OCCUP 
           END-EVALUATE.
           PERFORM DIS-HE-06.

       APRES-7.
      *    IF JOUR-DEB = 0
      *       MOVE 1 TO JOUR-DEB
      *       MOVE 1 TO INPUT-ERROR
      *    END-IF.
           IF JOUR-DEB > MOIS-JRS(LNK-MOIS)
              MOVE MOIS-JRS(LNK-MOIS) TO JOUR-FIN
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-07.

       APRES-8.
           IF JOUR-FIN < JOUR-DEB
              MOVE JOUR-DEB TO JOUR-FIN
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF JOUR-FIN > MOIS-JRS(LNK-MOIS)
              MOVE MOIS-JRS(LNK-MOIS) TO JOUR-FIN
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-08.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON
           END-IF.
           IF  COUT > 0
           AND COUT NOT = CAR-COUT  
              GO READ-PERSON
           END-IF.
           CALL "4-HRDEL" USING LINK-V
                                REG-RECORD
                                CAR-RECORD
                                OCC-RECORD
                                JOUR-DEB
                                JOUR-FIN.
           CALL "4-JRDEL" USING LINK-V
                                REG-RECORD
                                CAR-RECORD
                                OCC-RECORD
                                JOUR-DEB
                                JOUR-FIN.

           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
      *    IF REG-PERSON > 0
      *       PERFORM PRESENCE
      *       IF EXC-KEY = 65
      *       OR EXC-KEY = 66
      *          IF PRES-TOT(LNK-MOIS) = 0
      *             GO NEXT-REGIS
      *          END-IF
      *       END-IF
      *    END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-OCCUP.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD SAVE-KEY.
           CALL "6-OCCTXT" USING LINK-V OCC-RECORD.

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE OCC-KEY TO HE-Z2.
           DISPLAY HE-Z2   LINE 11 POSITION 31.
           DISPLAY OCC-NOM LINE 11 POSITION 35.
       DIS-HE-07.
           MOVE JOUR-DEB TO HE-Z2.
           DISPLAY HE-Z2 LINE 13 POSITION 31.
           IF JOUR-DEB > 0 
              PERFORM DIS-JOUR-DEB
           END-IF.
       DIS-HE-08.
           MOVE JOUR-FIN TO HE-Z2.
           DISPLAY HE-Z2 LINE 14 POSITION 31.
           IF JOUR-FIN > 0 
              PERFORM DIS-JOUR-FIN
           END-IF.
       DIS-HE-END.
           EXIT.

       DIS-JOUR-DEB.
           MOVE SEM-IDX(LNK-MOIS, JOUR-DEB) TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "SW" TO LNK-AREA.
           MOVE 13351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       DIS-JOUR-FIN.
           MOVE SEM-IDX(LNK-MOIS, JOUR-FIN) TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "SW" TO LNK-AREA.
           MOVE 14351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       AFFICHAGE-ECRAN.
           MOVE 307 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


