      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-STATUT GESTION DES STATUTS                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-STATUT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "STATUT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "STATUT.FDE".

       WORKING-STORAGE SECTION.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

      *    COPY "V-VAR.CPY".
       01  DEL-KEY               PIC 9(4) COMP-1 VALUE 98.
       01  WR-KEY                PIC 9(4) COMP-1 VALUE 99.
       01  PV-KEY                PIC 9(4) COMP-1 VALUE 65.
       01  NX-KEY                PIC 9(4) COMP-1 VALUE 66.
       01  EXC-KEY               PIC 9(4) COMP-1 VALUE 0.
       01  INDICE-ZONE           PIC 999 VALUE 1.
       01  INPUT-ERROR           PIC 9 VALUE 0.
       01  DECISION              PIC 999.
       01  ACTION                PIC X.
       01  SAVE-LANGUAGE         PIC XX.
       01  LIN-IDX               PIC 99.
       01  IDX                   PIC 9999.

       01  EXC-KEY-CTL.
           02 EXC-KEY-FUN     PIC 99  OCCURS 70.
       01  EXC-KEY-CTL-R REDEFINES EXC-KEY-CTL.
           02 EXC-KFR         PIC 9(10)  OCCURS 14.

       01  TODAY.
           02 TODAY-DATE   PIC 9(8).
           02 TODAY-DATE-R REDEFINES TODAY-DATE.
              03 TODAY-ANNEE  PIC 9999.
              03 TD-ANNEE-R REDEFINES TODAY-ANNEE.
                 04 TD-SIECLE PIC 99.
                 04 TD-ANNEE  PIC 99.
              03 TODAY-MJ  PIC 9999.
              03 TD-MJ-R REDEFINES TODAY-MJ.
                 04 TODAY-MOIS   PIC 99.
                 04 TODAY-JOUR   PIC 99.
           02 TODAY-TEMPS.
              03 TODAY-HEURE  PIC 99.
              03 TODAY-MIN    PIC 99.
              03 TODAY-SECS   PIC 9999.
       01  TODAY-A REDEFINES TODAY.
           02 TODAY-TIME.
              03 TODAY-DATE-A    PIC 9(8).
              03 TODAY-TEMPS-A   PIC 9(4).
           02 TODAY-FILLER.
              03 TODAY-FILL   PIC 9999.
      


       01  CHOIX-MAX             PIC 99 VALUE 4. 
       01  LANGUE                PIC XX. 
       01  HE-ZZ                 PIC ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON STATUT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-1-STATUT.
       
           OPEN I-O STATUT.

           PERFORM AFFICHAGE-ECRAN .
           MOVE LNK-LANGUAGE TO LANGUE SAVE-LANGUAGE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0029000000 TO EXC-KFR (1)
                      MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
           WHEN 4     MOVE 0100000005 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-DESCR
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  2 PERFORM APRES-2 
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT LANGUE
             LINE  3 POSITION 30 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF  EXC-KEY = 12
           AND LNK-SQL = "Y"
              INITIALIZE STAT-RECORD
              PERFORM SQL THRU SQL-END.

       AVANT-2.            
           ACCEPT STAT-CODE
             LINE  4 POSITION 30 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-DESCR.
           COMPUTE LIN-IDX = 2 + INDICE-ZONE.
           COMPUTE IDX = INDICE-ZONE - 2.
           ACCEPT STAT-NOM
             LINE  LIN-IDX POSITION 30 SIZE 25
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           IF LANGUE = SPACES
              MOVE LNK-LANGUAGE TO LANGUE 
              MOVE 1 TO INPUT-ERROR.
             MOVE LANGUE TO STAT-LANGUE.

       APRES-2.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-STATUT
           WHEN   2 MOVE LANGUE TO LNK-LANGUAGE 
                    CALL "2-STATUT" USING LINK-V STAT-RECORD
                    CANCEL "2-STATUT"
                    MOVE SAVE-LANGUAGE TO LNK-LANGUAGE 
                    PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM READ-STATUT.
           IF STAT-CODE = 0
              MOVE 1 TO INPUT-ERROR.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 MOVE LANGUE TO STAT-LANGUE STAT-LANGUE-1
                    MOVE STAT-CODE   TO STAT-CODE-1 LNK-VAL
                    WRITE STAT-RECORD INVALID 
                        REWRITE STAT-RECORD
                    END-WRITE   
                    IF LNK-SQL = "Y" 
                       CALL "9-STATUT" USING LINK-V STAT-RECORD WR-KEY
                    END-IF
                    MOVE 1 TO DECISION
                    PERFORM AFFICHAGE-DETAIL
            WHEN  8 DELETE STATUT INVALID CONTINUE
                    END-DELETE
                    IF LNK-SQL = "Y" 
                       CALL "9-STATUT" USING LINK-V STAT-RECORD DEL-KEY
                    END-IF
                    INITIALIZE STAT-REC-DET
                    MOVE 1 TO DECISION
                    PERFORM AFFICHAGE-DETAIL
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION > 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       READ-STATUT.
           MOVE LANGUE TO STAT-LANGUE.
           READ STATUT INVALID INITIALIZE STAT-REC-DET.

       NEXT-STATUT.
           MOVE LANGUE TO LNK-LANGUAGE.
           CALL "6-STATUT" USING LINK-V STAT-RECORD EXC-KEY.
           MOVE SAVE-LANGUAGE TO LNK-LANGUAGE.

        SQL.
           CALL "6-STATUT" USING LINK-V STAT-RECORD NX-KEY.
           IF STAT-CODE = 0
              GO SQL-END.
           CALL "9-STATUT" USING LINK-V STAT-RECORD WR-KEY.
           GO SQL.
        SQL-END.
           INITIALIZE STAT-RECORD.

       DIS-HE-01.
           DISPLAY LANGUE LINE  3 POSITION 30.
       DIS-HE-02.
           MOVE STAT-CODE TO HE-ZZ.
           DISPLAY HE-ZZ LINE 4 POSITION 29.
           IF EXC-KEY NOT = 4 MOVE STAT-CODE TO LNK-VAL.
       DIS-HE-03.
           DISPLAY STAT-NOM LINE 5 POSITION 30.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 18 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE STATUT.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
           
      