      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-PRIME0 IMPRESSION / CREATION              �
      *  � PRIME DE FIN D'ANNEE / ABSENTEISME                    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-PRIME0.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM130.FDE".
           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  PRECISION             PIC 9 VALUE 0.
       01  JOB-STANDARD          PIC X(10) VALUE "130       ".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "TAUXCC.REC".
           COPY "STATUT.REC".
           COPY "LIVRE.REC".
           COPY "COUT.REC".
           COPY "CODPAR.REC".
           COPY "CODPAIE.REC".
           COPY "CONTRAT.REC".
           COPY "IMPRLOG.REC".
           COPY "LIVRE.CUM".
           COPY "CCOL.REC".
           COPY "CALEN.REC".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  TIRET-TEXTE           PIC X VALUE "�".

       01  ACCIDENT              PIC 9 VALUE 0.    
       01  PERIODE               PIC 99 VALUE 0.    
       01  PRIME                 PIC 9(6)V99.
       01  GAIN                  PIC 9(6)V99.
       01  POURCENTAGE           PIC 999.
       01  TIPE                  PIC 99.
       01  ELIGIBLE              PIC 9.    
       01  JRS-INTERRUPTION      PIC 999.    
       01  CREATE-CODE           PIC X VALUE "N".

       01  HELP-CUMUL.
           02 H-J OCCURS 12.
              03 HR-TOT-JOUR     PIC 99V99 OCCURS 31.

       01  HELP-PERIODES.
           02 H-P OCCURS 30.
              03 JOUR-DEBUT      PIC 99.
              03 MOIS-DEBUT      PIC 99.
              03 JOUR-FIN        PIC 99.
              03 MOIS-FIN        PIC 99.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".PFA".
       01  JOURS-NAME.
           02 JOURS-ID           PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z2Z2 PIC ZZ,ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                JOURS 
                FORM. 

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-PRIME0.

           MOVE 1 TO STATUT.
           PERFORM AFFICHAGE-ECRAN .
           CALL "0-TODAY" USING TODAY.
           CALL "6-GCP" USING LINK-V CP-RECORD.
           MOVE FR-ACCIDENT TO LNK-NUM HE-Z2.
           DISPLAY HE-Z2 LINE 18 POSITION 4.
           MOVE "AC" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           DISPLAY LNK-TEXT LINE 18 POSITION  9 SIZE 40.

           INITIALIZE CC-RECORD LIN-NUM LIN-IDX.
           CALL "6-TAUXCC" USING LINK-V CC-RECORD.
           CANCEL "6-TAUXCC".
           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           OPEN INPUT JOURS.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.

           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT CREATE-CODE
             LINE 16 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           IF CREATE-CODE = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM READ-CONTRAT.
           IF LNK-MOIS NOT = 12
              IF  CON-FIN-A NOT = LNK-ANNEE
              AND CON-FIN-M NOT = LNK-MOIS
                  GO READ-PERSON-1
               END-IF
           END-IF.
           PERFORM CAR-RECENTE.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           IF FR-PRIME = 6
              IF  CAR-CONGE NOT = 8  
              AND CAR-CONGE NOT = 2
                 GO READ-PERSON-1
              END-IF
           END-IF.

           INITIALIZE ACCIDENT.
           PERFORM CUMUL-JOURS.
           PERFORM FULL-PROCESS.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.
           PERFORM END-PROGRAM.
                
       FULL-PROCESS.
           PERFORM TEST-LINE.
           PERFORM FILL-FILES.
           PERFORM DIS-HE-01.

       TEST-LINE.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM FILL-FIRME
              COMPUTE LIN-NUM = LIN-IDX
           END-IF.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           MOVE CCOL-CALENDRIER TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.

           MOVE 0 TO ELIGIBLE.
           COMPUTE IDX-1 = REG-ANCIEN-A.
           IF  LNK-ANNEE = REG-ANCIEN-A 
           AND PRES-JOUR(1, 1)   = 1
           AND PRES-JOUR(12, 31) = 1
              ADD 1 TO IDX-1
           END-IF.
           COMPUTE IDX-1 = IDX-1 - LNK-ANNEE.
           IF FR-PRIME = 7
           AND REG-ANCIEN-M < 7
              ADD 1 TO IDX-1
           END-IF.

           IF IDX-1 > 0
              MOVE 1 TO ELIGIBLE.
           MOVE 1 TO TIPE.
           IF FR-PRIME = 7
              EVALUATE IDX-1
                WHEN 0 THRU 2 MOVE 0 TO ELIGIBLE
                WHEN 3 MOVE 1 TO TIPE
                WHEN 4 THRU 5 MOVE 2 TO TIPE
                WHEN 6 THRU 9 MOVE 3 TO TIPE
                WHEN 10 THRU 14 MOVE 4 TO TIPE
                WHEN OTHER MOVE 5 TO TIPE
              END-EVALUATE
           END-IF. 
           IF FR-PRIME = 9
              EVALUATE IDX-1
                WHEN 0 MOVE 0 TO ELIGIBLE
                WHEN 1 THRU 2 MOVE 1 TO TIPE
                WHEN 3 MOVE 2 TO TIPE
                WHEN 4 THRU 6 MOVE 3 TO TIPE
                WHEN 7 THRU 9 MOVE 4 TO TIPE
                WHEN 10 THRU 14 MOVE 5 TO TIPE
                WHEN OTHER MOVE 6 TO TIPE
              END-EVALUATE
           END-IF. 
           IF FR-PRIME = 4
              EVALUATE IDX-1
                WHEN 0 MOVE 0 TO ELIGIBLE
                       MOVE 1 TO TIPE
                WHEN 1 THRU 2 MOVE 1 TO TIPE
                WHEN 3 THRU 4 MOVE 2 TO TIPE
                WHEN OTHER MOVE 3 TO TIPE
              END-EVALUATE
           END-IF. 

       LIVRE.
           ADD 1 TO LNK-MOIS.
           CALL "4-LPCUM" USING LINK-V REG-RECORD LCUM-RECORD.
           SUBTRACT 1 FROM LNK-MOIS.

       CUMUL-JOURS.
           INITIALIZE JRS-RECORD HELP-CUMUL.
           MOVE FR-KEY TO JRS-FIRME-2.
           MOVE REG-PERSON TO JRS-PERSON-2.
           MOVE 1 TO JRS-OCCUPATION-2.
           START JOURS KEY >= JRS-KEY-2 INVALID CONTINUE
                NOT INVALID
                PERFORM READ-HEURES THRU READ-HEURES-END.

       READ-HEURES.
           READ JOURS NEXT AT END
               GO READ-HEURES-END.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR JRS-MOIS > LNK-MOIS
              GO READ-HEURES-END
           END-IF.
           IF JRS-COMPLEMENT NOT = 0
           OR JRS-OCCUPATION =  0
           OR JRS-OCCUPATION = 10
           OR JRS-OCCUPATION > 11
              GO READ-HEURES
           END-IF.
           INITIALIZE L-RECORD 
           MOVE JRS-MOIS TO L-MOIS.
           MOVE JRS-OCCUPATION TO L-SUITE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-FLAG-ACCIDENT NOT = 0
              MOVE 1 TO ACCIDENT
           END-IF.
           IF L-FLAG-ACCIDENT NOT = 0
           OR L-FLAG-HOPITAL  NOT = 0
           OR L-FLAG-AGREE    NOT = 0
              GO READ-HEURES
           END-IF.
           PERFORM ADD-HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.
           GO READ-HEURES.
       READ-HEURES-END.
           INITIALIZE HELP-PERIODES PERIODE JRS-INTERRUPTION.
           PERFORM T-P VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > LNK-MOIS.

       ADD-HEURES.
           IF JRS-HRS(IDX) NOT = 0
              MOVE JRS-HRS(IDX) TO HR-TOT-JOUR(JRS-MOIS, IDX) 
           END-IF.

       T-P.
           PERFORM TEST-JOURS 
           VARYING IDX FROM 1 BY 1 UNTIL IDX > MOIS-JRS(IDX-1).

       TEST-JOURS.
           IF HR-TOT-JOUR(IDX-1, IDX)  = 0
              IF  CAL-JOUR(IDX-1, IDX) NOT = 1
              AND SEM-IDX(IDX-1, IDX)  < 6
                ADD 1 TO JRS-INTERRUPTION
              END-IF
           ELSE
              IF JRS-INTERRUPTION > 1
                 ADD 1 TO PERIODE
              END-IF
              IF PERIODE = 0
                 ADD 1 TO PERIODE
              END-IF
              IF JOUR-DEBUT(PERIODE) = 0
                 MOVE IDX   TO JOUR-DEBUT(PERIODE) 
                 MOVE IDX-1 TO MOIS-DEBUT(PERIODE) 
              END-IF
              MOVE IDX   TO JOUR-FIN(PERIODE) 
              MOVE IDX-1 TO MOIS-FIN(PERIODE) 
              MOVE 0 TO JRS-INTERRUPTION
           END-IF.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.

       FILL-FILES.

      * DONNEES PERSONNE
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  6 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           PERFORM FILL-HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 4.
           PERFORM FILL-DECOMPTE.
           PERFORM LIVRE.
           ADD 1 TO LIN-NUM.

           IF ELIGIBLE = 0
              MOVE "Entr괻:" TO  ALPHA-TEXTE
              MOVE 13 TO COL-NUM
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE REG-ANCIEN-J TO  ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE REG-ANCIEN-M TO  ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE REG-ANCIEN-A TO  ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.

           MOVE CC-PA(TIPE) TO SH-00.

           IF FR-PRIME = 7
           OR FR-ACCIDENT = 9
           OR FR-PRIME = 4
              MOVE 35 TO COL-NUM
              MOVE 1  TO CAR-NUM
              MOVE 2  TO DEC-NUM
              MOVE CC-PA(TIPE) TO SH-00 VH-00
              IF FR-ACCIDENT = 9
              AND ACCIDENT = 0
                 ADD CC-PA(10) TO SH-00 VH-00
              END-IF
              PERFORM FILL-FORM
              MOVE " %" TO ALPHA-TEXTE
              PERFORM FILL-FORM.


           COMPUTE VH-00 = LCUM-MOY(1, 6).

           MOVE 42 TO COL-NUM.
           MOVE 6  TO CAR-NUM.
           MOVE 2  TO DEC-NUM.
           PERFORM FILL-FORM.

      *    COMPUTE PRIME = VH-00 * CC-PA(TIPE) / 100 + ,005.
           COMPUTE PRIME = VH-00 * SH-00 / 100 + ,005.
           MOVE PRIME TO VH-00 GAIN.
           MOVE 55 TO COL-NUM.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.
           COMPUTE IDX-4 = (LNK-ANNEE * 100 + LNK-MOIS)
                         - (PR-NAISS-A * 100 + PR-NAISS-M).
           IF IDX-4 >= 5000 
              IF PERIODE > 0
                 SUBTRACT 1 FROM PERIODE
              END-IF
           END-IF.
           IF PERIODE > 0
              COMPUTE IDX = (PERIODE - 1)
           ELSE
              COMPUTE IDX = PERIODE
           END-IF.
           
           IF IDX > 4 MOVE 4 TO IDX.
           EVALUATE IDX
                WHEN 0 CONTINUE
                WHEN 1 THRU 3
                       COMPUTE PRIME = PRIME * (1 - (IDX * ,25)) + ,005
                WHEN OTHER MOVE 0 TO PRIME
           END-EVALUATE.

           COMPUTE VH-00 = 100 - (IDX * 25).
           MOVE 100 TO COL-NUM.
           MOVE 3 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE VH-00 TO POURCENTAGE.

           MOVE " %" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

      *    absence volontaire
           IF LCUM-UNITE(1, 40) NOT = 0
              MOVE 0 TO PRIME 
              MOVE "- ! -" TO ALPHA-TEXTE
              MOVE 88 TO COL-NUM
              PERFORM FILL-FORM
           ELSE
              MOVE PRIME TO VH-00
              MOVE 109 TO COL-NUM
              MOVE 5 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              PERFORM FILL-FORM.
           SUBTRACT PRIME FROM GAIN.
              MOVE GAIN  TO VH-00
              MOVE 80 TO COL-NUM
              MOVE 5 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              PERFORM FILL-FORM.

           MOVE 5 TO COL-NUM.
           ADD 1 TO LIN-NUM.
           IF LIN-NUM < 70
              PERFORM TIRET UNTIL COL-NUM > 124.
           ADD 1 TO LIN-NUM.
           IF  CREATE-CODE  NOT = "N" 
           AND COD-PAR(197, 1) NOT = 0
           AND ELIGIBLE = 1
               PERFORM WRITE-CS.

       WRITE-CS.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY     TO CSP-FIRME. 
           MOVE LNK-MOIS   TO CSP-MOIS.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE COD-PAR(197, 1) TO CSP-CODE. 
           MOVE PRIME TO CSP-TOTAL.
           MOVE GAIN  TO CSP-DONNEE-2.
           MOVE POURCENTAGE TO CSP-POURCENT.
           COMPUTE CSP-DONNEE-1 = CSP-DONNEE-2 + CSP-TOTAL.
           
           IF FR-PRIME = 6
              COMPUTE CSP-UNITE = 
              LCUM-UNITE(1, 95) + LCUM-UNITE(1, 100)
           END-IF.
           IF LCUM-UNITE(1, 40) > 0
              INITIALIZE PRIME CSP-DONNEE-1
           END-IF.
           IF PRIME NOT = 0 
           OR CSP-DONNEE-1 NOT = 0 
              CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY
           ELSE
              CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD DEL-KEY
           END-IF.
           CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       FILL-HEURES.
           IF JOUR-DEBUT(IDX)  NOT = 0
              COMPUTE COL-NUM = 50 + (IDX - 1) * 17
              MOVE JOUR-DEBUT(IDX) TO VH-00
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
              MOVE "." TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE MOIS-DEBUT(IDX) TO VH-00
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE "<>" TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE JOUR-FIN(IDX) TO VH-00
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
              MOVE "." TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE MOIS-FIN(IDX) TO VH-00
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
           END-IF.

       FILL-DECOMPTE.
           MOVE   4 TO CAR-NUM.
           MOVE 119 TO COL-NUM.
           COMPUTE VH-00 = PERIODE
           PERFORM FILL-FORM.

       FILL-FIRME.

      *    PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  3 TO CAR-NUM.
           MOVE  6 TO LIN-NUM.
           MOVE 118 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

           MOVE  2 TO LIN-NUM.
           MOVE 80 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.

           PERFORM FILL-FORM.
           MOVE  4 TO LIN-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE 111 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 114 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 117 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE 4 TO LIN-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE 8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           MOVE 15 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 5 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 6 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-CODE-POST TO VH-00.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       DIS-HE-01.
           DISPLAY A-N LINE 4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           IF A-N = "N"
              DISPLAY LNK-TEXT LINE 6 POSITION 27 SIZE 34
           ELSE
              DISPLAY SPACES   LINE 6 POSITION 37 SIZE 1
              DISPLAY LNK-TEXT LINE 6 POSITION 38 SIZE 23.
       DIS-HE-STAT.
           DISPLAY STAT-NOM  LINE 9 POSITION 35.
       DIS-HE-05.
           MOVE COUT TO HE-Z4.
           DISPLAY HE-Z4  LINE 10 POSITION 29.
           DISPLAY COUT-NOM LINE 10 POSITION 35.
       DIS-HE-END.
           EXIT.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 512 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF COUNTER > 0 
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".

