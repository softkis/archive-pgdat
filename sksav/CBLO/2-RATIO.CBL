      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-RATIO RATIO AGES ANCIENNETE               �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-RATIO.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
       01  CHOIX-MAX         PIC 99 VALUE 1. 
       01  SX                PIC 9. 
       01  ST                PIC 9. 
       01  CALCUL            PIC S9999. 

       01  HELP-CUMUL.           
           02 AGE OCCURS 3.
              03 AGE-G OCCURS 12.
                 04 AGE-GROUPE PIC 9999.
                 04 AGE-STAT   PIC 9999 OCCURS 3.
           02 ANC OCCURS 3.
              03 ANC-G OCCURS 8.
                 04 ANC-GROUPE PIC 9999.
                 04 ANC-STAT   PIC 9999 OCCURS 3.
           02 H-F OCCURS 3.
              03 HOM-FEM PIC 9999.
           
       01  ECR-DISPLAY.
           02 HE-Z2    PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4    PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6    PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z3Z2  PIC Z(3),ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-RATIO.
       
           PERFORM AFFICHAGE-ECRAN .

           PERFORM CUMUL-PERSON01.
           MOVE 1 TO INDICE-ZONE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
            UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      *    PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           
           PERFORM APRES-DEC.

           IF INDICE-ZONE > CHOIX-MAX
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           EXIT.

       CUMUL-PERSON01.
           MOVE "AA" TO LNK-AREA.
           MOVE 22 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           MOVE 0 TO LNK-NUM.
           INITIALIZE PR-RECORD HELP-CUMUL.
           MOVE 3 TO EXC-KEY.
           PERFORM LECTURE-PERSON THRU LECTURE-PERSON-END.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.

       LECTURE-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY
           IF REG-PERSON = 0
           AND REG-MATCHCODE = SPACES
              GO LECTURE-PERSON-END
           END-IF.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF PRES-TOT(LNK-MOIS) = 0
              GO LECTURE-PERSON
           END-IF .
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           MOVE PR-CODE-SEXE TO SX.
           MOVE CAR-STATUT TO ST.
           IF SX = 0
              MOVE 1 TO SX.
           IF ST = 0
              MOVE 1 TO ST.
           IF ST > 3
              MOVE 1 TO ST.
           ADD 1 TO HOM-FEM(SX) HOM-FEM(3).
           COMPUTE CALCUL = (LNK-ANNEE - REG-ANCIEN-A) * 12
                           + LNK-MOIS  - REG-ANCIEN-M.
                           
           EVALUATE CALCUL 
             WHEN   0 THRU   6 
                ADD 1 TO ANC-GROUPE(SX, 1) ANC-GROUPE(3, 1)
                         ANC-STAT(SX, 1, ST) ANC-STAT(3, 1, ST)
             WHEN   7 THRU  24 
                ADD 1 TO ANC-GROUPE(SX, 2) ANC-GROUPE(3, 2)
                         ANC-STAT(SX, 2, ST) ANC-STAT(3, 2, ST)
             WHEN  25 THRU  60 
                ADD 1 TO ANC-GROUPE(SX, 3) ANC-GROUPE(3, 3)
                         ANC-STAT(SX, 3, ST) ANC-STAT(3, 3, ST)
             WHEN  61 THRU 120 
                ADD 1 TO ANC-GROUPE(SX, 4) ANC-GROUPE(3, 4)
                         ANC-STAT(SX, 4, ST) ANC-STAT(3, 4, ST)
             WHEN 121 THRU 240 
                ADD 1 TO ANC-GROUPE(SX, 5) ANC-GROUPE(3, 5)
                         ANC-STAT(SX, 5, ST) ANC-STAT(3, 5, ST)
             WHEN 241 THRU 360 
                ADD 1 TO ANC-GROUPE(SX, 6) ANC-GROUPE(3, 6)
                         ANC-STAT(SX, 6, ST) ANC-STAT(3, 6, ST)
             WHEN OTHER 
                ADD 1 TO ANC-GROUPE(SX, 7) ANC-GROUPE(3, 7)
                         ANC-STAT(SX, 7, ST) ANC-STAT(3, 7, ST)
           END-EVALUATE.
           ADD 1 TO ANC-GROUPE(SX, 8) ANC-GROUPE(3, 8)
                    ANC-STAT(SX, 8, ST) ANC-STAT(3, 8, ST).

           COMPUTE CALCUL = (LNK-ANNEE - REG-NAISS-A) * 12
                           + LNK-MOIS  - REG-NAISS-M.
           EVALUATE CALCUL 
                WHEN   1 THRU  240 
                ADD 1 TO AGE-GROUPE(SX, 1) AGE-GROUPE(3, 1)
                         AGE-STAT(SX, 1, ST) AGE-STAT(3, 1, ST)
                WHEN 241 THRU  300 
                ADD 1 TO AGE-GROUPE(SX, 2) AGE-GROUPE(3, 2)
                         AGE-STAT(SX, 2, ST) AGE-STAT(3, 2, ST)
                WHEN 301 THRU  360 
                ADD 1 TO AGE-GROUPE(SX, 3) AGE-GROUPE(3, 3)
                         AGE-STAT(SX, 3, ST) AGE-STAT(3, 3, ST)
                WHEN 361 THRU  420 
                ADD 1 TO AGE-GROUPE(SX, 4) AGE-GROUPE(3, 4)
                         AGE-STAT(SX, 4, ST) AGE-STAT(3, 4, ST)
                WHEN 421 THRU  480 
                ADD 1 TO AGE-GROUPE(SX, 5) AGE-GROUPE(3, 5)
                         AGE-STAT(SX, 5, ST) AGE-STAT(3, 5, ST)
                WHEN 481 THRU  540 
                ADD 1 TO AGE-GROUPE(SX, 6) AGE-GROUPE(3, 6)
                         AGE-STAT(SX, 6, ST) AGE-STAT(3, 6, ST)
                WHEN 541 THRU  600 
                ADD 1 TO AGE-GROUPE(SX, 7) AGE-GROUPE(3, 7)
                         AGE-STAT(SX, 7, ST) AGE-STAT(3, 7, ST)
                WHEN 601 THRU  660 
                ADD 1 TO AGE-GROUPE(SX, 8) AGE-GROUPE(3, 8)
                         AGE-STAT(SX, 8, ST) AGE-STAT(3, 8, ST)
                WHEN 661 THRU  720 
                ADD 1 TO AGE-GROUPE(SX, 9) AGE-GROUPE(3, 9)
                         AGE-STAT(SX, 9, ST) AGE-STAT(3, 9, ST)
                WHEN 721 THRU  780 
                ADD 1 TO AGE-GROUPE(SX, 10) AGE-GROUPE(3, 10)
                         AGE-STAT(SX, 10, ST) AGE-STAT(3, 10, ST)
                WHEN OTHER 
                ADD 1 TO AGE-GROUPE(SX, 11) AGE-GROUPE(3, 11)
                         AGE-STAT(SX, 11, ST) AGE-STAT(3, 11, ST)
           END-EVALUATE.
           ADD 1 TO AGE-GROUPE(SX, 12) AGE-GROUPE(3, 12)
                    AGE-STAT(SX, 12, ST) AGE-STAT(3, 12, ST).
           GO LECTURE-PERSON.
       LECTURE-PERSON-END.
           PERFORM AFFICHAGE-DETAIL.

       DIS-E1-01.
           PERFORM DISPLAY-VAL VARYING IDX FROM 1 BY 1 UNTIL IDX > 3.

       DISPLAY-VAL.
      *    COMPUTE LIN-IDX = 3.
      *    MOVE HOM-FEM(IDX) TO HE-Z4.
           COMPUTE COL-IDX = IDX * 24 - 14.
      *    DISPLAY HE-Z4  LINE LIN-IDX POSITION COL-IDX.
           PERFORM DIS-ANC VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 7.
           PERFORM DIS-AGE VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 12.

       DIS-ANC.
           COMPUTE LIN-IDX = 16 + IDX-1.
           MOVE ANC-GROUPE(IDX, IDX-1) TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION COL-IDX.

           MOVE ANC-STAT(IDX, IDX-1, 1) TO HE-Z4.
           COMPUTE IDX-2 = COL-IDX + 4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION IDX-2.
           MOVE ANC-STAT(IDX, IDX-1, 2) TO HE-Z4.
           COMPUTE IDX-2 = COL-IDX + 8.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION IDX-2.
           MOVE ANC-STAT(IDX, IDX-1, 3) TO HE-Z4.
           COMPUTE IDX-2 = COL-IDX + 12.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION IDX-2.
           COMPUTE SH-00 = 100 * ANC-GROUPE(IDX, IDX-1)
                               / ANC-GROUPE(3, 8).

           MOVE SH-00 TO HE-Z3Z2.
           COMPUTE IDX-2 = COL-IDX + 16.
           DISPLAY HE-Z3Z2 LINE LIN-IDX POSITION IDX-2.
 
       DIS-AGE.
           COMPUTE LIN-IDX = 3 + IDX-1.
           MOVE AGE-GROUPE(IDX, IDX-1) TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION COL-IDX.

           MOVE AGE-STAT(IDX, IDX-1, 1) TO HE-Z4.
           COMPUTE IDX-2 = COL-IDX + 4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION IDX-2.
           MOVE AGE-STAT(IDX, IDX-1, 2) TO HE-Z4.
           COMPUTE IDX-2 = COL-IDX + 8.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION IDX-2.
           MOVE AGE-STAT(IDX, IDX-1, 3) TO HE-Z4.
           COMPUTE IDX-2 = COL-IDX + 12.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION IDX-2.

           COMPUTE SH-00 = 100 * AGE-GROUPE(IDX, IDX-1)
                               / AGE-GROUPE(3, 12).

           MOVE SH-00 TO HE-Z3Z2.
           COMPUTE IDX-2 = COL-IDX + 16.
           DISPLAY HE-Z3Z2 LINE LIN-IDX POSITION IDX-2.

       AFFICHAGE-ECRAN.
           MOVE 2238 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-E1-01.
           
       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

       AVANT-DEC.
           MOVE 0 TO DECISION.
           ACCEPT DECISION
             LINE 24 POSITION 27 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "REVERSE"
             ON EXCEPTION  EXC-KEY  CONTINUE.
                

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".
           
    
