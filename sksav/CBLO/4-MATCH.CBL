      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-MATCH MODULE GENERAL MATCHCODE REGISTRES  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-MATCH.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "REGISTRE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "REGISTRE.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN       PIC 9 VALUE 0.
       01  LAST-REGISTRE  PIC 9(8) VALUE 0.
       01  WR-KEY         PIC 9(4) COMP-1 VALUE 99.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PERSON.REC".
                 

       PROCEDURE DIVISION USING LINK-V PR-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON REGISTRE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-MATCH.
       
           IF NOT-OPEN = 0
              OPEN I-O REGISTRE
              MOVE 1 TO NOT-OPEN.

           INITIALIZE REG-RECORD.
           MOVE PR-MATRICULE TO REG-MATRICULE.
           START REGISTRE KEY > REG-KEYS INVALID GO RECT-END.
           PERFORM RECT THRU RECT-END.
           EXIT PROGRAM.
           
       RECT.
           READ REGISTRE NEXT NO LOCK AT END GO RECT-END END-READ.
           IF REG-MATRICULE = PR-MATRICULE
              MOVE PR-MATCHCODE TO REG-MATCHCODE
              IF LNK-SQL = "Y" 
                 CALL "9-REGIST" USING LINK-V REG-RECORD WR-KEY 
              END-IF
              REWRITE REG-RECORD INVALID CONTINUE END-REWRITE
              GO RECT
           END-IF.
       RECT-END.
           EXIT PROGRAM.




