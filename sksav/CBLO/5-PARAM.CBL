      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 5-PARAM PARAMETRES COMPTA ANALYTIQUE        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    5-PARAM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PAREX.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "PAREX.FDE".


       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1    PIC 99 VALUE 2.
           02  CHOIX-MAX-2    PIC 99 VALUE 21.
           02  CHOIX-MAX-3    PIC 99 VALUE 21.
           02  CHOIX-MAX-4    PIC 99 VALUE 21. 
           02  CHOIX-MAX-5    PIC 99 VALUE 21. 
           02  CHOIX-MAX-6    PIC 99 VALUE 21.
           02  CHOIX-MAX-7    PIC 99 VALUE 21.
           02  CHOIX-MAX-8    PIC 99 VALUE 21.
           02  CHOIX-MAX-9    PIC 99 VALUE 21.
           02  CHOIX-MAX-10   PIC 99 VALUE 21.
           02  CHOIX-MAX-11   PIC 99 VALUE 21.
           02  CHOIX-MAX-12   PIC 99 VALUE 21.
           02  CHOIX-MAX-13   PIC 99 VALUE 21.
           02  CHOIX-MAX-14   PIC 99 VALUE 21.
           02  CHOIX-MAX-15   PIC 99 VALUE 21.
           02  CHOIX-MAX-16   PIC 99 VALUE 21.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX PIC 99 OCCURS 16.

       01  ECRAN-SUITE.
           02 ECR-S1             PIC  9999 COMP-1 VALUE  405.
           02 ECR-S2             PIC  9999 COMP-1 VALUE  404.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S              PIC  9999 COMP-1 OCCURS 2.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "POCL.REC".
           COPY "OCCOM.REC".
           COPY "FIRME.REC".

       01  NO-POCL    PIC 9(8) VALUE 0.
       01  TX-POCL    PIC X(8).

       01  ECR-DISPLAY.
           02 HE-Z  PIC Z    BLANK WHEN ZERO.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z3 PIC Z(3) BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PAREX. 
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-5-PARAM.

           OPEN I-O   PAREX.

           INITIALIZE PAREX-RECORD.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions
           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0100000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14) 
           END-IF.        

           IF ECRAN-IDX > 1
               MOVE 0067680000 TO EXC-KFR (14) 
               EVALUATE INDICE-ZONE
               WHEN CHOIX-MAX(ECRAN-IDX)
                    MOVE 0100000005 TO EXC-KFR (1)
                    MOVE 0000080000 TO EXC-KFR (2)
                    MOVE 0052000000 TO EXC-KFR (11)
               WHEN OTHER 
                    MOVE 0029002215 TO EXC-KFR(1)
                    MOVE 0052535400 TO EXC-KFR(11)
                    MOVE 5600000000 TO EXC-KFR(12)
           END-IF.    

           IF ECRAN-IDX = 16
               MOVE 0067000000 TO EXC-KFR (14) 
           END-IF.    
               
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
               EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-1-1 
               WHEN  2 PERFORM AVANT-DEC.

           IF ECRAN-IDX > 1
              IF INDICE-ZONE < CHOIX-MAX(ECRAN-IDX)
                 PERFORM AVANT-ALL THRU AVANT-ALL-END
              ELSE 
                 PERFORM AVANT-DEC
              END-IF
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF EXC-KEY > 66 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              IF ECRAN-IDX = 1
                 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
              ELSE
                 INITIALIZE DECISION INDICE-ZONE
              END-IF
              GO TRAITEMENT-ECRAN
           END-IF.
           
           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
              WHEN  1 PERFORM APRES-1-1 
              WHEN  2 PERFORM APRES-DEC.

           IF ECRAN-IDX > 1
              IF INDICE-ZONE = CHOIX-MAX(ECRAN-IDX)
                 PERFORM APRES-DEC
              END-IF
           END-IF.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 52 SUBTRACT 1 FROM INDICE-ZONE.

           IF EXC-KEY = 13 
           OR EXC-KEY = 53
              IF INPUT-ERROR NOT = 0
                 ADD 0 TO INDICE-ZONE
              ELSE 
                 ADD 1 TO INDICE-ZONE.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.
           ACCEPT PAREX-FIRME
             LINE  4 POSITION 27 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
           COMPUTE IDX = INDICE-ZONE.
           MOVE 1 TO IDX-1.
       AVANT-ALL-1.
           IF IDX = 0
             MOVE 1 TO IDX.
           IF IDX-1 = 0
             MOVE 1 TO IDX-1.
           PERFORM DISPLAY-F-KEYS.
           PERFORM AVANT-PARAM.
           IF EXC-KEY = 82 PERFORM END-PROGRAM
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL-1.
           EVALUATE EXC-KEY
                WHEN 65 THRU 66 IF IDX-1 = 4
                          PERFORM NEXT-OCCOM
                          PERFORM DIS-DETAIL-3
                       END-IF
                       IF IDX-1 = 5
                          PERFORM NEXT-POCL
                          PERFORM DIS-DETAIL-4
                        END-IF
                WHEN  4 PERFORM COPIE-LIGNE
                        PERFORM DIS-DETAIL
                        ADD 1 TO IDX
                WHEN  5 IF IDX-1 = 5
                           CALL "1-PFRAIS" USING LINK-V
                           IF LNK-VAL NOT = 0
                              MOVE LNK-VAL TO PAREX-POSTE(IDX-2)
                           END-IF
                           CANCEL "1-PFRAIS"
                        END-IF
                        IF IDX-1 = 4
                           CALL "1-OCCOM" USING LINK-V
                           IF LNK-VAL NOT = 0
                              MOVE LNK-VAL TO PAREX-OCO(IDX-2)
                           END-IF
                           CANCEL "1-OCCOM"
                        END-IF
                        MOVE IDX   TO IDX-3
                        MOVE IDX-1 TO IDX-4
                        PERFORM AFFICHAGE-ECRAN 
                        PERFORM AFFICHAGE-DETAIL
                        MOVE IDX-3 TO IDX
                        MOVE IDX-4 TO IDX-1
                WHEN  2 IF IDX-1 = 5
                          CALL "2-POCL" USING LINK-V PC-RECORD
                          MOVE PC-NUMBER TO NO-POCL
                          IF NO-POCL NOT = 0
                             MOVE NO-POCL TO PAREX-POSTE(IDX-2)
                          PERFORM DIS-DETAIL
                        END-IF
                        CANCEL "2-POCL"
                        END-IF
                        IF IDX-1 = 4
                           CALL "2-OCCOM" USING LINK-V OCO-RECORD
                           IF OCO-NUMBER NOT = 0
                              MOVE OCO-NUMBER TO PAREX-OCO(IDX-2)
                           END-IF
                           CANCEL "2-OCCOM"
                        END-IF
                        MOVE IDX   TO IDX-3
                        MOVE IDX-1 TO IDX-4
                        PERFORM AFFICHAGE-ECRAN 
                        PERFORM AFFICHAGE-DETAIL
                        MOVE IDX-3 TO IDX
                        MOVE IDX-4 TO IDX-1
                WHEN 13 ADD 1 TO IDX-1
                WHEN 27 SUBTRACT 1 FROM IDX-1
                        IF IDX-1 = 0 
                           SUBTRACT 1 FROM IDX
                           MOVE 6 TO IDX-1
                        END-IF
                WHEN 52 SUBTRACT 1 FROM IDX
                        IF IDX = 0 
                           MOVE 1 TO IDX
                           MOVE 0 TO IDX-1
                        END-IF
                WHEN 53 ADD 1 TO IDX
                WHEN 54 MOVE 21 TO IDX INDICE-ZONE
                        GO AVANT-ALL-END
                WHEN 67 GO AVANT-ALL-END
                WHEN 68 GO AVANT-ALL-END
           END-EVALUATE.
           IF IDX-1 > 6 
              ADD 1 TO IDX 
              MOVE 1 TO IDX-1
           END-IF.
           IF IDX-1 > 0 AND  IDX < CHOIX-MAX(ECRAN-IDX)
              GO AVANT-ALL-1 .
           COMPUTE INDICE-ZONE = IDX.
           MOVE 98 TO EXC-KEY.
       AVANT-ALL-END.
           EXIT.

       AVANT-PARAM.
           COMPUTE LIN-IDX = IDX + 2.
           PERFORM GET-VAR.
           EVALUATE IDX-1
                WHEN 1  PERFORM ACCEPT-ANAL
                WHEN 2  PERFORM ACCEPT-PP
                WHEN 3  PERFORM ACCEPT-PRINT
                WHEN 4  PERFORM ACCEPT-OCO
                WHEN 5  PERFORM ACCEPT-POSTE
                WHEN 6  PERFORM ACCEPT-OCC
           END-EVALUATE.

       ACCEPT-ANAL. 
           ACCEPT PAREX-ANAL(IDX-2)
             LINE LIN-IDX POSITION 26 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
             PERFORM DIS-DETAIL-2.

       ACCEPT-PP. 
           ACCEPT PAREX-PP(IDX-2)
             LINE LIN-IDX POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       ACCEPT-PRINT. 
           ACCEPT PAREX-PRINT(IDX-2)
             LINE LIN-IDX POSITION 35 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
             PERFORM DIS-DETAIL-3.

       ACCEPT-OCO. 
           ACCEPT PAREX-OCO(IDX-2)
             LINE LIN-IDX POSITION 38 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
             PERFORM DIS-DETAIL-4.

       ACCEPT-POSTE.
           ACCEPT PAREX-POSTE(IDX-2)
             LINE LIN-IDX POSITION 54 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
             PERFORM DIS-DETAIL-5.

       ACCEPT-OCC. 
           ACCEPT PAREX-OCC(IDX-2)
             LINE LIN-IDX POSITION 80 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
             PERFORM DIS-DETAIL-6.

       COPIE-LIGNE.
           COMPUTE IDX-3 = IDX-2 - 1.
           IF IDX-3 > 0 PERFORM COPIE.

       COPIE.
           MOVE PAREX-PP(IDX-3)     TO PAREX-PP(IDX-2).
           MOVE PAREX-ANAL(IDX-3)   TO PAREX-ANAL(IDX-2).
           MOVE PAREX-PRINT(IDX-3) TO PAREX-PRINT(IDX-2).
           MOVE PAREX-OCO(IDX-3)    TO PAREX-OCO(IDX-2).
           MOVE PAREX-POSTE(IDX-3)  TO PAREX-POSTE(IDX-2).
           PERFORM DIS-DETAIL THRU DIS-DETAIL-END.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           EVALUATE EXC-KEY
           WHEN  3 PERFORM NEXT-PAREX
           END-EVALUATE.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
           IF PAREX-FIRME = 0
              MOVE 0 TO INPUT-ERROR.
           READ PAREX INVALID CONTINUE END-READ.


       APRES-DEC.
            EVALUATE EXC-KEY 
            WHEN  1 MOVE 00601005 TO LNK-VAL
                    PERFORM HELP-SCREEN
                    MOVE 1 TO INPUT-ERROR
            WHEN  5 WRITE PAREX-RECORD INVALID REWRITE PAREX-RECORD
                    END-WRITE   
                    INITIALIZE PAREX-RECORD  
                    MOVE 1 TO INDICE-ZONE ECRAN-IDX
                    PERFORM AFFICHAGE-ECRAN
                    PERFORM AFFICHAGE-DETAIL
                    MOVE 1 TO DECISION ECRAN-IDX
            WHEN  8 IF PAREX-FIRME > 0
                       DELETE PAREX INVALID CONTINUE END-DELETE
                    END-IF
                    INITIALIZE PAREX-RECORD DECISION
                    MOVE 1 TO DECISION ECRAN-IDX
                    PERFORM AFFICHAGE-ECRAN
                    PERFORM AFFICHAGE-DETAIL
            WHEN 52 COMPUTE DECISION = CHOIX-MAX(ECRAN-IDX) + 1
           END-EVALUATE.
           IF DECISION NOT = 0
               COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX (ECRAN-IDX)
               MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.


       READ-FIRME.
           CALL "6-FIRMES" USING LINK-V FIRME-RECORD "N" EXC-KEY.

       NEXT-PAREX.
           START PAREX KEY > PAREX-KEY INVALID KEY
                INITIALIZE PAREX-RECORD
                NOT INVALID
           READ PAREX NEXT AT END 
                INITIALIZE PAREX-RECORD
                MOVE 1 TO INDICE-ZONE
                END-READ.

       NEXT-POCL.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" EXC-KEY.
           MOVE PC-NUMBER TO PAREX-POSTE(IDX-2).

       READ-POCL.
           MOVE PAREX-POSTE(IDX-2) TO PC-NUMBER.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY.

       NEXT-OCCOM.
           CALL "6-OCCOM" USING LINK-V OCO-RECORD EXC-KEY.
           MOVE OCO-NUMBER TO PAREX-OCO(IDX-2).

       READ-OCCOM.
           MOVE PAREX-OCO(IDX-2) TO OCO-NUMBER.
           CALL "6-OCCOM" USING LINK-V OCO-RECORD FAKE-KEY.

       DIS-LIGNE.
           MOVE IDX TO HE-Z4.
           DISPLAY HE-Z4 LINE 19 POSITION 4.
           MOVE IDX-2 TO HE-Z4.
           DISPLAY HE-Z4 LINE 19 POSITION 14.
           PERFORM READ-OCCOM.
           DISPLAY OCO-NOM   LINE 20 POSITION 15.

       AFFICHAGE-ECRAN.
           IF ECRAN-IDX = 1
              MOVE ECR-S(1) TO LNK-VAL
           ELSE
              MOVE ECR-S(2) TO LNK-VAL
           END-IF.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           IF ECRAN-IDX > 1
              PERFORM TABS VARYING IDX FROM 1 BY 1 UNTIL IDX > 20
           END-IF.
           IF ECRAN-IDX > 1
              PERFORM AFFICHAGE-ECRAN-DETAIL.

       AFFICHAGE-ECRAN-DETAIL.
           DISPLAY SPACES  LINE 2 POSITION 2 SIZE 70.
           DISPLAY "AN"    LINE 2 POSITION 26.
           DISPLAY "PP"    LINE 2 POSITION 31.
           DISPLAY "Imp"   LINE 2 POSITION 34.
           DISPLAY "COMP"  LINE 2 POSITION 38.
           DISPLAY "POSTE" LINE 2 POSITION 54.
           DISPLAY "V"     LINE 2 POSITION 80.


       TABS.
           COMPUTE LIN-IDX = IDX + 2.
           COMPUTE IDX-1 = (ECRAN-IDX - 2) * 20 + IDX.
           MOVE IDX-1  TO HE-Z3.
           DISPLAY HE-Z3 LINE LIN-IDX POSITION 1 LOW.
           MOVE IDX-1 TO LNK-NUM.
           MOVE "L"   TO LNK-LOW.
           COMPUTE LNK-POSITION = LIN-IDX * 1000000 +  52000. 
           CALL "0-DLEX" USING LINK-V.
    
       AFFICHAGE-DETAIL.
           EVALUATE ECRAN-IDX
              WHEN 1 PERFORM DIS-HE-01 THRU DIS-HE-END 
              WHEN OTHER PERFORM DIS-DETAIL THRU DIS-DETAIL-END 
              VARYING IDX FROM 1 BY 1 UNTIL IDX = CHOIX-MAX(ECRAN-IDX)
           END-EVALUATE.

       DIS-DETAIL.
           COMPUTE LIN-IDX = IDX + 2.
           PERFORM GET-VAR.
           DISPLAY PAREX-ANAL(IDX-2) LINE LIN-IDX POSITION 26.
       DIS-DETAIL-2.
           DISPLAY PAREX-PP(IDX-2)   LINE LIN-IDX POSITION 32.
       DIS-DETAIL-3.
           DISPLAY PAREX-PRINT(IDX-2) LINE LIN-IDX POSITION 35.
       DIS-DETAIL-4.
           MOVE PAREX-OCO(IDX-2) TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 38.
           PERFORM READ-OCCOM.
           DISPLAY OCO-NOM LINE LIN-IDX POSITION 41 SIZE 15.
       DIS-DETAIL-5.
           MOVE PAREX-POSTE(IDX-2) TO HE-Z8.
           DISPLAY HE-Z8 LINE LIN-IDX POSITION 54.
           PERFORM READ-POCL.
           IF PAREX-POSTE(IDX-2) = 0
              DISPLAY SPACES  LINE LIN-IDX POSITION 63 SIZE 17
           ELSE
              DISPLAY PC-NOM  LINE LIN-IDX POSITION 63 SIZE 17.
       DIS-DETAIL-6.
           MOVE PAREX-OCC(IDX-2) TO HE-Z.
           DISPLAY HE-Z LINE LIN-IDX POSITION 80.
       DIS-DETAIL-END.
           EXIT.       

       GET-VAR.
           COMPUTE IDX-2 = IDX + (20 * (ECRAN-IDX - 2)).


       DIS-HE-01.
           MOVE PAREX-FIRME TO FIRME-KEY.
           PERFORM READ-FIRME.
           DISPLAY PAREX-FIRME LINE  4 POSITION 27.
           DISPLAY FIRME-NOM LINE 4 POSITION 32.
       DIS-HE-END.
           EXIT.

       END-PROGRAM.
           CLOSE PAREX.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


