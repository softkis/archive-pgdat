      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-PARMOD LECTURE ECRITURE PARAMETRES        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-PARMOD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PARMOD.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "PARMOD.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       01  LNK-RECORD.
           02 LNK-KEY.
              03 LNK-MODULE  PIC X(10).
              03 LNK-FIRME   PIC 9999.

           02 LNK-REC-DET.
              03 LNK-PROG-NUMBER-1        PIC 9(8).
              03 LNK-PROG-NUMBER-2        PIC 9(8).
              03 LNK-PROG-NUMBER-3        PIC 9(8).
              03 LNK-PROG-NUMBER-4        PIC 9(8).
              03 LNK-EXTENSION-1          PIC X(8).
              03 LNK-EXTENSION-2          PIC X(8).
              03 LNK-EXTENSION-3          PIC X(8).
              03 LNK-EXTENSION-4          PIC X(8).
              03 LNK-PATH                 PIC X(40).
              03 LNK-SETTINGS             PIC X(100).
              03 LNK-FILLER               PIC X(100).

       01  ACTION   PIC X.

           
       PROCEDURE DIVISION USING LINK-V LNK-RECORD ACTION.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PARMOD.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-A-PARMOD.

       
           IF NOT-OPEN = 0
              OPEN I-O PARMOD
              MOVE 1 TO NOT-OPEN.

           MOVE LNK-RECORD TO PARMOD-RECORD.
           IF ACTION = "D"
              DELETE PARMOD INVALID CONTINUE END-DELETE
              EXIT PROGRAM
           END-IF.
           IF ACTION = "W"
              WRITE PARMOD-RECORD INVALID REWRITE PARMOD-RECORD 
              END-WRITE
              EXIT PROGRAM
           END-IF.
           READ PARMOD NO LOCK INVALID INITIALIZE PARMOD-TX
           MOVE PARMOD-RECORD TO LNK-RECORD
           EXIT PROGRAM
             NOT INVALID
             MOVE PARMOD-RECORD TO LNK-RECORD
             EXIT PROGRAM.
