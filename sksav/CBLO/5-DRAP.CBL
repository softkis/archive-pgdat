      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 5-DRAP ENLEVEMENT DRAPEAUX JOURNAUX         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    5-DRAP.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURNAL.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURNAL.FDE".

       WORKING-STORAGE SECTION.

       01  CHOIX-MAX             PIC 99 VALUE 5.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  COMPTEUR              PIC 99.
       01  HELP-1                PIC 9(4) COMP-1.
       01  NO-JOURNAL            PIC 9(8) VALUE 0.
       01  JOURNAL-NAME.
           02 PRECISION          PIC X(4) VALUE "S-JO".
           02 FIRME-JOURNAL      PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 ANNEE-JOURNAL      PIC 999.

       01  DATE-COMPTA.
           04 D-COMPTA-A      PIC 9999.
           04 D-COMPTA-M      PIC 99.
           04 D-COMPTA-J      PIC 99.

       01  DATE-TRANS.
           04 D-TRANS-A      PIC 9999.
           04 D-TRANS-M      PIC 99.
           04 D-TRANS-J      PIC 99.

       01  DATE-MAJ.
           04 D-MAJ-A      PIC 9999.
           04 D-MAJ-M      PIC 99.
           04 D-MAJ-J      PIC 99.

       01  ECR-DISPLAY.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURNAL.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-5-DRAP.

           MOVE LNK-SUFFIX TO ANNEE-JOURNAL.
           MOVE FR-KEY     TO FIRME-JOURNAL.
           IF MENU-PROG-NUMBER > 0
              MOVE "S-JM" TO PRECISION.
           IF MENU-PROG-NUMBER = 2001
              MOVE "S-JN" TO PRECISION.

           OPEN I-O JOURNAL.

           CALL "0-TODAY" USING TODAY.
           MOVE TODAY-ANNEE TO D-COMPTA-A.
           MOVE TODAY-MOIS  TO D-COMPTA-M.
           MOVE TODAY-JOUR  TO D-COMPTA-J.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1 THRU 3 MOVE 0000000059 TO EXC-KFR (1)
           WHEN 5     MOVE 0000000025 TO EXC-KFR (1)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
      *    WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3
           WHEN  4 IF MENU-BATCH = 1
                      PERFORM AVANT-NUMBER
                   END-IF
           WHEN  5 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-DATE
                   IF EXC-KEY = 13 
                      ADD 2 TO INDICE-ZONE
                   END-IF
      *    WHEN  2 PERFORM APRES-DATE
      *            IF EXC-KEY = 13 
      *               ADD 1 TO INDICE-ZONE
      *            END-IF
           WHEN  3 PERFORM APRES-DATE
           WHEN  4 IF MENU-BATCH = 1
                      PERFORM APRES-NUMBER
                   END-IF
           WHEN  5 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           MOVE 08320000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-COMPTA
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO D-COMPTA-A
              MOVE TODAY-MOIS  TO D-COMPTA-M
              MOVE TODAY-JOUR  TO D-COMPTA-J
              MOVE 0 TO LNK-NUM.

       AVANT-2.
           MOVE 10320000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-TRANS
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO D-TRANS-A
              MOVE TODAY-MOIS  TO D-TRANS-M
              MOVE TODAY-JOUR  TO D-TRANS-J
              MOVE 0 TO LNK-NUM.

       AVANT-3.
           MOVE 12320000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-MAJ
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO D-MAJ-A
              MOVE TODAY-MOIS  TO D-MAJ-M
              MOVE TODAY-JOUR  TO D-MAJ-J
              MOVE 0 TO LNK-NUM.

       AVANT-NUMBER.
           ACCEPT NO-JOURNAL
             LINE 20 POSITION 32 SIZE 8
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DATE.
           IF LNK-NUM NOT = 0
              MOVE "AA" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-NUMBER.
           IF NO-JOURNAL = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.


       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM JOURNAL THRU JOURNAL-END
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.


       JOURNAL.
           IF  D-TRANS-A  = 0
           AND D-COMPTA-A = 0
           AND D-MAJ-A    = 0
               PERFORM END-PROGRAM.
           INITIALIZE JRL-RECORD.
           START JOURNAL KEY >= JRL-KEY INVALID 
                GO JOURNAL-END.
       JOURNAL-1.
           READ JOURNAL NEXT AT END 
                GO JOURNAL-END.
           IF  NO-JOURNAL NOT = 0
           AND JRL-NUMERO NOT = NO-JOURNAL 
               GO JOURNAL-1.
           IF  JRL-ANNEE-C = D-COMPTA-A
           AND JRL-MOIS-C  = D-COMPTA-M
           AND JRL-JOUR-C  = D-COMPTA-J
               INITIALIZE JRL-DATE-COMPTA JRL-NUMERO
               PERFORM WRITE-JOURNAL.
           IF  JRL-ANNEE-E = D-TRANS-A
           AND JRL-MOIS-E  = D-TRANS-M
           AND JRL-JOUR-E  = D-TRANS-J
               INITIALIZE JRL-DATE-EDITION
               PERFORM WRITE-JOURNAL.
           IF  JRL-ANNEE-M = D-MAJ-A
           AND JRL-MOIS-M  = D-MAJ-M
           AND JRL-JOUR-M  = D-MAJ-J
           AND D-MAJ-A NOT = 0
               DELETE JOURNAL INVALID CONTINUE.
           GO JOURNAL-1.
       JOURNAL-END.
           CLOSE JOURNAL.
           PERFORM END-PROGRAM.

       WRITE-JOURNAL.
           REWRITE JRL-RECORD INVALID CONTINUE.

       DIS-HE-01.
           MOVE D-COMPTA-A TO HE-AA.
           MOVE D-COMPTA-M TO HE-MM.
           MOVE D-COMPTA-J TO HE-JJ.
           DISPLAY HE-DATE LINE  8 POSITION 32.

       DIS-HE-02.
           MOVE D-TRANS-A TO HE-AA.
           MOVE D-TRANS-M TO HE-MM.
           MOVE D-TRANS-J TO HE-JJ.
           DISPLAY HE-DATE LINE 10 POSITION 32.

       DIS-HE-03.
           MOVE D-MAJ-A TO HE-AA.
           MOVE D-MAJ-M TO HE-MM.
           MOVE D-MAJ-J TO HE-JJ.
           DISPLAY HE-DATE LINE 12 POSITION 32.

       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE  437 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 4.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *-----------------------------
      *    Routines standard: clause copies
      *-----------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

