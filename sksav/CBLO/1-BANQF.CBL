      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-BANQF  GESTION DES BANQUES FIRMES         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-BANQF .

       ENVIRONMENT DIVISION.
      *袴袴袴袴袴袴袴袴袴袴
       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "BANQF.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "BANQF.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "BANQUE.REC".
           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 5. 
       01  TEST-COMPTE           PIC X(50). 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON BANQF.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-1-BANQF .

           OPEN I-O BANQF .

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0017292415 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2
           WHEN  3 PERFORM AVANT-3
           WHEN  4 PERFORM AVANT-4
           WHEN  5 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM APRES-1 
               WHEN  2 PERFORM APRES-2 
               WHEN  3 PERFORM APRES-3 
               WHEN  4 PERFORM APRES-YN
               WHEN  5 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE.

           IF EXC-KEY = 13 
           IF INPUT-ERROR = 0
              ADD 1 TO INDICE-ZONE.
           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT BQF-CODE
             LINE  4 POSITION 20 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT BQF-COMPTE
             LINE  5 POSITION 20 SIZE 50
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           ACCEPT BQF-TITULAIRE
             LINE  7 POSITION 20 SIZE 35
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT BQF-TITULAIRE CONVERTING
           "굤닀뀑뙎봺뼏꽇쉸�" TO "EEEEAIIOOUUUAOUAE".

       AVANT-4.
           ACCEPT BQF-ACTIF
             LINE 9 POSITION 20 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE FR-KEY TO BQF-FIRME.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-BANQUE" USING LINK-V BQ-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-HE-01 THRU DIS-HE-END
                   PERFORM DISPLAY-F-KEYS
                   IF BQ-CODE NOT = SPACES
                      MOVE BQ-CODE TO BQF-CODE 
                   END-IF
                   MOVE 1 TO INPUT-ERROR
            WHEN  3 CALL "2-BANQF" USING LINK-V BQF-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    PERFORM DISPLAY-F-KEYS
                    MOVE 1 TO INPUT-ERROR
           WHEN   4 PERFORM NEXT-BANQF 
           WHEN   5 MOVE BQF-CODE TO LNK-TEXT
                    CALL "1-BANQUE" USING LINK-V
                    MOVE LNK-TEXT TO BQF-CODE BQ-CODE
                    PERFORM AFFICHAGE-ECRAN 
           WHEN  65 THRU 66 PERFORM NEXT-BANQUE
                    MOVE BQ-CODE TO BQF-CODE 
           WHEN OTHER READ BANQF  INVALID 
                      INITIALIZE BQF-REC-DET 
                      END-READ.
           MOVE BQF-CODE TO BQ-CODE.
           PERFORM READ-BANQUE.
           MOVE NOT-FOUND TO INPUT-ERROR
           IF BQF-KEY = SPACES MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       APRES-2.
           IF BQF-COMPTE-PAYS NUMERIC 
           OR BQF-COMPTE-PAYS = SPACES 
              IF BQ-PAYS = "NL"
              AND BQ-SWIFT-BQ NOT = "PSTB"
                 MOVE BQF-COMPTE TO TEST-COMPTE
                 CALL "0-NL11" USING LINK-V TEST-COMPTE
                 IF LNK-NUM NOT = 0
                    MOVE "SL" TO LNK-AREA
                    PERFORM DISPLAY-MESSAGE
                    MOVE 1 TO INPUT-ERROR
                 END-IF
              END-IF
           ELSE
              MOVE BQF-COMPTE TO TEST-COMPTE
              CALL "0-IBAN" USING LINK-V TEST-COMPTE
              MOVE TEST-COMPTE TO BQF-COMPTE
              IF LNK-NUM NOT = 0
                 MOVE "SL" TO LNK-AREA
                 PERFORM DISPLAY-MESSAGE
                 MOVE 1 TO INPUT-ERROR
              END-IF
              MOVE 0 TO IDX
              INSPECT BQF-COMPTE TALLYING IDX FOR CHARACTERS BEFORE " "
              IF IDX < 16
                  MOVE 54 TO LNK-NUM
                  MOVE "SL" TO LNK-AREA
                  PERFORM DISPLAY-MESSAGE
                  MOVE 1 TO INPUT-ERROR
              END-IF
              IF BQ-SWIFT-PAYS NOT = BQF-COMPTE-PAYS
                 MOVE 77 TO LNK-NUM
                 MOVE "SL" TO LNK-AREA
                 PERFORM DISPLAY-MESSAGE
                 MOVE 1 TO INPUT-ERROR
               END-IF
           END-IF.
           PERFORM DIS-HE-02.

       APRES-3.
           PERFORM DIS-HE-03.

       APRES-YN.
           IF BQF-ACTIF = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE SPACES TO BQF-ACTIF 
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           MOVE FR-KEY TO BQF-FIRME.
           EVALUATE EXC-KEY
           WHEN 5 CALL "0-TODAY" USING TODAY
                  MOVE TODAY-TIME TO BQF-TIME
                  MOVE LNK-USER TO BQF-USER
                  IF LNK-SQL = "Y" 
                     CALL "9-BANQF" USING LINK-V BQF-RECORD WR-KEY 
                  END-IF
                  WRITE BQF-RECORD INVALID REWRITE BQF-RECORD 
                  END-WRITE
                  MOVE BQ-CODE TO LNK-TEXT
                  MOVE 1 TO DECISION
           WHEN 8 IF LNK-SQL = "Y" 
                     CALL "9-BANQF" USING LINK-V BQF-RECORD DEL-KEY 
                  END-IF
                  DELETE BANQF INVALID CONTINUE END-DELETE
                  INITIALIZE BQF-RECORD 
                  MOVE 1 TO DECISION.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-BANQUE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD EXC-KEY.
               
       READ-BANQUE.
           MOVE 0 TO NOT-FOUND.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD FAKE-KEY.
           IF BQ-CODE = SPACES 
              MOVE 1 TO NOT-FOUND.


       NEXT-BANQF .
           MOVE FR-KEY TO BQF-FIRME.
           START BANQF  KEY > BQF-KEY INVALID KEY
                INITIALIZE BQF-RECORD BQ-RECORD
                NOT INVALID
           READ BANQF  NEXT AT END 
                INITIALIZE BQF-RECORD BQ-RECORD
                MOVE 1 TO INDICE-ZONE
                END-READ.
           IF FR-KEY NOT = BQF-FIRME
              INITIALIZE BQF-RECORD BQ-RECORD
                MOVE 1 TO INDICE-ZONE.
           MOVE BQF-KEY TO BQ-CODE.  
        

       DIS-HE-01.
           IF BQ-NOM = SPACES
              MOVE "???" TO BQ-NOM
           END-IF.
           DISPLAY BQF-CODE  LINE  4 POSITION 20.
           DISPLAY BQ-NOM LINE 4 POSITION 30 HIGH.
           DISPLAY BQ-SWIFT LINE 4 POSITION 70 HIGH.
           IF EXC-KEY NOT = 5 MOVE BQ-CODE TO LNK-TEXT.
       DIS-HE-02.
           DISPLAY BQF-COMPTE  LINE  5 POSITION 20.
       DIS-HE-03.
           DISPLAY BQF-TITULAIRE  LINE  7 POSITION 20.
       DIS-HE-04.
           DISPLAY BQF-ACTIF  LINE  9 POSITION 20.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 140  TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE BANQF .
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
           
      