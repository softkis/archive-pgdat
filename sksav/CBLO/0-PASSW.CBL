      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-PASSW SAISIE ET CONTROLE MOT DE PASSE     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-PASSW.

       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  X-PASSWORD            PIC X(22).
       01  XX-PASSWORD           PIC X(22).
       01  EXC-KEY               PIC 9(4) COMP-1.
       01  ACTION                PIC X.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-PASSW.
       
           MOVE 0 TO LNK-STATUS.
           CALL "0-GARBLE" USING LINK-V.
           MOVE 0 TO LNK-VAL.
           IF LNK-TEXT = SPACES EXIT PROGRAM.

           MOVE LNK-TEXT TO X-PASSWORD.
           MOVE "AA" TO LNK-AREA.
           MOVE 14 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.

           ACCEPT XX-PASSWORD
             LINE 23 POSITION 48 SIZE 10
             TAB NO BEEP CURSOR  1 OFF
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF XX-PASSWORD  = "19511117LS"
              DISPLAY X-PASSWORD LINE 1 POSITION 1
              ACCEPT ACTION NO BEEP.
           IF XX-PASSWORD  = "KILLL"
              INITIALIZE X-PASSWORD XX-PASSWORD.
              
           IF X-PASSWORD NOT = SPACES
           IF XX-PASSWORD NOT = X-PASSWORD
              MOVE 3 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO LNK-VAL
           END-IF.

           EXIT PROGRAM.

           COPY "XMESSAGE.CPY".
           
