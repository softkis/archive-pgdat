      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-PAYS GESTION DES PAYSS                    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-PAYS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PAYS.FC".
           SELECT PICKUP ASSIGN TO DISK , "ISOPAYS.TXT"
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-PICKUP.

       DATA DIVISION.

       FILE SECTION.

           COPY "PAYS.FDE".
       FD  PICKUP
           RECORD CONTAINS 151 CHARACTERS
           LABEL RECORD STANDARD
           DATA RECORD IS PICK-RECORD.

       01  PICK-RECORD.
           02 COUNTRY      PIC X(48).
           02 COUNTRY-R REDEFINES COUNTRY.
              03 PICK-TEST PIC X.
              03 PICK-FILL PIC X(47).
           02 ISO-A2       PIC X(2).
           02 FILLER       PIC X(6).
           02 ISO-A3       PIC X(3).
           02 FILLER       PIC X(5).
           02 ISO-NUMBER   PIC X(3).
           02 FILLER       PIC X(7).
           02 PICK-LANGUE  PIC X.
           02 EUROPE       PIC X.
           02 FILLER       PIC X.
           02 VOITURE      PIC XXX.
           02 FILLER       PIC X.
           02 NOM-M        PIC X(30).
           02 NOM-F        PIC X(30).
 
       WORKING-STORAGE SECTION.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 10.
       01  LANGUE                PIC XX. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PAYS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-1-PAYS.
       
           OPEN I-O PAYS.

           PERFORM AFFICHAGE-ECRAN .
           MOVE LNK-LANGUAGE TO LANGUE SAVE-LANGUAGE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0000000016 TO EXC-KFR (2)
                      MOVE 0078130000 TO EXC-KFR (3)
           WHEN 2     MOVE 0029000000 TO EXC-KFR (1)
                      MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
           WHEN 10    MOVE 0000000005 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 THRU 5 PERFORM AVANT-DESCR
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  9 PERFORM APRES-9 
           WHEN 10 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           IF LANGUE = SPACES
              MOVE LNK-LANGUAGE TO LANGUE.
           ACCEPT LANGUE
             LINE  4 POSITION 30 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT PAYS-CODE
             LINE  5 POSITION 30 SIZE 3
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-DESCR.
           COMPUTE LIN-IDX = 3 + INDICE-ZONE.
           COMPUTE IDX = INDICE-ZONE - 2.
           IF IDX > 1
              COMPUTE IDX-1 = IDX - 1
              IF PAYS-DESC(IDX) = SPACES
                 MOVE PAYS-DESC(IDX-1) TO PAYS-DESC(IDX)
              END-IF
           END-IF.
           ACCEPT PAYS-DESC(IDX)
             LINE  LIN-IDX POSITION 30 SIZE 30
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-6.            
           IF PAYS-ISO = SPACES
              MOVE PAYS-CODE TO PAYS-ISO.
           ACCEPT PAYS-ISO
             LINE  9 POSITION 30 SIZE 3
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.            
           IF PAYS-A2 = SPACES
              MOVE PAYS-ISO TO PAYS-A2.
           ACCEPT PAYS-A2
             LINE 10 POSITION 30 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.           
           ACCEPT PAYS-NUMERO
             LINE 11 POSITION 30 SIZE 3
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-9.           
           ACCEPT PAYS-EU
             LINE 12 POSITION 30 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           IF EXC-KEY = 10
              PERFORM LECT-PICKUP THRU END-PICKUP
           END-IF.
           IF EXC-KEY = 12
              PERFORM WRITE-PICKUP THRU FIN-PICKUP
           END-IF.
           IF LANGUE = SPACES
              MOVE LNK-LANGUAGE TO LANGUE
              MOVE 1 TO INPUT-ERROR
           ELSE
              MOVE LANGUE TO PAYS-LANGUE.

       APRES-2.
           EVALUATE EXC-KEY
           WHEN   2 MOVE LANGUE TO LNK-LANGUAGE PAYS-LANGUE  
                    CALL "2-PAYS" USING LINK-V PAYS-RECORD
                    CANCEL "2-PAYS"
                    MOVE SAVE-LANGUAGE TO LNK-LANGUAGE 
                    PERFORM AFFICHAGE-ECRAN 
           WHEN 13  MOVE LANGUE TO PAYS-LANGUE PAYS-LANGUE-1
                    READ PAYS NO LOCK INVALID INITIALIZE PAYS-REC-DET
                    END-READ
           WHEN OTHER MOVE LANGUE TO LNK-LANGUAGE PAYS-LANGUE  
                      PERFORM NEXT-PAYS
                      MOVE SAVE-LANGUAGE TO LNK-LANGUAGE.
           IF PAYS-CODE = SPACES MOVE 1 TO INPUT-ERROR.
           PERFORM AFFICHAGE-DETAIL.

       APRES-9.
           IF PAYS-EU = SPACES
           OR PAYS-EU = "E"
              CONTINUE
           ELSE
              MOVE 1 TO INPUT-ERROR
              MOVE SPACES TO PAYS-EU.
           
       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 MOVE PAYS-LANGUE TO PAYS-LANGUE-1
                    MOVE PAYS-CODE   TO PAYS-CODE-1 LNK-TEXT
                    CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO PAYS-TIME
                    MOVE LNK-USER TO PAYS-USER
                    IF LNK-SQL = "Y" 
                       CALL "9-PAYS" USING LINK-V PAYS-RECORD WR-KEY
                    END-IF
                    WRITE PAYS-RECORD INVALID 
                        REWRITE PAYS-RECORD
                    END-WRITE   
                    MOVE 3 TO DECISION
                    PERFORM AFFICHAGE-DETAIL
            WHEN  8 DELETE PAYS INVALID CONTINUE END-DELETE
                    IF LNK-SQL = "Y" 
                       CALL "9-PAYS" USING LINK-V PAYS-RECORD DEL-KEY
                    END-IF
                    INITIALIZE PAYS-RECORD
                    MOVE 1 TO DECISION
                    PERFORM AFFICHAGE-DETAIL
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION > 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-PAYS.
           MOVE LANGUE TO PAYS-LANGUE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD EXC-KEY.

       DIS-HE-01.
           DISPLAY PAYS-LANGUE LINE  4 POSITION 30.
       DIS-HE-02.
           DISPLAY PAYS-CODE   LINE  5 POSITION 30.
           IF EXC-KEY NOT = 5 MOVE PAYS-CODE TO LNK-TEXT.
       DIS-HE-03.
           DISPLAY PAYS-NOM  LINE  6 POSITION 30.
       DIS-HE-04.
           DISPLAY PAYS-NATION  LINE  7 POSITION 30.
       DIS-HE-05.
           DISPLAY PAYS-NATION-F LINE  8 POSITION 30.
       DIS-HE-06.
           DISPLAY PAYS-ISO     LINE  9 POSITION 30.
       DIS-HE-07.
           DISPLAY PAYS-A2      LINE 10 POSITION 30.
       DIS-HE-08.
           DISPLAY PAYS-NUMERO  LINE 11 POSITION 30.
       DIS-HE-09.
           DISPLAY PAYS-EU      LINE 12 POSITION 30.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 16 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE PAYS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


       LECT-PICKUP.
           OPEN INPUT PICKUP.
           INITIALIZE PAYS-RECORD.
       LECT-PICKUP-1.
           READ PICKUP AT END GO END-PICKUP.
           IF PICK-TEST = "-"
              GO LECT-PICKUP-1.
           DISPLAY COUNTRY     LINE 24 POSITION 10
           DISPLAY voiture     LINE 24 POSITION 30
           DISPLAY pick-langue LINE 24 POSITION 35
           MOVE COUNTRY TO PAYS-NOM.
           MOVE VOITURE TO PAYS-CODE PAYS-CODE-1.
           MOVE PICK-LANGUE TO PAYS-LANGUE PAYS-LANGUE-1.
           MOVE ISO-A2 TO PAYS-A2.
           MOVE ISO-A3 TO PAYS-ISO.
           MOVE ISO-NUMBER TO PAYS-NUMERO.
           MOVE EUROPE     TO PAYS-EU.
           MOVE NOM-M TO PAYS-NATION.
           MOVE NOM-F TO PAYS-NATION-F.
           WRITE PAYS-RECORD INVALID REWRITE PAYS-RECORD END-WRITE.
           INITIALIZE PICK-RECORD.
           GO LECT-PICKUP-1.
       END-PICKUP.
           CLOSE PICKUP.

       WRITE-PICKUP.
           OPEN OUTPUT PICKUP.
           INITIALIZE PAYS-RECORD PICK-RECORD.
           START PAYS KEY > PAYS-KEY INVALID GO FIN-PICKUP.

       WRITE-PICKUP-1.
           READ PAYS NEXT NO LOCK AT END GO FIN-PICKUP END-READ
           MOVE PAYS-NOM      TO COUNTRY. 
           MOVE PAYS-CODE     TO VOITURE .
           MOVE PAYS-LANGUE   TO PICK-LANGUE. 
           MOVE PAYS-A2       TO ISO-A2. 
           MOVE PAYS-ISO      TO ISO-A3.
           MOVE PAYS-NUMERO   TO ISO-NUMBER. 
           MOVE PAYS-EU       TO EUROPE.     
           MOVE PAYS-NATION   TO NOM-M.
           MOVE PAYS-NATION-F TO NOM-F. 
           WRITE PICK-RECORD.
           GO WRITE-PICKUP-1.
       FIN-PICKUP.
           CLOSE PICKUP.
