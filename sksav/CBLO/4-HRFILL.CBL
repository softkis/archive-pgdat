      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-HRFILL COMPLETER  HEURES D'OCCUPATION     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-HRFILL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  JOUR-IDX          PIC 99.
       01  JOUR              PIC 99.
       01  NOT-OPEN          PIC 9 VALUE 0.
       01  COMPLEMENT        PIC 99 VALUE 0.
       77  HELP-5     PIC S999V99 COMP-3.

       01  TOTAL-GLOBAL.
           03 TOT-HRS PIC S9(6)V99 COMP-3 OCCURS 32.
           03 T-G PIC S9(6)V99 COMP-3 OCCURS 32.
           03 MALADE PIC 9 OCCURS 31.

       01  TOTAL-COMPL.
           02 T-C OCCURS 40.
              03 TOT-COMPL PIC S9(6)V99 COMP-3 OCCURS 32.

       01  JOURS-NAME.
           02 JOURS-ID           PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "CCOL.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "POSE.REC".
           COPY "HORSEM.REC".
           COPY "CALEN.REC".
           COPY "OCCUP.REC".
       01  JOUR-DEB          PIC 99.
       01  JOUR-FIN          PIC 99.

       PROCEDURE DIVISION USING LINK-V
                                REG-RECORD
                                PRESENCES
                                CAR-RECORD
                                CCOL-RECORD
                                POSE-RECORD
                                HJS-RECORD
                                CAL-RECORD
                                OCC-RECORD
                                JOUR-DEB
                                JOUR-FIN.
       
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-4-HRFILL.

           IF  CCOL-OCC-NULL = "N"
           AND CAR-POSTE-FRAIS = 0
           AND CAR-CONGE NOT   = 8
              PERFORM END-PROGRAM
           END-IF.              
       
           IF NOT-OPEN = 0
              MOVE LNK-SUFFIX TO ANNEE-JOURS
              OPEN I-O JOURS
              MOVE 1 TO NOT-OPEN.

           IF JOUR-DEB = 0
              MOVE 1 TO JOUR-DEB.
           IF JOUR-FIN = 0
              MOVE MOIS-JRS(LNK-MOIS) TO JOUR-FIN.
           MOVE 0 TO COMPLEMENT.
           INITIALIZE TOTAL-GLOBAL TOTAL-COMPL.
           PERFORM INIT-HEURES THRU INIT-HEURES-END.
           INITIALIZE JRS-RECORD.
           PERFORM KEY-JOURS.
           READ JOURS INVALID INITIALIZE JRS-REC-DET.
           PERFORM FILL-HEURE VARYING JOUR-IDX FROM JOUR-DEB BY 1 
           UNTIL JOUR-IDX > JOUR-FIN.
           MOVE 0 TO COMPLEMENT JRS-HRS(32) JRS-JOURS.
           PERFORM ADD-JOURS VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 31.
           IF JRS-HRS(32) > 0
              PERFORM WRITE-JOURS
              PERFORM WRITE-JRS
           END-IF.
           GO END-PROGRAM.


       FILL-HEURE.
           IF PRES-JOUR(LNK-MOIS, JOUR-IDX) > 0
           AND MALADE(JOUR-IDX) = 0
              IF POSE-FIRME = 0
                 PERFORM FILL-HEURE-1
              ELSE
                 PERFORM FILL-POSE
              END-IF
           END-IF.

       FILL-HEURE-1.
           MOVE SEM-IDX(LNK-MOIS, JOUR-IDX) TO IDX-3.
           MOVE HJS-HEURES(IDX-3) TO HELP-5.
           
           IF LNK-VAL-2 > 0 
           AND LNK-VAL-2 < 100
              COMPUTE HELP-5 = HELP-5 / 100 * LNK-VAL-2 
           END-IF.
           IF CAL-JOUR(LNK-MOIS, JOUR-IDX) = 0 
           AND OCC-PERMIS-FERIE < 2 
              PERFORM ADD-HRS
           END-IF.
           IF CAL-JOUR(LNK-MOIS, JOUR-IDX) = 1 
           AND OCC-PERMIS-FERIE = 2 
              PERFORM ADD-HRS
           END-IF.

       ADD-HRS.
           IF TOT-HRS(JOUR-IDX) < HELP-5
           AND HELP-5 > 0
           AND JRS-HRS(JOUR-IDX) = 0
              COMPUTE JRS-HRS(JOUR-IDX) = HELP-5 - T-G(JOUR-IDX)
           END-IF.
           IF LNK-VAL > 0 
           AND LNK-VAL NOT = JRS-HRS(JOUR-IDX) 
              MOVE LNK-VAL TO JRS-HRS(JOUR-IDX) 
           END-IF.
           PERFORM TEST-NUIT.
           PERFORM TEST-DIM.
           PERFORM TEST-FERIE.
           PERFORM TEST-SAMEDI.

       FILL-POSE.
           MOVE CAR-HRS-JOUR TO HELP-5.
           IF LNK-VAL-2 > 0 
           AND LNK-VAL-2 < 100
              COMPUTE HELP-5 = HELP-5 / 100 * LNK-VAL-2 
           END-IF.
           IF OCC-KEY = 0
              IF POSE-JOUR(LNK-MOIS, JOUR-IDX) > 0 
                 PERFORM ADD-HRS
              END-IF
           END-IF.

       TEST-NUIT.
           IF  OCC-NUIT = 1
           AND POSE-JOUR(LNK-MOIS, JOUR-IDX) > 2
              COMPUTE IDX = POSE-JOUR(LNK-MOIS, JOUR-IDX) - 2
              MOVE JRS-HRS(JOUR-IDX) TO TOT-COMPL(IDX, JOUR-IDX)
           END-IF.

       TEST-DIM.
           IF OCC-DIM > 0 
           AND SEM-IDX(LNK-MOIS, JOUR-IDX) = 7 
              IF CCOL-TAUX-LIBRE(10) > 0
              OR CCOL-FACT-REPOS(10) > 0
                 MOVE JRS-HRS(JOUR-IDX) TO TOT-COMPL(20, JOUR-IDX)
              END-IF
           END-IF.

       TEST-FERIE.
           IF OCC-FERIE > 0 
           AND CAL-JOUR(LNK-MOIS, JOUR-IDX) = 1
              IF CCOL-TAUX-LIBRE(1) > 0
              OR CCOL-FACT-REPOS(1) > 0
                 MOVE JRS-HRS(JOUR-IDX) TO TOT-COMPL(11, JOUR-IDX)
              END-IF
           END-IF.

       TEST-SAMEDI.
           IF SEM-IDX(LNK-MOIS, JOUR-IDX) = 9 
              IF CCOL-TAUX-LIBRE(9) > 0
              OR CCOL-FACT-REPOS(9) > 0
                 MOVE JRS-HRS(JOUR-IDX) TO TOT-COMPL(19, JOUR-IDX)
              END-IF
           END-IF.

       END-PROGRAM.
           EXIT PROGRAM.

       KEY-JOURS.
           MOVE FR-KEY TO JRS-FIRME JRS-FIRME-1 JRS-FIRME-2 JRS-FIRME-3.
           MOVE REG-PERSON TO JRS-PERSON JRS-PERSON-1 
                              JRS-PERSON-2 JRS-PERSON-3.
           MOVE LNK-MOIS TO JRS-MOIS JRS-MOIS-1 JRS-MOIS-2 JRS-MOIS-3.
           MOVE OCC-KEY TO JRS-OCCUPATION   JRS-OCCUPATION-1 
                           JRS-OCCUPATION-2 JRS-OCCUPATION-3.
           MOVE COMPLEMENT TO JRS-COMPLEMENT JRS-COMPLEMENT-1 
                              JRS-COMPLEMENT-2 JRS-COMPLEMENT-3.

       INIT-HEURES.
           INITIALIZE TOTAL-GLOBAL TOTAL-COMPL.
           PERFORM KEY-JOURS.
           MOVE 0 TO JRS-OCCUPATION COMPLEMENT.
           START JOURS KEY >= JRS-KEY INVALID GO INIT-HEURES-END.

       INIT-HEURES-1.
           READ JOURS NEXT NO LOCK AT END GO INIT-HEURES-END.
           IF FR-KEY     NOT = JRS-FIRME 
           OR LNK-MOIS   NOT = JRS-MOIS 
           OR REG-PERSON NOT = JRS-PERSON 
              GO INIT-HEURES-END.
           IF JRS-POSTE NOT = 0
              GO INIT-HEURES-1.
           IF JRS-COMPLEMENT > 0
              IF JRS-OCCUPATION = OCC-KEY
                 MOVE JRS-HEURES TO T-C(JRS-COMPLEMENT)
              END-IF
              GO INIT-HEURES-1
           END-IF.
           IF JRS-OCCUPATION = 99
              GO INIT-HEURES-1.
           PERFORM ADD-GLOBAL VARYING JOUR-IDX FROM 1 BY 1 
           UNTIL JOUR-IDX > 31.
           GO INIT-HEURES-1.
       INIT-HEURES-END.
           EXIT.
       
       ADD-JOURS.
           IF JRS-HRS(IDX-1) > 0
              ADD JRS-HRS(IDX-1) TO JRS-HRS(32)
              ADD 1 TO JRS-JOURS.

       ADD-GLOBAL.
           IF JRS-HRS(JOUR-IDX) > 0
              ADD JRS-HRS(JOUR-IDX) TO T-G(JOUR-IDX)
           END-IF.
           IF JRS-HRS(JOUR-IDX) < 0
              MOVE 1 TO MALADE(JOUR-IDX).

       WRITE-JRS.
           PERFORM RECORD-JRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.
           
       RECORD-JRS.
           INITIALIZE TOT-COMPL(IDX, 32).
           PERFORM ADD-UP VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 31.
           MOVE IDX TO JRS-COMPLEMENT   JRS-COMPLEMENT-1
                       JRS-COMPLEMENT-2 JRS-COMPLEMENT-3.
           IF TOT-COMPL(IDX, 32) > 0
              INITIALIZE JRS-RECORD
              MOVE IDX TO COMPLEMENT
              MOVE T-C(IDX) TO JRS-HEURES
              PERFORM WRITE-JOURS
           END-IF.

       ADD-UP.
           ADD TOT-COMPL(IDX, IDX-1) TO TOT-COMPL(IDX, 32).

       WRITE-JOURS.
           PERFORM KEY-JOURS.
           IF JRS-HRS(32) > 0
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-TIME TO JRS-TIME
              MOVE LNK-USER   TO JRS-USER
              IF LNK-SQL = "Y" 
                 CALL "9-JOURS" USING LINK-V JRS-RECORD WR-KEY 
              END-IF
              WRITE JRS-RECORD INVALID REWRITE JRS-RECORD END-WRITE
           END-IF.
           IF CCOL-OCC-NULL = "N"
           AND JRS-OCCUPATION = 0
              MOVE CAR-POSTE-FRAIS TO 
              JRS-POSTE JRS-POSTE-1 JRS-POSTE-2 JRS-POSTE-3
              IF LNK-SQL = "Y" 
                 CALL "9-JOURS" USING LINK-V JRS-RECORD WR-KEY 
              END-IF
              WRITE JRS-RECORD INVALID REWRITE JRS-RECORD END-WRITE
           END-IF.

