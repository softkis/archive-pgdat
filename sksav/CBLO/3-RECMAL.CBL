      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-RECMAL IMPRESSION RECAPITULATION MALADIE  �
      *  � PAR PERSONNE DE MOIS DEBUT A MOIS FIN                 �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-RECMAL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM130.FDE".
       FD  TF-TRANS
           RECORD VARYING DEPENDING ON MAX-LONGUEUR
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 240 DEPENDING MAX-LONGUEUR.
      

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "130       ".
       01  CHOIX-MAX             PIC 99 VALUE 10.
       01  MAX-LIGNES            PIC 9 VALUE 2.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "DIVISION.REC".
           COPY "IMPRLOG.REC".
           COPY "LIVRE.REC".
           COPY "MESSAGE.REC".
           COPY "PARMOD.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

       01  T1-RECORD.
           02 T1-A.
              03 T1-FIRME  PIC 9(6).
              03 T1-DELIM0 PIC X VALUE ";".
              03 T1-PERSON PIC 9(8).
              03 T1-DELIM1 PIC X VALUE ";".
              03 T1-NOM    PIC X(40).
              03 T1-DELIM2 PIC X VALUE ";".

              03 T1-FILLER PIC X(6) VALUE SPACES.
              03 T1-COUT   PIC Z(8).
              03 T1-DELIM3 PIC X VALUE ";".
              03 T1-ANNEE  PIC ZZZZZ.
              03 T1-DELIM4 PIC X VALUE ";".
              03 T1-TYPE   PIC ZZZZ.
              03 T1-DELIM5 PIC X VALUE ";".
              03 T1-TEXT   PIC X(15).
              03 T1-DELIM5 PIC X VALUE ";".
              03 T1-M OCCURS 14.
                 05 T1-MOIS PIC -ZZZZZ,ZZ.
                 05 T1-DEL  PIC X.

       01  TXT-RECORD.
           02 TXT-A.
              03 TXT-FIRME  PIC X(6).
              03 TXT-DELIM0 PIC X VALUE ";".
              03 TXT-PERSON PIC X(8).
              03 TXT-DELIM1 PIC X VALUE ";".
              03 TXT-NOM    PIC X(40).
              03 TXT-DELIM2 PIC X VALUE ";".

              03 TXT-COUT   PIC X(14).
              03 TXT-DELIM3 PIC X VALUE ";".
              03 TXT-ANNEE  PIC XXXXX.
              03 TXT-DELIM4 PIC X VALUE ";".
              03 TXT-TYPE   PIC XXXX.
              03 TXT-DELIM5 PIC X VALUE ";".
              03 TXT-TEXT   PIC X(15).
              03 TXT-DELIM5 PIC X VALUE ";".
              03 TXT-M OCCURS 13.
                 05 TXT-MOIS PIC X(9).
                 05 TXT-DEL  PIC X.
              03 TXT-JOURS  PIC X(9).
              03 TXT-DELIM9 PIC X VALUE ";".

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.
       01  POINT-IDX             PIC 99.
       01  COMPTEUR              PIC 9999 COMP-1.
       01  MAX-LONGUEUR          PIC 9999 VALUE 240.
       01  ASCII-FILE            PIC 9 VALUE 0.
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  AJOUTE                PIC 9 VALUE 0.
       01  TABLEAU               PIC 9 VALUE 0.

       01  DEBUT-FIN.
           02 DEBUT-J PIC 99.
           02 DEBUT-M PIC 99.
           02 FIN-J PIC 99.
           02 FIN-M PIC 99.

           COPY "V-VH00.CPY".


       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".RMA".

           COPY "V-VAR.CPY".
        
       01  PERSONNE              PIC 9(6) VALUE 0.
       01  BEG-PERSON            PIC 9(6).
       01  BEG-MATCHCODE         PIC X(10).
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.
       01  DIV                   PIC 9(8) VALUE 0.
       01  ABSENTEISME           PIC X VALUE "N".

       01 HE-SEL.
          02 H-S PIC X       OCCURS 20.

       01  HELP-CUMUL.
           02 HR-MAL-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-ACC-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-HOS-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-FAM-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-PRM-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-DIS-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-LON-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-NON-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-----MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-MAT-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-EMP-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-MED-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-TOT-MOIS        PIC 9(4)V99 OCCURS 13.
           02 HR-THEO-MOIS       PIC 9(4)V99 OCCURS 13.
       01  HELP-CUMUL-R REDEFINES HELP-CUMUL.
           02 HR-MALADIE OCCURS 14.
              03 H-M             PIC 9(4)V99 OCCURS 13.

       01  SAVE-UNITS.
           02 SV-UNIT            PIC 9(3)V99 OCCURS 12.

       01  HELP-TOTAL.
           02 HT-TOT-MOIS        PIC 9(6)V99 OCCURS 13.
           02 HT-THEO-MOIS       PIC 9(6)V99 OCCURS 13.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM
               TF-TRANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-RECMAL.

           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-SETTINGS TO HE-SEL.
       
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE LNK-MOIS TO SAVE-MOIS.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE HELP-TOTAL.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8 THRU 9
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 10    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700009278 TO EXC-KFR(2)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-8
           WHEN  9 PERFORM APRES-9
           WHEN 10 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-7.
           IF PARMOD-FILLER-2 > SPACES
              MOVE PARMOD-FILLER-2 TO ABSENTEISME.
           ACCEPT ABSENTEISME 
             LINE 13 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-9.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
       AVANT-PATH.
           MOVE "AA" TO LNK-AREA.
           MOVE 32 TO LNK-NUM.
           MOVE "L" TO LNK-LOW.
           PERFORM DISPLAY-MESSAGE.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 24 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-7.
           IF ABSENTEISME = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
              MOVE ABSENTEISME TO PARMOD-FILLER-2
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W"
           ELSE
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-07.

       APRES-8.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-08.

       APRES-9.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-09.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           IF EXC-KEY = 9 
              MOVE 1 TO TABLEAU
              MOVE 10 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM TRAITEMENT-RUN
           WHEN 10 MOVE 1 TO ASCII-FILE
                   PERFORM AVANT-PATH
                   PERFORM TRAITEMENT-RUN
                   PERFORM END-PROGRAM
            WHEN 68 MOVE "AM" TO LNK-AREA
                    CALL "5-SEL" USING LINK-V PARMOD-RECORD
                    MOVE PARMOD-SETTINGS TO HE-SEL
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    PERFORM DISPLAY-F-KEYS
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM DIS-HE-01.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-3
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
               GO READ-PERSON-3
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
               GO READ-PERSON-3
           END-IF.
           IF  DIV > 0 
           AND DIV NOT = CAR-DIVISION
               GO READ-PERSON-3
           END-IF.
           INITIALIZE HELP-CUMUL SAVE-UNITS.
           PERFORM HRS-NORMALES VARYING IDX-2 FROM MOIS-DEBUT
           BY 1 UNTIL IDX-2 > MOIS-FIN.
           IF HR-TOT-MOIS(13) = 0
              GO READ-PERSON-3
           END-IF.
           PERFORM DIS-HE-01.
           IF ASCII-FILE = 0
              PERFORM FILL-FILES
           ELSE
              PERFORM WRITE-TEXT
           END-IF.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.
           EXIT.


       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.


       TRANSMET.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       TOTAUX.
           ADD 1 TO LIN-NUM.
           IF LIN-NUM >= 67
      *    IMPL-MAX-LINE
              PERFORM TRANSMET
              PERFORM READ-FORM
              PERFORM ENTETE
              COMPUTE LIN-NUM = LIN-IDX - 1
           END-IF.
           IF ABSENTEISME NOT = "N"
              PERFORM FILL-THEO VARYING IDX FROM MOIS-DEBUT BY 1 
              UNTIL IDX > 13
              ADD 1 TO LIN-NUM.

           PERFORM FILL-TOTAL VARYING IDX FROM MOIS-DEBUT BY 1 
           UNTIL IDX > 13.
           ADD 1 TO LIN-NUM.
           PERFORM FILL-PCT VARYING IDX FROM MOIS-DEBUT BY 1 
           UNTIL IDX > MOIS-FIN.
           MOVE 13 TO IDX.
           PERFORM FILL-PCT.


       FILL-FILES.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE IDX = LIN-NUM + 3.
           IF IDX >= 66
      *    IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              COMPUTE LIN-NUM = LIN-IDX - 1
           END-IF.
           ADD 1 TO LIN-NUM.

      * DONNEES PERSONNE
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  6 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           PERFORM COLONNES VARYING IDX FROM 1 BY 1 UNTIL IDX > 14.
           MOVE 0 TO IDX-4.
           PERFORM FILL-TYPE VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 12
           IF IDX-4 > 1
              MOVE 13 TO IDX-1
              PERFORM FILL-TYPE 
           END-IF.
           PERFORM FILL-DECOMPTE.

       COLONNES.
           COMPUTE COL-NUM = 6 + IDX * 8.
           MOVE "+" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       FILL-TYPE.
           COMPUTE IDX = LIN-NUM + 3.
           IF IDX >= 69
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
              PERFORM READ-FORM
              PERFORM ENTETE
              COMPUTE LIN-NUM = LIN-IDX - 1
           END-IF.
           IF H-M(IDX-1, 13) NOT = 0
              ADD 1 TO LIN-NUM IDX-4
              PERFORM P-LINE
              PERFORM FILL-HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 13
              ADD 1 TO LIN-NUM
              PERFORM COLONNES VARYING IDX FROM 1 BY 1 UNTIL IDX > 14
           END-IF.

       FILL-HEURES.
           IF H-M(IDX-1, IDX) NOT = 0
              COMPUTE COL-NUM = 8 + IDX * 8
              MOVE H-M(IDX-1, IDX) TO VH-00
              MOVE 1 TO DEC-NUM
              MOVE 3 TO CAR-NUM
              IF IDX = 13
                 MOVE 4 TO CAR-NUM
              END-IF
              PERFORM FILL-FORM
           END-IF.

       FILL-DECOMPTE.
           SUBTRACT 1 FROM LIN-NUM.
           MOVE   3 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE 119 TO COL-NUM.
           COMPUTE VH-00 = HR-TOT-MOIS(13) / CAR-HRS-JOUR + ,005.
           PERFORM FILL-FORM.
           MOVE   3 TO CAR-NUM.
           MOVE 125 TO COL-NUM.
           MOVE   2 TO DEC-NUM.
           IF HR-TOT-MOIS(13) > 0
              COMPUTE VH-00 = 100 / HR-THEO-MOIS(13) * HR-TOT-MOIS(13)
              + ,005
              PERFORM FILL-FORM
           END-IF.
           ADD 1 TO LIN-NUM.

       FILL-TOTAL.
           IF HT-TOT-MOIS(IDX) NOT = 0
              COMPUTE COL-NUM = 5 + IDX * 8 + (IDX / 13)
              MOVE HT-TOT-MOIS(IDX) TO VH-00
              MOVE 1 TO DEC-NUM
              MOVE 6 TO CAR-NUM
              PERFORM FILL-FORM
           END-IF.

       FILL-THEO.
           IF HT-THEO-MOIS(IDX) NOT = 0
              COMPUTE COL-NUM = 5 + IDX * 8 + (IDX / 13)
              MOVE HT-THEO-MOIS(IDX) TO VH-00
              MOVE 1 TO DEC-NUM
              MOVE 6 TO CAR-NUM
              PERFORM FILL-FORM
           END-IF.

       FILL-PCT.
           COMPUTE COL-NUM = 8 + IDX * 8
           IF IDX = 13
              MOVE 125 TO COL-NUM
           END-IF.
           MOVE 3 TO CAR-NUM
           MOVE 2 TO DEC-NUM.
           IF HT-TOT-MOIS(IDX) > 0
              COMPUTE VH-00 = 100 / HT-THEO-MOIS(IDX) * HT-TOT-MOIS(IDX)
              + ,005
              PERFORM FILL-FORM
              MOVE " %" TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.


       HRS-NORMALES.
           INITIALIZE L-RECORD.
           MOVE IDX-2 TO L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           ADD L-UNI-REGUL-LIM TO HR-THEO-MOIS(IDX-2) HR-THEO-MOIS(13)
                                  HT-THEO-MOIS(IDX-2) HT-THEO-MOIS(13).
           PERFORM SAVE-UNIT VARYING IDX FROM 1 BY 1 UNTIL IDX > 12.
           PERFORM TEST-FLAG VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           IF H-S(11) NOT = "N"
              MOVE 11 TO IDX
              PERFORM HRS-MAL
           END-IF.
           IF H-S(12) NOT = "N"
              MOVE 12 TO IDX
              PERFORM HRS-MAL
           END-IF.

       HRS-MAL.
           ADD SV-UNIT(IDX) TO H-M(IDX, IDX-2) H-M(IDX, 13) 
                               HR-TOT-MOIS(IDX-2) HR-TOT-MOIS(13)
                               HT-TOT-MOIS(IDX-2) HT-TOT-MOIS(13).

       SAVE-UNIT.
           MOVE L-UNIT(IDX) TO SV-UNIT(IDX).

       TEST-FLAG.
           IF SV-UNIT(IDX) > 0
              PERFORM TEST-FLAG-1.

       TEST-FLAG-1.
           MOVE IDX TO L-SUITE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           MOVE 0 TO IDX-1.
           IF L-SUITE < 10
              MOVE 1 TO IDX-1.

           IF L-FLAG-ACCIDENT NOT = 0
              MOVE 2 TO IDX-1
           END-IF.
           IF L-FLAG-HOPITAL NOT = 0
              MOVE 3 TO IDX-1
           END-IF.
           IF L-FLAG-FAMILLE NOT = 0
              MOVE 4 TO IDX-1
           END-IF.
           IF L-FLAG-AGREE NOT = 0
           AND H-S(5) NOT = "N"
              MOVE 5 TO IDX-1
           END-IF.
           IF L-FLAG-DISPENSE NOT = 0
              MOVE 6 TO IDX-1
           END-IF.
           IF L-FLAG-LONG-TERME NOT = 0
              MOVE 7 TO IDX-1
           END-IF.
           IF L-FLAG-AVANCE NOT = 0
              MOVE 8 TO IDX-1
           END-IF.
           IF  L-SUITE = 10
           AND L-FLAG-FAMILLE = 0
              MOVE 10 TO IDX-1
           END-IF.
           IF IDX-1 > 0
              IF H-S(IDX-1) NOT = "N"
                 ADD L-UNI-SALAIRE TO H-M(IDX-1, IDX-2) H-M(IDX-1, 13) 
                                      HR-TOT-MOIS(IDX-2) HR-TOT-MOIS(13)
                                      HT-TOT-MOIS(IDX-2) HT-TOT-MOIS(13)
              END-IF
           END-IF.

       P-LINE.
           MOVE IDX-1 TO LNK-NUM.
           MOVE "AM" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           MOVE 1 TO COL-NUM.
           PERFORM FILL-FORM.

       TRAITEMENT-RUN.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.
           PERFORM END-PROGRAM.

       ENTETE.
           MOVE  4 TO LIN-NUM.
           MOVE MOIS-DEBUT TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           MOVE 17 TO COL-NUM.
           PERFORM FILL-FORM
           IF MOIS-DEBUT NOT = MOIS-FIN
              ADD  2 TO COL-NUM 
              MOVE "-" TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE MOIS-FIN TO LNK-NUM
              PERFORM MOIS-NOM-1
              ADD 2 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.
           MOVE LNK-ANNEE TO VH-00
           MOVE  4 TO CAR-NUM
           ADD   2 TO COL-NUM.
           PERFORM FILL-FORM.
      * DONNEES FIRME

           MOVE  6 TO CAR-NUM.
           MOVE 120 TO COL-NUM.
           ADD 1 TO PAGE-NUMBER.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 75 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 80 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 5.
           MOVE 75 TO COL-NUM.
           MOVE FR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 80 TO COL-NUM.
           MOVE FR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 6.
           MOVE 75 TO COL-NUM.
           MOVE FR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  6 TO CAR-NUM.
           MOVE FR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

      *    DATE EDITION
           MOVE 20 TO COL-NUM.
           MOVE TODAY-JOUR TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 23 TO COL-NUM.
           MOVE TODAY-MOIS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 26 TO COL-NUM.
           MOVE TODAY-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE DIV-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 11 POSITION 25.
           DISPLAY DIV-NOM LINE 11 POSITION 35 SIZE 45.
       DIS-HE-07.
           DISPLAY ABSENTEISME LINE 13 POSITION 32.
       DIS-HE-08.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 19 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-09.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 1366 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE 11 TO LIN-IDX.
           MOVE 0 TO COMPTEUR.
           PERFORM AFFICHE-D THRU AFFICHE-END.

       AFFICHE-D.
           MOVE COMPTEUR TO LNK-NUM.
           ADD 1 TO LIN-IDX LNK-NUM.
           MOVE "AM" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           IF LNK-TEXT = SPACES
              GO AFFICHE-END.
           ADD 1 TO COMPTEUR.
           IF H-S(COMPTEUR) NOT = "N"
              DISPLAY LNK-TEXT LINE LIN-IDX POSITION 53 SIZE 18 REVERSE
           ELSE
              DISPLAY LNK-TEXT LINE LIN-IDX POSITION 55 SIZE 18 LOW.
           GO AFFICHE-D.
       AFFICHE-END.
           EXIT.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF ASCII-FILE = 0
              IF COUNTER > 0 
                 PERFORM TOTAUX
                 PERFORM TRANSMET
              END-IF
              IF COUNTER > 0
                 MOVE 99 TO LNK-VAL
                 CALL "P130" USING LINK-V FORMULAIRE
                 CANCEL "P130"
              END-IF
           END-IF.
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDIV.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".


       WRITE-TEXT.
           IF NOT-OPEN = 0
              IF AJOUTE = 1
                 OPEN EXTEND TF-TRANS
              ELSE
                 OPEN OUTPUT TF-TRANS
              END-IF
              MOVE 1 TO NOT-OPEN
              IF TABLEAU = 1
              AND AJOUTE = 0
                  PERFORM HEAD-LINE
                  WRITE TF-RECORD FROM Txt-RECORD
              END-IF
      *       INITIALIZE TXT-RECORD
           END-IF.
           PERFORM ASCII VARYING IDX FROM 1 BY 1 UNTIL IDX > 12.

       ASCII.
           IF H-M(IDX, 13) NOT = 0
              PERFORM T-LINE
           END-IF.

       T-LINE.
           MOVE IDX TO LNK-NUM T1-TYPE.
           MOVE "AM" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO T1-TEXT.
           MOVE LNK-ANNEE TO T1-ANNEE.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO T1-NOM.
           MOVE CAR-COUT TO T1-COUT.
           MOVE FR-KEY TO T1-FIRME.
           MOVE REG-PERSON TO T1-PERSON.

           PERFORM LIGNE VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 13.
           COMPUTE SH-00 = H-M(IDX, 13) / CAR-HRS-JOUR.
           MOVE SH-00 TO T1-MOIS(14).
           MOVE ";" TO T1-DEL(14).
           WRITE TF-RECORD FROM T1-RECORD.

       LIGNE.
           MOVE H-M(IDX, IDX-1) TO T1-MOIS(IDX-1).
           MOVE ";" TO T1-DEL(IDX-1).

       HEAD-LINE.
           PERFORM MOIS VARYING IDX  FROM 1 BY 1 UNTIL IDX > 13.
           MOVE "AA" TO LNK-AREA.
           MOVE 117 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-ANNEE.
           MOVE "AY" TO LNK-AREA.
           MOVE  5  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-PERSON.
           MOVE "FI" TO LNK-AREA.
           MOVE  1  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-FIRME.
           MOVE "PR" TO LNK-AREA.
           MOVE  3  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-NOM.
           MOVE "FR" TO LNK-AREA.
           MOVE  25 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-COUT.
           MOVE "AA" TO LNK-AREA.
           MOVE 122 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-JOURS.
           MOVE "AA" TO LNK-AREA.
           MOVE 50  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-TYPE.
           MOVE "AA" TO LNK-AREA.
           MOVE 49  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-TEXT.

       MOIS.
           MOVE IDX TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-MOIS(IDX).
           MOVE ";" TO TXT-DEL(IDX).
