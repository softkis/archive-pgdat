      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-CSMUL IMPRESSION CONTROLE CODES SALAIRE   �
      *  � RECAPITULATION PERIODIQUE - TABLEAU                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-CSMUL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODPAIE.FC".
           SELECT OPTIONAL TF-CSM ASSIGN TO DISK PARMOD-PATH5,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CODPAIE.FDE".
       FD  TF-CSM
           RECORD CONTAINS 161 CHARACTERS
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TF-FIRME      PIC Z(6).
           02 TF-FIRME-T REDEFINES TF-FIRME PIC X(6).
           02 TF-FILLER-1   PIC X.
           02 TF-ANNEE      PIC Z(5).
           02 TF-ANNEE-T REDEFINES TF-ANNEE PIC X(5).
           02 TF-FILLER-2   PIC X.
           02 TF-MOIS       PIC Z(5).
           02 TF-MOIS-T REDEFINES TF-MOIS PIC X(5).
           02 TF-FILLER-3   PIC X.
           02 TF-PERSON     PIC Z(8).
           02 TF-PERS-T REDEFINES TF-PERSON PIC X(8).
           02 TF-FILLER-4   PIC X.
           02 TF-NOM        PIC X(30).
           02 TF-FILLER-5   PIC X.
           02 TF-COUT       PIC Z(8).
           02 TF-COUT-T REDEFINES TF-COUT PIC X(8).
           02 TF-FILLER-6   PIC X.
           02 TF-CODE       PIC Z(8).
           02 TF-CODE-T REDEFINES TF-CODE PIC X(8).
           02 TF-FILLER-7   PIC X.
           02 TF-DONNEES OCCURS 6.
              04 TF-VAL    PIC Z(8),ZZ.
              04 TF-TXT REDEFINES TF-VAL PIC X(11).
              04 TF-FILL   PIC X.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 11.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "CODSAL.REC".
           COPY "PARMOD.REC".

       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.

           COPY "V-VAR.CPY".
        
       01  NOT-OPEN              PIC 9  VALUE 0.
       01  LAST-CS               PIC 99 VALUE 0.
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  VALEUR                PIC X.

       01  PAR-RECORD.
           03 PARAM-VAL PIC 9999 OCCURS 60.
       01  PAR-RECORD-R REDEFINES PAR-RECORD.
           02 PAR-DEF OCCURS 3.
              03 PARAM-CS PIC 9999 OCCURS 20.

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CODPAIE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-CSMUL.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-TX3 TO PAR-RECORD.

           MOVE LNK-SUFFIX TO ANNEE-PAIE.
           OPEN INPUT CODPAIE.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8 THRU 9
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 11    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-PATH
           WHEN 11 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  7 PERFORM APRES-7 
           WHEN  8 PERFORM APRES-8 
           WHEN  9 PERFORM APRES-9 
           WHEN 11 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-7.
           ACCEPT VALEUR
             LINE 15 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
                     PERFORM APRES-8 THRU APRES-9
             WHEN 66 ADD      1 TO   MOIS-DEBUT
                     PERFORM APRES-8 THRU APRES-9
             END-EVALUATE.

       AVANT-9.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
                     PERFORM APRES-8 THRU APRES-9
             WHEN 66 ADD      1 TO   MOIS-FIN
                     PERFORM APRES-8 THRU APRES-9
             END-EVALUATE.

       AVANT-PATH.
           IF PARMOD-PATH5 = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH5
           END-IF.
           ACCEPT PARMOD-PATH5
             LINE 22 POSITION 20 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-7.
           IF VALEUR = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE "N" TO VALEUR
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-07.
           MOVE VALEUR TO PARMOD-SETTINGS.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       APRES-8.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-08.
           PERFORM DIS-HE-09.

       APRES-9.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.
           PERFORM DIS-HE-09.

       READ-CS.
           MOVE CSP-CODE TO CS-NUMBER.
           CALL "6-CODSAL" USING LINK-V CS-RECORD FAKE-KEY.
           IF LNK-LANGUAGE NOT = "F"
              CALL "6-CODTXT" USING LINK-V CS-RECORD.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM START-PERSON
                  PERFORM END-PROGRAM
           WHEN 68 CALL "4-CSTAB" USING LINK-V PAR-RECORD
                  MOVE PAR-RECORD TO PARMOD-TX3
                  CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W"
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
                  PERFORM DISPLAY-F-KEYS
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       START-PERSON.
           PERFORM CHECK-TAB VARYING IDX FROM 1 BY 1 UNTIL IDX > 60.
           IF LAST-CS = 0
              PERFORM END-PROGRAM
           END-IF.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           PERFORM READ-PERSON THRU READ-EXIT.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           PERFORM RECHERCHE-CSP.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.


       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.


       RECHERCHE-CSP.
           PERFORM DIS-HE-01.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY       TO CSP-FIRME.
           MOVE REG-PERSON   TO CSP-PERSON.
           MOVE MOIS-DEBUT   TO CSP-MOIS.
           START CODPAIE KEY >= CSP-KEY INVALID CONTINUE
               NOT INVALID PERFORM READ-CSP THRU READ-CSP-END.

       READ-CSP.
           READ CODPAIE NEXT AT END
               GO READ-CSP-END
           END-READ.
           IF FR-KEY       NOT = CSP-FIRME
           OR REG-PERSON   NOT = CSP-PERSON
           OR MOIS-FIN     <     CSP-MOIS 
              GO READ-CSP-END
           END-IF.
           IF CSP-SUITE NOT = 0
              GO READ-CSP
           END-IF.
           MOVE 0 TO IDX.
           PERFORM TEST-TAB THRU TEST-TAB-END.
           IF PARAM-VAL(IDX) NOT = CSP-CODE
              GO READ-CSP
           END-IF.

           IF NOT-OPEN = 0 
              OPEN OUTPUT TF-CSM
              INITIALIZE TF-RECORD
              INSPECT TF-RECORD REPLACING ALL " " BY ";"
              EVALUATE LNK-LANGUAGE
                 WHEN "F" PERFORM ENTETE-F
                 WHEN "D" PERFORM ENTETE-D
                 WHEN "E" PERFORM ENTETE-E
              END-EVALUATE
              WRITE TF-RECORD
              ADD 1 TO NOT-OPEN
           END-IF.
           IF CSP-CODE NOT = CS-NUMBER
               PERFORM READ-CS.
           MOVE FR-KEY     TO TF-FIRME.
           MOVE LNK-ANNEE  TO TF-ANNEE.
           MOVE CSP-MOIS   TO TF-MOIS.
           MOVE REG-PERSON TO TF-PERSON.
           MOVE PR-NOM     TO TF-NOM.
           MOVE CAR-COUT   TO TF-COUT.
           MOVE CSP-CODE   TO TF-CODE.
           IF VALEUR = "N"
              MOVE 0 TO CSP-UNITAIRE CSP-TOTAL CSP-DONNEE-2
           END-IF.
           PERFORM FILL-CS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.
           MOVE ";" TO TF-FILLER-1 
                       TF-FILLER-2
                       TF-FILLER-3
                       TF-FILLER-4
                       TF-FILLER-5
                       TF-FILLER-6
                       TF-FILLER-7.
           WRITE TF-RECORD.
           GO READ-CSP.
       READ-CSP-END.
           EXIT.

       FILL-CS.
           MOVE CSP-DONNEE(IDX) TO TF-VAL(IDX).
           MOVE ";" TO TF-FILL(IDX).

       FILL-DESCR.
           MOVE CS-DESCRIPTION(IDX) TO TF-TXT(IDX).
           MOVE ";" TO TF-FILL(IDX).


       CHECK-TAB.
           IF PARAM-VAL(IDX) NOT = 0
              MOVE IDX TO LAST-CS
           END-IF.

       TEST-TAB.
           ADD 1 TO IDX.
           IF PARAM-VAL(IDX) = CSP-CODE
              GO TEST-TAB-END
           END-IF.
           IF IDX < LAST-CS
              GO TEST-TAB.
       TEST-TAB-END.
           EXIT.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           MOVE 27 TO COL-IDX.
           IF A-N = "A"
              ADD 11 TO COL-IDX
              DISPLAY SPACES LINE 6 POSITION 37 SIZE 1.
           COMPUTE IDX = 70 - COL-IDX.
           MOVE 0 TO SIZ-IDX IDX-1.
           INSPECT PR-NOM TALLYING  SIZ-IDX FOR CHARACTERS BEFORE "  ".
           INSPECT PR-NOM-JF TALLYING IDX-1 FOR CHARACTERS BEFORE "  ".

           IF FR-NOM-JF = SPACES
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX  = COL-IDX + SIZ-IDX + 1
              IF PR-NOM-JF NOT = SPACES
                 COMPUTE IDX = 70 - COL-IDX
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
           ELSE
              IF PR-NOM-JF NOT = SPACES
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
              COMPUTE IDX = 70 - COL-IDX
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX = COL-IDX + SIZ-IDX + 1
           END-IF.
           IF COL-IDX < 70
              COMPUTE IDX = 70 - COL-IDX
              DISPLAY PR-PRENOM LINE 6 POSITION COL-IDX SIZE IDX LOW.
       DIS-HE-STAT.
           DISPLAY STATUT    LINE 9 POSITION 32.
           DISPLAY STAT-NOM  LINE 9 POSITION 35.
       DIS-HE-05.
           MOVE COUT-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 10 POSITION 25.
           DISPLAY COUT-NOM LINE 10 POSITION 35 SIZE 30.
       DIS-HE-07.
           DISPLAY VALEUR LINE 15 POSITION 32.
       DIS-HE-08.
           DISPLAY MOIS-DEBUT LINE 19 POSITION 31.
           MOVE MOIS-DEBUT TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-09.
           DISPLAY MOIS-FIN LINE 20 POSITION 31.
           MOVE MOIS-FIN TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-10.
           DISPLAY PARMOD-PATH5 LINE 22 POSITION 20 SIZE 40.
       DIS-HE-END.
           EXIT.

       ENTETE-F.
           MOVE "FIRME"     TO TF-FIRME-T.
           MOVE "ANNEE"     TO TF-ANNEE-T.
           MOVE "MOIS"      TO TF-MOIS-T.
           MOVE "PERSONNE"  TO TF-PERS-T.
           MOVE "NOM"       TO TF-NOM.
           MOVE "COUT"      TO TF-COUT-T.
           MOVE "CODE"      TO TF-CODE-T.
           MOVE "DONNEE 1"  TO TF-TXT(1).
           MOVE "DONNEE 2"  TO TF-TXT(2).
           MOVE "DONNEE 3"  TO TF-TXT(3).
           MOVE "DONNEE 4"  TO TF-TXT(4).
           MOVE "DONNEE 5"  TO TF-TXT(5).
           MOVE "DONNEE 6"  TO TF-TXT(6).

       ENTETE-D.
           MOVE "FIRMA"     TO TF-FIRME-T.
           MOVE "JAHR"      TO TF-ANNEE-T.
           MOVE "MONAT"     TO TF-MOIS-T.
           MOVE "PERSON"    TO TF-PERS-T.
           MOVE "NAME"      TO TF-NOM.
           MOVE "KOSTENST"  TO TF-COUT-T.
           MOVE "L-ART"     TO TF-CODE-T.
           MOVE "FELD 1"    TO TF-TXT(1).
           MOVE "FELD 2"    TO TF-TXT(2).
           MOVE "FELD 3"    TO TF-TXT(3).
           MOVE "FELD 4"    TO TF-TXT(4).
           MOVE "FELD 5"    TO TF-TXT(5).
           MOVE "FELD 6"    TO TF-TXT(6).
       ENTETE-E.
           MOVE "FIRM "     TO TF-FIRME-T.
           MOVE "YEAR"      TO TF-ANNEE-T.
           MOVE "MONTH"     TO TF-MOIS-T.
           MOVE "PERSON"    TO TF-PERS-T.
           MOVE "NAME"      TO TF-NOM.
           MOVE "COSTCENT"  TO TF-COUT-T.
           MOVE "CODE "     TO TF-CODE-T.
           MOVE "FIELD 1"   TO TF-TXT(1).
           MOVE "FIELD 2"   TO TF-TXT(2).
           MOVE "FIELD 3"   TO TF-TXT(3).
           MOVE "FIELD 4"   TO TF-TXT(4).
           MOVE "FIELD 5"   TO TF-TXT(5).
           MOVE "FIELD 6"   TO TF-TXT(6).

       AFFICHAGE-ECRAN.
           MOVE 604 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           DISPLAY SPACES LINE 13 POSITION 1 SIZE 80.
           PERFORM AFFICHAGE-CS.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       AFFICHAGE-CS.
           PERFORM DIS-VAR VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > 3.

       DIS-VAR.
           PERFORM DIS-DETAIL VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.

       DIS-DETAIL.
           COMPUTE LIN-IDX = IDX + 2.
           COMPUTE COL-IDX = IDX-4 * 4 + 65.
           MOVE PARAM-CS(IDX-4, IDX) TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION COL-IDX.

       END-PROGRAM.
           CANCEL "4-CSTAB".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

