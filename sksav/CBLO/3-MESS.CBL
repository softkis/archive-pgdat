      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-MESS IMPRESSION MESSAGES D'ERREURS        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-MESS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MESSAGE.FC".
           COPY "IMPRLOG.FC".
           COPY "FORM80.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.

           COPY "MESSAGE.FDE".
           COPY "IMPRLOG.FDE".
           COPY "FORM80.FDE".
       FD  TF-TRANS
           RECORD VARYING DEPENDING ON IDX-COL
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 130 DEPENDING IDX-COL.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  IDX-COL               PIC 999 VALUE 80.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  AREA-CODE             PIC XXXX VALUE SPACES.
       01  ASCII-ANSI            PIC 9 VALUE 0.
       01  ASCII-FILE            PIC 9 VALUE 0.
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  TXT-RECORD            PIC X(150).

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".
           COPY "PARMOD.REC".

       01  FORM-NAME            PIC X(9) VALUE "FORMF.MES".

       01   ECR-DISPLAY.
            02 HE-Z2 PIC Z(2).
            02 HE-Z4 PIC Z(4).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               IMPLOG 
               TF-TRANS
               MESSAGES 
               FORM.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-MESS.
       
           OPEN INPUT MESSAGES.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

      *    INDEX LANGUE 1 = DEFAUT
           MOVE "FORMF.MES" TO  FORM-NAME.
           EVALUATE LNK-LANGUAGE
                WHEN "D" MOVE "FORMD.MES" TO  FORM-NAME
                WHEN "E" MOVE "FORME.MES" TO  FORM-NAME
           END-EVALUATE.

           CALL "0-TODAY" USING TODAY.

           MOVE 0 TO CAR-NUM DEC-NUM LIN-NUM LIN-IDX.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions

           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).
      
           EVALUATE INDICE-ZONE
               WHEN 2 IF ASCII-ANSI = 0
                         MOVE 9600000025 TO EXC-KFR(1)
                      ELSE
                         MOVE 9700000025 TO EXC-KFR(1)
                      END-IF
                      MOVE 1700000078 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
               WHEN  2 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       AVANT-1.
           ACCEPT AREA-CODE
             LINE 10 POSITION 33 SIZE 3
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PATH.
           MOVE "AA" TO LNK-AREA.
           MOVE 32 TO LNK-NUM.
           MOVE "L" TO LNK-LOW.
           PERFORM DISPLAY-MESSAGE.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 24 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       APRES-DEC.
           IF EXC-KEY = 1
              IF ASCII-ANSI = 0
                 MOVE 1 TO ASCII-ANSI
              ELSE
                 MOVE 0 TO ASCII-ANSI
              END-IF
           END-IF.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-MESSAGE
                    PERFORM READ-MESSAGE 
           WHEN 10 MOVE 1 TO ASCII-FILE
                   PERFORM AVANT-PATH
                   PERFORM START-MESSAGE
                   PERFORM READ-MESS-ASCII
                   PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       START-MESSAGE.
           INITIALIZE MS-RECORD.
           MOVE AREA-CODE    TO MS-AREA.
           MOVE LNK-LANGUAGE TO MS-LANGUAGE.
           START MESSAGES KEY > MS-KEY 
              INVALID PERFORM END-PROGRAM.

       READ-MESSAGE.
           READ MESSAGES NEXT AT END 
                PERFORM END-PROGRAM
           END-READ.
           IF  AREA-CODE > SPACES
           AND AREA-CODE NOT = MS-AREA
              PERFORM END-PROGRAM
           END-IF.
           IF LNK-LANGUAGE NOT = MS-LANGUAGE
              PERFORM END-PROGRAM
           END-IF.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM > 65
              PERFORM TRANSMET
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           ADD 1 TO LIN-NUM.
           PERFORM FILL-TEXTE.
           PERFORM DIS-HE-01.
           GO READ-MESSAGE.

       DIS-HE-01.
           DISPLAY MS-AREA LINE 8 POSITION 10.
           DISPLAY MS-NUMBER LINE 8 POSITION 14.
           DISPLAY MS-DESCRIPTION  LINE 8 POSITION 20.
       DIS-HE-END.
           EXIT.

       FILL-TEXTE.
           MOVE MS-AREA TO ALPHA-TEXTE.
           MOVE  5 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 4 TO CAR-NUM.
           MOVE MS-NUMBER TO VH-00.
           MOVE 13 TO COL-NUM.
           PERFORM FILL-FORM.
           IF ASCII-ANSI = 1
              INSPECT MS-DESCRIPTION CONVERTING
              "굤닀뀑뙏뱚뼏꽇쉸릥�" TO "郵幽攝專抄化斡寶�譽"
           END-IF.
           MOVE MS-DESCRIPTION TO ALPHA-TEXTE.
           MOVE 20 TO COL-NUM.
           PERFORM FILL-FORM.
               
       WRITE-PAGE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  0 TO DEC-NUM.
           MOVE  4 TO LIN-NUM CAR-NUM.
           MOVE 73 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  6 TO LIN-NUM.
           MOVE 67 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 70 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 73 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       AFFICHAGE-ECRAN.
           MOVE 1003 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           PERFORM WRITE-PAGE.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           CLOSE MESSAGES.
           MOVE " " TO LNK-YN.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".


       READ-MESS-ASCII.
           INITIALIZE TXT-RECORD.
           READ MESSAGES NEXT AT END 
                PERFORM END-PROGRAM
           END-READ.
           IF  AREA-CODE > SPACES
           AND AREA-CODE NOT = MS-AREA
              PERFORM END-PROGRAM
           END-IF.
           IF LNK-LANGUAGE NOT = MS-LANGUAGE
              PERFORM END-PROGRAM
           END-IF.
           MOVE 1 TO IDX-4.
           PERFORM FILL-TEXTE-A.
           PERFORM WRITE-TEXT.
           PERFORM DIS-HE-01.
           GO READ-MESS-ASCII.

       WRITE-TEXT.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-TRANS
              MOVE 1 TO NOT-OPEN 
           END-IF.
           WRITE TF-RECORD FROM TXT-RECORD.
           INITIALIZE TXT-RECORD.


       FILL-TEXTE-A.
           MOVE 1 TO IDX-4.
           STRING MS-AREA  DELIMITED BY SIZE INTO TXT-RECORD 
           POINTER IDX-4 ON OVERFLOW CONTINUE END-STRING.

           MOVE 5 TO IDX-4.
           MOVE MS-NUMBER TO HE-Z4.
           STRING HE-Z4 DELIMITED BY SIZE INTO TXT-RECORD 
           POINTER IDX-4 ON OVERFLOW CONTINUE END-STRING.

           MOVE 10 TO IDX-4.
           IF ASCII-ANSI = 1
              INSPECT MS-DESCRIPTION CONVERTING
              "굤닀뀑뙏뱚뼏꽇쉸릥�" TO "郵幽攝專抄化斡寶�譽"
           END-IF.
           STRING MS-DESCRIPTION DELIMITED BY SIZE INTO TXT-RECORD 
           POINTER IDX-4 ON OVERFLOW CONTINUE END-STRING.
