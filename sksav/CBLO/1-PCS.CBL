      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-PCS CODES SALAIRES PAIES                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-PCS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODPAIE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CODPAIE.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 6.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "TXACCID.REC".
           COPY "LIVRE.REC".
           COPY "LIVRE.CUM".
           COPY "V-BASES.REC".
           COPY "CCOL.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "FICHE.REC".
           COPY "CODSAL.REC".
           COPY "PRESENCE.REC".
           COPY "POCL.REC".
           COPY "PAREX.REC".
           COPY "HORSEM.REC".
           COPY "POSE.REC".
       01  LSAVE-RECORD.
           02 LSAVE-REC-DETAIL OCCURS 5.
              03 LSAVE-DEBIT-CREDIT.
                 04 LSAVE-DC     PIC 9(8)V99 OCCURS 300.
              03 LSAVE-UNITES.
                 04 LSAVE-UNITE  PIC 9(6)V99 OCCURS 200.
              03 LSAVE-JOURS.
                 04 LSAVE-JRS PIC 9(6) OCCURS 70.
              03 LSAVE-COTISABLES.
                 04 LSAVE-COT    PIC 9(8)V99 OCCURS 70.
              03 LSAVE-ABATTEMENTS.
                 04 LSAVE-ABAT   PIC 9(6)V99 OCCURS 10.
              03 LSAVE-IMPOSABLES.
                 04 LSAVE-IMPOS  PIC 9(8)V99 OCCURS 40.
              03 LSAVE-SNOCS-DET.
                 04 LSAVE-SNOCS  PIC 9(8)V99 OCCURS 10.
              03 LSAVE-MOYENNE.
                 04 LSAVE-MOY    PIC 9(8)V99 OCCURS 10.
              03 LSAVE-FLAGS.
                 04 LSAVE-FLAG   PIC 99 OCCURS 10.


       01  COMPTEUR              PIC 99 COMP-1.
       01  MODIFICATION          PIC 9 VALUE 0.
       01  MOIS-EN-COURS.
           02 JOUR-DEB     PIC 99.
           02 JOUR-FIN     PIC 99.
       01  LAST-FIRME            PIC 9(6) VALUE 0.
       01  LAST-MOIS             PIC 9(2) VALUE 0.
       01  TEST-STOP             PIC 9(2) VALUE 0.

       01  HRS-MOIS              PIC 999V99.
       01  SAL-ARR               PIC 9(8)V99.
       01  SAL-TOT               PIC 9(8)V9(4).
       01  SAL-ACT               PIC S9(8)V9(4).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02 SAL-ACT-A          PIC S9(8).
           02 SAL-ACT-B          PIC 9(4).
       01  IND-TEXTE             PIC X(25).

       01  VAL-HLP-08.
           02 VH-08          PIC 9(8)V9999 OCCURS 8.

       01  H-Z1.
           03 FILLER         PIC Z(7).
           03 ZZ-01          PIC Z,ZZZZ.
       01  H-Z2 REDEFINES H-Z1.
           03 FILLER         PIC Z(6).
           03 ZZ-02          PIC ZZ,ZZZZ.
       01  H-Z3 REDEFINES H-Z1.
           03 FILLER         PIC Z(5).
           03 ZZ-03          PIC ZZZ,ZZZZ.
       01  H-Z4 REDEFINES H-Z1.
           03 FILLER         PIC Z(4).
           03 ZZ-04          PIC Z(4),ZZZZ.
       01  H-Z5 REDEFINES H-Z1.
           03 FILLER         PIC Z(3).
           03 ZZ-05          PIC Z(5),ZZZZ.
       01  H-Z6 REDEFINES H-Z1.
           03 FILLER         PIC ZZ.
           03 ZZ-06          PIC Z(6),ZZZZ.
       01  H-Z7 REDEFINES H-Z1.
           03 FILLER         PIC Z.
           03 ZZ-07          PIC Z(7),ZZZZ.
       01  H-Z8 REDEFINES H-Z1.
           03 ZZ-08          PIC Z(8),ZZZZ.

       01  HELP-VAL              PIC Z BLANK WHEN ZERO.
       01  ESSAIS                PIC 9999.
       01  HELP-2                PIC 9.
       01  HELP-3                PIC S9(6)V99 COMP-3.
       01  HAUT                  PIC 9(8)v99.
       01  BAS                   PIC 9(8)v99.
       01  MEMOIRE               PIC 9(8)v99.
       01  NB-NET                PIC 9(8)v99.

       01  LIVRE-NAME.
           02 FILLER             PIC X(8) VALUE "S-LIVRE.".
           02 ANNEE-LIVRE        PIC 999.
       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.
  
       01   ECR-DISPLAY.
            02 HE-Z2 PIC ZZ  BLANK WHEN ZERO.
            02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4).
            02 HE-Z6 PIC Z(6).
            02 HE-Z8 PIC Z(8)-.
            02 HE-Z2Z2 PIC ZZ,ZZ. 
            02 HE-Z4Z2 PIC -ZZZZ,ZZ BLANK WHEN ZERO. 
            02 HE-Z6Z2 PIC Z(6),ZZ. 
            02 HE-Z6Z4 PIC Z(6),Z(4). 
            02 HE-Z8Z3 PIC Z(8),ZZZ BLANK WHEN ZERO.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CODPAIE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-PCS .
       
           MOVE LNK-SUFFIX TO ANNEE-PAIE.
           INITIALIZE CSP-RECORD PC-RECORD.
           CALL "6-PARAM" USING LINK-V PAREX-RECORD.
           CANCEL "4-AUTO". 
           CANCEL "4-CALCUL". 
           CANCEL "6-CODPAI". 

           OPEN I-O   CODPAIE.

           PERFORM AFFICHAGE-ECRAN .
           INITIALIZE TXA-RECORD.
           CALL "6-ASSACC" USING LINK-V TXA-RECORD.
           IF TXA-VALUE(1) = 0
              MOVE "SL" TO LNK-AREA
              MOVE 0 TO LNK-POSITION
              MOVE 61 TO LNK-NUM
              CALL "0-DMESS" USING LINK-V
              ACCEPT ACTION
           END-IF.
           MOVE 0 TO MODIFICATION.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 3  MOVE 0064230000 TO EXC-KFR(1)
                      MOVE 4129000000 TO EXC-KFR(2)
                      MOVE 0000130000 TO EXC-KFR(3)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 4  MOVE 0163640015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 5  MOVE 0100000050 TO EXC-KFR(1)
                      MOVE 5100080000 TO EXC-KFR(2)
              WHEN 6  MOVE 0100000050 TO EXC-KFR(1)
                      MOVE 5100080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
           END-EVALUATE.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
              LINE 3 POSITION 15 SIZE 6
              TAB UPDATE NO BEEP CURSOR 1
              ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 PERFORM CHANGE-MOIS
             MOVE 99 TO EXC-KEY
             END-EVALUATE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.            
           ACCEPT CS-NUMBER
             LINE  4 POSITION 24 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 PERFORM CHANGE-MOIS
             MOVE 99 TO EXC-KEY
           END-EVALUATE.

       AVANT-4.
           IF  CCOL-OCC-NULL = "N"
           AND CS-FRAIS NOT  = "N"
           AND CS-FRAIS NOT  = " "
              ACCEPT PC-NUMBER
                LINE  6 POSITION 20 SIZE 8
                TAB UPDATE NO BEEP CURSOR  1
                ON EXCEPTION  EXC-KEY  CONTINUE
            ELSE
               MOVE 0 TO PC-NUMBER
            END-IF.


       AVANT-5.
           COMPUTE LIN-IDX = 13.
           COMPUTE COL-IDX = 24 - CS-COLONNES(6).
           COMPUTE SIZ-IDX = CS-DECIMALES(6) + CS-COLONNES(6).
           IF CS-DECIMALES(6) > 0 ADD 1 TO SIZ-IDX.
           IF CS-SAISIE(6) > 0 
              PERFORM AVANT-ALL-1
           END-IF.
             
       AVANT-ALL-1.
           MOVE VH-08(6) TO ZZ-08.
           EVALUATE CS-COLONNES(6)
           WHEN 1 ACCEPT ZZ-01
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 2 ACCEPT ZZ-02
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           WHEN 3 ACCEPT ZZ-03
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 4 ACCEPT ZZ-04
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           WHEN 5 ACCEPT ZZ-05
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 6 ACCEPT ZZ-06
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           WHEN 7 ACCEPT ZZ-07
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 8 ACCEPT ZZ-08
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           END-EVALUATE.
           INSPECT ZZ-08 REPLACING ALL "." BY ",".
           MOVE ZZ-08 TO VH-08(6).

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN 5 CALL "1-REGIST" USING LINK-V
                  IF LNK-VAL > 0
                     MOVE LNK-VAL TO REG-PERSON
                     PERFORM NEXT-REGIS
                  END-IF
                  CANCEL "1-REGIST"  
                  PERFORM AFFICHAGE-ECRAN 
           WHEN 27 MOVE 0 TO LNK-PERSON
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              IF PRES-TOT(LNK-MOIS) = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           IF LNK-COMPETENCE < REG-COMPETENCE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR = 0
              PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN 4 MOVE REG-PERSON TO LNK-NUM
                  CALL "1-CARRI" USING LINK-V
                  CANCEL "1-CARRI"
                  CANCEL "1-GCARR"
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM DIS-HE-01 THRU DIS-HE-04
                  MOVE 1 TO INPUT-ERROR
           WHEN 5 MOVE REG-PERSON TO LNK-NUM
                  CALL "1-FICHE" USING LINK-V
                  CANCEL "1-FICHE"
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM DIS-HE-01 THRU DIS-HE-03
                  MOVE 1 TO INPUT-ERROR
           WHEN 27 MOVE 0 TO LNK-PERSON
           WHEN  13 CONTINUE
           WHEN  OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF  REG-PERSON NOT = 0
           AND INPUT-ERROR = 0
           AND EXC-KEY = 13
              PERFORM DONNEES-PERSONNE
              IF CAR-IN-ACTIF = 1 
                 MOVE "SL" TO LNK-AREA
                 MOVE 6 TO LNK-NUM
                 PERFORM DISPLAY-MESSAGE
                 MOVE 1 TO INPUT-ERROR
              END-IF 
           ELSE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF LNK-COMPETENCE < REG-COMPETENCE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-01.
           MOVE 0 TO LNK-SUITE.

       APRES-3.
           MOVE 0 TO INPUT-ERROR.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 1 MOVE 01001001 TO LNK-VAL
                  PERFORM HELP-SCREEN
                  MOVE 1 TO INPUT-ERROR
           WHEN 2 CALL "2-CODSAL" USING LINK-V CS-RECORD
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR
                  CANCEL "2-CODSAL" 
           WHEN 3 CALL "2-CS" USING LINK-V CS-RECORD
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR
                  CANCEL "2-CS" 
           WHEN OTHER PERFORM NEXT-CS 
           END-EVALUATE.
           IF CS-NOM = SPACES
              MOVE 1 TO INPUT-ERROR
           ELSE       
              PERFORM TEST-CS
           END-IF.              
           IF INPUT-ERROR = 0 
              PERFORM LOAD-CSP
           ELSE
              PERFORM CS-MESSAGE.
      *    PERFORM CS-TEXT THRU CS-TEXT-END.

       APRES-4.
           IF CCOL-OCC-NULL = "N"
           AND CS-FRAIS NOT  = "N"
           AND CS-FRAIS NOT  = " "
              PERFORM APRES-PF
           ELSE
              INITIALIZE PC-RECORD
           END-IF.

       APRES-PF.
           MOVE "N" TO A-N LNK-A-N.
           IF EXC-KEY = 3
              MOVE "A" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3 CALL "2-POCL" USING LINK-V PC-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   CANCEL "2-POCL"
           WHEN  7 MOVE REG-PERSON TO LNK-PERSON
                   CALL "2-JOURS" USING LINK-V
                   CANCEL "2-JOURS"
                   MOVE 0 TO LNK-PERSON
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-HE-01 THRU DIS-HE-END
                   PERFORM DISPLAY-F-KEYS
           WHEN 10 MOVE REG-PERSON TO LNK-PERSON
                   CALL "2-HEURES" USING LINK-V
                   CANCEL "2-HEURES"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN 52 CONTINUE
           WHEN 68 MOVE 4 TO EXC-KEY
                   GO APRES-4
           WHEN OTHER MOVE EXC-KEY TO SAVE-KEY
                   PERFORM NEXT-POCL
           END-EVALUATE.
           IF  EXC-KEY NOT = 67
           AND EXC-KEY NOT = 52
              IF PC-NUMBER = 0
      *          MOVE 1 TO INPUT-ERROR
                 INITIALIZE PC-RECORD
              END-IF
           END-IF.
           PERFORM DIS-HE-04.

       DONNEES-PERSONNE.
           INITIALIZE CSP-RECORD.
           MOVE 0 TO JOUR-DEB JOUR-FIN.
           INITIALIZE CAR-RECORD FICHE-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           IF CAR-STATUT = 0
              MOVE "SL" TO LNK-AREA
              MOVE 0 TO LNK-POSITION
              MOVE 36 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              ACCEPT ACTION
              IF LNK-COMPETENCE NOT = 0
              AND LNK-COMPETENCE >= REG-COMPETENCE
              AND REG-PERSON > 0
                 MOVE REG-PERSON TO LNK-PERSON
                 CALL "1-CARRI" USING LINK-V
                 CANCEL "1-CARRI"
                 PERFORM AFFICHAGE-ECRAN 
                 PERFORM AFFICHAGE-DETAIL
                 GO DONNEES-PERSONNE
              END-IF
           END-IF.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD SAVE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.

           CALL "4-LPCUM" USING LINK-V 
                                REG-RECORD
                                LSAVE-RECORD.
           CALL "4-SALBAS" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 POSE-RECORD 
                                 HJS-RECORD 
                                 BASES-REMUNERATION.
           CALL "4-SALMOY" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 HJS-RECORD 
                                 BASES-REMUNERATION
                                 LCUM-RECORD.

      
       LOAD-CSP.
           PERFORM READ-CSP.
           IF CS-VALEUR(CAR-STATUT) = 0
              MOVE 0 TO CSP-UNITAIRE CSP-TOTAL
           END-IF.
           IF CS-VALEUR(10) = 1 AND CAR-HOR-MEN = 1
              MOVE 0 TO CSP-UNITAIRE CSP-TOTAL
           END-IF.
           IF CS-VALEUR(10) = 2 AND CAR-HOR-MEN = 0
              MOVE 0 TO CSP-UNITAIRE CSP-TOTAL
           END-IF.
           IF IND-TEXTE NOT = SPACES
              MOVE IND-TEXTE TO CSP-TEXTE
              INITIALIZE IND-TEXTE.
           PERFORM DIS-HE-03.
           PERFORM DIS-HE-ALL.


        READ-CSP.
           IF INPUT-ERROR = 0
              MOVE FR-KEY     TO CSP-FIRME 
              MOVE REG-PERSON TO CSP-PERSON
              MOVE CS-NUMBER  TO CSP-CODE
              MOVE LNK-SUITE  TO CSP-SUITE 
              MOVE LNK-MOIS   TO CSP-MOIS  
              MOVE PC-NUMBER     TO CSP-POSTE 
              READ CODPAIE NO LOCK INVALID
                   INITIALIZE CSP-REC-DET
              END-READ
           END-IF.    

       TEST-CS.
           MOVE 0 TO LNK-NUM.
           IF CS-PERIODE(LNK-MOIS) = 0
              MOVE 5 TO LNK-NUM.
           IF CS-PERMIS(CAR-STATUT) = 0
              MOVE 17 TO LNK-NUM.
           IF CS-PERMIS(10) = 1 AND CAR-HOR-MEN = 1
              MOVE 32 TO LNK-NUM.
           IF CS-PERMIS(10) = 2 AND CAR-HOR-MEN = 0
              MOVE 31 TO LNK-NUM.
           IF CS-MALADIE = 2 
              MOVE 34 TO LNK-NUM.
           IF CS-NOM = SPACES
              MOVE 1 TO LNK-NUM.
           IF  CS-LIVRE(6, 2) NOT = 501
           AND CS-LIVRE(6, 2) NOT = 502
              MOVE 30 TO LNK-NUM
           END-IF.
           IF LNK-NUM = 0 
              MOVE 0 TO SH-00
              PERFORM TEST-CS1 VARYING IDX-1 FROM 1 BY 1 UNTIL 
              IDX-1 > 5
              IF SH-00 NOT = 0 
                 MOVE 30 TO LNK-NUM
              END-IF
           END-IF.
           IF LNK-NUM NOT = 0 
              MOVE 1 TO INPUT-ERROR
           END-IF.

       CS-MESSAGE.
           IF LNK-NUM NOT = 0 
              IF LNK-NUM = 1 
                 MOVE "AA" TO LNK-AREA
              ELSE
                 MOVE "SL" TO LNK-AREA
              END-IF
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-NUM.
           
       TEST-CS1.
           ADD CS-PROCEDURE(IDX-1)      TO SH-00.
           ADD CS-FORMULE(IDX-1)        TO SH-00.
           ADD CS-VAL-PREDEFINIE(IDX-1) TO SH-00.
           ADD CS-VAL-MAX(IDX-1)        TO SH-00.
           ADD CS-VAL-MIN(IDX-1)        TO SH-00.
           ADD CS-NULL(IDX-1)           TO SH-00.
           ADD CS-INPUT(IDX-1)          TO SH-00.
           ADD CS-COLONNES(IDX-1)       TO SH-00.
           ADD CS-DECIMALES(IDX-1)      TO SH-00.
       
       APRES-5.
           MOVE VH-08(6) TO CSP-DONNEE(6).
           PERFORM DIS-HE-ALL.
           IF CSP-DONNEE(6) = 0
              MOVE 1 TO INPUT-ERROR
           ELSE
              EVALUATE EXC-KEY 
                 WHEN 5 THRU 6 PERFORM APRES-DEC.

       APRES-DEC.
           MOVE DECISION TO TEST-STOP.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM CSP-KEY
                  PERFORM WRITE-CSP
                  PERFORM CALCUL
                  PERFORM CALCUL-V
                  MOVE 1  TO DECISION
                  PERFORM DIS-HE-03 THRU DIS-HE-END
           WHEN 6 PERFORM DEL-CSP
                  MOVE 0 TO LNK-POSITION
                  MOVE "AA" TO LNK-AREA
                  MOVE 22 TO LNK-NUM
                  CALL "0-DMESS" USING LINK-V
                  PERFORM CALCUL
                  COMPUTE HAUT = CSP-DONNEE(6) * 3 
                  COMPUTE BAS  = CSP-DONNEE(6)
                  MOVE BAS TO MEMOIRE
                  MOVE HAUT TO CSP-DONNEE(6) 
220402*           IF L-COTISATIONS-NP > L-IMPOSABLE-BRUT-NP
      *              COMPUTE L-A-PAYER = L-A-PAYER +
      *              L-COTISATIONS-NP - L-IMPOSABLE-BRUT-NP  
      *           END-IF
                  IF CS-LIVRE(6, 2) = 501
                     COMPUTE NB-NET = BAS + L-SALAIRE-NET 
                  ELSE
                     COMPUTE NB-NET = BAS + L-SALAIRE-NET 
                                          + L-SALAIRE-NET-NP
                  END-IF
                  MOVE 0 TO ESSAIS
                  PERFORM APPROCHE
                  PERFORM CALCUL-V
                  MOVE 1  TO DECISION
                  DISPLAY SPACES LINE 24 POSITION 2 SIZE 70
                  PERFORM DIS-HE-03 THRU DIS-HE-END
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       CALCUL.
           INITIALIZE L-RECORD.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           PERFORM REPETE.
       
       REPETE.
           INITIALIZE L-REC-DETAIL.
           CALL "4-CSADD" USING LINK-V REG-RECORD L-RECORD.
           CANCEL "4-CSADD".
           MOVE LSAVE-RECORD TO LCUM-RECORD.
           CALL "4-CALCUL" USING LINK-V 
                                 REG-RECORD
                                 CAR-RECORD
                                 FICHE-RECORD
                                 CCOL-RECORD
                                 PRESENCES 
                                 L-RECORD
                                 LCUM-RECORD.
           IF LNK-YN = "E"
              PERFORM AFFICHAGE-ECRAN 
              PERFORM AFFICHAGE-DETAIL
              PERFORM DISPLAY-F-KEYS.

       CALCUL-V.
           IF CAR-HOR-MEN = 1
           AND LNK-SUITE  = 0
              PERFORM SAL-MOIS.
           MOVE CAR-STATUT   TO L-STATUT.
           MOVE CAR-HOR-MEN  TO L-HOR-MEN. 
           MOVE CAR-STATEC   TO L-STATEC.
           MOVE CAR-REGIME   TO L-REGIME.
           MOVE CAR-DIVISION TO L-DIVISION.
           MOVE CAR-EQUIPE   TO L-EQUIPE.
<          MOVE CAR-COUT     TO L-COUT.
           MOVE REG-SEXE     TO L-CODE-SEXE.
           COMPUTE SH-00 = CCOL-DUREE-JOUR * 21,667 - 1
           IF CAR-HRS-MOIS < SH-00
              MOVE 1 TO L-TEMPS-PARTIEL
           END-IF.
           MOVE FICHE-CLASSE TO L-CLASSE-I.
           MOVE FICHE-CLASSE TO L-CLASSE-I.
           MOVE FICHE-TAUX   TO L-TAUX.
           MOVE BAS-HORAIRE  TO L-SAL-HOR.
           MOVE BAS-PRORATA  TO L-SAL-HOR-P.
           MOVE BAS-MOIS     TO L-SAL-MOIS.
           MOVE BAS-CONGE-MOYENNE   TO L-SAL-HOR-CONGE.
<          MOVE BAS-MALADIE-MOYENNE TO L-SAL-MOY-MAL.

           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD WR-KEY.
           CALL "5-J" USING LINK-V REG-RECORD L-RECORD.
           IF FR-CCM = "X"
             CALL "5-CCM" USING LINK-V REG-RECORD L-RECORD CAR-RECORD
           END-IF.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           MOVE 0 TO JOUR-DEB JOUR-FIN.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           PERFORM TEST-FIN VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.

       TEST-FIN.
           IF PRES-JOUR(LNK-MOIS, IDX) = 1
              IF JOUR-DEB = 0
                 MOVE IDX TO JOUR-DEB 
              END-IF
              MOVE IDX TO JOUR-FIN 
           END-IF.

       READ-POCL.
           MOVE 13 TO SAVE-KEY.
           MOVE "N" TO A-N
           PERFORM NEXT-POCL.
           
       NEXT-POCL.
           CALL "6-POCL" USING LINK-V PC-RECORD A-N SAVE-KEY.
           IF LNK-VAL > 0
              GO NEXT-POCL
           END-IF.
           MOVE 13 TO SAVE-KEY.

       NEXT-CS.
           MOVE 0 TO INPUT-ERROR.
           CALL "6-CODSAL" USING LINK-V CS-RECORD SAVE-KEY.
           IF CS-NOM NOT = SPACES
              PERFORM TEST-CS
              IF INPUT-ERROR = 1
              AND SAVE-KEY NOT = 13
                 GO NEXT-CS
              END-IF
           END-IF.
           IF LNK-LANGUAGE NOT = "F"
              CALL "6-CODTXT" USING LINK-V CS-RECORD.

       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-02-M.
           MOVE L-JOUR-DEBUT TO HE-Z2.
           DISPLAY HE-Z2 LINE 6 POSITION 35.
           DISPLAY "-"   LINE 6 POSITION 37.
           MOVE L-JOUR-FIN TO HE-Z2.
           DISPLAY HE-Z2 LINE 6 POSITION 38.
       DIS-HE-03.
           MOVE CS-NUMBER    TO HE-Z4.
           DISPLAY CSP-PROTECT LINE 4 POSITION 29.
           DISPLAY HE-Z4  LINE  4 POSITION 24.
           IF CSP-TEXTE = SPACES
              DISPLAY CS-NOM LINE 4 POSITION 30
           ELSE
              DISPLAY CSP-TEXTE LINE 4 POSITION 30
           END-IF.
           DISPLAY CS-COMMENTAIRE  LINE  5 POSITION 24 SIZE 55.
           PERFORM DISPLAY-TEXTE VARYING IDX-2 FROM 1 BY 1
           UNTIL IDX-2 > 6.

       DIS-HE-04.
           MOVE PC-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 6 POSITION 20.
           DISPLAY PC-NOM LINE 6 POSITION 33 SIZE 30.
       DIS-HE-ALL.
           MOVE 6 TO IDX-1.
           PERFORM DISPLAY-VAL. 
       DIS-HE-END.
           EXIT.

       DISPLAY-VAL.
           COMPUTE LIN-IDX = IDX-1 + 7.
           MOVE 24 TO COL-IDX.
           IF CS-DECIMALES(IDX-1) > 0
              COMPUTE COL-IDX = 25 + CS-DECIMALES(IDX-1).
           MOVE VH-08(IDX-1) TO HE-Z8Z3.
           DISPLAY HE-Z8Z3 LINE LIN-IDX POSITION 16.
           DISPLAY "    " LINE LIN-IDX POSITION COL-IDX.

       DISPLAY-TEXTE.
           COMPUTE LIN-IDX = IDX-2 + 7.
           DISPLAY CS-DESCRIPTION(IDX-2) LINE LIN-IDX POSITION 5 LOW.

       CS-TEXT.
           MOVE CSP-TEXTE TO IND-TEXTE.
           MOVE 0000000000 TO EXC-KFR(1).
           MOVE 0000080000 TO EXC-KFR(2).
           PERFORM DISPLAY-F-KEYS.
           ACCEPT IND-TEXTE LINE 5 POSITION 33 SIZE 25
           TAB UPDATE NO BEEP CURSOR 1
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO CS-TEXT.
           IF EXC-KEY = 98 GO CS-TEXT.
           MOVE IND-TEXTE TO CSP-TEXTE.
           REWRITE CSP-RECORD INVALID CONTINUE 
           NOT INVALID INITIALIZE IND-TEXTE
           END-REWRITE.
           IF LNK-SQL = "Y"
              CALL "9-CODPAI" USING LINK-V CSP-RECORD WR-KEY.
           PERFORM DIS-HE-03.
       CS-TEXT-END.
           EXIT.

       WRITE-CSP.
           IF CSP-TOTAL NOT = 0
              IF CSP-CODE NOT = 0
                 CALL "0-TODAY" USING TODAY
                 MOVE TODAY-TIME TO CSP-TIME
                 MOVE LNK-USER TO CSP-USER
              CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY
              END-IF
           END-IF.

       DEL-CSP.
           PERFORM CSP-KEY.
           IF LNK-SQL = "Y"
              CALL "9-CODPAI" USING LINK-V CSP-RECORD DEL-KEY.
           DELETE CODPAIE INVALID CONTINUE
           NOT INVALID MOVE 1 TO MODIFICATION
           END-DELETE.

       CSP-KEY.
           MOVE FR-KEY     TO CSP-FIRME.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE LNK-MOIS   TO CSP-MOIS.
           MOVE LNK-SUITE  TO CSP-SUITE.
           MOVE CS-NUMBER  TO CSP-CODE.
           MOVE PC-NUMBER     TO CSP-POSTE.
       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS THRU SUBTRACT-END
             WHEN 58 PERFORM ADD-MOIS THRU ADD-END
           END-EVALUATE.
           CANCEL "6-CARRI".
           DISPLAY LNK-MOIS LINE 1 POSITION 74.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0 
              GO SUBTRACT-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO SUBTRACT-END.
           GO SUBTRACT-MOIS.
       SUBTRACT-END.
           IF LNK-MOIS = 0
              MOVE SAVE-MOIS TO LNK-MOIS
           END-IF.
       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
              GO ADD-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO ADD-END.
           GO ADD-MOIS.
       ADD-END.
           IF LNK-MOIS > 12
              MOVE SAVE-MOIS TO LNK-MOIS
           END-IF.

       SAL-MOIS.
           MOVE L-UNIT(100) TO HRS-MOIS.
           MOVE 0 TO IDX-2.
           PERFORM OCCUP VARYING IDX-1 FROM 11 BY 1 UNTIL IDX-1 > 50.
           MOVE 100 TO IDX-3.
           IF IDX-2 > 0
              MOVE L-DEB(100) TO SH-00
              IF L-UNIT(100) = 0
                 MOVE L-DEB(100) TO L-DEB(IDX-2)
                 MOVE 0 TO L-DEB(100) 
                 MOVE IDX-2 TO IDX-3
                 SUBTRACT 1 FROM IDX-2
              END-IF
              PERFORM OCCUP-1 VARYING IDX-1 FROM 11 BY 1 UNTIL IDX-1 
                 > IDX-2
           END-IF.

       OCCUP.
           IF PAREX-OCC(IDX-1) > 0
           AND L-UNIT(IDX-1) > 0 
              ADD L-UNIT(IDX-1) TO HRS-MOIS
              MOVE IDX-1 TO IDX-2
           END-IF.

       OCCUP-1.
           IF PAREX-OCC(IDX-1) > 0
           AND L-DEB(IDX-1) = 0 
              COMPUTE SAL-ARR = SH-00 / HRS-MOIS * L-UNIT(IDX-1) + ,005
              MOVE SAL-ARR TO L-DEB(IDX-1)
              SUBTRACT SAL-ARR FROM L-DEB(IDX-3)
           END-IF.
        
       AFFICHAGE-ECRAN.
           MOVE 501 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF MODIFICATION = 1
              PERFORM CALCUL
           END-IF.
           MOVE 0 TO LNK-SUITE.
           CLOSE CODPAIE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


       APPROCHE.    
           PERFORM WRITE-CSP.
           PERFORM REPETE.
           ADD 1 TO ESSAIS.
           IF CS-LIVRE(6, 2) = 501
              COMPUTE SH-00 = L-SALAIRE-NET 
           ELSE
              COMPUTE SH-00 = L-SALAIRE-NET + L-SALAIRE-NET-NP
           END-IF.
           IF SH-00 > NB-NET
              MOVE CSP-TOTAL TO HAUT
           ELSE
              MOVE CSP-TOTAL TO BAS
           END-IF.
           IF ESSAIS > 25
              MOVE "AA" TO LNK-AREA
              MOVE 25 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              PERFORM END-PROGRAM
              PERFORM AA
           END-IF.
           IF TEST-STOP = 99
              PERFORM TEST-STOP.
           IF SH-00 NOT = NB-NET
              COMPUTE CSP-TOTAL = (HAUT + BAS) / 2
              GO APPROCHE
           END-IF.

       TEST-STOP. 
           MOVE SH-00 TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 23 POSITION 50.
           MOVE NB-NET  TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 23 POSITION 60.
           MOVE HAUT TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 24 POSITION 50.
           MOVE BAS  TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 24 POSITION 60.
           PERFORM AA.
