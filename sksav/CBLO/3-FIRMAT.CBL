      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-FIRMAT LISTE CONTROLE MATRICULE FIRMES    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  3-FIRMAT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  JOB-STANDARD          PIC X(10) VALUE "80       ".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "IMPRLOG.REC".
       01  NOT-OPEN              PIC 9 VALUE 0.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".F01".

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-FIRMAT .

           IF  FR-FIN-A NOT = 0
           AND FR-FIN-A < LNK-ANNEE
               EXIT PROGRAM
           END-IF.
           IF  FR-FIN-M NOT = 0
           AND FR-FIN-M < LNK-MOIS
               EXIT PROGRAM
           END-IF.
           IF  FR-SNOCS  NOT = 0
           AND FR-EXTENS NOT = 0
               EXIT PROGRAM
           END-IF.

       
           IF LNK-VAL = 99
              PERFORM END-PROGRAM.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= 64
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              MOVE LNK-LANGUAGE TO FORM-LANGUE
              PERFORM READ-FORM
              PERFORM PAGE-DATE 
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           PERFORM DONNEES-FIRME.
           EXIT PROGRAM.

       DONNEES-FIRME.
           MOVE  0 TO DEC-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  5 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE FR-PHONE  TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           ADD 1 TO LIN-NUM.
           MOVE 13 TO COL-NUM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE FR-FAX TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           ADD 1 TO LIN-NUM.
           MOVE 13 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE FR-CODE-POST TO VH-00.
           MOVE 5 TO CAR-NUM.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 60 TO COL-NUM.
           EVALUATE FR-FISC-CODE  
                WHEN 0 MOVE 104 TO LNK-NUM
                WHEN 1 MOVE 105 TO LNK-NUM
                WHEN 2 MOVE 107 TO LNK-NUM
           END-EVALUATE.
           MOVE "AA" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 75 TO COL-NUM.
           MOVE FR-LANGUE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           ADD 1 TO LIN-NUM.
           MOVE 13 TO COL-NUM.
           MOVE FR-DATE-ETAB TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 30 TO COL-NUM.
           MOVE FR-ACTIVITE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE FR-CORRESPONDANT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 2 TO LIN-NUM.

       PAGE-DATE.
           CALL "0-TODAY" USING TODAY.
           ADD 1 TO PAGE-NUMBER.
           MOVE  4 TO CAR-NUM.
           MOVE  2 TO LIN-NUM.
           MOVE 73 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 47 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 50 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 53 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.


       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF COUNTER > 0
              IF LIN-IDX < LIN-NUM
                 PERFORM TRANSMET
              END-IF
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE
              CANCEL "P080".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".

