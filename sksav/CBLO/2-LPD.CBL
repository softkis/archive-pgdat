      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-LPD AFFICHAGE PERSONNES AVEC UNE ZONE     �
      *  � DU LIVRE DE PAYE DONN륟                               �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-LPD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "LIVRE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "LIVRE.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "LIVREX.REC".
           COPY "V-VAR.CPY".
           COPY "PRESENCE.REC".

       01  CHOIX-MAX             PIC 99 VALUE 3.
       01  CHOIX                 PIC X(10) VALUE SPACES.
       01  COMPTEUR              PIC 9999 VALUE 0.
       01  CS-VENTILATION.
           05 CS-LIVRE       PIC 9999.
           05 CS-AB REDEFINES CS-LIVRE.
              06 CS-A        PIC 9.
              06 CS-B        PIC 999.
           05 CS-CD REDEFINES CS-LIVRE.
              06 CS-C        PIC 99.
              06 CS-D        PIC 99.

       01  LIVRE-NAME.
           02 FILLER             PIC X(8) VALUE "S-LIVRE.".
           02 ANNEE-LIVRE         PIC 999.
       
       01  MINIMAX.
           02 TOT-MINI           PIC 9(8)V99 VALUE ,1.
           02 TOT-MAX            PIC 9(8)V99 VALUE 99999999.
       01  TOTAL-VALEUR.
           02 TOT-TOTAL          PIC 9(8)V99 COMP-3.
           02 TOT-PERS           PIC 9(4) COMP-3.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
           02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.
           02 HE-Z8Z2 PIC Z(8),ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON LIVRE.


       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-LPD .

           MOVE LNK-SUFFIX TO ANNEE-LIVRE.
           OPEN INPUT LIVRE.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1  MOVE 0029000000 TO EXC-KFR(1)
                   MOVE 0000000065 TO EXC-KFR(13)
                   MOVE 6600000000 TO EXC-KFR(14).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1
           WHEN  2 PERFORM AVANT-2
           WHEN  3 PERFORM AVANT-3.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2
           WHEN  3 PERFORM APRES-3.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT LEX-NUMBER
             LINE  3 POSITION 25 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 
                PERFORM CHANGE-MOIS
               MOVE 13 TO EXC-KEY
           END-EVALUATE.

       AVANT-2.
           MOVE TOT-MINI TO HE-Z8Z2.
           MOVE 0 TO IDX-3.
           INSPECT HE-Z8Z2 TALLYING IDX-3 FOR ALL " ".
      *    ADD 1 TO IDX-3.
           ACCEPT HE-Z8Z2
           LINE 4 POSITION 25 SIZE 11
           TAB UPDATE NO BEEP CURSOR  IDX-3 
           ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z8Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z8Z2 TO TOT-MINI.
       AVANT-3.
           MOVE TOT-MAX TO HE-Z8Z2.
           MOVE 0 TO IDX-3.
           INSPECT HE-Z8Z2 TALLYING IDX-3 FOR ALL " ".
           ADD 1 TO IDX-3.
           ACCEPT HE-Z8Z2
           LINE 4 POSITION 55 SIZE 11
           TAB UPDATE NO BEEP CURSOR  IDX-3 
           ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z8Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z8Z2 TO TOT-MAX.
           
      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-LEX" USING LINK-V LEX-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-LIVREX
           END-EVALUATE.
           IF LEX-NUMBER = 0 MOVE 1 TO INPUT-ERROR.
           PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           MOVE TOT-MINI TO HE-Z8Z2.
           DISPLAY HE-Z8Z2 LINE  4 POSITION 25.

       APRES-3.
           IF TOT-MINI > TOT-MAX
              MOVE TOT-MINI TO TOT-MAX
              MOVE 1 TO INPUT-ERROR.
           MOVE TOT-MAX TO HE-Z8Z2.
           DISPLAY HE-Z8Z2 LINE  4 POSITION 55.
       
           IF LEX-NUMBER > 0
           AND EXC-KEY NOT = 27
               MOVE LEX-NUMBER TO CS-VENTILATION
               PERFORM AFFICHE-DEBUT THRU AFFICHE-END.
           IF EXC-KEY = 13 MOVE 52 TO EXC-KEY.

       NEXT-LIVREX.
           CALL "6-LEX" USING LINK-V LEX-RECORD SAVE-KEY.

       DIS-HE-01.
           DISPLAY LEX-NUMBER LINE  3 POSITION 25
           DISPLAY LEX-DESCRIPTION  LINE  3 POSITION 35 SIZE 45.
       DIS-HE-END.
           EXIT.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE NOT-FOUND COMPTEUR CHOIX TOTAL-VALEUR.
           MOVE 5 TO LIN-IDX.
           MOVE 0 TO NOT-FOUND.
           INITIALIZE L-RECORD.
           PERFORM READ-LEX THRU READ-LEX-END.
       AFFICHE-END.
           EXIT.

       READ-LEX.
           MOVE 66 TO SAVE-KEY.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" SAVE-KEY.
           IF REG-PERSON  = 0
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              GO READ-LEX-END.
           IF LNK-COMPETENCE < REG-COMPETENCE
              GO READ-LEX.
            PERFORM RECHERCHE-L.
      *     MOVE FR-KEY TO L-FIRME.
      *     MOVE REG-PERSON TO L-PERSON.
      *     MOVE 0         TO L-SUITE.
      *     MOVE LNK-MOIS  TO L-MOIS.
      *     MOVE LEX-NUMBER TO L-CODE.
      *     READ LIVRE INVALID CONTINUE
      *         NOT INVALID
      *         PERFORM DIS-HIS-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              MOVE 5 TO LIN-IDX
           END-IF.
           IF EXC-KEY = 52 GO READ-LEX-END.
           GO READ-LEX.
       READ-LEX-END.
           PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21.
           PERFORM DIS-TOT-LIGNE.


      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           ADD 1 TO COMPTEUR.
           MOVE REG-PERSON TO HE-Z6.
           DISPLAY HE-Z6  LINE LIN-IDX POSITION 02 LOW.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE LIN-IDX POSITION 9 SIZE 50.
           ADD 1 TO TOT-PERS.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF PRES-TOT(L-MOIS) = 0
              DISPLAY "?" LINE LIN-IDX POSITION 77.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000030000 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052000000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           PERFORM DIS-TOT-LIGNE.
           ACCEPT CHOIX 
           LINE 23 POSITION 30 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY = 66
              MOVE 13 TO EXC-KEY.
           IF EXC-KEY = 52 GO INTERRUPT-END.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.

       INTERRUPT-END.
           EXIT.

       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS 
             WHEN 58 PERFORM ADD-MOIS 
           END-EVALUATE.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-DETAIL.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0
              MOVE 1 TO LNK-MOIS
           END-IF.
       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
              MOVE 12 TO LNK-MOIS
           END-IF.


       AFFICHAGE-ECRAN.
           MOVE 2507 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CANCEL "6-LEX".
           CLOSE LIVRE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

       DIS-HIS-LIGNE.
           PERFORM DIS-DET-LIGNE.
           MOVE SH-00 TO HE-Z8Z2.
           INSPECT HE-Z8Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z8Z2   LINE  LIN-IDX POSITION 66.
           ADD SH-00 TO TOT-TOTAL.

       DIS-TOT-LIGNE.
           MOVE 22 TO LIN-IDX.
           MOVE TOT-PERS TO HE-Z4.
           DISPLAY HE-Z4   LINE  LIN-IDX POSITION 2.
           MOVE TOT-TOTAL TO HE-Z8Z2.
           INSPECT HE-Z8Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z8Z2 LINE  LIN-IDX POSITION 66.

       RECHERCHE-L.
           INITIALIZE L-RECORD.
           MOVE FR-KEY     TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON.
           MOVE LNK-MOIS   TO L-MOIS.
           START LIVRE KEY >= L-KEY INVALID CONTINUE
               NOT INVALID PERFORM READ-L THRU READ-L-END.

       READ-L.
           READ LIVRE NEXT NO LOCK AT END
               GO READ-L-END
           END-READ.
           IF FR-KEY     NOT = L-FIRME
           OR REG-PERSON NOT = L-PERSON
           OR LNK-MOIS   NOT = L-MOIS 
              GO READ-L-END
           END-IF.
           IF L-SUITE NOT = 0
              GO READ-L
           END-IF.
           PERFORM VENT.
           IF SH-00 < TOT-MINI
           OR SH-00 > TOT-MAX
              GO READ-L
           END-IF.
           IF LIN-IDX > 20
              PERFORM INTERRUPT THRU INTERRUPT-END
              MOVE 5 TO LIN-IDX
           END-IF.
           IF EXC-KEY = 52 GO READ-L-END.
           PERFORM DIS-HIS-LIGNE.
           GO READ-L.
       READ-L-END.
           EXIT.

       VENT.
           IF CS-LIVRE > 0
              MOVE CS-B TO IDX-2
              MOVE CS-D TO IDX-3
           EVALUATE CS-C
             WHEN 00 THRU 03 PERFORM GET-DC
             WHEN 04 PERFORM GET-COT
             WHEN 05 PERFORM GET-IMP
             WHEN 06 PERFORM GET-MOY
             WHEN 07 PERFORM GET-SNO
             WHEN 08 PERFORM GET-FLAG
             WHEN 09 PERFORM GET-JRS
             WHEN 10 THRU 12 PERFORM GET-UN
MEMO         WHEN OTHER CONTINUE
           END-EVALUATE.

       GET-DC.
           MOVE L-DC(IDX-2) TO SH-00.
       GET-UN.
           MOVE L-UNITE(IDX-2) TO SH-00.
       GET-COT.
           MOVE L-COT(IDX-3) TO SH-00.
       GET-IMP.
           MOVE L-IMPOS(IDX-3) TO SH-00.
       GET-SNO.
           MOVE L-SNOCS(IDX-3) TO SH-00.
       GET-FLAG.
           MOVE L-FLAG(IDX-3) TO SH-00.
       GET-MOY.
           MOVE L-MOY(IDX-3) TO SH-00.
       GET-JRS.
           MOVE L-JRS(IDX-3) TO SH-00.


