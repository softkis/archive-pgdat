      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-HEURES INTERROGATION JOURS PERSONNE MOIS  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-HEURES.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "POCL.REC".
           COPY "OCCOM.REC".
           COPY "OCCUP.REC".
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  PRECISION             PIC 9 VALUE 0.
       01  SHOW-KEY              PIC 9 VALUE 0.
       01  HE-VAL              PIC ZZ BLANK WHEN ZERO.
       01  HELP-1                PIC 9999.
       01  HELP-2                PIC 9.
       01  CHOIX                 PIC X.
       
       01  JOURS-NAME.
           02 JOURS-ID           PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.
  
       01  ECR-DISPLAY.
           02 HE-Z PIC ,Z.
           02 HE-Z2 PIC ZZ.
           02 HE-Z3Z2 PIC Z(3),ZZ.
           02 HE-Z4Z2 PIC Z(4),ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-HEURES.
       


           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           IF MENU-BATCH > 0
              MOVE "S-PLJRS." TO JOURS-ID.
           OPEN INPUT JOURS.
           PERFORM AFFICHAGE-ECRAN .
           MOVE 1 TO INDICE-ZONE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF EXC-KEY > 66 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              MOVE CHOIX-MAX TO INDICE-ZONE
              GO TRAITEMENT-ECRAN
           END-IF.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 .

           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           MOVE FR-KEY TO REG-FIRME.
           IF LNK-PERSON NOT = 0 
              MOVE LNK-PERSON TO REG-PERSON 
              MOVE 27 TO EXC-KEY
              MOVE 0 TO LNK-PERSON
           ELSE    
           ACCEPT REG-PERSON 
             LINE  3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 PERFORM CHANGE-MOIS
             MOVE 27 TO EXC-KEY
           END-EVALUATE.
           IF EXC-KEY = 12
              IF SHOW-KEY = 1
                 MOVE 0 TO SHOW-KEY 
              ELSE
                 MOVE 1 TO SHOW-KEY 
              END-IF
           END-IF.

       AVANT-2.            
           ACCEPT REG-MATCHCODE
             LINE  3 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           PERFORM DIS-HE-01.
           PERFORM PRESENCE.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   PERFORM PRESENCE
                   PERFORM DIS-HE-01
                   PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.                     
           
        NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           PERFORM DISPLAY-MESSAGE.

       DIS-HE-01.
           MOVE REG-PERSON TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
           DISPLAY REG-MATCHCODE LINE  3 POSITION 33.


       AFFICHAGE-ECRAN.
           MOVE 2300 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           PERFORM AFFICHAGE-CALENDRIER.

       AFFICHAGE-CALENDRIER.
           MOVE  6 TO LNK-LINE.
           MOVE  9 TO LNK-COL.
           MOVE  0 TO LNK-SPACE.
           MOVE LNK-MOIS TO LNK-SIZE.
           CALL "0-DSCAL" USING LINK-V.

       AFFICHAGE-DETAIL.
           PERFORM AFFICHAGE-JOURS.

       AFFICHAGE-JOURS.
           MOVE 6 TO LIN-IDX.
           INITIALIZE JRS-RECORD NOT-FOUND IDX.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE LNK-MOIS   TO JRS-MOIS.
           MOVE REG-PERSON TO JRS-PERSON.
           START JOURS KEY >= JRS-KEY 
               INVALID MOVE 6 TO LIN-IDX
               PERFORM READ-JRS-END
               NOT INVALID PERFORM READ-JOURS THRU READ-JRS-END.

       READ-JOURS.
           READ JOURS NEXT AT END
               GO READ-JRS-END
           END-READ.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
              GO READ-JRS-END
           END-IF.
           ADD 1 TO IDX.
           IF  JRS-OCCUPATION = 0
           AND JRS-COMPLEMENT = 0
           AND JRS-POSTE = 0
               CONTINUE
           ELSE
              ADD 1 TO LIN-IDX
              DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           IF JRS-COMPLEMENT NOT = 0
              MOVE JRS-COMPLEMENT TO OCO-NUMBER HE-Z8
              CALL "6-OCCOM" USING LINK-V OCO-RECORD FAKE-KEY
              DISPLAY OCO-NOM LINE LIN-IDX POSITION 10 LOW
              DISPLAY HE-Z8 LINE LIN-IDX POSITION 1 LOW
           ELSE
              IF JRS-POSTE NOT = 0
                 MOVE JRS-POSTE TO PC-NUMBER HE-Z8 
                 DISPLAY HE-Z8 LINE LIN-IDX POSITION 1
                 CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
                 INSPECT PC-NOM TALLYING IDX-4 FOR CHARACTERS BEFORE 
                 "   "
                 DISPLAY PC-NOM LINE LIN-IDX POSITION 10 SIZE IDX-4
                 REVERSE
                 INSPECT JRS-EXTENSION TALLYING IDX-4 FOR CHARACTERS 
                 BEFORE  "   "
                 INSPECT JRS-EXTENSION REPLACING ALL "000" BY "   "
                 DISPLAY JRS-EXTENSION LINE LIN-IDX POSITION 56
                 SIZE IDX-4 LOW
              ELSE
                 IF JRS-OCCUPATION NOT = 0
                    MOVE JRS-OCCUPATION TO OCC-KEY HE-Z8
                    CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY
                    CALL "6-OCCTXT" USING LINK-V OCC-RECORD 
                    DISPLAY OCC-NOM LINE LIN-IDX POSITION 10
                    DISPLAY HE-Z8 LINE LIN-IDX POSITION 1
                    DISPLAY JRS-MUTUELLE LINE LIN-IDX POSITION 80
                 END-IF
              END-IF
           END-IF.
           ADD 1 TO LIN-IDX.
           MOVE 6 TO COL-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           PERFORM DIS-JOUR VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 31.
           MOVE JRS-HRS(32) TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   "
           MOVE 72 TO COL-IDX.
           DISPLAY HE-Z4Z2 LINE LIN-IDX POSITION COL-IDX.

           IF JRS-COMPLEMENT = 0
              MOVE JRS-JOURS TO HE-Z2
              DISPLAY HE-Z2 LINE LIN-IDX POSITION 1.

           IF SHOW-KEY = 1
              DISPLAY JRS-KEY LINE LIN-IDX POSITION 1
           END-IF.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 52 
                 GO READ-JRS-END
              END-IF
              MOVE 6 TO LIN-IDX
           END-IF.
           GO READ-JOURS.
       READ-JRS-END.
           ADD 1 TO LIN-IDX.
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 23.

       DIS-JOUR.
           ADD 2 TO COL-IDX.
           DIVIDE IDX-1 BY 2 GIVING IDX-3 REMAINDER IDX-4.
           IF JRS-HRS(IDX-1) > 0
              MOVE JRS-HRS(IDX-1) TO HE-VAL HE-Z
              IF JRS-HRS(IDX-1) >= 1
                 IF IDX-4 = 0
                    DISPLAY HE-VAL LINE LIN-IDX POSITION COL-IDX HIGH
                 ELSE
                    DISPLAY HE-VAL LINE LIN-IDX POSITION COL-IDX REVERSE
                 END-IF
              ELSE
      *          IF IDX-4 = 0
      *             DISPLAY HE-Z LINE LIN-IDX POSITION COL-IDX HIGH
      *          ELSE
      *             DISPLAY HE-Z LINE LIN-IDX POSITION COL-IDX REVERSE
      *          END-IF
                   DISPLAY HE-Z LINE LIN-IDX POSITION COL-IDX LOW
              END-IF
           END-IF.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052000000 TO EXC-KFR (11).
           MOVE 0067000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.

           ACCEPT CHOIX 
           LINE 23 POSITION 30 SIZE 1
           TAB UPDATE NO BEEP CURSOR 1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 1 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY = 66
              MOVE 13 TO EXC-KEY.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 52 
              PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM 8
              BY 1 UNTIL LIN-IDX > 23
              GO INTERRUPT-END.

       INTERRUPT-END.
           EXIT.

       END-PROGRAM.
           CLOSE JOURS.
           CANCEL "6-POSTE".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       DISPLAY-JOUR-VAL.
           ADD 2 TO COL-IDX.
           DIVIDE IDX-1 BY 2 GIVING IDX-3 REMAINDER IDX-4.
           IF JRS-VAL(IDX-1) > 0
              IF IDX-4 = 0
                 DISPLAY " X" LINE LIN-IDX POSITION COL-IDX HIGH
              ELSE
                 DISPLAY " X" LINE LIN-IDX POSITION COL-IDX REVERSE
              END-IF
           END-IF.

       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS THRU SUBTRACT-END
             WHEN 58 PERFORM ADD-MOIS THRU ADD-END
           END-EVALUATE.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-CALENDRIER.
           PERFORM AFFICHAGE-DETAIL.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0 
           OR REG-PERSON = 0
              GO SUBTRACT-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO SUBTRACT-END.
           GO SUBTRACT-MOIS.
       SUBTRACT-END.
           IF LNK-MOIS = 0
              IF REG-PERSON = 0
                 MOVE 1 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.
       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
           OR REG-PERSON = 0
              GO ADD-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO ADD-END.
           GO ADD-MOIS.
       ADD-END.
           IF LNK-MOIS > 12
              IF REG-PERSON = 0
                 MOVE 12 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.

              
