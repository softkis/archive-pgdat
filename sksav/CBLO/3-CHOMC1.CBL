      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-CHOMC1 IMPRESSION FICHE CHOMAGE CONJ.     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CHOMC1.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM130.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 6.
       01  PRECISION             PIC 9 VALUE 0.
       01  JOB-STANDARD          PIC X(10) VALUE "130       ".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "FIRME.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "STATUT.REC".
           COPY "LIVRE.REC".
           COPY "COUT.REC".
           COPY "CODPAR.REC".
           COPY "CODPAIE.REC".
           COPY "IMPRLOG.REC".
           COPY "METIER.REC".
           COPY "PAYS.REC".


           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME            PIC X(8) VALUE "FORM.CC1".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z2Z2 PIC ZZ,ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                FORM.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CHOMC1.
       
           PERFORM AFFICHAGE-ECRAN .
           CALL "6-GCP" USING LINK-V CP-RECORD.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.

           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           INITIALIZE L-RECORD LNK-SUITE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-UNI-CHOM-C-E = 0
              GO READ-PERSON-1
           END-IF.
           MOVE CAR-METIER TO MET-CODE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE PR-NATIONALITE TO PAYS-CODE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.
           PERFORM FILL-FILES.

       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.


       FILL-FILES.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 1 TO COUNTER
           END-IF.
           PERFORM READ-FORM.
           COMPUTE LIN-NUM =  3 .
           MOVE 17 TO COL-NUM.
           MOVE FR-ETAB-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 22 TO COL-NUM.
           MOVE FR-ETAB-N TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 28 TO COL-NUM.
           MOVE FR-SNOCS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 32 TO COL-NUM.
           MOVE FR-EXTENS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 4 .
           MOVE 10 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 5 .
           MOVE 10 TO COL-NUM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 6 .
           MOVE 10 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 5 .
           MOVE 54 TO COL-NUM.
           PERFORM MOIS-NOM.
           PERFORM FILL-FORM.
           MOVE 71 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM =  13.
           MOVE 55 TO COL-NUM.
           MOVE PR-NAISS-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 45 TO COL-NUM.
           MOVE PR-NAISS-M TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           PERFORM FILL-FORM.
           MOVE 42 TO COL-NUM.
           MOVE PR-NAISS-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 65 TO COL-NUM.
           MOVE "Masc" TO  ALPHA-TEXTE.
           IF PR-CODE-SEXE = 2
               MOVE "Fem" TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.


           COMPUTE LIN-NUM =  11.
           MOVE 42 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 15.
           MOVE 42 TO COL-NUM.
           MOVE PR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           IF PR-PAYS = "D" OR "F" OR "CZ" OR "I"
              MOVE PR-CP5 TO ALPHA-TEXTE
           ELSE
              MOVE PR-CP4 TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-LOCALITE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 17.
           MOVE 42 TO COL-NUM.
           MOVE PAYS-NATION TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 19.
           MOVE 42 TO COL-NUM.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           IF ALPHA-TEXTE NOT = SPACES
               PERFORM FILL-FORM
               ADD 1 TO COL-NUM.
           MOVE CAR-POSITION TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 20.
           MOVE 42 TO COL-NUM.
           MOVE PR-MATRICULE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 25.
           MOVE 101 TO COL-NUM.
           MOVE   3 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE L-UNI-SALAIRE TO VH-00.
           ADD  L-UNI-RECUP   TO VH-00.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 27.
           MOVE 101 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           COMPUTE VH-00 = L-UNI-CHOM-CON-P.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 29.
           MOVE 101 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           COMPUTE VH-00 = L-UNI-REGUL-LIM - L-UNI-SALAIRE - L-UNI-RECUP
                         - L-UNI-CHOM-CON-P.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 31.
           MOVE 101 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           COMPUTE VH-00 = L-UNI-REGUL-LIM.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 35.
           MOVE 101 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           COMPUTE VH-00 = L-UNI-CHOM-CON-P.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 41.
           MOVE 101 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           COMPUTE VH-00 = L-UNI-CHOM-C-S.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 43.
           MOVE 101 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           COMPUTE VH-00 = L-UNI-CHOM-C-P + L-UNI-CHOM-C-E.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 51.
           MOVE 10 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           PERFORM FILL-FORM.


           INITIALIZE CSP-RECORD.
           MOVE FR-KEY     TO CSP-FIRME.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE LNK-MOIS   TO CSP-MOIS.
           MOVE COD-PAR(145, 1) TO CSP-CODE.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           COMPUTE LIN-NUM = 49.
           MOVE 95 TO COL-NUM.
           MOVE  7 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE CSP-TOTAL TO VH-00.
           PERFORM FILL-FORM.
           MOVE 29 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE CSP-POURCENT TO VH-00.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE CSP-UNITE TO VH-00.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE  5 TO CAR-NUM.
           MOVE  4 TO DEC-NUM.
           MOVE CSP-UNITAIRE TO VH-00.
           PERFORM FILL-FORM.

           MOVE COD-PAR(146, 1) TO CSP-CODE.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           COMPUTE LIN-NUM = 47.
           MOVE 97 TO COL-NUM.
           MOVE  5 TO CAR-NUM.
           MOVE  4 TO DEC-NUM.
           MOVE CSP-DONNEE-2 TO VH-00.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 51.
           MOVE 29 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE CSP-POURCENT TO VH-00.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE CSP-UNITE TO VH-00.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE  5 TO CAR-NUM.
           MOVE  4 TO DEC-NUM.
           MOVE CSP-UNITAIRE TO VH-00.
           PERFORM FILL-FORM.
           MOVE 95 TO COL-NUM.
           MOVE  7 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE CSP-TOTAL TO VH-00.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 53.
           MOVE 95 TO COL-NUM.
           MOVE  7 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           COMPUTE VH-00 = L-MNT-CHOM-CON-P + L-MNT-CHOM-C-E.
           PERFORM FILL-FORM.

           PERFORM TRANSMET.
           PERFORM DIS-HE-01.

       AFFICHAGE-ECRAN.
           MOVE 1922 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".


