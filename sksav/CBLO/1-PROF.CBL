      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-PROF GESTION DES PROFESSIONS              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-PROF.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PROF.FC".


       DATA DIVISION.

       FILE SECTION.

           COPY "PROF.FDE".


       WORKING-STORAGE SECTION.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 5. 
       01  LANGUE                PIC XX. 

       01   ECR-DISPLAY.
            02 HE-Z2 PIC Z(2).
            02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4).
            02 HE-Z5 PIC Z(5).
            02 HE-Z6 PIC Z(6).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PROFESSION.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-1-PROF.
       
           OPEN I-O PROFESSION.

           PERFORM AFFICHAGE-ECRAN .
           MOVE LNK-LANGUAGE TO LANGUE SAVE-LANGUAGE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0023640000 TO EXC-KFR (1)
                      MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
           WHEN 5     MOVE 0000000005 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 THRU 4 PERFORM AVANT-DESCR
           WHEN  5 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  2 PERFORM APRES-2 
           WHEN  5 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           IF LANGUE = SPACES
              MOVE LNK-LANGUAGE TO LANGUE.
           ACCEPT LANGUE
             LINE  4 POSITION 25 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT PROF-CODE
             LINE  5 POSITION 25 SIZE 5
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-DESCR.
           COMPUTE LIN-IDX = 3 + INDICE-ZONE.
           COMPUTE IDX = INDICE-ZONE - 2.
           ACCEPT PROF-NOM(IDX)
             LINE  LIN-IDX POSITION 20 SIZE 60
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           IF PROF-LANGUE = SPACES
              MOVE LNK-LANGUAGE TO LANGUE
              MOVE 1 TO INPUT-ERROR
           ELSE
              MOVE LANGUE TO PROF-LANGUE.

       APRES-2.
           EVALUATE EXC-KEY
           WHEN 2 MOVE LANGUE TO LNK-LANGUAGE 
                  CALL "2-CITP" USING LINK-V PROF-RECORD
                  CANCEL "2-CITP"
                  MOVE SAVE-LANGUAGE TO LNK-LANGUAGE 
                  PERFORM AFFICHAGE-ECRAN 
           WHEN 3 MOVE LANGUE TO LNK-LANGUAGE 
                  CALL "2-PROF" USING LINK-V PROF-RECORD
                  CANCEL "2-PROF"
                  MOVE SAVE-LANGUAGE TO LNK-LANGUAGE 
                  PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER
                  PERFORM NEXT-PROF.
           IF PROF-CODE = 0
              MOVE 1 TO INPUT-ERROR.
           PERFORM AFFICHAGE-DETAIL.


       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 MOVE PROF-LANGUE TO PROF-LANGUE-1
                    MOVE PROF-CODE   TO PROF-CODE-1 LNK-TEXT
                    CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO PROF-TIME
                    MOVE LNK-USER   TO PROF-USER
                    WRITE PROF-RECORD INVALID 
                        REWRITE PROF-RECORD
                    END-WRITE   
                    MOVE 3 TO DECISION
            WHEN  8 DELETE PROFESSION INVALID CONTINUE
                    END-DELETE
                    INITIALIZE PROF-RECORD
                    MOVE 2 TO DECISION
                    PERFORM AFFICHAGE-DETAIL
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION > 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       DIS-HE-01.
           DISPLAY PROF-LANGUE LINE  4 POSITION 25.
       DIS-HE-02.
           DISPLAY PROF-CODE   LINE  5 POSITION 25.
       DIS-HE-03.
           DISPLAY PROF-NOM(1)  LINE  6 POSITION 20.
       DIS-HE-04.
           DISPLAY PROF-NOM(2)  LINE  7 POSITION 20.
       DIS-HE-END.
           EXIT.

       NEXT-PROF.
           MOVE LANGUE TO PROF-LANGUE.
           CALL "6-PROF" USING LINK-V PROF-RECORD EXC-KEY.

       AFFICHAGE-ECRAN.
           MOVE 22 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE PROFESSION.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
             IF DECISION = 99 
                DISPLAY PROF-USER LINE 24 POSITION 5
                DISPLAY PROF-ST-JOUR  LINE 24 POSITION 15
                DISPLAY PROF-ST-MOIS  LINE 24 POSITION 18
                DISPLAY PROF-ST-ANNEE LINE 24 POSITION 21
                DISPLAY PROF-ST-HEURE LINE 24 POSITION 30
                DISPLAY PROF-ST-MIN   LINE 24 POSITION 33
                GO AVANT-DEC
             END-IF.
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
