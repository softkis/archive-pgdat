      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-P21 FIN D'ESSAI           DU MOIS         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-P21.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  CHOIX-MAX             PIC 99 VALUE 6.
       01  PRECISION             PIC 9 VALUE 0.
       01  S-KEY    PIC 9(4) COMP-1 VALUE 65.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".
           COPY "METIER.REC".
           COPY "CONTRAT.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.
       01  COMPTEUR              PIC 9999 VALUE 0.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".P21".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-P21.
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           INITIALIZE LIN-NUM LIN-IDX LNK-VAL.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-3
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-3
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-3
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM READ-CONTRAT.
           IF  CON-ESSAI-A = LNK-ANNEE
           AND CON-ESSAI-M = LNK-MOIS
              PERFORM FILL-FILES
              PERFORM FILL-LINE
           END-IF.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.


       ENTETE.
      * DONNEES FIRME

           COMPUTE LIN-NUM = 2.
           MOVE 55 TO COL-NUM.
           MOVE LNK-MOIS TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT  TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD  2 TO COL-NUM.
           MOVE LNK-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 4.
           MOVE  4 TO CAR-NUM.
           MOVE  9 TO COL-NUM.
           MOVE  0 TO POINTS DEC-NUM.
           INITIALIZE VH-XX.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 14 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 5.
           MOVE 14 TO COL-NUM.
           MOVE FR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 20 TO COL-NUM.
           MOVE FR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 6.
           MOVE 14 TO COL-NUM.
           MOVE FR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 16 TO COL-NUM.
           MOVE FR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 21 TO COL-NUM.
           MOVE FR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM =  7 .
           MOVE 14 TO COL-NUM.
           MOVE FR-ETAB-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD   1 TO COL-NUM.
           MOVE FR-ETAB-N TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD   1 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD   1 TO COL-NUM.
           MOVE FR-SNOCS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD   1 TO COL-NUM.
           MOVE FR-EXTENS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE  2 TO CAR-NUM.
           MOVE  64 TO COL-NUM.
           ADD 1 TO PAGE-NUMBER.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

      *    DATE EDITION
           COMPUTE LIN-NUM = 4.
           MOVE  64 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  67 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  71 TO COL-NUM.
           MOVE TODAY-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

       FILL-FILES.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER 
           END-IF.
           COMPUTE IDX = LIN-NUM + 3.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              PERFORM READ-FORM
              PERFORM ENTETE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              COMPUTE LIN-NUM = LIN-IDX 
           END-IF.

       FILL-LINE.
      * DONNEES PERSONNE
           ADD 1 TO LIN-NUM.
           MOVE 6 TO CAR-NUM.
           MOVE 7 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.

           MOVE 14 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 55 TO COL-NUM.
           MOVE CON-DEBUT-J TO ALPHA-TEXTE
           PERFORM FILL-FORM
           MOVE CON-DEBUT-M TO ALPHA-TEXTE
           ADD 1 TO COL-NUM
           PERFORM FILL-FORM
           MOVE CON-DEBUT-A TO ALPHA-TEXTE
           ADD 1 TO COL-NUM
           PERFORM FILL-FORM.

           MOVE 68 TO COL-NUM.
           MOVE CON-ESSAI-J TO ALPHA-TEXTE
           PERFORM FILL-FORM
           MOVE CON-ESSAI-M TO ALPHA-TEXTE
           ADD 1 TO COL-NUM
           PERFORM FILL-FORM
           MOVE CON-ESSAI-A TO ALPHA-TEXTE
           ADD 1 TO COL-NUM
           PERFORM FILL-FORM.

           ADD 1 TO LIN-NUM.
           MOVE CAR-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           MOVE COUT-NOM TO ALPHA-TEXTE.
           MOVE 14 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE CAR-METIER TO MET-CODE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           ADD 5 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.

       TRANSMET.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.
           PERFORM END-PROGRAM.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE 12        TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD S-KEY.

           COPY "XDIS.CPY".
           
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1221 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF LIN-NUM > LIN-IDX 
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".

