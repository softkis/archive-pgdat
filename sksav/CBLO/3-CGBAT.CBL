      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-CGBAT IMPRESSION RECAPITULATION ANNUELLE  �
      *  � CONGE BATIMENT                                        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CGBAT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM160.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM160.FDE".
           COPY "TRIPR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "160       ".
       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  MAX-LIGNES            PIC 99 VALUE 2.
       01  PRECISION             PIC 9 VALUE 1.
       01  TIRET-TEXTE           PIC X VALUE "�".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "LIVRE.REC".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".
           COPY "CONGE.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(160) OCCURS 45.

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  IDX-5                 PIC 99 VALUE 0.
       01  CONTROLE              PIC S999V99.
       01  ACTIF-YN              PIC X VALUE "N".

      * champ 17 = contr뱇e mouvements

       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

           COPY "V-VH00.CPY".
       01  SOLDES.
           03 SOLDE-CG OCCURS 5.
              04 SOLDE              PIC S9(6)V99 OCCURS 17.
              04 TESTS              PIC 9999V99.

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".CGB".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.

       01 HE-SEL.
          02 H-S PIC X       OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CGBAT.
       

           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TEST-EXTENSION.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-SORT
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-6.
           ACCEPT ACTIF-YN 
             LINE 13 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.            
           ACCEPT MOIS-FIN
             LINE 20 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           IF ACTIF-YN = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-06.

--     APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-07.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
           OR CAR-CONGE NOT = 8
              GO READ-PERSON-2
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           IF  CHOIX > 0
           AND CHOIX < 90
           AND COUNTER > 0
           AND TRIPR-CHOIX NOT = TEST-EXTENSION
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF CHOIX > 0
              MOVE TRIPR-CHOIX TO TEST-EXTENSION.
           INITIALIZE LAST-PERSON.
           PERFORM RECHERCHE.
           IF CONTROLE > 0
              PERFORM DONNEES-PERSONNE.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
                 IF  ACTIF-YN NOT = "N"
                 AND PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF

              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.


       ENTETE.
           ADD 1 TO PAGE-NUMBER.
           MOVE 4 TO CAR-NUM.
           MOVE 3 TO LIN-NUM.
           MOVE 120 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  6 TO LIN-NUM.
           MOVE 112 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 115 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 118 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           IF COUT-NUMBER NOT = 0
           AND CHOIX = 0
              MOVE 5 TO LIN-NUM
              MOVE 65 TO COL-NUM
              MOVE COUT-NOM TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE COUT-NUMBER TO VH-00 
              MOVE 4 TO CAR-NUM
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM.

           MOVE  3 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           IF STATUT NOT = 0
              MOVE "-" TO ALPHA-TEXTE
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM
              MOVE STAT-NOM TO ALPHA-TEXTE
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           
           MOVE FR-PAYS TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 3 TO LIN-NUM.
           MOVE 65 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE LNK-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           IF CHOIX NOT = 0 
              PERFORM DETAIL-CHOIX.

       DETAIL-CHOIX.
           MOVE  4 TO LIN-NUM.
           MOVE 65 TO COL-NUM.
           MOVE TEST-NUMBER TO LNK-POSITION.
           MOVE TEST-ALPHA  TO LNK-TEXT.
           MOVE CHOIX       TO LNK-NUM.
           CALL "4-CHOIX" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

        DONNEES-PERSONNE.
           PERFORM DIS-HE-01.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           
           COMPUTE IDX = LIN-NUM + (CONTROLE * 2) + 1.
           IF IDX >= 40
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           MOVE REG-PERSON TO LAST-PERSON.
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  3 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           PERFORM LIGNES VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           MOVE  5 TO COL-NUM.
           PERFORM TIRET UNTIL COL-NUM > 153.
           ADD 1 TO LIN-NUM.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       RECHERCHE.
           INITIALIZE CONGE-RECORD SOLDES L-RECORD LNK-SUITE.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD FAKE-KEY.
           PERFORM LOAD-REPORT VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           PERFORM CONGE  VARYING IDX FROM 1 BY 1 UNTIL IDX > MOIS-FIN.
           ADD CONGE-AJUSTE TO SOLDE(1, 2) SOLDE(1, 15).
           ADD CONGE-MONTANT-SOLDE TO SOLDE(5, 1) SOLDE(5, 3) 
           SOLDE(3, 1)  SOLDE(3, 15) SOLDE(5, 15).
           PERFORM LP VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > MOIS-FIN.
           MOVE 0 TO CONTROLE.
           PERFORM TESTS.
           
       LOAD-REPORT.
           MOVE CONGE-IDX(IDX) TO SOLDE(IDX, 1) SOLDE(IDX, 15).

       CONGE.
           ADD CONGE-MOIS(IDX) TO SOLDE(1, 2) SOLDE(1, 15) SOLDE(1, 17).

       LP.
           INITIALIZE L-RECORD.
           MOVE IDX-4 TO L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-MOIS NOT = 0
              PERFORM CALCUL.

       CALCUL.
           COMPUTE IDX-1 = 10 + IDX.
           COMPUTE IDX-2 = 20 + IDX.
           COMPUTE IDX-3 = L-MOIS + 2.
           COMPUTE IDX-5 = L-MOIS + 1.
           ADD L-UNI-CONGE-PLUS TO SOLDE(1, 2) SOLDE(1 15) SOLDE(1, 17) 
           MOVE 0 TO SH-00.
           SUBTRACT L-UNI-CONGE FROM SH-00.
           SUBTRACT L-UNI-CONGE-MIN FROM SH-00.
           ADD SH-00 TO SOLDE(1, IDX-3) SOLDE(1, 15).
           IF SH-00 > 0
              ADD SH-00 TO SOLDE(1, 17)
           ELSE
              SUBTRACT SH-00 FROM SOLDE(1, 17)
           END-IF.
           ADD L-BASE-CONGE TO SOLDE(2, IDX-3) SOLDE(2, 15).
           ADD L-PROV-CONGE-PLUS TO SOLDE(3, IDX-3) SOLDE(3, 15).
           COMPUTE SH-00 = L-DC(20) + L-DC(106) + L-DC(120).
           ADD SH-00 TO SOLDE(4, IDX-3) SOLDE(4, 15).
           COMPUTE SH-00 = SOLDE(3, IDX-3) - SOLDE(4, IDX-3).
           ADD SH-00 TO SOLDE(5, IDX-3) SOLDE(5, 15).
           IF IDX > 1
              ADD SOLDE(5, IDX-5) TO SOLDE(5, IDX-3).

       TESTS.
           COMPUTE SOLDE(1, 16) = SOLDE(1, 15) / CAR-HRS-JOUR
           IF SOLDE(1, 17) NOT = 0
           OR SOLDE(1, 1 ) NOT = 0
              ADD 1 TO CONTROLE
           END-IF.

       LIGNES.
           IF SOLDE(IDX, 1)  NOT = 0
           OR SOLDE(IDX, 2)  NOT = 0
           OR SOLDE(IDX, 3)  NOT = 0
           OR SOLDE(IDX, 15) NOT = 0
           OR SOLDE(IDX, 17) NOT = 0
              PERFORM P-LINE
           END-IF.

       P-LINE.
           MOVE 1 TO COL-NUM.
           PERFORM FILL-FORM.
           PERFORM COLONNES VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 15.
           ADD 1 TO LIN-NUM.
           EVALUATE IDX 
              WHEN 1 MOVE SPACES   TO ALPHA-TEXTE 
              WHEN 2 MOVE "BAS"    TO ALPHA-TEXTE 
              WHEN 3 MOVE "plus %" TO ALPHA-TEXTE 
              WHEN 4 MOVE "minus"  TO ALPHA-TEXTE 
              WHEN 5 MOVE " ="     TO ALPHA-TEXTE 
           END-EVALUATE.
           MOVE 7 TO COL-NUM.
           PERFORM FILL-FORM.
           PERFORM FILL-REP VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 16.
           ADD 1 TO LIN-NUM.

       COLONNES.
           COMPUTE COL-NUM = 12 + IDX-1 * 9.
           MOVE "+" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       FILL-REP.
           IF SOLDE(IDX, IDX-1) NOT = 0
              COMPUTE COL-NUM = 5 + IDX-1 * 9
              MOVE SOLDE(IDX, IDX-1) TO VH-00
              MOVE 2 TO DEC-NUM
              MOVE 4 TO CAR-NUM
              IF IDX-1 = 16
                 ADD 1 TO COL-NUM
              END-IF
              PERFORM FILL-FORM
           END-IF.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

           COPY "XDIS.CPY".
       DIS-HE-06.
           DISPLAY ACTIF-YN LINE 13 POSITION 32.
       DIS-HE-07.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 35.
           MOVE "MO" TO LNK-AREA.
           MOVE 20401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 1351 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L160O" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.


       END-PROGRAM.
           IF COUNTER > 0 
              PERFORM TRANSMET
              MOVE 99 TO LNK-VAL
              CALL "L160O" USING LINK-V FORMULAIRE
           END-IF.
           CANCEL "L160O".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".
           COPY "XSORT.CPY".

