      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-FIRME MODULE GENERAL LECTURE FIRMES       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-FIRME.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FIRME.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FIRME.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN              PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
       01  A-N                   PIC X.
       01  EXC-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V A-N EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FIRME .
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-FIRME.
       
           IF NOT-OPEN = 0
              OPEN I-O FIRME
              MOVE 1 TO NOT-OPEN.

           MOVE FR-RECORD TO FIRME-RECORD.
           
           EVALUATE EXC-KEY 
               WHEN 65
               IF A-N = "N"
                  PERFORM START-1
               ELSE
                  PERFORM START-2
               END-IF
               READ FIRME NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN 66
               IF A-N = "N"
                  PERFORM START-3
               ELSE
                  PERFORM START-4
               END-IF
               READ FIRME PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN 99
                    WRITE FIRME-RECORD INVALID REWRITE FIRME-RECORD
                    END-WRITE
                    EXIT PROGRAM
               WHEN OTHER
                  MOVE FR-RECORD TO FIRME-RECORD
               GO EXIT-3
           END-EVALUATE.

       EXIT-1.
           INITIALIZE FR-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE FIRME-RECORD TO FR-RECORD.
           EXIT PROGRAM.

       EXIT-3.
           READ FIRME NO LOCK INVALID GO EXIT-1
                NOT   INVALID GO EXIT-2.

       START-1.
           START FIRME KEY < FIRME-KEY       INVALID GO EXIT-1.
       START-2.
           START FIRME KEY < FIRME-KEY-ALPHA INVALID GO EXIT-1.
       START-3.
           START FIRME KEY > FIRME-KEY       INVALID GO EXIT-1.
       START-4.
           START FIRME KEY > FIRME-KEY-ALPHA INVALID GO EXIT-1.

