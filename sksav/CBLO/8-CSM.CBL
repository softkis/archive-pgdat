      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-CSM IMPRESSION CONTROLE CODES SALAIRE     �
      *  � RECAPITULATION MENSUELLE                              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-CSM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODPAIE.FC".
           SELECT OPTIONAL TF-CSM ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CODPAIE.FDE".
       FD  TF-CSM
           RECORD CONTAINS 161 CHARACTERS
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TF-FIRME      PIC Z(6).
           02 TF-FIRME-T REDEFINES TF-FIRME PIC X(6).
           02 TF-FILLER-1   PIC X.
           02 TF-ANNEE      PIC Z(5).
           02 TF-ANNEE-T REDEFINES TF-ANNEE PIC X(5).
           02 TF-FILLER-2   PIC X.
           02 TF-MOIS       PIC Z(5).
           02 TF-MOIS-T REDEFINES TF-MOIS PIC X(5).
           02 TF-FILLER-3   PIC X.
           02 TF-PERSON     PIC Z(8).
           02 TF-PERS-T REDEFINES TF-PERSON PIC X(8).
           02 TF-FILLER-4   PIC X.
           02 TF-NOM        PIC X(30).
           02 TF-FILLER-5   PIC X.
           02 TF-COUT       PIC Z(8).
           02 TF-COUT-T REDEFINES TF-COUT PIC X(8).
           02 TF-FILLER-6   PIC X.
           02 TF-POSTE      PIC Z(8).
           02 TF-POSTE-T REDEFINES TF-POSTE PIC X(8).
           02 TF-FILLER-7   PIC X.
           02 TF-DONNEES OCCURS 6.
              04 TF-VAL    PIC Z(8),ZZZZ.
              04 TF-TXT REDEFINES TF-VAL PIC X(13).
              04 TF-FILL   PIC X.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 11.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "CODSAL.REC".
           COPY "PARMOD.REC".

       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.

           COPY "V-VAR.CPY".
       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
        
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  VALEUR                PIC X.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CODPAIE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-CSM.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-SETTINGS TO VALEUR.
           MOVE PARMOD-PROG-NUMBER-2 TO CS-NUMBER.
           MOVE PARMOD-MOIS-DEB TO MOIS-DEBUT.
           MOVE PARMOD-MOIS-FIN TO MOIS-FIN.
           PERFORM APRES-8 THRU APRES-9.
 
           MOVE LNK-SUFFIX TO ANNEE-PAIE.
           OPEN INPUT CODPAIE.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8 THRU 9
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 11    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-PATH
           WHEN 11 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7 
           WHEN  8 PERFORM APRES-8 
           WHEN  9 PERFORM APRES-9
           WHEN 11 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.            
           ACCEPT CS-NUMBER
             LINE 13 POSITION 29 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       AVANT-7.
           ACCEPT VALEUR
             LINE 15 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
                     PERFORM APRES-8 THRU APRES-9
             WHEN 66 ADD      1 TO   MOIS-DEBUT
                     PERFORM APRES-8 THRU APRES-9
             END-EVALUATE.

       AVANT-9.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
                     PERFORM APRES-8 THRU APRES-9
             WHEN 66 ADD      1 TO   MOIS-FIN
                     PERFORM APRES-8 THRU APRES-9
             END-EVALUATE.

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 22 POSITION 35 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

--     APRES-6.
           MOVE 0 TO INPUT-ERROR.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 2 CALL "2-CODSAL" USING LINK-V CS-RECORD
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR
                  CANCEL "2-CODSAL" 
           WHEN OTHER PERFORM NEXT-CS 
           END-EVALUATE.
           IF CS-NOM = SPACES
              MOVE 1 TO INPUT-ERROR
           ELSE       
              PERFORM TEST-CS.
           PERFORM DIS-HE-06.
           IF INPUT-ERROR = 0
              MOVE CS-NUMBER TO PARMOD-PROG-NUMBER-2
           END-IF.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       APRES-7.
           IF VALEUR = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE "N" TO VALEUR
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-07.
           MOVE VALEUR TO PARMOD-SETTINGS.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       APRES-8.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-08.
           PERFORM DIS-HE-09.

       APRES-9.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.
           PERFORM DIS-HE-09.
           MOVE MOIS-DEBUT TO PARMOD-MOIS-DEB.
           MOVE MOIS-FIN   TO PARMOD-MOIS-FIN.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       NEXT-CS.
           MOVE 0 TO INPUT-ERROR.
           CALL "6-CODSAL" USING LINK-V CS-RECORD SAVE-KEY.
           IF CS-NOM NOT = SPACES
              PERFORM TEST-CS
              IF INPUT-ERROR = 1
              AND SAVE-KEY NOT = 13
                 GO NEXT-CS
              END-IF
           END-IF.
           IF LNK-LANGUAGE NOT = "F"
              CALL "6-CODTXT" USING LINK-V CS-RECORD.

       TEST-CS.
           MOVE 0 TO LNK-NUM INPUT-ERROR.
           IF CS-MALADIE = 2
              MOVE 34 TO LNK-NUM.
           IF CS-NOM = SPACES
              MOVE 1 TO LNK-NUM.
           IF LNK-NUM NOT = 0 
              IF LNK-NUM = 1 
                 MOVE "AA" TO LNK-AREA
              ELSE
                 MOVE "SL" TO LNK-AREA
              END-IF
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-NUM.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM START-PERSON
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           PERFORM READ-PERSON THRU READ-EXIT.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           PERFORM RECHERCHE-CSP.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.


       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.


       RECHERCHE-CSP.
           PERFORM DIS-HE-01.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY       TO CSP-FIRME-2.
           MOVE REG-PERSON   TO CSP-PERSON-2.
           MOVE MOIS-DEBUT   TO CSP-MOIS-2.
           MOVE CS-NUMBER    TO CSP-CODE-2.
           START CODPAIE KEY >= CSP-KEY-2 INVALID CONTINUE
               NOT INVALID PERFORM READ-CSP THRU READ-CSP-END.

       READ-CSP.
           READ CODPAIE NEXT AT END
               GO READ-CSP-END
           END-READ.
           IF FR-KEY       NOT = CSP-FIRME
           OR REG-PERSON   NOT = CSP-PERSON
           OR MOIS-FIN     < CSP-MOIS 
              GO READ-CSP-END
           END-IF.
           IF CS-NUMBER    NOT = CSP-CODE
           OR CSP-SUITE    NOT = 0
              GO READ-CSP
           END-IF.

           IF NOT-OPEN = 0 
              OPEN OUTPUT TF-CSM
              INITIALIZE TF-RECORD
              INITIALIZE TF-RECORD
              INSPECT TF-RECORD REPLACING ALL " " BY ";"
              EVALUATE LNK-LANGUAGE
                 WHEN "F" PERFORM ENTETE-F
                 WHEN "D" PERFORM ENTETE-D
                 WHEN "E" PERFORM ENTETE-E
              END-EVALUATE

              PERFORM FILL-DESCR VARYING IDX FROM 1 BY 1 UNTIL IDX > 6
              WRITE TF-RECORD
              ADD 1 TO NOT-OPEN
           END-IF.
           MOVE FR-KEY     TO TF-FIRME.
           MOVE LNK-ANNEE  TO TF-ANNEE.
           MOVE CSP-MOIS   TO TF-MOIS.
           MOVE REG-PERSON TO TF-PERSON.
           MOVE PR-NOM     TO TF-NOM.
           MOVE CAR-COUT   TO TF-COUT.
           MOVE CSP-POSTE  TO TF-POSTE.
           IF VALEUR = "N"
              MOVE 0 TO CSP-UNITAIRE CSP-TOTAL CSP-DONNEE-2
           END-IF.
           PERFORM FILL-CS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.
           MOVE ";" TO TF-FILLER-1 
                       TF-FILLER-2
                       TF-FILLER-3
                       TF-FILLER-4
                       TF-FILLER-5
                       TF-FILLER-6
                       TF-FILLER-7.
           WRITE TF-RECORD.
           GO READ-CSP.
       READ-CSP-END.
           EXIT.

       FILL-CS.
           MOVE CSP-DONNEE(IDX) TO TF-VAL(IDX).
           MOVE ";" TO TF-FILL(IDX).

       FILL-DESCR.
           MOVE CS-DESCRIPTION(IDX) TO TF-TXT(IDX).
           MOVE ";" TO TF-FILL(IDX).


       ENTETE-F.
           MOVE "FIRME"     TO TF-FIRME-T.
           MOVE "ANNEE"     TO TF-ANNEE-T.
           MOVE "MOIS"      TO TF-MOIS-T.
           MOVE "PERSONNE"  TO TF-PERS-T.
           MOVE "NOM"       TO TF-NOM.
           MOVE "COUT"      TO TF-COUT-T.
           MOVE "POSTE"     TO TF-POSTE-T.

       ENTETE-D.
           MOVE "FIRMA"     TO TF-FIRME-T.
           MOVE "JAHR"      TO TF-ANNEE-T.
           MOVE "MONAT"     TO TF-MOIS-T.
           MOVE "PERSON"    TO TF-PERS-T.
           MOVE "NAME"      TO TF-NOM.
           MOVE "KOSTENST"  TO TF-COUT-T.
           MOVE "KOSTENTR"  TO TF-POSTE-T.
       ENTETE-E.
           MOVE "FIRM "     TO TF-FIRME-T.
           MOVE "YEAR"      TO TF-ANNEE-T.
           MOVE "MONTH"     TO TF-MOIS-T.
           MOVE "PERSON"    TO TF-PERS-T.
           MOVE "NAME"      TO TF-NOM.
           MOVE "COSTCENT"  TO TF-COUT-T.
           MOVE "COSTUNIT"  TO TF-POSTE-T.



           COPY "XDIS.CPY".
       DIS-HE-06.
           CALL "6-CODSAL" USING LINK-V CS-RECORD FAKE-KEY.
           IF LNK-LANGUAGE NOT = "F"
              CALL "6-CODTXT" USING LINK-V CS-RECORD.
           MOVE CS-NUMBER TO HE-Z4.
           DISPLAY HE-Z4  LINE 13 POSITION 29.
           DISPLAY CS-NOM LINE 13 POSITION 35.
       DIS-HE-07.
           DISPLAY VALEUR LINE 15 POSITION 32.
       DIS-HE-08.
           DISPLAY MOIS-DEBUT LINE 19 POSITION 31.
           MOVE MOIS-DEBUT TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-09.
           DISPLAY MOIS-FIN LINE 20 POSITION 31.
           MOVE MOIS-FIN TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-10.
           DISPLAY PARMOD-PATH  LINE 22 POSITION 35.
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 604 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


