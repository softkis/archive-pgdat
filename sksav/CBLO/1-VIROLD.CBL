      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-VIROLD REPRISE ANCIENS VIREMENTS          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-VIROLD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "VIREMENT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "VIREMENT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "BANQF.REC".
           COPY "BANQP.REC".

       01  CHOIX-MAX             PIC 99 VALUE 4. 
       01  PRECISION             PIC 9 VALUE 0.

       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIR          PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6).
           02 HE-Z6Z2 PIC Z(6),ZZ.
           02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               VIREMENT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-VIROLD.

           MOVE 66 TO SAVE-KEY.
           INITIALIZE BQF-RECORD.
           MOVE LNK-SUFFIX TO ANNEE-VIR.
           OPEN I-O   VIREMENT.
           PERFORM NEXT-BANQF.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 3  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0000000025 TO EXC-KFR (1).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           INITIALIZE VIR-RECORD.
           ACCEPT BQF-CODE
             LINE 4 POSITION 25 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT REG-PERSON 
             LINE  6 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           ACCEPT REG-MATCHCODE
             LINE  6 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4.
           MOVE SH-00 TO HE-Z6Z2.
           MOVE 0 TO IDX-3.
           INSPECT HE-Z6Z2 TALLYING IDX-3 FOR ALL " ".
           ADD 1 TO IDX-3.
           IF IDX-3 > 5
              MOVE 3 TO IDX-3
           END-IF.
           ACCEPT HE-Z6Z2
             LINE 16 POSITION 30 SIZE 9
             TAB UPDATE NO BEEP CURSOR IDX-3
             ON EXCEPTION EXC-KEY CONTINUE.
           INSPECT HE-Z6Z2 REPLACING ALL "." BY ","
           MOVE HE-Z6Z2 TO SH-00.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-1.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-BANQF" USING LINK-V BQF-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER MOVE EXC-KEY TO SAVE-KEY
                      PERFORM NEXT-BANQF
           END-EVALUATE.
           IF BQF-CODE = SPACES 
           OR BQF-KEY = SPACES 
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-01.

       APRES-2.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 0 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              IF PRES-TOT(LNK-MOIS) = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           IF LNK-COMPETENCE < REG-COMPETENCE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM READ-VIR.
           PERFORM AFFICHAGE-DETAIL.

       APRES-3.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN  13 CONTINUE
           WHEN  OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF CAR-IN-ACTIF = 1 
              MOVE "SL" TO LNK-AREA
              MOVE 6 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF LNK-COMPETENCE < REG-COMPETENCE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-01.
           MOVE 0 TO LNK-SUITE.
           PERFORM READ-VIR.
           PERFORM AFFICHAGE-DETAIL.
           IF REG-PERSON = 0 
              MOVE 1 TO INPUT-ERROR.

       APRES-4.
           IF SH-00 = 0 MOVE 1 TO INPUT-ERROR.
           EVALUATE EXC-KEY 
            WHEN 13 PERFORM CREATION
                    MOVE 1 TO INDICE-ZONE
           END-EVALUATE.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.

       CREATION.
           INITIALIZE BQP-RECORD.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD FAKE-KEY.
           IF BQP-BANQUE = SPACES
               MOVE "C" TO BQP-BANQUE
           END-IF.
           PERFORM MAKE-KEY.
           COMPUTE VIR-A-PAYER = SH-00.
           PERFORM WRITE-VIREMENT.

       WRITE-VIREMENT.
           INITIALIZE VIR-DATE-EDITION VIR-DATE-VIREMENT.
           MOVE BQF-COMPTE TO VIR-COMPTE-D.
           MOVE BQP-COMPTE TO VIR-COMPTE-C.
           IF LNK-SQL = "Y" 
              CALL "9-VIREM" USING LINK-V VIR-RECORD WR-KEY 
           END-IF.
           WRITE VIR-RECORD INVALID 
               REWRITE VIR-RECORD
           END-WRITE.

       MAKE-KEY.
           INITIALIZE VIR-RECORD.
           MOVE FR-KEY TO VIR-FIRME    VIR-FIRME-B 
                          VIR-FIRME-S  VIR-FIRME-N.
           MOVE REG-PERSON TO VIR-PERSON   VIR-PERSON-B
                              VIR-PERSON-S VIR-PERSON-N.
           MOVE LNK-MOIS TO VIR-MOIS VIR-MOIS-B VIR-MOIS-S VIR-MOIS-N.
           MOVE 1 TO VIR-SUITE VIR-SUITE-B VIR-SUITE-S VIR-SUITE-N.
           MOVE BQF-CODE TO VIR-BANQUE-D.
           MOVE BQP-CODE TO VIR-BANQUE-C.

        READ-VIR.
           PERFORM MAKE-KEY.
           READ VIREMENT INVALID INITIALIZE VIR-REC-DET.
           MOVE VIR-A-PAYER TO SH-00.
           PERFORM DIS-HE-04.


       DIS-HE-01.
           DISPLAY BQF-CODE   LINE 4 POSITION 25.
           DISPLAY BQF-COMPTE LINE 4 POSITION 40.
       DIS-HE-02.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 6 POSITION 47 SIZE 33.
       DIS-HE-03.
           DISPLAY REG-MATCHCODE LINE 6 POSITION 33.
       DIS-HE-04.
           MOVE SH-00 TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 16 POSITION 30.
       DIS-HE-END.
           EXIT.

       NEXT-BANQF.
           CALL "6-BANQF" USING LINK-V BQF-RECORD SAVE-KEY.

       NEXT-BANQP.
           CALL "6-BANQP" USING LINK-V BQP-RECORD SAVE-KEY.


       AFFICHAGE-ECRAN.
           MOVE 584 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE VIREMENT.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

           