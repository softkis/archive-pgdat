      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-TDATE MODULE CONTROLE DATE                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-TDATE.

       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.

           COPY "MOIS-JRS.CPY".
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       77  HELP-2    PIC 99.
       77  HELP-3    PIC 9(4).


       LINKAGE SECTION.
      *----------------

       01  DATE-.
           02 ANNEE       PIC 9999.
           02 MOIS        PIC 99.
           02 JOUR        PIC 99.
       01  LNK-NUM        PIC 9(6).
 
       PROCEDURE DIVISION USING DATE- LNK-NUM.

       START-CALC    SECTION.
             
       START-PROGRAMME-0-TDATE.
                     
           MOVE 0 TO LNK-NUM.
           DIVIDE ANNEE BY 4 GIVING HELP-3 REMAINDER HELP-2.
                             
           MOVE 28 TO MOIS-02.
           IF HELP-2 = 0 
              ADD 1 TO MOIS-02.
        

           IF ANNEE < 1900
              MOVE 1 TO LNK-NUM
           END-IF.
           IF MOIS = 0
           OR MOIS > 12
              ADD 2 TO LNK-NUM
           END-IF.
           IF JOUR = 0
              ADD 4 TO LNK-NUM
           END-IF.
           IF LNK-NUM > 0
              GO END-PROGRAM
           END-IF.

           IF JOUR > MOIS-JRS(MOIS)
              MOVE 8 TO LNK-NUM
           END-IF.
           GO END-PROGRAM.

       END-PROGRAM.
           EXIT PROGRAM.

