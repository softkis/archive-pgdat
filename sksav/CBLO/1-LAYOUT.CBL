      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-LAYOUT  GESTION DES LAYOUTS               �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-LAYOUT.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "LAYOUT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "LAYOUT.FDE".

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 6.  

       01  LAY-NAME.
           02 LAY-BODY           PIC X(8) VALUE "S-LAYOUT".
           02 LAY-EXT            PIC X(4) VALUE SPACES.
       01   ECR-DISPLAY.
            02 HE-03 PIC ZZZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON LAYOUT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-LAYOUT.

           MOVE MENU-EXTENSION-2 TO LAY-EXT.
           INITIALIZE LAY-RECORD. 
           MOVE LNK-LANGUAGE TO SAVE-LANGUAGE LAY-LANGUAGE.
           MOVE "FP" TO LAY-AREA.
           OPEN I-O LAYOUT.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0100000000 TO EXC-KFR(1)
           WHEN 3     MOVE 0129002200 TO EXC-KFR(1)
                      MOVE 2100200000 TO EXC-KFR(2)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0100000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 23 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  6 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT LAY-LANGUAGE
             LINE  4 POSITION 30 SIZE 1 
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT LAY-AREA 
             LINE  5 POSITION 30 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.            
           ACCEPT LAY-NUMBER  
             LINE  6 POSITION 25 SIZE  4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4.            
           ACCEPT LAY-DESCR-1
             LINE 10 POSITION 15 SIZE 65
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-5.            
           ACCEPT LAY-DESCR-2
             LINE 13 POSITION 15 SIZE 65
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE LAY-LANGUAGE 
                WHEN "F"   MOVE 0 TO INPUT-ERROR 
                WHEN "D"   MOVE 0 TO INPUT-ERROR 
                WHEN "E"   MOVE 0 TO INPUT-ERROR 
                WHEN OTHER MOVE 1 TO INPUT-ERROR .
       
       APRES-2.
           IF EXC-KEY = 1
              MOVE 1003001 TO LNK-VAL
              PERFORM HELP-SCREEN
              MOVE 1 TO INPUT-ERROR
           END-IF.
           
       APRES-3.
           MOVE LAY-AREA TO LNK-AREA.
           MOVE LAY-LANGUAGE TO LNK-LANGUAGE.
           EVALUATE EXC-KEY
           WHEN   4 PERFORM COPY-LAYOUT
           WHEN  65 THRU 66 PERFORM NEXT-LAYOUT
           WHEN   2 MOVE LAY-AREA TO LNK-AREA
                    MOVE LAY-LANGUAGE TO LNK-LANGUAGE
                    CALL "2-LAYOUT" USING LINK-V LAY-RECORD
                    MOVE SAVE-LANGUAGE TO LNK-LANGUAGE
                    PERFORM AFFICHAGE-ECRAN 
            WHEN  8 DELETE LAYOUT  INVALID CONTINUE
                    END-DELETE.
           IF EXC-KEY NOT = 4
              PERFORM READ-LAYOUT.
           PERFORM DIS-HE-01 THRU DIS-HE-04.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 WRITE LAY-RECORD INVALID 
                        REWRITE LAY-RECORD
                    END-WRITE   
                    MOVE 4 TO DECISION
                    PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN  8 DELETE LAYOUT  INVALID CONTINUE
                    END-DELETE
                    INITIALIZE LAY-NUMBER LAY-DESCRIPTION
                    MOVE 4 TO DECISION
                   PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

           
       COPY-LAYOUT.   
           MOVE LAY-NUMBER TO IDX-2.
           ACCEPT LAY-NUMBER
             LINE  6 POSITION 60 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           READ LAYOUT INVALID CONTINUE
                NOT INVALID
                MOVE IDX-2 TO LAY-NUMBER
                WRITE LAY-RECORD INVALID CONTINUE END-WRITE
                END-READ.


       READ-LAYOUT.
           MOVE EXC-KEY TO SAVE-KEY.
           MOVE 13 TO EXC-KEY.
           PERFORM NEXT-LAYOUT.
           MOVE SAVE-KEY TO EXC-KEY.

       NEXT-LAYOUT.
           MOVE LAY-LANGUAGE TO LNK-LANGUAGE.
           MOVE LAY-AREA TO LNK-AREA.
           CALL "6-LAYOUT" USING LINK-V LAY-RECORD EXC-KEY.
           MOVE SAVE-LANGUAGE TO LNK-LANGUAGE.

       NEXT-NUMBER .
           MOVE 0 TO NOT-FOUND.
           START LAYOUT KEY > LAY-KEY INVALID KEY
                INITIALIZE LAY-NUMBER LAY-DESCRIPTION NOT INVALID 
                PERFORM NEXT-FNUMBER-1 UNTIL NOT-FOUND = 1.
           INITIALIZE LAY-DESCRIPTION.
           ADD 1 TO LAY-NUMBER.
           MOVE 3 TO INDICE-ZONE.
                
       NEXT-FNUMBER-1.
           READ LAYOUT NEXT AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
                
       DIS-HE-01.
           DISPLAY LAY-LANGUAGE LINE 4 POSITION 30.
       DIS-HE-02.
           DISPLAY LAY-AREA LINE 5 POSITION 30.
       DIS-HE-03.
           MOVE LAY-NUMBER TO HE-03.
           DISPLAY HE-03  LINE  6 POSITION 25.
       DIS-HE-04.
           DISPLAY LAY-DESCR-1 LINE 10 POSITION 15.
       DIS-HE-05.
           DISPLAY LAY-DESCR-2 LINE 13 POSITION 15.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 13 TO LNK-VAL.
           MOVE "   " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-04.

       END-PROGRAM.
           CLOSE LAYOUT.
           CANCEL "2-LAYOUT".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
