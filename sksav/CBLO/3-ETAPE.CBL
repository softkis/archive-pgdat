      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-ETAPE IMPRESSION LISTE PERSONNES          �
      *  � FICHE ETAPE PERSONNELLE                               �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-ETAPE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "TRIPR.FC".
           COPY "ETAPE.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".
           COPY "TRIPR.FDE".
           COPY "ETAPE.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  MAX-LIGNES            PIC 9 VALUE 2.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".
           COPY "BAREME.REC".
           COPY "BARDEF.REC".
           COPY "POINTS.REC".
           COPY "INDEX.REC".
           COPY "V-BASES.REC".

       01  SAL-ACT               PIC 9(8)V9(4).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02  SAL-ACT-A         PIC 9(10).
           02  SAL-ACT-B         PIC 99.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".ETP".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR
               ETAPE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-ETAPE.
       
           MOVE LNK-ANNEE  TO SAVE-ANNEE.
           MOVE LNK-MOIS   TO SAVE-MOIS.
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           OPEN INPUT ETAPE.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TEST-EXTENSION.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.
      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
       TRAITEMENT-ECRAN.
      *----------------- 
           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-SORT
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-3
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-3
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-3
           END-IF.
           PERFORM READ-CONTRAT.
           IF  CON-FIN-A = LNK-ANNEE
           AND CON-FIN-M < LNK-MOIS
              GO READ-PERSON-3
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM START-ETP.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.


       DETAIL-CHOIX.
           MOVE  4 TO LIN-NUM.
           MOVE 65 TO COL-NUM.
           MOVE TEST-NUMBER TO LNK-POSITION.
           MOVE TEST-ALPHA  TO LNK-TEXT.
           MOVE CHOIX       TO LNK-NUM.
           CALL "4-CHOIX" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
       START-ETP.
           INITIALIZE ETP-RECORD.
           MOVE FR-KEY       TO ETP-FIRME.
           MOVE REG-PERSON   TO ETP-PERSON.
           MOVE REG-ANCIEN-A TO ETP-ANNEE.
           MOVE REG-ANCIEN-M TO ETP-MOIS.
           START ETAPE KEY >= ETP-KEY INVALID CONTINUE
                NOT INVALID PERFORM NEXT-ETP THRU NEXT-ETP-END.

       NEXT-ETP.
           READ ETAPE NEXT NO LOCK AT END 
                GO NEXT-ETP-END
           END-READ.
           IF FR-KEY     NOT = ETP-FIRME
           OR REG-PERSON NOT = ETP-PERSON
              GO NEXT-ETP-END.
           MOVE ETP-ANNEE TO INDEX-ANNEE.
           MOVE ETP-MOIS  TO INDEX-MOIS.
           CALL "6-INDEX" USING LINK-V INDEX-RECORD NUL-KEY.
           MOVE ETP-BAREME TO BAR-BAREME BDF-CODE.
           MOVE ETP-GRADE  TO BAR-GRADE.
           MOVE ETP-ANNEE  TO BAR-ANNEE INDEX-ANNEE.
           MOVE ETP-MOIS   TO BAR-MOIS  INDEX-MOIS.
           CALL "6-BARDEF" USING LINK-V BDF-RECORD FAKE-KEY.
           CALL "6-BAREME" USING LINK-V BAR-RECORD NUL-KEY.
           CALL "6-INDEX"  USING LINK-V INDEX-RECORD NUL-KEY.
           PERFORM FILL-FILES.
           GO NEXT-ETP.
       NEXT-ETP-END.
           PERFORM TRANSMET.
           MOVE 0 TO LIN-NUM.

       ENTETE.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           MOVE 2 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD  3 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.

           MOVE FR-MAISON TO ALPHA-TEXTE.
           MOVE 3 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           MOVE 4 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.


      * DONNEES PERSONNE
           MOVE 06 TO LIN-NUM.
           MOVE 7  TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           IF PR-NOM-JF > SPACES
              MOVE PR-NOM-JF TO ALPHA-TEXTE
              ADD 1 TO COL-NUM
              PERFORM FILL-FORM.

           MOVE PR-PRENOM TO ALPHA-TEXTE.
           ADD  2 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  6 TO CAR-NUM.
           ADD   3 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.

           MOVE 67 TO COL-NUM.
           MOVE PR-NAISS-J TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 70 TO COL-NUM.
           MOVE PR-NAISS-M TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 73 TO COL-NUM.
           MOVE PR-NAISS-A TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 7 TO LIN-NUM.
           MOVE 7 TO COL-NUM.
           MOVE PR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE PR-RUE TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 67 TO COL-NUM.
           MOVE CON-DEBUT-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 70 TO COL-NUM.
           MOVE CON-DEBUT-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 73 TO COL-NUM.
           MOVE CON-DEBUT-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.

           MOVE 8 TO LIN-NUM.
           MOVE 7 TO COL-NUM.
           MOVE PR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE PR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE PR-LOCALITE TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.

           IF CON-FIN-A > 0
              MOVE 67 TO COL-NUM
              MOVE CON-FIN-J TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE 70 TO COL-NUM
              MOVE CON-FIN-M TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE 73 TO COL-NUM
              MOVE CON-FIN-A TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.

       FILL-FILES.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE IDX = LIN-NUM + 3.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              PERFORM PAGE-DATE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.

           MOVE 4 TO COL-NUM.
           MOVE 2 TO CAR-NUM.
           MOVE ETP-MOIS TO VH-00.
           PERFORM FILL-FORM.

           MOVE 7 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE ETP-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE BDF-CODE TO ALPHA-TEXTE.
           MOVE 12 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE ETP-GRADE TO VH-00.
           MOVE  3 TO CAR-NUM.
           MOVE 28 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE BAR-DESCRIPTION TO ALPHA-TEXTE.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE ETP-ECHELON TO VH-00.
           MOVE  3 TO CAR-NUM.
           MOVE 42 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE ETP-POURCENT TO VH-00.
           MOVE  3 TO CAR-NUM.
           MOVE 46 TO COL-NUM.
           PERFORM FILL-FORM.

           INITIALIZE SAL-ACT.
           IF BDF-POINTS > 0
              IF ETP-ECHELON > 0
                 MOVE BAR-POINTS(ETP-ECHELON) TO VH-00
                 MOVE  4 TO CAR-NUM
                 MOVE 50 TO COL-NUM
                 PERFORM FILL-FORM
              END-IF
           ELSE
              MOVE BAR-ECHELON-I100(ETP-ECHELON) TO VH-00
              MOVE  3 TO CAR-NUM
              MOVE  4 TO DEC-NUM
              MOVE 55 TO COL-NUM
              PERFORM FILL-FORM.

           IF BDF-POINTS > 0
              INITIALIZE PTS-RECORD
              MOVE ETP-ANNEE TO PTS-ANNEE
              MOVE ETP-MOIS  TO PTS-MOIS
              CALL "6-POINT" USING LINK-V PTS-RECORD
              MOVE PTS-I100(BDF-POINTS) TO VH-00
              MOVE  4 TO DEC-NUM
              MOVE  3 TO CAR-NUM
              MOVE 55 TO COL-NUM
              PERFORM FILL-FORM.

           MOVE INDEX-TAUX TO VH-00.
           MOVE  2 TO DEC-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE 65 TO COL-NUM.
           PERFORM FILL-FORM.

           IF BDF-POINTS > 0
              COMPUTE SAL-ACT = BAR-POINTS(ETP-ECHELON) 
                              * PTS-I100(BDF-POINTS)
                              * INDEX-FACT + ,00009
           ELSE
              COMPUTE SAL-ACT = BAR-ECHELON-I100(ETP-ECHELON) 
                              * INDEX-FACT + ,00009
           END-IF.
           MOVE 0 TO SAL-ACT-B.
           MOVE SAL-ACT TO VH-00.
           MOVE  5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 72 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.

       PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.

           MOVE  2 TO CAR-NUM.
           MOVE  4 TO LIN-NUM.
           MOVE 67 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 70 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 73 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.
           


       ENTREE-SORTIE.
           IF PRES-ANNEE = 0 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1232 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           IF CHOIX NOT = 0 
              PERFORM DETAIL-CHOIX.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.

       END-PROGRAM.
           MOVE SAVE-ANNEE TO LNK-ANNEE.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           CLOSE ETAPE.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".
           COPY "XSORT.CPY".

