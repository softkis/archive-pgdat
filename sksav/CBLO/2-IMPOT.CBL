      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-IMPOT CONTROLE + MODIFICATION IMPOT       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  2-IMPOT.

       ENVIRONMENT DIVISION.
      *袴袴袴袴袴袴袴袴袴袴
       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "IMPOT.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.

           COPY "IMPOT.FDE".
       FD  TF-TRANS
           RECORD VARYING DEPENDING ON IDX-COL
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 130 DEPENDING IDX-COL.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
       01  IDX-COL               PIC 999 VALUE 80.

       01  TXT-RECORD.
              03 TXT-FIRME  PIC X(6) VALUE "FIRME".
              03 TXT-DELIM0 PIC X VALUE ";".
              03 TXT-ANNEE  PIC XXXXX VALUE "ANNEE".
              03 TXT-DELIM4 PIC X VALUE ";".
              03 TXT-MOIS   PIC X(5) VALUE "MOIS ".
              03 TXT-DELIM4 PIC X VALUE ";".
              03 TXT-SUITE  PIC X(5) VALUE "SUITE".
              03 TXT-DELIM4 PIC X VALUE ";".
              03 TXT-EDITION PIC X(8) VALUE "EDITION".
              03 TXT-DELIM1  PIC X VALUE ";".
              03 TXT-VIREMENT PIC X(8) VALUE "VIREMENT".
              03 TXT-DELIM1  PIC X VALUE ";".
              03 TXT-MONTANT  PIC X(11) VALUE "IMPOT   ".
              03 TXT-DELIM1  PIC X VALUE ";".
              03 TXT-DECOMPTE PIC X(9) VALUE "DECOMPTE".
              03 TXT-DELIM1  PIC X VALUE ";".
              03 TXT-BANQUE  PIC X(10) VALUE "BANQUE".
              03 TXT-DELIM1  PIC X VALUE ";".
                                   
       01  TFT-RECORD.
              03 TFT-FIRME    PIC Z(6).
              03 TFT-DELIM0   PIC X VALUE ";".
              03 TFT-ANNEE    PIC ZZZZZ.
              03 TFT-DELIM4   PIC X VALUE ";".
              03 TFT-MOIS     PIC Z(5).
              03 TFT-DELIM4   PIC X VALUE ";".
              03 TFT-SUITE    PIC Z(5).
              03 TFT-DELIM4   PIC X VALUE ";".
              03 TFT-EDITION  PIC 9(8).
              03 TFT-DELIM1   PIC X VALUE ";".
              03 TFT-VIREMENT PIC 9(8).
              03 TFT-DELIM1   PIC X VALUE ";".
              03 TFT-MONTANT  PIC -Z(7),ZZ.
              03 TFT-DELIM1   PIC X VALUE ";".
              03 TFT-DECOMPTE PIC -Z(5),ZZ.
              03 TFT-DELIM1   PIC X VALUE ";".
              03 TFT-BANQUE   PIC X(10).
              03 TFT-DELIM1   PIC X VALUE ";".
                                   
           COPY "V-VAR.CPY".
           COPY "PARMOD.REC".

       01  IMPOT-IDX             PIC 99.
       01  CHOIX                 PIC X.
       01  CUMUL                 PIC S9(6)V99.
       
       01  IMPOT-NAME.
           02 FILLER             PIC X(8) VALUE "S-IMPOT.".
           02 ANNEE-IMPOT       PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ BLANK WHEN ZERO.
           02 HE-Z10 PIC Z(6),ZZ- BLANK WHEN ZERO.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC 99.

       01  HE-VIR.
           02 H-V OCCURS 20.
             03 HE-SUITE              PIC 99.
             03 HE-IMPOT-MOIS         PIC 99.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON IMPOT.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-IMPOT .
       
           MOVE LNK-SUFFIX TO ANNEE-IMPOT.
           OPEN I-O IMPOT.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY         TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           PERFORM AFFICHAGE-ECRAN .
           PERFORM TOTAL-IMPOT THRU TOTAL-IMPOT-END.
           ACCEPT ACTION
             LINE  4 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM END-PROGRAM.

       AVANT-PATH.
           MOVE "AA" TO LNK-AREA.
           MOVE 32 TO LNK-NUM.
           MOVE "L" TO LNK-LOW.
           PERFORM DISPLAY-MESSAGE.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 24 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           MOVE FR-KEY TO IMPOT-FIRME.
           MOVE HE-IMPOT-MOIS(IMPOT-IDX)  TO IMPOT-MOIS .
           MOVE HE-SUITE(IMPOT-IDX) TO IMPOT-SUITE .
           EVALUATE EXC-KEY 
           WHEN  4 THRU 6
               READ IMPOT INVALID CONTINUE
                   NOT INVALID
                       IF EXC-KEY = 4
                          INITIALIZE IMPOT-DATE-EDITION 
                       END-IF
                       IF EXC-KEY = 6
                          INITIALIZE IMPOT-DATE-VIREMENT 
                       END-IF
                       IF LNK-SQL = "Y" 
                         CALL "9-IMPOT" USING LINK-V IMPOT-RECORD WR-KEY
                       END-IF
                       REWRITE IMPOT-RECORD INVALID CONTINUE
                       END-REWRITE   
               PERFORM DIS-DATES
               END-READ
           WHEN  8 DELETE IMPOT INVALID CONTINUE END-DELETE
                   IF LNK-SQL = "Y" 
                      CALL "9-IMPOT" USING LINK-V IMPOT-RECORD DEL-KEY
                   END-IF
           END-EVALUATE.

      *    HISTORIQUE DES JOURNAUX.
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       TOTAL-IMPOT.
           INITIALIZE IMPOT-IDX HE-VIR CUMUL IMPOT-RECORD EXC-KEY.

           MOVE 4 TO LIN-IDX.
           MOVE FR-KEY TO IMPOT-FIRME.
           START IMPOT KEY >= IMPOT-KEY INVALID  
               MOVE 5 TO LIN-IDX
               PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
               BY 1 UNTIL LIN-IDX > 24
               GO TOTAL-IMPOT-END.
           PERFORM READ-IMPOT THRU READ-IMPOT-END.
           IF EXC-KEY = 8
              GO TOTAL-IMPOT.
           PERFORM INTERRUPT THRU INTERRUPT-END.
      *    IF EXC-KEY = 8
              GO TOTAL-IMPOT.
              
       TOTAL-IMPOT-END.
           
           EXIT.

       READ-IMPOT.
           READ IMPOT NEXT AT END 
                GO READ-IMPOT-END.
           IF FR-KEY NOT = IMPOT-FIRME
              GO READ-IMPOT-END.
           ADD 1 TO IMPOT-IDX.
           PERFORM DIS-HIS-LIGNE.
           IF LIN-IDX > 22
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 8
                 GO READ-IMPOT-END
              END-IF
              IF EXC-KEY = 13
                 INITIALIZE IMPOT-IDX HE-VIR
                 PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM 6
                 BY 1 UNTIL LIN-IDX > 24
                 MOVE 4 TO LIN-IDX
                 GO READ-IMPOT
              END-IF
           END-IF.
           START IMPOT KEY > IMPOT-KEY INVALID  
              GO READ-IMPOT-END.
           GO READ-IMPOT.
       READ-IMPOT-END.
           IF EXC-KEY NOT = 52
              ADD 1 TO LIN-IDX
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 24.


       INTERRUPT.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000092 TO EXC-KFR(2).
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0000530000 TO EXC-KFR(11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.

           ACCEPT CHOIX 
           LINE  3 POSITION 32 SIZE 1
           TAB UPDATE NO BEEP CURSOR 1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 10 PERFORM TABLE-IMPOT.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 53
              MOVE 1 TO IMPOT-IDX
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              IF EXC-KEY NOT = 8
                 GO INTERRUPT
              END-IF
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IMPOT-IDX.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.

       DIS-HIS-LIGNE.
           ADD 1 TO LIN-IDX.
           MOVE IMPOT-SUITE  TO HE-Z2 HE-SUITE(IMPOT-IDX).
           DISPLAY HE-Z2   LINE LIN-IDX POSITION  2.
           MOVE IMPOT-MOIS    TO HE-Z2 HE-IMPOT-MOIS(IMPOT-IDX).
           DISPLAY HE-Z2   LINE LIN-IDX POSITION 5.
           PERFORM DIS-DATES.
           MOVE IMPOT-A-PAYER TO HE-Z10.
           ADD  IMPOT-A-PAYER TO CUMUL.
           DISPLAY HE-Z10 LINE LIN-IDX POSITION 37.
           MOVE IMPOT-DECOMPTE TO HE-Z10.
           SUBTRACT IMPOT-DECOMPTE FROM CUMUL.
           DISPLAY HE-Z10 LINE LIN-IDX POSITION 49.
           MOVE CUMUL TO HE-Z10.
           DISPLAY HE-Z10 LINE LIN-IDX POSITION 61.
           DISPLAY IMPOT-BANQUE-D LINE LIN-IDX POSITION 72 SIZE 3 LOW.
           DISPLAY IMPOT-BANQUE-C LINE LIN-IDX POSITION 76 SIZE 4.


       DIS-DATES.
           MOVE IMPOT-MAJ-J   TO HE-JJ.
           MOVE IMPOT-MAJ-M   TO HE-MM.
           MOVE IMPOT-MAJ-A   TO HE-AA.
           DISPLAY HE-DATE LINE LIN-IDX POSITION 28.
           COMPUTE LNK-NUM = IMPOT-MAJ-M + 200.
           MOVE "MO" TO LNK-AREA.
           MOVE LIN-IDX TO LNK-LINE.
           MOVE 30      TO LNK-COL.
           MOVE 3       TO LNK-SIZE.
           MOVE "L"     TO LNK-LOW.
           CALL "0-DMESS" USING LINK-V.

           MOVE IMPOT-EDIT-J  TO HE-JJ.
           MOVE IMPOT-EDIT-M  TO HE-MM.
           MOVE IMPOT-EDIT-A  TO HE-AA.
           DISPLAY HE-DATE LINE LIN-IDX POSITION 8.
           COMPUTE LNK-NUM = IMPOT-EDIT-M + 200.
           MOVE "MO" TO LNK-AREA.
           MOVE LIN-IDX TO LNK-LINE.
           MOVE 10      TO LNK-COL.
           MOVE 3       TO LNK-SIZE.
           MOVE "L"     TO LNK-LOW.
           CALL "0-DMESS" USING LINK-V.
           MOVE IMPOT-VIR-J   TO HE-JJ.
           MOVE IMPOT-VIR-M   TO HE-MM.
           MOVE IMPOT-VIR-A   TO HE-AA.
           DISPLAY HE-DATE LINE LIN-IDX POSITION 17.
           COMPUTE LNK-NUM = IMPOT-VIR-M + 200.
           MOVE "MO" TO LNK-AREA.
           MOVE LIN-IDX TO LNK-LINE.
           MOVE 19      TO LNK-COL.
           MOVE 3       TO LNK-SIZE.
           MOVE "L"     TO LNK-LOW.
           CALL "0-DMESS" USING LINK-V.

       AFFICHAGE-ECRAN.
           MOVE 2451 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           EXIT.

       END-PROGRAM.
           CLOSE IMPOT.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000002000 TO EXC-KFR (1).
           MOVE 2000080000 TO EXC-KFR (2).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IMPOT-IDX + 4.
           ACCEPT HE-SUITE(IMPOT-IDX)
             LINE  LIN-IDX POSITION 2 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           MOVE HE-SUITE(IMPOT-IDX) TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 2.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IMPOT-IDX
                WHEN 53 ADD 1 TO IMPOT-IDX
                WHEN OTHER PERFORM APRES-DEC
                     IF EXC-KEY = 8
                        GO AVANT-ALL-END
                     END-IF
           END-EVALUATE.
           IF IMPOT-IDX = 0
              GO AVANT-ALL-END.
           IF HE-SUITE(IMPOT-IDX) = 0
           AND HE-IMPOT-MOIS(IMPOT-IDX) = 0
              SUBTRACT 1 FROM IMPOT-IDX
           END-IF.
           GO AVANT-ALL.
       AVANT-ALL-END.
           EXIT.

       TABLE-IMPOT.
           INITIALIZE IMPOT-RECORD.
           PERFORM AVANT-PATH.
           OPEN OUTPUT TF-TRANS
           WRITE TF-RECORD FROM TXT-RECORD.
           MOVE FR-KEY TO IMPOT-FIRME TFT-FIRME.
           MOVE LNK-ANNEE TO TFT-ANNEE.
           START IMPOT KEY >= IMPOT-KEY INVALID EXIT PROGRAM.
           PERFORM TAB-IMPOT THRU TAB-IMPOT-END.
           EXIT PROGRAM.
              
       TAB-IMPOT.
           READ IMPOT NEXT AT END 
              GO TAB-IMPOT-END.
           IF FR-KEY NOT = IMPOT-FIRME
              GO TAB-IMPOT-END.
           MOVE IMPOT-MOIS TO TFT-MOIS.
           MOVE IMPOT-SUITE TO TFT-SUITE
           MOVE IMPOT-DATE-EDITION TO TFT-EDITION 
           MOVE IMPOT-DATE-VIREMENT TO TFT-VIREMENT.
           MOVE IMPOT-A-PAYER TO TFT-MONTANT.
           MOVE IMPOT-DECOMPTE TO TFT-DECOMPTE.
           MOVE IMPOT-BANQUE-D TO TFT-BANQUE.
           WRITE TF-RECORD FROM TFT-RECORD.
           GO TAB-IMPOT.
       TAB-IMPOT-END.

