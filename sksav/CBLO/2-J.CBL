      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-J CONTROLE DETAIL FICHIER JOURNAL         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-J.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURNAL.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURNAL.FDE".


       WORKING-STORAGE SECTION.

       01  ECRAN-SUITE.
           02 ECR-S1  PIC  9999 COMP-1 VALUE 2410.
           02 ECR-S2  PIC  9999 COMP-1 VALUE 2411.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S   PIC  9999 COMP-1 OCCURS 2.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "POCL.REC".
           COPY "STATUT.REC".
           COPY "LIVREX.REC".

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE 8.
           02  CHOIX-MAX-2       PIC 99 VALUE 1. 
           02  CHOIX-MAX-3       PIC 99 VALUE 1. 
           02  CHOIX-MAX-4       PIC 99 VALUE 1. 
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 4.

       01  JOURNAL-NAME.
           02 PRECISION          PIC X(4) VALUE "S-JO".
           02 FIRME-JOURNAL      PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 ANNEE-JOURNAL      PIC 999.

       01  ECR-DISPLAY.
           02 HE-DATE.
              03 HEJJ PIC ZZ  BLANK WHEN ZERO.
              03 FILLER PIC X VALUE ".".
              03 HEMM PIC ZZ  BLANK WHEN ZERO.
              03 FILLER PIC X VALUE ".".
              03 HEAA PIC ZZZZ  BLANK WHEN ZERO.
           02 HEZ2    PIC ZZ.
           02 HEZ2Z2  PIC ZZ,ZZ.
           02 HEZ3    PIC ZZZ.
           02 HEZ4    PIC ZZZZ.
           02 HEZ6    PIC Z(6).
           02 HE-Z8   PIC Z(8).
           02 HEZ8    PIC -Z(7) BLANK WHEN ZERO.
           02 HEZ5    PIC -Z(4),ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURNAL.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-J.

           MOVE LNK-SUFFIX TO ANNEE-JOURNAL.
           MOVE FR-KEY TO FIRME-JOURNAL.
           OPEN I-O   JOURNAL.
           INITIALIZE JRL-RECORD.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
            UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions
           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0163604500 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 3 THRU 5
                      MOVE 5600000000 TO EXC-KFR(12)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 6 THRU 7
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 8 MOVE 0000080000 TO EXC-KFR (2)
                     MOVE 0000680000 TO EXC-KFR (14) 
           END-IF.        

           IF ECRAN-IDX > 1 
               MOVE 0100000000 TO EXC-KFR (1)
               MOVE 0067680000 TO EXC-KFR (14) 
               MOVE 0052000000 TO EXC-KFR (11)
           END-IF.    

           IF ECRAN-IDX = 4
               MOVE 0100000000 TO EXC-KFR (1)
               MOVE 0067000000 TO EXC-KFR (14) 
           END-IF.    

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
               EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-1-1 
               WHEN  2 PERFORM AVANT-1-2 
               WHEN  3 PERFORM AVANT-1-3 
               WHEN  4 PERFORM AVANT-1-4 
               WHEN  5 PERFORM AVANT-1-5 
               WHEN  6 PERFORM AVANT-1-6 
               WHEN  7 PERFORM AVANT-1-7 
               WHEN  8 PERFORM AVANT-DEC.

           IF ECRAN-IDX > 1 AND ECRAN-IDX < 5
              PERFORM AVANT-DEC
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF ECRAN-IDX = 1 AND EXC-KEY = 67 PERFORM END-PROGRAM.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF EXC-KEY > 66 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
              INITIALIZE DECISION
              GO TRAITEMENT-ECRAN
           END-IF.
           
           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
              WHEN  1 PERFORM APRES-1-1 
              WHEN  2 PERFORM APRES-1-2 
              WHEN  3 PERFORM APRES-1-3 
              WHEN  4 PERFORM APRES-1-4 
              WHEN  5 PERFORM APRES-1-5 
              WHEN  6 PERFORM APRES-1-6 
              WHEN  7 PERFORM APRES-1-7 
              WHEN  8 PERFORM APRES-DEC.

           IF ECRAN-IDX > 1 AND ECRAN-IDX < 15 AND INDICE-ZONE = 1
              PERFORM APRES-DEC
           END-IF.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
           END-EVALUATE. 
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.          
           ACCEPT STAT-CODE 
             LINE  5 POSITION 19 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-4.          
           ACCEPT COUT-NUMBER 
             LINE  6 POSITION 17 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-5.
           ACCEPT PC-NUMBER
             LINE  7 POSITION 13 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-6.            
           IF JRL-MOIS  = 0 
               MOVE LNK-MOIS  TO JRL-MOIS.
           ACCEPT JRL-MOIS  
             LINE  8 POSITION 19 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE JRL-MOIS TO HEZ2.
           DISPLAY HEZ2 LINE  8 POSITION 19.

       AVANT-1-7. 
           ACCEPT JRL-SUITE 
             LINE  9 POSITION 18 SIZE 3 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE JRL-SUITE TO HEZ3.
           DISPLAY HEZ3 LINE  9 POSITION 18.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

           
       APRES-1-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 0 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM PRESENCE
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     

           IF PRES-ANNEE = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-02.
           INITIALIZE JRL-RECORD.
           MOVE REG-PERSON TO JRL-PERS.
           MOVE LNK-MOIS  TO JRL-MOIS.
           PERFORM NEXT-JOURNAL.

       APRES-1-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN 13 CONTINUE
           WHEN  OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
           AND EXC-KEY NOT = 13
              IF PRES-ANNEE = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           IF REG-PERSON NOT = 0
              PERFORM CAR-RECENTE
              IF COUT-NUMBER = 0
                 MOVE CAR-COUT TO COUT-NUMBER 
              END-IF
              IF STAT-CODE  = 0
                 MOVE CAR-STATUT TO STAT-CODE 
              END-IF
           ELSE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       APRES-1-3.
           IF STAT-CODE  = 0
              MOVE CAR-STATUT TO STAT-CODE 
           END-IF.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-STAT" USING LINK-V STAT-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   OTHER  PERFORM NEXT-STATUT
           END-EVALUATE.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       APRES-1-4.
           IF COUT-NUMBER = 0
              MOVE CAR-COUT TO COUT-NUMBER 
           END-IF.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-COUT" USING LINK-V COUT-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   OTHER  PERFORM NEXT-COUT
           END-EVALUATE.
           PERFORM DIS-HE-01 THRU DIS-HE-END.



       APRES-1-5.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-POCL" USING LINK-V PC-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-HE-01 THRU DIS-HE-END
                   PERFORM DISPLAY-F-KEYS
           WHEN  3 MOVE "A" TO A-N LNK-A-N
                   CALL "2-POCL" USING LINK-V PC-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-HE-01 THRU DIS-HE-END
                   PERFORM DISPLAY-F-KEYS
           WHEN 13 CONTINUE
           WHEN OTHER PERFORM NEXT-POCL
           END-EVALUATE.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       APRES-1-6.
           EVALUATE EXC-KEY
           WHEN  65 PERFORM PREV-JOURNAL
           WHEN  66 PERFORM NEXT-JOURNAL
           END-EVALUATE.
           IF JRL-MOIS > 12
              MOVE 12 TO JRL-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF JRL-MOIS < 1
              MOVE 1 TO JRL-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0
           PERFORM READ-JOURNAL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       APRES-1-7.
           EVALUATE EXC-KEY
           WHEN  65 PERFORM PREV-JOURNAL
           WHEN  66 PERFORM NEXT-JOURNAL.
      *    WHEN OTHER PERFORM READ-JOURNAL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  1 CALL "2-LEX" USING LINK-V LEX-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
            WHEN  8 DELETE JOURNAL INVALID CONTINUE
                    END-DELETE
                    INITIALIZE JRL-RECORD
            WHEN 27 COMPUTE DECISION = INDICE-ZONE + 1
            WHEN 52 COMPUTE DECISION = INDICE-ZONE + 1
           END-EVALUATE.
           COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX (ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.
           
       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-POCL.
           CALL "6-POCL" USING LINK-V PC-RECORD A-N EXC-KEY.

       NEXT-COUT.
           CALL "6-COUT" USING LINK-V COUT-RECORD EXC-KEY.
           IF FR-KEY NOT = COUT-FIRME
              MOVE 1 TO NOT-FOUND.


       NEXT-STATUT.
           MOVE 0 TO NOT-FOUND.
           CALL "6-STATUT" USING LINK-V STAT-RECORD EXC-KEY.

       GET-JOURNAL.
           MOVE LNK-MOIS  TO JRL-MOIS.
           MOVE 0 TO JRL-SUITE.
           PERFORM READ-JOURNAL.

       NEXT-JOURNAL.
           START JOURNAL KEY > JRL-KEY INVALID KEY 
              INITIALIZE JRL-REC-DET
              NOT INVALID PERFORM READ-NEXT-JOURNAL.

       READ-NEXT-JOURNAL.
           READ JOURNAL NEXT NO LOCK AT END 
                INITIALIZE JRL-REC-DET.
           PERFORM TRANSLATE.         

       PREV-JOURNAL.
           START JOURNAL KEY < JRL-KEY INVALID KEY 
              INITIALIZE JRL-REC-DET
              NOT INVALID PERFORM READ-PREV-JOURNAL.

       READ-PREV-JOURNAL.
           READ JOURNAL PREVIOUS NO LOCK AT END 
                INITIALIZE JRL-REC-DET.
           PERFORM TRANSLATE.         

       READ-JOURNAL.
           MOVE REG-PERSON  TO JRL-PERS.
           MOVE COUT-NUMBER TO JRL-COUT. 
           MOVE STAT-CODE   TO JRL-STAT. 
           MOVE PC-NUMBER   TO JRL-POSTE. 
           display jrl-key line 1 position 1.
           READ JOURNAL NO LOCK INVALID 
                INITIALIZE JRL-REC-DET.
           PERFORM TRANSLATE.         

       TRANSLATE.         
           MOVE JRL-PERS  TO REG-PERSON.
           MOVE JRL-COUT  TO COUT-NUMBER.
           MOVE JRL-STAT  TO STAT-CODE.
           MOVE JRL-POSTE TO PC-NUMBER.
           PERFORM AFFICHAGE-DETAIL.


       DIS-HE-01.
           MOVE REG-PERSON  TO HEZ6.
           DISPLAY HEZ6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-03.
           MOVE STAT-CODE  TO HEZ4.
           DISPLAY HEZ4 LINE  5 POSITION 17.
           DISPLAY STAT-NOM  LINE 5 POSITION 33.
       DIS-HE-04.
           MOVE COUT-NUMBER TO HEZ4.
           DISPLAY HEZ4  LINE  6 POSITION 17.
           DISPLAY COUT-NOM LINE 6 POSITION 33.
       DIS-HE-05.
           PERFORM NEXT-POCL.
           MOVE PC-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 7 POSITION 13.
           DISPLAY PC-NOM LINE 7 POSITION 33 SIZE 40.
       DIS-HE-06.
           MOVE JRL-MOIS TO HEZ2.
           DISPLAY HEZ2  LINE  8 POSITION 19.
       DIS-HE-07.
           MOVE JRL-SUITE TO HEZ3.
           DISPLAY HEZ3  LINE  9 POSITION 18.
       DIS-HE-END.
           EXIT.

       DIS-E1-01.
           MOVE JRL-ANNEE-E  TO HEAA.
           MOVE JRL-MOIS-E   TO HEMM.
           MOVE JRL-JOUR-E   TO HEJJ.
           DISPLAY HE-DATE LINE 10 POSITION 40.
           MOVE JRL-ANNEE-M  TO HEAA.
           MOVE JRL-MOIS-M   TO HEMM.
           MOVE JRL-JOUR-M   TO HEJJ.
           DISPLAY HE-DATE LINE 11 POSITION 40.
           MOVE JRL-ANNEE-C  TO HEAA.
           MOVE JRL-MOIS-C   TO HEMM.
           MOVE JRL-JOUR-C   TO HEJJ.
           DISPLAY HE-DATE LINE 12 POSITION 40.

       TABS.
           COMPUTE COL-IDX = (IDX - 1) / 20.
           COMPUTE LIN-IDX = IDX - (20 * COL-IDX) + 2.
           COMPUTE COL-IDX = COL-IDX  * 16 + 1.
           COMPUTE IDX-1 = (ECRAN-IDX - 2)  * 100 + IDX.
           MOVE IDX-1 TO HEZ3.
           DISPLAY HEZ3 LINE LIN-IDX POSITION COL-IDX LOW.
           ADD 3 TO COL-IDX.

       DIS-00.
           PERFORM TABS.
           MOVE  JRL-V(IDX-1)  TO HEZ5.
           DISPLAY HEZ5 LINE LIN-IDX POSITION COL-IDX HIGH.
           ADD 8 TO COL-IDX.
           IF ECRAN-IDX = 2
           AND JRL-U(IDX-1) > 0
              MOVE JRL-U(IDX-1) TO HEZ2Z2
              DISPLAY HEZ2Z2 LINE LIN-IDX POSITION COL-IDX REVERSE
           ELSE
              DISPLAY "     " LINE LIN-IDX POSITION COL-IDX LOW
           END-IF.

       AFFICHAGE-ECRAN.
           IF ECRAN-IDX = 1
              MOVE ECR-S(1) TO LNK-VAL
           ELSE
              MOVE ECR-S(2) TO LNK-VAL
           END-IF.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           EVALUATE ECRAN-IDX
             WHEN 1 PERFORM DIS-HE-01 THRU DIS-HE-01
             WHEN OTHER
                  PERFORM DIS-00 VARYING IDX FROM 1 BY 1 UNTIL IDX > 100
                  DISPLAY ECRAN-IDX LINE 2 POSITION 40
           END-EVALUATE.
           

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
