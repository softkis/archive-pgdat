      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-ANCMOY INTERROGATION ANCIENNETE MOYENNE   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-ANCMOY.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
       01  CHOIX-MAX         PIC 99 VALUE 1. 

       01  HELP-CUMUL.           
           02 O-E OCCURS 3.
              03 OUV-EMP PIC 9999.
              03 OE-ANNEES PIC 99V99.
              03 OE-MOIS   PIC 999999.
           02 H-F OCCURS 3.
              03 HOM-FEM PIC 9999.
              03 HF-ANNEES PIC 99V99.
              03 HF-MOIS   PIC 999999.
           
       01  ECR-DISPLAY.
           02 HE-Z2    PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z2Z2  PIC ZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z4    PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6    PIC Z(6) BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-ANCMOY.
       
           PERFORM AFFICHAGE-ECRAN .

           PERFORM CUMUL-PERSON01.
           MOVE 1 TO INDICE-ZONE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
            UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           
           PERFORM APRES-DEC.

           IF INDICE-ZONE > CHOIX-MAX
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           EXIT.

       CUMUL-PERSON01.
           MOVE "AA" TO LNK-AREA.
           MOVE 22 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           MOVE 0 TO LNK-NUM.
           INITIALIZE REG-RECORD HELP-CUMUL.
           PERFORM LECTURE-PERSON THRU LECTURE-PERSON-END.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.

       LECTURE-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY
           IF REG-PERSON = 0
              GO LECTURE-PERSON-END
           END-IF.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF PRES-TOT(LNK-MOIS) = 0
              GO LECTURE-PERSON
           END-IF.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF CAR-STATUT > 3
              GO LECTURE-PERSON
           END-IF.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           IF PR-CODE-SEXE = 0
              MOVE 1 TO PR-CODE-SEXE
           END-IF.
           ADD 1 TO HOM-FEM(PR-CODE-SEXE) HOM-FEM(3).
           ADD 1 TO OUV-EMP(CAR-STATUT) OUV-EMP(3).
           COMPUTE IDX-1 = (LNK-ANNEE - REG-ANCIEN-A - 1) * 12.
           ADD IDX-1 TO OE-MOIS(CAR-STATUT) OE-MOIS(3).
           ADD IDX-1 TO HF-MOIS(PR-CODE-SEXE) HF-MOIS(3).
           COMPUTE IDX-1 = LNK-MOIS + 13 - REG-ANCIEN-M.
           ADD IDX-1 TO OE-MOIS(CAR-STATUT) OE-MOIS(3).
           ADD IDX-1 TO HF-MOIS(PR-CODE-SEXE) HF-MOIS(3).
           GO LECTURE-PERSON.
       LECTURE-PERSON-END.
           COMPUTE HF-ANNEES(1) = HF-MOIS(1) / (12 * HOM-FEM(1))
           COMPUTE HF-ANNEES(2) = HF-MOIS(2) / (12 * HOM-FEM(2))
           COMPUTE HF-ANNEES(3) = HF-MOIS(3) / (12 * HOM-FEM(3))
           COMPUTE OE-ANNEES(1) = OE-MOIS(1) / (12 * OUV-EMP(1))
           COMPUTE OE-ANNEES(2) = OE-MOIS(2) / (12 * OUV-EMP(2))
           COMPUTE OE-ANNEES(3) = OE-MOIS(3) / (12 * OUV-EMP(3))


           PERFORM AFFICHAGE-DETAIL.

       DIS-FIRME.
           DISPLAY FR-NOM    LINE  3 POSITION  3.
           DISPLAY FR-PAYS LINE  5 POSITION  3.
           MOVE FR-CODE-POST   TO HE-Z6.
           DISPLAY HE-Z6  LINE  5 POSITION  5.
           DISPLAY FR-MAISON LINE  4 POSITION  7.
           DISPLAY FR-RUE   LINE  4 POSITION 12.
           DISPLAY FR-LOCALITE  LINE  5 POSITION 12.

       DIS-E1-01.
           COMPUTE LIN-IDX = 9.
           MOVE OUV-EMP(1) TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 17.
           MOVE OUV-EMP(2) TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 43.
           MOVE OUV-EMP(3) TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 65.

           COMPUTE LIN-IDX = 11.
           MOVE OE-ANNEES(1) TO HE-Z2Z2.
           DISPLAY HE-Z2Z2 LINE LIN-IDX POSITION 17.
           MOVE OE-ANNEES(2) TO HE-Z2Z2.
           DISPLAY HE-Z2Z2 LINE LIN-IDX POSITION 43.
           MOVE OE-ANNEES(3) TO HE-Z2Z2.
           DISPLAY HE-Z2Z2 LINE LIN-IDX POSITION 65.

           COMPUTE LIN-IDX = 15.
           MOVE HOM-FEM(1) TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 17.
           MOVE HOM-FEM(2) TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 43.
           MOVE HOM-FEM(3) TO HE-Z4.
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 65.

           COMPUTE LIN-IDX = 17.
           MOVE HF-ANNEES(1) TO HE-Z2Z2.
           DISPLAY HE-Z2Z2 LINE LIN-IDX POSITION 17.
           MOVE HF-ANNEES(2) TO HE-Z2Z2.
           DISPLAY HE-Z2Z2 LINE LIN-IDX POSITION 43.
           MOVE HF-ANNEES(3) TO HE-Z2Z2.
           DISPLAY HE-Z2Z2 LINE LIN-IDX POSITION 65.

       AFFICHAGE-ECRAN.
           MOVE 2206 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-FIRME THRU DIS-E1-01.
           
       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
    
