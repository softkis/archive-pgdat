      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-VIRSD PAYEMENTS POUR SOLDE                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-VIRSD.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "VIREMENT.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴
           COPY "VIREMENT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
       01  CHOIX-MAX             PIC 99 VALUE 10.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "CONTRAT.REC".

           COPY "LIVRE.REC".
           COPY "BANQF.REC".
           COPY "BANQF.LNK".
           COPY "BANQUE.REC".
           COPY "BANQP.REC".
           COPY "CODPAIE.REC".
           COPY "SYNDICAT.REC".

           COPY "STATUT.REC".
           COPY "COUT.REC".
           COPY "V-VAR.CPY".
       01  COMPTEUR              PIC 99.
       01  HELP-1                PIC 9(6)V99.
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  TOTAUX.
           02 DEBIT              PIC 9(8)V99.
           02 CREDIT             PIC 9(8)V99.
       01  TEST-MOIS             PIC 99.
       01  EF-DATE.
           02 EF-ANNEE           PIC 9999.
           02 EF-MOIS            PIC 99.
           02 EF-JOUR            PIC 99.

       01  BQ-IDX                PIC 99 VALUE 0.
       01  CORRESPONDANCE        PIC X VALUE "N".
       01  LANGUE                PIC 9.
       01  HELP-2                PIC 9(8)V99.

       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIR          PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(6),ZZ-.

      *SPECIFIQUE VENTILATION
      *컴컴컴컴컴컴컴컴컴컴컴

       01  BANQUES.
           02 COR-BANQUE           PIC X(10) OCCURS 20.
           02 COR-COMPTE           PIC X(50) OCCURS 20.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON VIREMENT.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-VIRSD.
       
           MOVE LNK-MOIS  TO SAVE-MOIS.
           MOVE LNK-ANNEE TO SAVE-ANNEE
           MOVE LNK-SUFFIX TO ANNEE-VIR.
           OPEN I-O   VIREMENT.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN.
           PERFORM BANQUES-CORRESPONDANTES.
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0100000000 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0063000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 10    MOVE 0000000025 TO EXC-KFR (1)
                      MOVE 2500000000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN  9 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 1 
           AND INDICE-ZONE < 8
               MOVE 00503001 TO LNK-VAL
               CALL "0-BOOK" USING LINK-V
               PERFORM AFFICHAGE-ECRAN
               PERFORM AFFICHAGE-DETAIL
               MOVE 98 TO EXC-KEY
           END-IF.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5
           WHEN  6 PERFORM APRES-6
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-8
           WHEN  9 PERFORM APRES-9
           WHEN 10 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-5.
           IF REG-PERSON NOT = END-NUMBER
           ACCEPT COUT
             LINE 11 POSITION 32 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE
           ELSE 
             MOVE 0 TO COUT
           END-IF.

       AVANT-6.
           ACCEPT BQF-CODE
             LINE 13 POSITION 25 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           IF BQ-IDX > 1
           ACCEPT CORRESPONDANCE
             LINE 15 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           IF EF-ANNEE < LNK-ANNEE 
              MOVE LNK-ANNEE TO EF-ANNEE.
              COMPUTE TEST-MOIS = LNK-MOIS + 1.
           IF EF-ANNEE <= LNK-ANNEE 
           AND LNK-MOIS = 12
              COMPUTE EF-ANNEE = LNK-ANNEE + 1.
           ACCEPT EF-ANNEE
             LINE 17 POSITION 25 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-9.
           IF EF-ANNEE NOT = 0
              ACCEPT EF-MOIS 
             LINE 19 POSITION 27 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF EXC-KEY = 65
           AND EF-MOIS > 1
              SUBTRACT 1 FROM EF-MOIS.
           IF EXC-KEY = 66
           AND EF-MOIS < 12
              ADD 1 TO EF-MOIS.
           PERFORM DIS-HE-09.


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-5.
           MOVE COUT TO COUT-NUMBER.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-COUT" USING LINK-V
                    MOVE LNK-VAL TO COUT
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   2 CALL "2-COUT" USING LINK-V COUT-RECORD
                    MOVE COUT-NUMBER TO COUT
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   OTHER 
                    PERFORM NEXT-COUT
                    MOVE COUT-NUMBER TO COUT
           END-EVALUATE.
           IF COUT > 0
           AND EXC-KEY = 13
              MOVE COUT TO COUT-NUMBER
              PERFORM NEXT-COUT
              IF COUT-NUMBER = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF
           ELSE 
              INITIALIZE COUT-RECORD
           END-IF.
           PERFORM DIS-HE-05.

       APRES-6.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-BANQF" USING LINK-V BQF-RECORD
                   IF BQF-CODE NOT = SPACES
                      MOVE BQF-CODE TO BQ-CODE
                   END-IF
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER MOVE EXC-KEY TO SAVE-KEY
                      PERFORM NEXT-BANQF
                      MOVE BQF-CODE TO BQ-CODE
           END-EVALUATE.
           IF BQF-ACTIF = "N"
              MOVE 1 TO INPUT-ERROR
              MOVE "SL" TO LNK-AREA
              MOVE 78   TO LNK-NUM
              MOVE 0    TO LNK-POSITION
              CALL "0-DMESS" USING LINK-V
           END-IF.
           PERFORM DIS-HE-06.

       APRES-7. 
           IF CORRESPONDANCE NOT = " " 
           AND NOT = "N"
           AND NOT = "Y"
           AND NOT = "O"
           AND NOT = "J"
               MOVE 1 TO INPUT-ERROR.

       APRES-8.
           IF EF-ANNEE NOT = 0 
           AND EF-ANNEE < TODAY-ANNEE
              AND TODAY-MOIS > 1
              MOVE TODAY TO EF-DATE
              MOVE 1 TO INPUT-ERROR.
           IF BQF-CODE = SPACES
           AND EF-ANNEE = 0
              MOVE 1 TO INPUT-ERROR.

       APRES-9.
           IF EF-ANNEE = LNK-ANNEE 
           AND EF-MOIS < TEST-MOIS
              MOVE TEST-MOIS TO EF-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF EF-ANNEE = LNK-ANNEE AND EF-MOIS = LNK-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF EF-ANNEE < LNK-ANNEE AND EF-MOIS < 12
              MOVE 12 TO EF-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF EF-ANNEE NOT = 0 
              IF EF-MOIS < 1 OR EF-MOIS > 12
                 MOVE TODAY-MOIS TO EF-MOIS
                 MOVE 1 TO INPUT-ERROR
              END-IF
           ELSE
              MOVE 0 TO EF-MOIS INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-09.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-PERSON
                    PERFORM READ-PERSON THRU READ-PERSON-END
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-PERSON-END
           END-IF.
           MOVE 1 TO LANGUE.
           EVALUATE PR-LANGUAGE
                WHEN "D" MOVE 2 TO LANGUE
                WHEN "E" MOVE 3 TO LANGUE
           END-EVALUATE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           PERFORM READ-COUT.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-1
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.

           INITIALIZE L-RECORD SH-00 COMPTEUR SAVE-KEY VIR-RECORD
           LNK-SUITE.
           PERFORM READ-VIREM THRU READ-VIR-END.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           COMPUTE SH-00 = L-A-PAYER - SH-00 - L-TROP-PERCU.
           IF SH-00 = 0
              GO READ-PERSON-1
           END-IF.
           PERFORM DIS-HE-01.
           IF BQF-CODE > SPACES
           AND SH-00 > 0
              PERFORM VIREMENT-SOLDE
              GO READ-PERSON-1
           END-IF.
           IF EF-MOIS = 0
              GO READ-PERSON-1
           END-IF.
           IF LNK-ANNEE = EF-ANNEE
              IF PRES-TOT(EF-MOIS) = 0
                 GO READ-PERSON-1
              END-IF
           ELSE
              PERFORM READ-CONTRAT
              IF  CON-FIN-A = EF-ANNEE
              AND CON-FIN-M < EF-MOIS 
                 GO READ-PERSON-1
              END-IF
              IF  CON-FIN-A > 0
              AND CON-FIN-A < EF-ANNEE
                 GO READ-PERSON-1
              END-IF
           END-IF.
           PERFORM AJUSTE.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
               GO READ-PERSON.
       READ-PERSON-END.
           PERFORM END-PROGRAM.

       AJUSTE.
           INITIALIZE CSP-RECORD.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE SH-00 TO HELP-1.
           IF SH-00 > 0
              COMPUTE  CSP-CODE = 1100 + LNK-MOIS
              MOVE EF-MOIS TO CSP-MOIS LNK-MOIS
              PERFORM WRITE-CSP-1
              COMPUTE  CSP-CODE = 2100 + EF-MOIS
              MOVE LNK-MOIS TO CSP-MOIS
              PERFORM WRITE-CSP
           ELSE
              COMPUTE  CSP-CODE = 2100 + LNK-MOIS
              MOVE EF-MOIS TO CSP-MOIS LNK-MOIS
              PERFORM WRITE-CSP-1
              COMPUTE  CSP-CODE = 1100 + EF-MOIS
              MOVE LNK-MOIS TO CSP-MOIS
              PERFORM WRITE-CSP
           END-IF.
           CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES.
qq         IF EF-ANNEE <= TODAY-ANNEE
              MOVE EF-MOIS  TO LNK-MOIS 
              MOVE EF-ANNEE TO LNK-ANNEE
              CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES
              MOVE SAVE-MOIS  TO LNK-MOIS 
              MOVE SAVE-ANNEE TO LNK-ANNEE
           END-IF.

       WRITE-CSP.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           ADD HELP-1 TO CSP-TOTAL.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.


       WRITE-CSP-1.
           IF EF-ANNEE = LNK-ANNEE
              PERFORM WRITE-CSP
           ELSE
              MOVE SAVE-MOIS TO LNK-MOIS
              MOVE LNK-DATE TO SAVE-DATE 
              MOVE EF-DATE  TO LNK-DATE
              PERFORM WRITE-CSP
              MOVE SAVE-DATE TO LNK-DATE
           END-IF.
           MOVE SAVE-MOIS TO LNK-MOIS.

       VIREMENT-SOLDE.
           INITIALIZE BQP-RECORD VIR-RECORD.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD FAKE-KEY.
           IF BQP-BANQUE = SPACES
              MOVE "C" TO BQP-BANQUE 
           END-IF.
           MOVE SH-00 TO VIR-A-PAYER.
           IF CORRESPONDANCE NOT = "N"
              MOVE BQP-BANQUE TO VIR-BANQUE-D
              PERFORM GET-COR
           ELSE
              MOVE BQF-CODE TO VIR-BANQUE-D
           END-IF.
           IF BQP-BANQUE-PREF NOT = SPACES
              MOVE BQP-BANQUE-PREF TO VIR-BANQUE-D
           END-IF.
           MOVE BQP-BANQUE TO VIR-BANQUE-C.
           MOVE BQP-COMPTE TO VIR-COMPTE-C.
           ADD 1 TO COMPTEUR.
           MOVE 0 TO VIR-TYPE VIR-TYPE-B VIR-TYPE-S VIR-TYPE-N.
           MOVE COMPTEUR TO 
                VIR-SUITE VIR-SUITE-B VIR-SUITE-S VIR-SUITE-N.
           PERFORM WRITE-VIREMENT.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.

       NEXT-COUT.
           CALL "6-COUT" USING LINK-V COUT-RECORD EXC-KEY.

       READ-COUT.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.

       BANQUES-CORRESPONDANTES.
           INITIALIZE BQF-RECORD BANQUES INPUT-ERROR BQ-IDX.
           MOVE 66 TO SAVE-KEY.
           PERFORM B-C TEST BEFORE UNTIL INPUT-ERROR = 1.
           INITIALIZE BQF-RECORD.

       B-C.                
           CALL "6-BANQF" USING LINK-V BQF-RECORD SAVE-KEY.
           IF BQF-ACTIF = "N"
              GO B-C
           END-IF.
           IF BQF-FIRME NOT = 0
              ADD 1 TO BQ-IDX
              MOVE BQF-CODE TO COR-BANQUE(BQ-IDX)
              MOVE BQF-COMPTE TO COR-COMPTE(BQ-IDX)
           ELSE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF BQ-IDX = 20 MOVE 1 TO INPUT-ERROR.

       GET-COR.
           MOVE 1 TO INPUT-ERROR.
           MOVE 0 TO BQ-IDX.
           PERFORM COR THRU COR-END.
           IF INPUT-ERROR = 1
               MOVE BQF-CODE TO VIR-BANQUE-D.

       COR.
           ADD 1 TO BQ-IDX.
           IF BQ-IDX = 21
              GO COR-END.
           IF COR-BANQUE(BQ-IDX) =  VIR-BANQUE-D
              MOVE 0 TO INPUT-ERROR
              GO COR-END.
           GO COR.
       COR-END.
           EXIT.

       NEXT-BANQF.
           CALL "6-BANQF" USING LINK-V BQF-RECORD SAVE-KEY.
           IF BQF-ACTIF = "N"
           AND SAVE-KEY > 64
              GO NEXT-BANQF
           END-IF.

       WRITE-CS.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE BQF-CODE TO BQ-CODE.
           PERFORM READ-BANQUE.
           DISPLAY BQF-CODE   LINE 13 POSITION 25.
           DISPLAY BQ-NOM     LINE 13 POSITION 40.
           DISPLAY BQF-COMPTE LINE 14 POSITION 40.
       DIS-HE-07.
           DISPLAY CORRESPONDANCE LINE 15 POSITION 32.

       DIS-HE-08.
           MOVE EF-ANNEE TO HE-Z4.
           DISPLAY HE-Z4 LINE 17 POSITION 25.
       DIS-HE-09.
           MOVE EF-MOIS  TO HE-Z2.
           DISPLAY HE-Z2 LINE 19 POSITION 27.
           MOVE EF-MOIS TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 19401500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.

       DIS-HE-END.
           EXIT.

       READ-BANQUE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD EXC-KEY.

       READ-VIREM.
           CALL "6-VIREM" USING LINK-V REG-RECORD VIR-RECORD SAVE-KEY.
           IF FR-KEY     NOT = VIR-FIRME
           OR REG-PERSON NOT = VIR-PERSON
           OR VIR-TYPE   NOT = 0 
           OR VIR-MOIS   NOT = LNK-MOIS 
              GO READ-VIR-END.
           IF VIR-TYPE = 0
              MOVE VIR-SUITE TO COMPTEUR.
           ADD VIR-A-PAYER TO SH-00.
           GO READ-VIREM.
       READ-VIR-END.
           EXIT.

       VIR-KEY.
           INITIALIZE VIR-RECORD.
           MOVE FR-KEY     TO VIR-FIRME.
           MOVE REG-PERSON TO VIR-PERSON.
           MOVE 0          TO VIR-TYPE.
           MOVE COMPTEUR   TO VIR-SUITE.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE EF-ANNEE TO CON-DEBUT-A.
           MOVE EF-MOIS  TO CON-DEBUT-M.
           MOVE 32       TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.


       WRITE-VIREMENT.
           INITIALIZE VIR-DATE-EDITION VIR-DATE-VIREMENT.
           MOVE FR-KEY TO VIR-FIRME    VIR-FIRME-B 
                          VIR-FIRME-S  VIR-FIRME-N.
           MOVE REG-PERSON TO VIR-PERSON   VIR-PERSON-B
                              VIR-PERSON-S VIR-PERSON-N.
           MOVE LNK-MOIS  TO VIR-MOIS    VIR-MOIS-B  
                             VIR-MOIS-S  VIR-MOIS-N.
           CALL "0-TODAY" USING TODAY.
           MOVE TODAY-TIME TO VIR-TIME.
           MOVE LNK-USER TO VIR-USER.
           IF LNK-SQL = "Y" 
              CALL "9-VIREM" USING LINK-V VIR-RECORD WR-KEY 
           END-IF.
           WRITE VIR-RECORD INVALID REWRITE VIR-RECORD END-WRITE.
           INITIALIZE VIR-RECORD.

           
       AFFICHAGE-ECRAN.
           MOVE 522 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
       END-PROGRAM.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           MOVE SAVE-ANNEE TO LNK-ANNEE.
           CANCEL "6-VIREM"
           CLOSE VIREMENT.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

