      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-PRIME2 IMPRESSION / CREATION              �
      *  � PRIME DE FIN D'ANNEE / REDISTRIBUTION PRIME PERDUE    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-PRIME2.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM130.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  PRECISION             PIC 9 VALUE 0.
       01  JOB-STANDARD          PIC X(10) VALUE "130       ".
       01  MAX-LIGNES            PIC 9 VALUE 2.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "CODPAR.REC".
           COPY "CODPAIE.REC".
           COPY "CODSAL.REC".
           COPY "CONTRAT.REC".
           COPY "IMPRLOG.REC".
           COPY "CCOL.REC".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  TIRET-TEXTE           PIC X VALUE "�".

       01  PRIME                 PIC 9(7)V99 VALUE 0.
       01  OCCURENCES            PIC 9999 VALUE 0.
       01  TOT-HRS               PIC 9(8)V99 VALUE 0.
       01  CREATE-CODE           PIC X VALUE "N".

       01  TOTAL-VALEUR.
           02 TOT-VALEURS.
               04 TOT-DONNEE     PIC 9(8)V9(5) OCCURS 8.
           02 TOT-PERS           PIC 9(4).
       01  SAL-ACT                PIC 9(4)V99.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".CSM".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3 PIC Z(3).
           02 HE-Z6 PIC Z(6).
           02 HE-Z6Z2 PIC Z(6),ZZ.
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z2Z2 PIC ZZ,ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM. 

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-PRIME2.

           IF FR-PRIME NOT = 6
              MOVE "SL" TO LNK-AREA
              MOVE 49 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              ACCEPT ACTION
              EXIT PROGRAM
           END-IF.
           IF LNK-ANNEE < 2006
           OR LNK-MOIS NOT = 12
              MOVE "SL" TO LNK-AREA
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              ACCEPT ACTION
              EXIT PROGRAM
           END-IF.
           INITIALIZE LIN-NUM LIN-IDX REG-RECORD TOTAL-VALEUR.
           PERFORM AFFICHAGE-ECRAN .
           MOVE "N" TO A-N.
           CALL "6-GCP" USING LINK-V CP-RECORD.
           MOVE COD-PAR(197, 2) TO CS-NUMBER.
           CALL "6-CODSAL" USING LINK-V CS-RECORD FAKE-KEY.
           PERFORM READ-CSP THRU READ-CSP-END.
           IF PRIME = 0
           OR OCCURENCES = 0
              MOVE 7 TO LNK-NUM
              MOVE "SL" TO LNK-AREA
              MOVE 0 TO LNK-POSITION
              CALL "0-DMESS" USING LINK-V
              PERFORM AA
              EXIT PROGRAM
           END-IF.

           CALL "0-TODAY" USING TODAY.
           MOVE FR-ACCIDENT TO LNK-NUM HE-Z2.
           DISPLAY HE-Z2 LINE 18 POSITION 4.
           MOVE "AC" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           DISPLAY LNK-TEXT LINE 18 POSITION  9 SIZE 40.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.

           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT CREATE-CODE
             LINE 16 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           IF CREATE-CODE = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM GET-PERS.
           PERFORM GET-CSP.
           IF CSP-POURCENT = 100
              PERFORM WRITE-CS
              PERFORM DONNEES-PERSONNE.
           PERFORM DIS-HE-01.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.
           PERFORM END-PROGRAM.
                

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       WRITE-CS.
           PERFORM GET-CSP.
           COMPUTE SAL-ACT = CSP-UNITE.
           INITIALIZE CSP-RECORD.
           MOVE SAL-ACT TO CSP-UNITE.
           MOVE FR-KEY     TO CSP-FIRME. 
           MOVE LNK-MOIS   TO CSP-MOIS.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE COD-PAR(197, 2) TO CSP-CODE. 
           COMPUTE SAL-ACT = PRIME / TOT-HRS * CSP-UNITE + ,005.
           MOVE SAL-ACT TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 10 POSITION 40.
           MOVE SAL-ACT TO CSP-TOTAL.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.
           CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES.

       READ-CSP.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
              GO READ-CSP-END.
           PERFORM GET-CSP.
           IF CSP-POURCENT = 100
              ADD 1 TO OCCURENCES
              ADD CSP-UNITE TO TOT-HRS.
           ADD CSP-DONNEE-2 TO PRIME.
           GO READ-CSP.
       READ-CSP-END.
           MOVE PRIME TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 10 POSITION 20.
           MOVE OCCURENCES TO HE-Z3.
           DISPLAY HE-Z3 LINE 10 POSITION 40.
           MOVE TOT-HRS TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 10 POSITION 30.

       GET-CSP.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY     TO CSP-FIRME. 
           MOVE LNK-MOIS   TO CSP-MOIS.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE COD-PAR(197, 1) TO CSP-CODE. 
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

        DONNEES-PERSONNE.
           PERFORM DIS-HE-01.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           
           COMPUTE IDX = LIN-NUM + MAX-LIGNES.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           MOVE REG-PERSON TO LAST-PERSON.
           MOVE 0 TO DEC-NUM POINTS.
           MOVE 3 TO COL-NUM. 
           MOVE 6 TO CAR-NUM
           MOVE REG-PERSON TO VH-00
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE
           PERFORM FILL-FORM.
           PERFORM FILL-CS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.
           PERFORM ADD-CS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.
           ADD 1 TO TOT-PERS LIN-NUM.

       FILL-CS.
           COMPUTE COL-NUM = 31 + (IDX * 14).
           MOVE 9 TO CAR-NUM.
           MOVE 1 TO POINTS. 
           MOVE CS-DECIMALES(IDX) TO DEC-NUM.
           IF DEC-NUM > 4 MOVE 4 TO DEC-NUM.
           MOVE CSP-DONNEE(IDX) TO VH-00.
           IF VH-00 > 0
              PERFORM FILL-FORM
           END-IF.            

       ADD-CS.
           ADD CSP-DONNEE(IDX) TO TOT-DONNEE(IDX).  

       FILL-TOTAL.
           ADD 1 TO LIN-NUM.
           MOVE 13 TO COL-NUM.
           MOVE "TOTAL" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 4 TO CAR-NUM.
           MOVE TOT-PERS TO VH-00.
           ADD   1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE TOT-VALEURS TO CSP-VALEURS.
           PERFORM FILL-CS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.

       ENTETE.
           ADD 1 TO PAGE-NUMBER.
           MOVE 4 TO CAR-NUM.
           MOVE 2 TO LIN-NUM.
           MOVE 120 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  7 TO LIN-NUM.
           MOVE 112 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 115 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 118 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           
           MOVE FR-PAYS TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 2 TO LIN-NUM.
           MOVE 55 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE LNK-ANNEE TO VH-00.
           PERFORM FILL-FORM.
           MOVE LNK-MOIS TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 4 TO LIN-NUM.
           MOVE 30 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE CS-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE CS-NOM TO ALPHA-TEXTE.
           MOVE 35 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 9 TO LIN-NUM.
           PERFORM FILL-DESCR VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.

       FILL-DESCR.
           COMPUTE COL-NUM = 33 + (IDX * 14).
           MOVE CS-DESCRIPTION(IDX) TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 27 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 6 POSITION 47 SIZE 33.
           IF A-N = "N"
              DISPLAY SPACES LINE 6 POSITION 35 SIZE 10.
       DIS-HE-END.
           EXIT.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 512 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           DISPLAY SPACES LINE  9 POSITION 1 SIZE 80.
           DISPLAY SPACES LINE 10 POSITION 1 SIZE 80.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF COUNTER > 0 
              PERFORM FILL-TOTAL
              PERFORM TRANSMET.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".

