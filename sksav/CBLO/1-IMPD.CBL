      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-IMPD ENLEVEMENT DRAPEAUX IMPOTS           �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-IMPD.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "IMPOT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "IMPOT.FDE".

       WORKING-STORAGE SECTION.

       01  CHOIX-MAX             PIC 99 VALUE 4.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  COMPTEUR              PIC 99.
       01  HELP-1                PIC 9(4) COMP-1.
       01  IMPOT-NAME.
           02 FILLER             PIC X(8) VALUE "S-IMPOT.".
           02 ANNEE-IMPOT        PIC 999 VALUE 0.

       01  DATE-EDIT        PIC 9(8).
       01  EDIT-R REDEFINES DATE-EDIT.
           04 D-EDIT-A      PIC 9999.
           04 D-EDIT-M      PIC 99.
           04 D-EDIT-J      PIC 99.

       01  DATE-VIR        PIC 9(8).
       01  VIR-R REDEFINES DATE-VIR.
           04 D-VIR-A      PIC 9999.
           04 D-VIR-M      PIC 99.
           04 D-VIR-J      PIC 99.

       01  DATE-MAJ        PIC 9(8).
       01  MAJ-R REDEFINES DATE-MAJ.
           04 D-MAJ-A      PIC 9999.
           04 D-MAJ-M      PIC 99.
           04 D-MAJ-J      PIC 99.

       01  ECR-DISPLAY.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON IMPOT.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-IMPD.

           MOVE LNK-SUFFIX TO ANNEE-IMPOT.

           OPEN I-O IMPOT.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1 THRU 3 MOVE 0000000059 TO EXC-KFR (1)
           WHEN 4     MOVE 0000000025 TO EXC-KFR (1)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-DATE
           WHEN  2 PERFORM APRES-DATE
           WHEN  3 PERFORM APRES-DATE
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           MOVE 08320000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-EDIT
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.

       AVANT-2.
           MOVE 10320000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-VIR
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.

       AVANT-3.
           MOVE 12320000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-MAJ
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DATE.
           IF LNK-NUM NOT = 0
              MOVE "AA" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM IMPOT THRU IMPOT-END
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.


       IMPOT.
           IF  D-VIR-A  = 0
           AND D-EDIT-A = 0
           AND D-MAJ-A  = 0
               PERFORM END-PROGRAM.
           INITIALIZE IMPOT-RECORD.
           START IMPOT KEY >= IMPOT-KEY INVALID 
                GO IMPOT-END.
       IMPOT-1.
           READ IMPOT NEXT AT END 
                GO IMPOT-END.
           IF IMPOT-DATE-EDITION = DATE-EDIT
               INITIALIZE IMPOT-DATE-EDITION
               PERFORM WRITE-IMPOT.
           IF IMPOT-DATE-VIREMENT = DATE-VIR
               INITIALIZE IMPOT-DATE-VIREMENT
               PERFORM WRITE-IMPOT.
           IF IMPOT-DATE-MAJ = DATE-MAJ
              IF LNK-SQL = "Y" 
                 CALL "9-IMPOT" USING LINK-V IMPOT-RECORD DEL-KEY
              END-IF
              DELETE IMPOT INVALID CONTINUE.
           GO IMPOT-1.
       IMPOT-END.
           CLOSE IMPOT.
           PERFORM END-PROGRAM.

       WRITE-IMPOT.
           IF LNK-SQL = "Y" 
              CALL "9-IMPOT" USING LINK-V IMPOT-RECORD WR-KEY
           END-IF.
           REWRITE IMPOT-RECORD INVALID CONTINUE.

       DIS-HE-01.
           MOVE D-EDIT-A TO HE-AA.
           MOVE D-EDIT-M TO HE-MM.
           MOVE D-EDIT-J TO HE-JJ.
           DISPLAY HE-DATE LINE  8 POSITION 32.

       DIS-HE-02.
           MOVE D-VIR-A TO HE-AA.
           MOVE D-VIR-M TO HE-MM.
           MOVE D-VIR-J TO HE-JJ.
           DISPLAY HE-DATE LINE 10 POSITION 32.

       DIS-HE-03.
           MOVE D-MAJ-A TO HE-AA.
           MOVE D-MAJ-M TO HE-MM.
           MOVE D-MAJ-J TO HE-JJ.
           DISPLAY HE-DATE LINE 12 POSITION 32.

       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE  452 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 4.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *-----------------------------
      *    Routines standard: clause copies
      *-----------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

