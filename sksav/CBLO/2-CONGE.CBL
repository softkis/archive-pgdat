      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-CONGE CALENDRIER CONGES                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-CONGE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "JOURS.REC".
           COPY "LIVRE.REC".
           COPY "CONGE.REC".

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  MOIS                  PIC 99.
       01  OCCUP-TOTAL           PIC S9999v99 COMP-3.
       01  OCC-HELP.
           02 OCCUP-HELP         PIC 99999V99 COMP-3 OCCURS 12.
       01  CONGES.
           02 CONGE-CUMUL           PIC S9999v99 COMP-3.
           02 CONGE-DROIT           PIC S9999v99 COMP-3.
           02 CONGE-HELP            PIC 99999V99 COMP-3.
           02 CONGE-DU              PIC S999V99 COMP-3.
       
       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS       PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z3Z2 PIC Z(3),ZZ.
           02 HE-Z4Z2 PIC -ZZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HELP-VAL              PIC Z BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-CONGE.
       
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions


           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           END-EVALUATE.


       TRAITEMENT-ECRAN-01.

           PERFORM DISPLAY-F-KEYS.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1-1 
           WHEN  2 PERFORM AVANT-1-2.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-1 
           WHEN  2 PERFORM APRES-1-2.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-1-1.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       APRES-1-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM PRESENCE
                   PERFORM GET-PERS
           WHEN  OTHER PERFORM NEXT-REGIS.
           PERFORM DIS-E1-01 THRU DIS-E1-END.
           IF PRES-ANNEE > 0
           AND EXC-KEY NOT = 53
              PERFORM AFFICHAGE-OCCUP.
           
       APRES-1-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 
              PERFORM NEXT-REGIS
              PERFORM DIS-E1-01 THRU DIS-E1-END
              IF  REG-PERSON > 0 
              AND PRES-ANNEE > 0
                 PERFORM AFFICHAGE-OCCUP
              ELSE
                 MOVE 1 TO INPUT-ERROR
              END-IF
           END-EVALUATE.

       AFFICHAGE-ECRAN.
           MOVE 2216 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           PERFORM AFFICHAGE-CALENDRIER.

       AFFICHAGE-CALENDRIER.
           MOVE 10180000 TO LNK-POSITION.
           CALL "0-DSCAL" USING LINK-V.


       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       AFFICHAGE-OCCUP.
           INITIALIZE OCC-HELP OCCUP-TOTAL CONGE-RECORD CONGES
           CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD NUL-KEY.
           IF CAR-CONGE NOT = 9 
              IF  CONGE-TOTAL  = 0 
              AND CONGE-AJUSTE = 0
              AND CONGE-TIME   = 0
              CALL "4-CGREP" USING LINK-V REG-RECORD CAR-RECORD
                                          CONGE-RECORD
              CALL "4-CGINI" USING LINK-V REG-RECORD CONGE-RECORD
              CANCEL "4-CGREP" 
              CANCEL "4-CGINI" 
           END-IF.
           PERFORM READ-MOIS THRU READ-MOIS-END VARYING MOIS FROM 
           1 BY 1 UNTIL MOIS > 12.
           ADD  CONGE-TOTAL TO CONGE-CUMUL.
           ADD  CONGE-DU    TO CONGE-CUMUL.
           MOVE CONGE-CUMUL TO HE-Z4Z2.
           DISPLAY HE-Z4Z2 LINE 22 POSITION 9.
           MOVE CONGE-SOLDE TO HE-Z4Z2.
           DISPLAY HE-Z4Z2 LINE  6 POSITION 9.
           MOVE CONGE-AN    TO HE-Z4Z2.
           DISPLAY HE-Z4Z2 LINE  5 POSITION 9.
           MOVE CONGE-AJUSTE TO HE-Z4Z2.
           DISPLAY HE-Z4Z2 LINE  7 POSITION 9.
           MOVE CONGE-DU    TO HE-Z4Z2.
           DISPLAY HE-Z4Z2 LINE  8 POSITION  9.
           
       READ-MOIS.
           INITIALIZE JRS-RECORD L-RECORD .
           MOVE FR-KEY TO JRS-FIRME L-FIRME.
           MOVE MOIS   TO JRS-MOIS  L-MOIS.
           MOVE REG-PERSON TO JRS-PERSON L-PERSON.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           MOVE 20 TO JRS-OCCUPATION.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD FAKE-KEY.
           COMPUTE LIN-IDX = 9 + MOIS.
           MOVE 17 TO COL-IDX.
           PERFORM DIS-JOUR VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 31.
       READ-MOIS-END.
           PERFORM DISPLAY-TOTAL.

       DIS-JOUR.
           ADD 2 TO COL-IDX.
           MOVE JRS-HRS(IDX-1) TO HELP-VAL.
           DISPLAY HELP-VAL LINE LIN-IDX POSITION COL-IDX.

       DISPLAY-TOTAL.
           COMPUTE LIN-IDX = 9 + MOIS.
           MOVE JRS-HRS(32) TO CONGE-HELP.
           IF L-PERSON NOT = 0
              MOVE L-UNI-CONGE TO CONGE-HELP
              ADD L-UNI-CONGE-MIN TO CONGE-HELP 
060202*       SUBTRACT L-UNI-CONGE-PLUS FROM CONGE-DU
              ADD L-UNI-CONGE-PLUS TO CONGE-DU.

           SUBTRACT CONGE-HELP FROM CONGE-CUMUL.
           MOVE CONGE-HELP TO HE-Z3Z2.
           DISPLAY HE-Z3Z2 LINE LIN-IDX POSITION 11.

           
       AFFICHAGE-DETAIL.
           PERFORM AFFICHAGE-CALENDRIER.

       DIS-E1-01.
           MOVE REG-PERSON  TO HE-Z6.
           IF REG-PERSON NOT = 0
              MOVE REG-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 
             LINE 3 POSITION 15 SIZE 6
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-E1-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-E1-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

