      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-MAL CALENDRIER MALADIES                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-MAL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.


           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "STATUT.REC".
           COPY "CARRIERE.REC".

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  MOIS                  PIC 99.
       01  OCCUP-TOTAL           PIC S9999v99 COMP-3.
       01  OCC-HELP.
           02 OCCUP-HELP         PIC 99999V99 COMP-3 OCCURS 12.
       
       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS       PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z3Z2 PIC Z(3),ZZ.
           02 HE-Z4Z2 PIC ZZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HELP-VAL              PIC Z BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-MAL.
       
           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN INPUT JOURS.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0100000000 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions


           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 2     MOVE 0129000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6500000000 TO EXC-KFR(14)
           END-EVALUATE.


       TRAITEMENT-ECRAN-01.

           PERFORM DISPLAY-F-KEYS.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1-1 
           WHEN  2 PERFORM AVANT-1-2.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-1 
           WHEN  2 PERFORM APRES-1-2.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-1-1.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       APRES-1-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM PRESENCE
                   PERFORM GET-PERS
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   IF REG-PERSON > 0 
                      PERFORM AFFICHAGE-OCCUP
                      IF OCCUP-TOTAL = 0
                         GO APRES-1-1
                      END-IF
                   END-IF
           WHEN  OTHER PERFORM NEXT-REGIS.
           PERFORM DIS-E1-01 THRU DIS-E1-END.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE.
           PERFORM AFFICHAGE-DETAIL.
           IF PRES-ANNEE > 0
              PERFORM AFFICHAGE-OCCUP.
           
       APRES-1-2.
           MOVE "A" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
                   IF REG-PERSON > 0 
                      PERFORM PRESENCE
                   END-IF
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   IF REG-PERSON > 0 
                      PERFORM AFFICHAGE-OCCUP
                      IF OCCUP-TOTAL = 0
                         GO APRES-1-2
                      END-IF
                   END-IF
           END-EVALUATE.          
           PERFORM DIS-E1-01 THRU DIS-E1-END.
           IF REG-PERSON = 0 
              MOVE 1 TO INPUT-ERROR
           END-IF.

           IF EXC-KEY NOT = 27
              PERFORM AFFICHAGE-DETAIL
              IF PRES-ANNEE > 0
                 PERFORM AFFICHAGE-OCCUP.

       AFFICHAGE-ECRAN.
           MOVE 2550 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           PERFORM AFFICHAGE-CALENDRIER.

       AFFICHAGE-CALENDRIER.
           MOVE 10180000 TO LNK-POSITION.
           CALL "0-DSCAL" USING LINK-V.


       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.


       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       AFFICHAGE-OCCUP.
           INITIALIZE OCC-HELP OCCUP-TOTAL.
           PERFORM READ-MOIS VARYING MOIS FROM 1 BY 1 UNTIL MOIS > 12.
           PERFORM DISPLAY-TOTAL 
                   VARYING MOIS FROM 1 BY 1 UNTIL MOIS > 12.
           MOVE OCCUP-TOTAL TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z4Z2 LINE 22 POSITION 10.
           
       READ-MOIS.
           INITIALIZE JRS-RECORD.
           MOVE FR-KEY TO JRS-FIRME.
           MOVE MOIS      TO JRS-MOIS.
           MOVE REG-PERSON TO JRS-PERSON.
           START JOURS KEY > JRS-KEY INVALID KEY CONTINUE
               NOT INVALID PERFORM READ-HEURES THRU READ-HEURES-END.

       READ-HEURES.
           READ JOURS NEXT AT END
               GO READ-HEURES-END
           END-READ.
           IF FR-KEY NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR MOIS      NOT = JRS-MOIS
               GO READ-HEURES-END
           END-IF.
           IF JRS-COMPLEMENT NOT = 0
              GO READ-HEURES
           END-IF.
           IF JRS-OCCUPATION > 0 AND JRS-OCCUPATION < 12
              COMPUTE LIN-IDX = 9 + MOIS
              MOVE 17 TO COL-IDX
              PERFORM DISPLAY-JOUR-VAL 
                   VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 31
           END-IF.
           GO READ-HEURES.
       READ-HEURES-END.
           EXIT.

       DISPLAY-JOUR-VAL.
           ADD 2 TO COL-IDX.
           IF  JRS-HRS(IDX-1) > 0
               MOVE JRS-HRS(IDX-1) TO HELP-VAL
               ADD  JRS-HRS(IDX-1) TO OCCUP-HELP(MOIS)
               IF JRS-OCCUPATION = 10
               DISPLAY HELP-VAL LINE LIN-IDX POSITION COL-IDX REVERSE
               ELSE
               DISPLAY HELP-VAL LINE LIN-IDX POSITION COL-IDX HIGH.
           IF JRS-HRS(IDX-1) < 0
               DISPLAY "-" LINE LIN-IDX POSITION COL-IDX HIGH.

       DISPLAY-TOTAL.
           COMPUTE LIN-IDX = 9 + MOIS.
           MOVE OCCUP-HELP(MOIS) TO HE-Z4Z2.
           ADD OCCUP-HELP(MOIS) TO OCCUP-TOTAL.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z4Z2 LINE LIN-IDX POSITION 10.

           
       AFFICHAGE-DETAIL.
           PERFORM AFFICHAGE-CALENDRIER.

       DIS-E1-01.
           MOVE REG-PERSON  TO HE-Z6.
           IF REG-PERSON NOT = 0
              MOVE REG-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 
             LINE 3 POSITION 15 SIZE 6
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           MOVE CAR-STATUT TO STAT-CODE.
           CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY.
           DISPLAY STAT-NOM LINE 4 POSITION 77 SIZE 3.
       DIS-E1-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-E1-END.

       END-PROGRAM.
           CLOSE JOURS.
           EXIT PROGRAM.


      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

