      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-GIMPL RECHERCHE IMPRIMANTE LOGIQUE        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.   4-GIMPL.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "IMPRLOG.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "IMPRLOG.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  ACTION                PIC X.
       01  NOT-OPEN              PIC 9 VALUE 0.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".
      *  ENREGISTREMENT LINK IMPRIMANTES LOGIQUES
       01  IMPC-RECORD.
           02  IMPC-KEY.
               03  IMPC-JOB                 PIC X(10).
               03  IMPC-USER                PIC X(10).

           02  IMPC-REC-DET.
               03  IMPC-DESCR               PIC X(40).
               03  IMPC-PRINTER             PIC 99.
               03  IMPC-MACRO               PIC X(40).
               03  IMPC-TEST-PAGE1          PIC X.
               
           03  IMPC-PARAM.
               04 IMPC-PAR     PIC 999 COMP-1 OCCURS 15.

       01  JOB-STANDARD          PIC X(10).

       PROCEDURE DIVISION USING LINK-V IMPC-RECORD JOB-STANDARD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON IMPLOG.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       ACTION SECTION.

       READ-IMPRIMANTE.
           IF NOT-OPEN = 0
              OPEN INPUT IMPLOG
              MOVE 1 TO NOT-OPEN.
           IF IMPC-USER = "X"
              MOVE IMPC-RECORD TO IMPL-RECORD
              MOVE SPACES TO IMPL-USER 
              PERFORM READ-IMPLOG
              EXIT PROGRAM
           END-IF.

           MOVE MENU-PROG-NAME TO IMPL-JOB.
           MOVE LNK-USER TO IMPL-USER.
           PERFORM READ-IMPLOG.
           MOVE SPACES TO IMPL-USER
           PERFORM READ-IMPLOG.
           MOVE JOB-STANDARD TO IMPL-JOB.
           MOVE LNK-USER TO IMPL-USER.
           PERFORM READ-IMPLOG.
           MOVE SPACES TO IMPL-USER
           PERFORM READ-IMPLOG.
           MOVE "AA" TO LNK-AREA.
           MOVE 30 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           ACCEPT ACTION.
       READ-IMPRIMANTE-END.
           MOVE IMPL-RECORD TO IMPC-RECORD.
           EXIT PROGRAM.

       READ-IMPLOG.
           READ IMPLOG INVALID CONTINUE
               NOT INVALID PERFORM READ-IMPRIMANTE-END
           END-READ.

           COPY "XMESSAGE.CPY".
