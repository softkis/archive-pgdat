      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-METIER GESTION DES METIERS                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-METIER.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "METIER.FC".
           SELECT PICKUP ASSIGN TO DISK , "CHAM-MET"
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-PICKUP.


       DATA DIVISION.

       FILE SECTION.

           COPY "METIER.FDE".

       FD  PICKUP
           RECORD CONTAINS 57 CHARACTERS
           LABEL RECORD STANDARD
           DATA RECORD IS PICK-RECORD.

       01  PICK-RECORD.
           02 PICK-NUMBER          PIC 9(5).
           02 PICK-FILLER          PIC XX.
           02 PICK-METIER          PIC X(50).

       WORKING-STORAGE SECTION.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PROF.REC".
           COPY "METIER.LNK".
           COPY "SECTEUR.REC".
           COPY "PARMOD.REC".

       01  CHOIX-MAX             PIC 99 VALUE 9. 
       01  LANGUE                PIC XX. 

       01  HE-SEL.
           02 H-S PIC X       OCCURS 20.

       01   ECR-DISPLAY.
            02 HE-Z2 PIC Z(2).
            02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4).
            02 HE-Z5 PIC Z(5).
            02 HE-Z6 PIC Z(6).
            02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON METIER PICKUP.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-1-METIER.

           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY         TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           OPEN I-O METIER.
           PERFORM AFFICHAGE-ECRAN .
           MOVE LNK-LANGUAGE TO LANGUE SAVE-LANGUAGE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0000680000 TO EXC-KFR(14)
                      MOVE 0000000016 TO EXC-KFR (1)
           WHEN 2     MOVE 0029000000 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (4)
                      MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
           WHEN 5     MOVE 0023640000 TO EXC-KFR (1)
                      MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
           WHEN 6     MOVE 0029000000 TO EXC-KFR (1)
                      MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
           WHEN 8     MOVE 0029000016 TO EXC-KFR (1)
                      MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
           WHEN 9     MOVE 0000000005 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 THRU 4 PERFORM AVANT-DESCR
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 IF H-S(1) NOT = "N" AND NOT = " "
                      PERFORM AVANT-6
                   END-IF
           WHEN  7 IF H-S(2) NOT = "N" AND NOT = " "
                      PERFORM AVANT-7
                   END-IF
           WHEN  8 IF H-S(3) NOT = "N" AND NOT = " "
                      PERFORM AVANT-8
                   END-IF
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  5 PERFORM APRES-5 
           WHEN  6 IF H-S(1) NOT = "N" AND NOT = " "
                      PERFORM APRES-6
                   END-IF
           WHEN  8 IF H-S(3) NOT = "N" AND NOT = " "
                      PERFORM APRES-8
                   END-IF
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           IF LANGUE = SPACES
              MOVE LNK-LANGUAGE TO LANGUE.
           ACCEPT LANGUE
             LINE  4 POSITION 25 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT MET-CODE
             LINE  5 POSITION 25 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-DESCR.
           IF MET-NOM(2) = SPACES
              MOVE MET-NOM(1) TO MET-NOM(2).
           COMPUTE LIN-IDX = 3 + INDICE-ZONE.
           COMPUTE IDX = INDICE-ZONE - 2.
           ACCEPT MET-NOM(IDX)
             LINE  LIN-IDX POSITION 25 SIZE 50
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-5.
           ACCEPT MET-PROF
             LINE  8 POSITION 19 SIZE 5
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-6.
           ACCEPT MET-CM 
             LINE 10 POSITION 25 SIZE 5
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           ACCEPT MET-NUMERO
             LINE 11 POSITION 25 SIZE 8
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM DIS-HE-07.

       AVANT-8.
           ACCEPT MET-SECTEUR
             LINE 12 POSITION 25 SIZE 20
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM DIS-HE-08.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY
           WHEN   5 PERFORM LECT-PICKUP THRU END-PICKUP
           END-EVALUATE.
           IF EXC-KEY = 68
              MOVE "MR" TO LNK-AREA
              CALL "5-SEL" USING LINK-V PARMOD-RECORD
              MOVE PARMOD-SETTINGS TO HE-SEL
              PERFORM AFFICHAGE-ECRAN 
              PERFORM AFFICHAGE-DETAIL
              PERFORM DISPLAY-F-KEYS
           END-IF.
           IF LANGUE = "F"
           OR LANGUE = "D"
           OR LANGUE = "E"
              CONTINUE
           ELSE
              MOVE LNK-LANGUAGE TO LANGUE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           MOVE LANGUE TO MET-LANGUE.
           PERFORM DIS-HE-01.

       APRES-2.
           EVALUATE EXC-KEY
           WHEN   2 MOVE LANGUE TO LNK-LANGUAGE 
                    CALL "2-METIER" USING LINK-V MET-RECORD
                    CANCEL "2-METIER"
                    MOVE SAVE-LANGUAGE TO LNK-LANGUAGE 
                    PERFORM AFFICHAGE-ECRAN 
           WHEN 13  READ METIER NO LOCK INVALID CONTINUE END-READ
                    MOVE LANGUE TO MET-LANGUE
           WHEN 18  PERFORM DEL-TOTAL
                    MOVE 66 TO EXC-KEY
                    PERFORM NEXT-METIER
           WHEN OTHER PERFORM NEXT-METIER.
           IF MET-CODE = SPACES MOVE 1 TO INPUT-ERROR.
           PERFORM AFFICHAGE-DETAIL.


       APRES-5.
           MOVE MET-PROF TO PROF-CODE.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-PROF" USING LINK-V
                    IF LNK-VAL > 0
                       MOVE LNK-VAL TO MET-PROF PROF-CODE
                    END-IF
                    CANCEL "1-PROF"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   2 CALL "2-CITP" USING LINK-V PROF-RECORD
                    MOVE PROF-CODE TO MET-PROF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "2-CITP"
           WHEN   3 CALL "2-PROF" USING LINK-V PROF-RECORD
                    MOVE PROF-CODE TO MET-PROF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   OTHER  PERFORM NEXT-PROF
           END-EVALUATE.
           MOVE PROF-CODE TO MET-PROF.
           PERFORM DIS-HE-05.

       APRES-6.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 
                    MOVE "CM" TO LINK-LANGUE LNK-TEXT
                    MOVE MET-CM TO LINK-CODE-CH
                    PERFORM NEXT-CHM
                    MOVE LINK-CODE-CH TO MET-CM
           WHEN   2 MOVE "CM" TO LINK-LANGUE LNK-TEXT
                    CALL "2-METIER" USING LINK-V LINK-RECORD
                    IF LINK-CODE NOT = SPACES
                       MOVE LINK-CODE-CH TO MET-CM
                    END-IF
                    MOVE SPACES TO LNK-TEXT
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.
           PERFORM DIS-HE-06.

       APRES-8.
           MOVE MET-SECTEUR TO SECT-CODE.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-SECT" USING LINK-V SECT-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   2 CALL "2-SECT" USING LINK-V SECT-RECORD
                    MOVE SECT-CODE TO MET-SECTEUR
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   OTHER  PERFORM NEXT-SECT
           END-EVALUATE.
           MOVE SECT-CODE TO MET-SECTEUR.
           PERFORM DIS-HE-08.

           
       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 MOVE LANGUE TO MET-LANGUE MET-LANGUE-1
                    MOVE MET-CODE   TO MET-CODE-1 LNK-TEXT
                    CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO MET-TIME
                    MOVE LNK-USER   TO MET-USER
                    IF LNK-SQL = "Y" 
                       CALL "9-METIER" USING LINK-V MET-RECORD WR-KEY
                    END-IF
                    WRITE MET-RECORD INVALID 
                        REWRITE MET-RECORD
                    END-WRITE   
                    IF LNK-SQL = "Y"
                       CALL "9-METIER" USING LINK-V MET-RECORD WR-KEY
                    END-IF
                    PERFORM AUTRES-LANGUES
                    MOVE 3 TO DECISION
            WHEN  8 MOVE LANGUE TO MET-LANGUE MET-LANGUE-1
                    IF LNK-SQL = "Y"
                       CALL "9-METIER" USING LINK-V MET-RECORD DEL-KEY
                    END-IF
                    DELETE METIER INVALID CONTINUE END-DELETE
                    INITIALIZE MET-RECORD
                    MOVE 2 TO DECISION
                    PERFORM AFFICHAGE-DETAIL
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION > 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-METIER.
           MOVE LANGUE TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD EXC-KEY.

       NEXT-SECT.
           CALL "6-SECT" USING LINK-V SECT-RECORD EXC-KEY.


       DIS-HE-01.
           DISPLAY LANGUE LINE 4 POSITION 25.
       DIS-HE-02.
           DISPLAY MET-CODE   LINE  5 POSITION 25.
           IF EXC-KEY NOT = 5 MOVE MET-CODE TO LNK-TEXT.
       DIS-HE-03.
           DISPLAY MET-NOM(1)  LINE  6 POSITION 25.
       DIS-HE-04.
           DISPLAY MET-NOM(2)  LINE  7 POSITION 25.
       DIS-HE-05.
           INITIALIZE PROF-RECORD.
           MOVE MET-PROF TO HE-Z5 PROF-CODE.
           DISPLAY HE-Z5 LINE  8 POSITION 19.
           CALL "6-PROF" USING LINK-V PROF-RECORD FAKE-KEY.
           DISPLAY PROF-NOM(1) LINE 8 POSITION 25 SIZE 56.
           DISPLAY PROF-NOM(2) LINE 9 POSITION 25 SIZE 56.
       DIS-HE-06.
           IF H-S(1) NOT = "N" AND NOT = " "
              PERFORM DIS-HE-D6.
       DIS-HE-07.
           IF H-S(2) NOT = "N" AND NOT = " "
              PERFORM DIS-HE-D7.
       DIS-HE-08.
           IF H-S(2) NOT = "N" AND NOT = " "
              PERFORM DIS-HE-D8.
       DIS-HE-END.
           EXIT.

       DIS-HE-D6.
           DISPLAY MET-CM LINE 10 POSITION 25.
           INITIALIZE LINK-RECORD.
           IF MET-CM NOT = 0
              MOVE MET-CM TO HE-Z5
              MOVE HE-Z5 TO LINK-CODE
              MOVE "CM" TO LINK-LANGUE
              CALL "6-METIER" USING LINK-V LINK-RECORD FAKE-KEY
           ELSE
              INITIALIZE LINK-RECORD
           END-IF.
           DISPLAY LINK-CHM LINE 10 POSITION 31.
       DIS-HE-D7.
           MOVE MET-NUMERO TO HE-Z8.
           DISPLAY HE-Z8 LINE 11 POSITION 25.
       DIS-HE-D8.
           MOVE MET-SECTEUR TO SECT-CODE.
           DISPLAY MET-SECTEUR LINE 12 POSITION 25.
           CALL "6-SECT" USING LINK-V SECT-RECORD FAKE-KEY.
           DISPLAY SECT-NOM    LINE 12 POSITION 45 SIZE 35.

       NEXT-PROF.
           CALL "6-PROF" USING LINK-V PROF-RECORD EXC-KEY.

       NEXT-CHM.
           CALL "6-METIER" USING LINK-V LINK-RECORD EXC-KEY.

       AFFICHAGE-ECRAN.
           MOVE 19 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           IF H-S(1) = "N" OR " "
              DISPLAY SPACES LINE 10 POSITION 1 SIZE 80
           END-IF.
           IF H-S(2) = "N" OR " "
              DISPLAY SPACES LINE 11 POSITION 1 SIZE 80
           END-IF.
           IF H-S(3) = "N" OR " "
              DISPLAY SPACES LINE 12 POSITION 1 SIZE 80
           END-IF.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE METIER.
           CANCEL "2-SECT".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
             IF DECISION = 99 
                DISPLAY MET-USER LINE 24 POSITION 5
                DISPLAY MET-ST-JOUR  LINE 24 POSITION 15
                DISPLAY MET-ST-MOIS  LINE 24 POSITION 18
                DISPLAY MET-ST-ANNEE LINE 24 POSITION 21
                DISPLAY MET-ST-HEURE LINE 24 POSITION 30
                DISPLAY MET-ST-MIN   LINE 24 POSITION 33
                GO AVANT-DEC
             END-IF.
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

       AUTRES-LANGUES.
           EVALUATE MET-LANGUE 
               WHEN "F" PERFORM ALLEMAND
                        PERFORM ANGLAIS
                        MOVE "F" TO MET-LANGUE MET-LANGUE-1
               WHEN "D" PERFORM FRANCAIS
                        PERFORM ANGLAIS
                        MOVE "D" TO MET-LANGUE MET-LANGUE-1
               WHEN "E" PERFORM ALLEMAND
                        PERFORM FRANCAIS
                        MOVE "E" TO MET-LANGUE MET-LANGUE-1
           END-EVALUATE.
           PERFORM TEST-EXIST.
                         
       ALLEMAND.     
           MOVE "D" TO MET-LANGUE MET-LANGUE-1
           PERFORM TEST-EXIST.
       ANGLAIS.     
           MOVE "E" TO MET-LANGUE MET-LANGUE-1
           PERFORM TEST-EXIST.
       FRANCAIS.     
           MOVE "F" TO MET-LANGUE MET-LANGUE-1
           PERFORM TEST-EXIST.

       TEST-EXIST.     
           READ METIER NO LOCK INVALID CONTINUE 
           NOT INVALID PERFORM ADAPTE END-READ.

       ADAPTE.
           IF PROF-CODE > 0
              MOVE PROF-CODE TO MET-PROF
           END-IF.
           IF LINK-CODE-CH > 0
              MOVE LINK-CODE-CH TO MET-CM
           END-IF.
           CALL "0-TODAY" USING TODAY.
           MOVE TODAY-TIME TO MET-TIME.
           MOVE LNK-USER   TO MET-USER.
           REWRITE MET-RECORD INVALID CONTINUE.

       DEL-TOTAL.
           MOVE "F" TO MET-LANGUE MET-LANGUE-1
           IF LNK-SQL = "Y"
              CALL "9-METIER" USING LINK-V MET-RECORD DEL-KEY
           END-IF
           DELETE METIER INVALID CONTINUE END-DELETE
           MOVE "D" TO MET-LANGUE MET-LANGUE-1
           IF LNK-SQL = "Y"
              CALL "9-METIER" USING LINK-V MET-RECORD DEL-KEY
           END-IF
           DELETE METIER INVALID CONTINUE END-DELETE
           MOVE "D" TO MET-LANGUE MET-LANGUE-1
           IF LNK-SQL = "Y"
              CALL "9-METIER" USING LINK-V MET-RECORD DEL-KEY
           END-IF
           DELETE METIER INVALID CONTINUE END-DELETE.
           MOVE LANGUE TO MET-LANGUE.

       LECT-PICKUP.
           INITIALIZE MET-RECORD.
           MOVE "CM" TO MET-LANGUE.
           PERFORM DELETE-CM THRU DELETE-CM-FIN.
           INITIALIZE MET-RECORD.
           OPEN INPUT PICKUP.
       LECT-PICKUP-1.
           READ PICKUP AT END GO END-PICKUP.
           INSPECT PICK-RECORD REPLACING ALL '"' BY " ".
           DISPLAY PICK-NUMBER LINE 13 POSITION 5.
           DISPLAY PICK-METIER LINE 13 POSITION 15.
           MOVE "CM"        TO MET-LANGUE MET-LANGUE-1.
           MOVE PICK-NUMBER TO MET-CODE MET-CODE-1.
           MOVE PICK-METIER TO MET-CHM.
           WRITE MET-RECORD INVALID REWRITE MET-RECORD END-WRITE.
           INITIALIZE PICK-RECORD.
           GO LECT-PICKUP-1.
       END-PICKUP.
           CLOSE PICKUP.

       DELETE-CM.
           START METIER KEY > MET-KEY INVALID GO DELETE-CM-FIN.
       DELETE-CM-1.
           READ METIER NEXT NO LOCK AT END GO DELETE-CM-FIN END-READ.
           IF MET-LANGUE = "CM"
              DELETE METIER INVALID CONTINUE END-DELETE
              GO DELETE-CM.
           GO DELETE-CM-1.
       DELETE-CM-FIN.
           
