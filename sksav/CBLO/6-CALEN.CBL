      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-CALEN MODULE GENERAL LECTURE CALENDRIER   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-CALEN.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CALEN.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CALEN.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  WR-KEY           PIC 9(4) COMP-1 VALUE 99.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "CALEN.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING  LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CALENDRIER.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-REG.
       
           IF LINK-NUMBER = 0
              GO EXIT-1.
       
           IF NOT-OPEN = 0
              OPEN I-O CALENDRIER
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CAL-RECORD.
           MOVE LNK-ANNEE TO CAL-ANNEE.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ CALENDRIER PREVIOUS NO LOCK AT END GO EXIT-1 
               END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ CALENDRIER NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ CALENDRIER NO LOCK INVALID 
                GO EXIT-3
                END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF LNK-ANNEE NOT = CAL-ANNEE 
              GO EXIT-1
           END-IF.
           MOVE CAL-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-3.
           INITIALIZE CAL-REC-DET.
           EVALUATE LNK-ANNEE
                WHEN 1996 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 4,  8)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 5, 16)
                CAL-JOUR( 5, 27)
                CAL-JOUR( 6, 24)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
                WHEN 1997 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 3, 31)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 5,  8)
                CAL-JOUR( 5, 19)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
                MOVE 2 TO CAL-JOUR(11,  1)
                WHEN 1998 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 4, 13)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 5, 21)
                CAL-JOUR( 6,  1)
                CAL-JOUR( 6, 23)
                CAL-JOUR(11,  2)
                CAL-JOUR(12, 25)
                MOVE 2 TO 
                CAL-JOUR( 8, 15)
                CAL-JOUR(12, 26)
                WHEN 1999 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 4,  5)
                CAL-JOUR( 5, 13)
                CAL-JOUR( 5, 24)
                CAL-JOUR( 6, 23)
                CAL-JOUR(11,  1)
                MOVE 2 TO 
                CAL-JOUR( 5,  1)
                CAL-JOUR( 8, 15)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
                WHEN 2000 MOVE 1 TO
                CAL-JOUR( 4, 24)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 6,  1)
                CAL-JOUR( 6, 12)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
                MOVE 2 TO 
                CAL-JOUR( 1,  1)
                WHEN 2001 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 4, 16)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 5, 24)
                CAL-JOUR( 6,  4)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
                CAL-JOUR( 6, 23)
                WHEN 2002 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 5,  9)
                CAL-JOUR( 5, 20)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
                WHEN 2003 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
mob
                CAL-JOUR( 5, 29)
                CAL-JOUR( 4, 21)
                CAL-JOUR( 6,  9)
                WHEN 2004 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
mob
                CAL-JOUR( 5, 20)
                CAL-JOUR( 4, 12)
                CAL-JOUR( 5, 31)
                WHEN 2005 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
mob
                CAL-JOUR( 5,  5)
                CAL-JOUR( 3, 28)
                CAL-JOUR( 5, 16)
                WHEN 2006 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
mob
                CAL-JOUR( 5, 25)
                CAL-JOUR( 4, 17)
                CAL-JOUR( 6,  5)
                WHEN 2007 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
mob
                CAL-JOUR( 5, 17)
                CAL-JOUR( 4,  9)
                CAL-JOUR( 5, 28)
                WHEN 2008 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
mob
!!              CAL-JOUR( 5,  1)
                CAL-JOUR( 3, 24)
                CAL-JOUR( 5, 12)
                WHEN 2009 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
mob
                CAL-JOUR( 5, 21)
                CAL-JOUR( 4, 13)
                CAL-JOUR( 6,  1)
                WHEN 2010 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
mob
                CAL-JOUR( 5, 13)
                CAL-JOUR( 4,  5)
                CAL-JOUR( 5, 24)
                WHEN 2011 MOVE 1 TO
                CAL-JOUR( 1,  1)
                CAL-JOUR( 5,  1)
                CAL-JOUR( 6, 23)
                CAL-JOUR( 8, 15)
                CAL-JOUR(11,  1)
                CAL-JOUR(12, 25)
                CAL-JOUR(12, 26)
mob
                CAL-JOUR( 2,  6)
                CAL-JOUR( 4, 25)
                CAL-JOUR( 6, 13)
           END-EVALUATE.
           IF LNK-SQL = "Y" 
              CALL "9-CALEN" USING LINK-V CAL-RECORD WR-KEY
           END-IF.
           WRITE CAL-RECORD INVALID CONTINUE.
           GO EXIT-2.

       START-1.
           START CALENDRIER KEY < CAL-KEY INVALID GO EXIT-1.
       START-2.
           START CALENDRIER KEY > CAL-KEY INVALID GO EXIT-1.


