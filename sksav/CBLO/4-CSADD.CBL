      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-CSADD REMISE CODES SALAIRES DANS LIVRE    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-CSADD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODPAIE.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CODPAIE.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CODSAL.REC".

       01  COMPTEUR              PIC 999 VALUE 0.
       01  NOT-OPEN              PIC 9 VALUE 0.

       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-CODPAIE      PIC 999.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "LIVRE.REC".


       PROCEDURE DIVISION USING LINK-V REG-RECORD L-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON CODPAIE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-CSADD.
       
           IF NOT-OPEN = 1 
              IF  LNK-SUFFIX-D NOT = 0
              AND LNK-SUFFIX-D NOT = ANNEE-CODPAIE
                 CLOSE CODPAIE
                 MOVE 0 TO NOT-OPEN
              ELSE
                 IF LNK-SUFFIX NOT = ANNEE-CODPAIE
                    CLOSE CODPAIE
                    MOVE 0 TO NOT-OPEN
                  END-IF
              END-IF
           END-IF.

           IF NOT-OPEN = 0
              IF LNK-SUFFIX-D NOT = 0
                 MOVE LNK-SUFFIX-D TO ANNEE-CODPAIE
              ELSE
                 MOVE LNK-SUFFIX TO ANNEE-CODPAIE
              END-IF
              OPEN I-O CODPAIE
              MOVE 1 TO NOT-OPEN
           END-IF.


       CREATE.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY     TO CSP-FIRME-2.
           MOVE REG-PERSON TO CSP-PERSON-2.
           MOVE LNK-MOIS   TO CSP-MOIS-2.
           MOVE LNK-SUITE  TO CSP-SUITE-2.

           START CODPAIE KEY > CSP-KEY-2 INVALID CONTINUE
                NOT INVALID PERFORM READ-CSP THRU READ-END.
           EXIT PROGRAM.
                
       READ-CSP.
           READ CODPAIE NEXT NO LOCK AT END GO READ-END.
           IF FR-KEY     NOT = CSP-FIRME
           OR REG-PERSON NOT = CSP-PERSON
           OR LNK-MOIS   NOT = CSP-MOIS
           OR LNK-SUITE  NOT = CSP-SUITE
              GO READ-END
           END-IF.
           ADD 1 TO COMPTEUR.
           MOVE CSP-CODE TO CS-NUMBER.
           CALL "6-CODSAL" USING LINK-V CS-RECORD FAKE-KEY.
           CALL "4-LPVENT" USING LINK-V CSP-RECORD CS-RECORD L-RECORD.
           GO READ-CSP.
       READ-END.
           EXIT.

           COPY "Xaction.CPY".
