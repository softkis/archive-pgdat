      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-LEX  GESTION DESCRIPTION LIVRE DE PAIE    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-LEX.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "LIVREX.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "LIVREX.FDE".

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 4.  

       01   ECR-DISPLAY.
            02 HE-03 PIC ZZZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON LIVREX.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-LEX.

           MOVE 0 TO LEX-NUMBER. 
           MOVE LNK-LANGUAGE TO SAVE-LANGUAGE LEX-LANGUAGE.
           OPEN I-O LIVREX.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.
      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0129002200 TO EXC-KFR (1)
                      MOVE 2100080000 TO EXC-KFR (2)
                      MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
                      MOVE 0012130000 TO EXC-KFR (3)
           WHEN 4     MOVE 0100000005 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 23 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 12 THRU 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT LEX-LANGUAGE
             LINE  4 POSITION 30 SIZE 1 
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT LEX-NUMBER  
             LINE  6 POSITION 25 SIZE  4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.            
           ACCEPT LEX-DESCRIPTION
             LINE  7 POSITION 25 SIZE 50
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE LEX-LANGUAGE 
                WHEN "F"   MOVE 0 TO INPUT-ERROR 
                WHEN "D"   MOVE 0 TO INPUT-ERROR 
                WHEN "E"   MOVE 0 TO INPUT-ERROR 
                WHEN OTHER MOVE 1 TO INPUT-ERROR.
       
       APRES-2.
           MOVE LEX-LANGUAGE TO LNK-LANGUAGE.
           EVALUATE EXC-KEY
           WHEN   1 MOVE LEX-NUMBER TO LNK-NUM
                    CALL "2-LEXCS" USING LINK-V 
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   4 PERFORM COPY-LIVREX
           WHEN  65 THRU 66 PERFORM NEXT-LIVREX
           WHEN   2 MOVE LEX-LANGUAGE TO LNK-LANGUAGE
                    CALL "2-LEX" USING LINK-V LEX-RECORD
                    MOVE SAVE-LANGUAGE TO LNK-LANGUAGE LEX-LANGUAGE 
                    PERFORM AFFICHAGE-ECRAN 
            WHEN  8 PERFORM APRES-DEC
            WHEN  6 ADD 1 TO LEX-NUMBER INDICE-ZONE
            WHEN 12 ADD 1 TO LEX-NUMBER
           END-EVALUATE.
           IF  EXC-KEY NOT = 4
           AND EXC-KEY NOT = 12
              PERFORM READ-LIVREX.
           PERFORM DIS-HE-01 THRU DIS-HE-END.


       APRES-3.
           EVALUATE EXC-KEY
           WHEN 12 MOVE 5 TO EXC-KEY
           WHEN 13 MOVE 5 TO EXC-KEY
                   PERFORM APRES-DEC
           END-EVALUATE.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 WRITE LEX-RECORD INVALID 
                        REWRITE LEX-RECORD
                    END-WRITE   
                    MOVE 3 TO DECISION
                    PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN  8 DELETE LIVREX  INVALID CONTINUE
                    END-DELETE
                    INITIALIZE LEX-NUMBER LEX-DESCRIPTION
                    MOVE 3 TO DECISION
                   PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       COPY-LIVREX.   
           MOVE LEX-NUMBER TO IDX-2.
           ACCEPT LEX-NUMBER
             LINE  6 POSITION 60 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           READ LIVREX INVALID CONTINUE
                NOT INVALID
                MOVE IDX-2 TO LEX-NUMBER
                WRITE LEX-RECORD INVALID CONTINUE END-WRITE
                END-READ.


       READ-LIVREX.
           MOVE EXC-KEY TO SAVE-KEY.
           MOVE 13 TO EXC-KEY.
           PERFORM NEXT-LIVREX.
           MOVE SAVE-KEY TO EXC-KEY.

       NEXT-LIVREX.
           MOVE LEX-LANGUAGE TO LNK-LANGUAGE.
           CALL "6-LEX" USING LINK-V LEX-RECORD EXC-KEY.
           MOVE SAVE-LANGUAGE TO LNK-LANGUAGE LEX-LANGUAGE.

       NEXT-NUMBER .
           MOVE 0 TO NOT-FOUND.
           START LIVREX KEY > LEX-KEY INVALID KEY
                INITIALIZE LEX-NUMBER LEX-DESCRIPTION NOT INVALID 
                PERFORM NEXT-FNUMBER-1 UNTIL NOT-FOUND = 1.
           INITIALIZE LEX-DESCRIPTION.
           ADD 1 TO LEX-NUMBER.
           MOVE 3 TO INDICE-ZONE.
                
       NEXT-FNUMBER-1.
           READ LIVREX NEXT AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
                
       DIS-HE-01.
           DISPLAY LEX-LANGUAGE LINE 4 POSITION 30.
       DIS-HE-02.
           MOVE LEX-NUMBER TO HE-03.
           DISPLAY HE-03  LINE  6 POSITION 25.
       DIS-HE-03.
           DISPLAY LEX-DESCRIPTION LINE  7 POSITION 25.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 12 TO LNK-VAL.
           MOVE "   " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE LIVREX.
           CANCEL "2-LEX".
           CANCEL "6-LEX".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
