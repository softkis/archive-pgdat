      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-RECAP IMPRESSION RECAPITULATION SALAIRES  �
      *  � PAR PERSONNE DE MOIS DEBUT A MOIS FIN                 �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-RECAP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM130.FDE".
           COPY "TRIPR.FDE".
      

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "130       ".
       01  CHOIX-MAX             PIC 99 VALUE 10.
       01  MAX-LIGNES            PIC 9 VALUE 2.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "LIVRE.REC".
           COPY "METIER.REC".
           COPY "JOURS.REC".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

       01  COLONNE-BASE          PIC 999 VALUE 0.    
       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.
       01  DOKUMENT              PIC 9 VALUE 1.
       01  POINT-IDX             PIC 99.
       01  COMPTEUR              PIC 9999 COMP-1.
       01  CHOIX                 PIC 99 VALUE 0.

       01  DEBUT-FIN.
           02 DEBUT-J PIC 99.
           02 DEBUT-M PIC 99.
           02 FIN-J PIC 99.
           02 FIN-M PIC 99.

       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

       01 HELP-CUMUL.
          02 HELP-HEURES.
             03 HELP-HRS             PIC 9(8)V99 OCCURS 7.
          02 HELP-HEURES-R REDEFINES HELP-HEURES.
             03 HELP-HRS-PROD-TOT    PIC 9(8)V99.
             03 HELP-HRS-PROD-SUPPL  PIC 9(8)V99.
             03 HELP-HRS-CONGE-TOT   PIC 9(8)V99.
             03 HELP-HRS-EXTRA-TOT   PIC 9(8)V99.
             03 HELP-HRS-CHOM-TOT    PIC 9(8)V99.
             03 HELP-HRS-MALAD-TOT   PIC 9(8)V99.
             03 HELP-HRS-CUMUL       PIC 9(8)V99.

          02 HELP-MONTANT.
             03 HELP-VAL             PIC S9(8)V99 OCCURS 37.
          02 HELP-MONTANT-R REDEFINES HELP-MONTANT.
1            03 HELP-BASE            PIC S9(8)V99.
2            03 HELP-HS              PIC S9(8)V99.
3            03 HELP-NDF             PIC S9(8)V99.
4            03 HELP-PRIMES          PIC S9(8)V99.
5            03 HELP-BRUT            PIC S9(8)V99.
6            03 HELP-COTIS-MAL-SAL   PIC S9(8)V99.
7            03 HELP-COTIS-PEN-SAL   PIC S9(8)V99.
8            03 HELP-SHS-FRANCHISE   PIC S9(8)V99.
9            03 HELP-NDF-FRANCHISE   PIC S9(8)V99.
10           03 HELP-ABAT-TOT        PIC S9(8)V99.
11           03 HELP-EXEMPTION       PIC S9(8)V99.
12           03 HELP-IMPOSABLE       PIC S9(8)V99.
13           03 HELP-IMPOTS          PIC S9(8)V99.
14           03 HELP-COTIS-DEP-SAL   PIC S9(8)V99.
15           03 HELP-IMPOT-SPECIAL   PIC S9(8)V99.
16           03 HELP-NET             PIC S9(8)V99.
17           03 HELP-BRUT-PER        PIC S9(8)V99.
18           03 HELP-COTIS-MAL-SAL-P PIC S9(8)V99.
19           03 HELP-COTIS-PEN-SAL-P PIC S9(8)V99.
20           03 HELP-IMPOSABLE-PER   PIC S9(8)V99.
21           03 HELP-IMPOTS-PER      PIC S9(8)V99.
22           03 HELP-COTIS-DEP-SAL-P PIC S9(8)V99.
23           03 HELP-NET-PER         PIC S9(8)V99.
24           03 HELP-DECOMPTE        PIC S9(8)V99.
25           03 HELP-IMPOTS-TOT      PIC S9(8)V99.
26           03 HELP-NET-TOTAL       PIC S9(8)V99.
27           03 HELP-CMO-PLUS        PIC S9(8)V99.
28           03 HELP-TOTAL-PLUS      PIC S9(8)V99.
29           03 HELP-TOTAL-MINUS     PIC S9(8)V99.
30           03 HELP-A-PAYER         PIC S9(8)V99.

31           03 HELP-COTIS-MAL-PAT   PIC S9(8)V99.
32           03 HELP-COTIS-PEN-PAT   PIC S9(8)V99.
33           03 HELP-COTIS-ACC-PAT   PIC S9(8)V99.
34           03 HELP-COTIS-FAM-PAT   PIC S9(8)V99.
35           03 HELP-COTIS-SAN-PAT   PIC S9(8)V99.
36           03 HELP-IMPOT-FORFAIT   PIC S9(8)V99.
37           03 HELP-COUT-TOTAL      PIC S9(8)V99.
          02 HELP-JRS-IMP            PIC 9(8).

       01 TOTAL-CUMUL.
          02 TOTAL-HEURES.
             03 TOTAL-HRS             PIC 9(8)V99 OCCURS 7.

          02 TOTAL-MONTANT.
             03 TOTAL-VAL             PIC S9(8)V99 OCCURS 37.
          02 TOTAL-JRS-IMP            PIC 9(8).


       01 LINE-CUMUL.
          02 LINE-HEURES.
             03 LINE-HRS-PROD-TOT    PIC 999 VALUE 18.
             03 LINE-HRS-PROD-SUPPL  PIC 999 VALUE 19.
             03 LINE-HRS-CONGE-TOT   PIC 999 VALUE 20.
             03 LINE-HRS-EXTRA-TOT   PIC 999 VALUE 21.
             03 LINE-HRS-CHOM-TOT    PIC 999 VALUE 22.
             03 LINE-HRS-MALAD-TOT   PIC 999 VALUE 23.
             03 LINE-HRS-CUMUL       PIC 999 VALUE 16.
          02 LINE-HEURES-R REDEFINES LINE-HEURES.
             03 LINE-HRS             PIC 999 OCCURS 7.

          02 LINE-MONTANT.
1            03 LINE-BASE            PIC 999 VALUE 25.
2            03 LINE-HS              PIC 999 VALUE 26.
3            03 LINE-NDF             PIC 999 VALUE 27.
4            03 LINE-PRIMES          PIC 999 VALUE 28.
5            03 LINE-BRUT            PIC 999 VALUE 30.
6            03 LINE-COTIS-MAL-SAL   PIC 999 VALUE 32.
7            03 LINE-COTIS-PEN-SAL   PIC 999 VALUE 33.
8            03 LINE-SHS-FRANCHISE   PIC 999 VALUE 34.
9            03 LINE-NDF-FRANCHISE   PIC 999 VALUE 35.
10           03 LINE-ABAT-TOT        PIC 999 VALUE 36.
11           03 LINE-EXEMPTION       PIC 999 VALUE 37.
12           03 LINE-IMPOSABLE       PIC 999 VALUE 38.
13           03 LINE-IMPOTS          PIC 999 VALUE 39.
14           03 LINE-COTIS-DEP-SAL   PIC 999 VALUE 40.
15           03 LINE-IMPOT-SPECIAL   PIC 999 VALUE 41.
16           03 LINE-NET             PIC 999 VALUE 43.
17           03 LINE-BRUT-PER        PIC 999 VALUE 45.
18           03 LINE-COTIS-MAL-SAL-P PIC 999 VALUE 47.
19           03 LINE-COTIS-PEN-SAL-P PIC 999 VALUE 48.
20           03 LINE-IMPOSABLE-PER   PIC 999 VALUE 49.
21           03 LINE-IMPOTS-PER      PIC 999 VALUE 50.
22           03 LINE-COTIS-DEP-SAL-P PIC 999 VALUE 51.
23           03 LINE-NET-PER         PIC 999 VALUE 53.
24           03 LINE-DECOMPTE        PIC 999 VALUE 54.
25           03 LINE-IMPOTS-TOT      PIC 999 VALUE 55.
26           03 LINE-NET-TOTAL       PIC 999 VALUE 56.
27           03 LINE-CMO-PLUS        PIC 999 VALUE 58.
28           03 LINE-TOTAL-PLUS      PIC 999 VALUE 59.
29           03 LINE-TOTAL-MINUS     PIC 999 VALUE 60.
30           03 LINE-A-PAYER         PIC 999 VALUE 61.

31           03 LINE-COTIS-MAL-PAT   PIC 999 VALUE 63.
32           03 LINE-COTIS-PEN-PAT   PIC 999 VALUE 64.
33           03 LINE-COTIS-ACC-PAT   PIC 999 VALUE 65.
34           03 LINE-COTIS-FAM-PAT   PIC 999 VALUE 66.
35           03 LINE-COTIS-SAN-PAT   PIC 999 VALUE 67.
36           03 LINE-IMPOT-FORFAIT   PIC 999 VALUE 68.
37           03 LINE-COUT-TOTAL      PIC 999 VALUE 69.
          02 LINE-MONTANT-R REDEFINES LINE-MONTANT.
             03 LINE-VAL             PIC 999 OCCURS 37.

           COPY "V-VH00.CPY".


       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".RAL".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  PERSONNE              PIC 9(6) VALUE 0.
       01  BEG-PERSON            PIC 9(6).
       01  BEG-MATCHCODE         PIC X(10).
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-RECAP.
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE LNK-MOIS TO SAVE-MOIS MOIS-DEBUT MOIS-FIN.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TEST-EXTENSION TOTAL-CUMUL.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7 THRU 8
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 10    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN 10 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-8
           WHEN  9 PERFORM APRES-SORT
           WHEN 10 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT DOKUMENT 
             LINE 13 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           IF DOKUMENT < 1 OR > 3
              MOVE 1 TO INPUT-ERROR DOKUMENT
           END-IF.
           PERFORM DIS-HE-06.

       APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-07.

       APRES-8.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM TRAITEMENT-RUN
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM DIS-HE-01.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-3
           END-IF.
           IF CHOIX > 0
           AND CHOIX < 90
              IF TEST-ALPHA  NOT = SPACES
              OR TEST-NUMBER NOT = 0
                 IF TRIPR-CHOIX NOT = TEST-EXTENSION
                    INITIALIZE DEBUT-FIN
                    MOVE TOTAL-CUMUL TO HELP-CUMUL
                    IF HELP-COUT-TOTAL  > 0
                    OR HELP-A-PAYER NOT = 0
                    OR HELP-TOTAL-PLUS NOT = 0
                    OR HELP-TOTAL-MINUS NOT = 0 
                       PERFORM FILL-FILES
                       PERFORM TRANSMET
                    END-IF
                    INITIALIZE TOTAL-CUMUL LIN-NUM COLONNE-BASE 
                    TEST-EXTENSION 
                 END-IF
              END-IF
           END-IF.

           PERFORM CUMUL-LIVRE.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.
           EXIT.


       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

       METIER.
           MOVE CAR-METIER TO MET-CODE.
           MOVE "F "       TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           MOVE 20 TO LIN-NUM.
           MOVE 10 TO COL-NUM.
           PERFORM FILL-FORM.

           COPY "XDIS.CPY".
       DIS-HE-06.
           DISPLAY DOKUMENT LINE 13 POSITION 32.
       DIS-HE-07.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 19 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-08.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       DETAIL-CHOIX.
           MOVE 12 TO LIN-NUM.
           MOVE COLONNE-BASE TO COL-NUM
           MOVE TRIPR-CHOIX TO TEST-EXTENSION
           MOVE TEST-NUMBER TO LNK-POSITION.
           MOVE TEST-ALPHA  TO LNK-TEXT.
           MOVE CHOIX       TO LNK-NUM.
           CALL "4-CHOIX" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       TRAITEMENT-RUN.
           IF CHOIX NOT = 0
              PERFORM CALL-SORT 
              MOVE LNK-USER  TO USER-TRIPR
              MOVE FR-KEY TO REG-FIRME FIRME-TRIPR
              OPEN I-O TRIPR
              PERFORM START-TRI
              PERFORM READ-TRI THRU READ-TRI-END
              PERFORM END-PROGRAM
           END-IF. 
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.
           PERFORM END-PROGRAM.


       CUMUL-LIVRE.
           INITIALIZE HELP-CUMUL L-RECORD DEBUT-FIN LNK-SUITE.
           MOVE MOIS-DEBUT TO LNK-MOIS L-MOIS.
           MOVE 67 TO SAVE-KEY.
           PERFORM READ-LIVRE THRU READ-LIVRE-END.

       READ-LIVRE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD SAVE-KEY.
           MOVE 66 TO SAVE-KEY.
           IF L-FIRME  = 0
           OR L-PERSON = 0
           OR L-MOIS   > MOIS-FIN
              GO READ-LIVRE-END.
           IF  STATUT > 0
           AND STATUT NOT = L-STATUT  
               GO READ-LIVRE
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = L-COUT
               GO READ-LIVRE
           END-IF.
           IF L-FLAG-AVANCE > 0
              GO READ-LIVRE
           END-IF.
           EVALUATE DOKUMENT
           WHEN 1 IF L-SUITE NOT = 0
                     GO READ-LIVRE
                  END-IF
           WHEN 3 IF L-SUITE = 0
                     GO READ-LIVRE
                  END-IF
           END-EVALUATE. 

           IF DOKUMENT > 2
              MOVE L-JOUR-FIN TO FIN-J
              MOVE L-MOIS     TO FIN-M
              IF DEBUT-J  = 0 
                 MOVE L-JOUR-DEBUT TO DEBUT-J
                 MOVE L-MOIS       TO DEBUT-M
              END-IF
           ELSE
              IF L-SUITE = 0
                 IF DEBUT-J  = 0 
                    MOVE L-JOUR-DEBUT TO DEBUT-J
                    MOVE L-MOIS       TO DEBUT-M
                 END-IF
                 MOVE L-JOUR-FIN TO FIN-J
                 MOVE L-MOIS     TO FIN-M
              END-IF
           END-IF.

           IF L-SUITE = 0
              PERFORM CUMUL-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6
           ELSE
              IF DOKUMENT = 3
              PERFORM CUMUL-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.

           PERFORM CUMUL VARYING IDX FROM 1 BY 1 UNTIL IDX > 36.
           ADD  L-ABATTEMENT TO HELP-VAL(10).
           ADD  L-SHS-AJUSTE TO HELP-VAL(8).
           ADD  L-NDF-FRANCHISE TO HELP-VAL(9).
           ADD  L-ETR-FRANCHISE TO HELP-VAL(11).
           ADD  L-CON-FRANCHISE TO HELP-VAL(11).
           ADD  L-HYP-FRANCHISE TO HELP-VAL(11).
           ADD  L-AGR-FRANCHISE TO HELP-VAL(11).
           ADD  L-DOM-FRANCHISE TO HELP-VAL(11).
           ADD  L-IMPOSABLE-NET TO HELP-VAL(12).
           ADD  L-IMPOSABLE-NET-NP TO HELP-VAL(20).
QQ         ADD  L-DECOMPTE-IMPOT TO HELP-VAL(26).
           ADD  L-SALAIRE-NET    TO HELP-VAL(16) HELP-VAL(26).
           ADD  L-SALAIRE-NET-NP TO HELP-VAL(23) HELP-VAL(26).
           COMPUTE HELP-VAL(25) = HELP-VAL(13) + HELP-VAL(15) 
           + HELP-VAL(21).
           COMPUTE HELP-VAL(5) = HELP-VAL(1) + HELP-VAL(2) 
           + HELP-VAL(3) + HELP-VAL(4).
           COMPUTE HELP-VAL(37) = HELP-VAL(5) + HELP-VAL(17) 
           + HELP-VAL(31) + HELP-VAL(32) + HELP-VAL(33) + HELP-VAL(34)
           + HELP-VAL(35) + HELP-VAL(36).
           SUBTRACT L-DC(200) FROM HELP-VAL(30).
           ADD L-JRS-IMP-TRAV TO HELP-JRS-IMP TOTAL-JRS-IMP.
           GO READ-LIVRE.
       READ-LIVRE-END.
           IF HELP-COUT-TOTAL  > 0
           OR HELP-A-PAYER     NOT = 0
           OR HELP-TOTAL-PLUS  NOT = 0
           OR HELP-TOTAL-MINUS NOT = 0 
              PERFORM FILL-FILES.

       CUMUL.
           EVALUATE IDX 
                WHEN 1 MOVE 100 TO IDX-2
                       MOVE 104 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE  88 TO IDX-2
                       MOVE  90 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE  11 TO IDX-2
                       MOVE  50 TO IDX-3
                WHEN 2 MOVE  95 TO IDX-2
                       MOVE  98 TO IDX-3
                WHEN 3 MOVE  61 TO IDX-2
                       MOVE  80 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE  91 TO IDX-2
                       MOVE  93 TO IDX-3
                WHEN 4 MOVE  51 TO IDX-2
                       MOVE  60 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE  81 TO IDX-2
                       MOVE  87 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE 104 TO IDX-2
                       MOVE 110 TO IDX-3
                WHEN 6 MOVE 231 TO IDX-2
                       MOVE 231 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE 236 TO IDX-2
                       MOVE 239 TO IDX-3
                WHEN 7 MOVE 232 TO IDX-2
                       MOVE 232 TO IDX-3

                WHEN 13 MOVE 201 TO IDX-2
                        MOVE 201 TO IDX-3
                WHEN 14 MOVE 235 TO IDX-2
                        MOVE 235 TO IDX-3
                WHEN 15 MOVE 203 TO IDX-2
                        MOVE 206 TO IDX-3
                WHEN 17 MOVE 111 TO IDX-2
                        MOVE 130 TO IDX-3
                WHEN 18 MOVE 241 TO IDX-2
                        MOVE 241 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE 246 TO IDX-2
                       MOVE 249 TO IDX-3
                WHEN 19 MOVE 242 TO IDX-2
                        MOVE 242 TO IDX-3
                WHEN 21 MOVE 202 TO IDX-2
                        MOVE 202 TO IDX-3
                WHEN 22 MOVE 245 TO IDX-2
                        MOVE 245 TO IDX-3
                WHEN 24 MOVE 133 TO IDX-2
                        MOVE 133 TO IDX-3
                WHEN 27 MOVE   1 TO IDX-2
                        MOVE   9 TO IDX-3
                WHEN 28 MOVE 181 TO IDX-2
                        MOVE 199 TO IDX-3
                WHEN 29 MOVE 266 TO IDX-2
                        MOVE 299 TO IDX-3
                WHEN 30 IF L-SUITE = 0
                           MOVE 300 TO IDX-2 IDX-3
                        ELSE 
                           MOVE 0 TO IDX-2 IDX-3
                        END-IF
                WHEN 31 MOVE 161 TO IDX-2
                        MOVE 161 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                        MOVE 171 TO IDX-2
                        MOVE 171 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                        MOVE 166 TO IDX-2
                        MOVE 169 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                        MOVE 176 TO IDX-2
                        MOVE 179 TO IDX-3


                WHEN 32 MOVE 162 TO IDX-2
                        MOVE 162 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                        MOVE 172 TO IDX-2
                        MOVE 172 TO IDX-3
                WHEN 33 MOVE 170 TO IDX-2
                        MOVE 170 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                        MOVE 180 TO IDX-2
                        MOVE 180 TO IDX-3
                WHEN 34 MOVE 163 TO IDX-2
                        MOVE 163 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                        MOVE 173 TO IDX-2
                        MOVE 173 TO IDX-3
                WHEN 35 MOVE 164 TO IDX-2
                        MOVE 164 TO IDX-3
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                        MOVE 174 TO IDX-2
                        MOVE 174 TO IDX-3
                WHEN 36 MOVE 141 TO IDX-2
                        MOVE 149 TO IDX-3
                WHEN OTHER MOVE 0 TO IDX-2 IDX-3
           END-EVALUATE.
           IF IDX-2 > 0
           PERFORM DC VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3.

       DC.
           ADD L-DC(IDX-1) TO HELP-VAL(IDX).


       CUMUL-HRS.
           EVALUATE IDX 
                WHEN 1 MOVE 100 TO IDX-2
                       MOVE 100 TO IDX-3
           PERFORM HR VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE  88 TO IDX-2
                       MOVE  90 TO IDX-3
                WHEN 2 MOVE  95 TO IDX-2
                       MOVE  95 TO IDX-3
                WHEN 3 MOVE  20 TO IDX-2
                       MOVE  20 TO IDX-3
                WHEN 4 MOVE  13 TO IDX-2
                       MOVE  16 TO IDX-3
           PERFORM HR VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE  21 TO IDX-2
                       MOVE  28 TO IDX-3
           PERFORM HR VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE 132 TO IDX-2
                       MOVE 132 TO IDX-3
                WHEN 5 MOVE  41 TO IDX-2
                       MOVE  43 TO IDX-3
                WHEN 6 MOVE 1 TO IDX-2
                       MOVE 9 TO IDX-3
           PERFORM HR VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE  11 TO IDX-2
                       MOVE  11 TO IDX-3
           END-EVALUATE.
           IF L-SUITE = 0
           PERFORM HR VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3.

       HR.
           ADD L-UNITE(IDX-1) TO HELP-HRS(IDX) HELP-HRS(7).

       FILL-FILES.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO COLONNE-BASE
              MOVE 1 TO COUNTER
           END-IF.
           IF COLONNE-BASE > 105
              PERFORM TRANSMET
              MOVE 0 TO COLONNE-BASE
           END-IF.
           IF COLONNE-BASE = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              ADD 2 TO COLONNE-BASE 
           END-IF.
           ADD 18 TO COLONNE-BASE .
           IF DEBUT-J NOT = 0
              PERFORM FILL-STATIC.
           PERFORM FILL-HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 7.
           PERFORM FILL-NORMAL VARYING IDX FROM 1 BY 1 UNTIL IDX > 37.

       ENTETE.
           COMPUTE LIN-NUM = 2.
           MOVE  3 TO LIN-NUM.
           MOVE MOIS-DEBUT TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           MOVE 17 TO COL-NUM.
           PERFORM FILL-FORM
           IF MOIS-DEBUT NOT = MOIS-FIN
              ADD  2 TO COL-NUM 
              MOVE "-" TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE MOIS-FIN TO LNK-NUM
              PERFORM MOIS-NOM-1
              ADD 2 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.
           MOVE LNK-ANNEE TO VH-00
           MOVE  3 TO LIN-NUM.
           MOVE  4 TO CAR-NUM
           ADD   2 TO COL-NUM.
           PERFORM FILL-FORM.
      * DONNEES FIRME

           MOVE  6 TO CAR-NUM.
           MOVE 120 TO COL-NUM.
           ADD 1 TO PAGE-NUMBER.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM =  3 .
           MOVE  4 TO CAR-NUM.
           MOVE 75 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 80 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 5.

           IF COUT > 0 
              MOVE  8 TO CAR-NUM
              MOVE 71 TO COL-NUM
              MOVE COUT  TO VH-00
              PERFORM FILL-FORM
              MOVE 80 TO COL-NUM
              MOVE COUT-NOM TO ALPHA-TEXTE
              PERFORM FILL-FORM.

      *    DATE EDITION
           MOVE 20 TO COL-NUM.
           MOVE TODAY-JOUR TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 23 TO COL-NUM.
           MOVE TODAY-MOIS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 26 TO COL-NUM.
           MOVE TODAY-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           IF DOKUMENT > 1
              PERFORM TEXT-MAL.

       TEXT-MAL.
           COMPUTE LIN-NUM = 5.
           MOVE "+" TO ALPHA-TEXTE.
           MOVE 36 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 21 TO MS-NUMBER.
           MOVE "SL" TO LNK-AREA.
           CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY.
           MOVE MS-DESCRIPTION TO ALPHA-TEXTE.
           MOVE 38 TO COL-NUM.
           IF DOKUMENT = 3
              MOVE 36 TO COL-NUM.
           PERFORM FILL-FORM.

       FILL-STATIC.
      * DONNEES PERSONNE
           MOVE COLONNE-BASE TO COL-NUM.
           MOVE 6 TO CAR-NUM.
           MOVE 7 TO LIN-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           COMPUTE COL-NUM = COLONNE-BASE + 8.
           MOVE PR-PRENOM TO  ALPHA-TEXTE.
           MOVE 10 TO POINT-IDX.
           STRING STOPS DELIMITED BY SIZE
           INTO ALPHA-TEXTE POINTER POINT-IDX
           ON OVERFLOW CONTINUE END-STRING.
           PERFORM FILL-FORM.
           MOVE COLONNE-BASE TO COL-NUM.
           ADD 1 TO LIN-NUM.
           MOVE PR-NOM TO  ALPHA-TEXTE.
           MOVE 18 TO POINT-IDX.
           STRING STOPS DELIMITED BY SIZE
           INTO ALPHA-TEXTE POINTER POINT-IDX
           ON OVERFLOW CONTINUE END-STRING.
           PERFORM FILL-FORM.
           IF REG-PERSON NOT = 0
           AND CHOIX NOT = 0
              PERFORM DETAIL-CHOIX.

           COMPUTE LIN-NUM = 10.
           COMPUTE MS-NUMBER = 200 + DEBUT-M.
           MOVE "MO" TO LNK-AREA.
           CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY.
           COMPUTE COL-NUM = COLONNE-BASE.
           MOVE DEBUT-J TO VH-00.
           MOVE 2 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE MS-DESCRIPTION TO ALPHA-TEXTE.
           ADD 2 TO COL-NUM.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 11.
           COMPUTE MS-NUMBER = 200 + FIN-M.
           MOVE "MO" TO LNK-AREA.
           CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY.
           COMPUTE COL-NUM = COLONNE-BASE.
           MOVE FIN-J TO VH-00.
           MOVE 2 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE MS-DESCRIPTION TO ALPHA-TEXTE.
           ADD 2 TO COL-NUM.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 14.
           COMPUTE COL-NUM = COLONNE-BASE.
           MOVE HELP-JRS-IMP TO VH-00.
           MOVE 6 TO CAR-NUM.
           PERFORM FILL-FORM.

       FILL-NORMAL.
           MOVE HELP-VAL(IDX) TO VH-00.
           ADD HELP-VAL(IDX) TO TOTAL-VAL(IDX).
           MOVE LINE-VAL(IDX) TO LIN-NUM.
           MOVE COLONNE-BASE TO COL-NUM.
           MOVE  8 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           PERFORM FILL-FORM.

       FILL-HEURES.
           MOVE HELP-HRS(IDX) TO VH-00.
           ADD HELP-HRS(IDX) TO TOTAL-HRS(IDX).
           MOVE LINE-HRS(IDX) TO LIN-NUM.
           MOVE COLONNE-BASE TO COL-NUM.
           MOVE  8 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           PERFORM FILL-FORM.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 1504 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
           
       END-PROGRAM.
           IF COUNTER > 0 
              INITIALIZE DEBUT-FIN
              MOVE TOTAL-CUMUL TO HELP-CUMUL
              PERFORM FILL-FILES
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".
           COPY "XSORT.CPY".

