      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-LIB MODULE GENERAL LECTURE LIBELLE        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-LIB.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "LIBELLE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "LIBELLE.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  LANGUE   PIC X VALUE "F".

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "LIBELLE.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING  LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON LIBELLE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-LIB.
       
           IF NOT-OPEN = 0
              OPEN I-O LIBELLE
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO LIB-RECORD.
           IF EXC-KEY = 99
              WRITE LIB-RECORD INVALID REWRITE LIB-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.
           MOVE LNK-LANGUAGE TO LANGUE.
           IF  MENU-PROG-NAME NOT = "1-LIB"
           AND MENU-PROG-NAME NOT = "5-PLAN"
              IF FR-LANGUE NOT = SPACES
                 MOVE FR-LANGUE TO LANGUE
              END-IF
           END-IF.
           MOVE LANGUE TO LIB-LANGUAGE.

           MOVE 0 TO LNK-VAL.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ LIBELLE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ LIBELLE NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ LIBELLE NO LOCK INVALID PERFORM
                FRENCH END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF LANGUE NOT = LIB-LANGUAGE
              PERFORM FRENCH
           END-IF.
           MOVE LIB-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START LIBELLE KEY < LIB-KEY INVALID GO EXIT-1.
       START-2.
           START LIBELLE KEY > LIB-KEY INVALID GO EXIT-1.

       FRENCH.
           MOVE "F" TO LIB-LANGUAGE.
           READ LIBELLE NO LOCK INVALID INITIALIZE LIB-REC-DET END-READ.
           MOVE LIB-RECORD TO LINK-RECORD.
           EXIT PROGRAM.
