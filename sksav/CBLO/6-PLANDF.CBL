      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-PLANDF LECTURE PLAN COMPTABLES            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-PLANDF.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PLANDEF.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "PLANDEF.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN              PIC 9 VALUE 0.
       01  LAST-PLANDEF           PIC 9(8) VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PLANDEF.LNK".
                 
       01  A-N             PIC X.
       01  EXC-KEY         PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD A-N EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PLANDEF.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-PLANDF.
       
           IF NOT-OPEN = 0
              OPEN INPUT PLANDEF
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO PLANDEF-RECORD.
           MOVE 0 TO LNK-VAL.
           
           EVALUATE EXC-KEY 
           WHEN 65
               IF A-N = "N"
                  PERFORM START-1
               ELSE
                  PERFORM START-2
               END-IF
               READ PLANDEF PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66
               IF A-N = "N"
                  PERFORM START-3
               ELSE
                  PERFORM START-4
               END-IF
               READ PLANDEF NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER
               IF EXC-KEY = 4 AND PLANDEF-KEY = 0
                  MOVE LAST-PLANDEF TO PLANDEF-KEY 
               END-IF
               READ PLANDEF NO LOCK INVALID INITIALIZE PLANDEF-REC-DET 
               END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE PLANDEF-RECORD TO LINK-RECORD.
           MOVE PLANDEF-KEY TO LAST-PLANDEF.
           EXIT PROGRAM.

       START-1.
           START PLANDEF KEY < PLANDEF-KEY INVALID GO EXIT-1.
       START-2.
           START PLANDEF KEY < PLANDEF-KEY-ALPHA INVALID GO EXIT-1.
       START-3.
           START PLANDEF KEY > PLANDEF-KEY INVALID GO EXIT-1.
       START-4.
           START PLANDEF KEY > PLANDEF-KEY-ALPHA INVALID GO EXIT-1.



