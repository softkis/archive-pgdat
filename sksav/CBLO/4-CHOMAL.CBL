      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-CHOMAL RECHERCHE CHOMAGE EN MALADIE       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-CHOMAL.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN     PIC 9 VALUE 0.
       01  NOT-FOUND    PIC 9.
       01  ACTION       PIC X.

       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.


       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".


       PROCEDURE DIVISION USING LINK-V REG-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-A-CHOMAL.

           IF NOT-OPEN = 0
              MOVE LNK-SUFFIX TO ANNEE-JOURS
              OPEN I-O JOURS
              MOVE 1 TO NOT-OPEN
           END-IF.

           INITIALIZE JRS-RECORD LNK-VAL LNK-VAL-2.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE LNK-MOIS   TO JRS-MOIS. 
           MOVE REG-PERSON TO JRS-PERSON. 
           MOVE 1          TO JRS-OCCUPATION.
           START JOURS KEY >= JRS-KEY INVALID EXIT PROGRAM.

           PERFORM NEXT-JOURS THRU NEXT-JOURS-END.
           EXIT PROGRAM.

       NEXT-JOURS.
           READ JOURS NEXT NO LOCK AT END GO NEXT-JOURS-END.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR JRS-OCCUPATION > 9
              GO NEXT-JOURS-END.
           IF JRS-COMPLEMENT = 8
              ADD JRS-HRS(32) TO LNK-VAL.
           IF JRS-COMPLEMENT = 9
              ADD JRS-HRS(32) TO LNK-VAL-2.
           GO NEXT-JOURS.
       NEXT-JOURS-END.
           IF LNK-VAL > 8
              MOVE 8 TO LNK-VAL.
           IF LNK-VAL-2 > 8
              MOVE 8 TO LNK-VAL-2.
           IF LNK-VAL = 0
              MOVE 0 TO LNK-VAL-2.



