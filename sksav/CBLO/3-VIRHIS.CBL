      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-VIRHIS IMPRESSION HISTORIQUE  VIREMENTS   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-VIRHIS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "VIREMENT.FC".
           COPY "FORM130.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "VIREMENT.FDE".
           COPY "FORM130.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD  PIC X(10) VALUE "130       ".
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "IMPRLOG.REC".
           COPY "V-VAR.CPY".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.
           
           COPY "V-VH00.CPY".
       01  CUMUL PIC 9(8)V99.

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".VIH".
       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIR          PIC 999.

       01  HE-Z5 PIC Z(5).
       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-ZX REDEFINES HE-Z4.
               03 HE-M PIC 9.
               03 HE-A PIC 999.


  
       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "BANQP.REC".

       PROCEDURE DIVISION USING LINK-V PR-RECORD REG-RECORD BQP-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                VIREMENT
                FORM.  
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-VIRHIS.
       

           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           CALL "0-TODAY" USING TODAY.


       TRAITEMENT.
           MOVE 0 TO CUMUL LIN-IDX LIN-NUM.
           MOVE BQP-DEBUT-A TO IDX-2.
           IF IDX-2 = 0
              MOVE LNK-ANNEE TO IDX-2.
           PERFORM START-VIR VARYING IDX-1 FROM IDX-2 BY 1
           UNTIL IDX-1 > LNK-ANNEE.
           IF LIN-IDX < LIN-NUM
              PERFORM TRANSMET.
           PERFORM END-PROGRAM.

       START-VIR.
           MOVE IDX-1 TO HE-Z4.
           MOVE HE-A TO ANNEE-VIR.
           OPEN INPUT VIREMENT.
           PERFORM TOTAL-VIREMENT.
           CLOSE VIREMENT.

       ENTETE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  4 TO CAR-NUM.
           MOVE  2 TO LIN-NUM.
           MOVE 119 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  3 TO LIN-NUM.
           MOVE 113 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 116 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 119 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE  3 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FR-MAISON TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE FR-PAYS TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.

           COMPUTE LIN-NUM = 2.
           MOVE BQP-BANQUE TO ALPHA-TEXTE.
           MOVE 45 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE BQP-COMPTE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           MOVE BQP-BENEFIC-NOM TO ALPHA-TEXTE.
           MOVE 45 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           MOVE BQP-BENEFIC-RUE TO ALPHA-TEXTE.
           MOVE 45 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           MOVE BQP-BENEFIC-LOC TO ALPHA-TEXTE.
           MOVE 45 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           MOVE BQP-LIBELLE TO ALPHA-TEXTE.
           MOVE 45 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 8 TO LIN-NUM.
           MOVE BQP-TOTAL TO VH-00.
           MOVE 6 TO LIN-NUM.
           MOVE 7 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE 117 TO COL-NUM.
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.

      * DONNEES PERSONNE
           MOVE  2 TO LIN-NUM.
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE 85 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           MOVE 85 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           ADD 1 TO LIN-NUM.
           MOVE 85 TO COL-NUM.
           IF PR-MAISON NOT = SPACES
              MOVE PR-MAISON TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM.
           MOVE PR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           MOVE 85 TO COL-NUM.
           MOVE PR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  5 TO CAR-NUM.
           ADD 1 TO COL-NUM.
           MOVE PR-CODE-POST TO VH-00.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

       TOTAL-VIREMENT.
           INITIALIZE VIR-RECORD.
           MOVE REG-FIRME  TO VIR-FIRME.
           MOVE REG-PERSON TO VIR-PERSON.
           START VIREMENT KEY > VIR-KEY INVALID CONTINUE
           NOT INVALID PERFORM READ-VIREM THRU READ-VIREM-END.

       READ-VIREM.
           READ VIREMENT NEXT AT END
                GO READ-VIREM-END.
           IF REG-FIRME  NOT = VIR-FIRME
           OR REG-PERSON NOT = VIR-PERSON
              GO READ-VIREM-END.
           IF BQP-TYPE   NOT = VIR-TYPE
              GO READ-VIREM.
           IF  BQP-TYPE > 0
           AND VIR-SUITE  NOT = BQP-SUITE
              GO READ-VIREM.
           PERFORM IMPRIME.
           GO READ-VIREM.
       READ-VIREM-END.
           EXIT.

       IMPRIME.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 1 TO COUNTER
              MOVE 0 TO LIN-NUM
           END-IF.
           COMPUTE IDX = LIN-NUM + 1.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           PERFORM FILL-FILE.
    
       FILL-FILE.
           ADD 1 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  5 TO COL-NUM.
           MOVE IDX-1 TO VH-00.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE VIR-MOIS TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           IF VIR-MOIS = 0
              MOVE 20 TO COL-NUM
              IF LNK-LANGUAGE = "D"
                 MOVE "쉇ertrag" TO ALPHA-TEXTE
              ELSE
                 MOVE "Report" TO ALPHA-TEXTE
              END-IF
           END-IF.
           PERFORM FILL-FORM.

           IF  VIR-ANNEE-E = 0
           AND VIR-ANNEE-V > 0
              MOVE VIR-DATE-VIREMENT TO VIR-DATE-EDITION
           END-IF.
           MOVE  2 TO CAR-NUM.
           MOVE 85 TO COL-NUM.
           MOVE VIR-JOUR-E TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  88 TO COL-NUM.
           MOVE VIR-MOIS-E TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE  91 TO COL-NUM.
           MOVE VIR-ANNEE-E TO VH-00.
           PERFORM FILL-FORM.
           MOVE VIR-A-PAYER TO VH-00.
           MOVE 7 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE 100 TO COL-NUM.
           IF VIR-MOIS = 0
              MOVE 117 TO COL-NUM.
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.
           IF VIR-MOIS > 0
              ADD VIR-A-PAYER TO CUMUL
           ELSE
              MOVE VIR-A-PAYER TO CUMUL.
           IF VIR-MOIS > 0
              MOVE CUMUL TO VH-00
              MOVE 7 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              MOVE 117 TO COL-NUM
              MOVE 1 TO POINTS
              PERFORM FILL-FORM.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XMESSAGE.CPY".

