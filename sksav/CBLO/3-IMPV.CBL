      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-IMPV IMPRESSION VIREMENT IMPOTS PAR LOOP  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  3-IMPV.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "IMPOT.FC".
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "IMPOT.FDE".
           COPY "FORM80.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  ESPACES               PIC 99 VALUE 35.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "FIRME.REC".
           COPY "IMPRLOG.REC".
           COPY "V-VAR.CPY".
           COPY "FISC.REC".
           COPY "BANQUE.REC".
           COPY "BANQF.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME             PIC X(8) VALUE "FORM.VII".
       01  IMPOT-NAME.
           02 FILLER             PIC X(8) VALUE "S-IMPOT.".
           02 ANNEE-IMPOT        PIC 999 VALUE 0.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                IMPOT
                FORM.
                
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-IMPV.
           IF ANNEE-IMPOT = 0
              MOVE LNK-SUFFIX TO ANNEE-IMPOT
              OPEN I-O   IMPOT
              CALL "0-TODAY" USING TODAY.
           IF LNK-VAL = 99
              PERFORM END-PROGRAM.

           MOVE FR-FISC-BUREAU TO FISC-KEY.
           CALL "6-FISC" USING LINK-V FISC-RECORD FAKE-KEY.
           INITIALIZE BQF-RECORD.
           CALL "6-BANQF" USING LINK-V BQF-RECORD NX-KEY.
           MOVE BQF-CODE TO BQ-CODE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD FAKE-KEY.
           IF BQ-CODE = "C" OR "CQ"
              INITIALIZE BQF-RECORD BQ-RECORD.


           INITIALIZE IMPOT-RECORD.
           MOVE FR-KEY  TO IMPOT-FIRME.
           START IMPOT KEY > IMPOT-KEY 
           INVALID PERFORM READ-IMPOT-END
           NOT INVALID PERFORM READ-IMPOT THRU READ-IMPOT-END.

       READ-IMPOT.
           READ IMPOT NEXT AT END 
                GO READ-IMPOT-END.
           IF FR-KEY NOT = IMPOT-FIRME
              GO READ-IMPOT-END.
           IF IMPOT-VIR-A NOT = 0
              GO READ-IMPOT.
           PERFORM FULL-PROCESS.
           MOVE TODAY-ANNEE TO IMPOT-VIR-A.
           MOVE TODAY-MOIS  TO IMPOT-VIR-M.
           MOVE TODAY-JOUR  TO IMPOT-VIR-J.
           IF LNK-SQL = "Y" 
              CALL "9-IMPOT" USING LINK-V IMPOT-RECORD WR-KEY
           END-IF.
           REWRITE IMPOT-RECORD INVALID CONTINUE.
           GO READ-IMPOT.
       READ-IMPOT-END.
           EXIT PROGRAM.
                
       FULL-PROCESS.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 1 TO COUNTER
           END-IF.
           PERFORM READ-FORM.
           MOVE 0 TO ESPACES.
           PERFORM FILL-FILES.
           MOVE 36 TO ESPACES.
           PERFORM FILL-FILES.
           COMPUTE SH-00 = IMPOT-A-PAYER - IMPOT-DECOMPTE.
           IF SH-00 > 0
              PERFORM TRANSMET.

       FILL-FILES.
           IF FR-FISC-A > 0
              MOVE FR-FISC TO FR-DATE-ETAB.
           COMPUTE LIN-NUM = ESPACES + 17.
           MOVE 32 TO COL-NUM.
           MOVE FR-ETAB-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 37 TO COL-NUM.
           MOVE FR-ETAB-N TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 42 TO COL-NUM.
           MOVE FR-SNOCS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 22.
           MOVE 3 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 4.
           MOVE  4 TO CAR-NUM.
           MOVE 70 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 23.
           MOVE 3  TO COL-NUM.
           MOVE FR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 2 TO COL-NUM.
           MOVE FR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 24.
           MOVE  3 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  6 TO CAR-NUM.
           MOVE FR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           ADD  1  TO COL-NUM.
           MOVE FR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 9.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 63 TO COL-NUM.
           COMPUTE VH-00 = IMPOT-A-PAYER - IMPOT-DECOMPTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 21.
           MOVE  3 TO COL-NUM.
           MOVE BQF-CODE  TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE BQF-COMPTE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 18.
           EVALUATE FR-FISC-CODE
           WHEN 0 MOVE IMPOT-MOIS TO LNK-NUM
                  PERFORM MOIS-NOM-1
                  MOVE 42 TO COL-NUM
           WHEN 1 MOVE "TRIMESTRE" TO ALPHA-TEXTE
                  MOVE 34 TO COL-NUM
                  PERFORM FILL-FORM
                  COMPUTE VH-00 = IMPOT-MOIS / 3
                  MOVE 2 TO CAR-NUM
           WHEN 2 MOVE "'EXERCICE" TO ALPHA-TEXTE
                  MOVE 32 TO COL-NUM
           END-EVALUATE.
           PERFORM FILL-FORM.
           COMPUTE VH-00 = LNK-ANNEE.
           MOVE 6 TO CAR-NUM.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 13.
           MOVE 40 TO COL-NUM.
           MOVE FISC-KEY  TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 9.
           MOVE  3 TO COL-NUM.
           MOVE FISC-BANQUE   TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE FISC-COMPTE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = ESPACES + 20.
           MOVE 57 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 63 TO COL-NUM.
           MOVE TODAY-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
