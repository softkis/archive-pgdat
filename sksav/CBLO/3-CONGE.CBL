      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 3-CONGE            LIVRE DE CONG�  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CONGE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".
           COPY "TRIPR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  CHOIX-MAX             PIC 99 VALUE 8.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CALEN.REC".
           COPY "CCOL.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "LIVRE.REC".
           COPY "METIER.REC".
           COPY "JOURS.REC".
           COPY "CONTRAT.REC".
           COPY "CONGE.REC".
           COPY "MESSAGE.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(080) OCCURS 70.

           COPY "V-VH00.CPY".

       01  HELP-CUMUL.
           02  CONGE-COMPENSE       PIC S9(4)V99 OCCURS 13.
           02  HR-TOT-MOIS          PIC S9(4)V99 OCCURS 13.
           02  HR-PLUS-MOIS         PIC S9(4)V99 OCCURS 13.
           02  CONGE-MONTANT-ACQUIS PIC S9(6)V99.
           02  CONGE-MONTANT-PRIS   PIC S9(6)V99.
           02  CONGE-DUS            PIC S9(4)V99.

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".CGE".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  DETAIL-PAIE           PIC X VALUE "N".
       01  CHOIX                 PIC 99 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CONGE.
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE PRECISION TO LNK-PRESENCE.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR (14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6  PERFORM AVANT-YN
           WHEN  7 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN  8 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-YN
           WHEN  7 PERFORM APRES-SORT
           WHEN  8 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-YN.
           ACCEPT DETAIL-PAIE
             LINE 16 POSITION 32 SIZE 1 
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-YN.
           IF DETAIL-PAIE = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE 
              MOVE "N" TO DETAIL-PAIE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-3
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
           OR CAR-CONGE = 9
              GO READ-PERSON-3
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-3
           END-IF.
           PERFORM JRS.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.


       METIER.
           MOVE CAR-METIER TO MET-CODE.
           MOVE "F "       TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           MOVE 20 TO LIN-NUM.
           MOVE 10 TO COL-NUM.
           PERFORM FILL-FORM.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 1350 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XSORT.CPY".

       JRS.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
           END-IF.
           PERFORM READ-FORM.
           INITIALIZE HELP-CUMUL.
           MOVE CAR-METIER TO MET-CODE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD FAKE-KEY.
           PERFORM CUMUL-HEURES.
           PERFORM CUMUL-LIVRE.
           IF HR-TOT-MOIS(13) > 0
           OR CONGE-COMPENSE(13) > 0
           OR CONGE-DUS > 0
               PERFORM FILL-FILES
               MOVE 0 TO LNK-VAL
               ADD 1 TO COUNTER
               MOVE 70 TO LNK-LINE
               CALL "P080" USING LINK-V FORMULAIRE.

       FILL-FILES.
           PERFORM READ-CONTRAT.
           COMPUTE LIN-NUM =  2.
           MOVE 45 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

      * DATE EDITION
           MOVE 68 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 71 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 74 TO COL-NUM.
           MOVE TODAY-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

      * DONNEES FIRME
           COMPUTE LIN-NUM =  4.
           MOVE  3 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.


      * DONNEES PERSONNE
           COMPUTE LIN-NUM = 7.
           MOVE  3 TO COL-NUM.
           MOVE PR-NAISS-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  8 TO COL-NUM.
           MOVE PR-NAISS-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 11 TO COL-NUM.
           MOVE PR-NAISS-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 14 TO COL-NUM.
           MOVE PR-SNOCS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 5.
           MOVE  6 TO CAR-NUM.
           MOVE 20 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           
           PERFORM FILL-FORM.
           MOVE 27 TO COL-NUM.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 6.
           MOVE 27 TO COL-NUM.
           MOVE PR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 31 TO COL-NUM.
           MOVE PR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 7.
           MOVE 27 TO COL-NUM.
           MOVE PR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  5 TO CAR-NUM.
           MOVE 29 TO COL-NUM.
           MOVE PR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 35 TO COL-NUM.
           MOVE PR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 8.
           MOVE  3 TO COL-NUM.
           MOVE MET-NOM(PR-CODE-SEXE) TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 9.
           MOVE 13 TO COL-NUM.
           MOVE CON-DEBUT-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 16 TO COL-NUM.
           MOVE CON-DEBUT-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 19 TO COL-NUM.
           MOVE CON-DEBUT-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 38 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE CON-FIN-J TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 41 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE CON-FIN-M TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 45 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE CON-FIN-A TO  VH-00.
           PERFORM FILL-FORM.

           PERFORM FILL-TOTAL VARYING IDX FROM 1 BY 1 UNTIL IDX > 12.
           PERFORM FILL-BOTTOM.

       FILL-TOTAL.
           COMPUTE LIN-NUM = 45.
           COMPUTE COL-NUM = 2 + IDX * 6.
           MOVE HR-TOT-MOIS(IDX) TO VH-00.
           MOVE   3 TO CAR-NUM.
           MOVE   1 TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 47.
           COMPUTE COL-NUM = 2 + IDX * 6.
           MOVE HR-PLUS-MOIS(IDX) TO VH-00.
           MOVE   3 TO CAR-NUM.
           MOVE   1 TO DEC-NUM.
           PERFORM FILL-FORM.
  
           COMPUTE LIN-NUM = 49.
           COMPUTE COL-NUM = 2 + IDX * 6.
           MOVE CONGE-COMPENSE(IDX) TO VH-00.
           MOVE   3 TO CAR-NUM.
           MOVE   1 TO DEC-NUM.
           PERFORM FILL-FORM.

       FILL-BOTTOM.
           COMPUTE LIN-NUM = 54.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE  40 TO COL-NUM.
           MOVE CONGE-SOLDE  TO VH-00.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 55.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE  40 TO COL-NUM.
           COMPUTE SH-00 = CONGE-AN + CONGE-DUS + CONGE-AJUSTE.
           MOVE SH-00 TO VH-00.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 56.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE  40 TO COL-NUM.
           MOVE HR-TOT-MOIS(13)   TO VH-00.
           ADD CONGE-COMPENSE(13) TO VH-00.
           PERFORM FILL-FORM.
           SUBTRACT HR-TOT-MOIS(13)    FROM SH-00.
           SUBTRACT CONGE-COMPENSE(13) FROM SH-00.
           ADD CONGE-SOLDE TO SH-00.
           COMPUTE LIN-NUM = 57.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE  40 TO COL-NUM.
           MOVE SH-00 TO VH-00.
           PERFORM FILL-FORM.

           IF  DETAIL-PAIE  NOT = "N" 
               PERFORM FILL-PAIE.

       FILL-PAIE.
           COMPUTE LIN-NUM = 59.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE  40 TO COL-NUM.
           MOVE CONGE-MONTANT-SOLDE TO VH-00.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 60.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE  40 TO COL-NUM.
           MOVE CONGE-MONTANT-ACQUIS TO VH-00.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 61.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE  40 TO COL-NUM.
           MOVE CONGE-MONTANT-PRIS TO VH-00.
           PERFORM FILL-FORM.
           IF CAR-CONGE = 8
              MOVE  62 TO LIN-NUM
              COMPUTE CONGE-MONTANT-SOLDE = CONGE-MONTANT-SOLDE +
                      CONGE-MONTANT-ACQUIS - CONGE-MONTANT-PRIS
              END-COMPUTE
              MOVE   5 TO CAR-NUM
              MOVE   2 TO DEC-NUM
              MOVE  40 TO COL-NUM
              MOVE CONGE-MONTANT-SOLDE TO VH-00
              PERFORM FILL-FORM
           END-IF.

       CUMUL-LIVRE.
           PERFORM LIVRE VARYING IDX FROM 1 BY 1 UNTIL IDX > 12.

       LIVRE.
           INITIALIZE L-RECORD.
           MOVE IDX TO L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-FIRME NOT = 0
              PERFORM CUMUL-ANNEE.

       CUMUL-ANNEE.
           ADD L-UNI-CONGE-PLUS TO CONGE-DUS HR-PLUS-MOIS(L-MOIS) 
                                             HR-PLUS-MOIS(13).
           ADD L-PROV-CONGE-PLUS TO CONGE-MONTANT-ACQUIS.
           ADD L-UNI-CONGE-MIN TO CONGE-COMPENSE(IDX) CONGE-COMPENSE(13).
           IF L-MNT-CONGE  NOT = 0
           OR L-CONGE-COMP NOT = 0
              IF DETAIL-PAIE NOT = "N" 
                 COMPUTE LIN-NUM = 51
                 COMPUTE COL-NUM = 1 + L-MOIS * 6
                 MOVE L-MNT-CONGE TO VH-00
                 MOVE 6 TO CAR-NUM
                 PERFORM FILL-FORM
                 ADD L-MNT-CONGE  TO CONGE-MONTANT-PRIS
                 ADD L-CONGE-COMP TO CONGE-MONTANT-PRIS
              END-IF
           END-IF.

           IF L-UNI-CONGE > HR-TOT-MOIS(IDX) 
              SUBTRACT HR-TOT-MOIS(IDX) FROM HR-TOT-MOIS(13) 
              MOVE L-UNI-CONGE TO HR-TOT-MOIS(IDX) 
              ADD  L-UNI-CONGE TO HR-TOT-MOIS(13) 
           END-IF.

       CUMUL-HEURES.
           PERFORM HRS VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 12.

       HRS.
           INITIALIZE JRS-RECORD.
           MOVE 20 TO JRS-OCCUPATION.
           MOVE IDX-1 TO JRS-MOIS.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD FAKE-KEY.
           IF JRS-HRS(32) > 0
              PERFORM FILL-HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 31
           END-IF.

       FILL-HEURES.
           IF JRS-HRS(IDX) NOT = 0
               COMPUTE LIN-NUM = 12 + IDX
               COMPUTE COL-NUM = 2 + JRS-MOIS * 6
               MOVE JRS-HRS(IDX) TO VH-00
               MOVE 1 TO DEC-NUM
               MOVE 3 TO CAR-NUM
               PERFORM FILL-FORM
               ADD JRS-HRS(IDX) TO HR-TOT-MOIS(JRS-MOIS) 
                                   HR-TOT-MOIS(13)    
           END-IF.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE 12        TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.
