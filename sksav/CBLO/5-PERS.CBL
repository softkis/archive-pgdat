      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 5-PERS CUMUL PERSONNES FICHIER COMPLET      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    5-PERS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL .
           COPY "PERSON.FC".
      *    Fichier des personnes      Salaire-minute
           SELECT OPTIONAL XERSONNE ASSIGN TO RANDOM, "X-PERSON"
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is XR-MATRICULE,
                  ALTERNATE RECORD KEY is XR-KEY,
                  STATUS FS-HELP.

       DATA DIVISION.

       FILE SECTION.

           COPY "PERSON.FDE".
       FD  XERSONNE
           LABEL RECORD STANDARD
           DATA RECORD XR-RECORD.

       01  XR-RECORD.
           02 XR-KEY.
              03 XR-MATCHCODE      PIC X(10).
              03 XR-MATRICULE      PIC 9(11).

           02 XR-REC-DET.
              03 XR-PAYS           PIC XXX.
              03 XR-CODE-POST      PIC 9(8).
              03 XR-NOM            PIC X(30).
              03 XR-PRENOM         PIC X(20).
              03 XR-NOM-JF         PIC X(25).
              03 XR-PRENOM-MARI    PIC X(20).
              03 XR-RUE            PIC X(40).
              03 XR-MAISON         PIC X(10).
              03 XR-LOCALITE       PIC X(40).
              03 XR-POBCO          PIC X(40).
              03 XR-CODE-SEXE      PIC 9.
              03 XR-NATIONALITE    PIC XXX.
              03 XR-LANGUAGE       PIC X.
              03 XR-LIEU-NAISSANCE PIC X(40).
              03 XR-TELEPHONE      PIC X(20).
              03 XR-TELEPHONE-INT  PIC X(20).
              03 XR-TELEPHONE-MOB  PIC X(20).
              03 XR-E-MAIL         PIC X(30).
              03 XR-MALADIE-AGENCE PIC X(15).
              03 XR-POLITESSE      PIC 99.
              03 XR-DOC-ID         PIC X(20).
              03 XR-PERMIS         PIC X(20).
              03 XR-CONTACT-NOM    PIC X(40).
              03 XR-CONTACT-ADR    PIC X(40).
              03 XR-CONTACT-TEL    PIC X(30).
              03 XR-CONTACT-TELB   PIC X(30).
              03 XR-ACTIF          PIC X.
              03 XR-ID-DEB         PIC 9(8).
              03 XR-ID-FIN         PIC 9(8).
              03 XR-FILLER         PIC X(300).
              03 XR-STAMP          PIC X(22). 

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".

       01  NOT-OPEN         PIC 9 VALUE 0.
        

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PERSONNE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-X-PERS.

           IF NOT-OPEN = 0 
              OPEN INPUT PERSONNE
              OPEN I-O   XERSONNE
              MOVE 1 TO NOT-OPEN
           END-IF.
           PERFORM TRAITEMENT.
           EXIT PROGRAM.

       TRAITEMENT.
           INITIALIZE PR-RECORD.
           START PERSONNE KEY >= PR-KEY INVALID EXIT PROGRAM
              NOT INVALID
              PERFORM C-C THRU C-C-END.
       C-C.
           READ PERSONNE NEXT NO LOCK AT END 
             GO C-C-END.
           IF PR-SNOCS < 10
           OR PR-CODE-SEXE = 0
           OR PR-NOM = SPACES
              GO C-C.
           IF PR-MATCHCODE = SPACES 
           OR PR-MATCHCODE < "A" 
              MOVE PR-NOM TO PR-MATCHCODE
              INSPECT PR-MATCHCODE CONVERTING
           "abcdefghijklmnopqrstuvwxyz뇘뀈굤닀땶뱮걭뿖쉸�.,;-_'+&*!?" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZCAAAEEEEIIOOUUUOUAE           "
           END-IF.

           DISPLAY PR-KEY LINE 24 POSITION 15.
           MOVE PR-RECORD TO XR-RECORD.
           WRITE XR-RECORD INVALID CONTINUE.
           GO C-C.
       C-C-END.
           EXIT.



      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XACTION.CPY".

