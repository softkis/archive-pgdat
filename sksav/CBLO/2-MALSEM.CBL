      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-MALSEM  MALADIES EN SEMAINES              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-MALSEM .

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM80.FDE".

       WORKING-STORAGE SECTION.

       01  CHOIX-MAX         PIC 99 VALUE 1. 
       01  JOB-STANDARD      PIC X(10) VALUE "80        ".
       01  TIRET-TEXTE           PIC X VALUE "�".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "IMPRLOG.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "REGISTRE.REC".
           COPY "HORSEM.REC".
           COPY "LIVRE.REC".
           COPY "STATUT.REC".

           COPY "V-VAR.CPY".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

       01  HELP-CUMUL.           
           02 O-E OCCURS 2.
              03 OUVRIER-T OCCURS 54.
                 04 OUVRIER-HRS PIC 9(6)V99.
                 04 OUVRIER-OCC PIC 9999.
       01  HELP-CUMUL-R REDEFINES HELP-CUMUL.
           02 O-E OCCURS 2.
              03 TO-E OCCURS 3.
                 04 OUVRIER-TAB OCCURS 18.
                    05 OUVRIER-H PIC 9(6)V99.
                    05 OUVRIER-O PIC 9999.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(3) VALUE ".SI".
           
       01  ECR-DISPLAY.
           02 HE-Z2    PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z3    PIC Z(3) BLANK WHEN ZERO.
           02 HE-Z4    PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z5    PIC Z(5) BLANK WHEN ZERO.
           02 HE-Z6    PIC Z(6) BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-MALSEM.
       
           PERFORM AFFICHAGE-ECRAN .
           MOVE LNK-MOIS  TO SAVE-MOIS.

           PERFORM CUMUL-PERSON.
           PERFORM AFFICHAGE-DETAIL.
           MOVE 1 TO INDICE-ZONE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
            UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.


      * param둻res sp괹ifiques touches de fonctions
           MOVE 0000000007 TO EXC-KFR(1).
           MOVE 1700000007 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82
              PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY = 98 GO TO TRAITEMENT-ECRAN-01.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           
           
           PERFORM APRES-DEC.

           IF INDICE-ZONE > CHOIX-MAX
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           IF EXC-KEY =  5 
              PERFORM EDITION
           END-IF.

       EDITION.
      *    PERFORM READ-IMPRIMANTE.
      *    MOVE LNK-LANGUAGE TO FORM-LANGUE.
      *    PERFORM READ-FORM.
           INITIALIZE FORMULAIRE.
           PERFORM FILL-FILES.
           PERFORM TRANSMET.
           PERFORM END-PROGRAM.

       CUMUL-PERSON.
           MOVE "AA" TO LNK-AREA.
           MOVE 22 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           MOVE 0 TO LNK-NUM.
           INITIALIZE REG-RECORD HELP-CUMUL.
           PERFORM LECTURE-PERSON THRU LECTURE-PERSON-END.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           PERFORM AFFICHAGE-DETAIL.

       LECTURE-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF REG-PERSON = 0
              GO LECTURE-PERSON-END.
           IF PRES-ANNEE = 0
              GO LECTURE-PERSON.
           PERFORM CUMULS.
           GO LECTURE-PERSON.
       LECTURE-PERSON-END.
           EXIT.

       CUMULS.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-GHJS"  USING LINK-V HJS-RECORD CAR-RECORD PRESENCES.
           INITIALIZE L-RECORD SH-00 LNK-SUITE.
           MOVE 67 TO SAVE-KEY.
           PERFORM READ-LIVRE THRU READ-LIVRE-END.

       READ-LIVRE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD SAVE-KEY.
           MOVE 66 TO SAVE-KEY.
           IF L-PERSON = 0
              GO READ-LIVRE-END.
           IF L-SUITE NOT = 0
              GO READ-LIVRE.
           PERFORM ADD-UP VARYING IDX FROM 1 BY 1 UNTIL IDX > 9.
           ADD L-UNITE(11) TO SH-00.
           GO READ-LIVRE.
       READ-LIVRE-END.
           IF SH-00 > 0
              DIVIDE SH-00 BY HJS-HEURES(8) GIVING IDX-1 
              REMAINDER SH-01
              IF SH-01 > 0
                 ADD 1 TO IDX-1
              END-IF
              ADD 1     TO OUVRIER-OCC(CAR-STATUT, IDX-1)
                           OUVRIER-OCC(CAR-STATUT, 54)
              ADD SH-00 TO OUVRIER-HRS(CAR-STATUT, IDX-1)
                           OUVRIER-HRS(CAR-STATUT, 54)
           END-IF.

       ADD-UP.
           ADD L-UNITE(IDX) TO SH-00.

       DIS-E1-01.
           PERFORM DIS-L VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 3.
       DIS-L.
           PERFORM DIS-LIGNE VARYING IDX FROM 1 BY 1 UNTIL IDX > 18.

       DIS-HE-END.
           EXIT.
        
       DIS-LIGNE.
           COMPUTE IDX-4 = (IDX-1 - 1) * 18 + IDX.
           COMPUTE LIN-IDX = IDX + 3.
           COMPUTE COL-IDX = (IDX-1 - 1) * 12 + 1.
           MOVE IDX-4 TO HE-Z2.
           IF IDX-4 < 54
              DISPLAY HE-Z2  LINE LIN-IDX POSITION COL-IDX LOW.
OUV
           ADD 2 TO COL-IDX.
           MOVE OUVRIER-HRS(1, IDX-4) TO HE-Z5.
           DISPLAY HE-Z5  LINE LIN-IDX POSITION COL-IDX.
           ADD 6 TO COL-IDX.
           MOVE OUVRIER-OCC(1, IDX-4) TO HE-Z3.
           DISPLAY HE-Z3  LINE LIN-IDX POSITION COL-IDX.
EMP
           COMPUTE COL-IDX = (IDX-1 - 1) * 12 + 41.
           IF IDX-4 < 54
              DISPLAY HE-Z2  LINE LIN-IDX POSITION COL-IDX LOW.
           ADD 2 TO COL-IDX.
           MOVE OUVRIER-HRS(2, IDX-4) TO HE-Z5.
           DISPLAY HE-Z5  LINE LIN-IDX POSITION COL-IDX.
           ADD 6 TO COL-IDX.
           MOVE OUVRIER-OCC(2, IDX-4) TO HE-Z3.
           DISPLAY HE-Z3  LINE LIN-IDX POSITION COL-IDX.

       AFFICHAGE-ECRAN.
           MOVE 32 TO LNK-VAL.
           ADD MENU-PROG-NUMBER TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

           MOVE 1 TO STAT-CODE.
           CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY
           DISPLAY STAT-NOM LINE 3 POSITION 15 LOW.
           MOVE 2 TO STAT-CODE.
           CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY
           DISPLAY STAT-NOM LINE 3 POSITION 55 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-E1-01. 
           
       END-PROGRAM.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XACTION.CPY".
    
       FILL-FILES.
           MOVE 03 TO LIN-NUM.
           MOVE 10 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE FR-KEY TO  VH-00.
           PERFORM FILL-FORM.

           MOVE 20 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 05 TO LIN-NUM.
           MOVE 20 TO COL-NUM.
           MOVE MENU-DESCRIPTION TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE LNK-ANNEE TO ALPHA-TEXTE.
           MOVE 60 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 07 TO LIN-NUM.
           MOVE 5 TO COL-NUM.
           PERFORM TIRET UNTIL COL-NUM > 75.
           MOVE 1 TO STAT-CODE.
           CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY
           MOVE STAT-NOM TO ALPHA-TEXTE.
           MOVE 09 TO LIN-NUM.
           MOVE 10 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 2 TO STAT-CODE.
           CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY
           MOVE STAT-NOM TO ALPHA-TEXTE.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.



           PERFORM FILL-L VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 54.

       FILL-L.
           COMPUTE LIN-NUM = IDX-1 + 10.

           MOVE IDX-1 TO VH-00.
           MOVE 7 TO COL-NUM.
           MOVE 2 TO CAR-NUM.
           IF IDX-1 < 54
              PERFORM FILL-FORM.

           MOVE 47 TO COL-NUM.
           MOVE 2 TO CAR-NUM.
           IF IDX-1 < 54
              PERFORM FILL-FORM.

           MOVE OUVRIER-HRS(1, IDX-1) TO VH-00.
           MOVE 11 TO COL-NUM.
           MOVE 5 TO CAR-NUM.
           PERFORM FILL-FORM.

           MOVE OUVRIER-OCC(1, IDX-1) TO VH-00.
           MOVE 18 TO COL-NUM.
           MOVE 3 TO CAR-NUM.
           PERFORM FILL-FORM.

           MOVE OUVRIER-HRS(2, IDX-1) TO VH-00.
           MOVE 51 TO COL-NUM.
           MOVE 5 TO CAR-NUM.
           PERFORM FILL-FORM.

           MOVE OUVRIER-OCC(2, IDX-1) TO VH-00.
           MOVE 58 TO COL-NUM.
           MOVE 3 TO CAR-NUM.
           PERFORM FILL-FORM.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           MOVE 99 TO LNK-VAL
           CALL "P080" USING LINK-V FORMULAIRE
           CANCEL "P080".
