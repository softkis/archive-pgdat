      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-DMESS  AFFICHAGE DES MESSAGES             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  0-DMESS.

       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  FAKE-KEY              PIC 9(4) COMP-1 VALUE 13.
       01  ACTION                PIC X.
           COPY "MESSAGE.REC".
       01  COUNTER               PIC Z(4).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       SCREEN SECTION.

       01  ECRAN-DEFAULT.
           02 HE-AREA PIC XX USING LNK-AREA LINE 24 COL 2. 
           02 HE-Z PIC ZZZZZ USING LNK-NUM .        

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-DMESS.
           MOVE LNK-NUM TO MS-NUMBER. 
           MOVE LNK-LANGUAGE TO MS-LANGUAGE.
           MOVE LNK-AREA TO MS-AREA.
           MOVE LNK-VAL  TO COUNTER.
           IF LNK-POSITION = 0
              MOVE 24107000 TO LNK-POSITION
              DISPLAY ECRAN-DEFAULT
           END-IF.
           CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY.
           INSPECT MS-DESCRIPTION REPLACING ALL "####" BY COUNTER.
           EVALUATE LNK-LOW 
              WHEN "L" DISPLAY MS-DESCRIPTION LINE LNK-LINE 
              POSITION LNK-COL SIZE LNK-SIZE LOW
              WHEN "R" DISPLAY MS-DESCRIPTION LINE LNK-LINE 
              POSITION LNK-COL SIZE LNK-SIZE REVERSE
              WHEN OTHER DISPLAY MS-DESCRIPTION LINE LNK-LINE 
              POSITION LNK-COL SIZE LNK-SIZE
           END-EVALUATE.
           IF MS-ACTION = "A" 
              ACCEPT ACTION NO BEEP.
           IF MS-ACTION = "B" 
              ACCEPT ACTION.
           INITIALIZE LNK-NUM LNK-LOW LNK-POSITION MS-ACTION.
           EXIT PROGRAM.
      