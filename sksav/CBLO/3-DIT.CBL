      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-DIT DECLARATION INCAPACITE DE TRAVAIL     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-DIT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
      *--------------------
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "DECMAL.REC".
           COPY "JOURS.REC".
           COPY "LIVRE.REC".
           COPY "PARMOD.REC".
           COPY "SUDPI.LNK".

           COPY "V-VAR.CPY".
        
       01  RENEW                 PIC 9  VALUE 0.
       01  TYP                   PIC 9  VALUE 0.
       01  COUNTER               PIC 999  VALUE 0.
       01  DECMAL-HRS            PIC 999V99 VALUE 0.
       01  MOIS                  PIC 99  VALUE 0.
       01  MOIS-EN-COURS         PIC 99  VALUE 0.
       01  JOUR                  PIC 99  VALUE 0.
       01  JOUR-D                PIC 99  VALUE 0.
       01  JOUR-F                PIC 99  VALUE 0.
       01  JOURS                 PIC 99V99  VALUE 0.
       01  RECTIF                PIC X.
       01  SAVE-FIRME            PIC 9(6).
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".

       01  TOTAL-HEURES.
           02 SUITES PIC 99V99 OCCURS 5.
           02 SOUS-TOTAUX.
              03 TOT-H OCCURS 5.
                 04 IDX-HRS   PIC 9.
                 04 TOT-HRS   PIC S999V99 OCCURS 31.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HE-TEMP PIC ZZ.ZZ BLANK WHEN ZERO.
           02 HE-FIN.
              03 HE-X PIC X VALUE ":".
              03 HE-F PIC ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-DIT.
       
           MOVE LNK-DATE TO SAVE-DATE.
           MOVE FR-KEY TO SAVE-FIRME.
           MOVE 0 TO FR-KEY IDX-1.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0000000000 TO EXC-KFR(1)
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TRAITEMENT.
           IF A-N = "A"
              MOVE 9999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           IF FR-KEY = END-NUMBER
              MOVE " " TO FR-SNOCS-YN 
           END-IF.
           IF FR-KEY = 0
              MOVE 66 TO EXC-KEY
           ELSE
              MOVE 13 TO EXC-KEY.
           PERFORM READ-FIRME THRU READ-FIRME-END.
           PERFORM END-PROGRAM.

       READ-FIRME.
           PERFORM NEXT-FIRME.
           MOVE 66 TO EXC-KEY.
           IF FR-KEY = 0
           OR FR-KEY > END-NUMBER
           OR FR-MATCHCODE > END-MATCHCODE
              GO READ-FIRME-END
           END-IF.
           IF FR-COMPETENCE > LNK-COMPETENCE
              GO READ-FIRME
           END-IF.
           IF FR-FIN-A > 0 AND < LNK-ANNEE
              GO READ-FIRME
           END-IF.
           IF FR-SNOCS-YN = "N" 
           OR FR-EXTENS   = 0
              GO READ-FIRME
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM START-PERSON.
           GO READ-FIRME.
       READ-FIRME-END.
           EXIT.

       START-PERSON.
           MOVE " " TO RECTIF.
           PERFORM MOIS VARYING MOIS FROM 1 BY 1 UNTIL MOIS > SAVE-MOIS.
           MOVE "X" TO RECTIF.
           PERFORM MOIS VARYING MOIS FROM 1 BY 1 UNTIL MOIS > SAVE-MOIS.

       MOIS.
           INITIALIZE DM-RECORD.
           MOVE MOIS TO DM-MOIS DM-MOIS-2 MOIS-EN-COURS.
           PERFORM READ-PERSON THRU READ-EXIT.
           IF IDX-1 > 0 
              PERFORM TRANSMET
           END-IF.

       READ-PERSON.
           CALL "6-DECMAL" USING LINK-V REG-RECORD DM-RECORD "1" NX-KEY.
           IF DM-FIRME = 0
              GO READ-EXIT
           END-IF.
           IF SAVE-ANNEE < DM-ANNEE 
           OR DM-MOIS > MOIS 
              GO READ-EXIT
           END-IF.
           IF DM-MEMO(1) < DM-MEMO(2)  
              GO READ-PERSON
           END-IF.
           MOVE DM-PERSON TO REG-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           PERFORM DIS-HE-00.
           IF REG-SNOCS-YN = "N"
           OR REG-PERSON = 0
              GO READ-PERSON
           END-IF.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           IF PR-NOM   = SPACES
           OR PR-SNOCS = 0
              GO READ-PERSON
           END-IF.
           IF RECTIF = SPACES
              IF  DM-MEMO(2) > 0
              AND DM-MEMO(1) > DM-MEMO(2) 
                 GO READ-PERSON
              END-IF
           END-IF.
           MOVE DM-MOIS  TO LNK-MOIS.
           MOVE DM-ANNEE TO LNK-ANNEE.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS = 13
              ADD  1 TO LNK-ANNEE
              MOVE 1 TO LNK-MOIS.
           INITIALIZE TOTAL-HEURES.
           PERFORM HEURES.
           PERFORM SUITES VARYING TYP FROM 1 BY 1 UNTIL TYP > 5.
           INITIALIZE SOUS-TOTAUX.
           MOVE DM-MOIS  TO LNK-MOIS.
           MOVE DM-ANNEE TO LNK-ANNEE.
           PERFORM HEURES.
           CALL "6-DECMAL" USING LINK-V REG-RECORD DM-RECORD "2" WR-KEY.
           GO READ-PERSON.
       READ-EXIT.
           EXIT.

       NEXT-FIRME.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.

       HEURES.
           INITIALIZE JRS-RECORD.
           MOVE LNK-MOIS TO JRS-MOIS.
           PERFORM READ-HEURES THRU READ-HEURES-END.

       READ-HEURES.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD NX-KEY.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
           OR JRS-OCCUPATION > 11
              GO READ-HEURES-END
           END-IF.
           IF JRS-COMPLEMENT > 0
           OR JRS-OCCUPATION < 1
              GO READ-HEURES.
           INITIALIZE L-RECORD.
           IF JRS-OCCUPATION < 11
              MOVE JRS-OCCUPATION TO LNK-SUITE
              CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           MOVE 1 TO TYP.
famil      IF L-FLAG(4) NOT = 0
              MOVE 2 TO TYP.
dispe      IF L-FLAG(6) NOT = 0
mater      OR L-SUITE = 10
              MOVE 3 TO TYP.
accue      IF  L-FLAG(4) NOT = 0
mater      AND L-SUITE = 10
              MOVE 4 TO TYP.
accom      IF L-FLAG(7) NOT = 0
              MOVE 5 TO TYP.
           MOVE 1 TO IDX-HRS(TYP).
           PERFORM ADD-HEURES VARYING JOUR FROM 1 BY 1 UNTIL
              JOUR > MOIS-JRS(LNK-MOIS).
           GO READ-HEURES.
       READ-HEURES-END.
           IF LNK-MOIS = DM-MOIS
              PERFORM FILL-FILE VARYING TYP FROM 1 BY 1 UNTIL TYP > 5.

       ADD-HEURES.
           IF JRS-HRS(JOUR) NOT = 0 
              MOVE JRS-HRS(JOUR) TO TOT-HRS(TYP, JOUR)
           END-IF.

       FILL-FILE.
           MOVE 0 TO JOUR-D JOUR-F DECMAL-HRS.
           IF IDX-HRS(TYP) > 0
           PERFORM TEST-PER VARYING JOUR FROM 1 BY 1 UNTIL
              JOUR > MOIS-JRS(DM-MOIS)
           END-IF.
           IF JOUR-D NOT = 0
              PERFORM FILL-LINE
           END-IF.

       TEST-PER.
           IF  JOUR-D = 0
           AND TOT-HRS(TYP, JOUR) NOT = 0
              MOVE JOUR TO JOUR-D JOUR-F
           END-IF.
           IF TOT-HRS(TYP, JOUR) NOT = 0
              MOVE JOUR TO JOUR-F
           END-IF.
           IF TOT-HRS(TYP, JOUR) > 0
              ADD TOT-HRS(TYP, JOUR) TO DECMAL-HRS
           END-IF.
           IF TOT-HRS(TYP, JOUR) = 0
           AND JOUR-D NOT = 0
               PERFORM FILL-LINE
           END-IF.

       SUITES.
           MOVE TOT-HRS(TYP, 1) TO SUITES(TYP).

       FILL-LINE.
           ADD 1 TO IDX-1.
           MOVE PR-NOM TO LINK-NOM(IDX-1).
           MOVE PR-PRENOM TO LINK-PRENOM(IDX-1).
           MOVE PR-MATRICULE TO LINK-MATRICULE(IDX-1).
           MOVE JOUR-D TO LINK-DU(IDX-1).
           MOVE JOUR-F TO LINK-AU(IDX-1).
           MOVE DECMAL-HRS TO LINK-HRS(IDX-1).
           MOVE TYP TO LINK-TYP(IDX-1).
           IF JOUR-F = MOIS-JRS(MOIS)
              IF SUITES(TYP) > 0
                 MOVE "00" TO LINK-FIN(IDX-1)
              END-IF
           END-IF.
           IF IDX-1 = 15
              PERFORM TRANSMET
           END-IF.
           MOVE 0 TO JOUR-D JOUR-F DECMAL-HRS.

       TRANSMET.
           MOVE FR-MATRICULE TO LINK-MATR.
           MOVE FR-NOM TO LINK-DENOMINATION.
           MOVE MOIS-EN-COURS TO LINK-MOIS.
           MOVE LNK-ANNEE TO LINK-ANNEE.
           MOVE TODAY-JOUR  TO HE-JJ.
           MOVE TODAY-MOIS  TO HE-MM.
           MOVE TODAY-ANNEE TO HE-AA.
           MOVE HE-DATE TO  LINK-DATE.
           MOVE RECTIF TO   LINK-RECTIFICATION.
           CALL "DIT" USING LINK-V LINK-RECORD.
           ADD 1 TO COUNTER.
           INITIALIZE IDX-1 LINK-RECORD.


       DIS-HE-00.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 16 POSITION 27 SIZE 6
           MOVE 0 TO COL-IDX
           IF  FR-NOM-JF = SPACES
           AND PR-NOM-JF = SPACES
              DISPLAY PR-NOM LINE 16 POSITION 47 SIZE 33
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "   "
           ELSE
              DISPLAY PR-NOM-JF LINE 16 POSITION 47 SIZE 33
           INSPECT PR-NOM-JF TALLYING COL-IDX FOR CHARACTERS BEFORE "  "
           END-IF.
           ADD 48 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 16 POSITION COL-IDX SIZE IDX LOW.
           IF A-N = "N"
              DISPLAY SPACES LINE 16 POSITION 35 SIZE 10.
       DIS-HE-01.
           MOVE FR-KEY    TO HE-Z6.
           DISPLAY HE-Z6  LINE  6 POSITION 17.
           DISPLAY FR-NOM LINE  6 POSITION 25 SIZE 30.
       DIS-HE-END.

       AFFICHAGE-ECRAN.
           MOVE 1552 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF COUNTER > 0 
              MOVE 99 TO LNK-VAL
              CALL "DIT" USING LINK-V LINK-RECORD
           END-IF.
           MOVE SAVE-DATE TO LNK-DATE.
           MOVE SAVE-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V A-N FAKE-KEY.
           CANCEL "6-DECMAL".
           CANCEL "DIT".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXF".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

