      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-P11 ANNIVERSAIRES ENTREES DU MOIS         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-P11.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".
           COPY "TRIPR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  CHOIX-MAX             PIC 99 VALUE 11.
       01  PRECISION             PIC 9 VALUE 0.
       01  S-KEY    PIC 9(4) COMP-1 VALUE 65.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".
           COPY "METIER.REC".
           COPY "CONTRAT.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.
       01  LIMITEUR              PIC X(6).
       01  COMPTEUR              PIC 9999 VALUE 0.

       01  NOT-OPEN              PIC 9 VALUE 0.

       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".P11".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.
       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.

       01  ANNEE-DEBUT            PIC 9999.
       01  ANNEE-FIN              PIC 9999.
       01  ANCIENNETE             PIC X VALUE "N".


       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM
               TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-P11.
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE LNK-MOIS  TO MOIS-DEBUT MOIS-FIN.
           MOVE FR-ETAB-A TO ANNEE-DEBUT.
           MOVE LNK-ANNEE TO ANNEE-FIN.
           INITIALIZE TEST-EXTENSION LIN-NUM LIN-IDX LNK-VAL.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TEST-EXTENSION.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 11    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8
           WHEN  9 PERFORM AVANT-9
           WHEN 10 PERFORM AVANT-10
           WHEN 11 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7 
           WHEN  8 PERFORM APRES-8 
           WHEN  9 PERFORM APRES-9 
           WHEN 10 PERFORM APRES-10
           WHEN 11 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-6.
           ACCEPT ANNEE-DEBUT
             LINE 12 POSITION 29 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           ACCEPT ANNEE-FIN
             LINE 13 POSITION 29 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
                     PERFORM APRES-8
             WHEN 66 ADD      1 TO   MOIS-DEBUT
                     PERFORM APRES-8
             END-EVALUATE.

       AVANT-9.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
                     PERFORM APRES-9
             WHEN 66 ADD      1 TO   MOIS-FIN
                     PERFORM APRES-9
             END-EVALUATE.

       AVANT-10.
           ACCEPT ANCIENNETE
             LINE 22 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       APRES-6.
           COMPUTE IDX-4 = LNK-ANNEE - 75
           IF ANNEE-DEBUT < IDX-4 
              MOVE IDX-4 TO ANNEE-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF ANNEE-DEBUT > LNK-ANNEE
              MOVE LNK-ANNEE TO ANNEE-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF ANNEE-DEBUT > ANNEE-FIN
              MOVE ANNEE-DEBUT TO ANNEE-FIN.
           PERFORM DIS-HE-06.
           PERFORM DIS-HE-07.

       APRES-7.
           IF ANNEE-FIN > LNK-ANNEE 
              MOVE LNK-ANNEE TO ANNEE-FIN
              MOVE 1 TO INPUT-ERROR.
           IF ANNEE-DEBUT > ANNEE-FIN
              MOVE ANNEE-DEBUT TO ANNEE-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-06.
           PERFORM DIS-HE-07.

       APRES-8.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-09.
           PERFORM DIS-HE-08.

       APRES-9.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-09.

       APRES-10.
           IF ANCIENNETE = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-2
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           IF COUNTER = 0
           AND CHOIX > 0
              IF  TEST-ALPHA  = SPACES
              AND TEST-NUMBER = 0
                 MOVE TRIPR-CHOIX TO TEST-EXTENSION
              END-IF
           ELSE
              IF TEST-ALPHA  NOT = SPACES
              OR TEST-NUMBER NOT = 0
                 IF TRIPR-CHOIX NOT = TEST-EXTENSION
                 INITIALIZE TEST-EXTENSION
                 PERFORM TRANSMET
                 MOVE 0 TO LIN-NUM
              END-IF
              IF  TEST-ALPHA  = SPACES
              AND TEST-NUMBER = 0
                 MOVE TRIPR-CHOIX TO TEST-EXTENSION
              END-IF
           END-IF.
           PERFORM DIS-HE-01.
           IF CHOIX = 0
              IF ANCIENNETE = "N"
                 PERFORM READ-CONTRAT
                 IF CON-DEBUT-A > ANNEE-FIN
                 OR CON-DEBUT-A < ANNEE-DEBUT
                    CONTINUE
                 ELSE
                    IF  CON-DEBUT-M >= MOIS-DEBUT
                    AND CON-DEBUT-M <= MOIS-FIN
                        PERFORM WRITE-SORT
                    END-IF
                 END-IF
              ELSE
                 IF REG-ANCIEN-A > ANNEE-FIN
                 OR REG-ANCIEN-A < ANNEE-DEBUT
                    CONTINUE
                 ELSE
                    IF  REG-ANCIEN-M >= MOIS-DEBUT
                    AND REG-ANCIEN-M <= MOIS-FIN
                        PERFORM WRITE-SORT
                    END-IF
                 END-IF
              END-IF
           ELSE
              PERFORM FILL-FILES
              PERFORM FILL-LINE
           END-IF.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.


       ENTETE.
      * DONNEES FIRME
           COMPUTE LIN-NUM = 4.
           MOVE  4 TO CAR-NUM.
           MOVE  9 TO COL-NUM.
           MOVE  0 TO POINTS DEC-NUM.
           INITIALIZE VH-XX.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 14 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 5.
           MOVE 14 TO COL-NUM.
           MOVE FR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 20 TO COL-NUM.
           MOVE FR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 6.
           MOVE 14 TO COL-NUM.
           MOVE FR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 16 TO COL-NUM.
           MOVE FR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 21 TO COL-NUM.
           MOVE FR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM =  7 .
           MOVE 14 TO COL-NUM.
           MOVE FR-ETAB-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD   1 TO COL-NUM.
           MOVE FR-ETAB-N TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD   1 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD   1 TO COL-NUM.
           MOVE FR-SNOCS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD   1 TO COL-NUM.
           MOVE FR-EXTENS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE  2 TO CAR-NUM.
           MOVE  64 TO COL-NUM.
           ADD 1 TO PAGE-NUMBER.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

      *    DATE EDITION
           COMPUTE LIN-NUM = 4.
           MOVE  64 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  67 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  71 TO COL-NUM.
           MOVE TODAY-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

       FILL-FILES.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER 
           END-IF.
           COMPUTE IDX = LIN-NUM + 3.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              PERFORM READ-FORM
              PERFORM ENTETE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              COMPUTE LIN-NUM = LIN-IDX 
           END-IF.

       FILL-LINE.
      * DONNEES PERSONNE
           ADD 1 TO LIN-NUM.
           MOVE 6 TO CAR-NUM.
           MOVE 7 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.

           MOVE 14 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 4 TO CAR-NUM.
           MOVE 48 TO COL-NUM.
           COMPUTE VH-00 = LNK-ANNEE - REG-ANCIEN-A.
           PERFORM FILL-FORM.

           MOVE 55 TO COL-NUM.
           MOVE REG-ANCIEN-A TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE REG-ANCIEN-M TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE REG-ANCIEN-J TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           IF REG-DATE-ANCIEN NOT = CON-DATE-DEBUT
              MOVE 68 TO COL-NUM
              MOVE CON-DEBUT-A TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE CON-DEBUT-M TO ALPHA-TEXTE
              ADD 1 TO COL-NUM
              PERFORM FILL-FORM
              MOVE CON-DEBUT-J TO ALPHA-TEXTE
              ADD 1 TO COL-NUM
              PERFORM FILL-FORM.

           ADD 1 TO LIN-NUM.
           MOVE CAR-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           MOVE COUT-NOM TO ALPHA-TEXTE.
           MOVE 14 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE CAR-METIER TO MET-CODE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           ADD 5 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.

       TRANSMET.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.
           IF COMPTEUR > 0
              MOVE 99 TO CHOIX
           ELSE
              PERFORM END-PROGRAM
           END-IF. 
           PERFORM START-TRI.
           PERFORM READ-TRI THRU READ-TRI-END.
           PERFORM END-PROGRAM.

       START-TRI.
           INITIALIZE TRIPR-RECORD.
           MOVE 13 TO EXC-KEY.
           START TRIPR KEY >  TRIPR-KEY INVALID
              PERFORM END-PROGRAM.

       READ-TRI.
           READ TRIPR NEXT AT END 
              GO READ-TRI-END
           END-READ.
           MOVE TRIPR-PERSON TO REG-PERSON.
           PERFORM NEXT-REGIS.
           PERFORM CAR-RECENTE.
              PERFORM FILL-FILES
              PERFORM FILL-LINE
      *    PERFORM READ-PERSON-1 THRU READ-PERSON-2.
           GO READ-TRI.
       READ-TRI-END.
           EXIT.       
       
       START-SORT.
           MOVE "AA" TO LNK-AREA.
           MOVE  23  TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           MOVE LNK-USER  TO USER-TRIPR.
           MOVE FR-KEY TO FIRME-TRIPR.
           OPEN I-O TRIPR.
           MOVE 1 TO NOT-OPEN.

       WRITE-SORT.
           IF NOT-OPEN = 0
              PERFORM START-SORT.
           IF ANCIENNETE = "N"
              MOVE CON-DEBUT-M TO TRIPR-NUMBER
           ELSE
              MOVE REG-ANCIEN-M TO TRIPR-NUMBER.
           IF A-N = "A"
              MOVE REG-MATCHCODE TO TRIPR-MATCHCODE  
           ELSE
              IF ANCIENNETE = "N"
                 MOVE CON-DATE-DEBUT TO TRIPR-MATCHCODE  
              ELSE
                 MOVE REG-DATE-ANCIEN TO TRIPR-MATCHCODE  
              END-IF
           END-IF.
           MOVE REG-PERSON TO TRIPR-PERSON.
           DISPLAY REG-PERSON LINE 24 POSITION 60.
           WRITE TRIPR-RECORD INVALID REWRITE TRIPR-RECORD END-WRITE.
           ADD 1 TO COMPTEUR.


       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE 12        TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD S-KEY.

           COPY "XDIS.CPY".
           
       DIS-HE-06.
           DISPLAY ANNEE-DEBUT LINE 12 POSITION 29.
       DIS-HE-07.
           DISPLAY ANNEE-FIN   LINE 13 POSITION 29.
       DIS-HE-08.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 19 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-09.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-10.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1211 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF NOT-OPEN = 1
              CLOSE TRIPR
              DELETE FILE TRIPR.
           IF LIN-NUM > LIN-IDX 
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           CANCEL "5-SEL"
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".

