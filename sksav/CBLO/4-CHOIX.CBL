      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-CHOIX DESIGNATION CHOIX ENTETE IMPRESSION �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-CHOIX.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "METIER.REC".
           COPY "MESSAGE.REC".
           COPY "EQUIPE.REC".
           COPY "POCL.REC".
           COPY "FICHE.REC".
           COPY "DIVISION.REC".
           COPY "REGIME.REC".
           COPY "PAYS.REC".
           COPY "CONTYPE.REC".

           COPY "V-VAR.CPY".
       01  HE-Z8 PIC Z(8).
       01  MONTANT-R REDEFINES HE-Z8 PIC X(8).
       01  HELP-TEXT PIC X(50).
        
       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-CHOIX.

           INITIALIZE HELP-TEXT.
           IF LNK-POSITION > 0
              MOVE LNK-POSITION TO HE-Z8
              MOVE 1 TO IDX-3
              INSPECT HE-Z8 TALLYING IDX-3 FOR ALL " " BEFORE ","
              UNSTRING MONTANT-R INTO LNK-TEXT WITH POINTER IDX-3
           END-IF.
           MOVE 1 TO IDX-3.
           STRING LNK-TEXT DELIMITED BY "  " INTO HELP-TEXT
           WITH POINTER IDX-3.
           ADD 2 TO IDX-3.
           IF LNK-NUM = 1
              MOVE FR-KEY TO COUT-FIRME 
              MOVE LNK-POSITION TO COUT-NUMBER
              CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY
              STRING COUT-NOM DELIMITED BY "  " INTO HELP-TEXT
              WITH POINTER IDX-3
           END-IF
           IF LNK-NUM = 2
              MOVE LNK-POSITION TO REGIME-NUMBER
              CALL "6-REGIME" USING LINK-V REGIME-RECORD EXC-KEY
              STRING REGIME-NOM DELIMITED BY "  " INTO HELP-TEXT
              WITH POINTER IDX-3
           END-IF
           IF LNK-NUM = 3
              MOVE FR-KEY TO EQ-FIRME 
              MOVE LNK-POSITION TO EQ-NUMBER
              CALL "6-EQUIPE" USING LINK-V EQ-RECORD FAKE-KEY
              STRING EQ-NOM DELIMITED BY "  " INTO HELP-TEXT
              WITH POINTER IDX-3
           END-IF
           IF LNK-NUM = 4
              MOVE FR-KEY TO DIV-FIRME 
              MOVE LNK-POSITION TO DIV-NUMBER
              CALL "6-DIV" USING LINK-V DIV-RECORD FAKE-KEY
              STRING DIV-NOM DELIMITED BY "  " INTO HELP-TEXT
              WITH POINTER IDX-3
           END-IF
           IF LNK-NUM = 7
              MOVE LNK-POSITION TO STAT-CODE
              CALL "6-STATUT" USING LINK-V STAT-RECORD EXC-KEY
              STRING STAT-NOM DELIMITED BY "  " INTO HELP-TEXT
              WITH POINTER IDX-3
           END-IF
           IF LNK-NUM = 6
              INITIALIZE MS-RECORD
              MOVE LNK-POSITION TO MS-NUMBER
              MOVE "SX" TO LNK-AREA
              CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY
              STRING MS-DESCRIPTION DELIMITED BY "  " INTO HELP-TEXT
              WITH POINTER IDX-3
           END-IF
           IF LNK-NUM = 8
              INITIALIZE MS-RECORD
              EVALUATE LNK-TEXT
                 WHEN "C" MOVE 1 TO MS-NUMBER
                 WHEN "M" MOVE 2 TO MS-NUMBER
                 WHEN "V" MOVE 3 TO MS-NUMBER
                 WHEN "D" MOVE 4 TO MS-NUMBER
                 WHEN "S" MOVE 5 TO MS-NUMBER
              END-EVALUATE
              MOVE "EC" TO LNK-AREA
              CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY
              STRING MS-DESCRIPTION DELIMITED BY "  " INTO HELP-TEXT
              WITH POINTER IDX-3
           END-IF
           IF LNK-NUM = 9
              MOVE LNK-TEXT TO MET-CODE
              CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY
              MOVE MET-NOM(1) TO HELP-TEXT
              MOVE 1 TO IDX-3
              STRING MET-NOM(1) DELIMITED BY "  " INTO HELP-TEXT
              WITH POINTER IDX-3
           END-IF
           IF  LNK-NUM > 13
           AND LNK-NUM < 16
              MOVE LNK-TEXT TO PAYS-CODE
              CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY
              STRING PAYS-NOM DELIMITED BY "  " INTO HELP-TEXT
              WITH POINTER IDX-3
           END-IF.
           IF LNK-NUM = 17
              MOVE LNK-POSITION TO CT-CODE
              MOVE LNK-LANGUAGE TO CT-LANGUE
              CALL "6-CONTYP" USING LINK-V CT-RECORD FAKE-KEY
              STRING CT-NOM DELIMITED BY "  " INTO HELP-TEXT
              WITH POINTER IDX-3
           END-IF.
           IF LNK-NUM = 22
              MOVE LNK-POSITION TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              STRING PC-NOM DELIMITED BY "  " INTO HELP-TEXT
              WITH POINTER IDX-3
           END-IF

           MOVE HELP-TEXT TO LNK-TEXT.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XACTION.CPY".


