      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-LP INTERROGATION LIVRE DE PAIE            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-LP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "LIVRE.REC".

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  HEL-VAL              PIC ZZ BLANK WHEN ZERO.
       01  HEL-1                PIC 9999.
       01  HEL-2                PIC 9.
       01  CHOIX                 PIC X.
       01  TOTAUX.
           02 SUITE OCCURS 2.
              03 TOT  PIC S9(8)V99 OCCURS 8.

       
       01   ECR-DISPLAY.
            02 HE-Z1 PIC Z.
            02 HE-Z2 PIC ZZ.
            02 HE-Z5 PIC Z(5).
            02 HE-Z6 PIC Z(6).
            02 HE-Z8 PIC Z(8).
            02 HEZ4Z2 PIC Z(4),ZZ.
            02 HEZ5Z2 PIC Z(5),ZZ.
            02 HEZ6Z2 PIC Z(6),ZZ.
            02 HEZ7Z2 PIC Z(7),ZZ.
            02 HEZ6Z1 PIC Z(6),Z-.
            02 HEZ7Z1 PIC Z(7),Z-.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-LP.
       
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0027000000 TO EXC-KFR(6).
           MOVE 0052530000 TO EXC-KFR(11).
           MOVE 0000000065 TO EXC-KFR(13).
           MOVE 6600000000 TO EXC-KFR(14).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 .

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 .

           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.          
           MOVE FR-KEY TO REG-FIRME.
           IF LNK-PERSON NOT = 0 
              MOVE LNK-PERSON TO REG-PERSON 
              MOVE LNK-MOIS   TO L-MOIS 
              MOVE 13 TO EXC-KEY
              PERFORM NEXT-REGIS
              MOVE 3 TO INDICE-ZONE
              MOVE 0 TO LNK-PERSON
           ELSE 
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN 5 CALL "1-REGIST" USING LINK-V
                  IF LNK-PERSON > 0
                     MOVE LNK-PERSON TO REG-PERSON
                     PERFORM NEXT-REGIS
                  END-IF
                  CANCEL "1-REGIST"  
                  PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE
              IF PRES-ANNEE = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           IF LNK-COMPETENCE < REG-COMPETENCE
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-PERSON
           END-IF.
           PERFORM DIS-HE-01 THRU DIS-HE-02.
           IF INPUT-ERROR = 0
              PERFORM AFFICHAGE-DETAIL
           END-IF.

       APRES-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   PERFORM DIS-HE-01 THRU DIS-HE-02
                   PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.                     


       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           
       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.

       AFFICHAGE-ECRAN.
           MOVE 2510 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           INITIALIZE L-RECORD IDX LNK-SUITE TOTAUX.
           MOVE 4 TO LIN-IDX.
           MOVE 66  TO SAVE-KEY.
           PERFORM READ-LIVRE THRU READ-LIVRE-END.

       READ-LIVRE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD SAVE-KEY.
           IF L-FIRME = 0
              GO READ-LIVRE-END
           END-IF.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           MOVE L-JOUR-DEBUT TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 1 LOW.
           MOVE L-JOUR-FIN TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 3.
           MOVE L-MOIS TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 5 REVERSE.
           MOVE  L-IMPOSABLE-BRUT TO HEZ7Z2.
           INSPECT HEZ7Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ7Z2  LINE  LIN-IDX POSITION 7.
           IF L-SUITE > 0
              MOVE L-SUITE TO HE-Z1
              DISPLAY HE-Z1 LINE LIN-IDX POSITION 7 LOW
              IF L-FLAG-AVANCE = 0
                 ADD L-IMPOSABLE-BRUT TO TOT(2, 1)
              END-IF
           ELSE
              ADD L-IMPOSABLE-BRUT TO TOT(1, 1)
           END-IF.
           COMPUTE SH-00 = L-COTISATIONS + L-CAISSE-DEP-SAL.
           MOVE SH-00 TO HEZ5Z2.
           INSPECT HEZ5Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ5Z2 LINE LIN-IDX POSITION 17 LOW.
           IF L-SUITE > 0
              IF L-FLAG-AVANCE = 0
                 ADD SH-00 TO TOT(2, 2)
              END-IF
           ELSE
              ADD SH-00 TO TOT(1, 2)
           END-IF.

           MOVE L-IMPOT TO HEZ6Z1.
           DISPLAY HEZ6Z1 LINE LIN-IDX POSITION 25.
           IF L-SUITE > 0
              IF L-FLAG-AVANCE = 0
                 ADD L-IMPOT TO TOT(2, 3)
              END-IF
           ELSE
              ADD L-IMPOT TO TOT(1, 3)
           END-IF.

           MOVE  L-SALAIRE-NET TO HEZ7Z2.
           INSPECT HEZ7Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ7Z2 LINE LIN-IDX POSITION 34.
           IF  L-SUITE NOT = 0  
           AND L-FLAG-AVANCE = 1
              DISPLAY HEZ7Z2 LINE LIN-IDX POSITION 34 REVERSE.

           IF L-SUITE > 0
              IF L-FLAG-AVANCE = 0
                 ADD L-SALAIRE-NET TO TOT(2, 4)
              END-IF
           ELSE
              ADD L-SALAIRE-NET TO TOT(1, 4)
           END-IF.

           MOVE  L-IMPOSABLE-BRUT-NP TO HEZ7Z2.
           INSPECT HEZ7Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ7Z2 LINE LIN-IDX POSITION 44.
           ADD L-IMPOSABLE-BRUT-NP TO TOT(1, 5).

           COMPUTE SH-00 = L-COTISATIONS-NP + L-CAISSE-DEP-SAL-NP.
           MOVE SH-00 TO HEZ4Z2.
           INSPECT HEZ4Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ4Z2 LINE LIN-IDX POSITION 54 LOW.
           ADD SH-00 TO TOT(1, 6).

           COMPUTE SH-00 = L-IMPOT-NP - L-DECOMPTE-IMPOT.
           MOVE  SH-00 TO HEZ7Z1.
           DISPLAY HEZ7Z1 LINE LIN-IDX POSITION 61.
           ADD SH-00 TO TOT(1, 7).


           MOVE L-SALAIRE-NET-NP TO HEZ7Z2.
           INSPECT HEZ7Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ7Z2 LINE LIN-IDX POSITION 71.
           ADD L-SALAIRE-NET-NP TO TOT(1, 8).

           IF LIN-IDX > 20
              PERFORM TT
              PERFORM INTERRUPT THRU INTERRUPT-END
              MOVE 4 TO LIN-IDX
           END-IF.
           GO READ-LIVRE.
       READ-LIVRE-END.
           ADD 1 TO LIN-IDX.
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 21.
           PERFORM TT.

       TT.
           MOVE  TOT(1, 1) TO HEZ7Z2.
           INSPECT HEZ7Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ7Z2  LINE  22 POSITION 7.

           MOVE  TOT(1, 2) TO HEZ5Z2.
           INSPECT HEZ5Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ5Z2 LINE 22 POSITION 17 LOW.

           MOVE  TOT(1, 3) TO HEZ6Z1.
           DISPLAY HEZ6Z1 LINE 22 POSITION 25.

           MOVE  TOT(1, 4) TO HEZ7Z2.
           INSPECT HEZ7Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ7Z2 LINE 22 POSITION 34.

           MOVE  TOT(1, 5) TO HEZ7Z2.
           INSPECT HEZ7Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ7Z2 LINE 22 POSITION 44.

           MOVE  TOT(1, 6) TO HEZ4Z2.
           INSPECT HEZ4Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ4Z2 LINE 22 POSITION 54 LOW.

           MOVE  TOT(1, 7) TO HEZ7Z1.
           DISPLAY HEZ7Z1 LINE 22 POSITION 61.

           MOVE  TOT(1, 8) TO HEZ7Z2.
           INSPECT HEZ7Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ7Z2 LINE 22 POSITION 71.

           MOVE  TOT(2, 1) TO HEZ7Z2.
           INSPECT HEZ7Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ7Z2  LINE  23 POSITION 7.

           MOVE  TOT(2, 2) TO HEZ5Z2.
           INSPECT HEZ5Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ5Z2 LINE 23 POSITION 17 LOW.

           MOVE  TOT(2, 3) TO HEZ6Z1.
           DISPLAY HEZ6Z1 LINE 23 POSITION 25.

           MOVE  TOT(2, 4) TO HEZ7Z2.
           INSPECT HEZ7Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ7Z2 LINE 23 POSITION 34.
           
       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.

           ACCEPT CHOIX 
           LINE 25 POSITION 3 SIZE 1
           TAB UPDATE NO BEEP CURSOR 1
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 65 OR 66
              MOVE 13 TO EXC-KEY
           END-IF.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
       INTERRUPT-END.
           EXIT.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

