      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-NATPER IMPRESSION LISTE NOMBRE DE         �
      *  � PERSONNES PAR NATIONALITE                             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-NATPER.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".

      *    Fichier interm괺iaire liste nombre de personnes par nationalit�
           SELECT OPTIONAL INTER ASSIGN TO RANDOM, "INTER.NAT",
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is INTER-KEY,
                  STATUS FS-INTER.

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM80.FDE".

       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire liste nombre de personnes par nationalit�
       01  INTER-RECORD.
           02 INTER-KEY PIC XXX.
           02 INTER-DETAIL.
              03 INTER-COUNTER PIC 9(4) OCCURS 12.
           02 INTER-D-R REDEFINES INTER-DETAIL.
              03 INTER-D OCCURS 4.
                 04 INTER-R PIC 9(4) OCCURS 3.

      *    1 = OM
      *    2 = OF
      *    3 = OT
      *    4 = EM
      *    5 = EF
      *    6 = ET
      *    7 = FM
      *    8 = FF
      *    9 = FT
      *   10 = TM
      *   11 = TF
      *   12 = TT
               
       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  PRECISION             PIC 9 VALUE 0.
       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PAYS.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "IMPRLOG.REC".
           COPY "CARRIERE.REC".
           COPY "V-VAR.CPY".

       01  LANGUE                PIC 9.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

       01  TOTAL-DETAIL.
           03 TOTAL-COUNTER PIC 9(4) OCCURS 12.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".PPN".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                FORM 
                INTER.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-NATPER.
       
           DELETE FILE INTER.
           OPEN I-O INTER.
           CALL "0-TODAY" USING TODAY.
           INITIALIZE LIN-NUM LIN-IDX TOTAL-DETAIL.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
      * param둻res sp괹ifiques touches de fonctions

           MOVE 0000000025 TO EXC-KFR(1).
           MOVE 1700000000 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.

           PERFORM APRES-DEC.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-PERSON
                    PERFORM READ-PERSON THRU READ-PERSON-END
                   PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       START-PERSON.
           INITIALIZE REG-RECORD.
           MOVE 66 TO EXC-KEY.
           MOVE 0  TO SAVE-KEY.

       READ-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" EXC-KEY.
           IF REG-PERSON = 0
              GO READ-PERSON-END
           END-IF.
           PERFORM PRESENCE.
           IF PRES-TOT(LNK-MOIS) = 0
              GO READ-PERSON
           END-IF.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           IF PR-CODE-SEXE = 0
              GO READ-PERSON
           END-IF.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           PERFORM DIS-HE-01.
           IF PR-NATIONALITE = SPACES
              MOVE "XXX" TO PR-NATIONALITE
           END-IF.
           MOVE PR-NATIONALITE TO INTER-KEY.
           READ INTER INVALID INITIALIZE INTER-DETAIL.
           ADD 1 TO INTER-R(CAR-STATUT, PR-CODE-SEXE)
                    INTER-R(CAR-STATUT, 3)
                    INTER-R(4, PR-CODE-SEXE)
                    INTER-R(4, 3).
           WRITE INTER-RECORD INVALID REWRITE INTER-RECORD.
           GO READ-PERSON.
        READ-PERSON-END.
           PERFORM START-INTER.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       START-INTER.
           INITIALIZE INTER-RECORD.
           START INTER KEY > INTER-KEY INVALID KEY CONTINUE
                NOT INVALID 
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END 
               GO READ-INTER-END
           END-READ.
           PERFORM READ-PAYS.
           GO READ-INTER.
       READ-INTER-END.
           IF LIN-NUM > LIN-IDX
              MOVE TOTAL-DETAIL TO INTER-DETAIL
              MOVE "TOT" TO INTER-KEY PAYS-NOM
              PERFORM FILL-FILES
              PERFORM PAGE-DATE
              PERFORM TRANSMET.

       READ-PAYS.
           MOVE INTER-KEY TO PAYS-CODE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE IDX = LIN-NUM + 3.
           IF IDX >= IMPL-MAX-LINE
              PERFORM PAGE-DATE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM FILL-FIRME
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           PERFORM FILL-FILES.

       DIS-HE-01.
           MOVE REG-PERSON   TO HE-Z6.
           DISPLAY HE-Z6     LINE 17 POSITION 35.
           DISPLAY PR-NOM    LINE 17 POSITION 45.
           DISPLAY PR-PRENOM LINE 18 POSITION 45.
       DIS-HE-END.
           EXIT.

       FILL-FIRME.
           MOVE  3 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM =  2.
           PERFORM MOIS-NOM.
           MOVE 45 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 55 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE LNK-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       FILL-FILES.
           ADD 2 TO LIN-NUM.

      * DONNEES PAYS
           MOVE  5 TO COL-NUM.
           MOVE PAYS-NOM TO ALPHA-TEXTE.
           IF ALPHA-TEXTE = SPACES
              MOVE INTER-KEY TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           PERFORM DONNEES-PERS VARYING IDX FROM 1 BY 1 UNTIL IDX > 12.

        DONNEES-PERS.
           COMPUTE COL-NUM = 15 + IDX * 5.
           MOVE 4 TO CAR-NUM.
           MOVE INTER-COUNTER(IDX) TO  VH-00.
           ADD INTER-COUNTER(IDX) TO TOTAL-COUNTER(IDX).
           PERFORM FILL-FORM.

       PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  4 TO CAR-NUM.
           MOVE  2 TO LIN-NUM.
           MOVE 73 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  6 TO LIN-NUM.
           MOVE 67 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 70 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 73 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 1205 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE INTER.
           DELETE FILE INTER.
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".

