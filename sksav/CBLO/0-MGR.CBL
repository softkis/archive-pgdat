      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-MGR GESTION DES MENUS                     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.   0-MGR.

       ENVIRONMENT DIVISION.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MENU.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "MENU.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PARMOD.REC".
           COPY "MENU.LNK".
       01  IDX-LEVEL             PIC 99 COMP-6 VALUE 1.
       01  CHOIX                 PIC 99.
       01  COPIE-MOIS            PIC 99.
       01  JOUR-SEMAINE          PIC 9.

       01  HELP-01               PIC 99.
       01  HE-Z4 PIC Z(4).
       01  HE-ZX REDEFINES HE-Z4.
              03 HE-M PIC Z.
              03 HE-F PIC Z.
              03 HE-A PIC ZZ.

       01  CALL-WORD.
           02 CW-1               PIC XX.
           02 CW-2               PIC X(8).
       01  CALL-WORD-R REDEFINES CALL-WORD.
           02 CW-0               PIC X.
           02 CW-9               PIC X(9).
       01  CALL-WORD-R1 REDEFINES CALL-WORD.
           02 CW-3               PIC X.
           02 CW-4               PIC X.
           02 CW-5               PIC X(8).

       01  H-M.
           02 HELP-MENU OCCURS 15.
              03 HELP-KEY.
                 04 HELP-LEVEL.
                    05 HELP-LEV          PIC 99 OCCURS 5.

              03 HELP-BODY.
                 04 HELP-PROG-NAME       PIC X(8).
                 04 HELP-PROG-NAME-R REDEFINES HELP-PROG-NAME.
                    05 HELP-PROG-BEG     PIC XX.
                    05 HELP-PROG-END     PIC X(6).
                 04 HELP-COMPETENCE      PIC 9.
                 04 HELP-CALL            PIC X(8).
                 04 HELP-PASSWORD        PIC X(22).
                 04 HELP-DESCRIPTION     PIC X(40).
                 04 HELP-TEXT            PIC 9(8).
                 04 HELP-PROG-NUMBER     PIC 9(8).
                 04 HELP-EXTENSION-1     PIC X(8).
                 04 HELP-EXTENSION-2     PIC X(8).
                 04 HELP-TEST            PIC 9.
                 04 HELP-BATCH           PIC 9.
                 04 HELP-LOG             PIC 9.
                 04 HELP-FILLER          PIC X(20).

       01  CHOIX-MAX             PIC 99 VALUE 1.

       01  X-PASSWORD            PIC X(10).
       01  X-CALLWORD            PIC X(8).
       01  XX                    PIC X.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MENU.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-MGR.

           PERFORM JOUR-SEMAINE.
           OPEN INPUT MENU.
           CALL "0-TODAY" USING TODAY.

           MOVE 1 TO IDX-LEVEL CHOIX.
           INITIALIZE PARMOD-RECORD.

           PERFORM AFFICHAGE-ECRAN.
           PERFORM NEXT-MENU VARYING IDX FROM 1 BY 1 UNTIL IDX > 15.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 
           IF INDICE-ZONE < 1 
              MOVE 1 TO INDICE-ZONE.
           INITIALIZE EXC-KEY-CTL.

           MOVE 3100000079 TO EXC-KFR (2).
           MOVE 0012130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).
           MOVE 0000000065 TO EXC-KFR (13).
           MOVE 6667680000 TO EXC-KFR (14).
            
       TRAITEMENT-ECRAN-01.

           MOVE 0063644332 TO EXC-KFR (1).
           IF CHOIX > 0
           AND CHOIX < 16
              IF HELP-TEXT(CHOIX) > 0
                 MOVE 0163644332 TO EXC-KFR (1)
              END-IF
           END-IF.
           IF LNK-FIRME > 0
              MOVE 0 TO EXC-KEY-FUN(2) EXC-KEY-FUN(3)
           END-IF.
           PERFORM DISPLAY-F-KEYS.

           PERFORM AVANT-1.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           PERFORM APRES-1.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-1.     
           IF CHOIX < 1 
              MOVE 1 TO CHOIX 
              PERFORM NEXT-MENU 
              VARYING IDX FROM 1 BY 1 UNTIL IDX > 15.
           MOVE SPACES TO CALL-WORD.
           IF CHOIX < 10
              COMPUTE HELP-01 = CHOIX * 10
              MOVE HELP-01  TO CW-0
           ELSE
              MOVE CHOIX TO CW-1.
           MOVE CHOIX TO IDX.
           ACCEPT CALL-WORD
             LINE 23 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF EXC-KEY = 11
              MOVE CHOIX TO MN-LEV(IDX-LEVEL)
              READ MENU 
                INVALID CONTINUE
              NOT INVALID
              DISPLAY MN-PROG-NAME   LINE 24 POSITION 10
              DISPLAY MN-EXTENSION-1 LINE 24 POSITION 20
              DISPLAY MN-EXTENSION-2 LINE 24 POSITION 30
              GO AVANT-1
           END-IF.
           IF CW-0 NUMERIC
              CONTINUE
           ELSE
              INSPECT CW-4 CONVERTING "0123456789" TO "          ".

           EVALUATE EXC-KEY
             WHEN 65 THRU 66 PERFORM CHANGE-MOIS
             WHEN 56 THRU 58 PERFORM CHANGE-MOIS
             GO AVANT-1
           END-EVALUATE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-1.
           IF EXC-KEY = 67 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 68 MOVE 13 TO EXC-KEY.
           EVALUATE EXC-KEY
                WHEN  1 MOVE HELP-TEXT(CHOIX) TO LNK-VAL
                        CALL "0-BOOK" USING LINK-V
                        CANCEL "0-BOOK" 
                        PERFORM AFFICHAGE-ECRAN 
                        PERFORM NEXT-MENU VARYING IDX FROM 1 
                        BY 1 UNTIL IDX  > 15
                WHEN  2 THRU 3 
                        IF EXC-KEY = 2
                           MOVE "A" TO LNK-A-N
                        ELSE
                           MOVE "N" TO LNK-A-N
                        END-IF
                        MOVE 1 TO LNK-PRESENCE
                        CALL "2-FIRME" USING LINK-V
                        CANCEL "2-FIRME"
                        MOVE 0 TO LNK-PRESENCE
                        PERFORM AFFICHAGE-ECRAN 
                        PERFORM NEXT-MENU VARYING IDX FROM 1 
                        BY 1 UNTIL IDX  > 15
                WHEN  4 PERFORM LANGUE
                WHEN  5 PERFORM ACCEPT-MOIS
                WHEN  6 PERFORM ACCEPT-ANNEE
                WHEN 10 INITIALIZE LNK-TEXT
                        CALL "2-MENU" USING LINK-V LINK-RECORD
                        IF LINK-PROG-NAME > SPACES
                           MOVE LINK-PASSWORD TO LNK-TEXT
                           CALL "0-PASSW" USING LINK-V
                           IF LNK-VAL NOT = 0
                              INITIALIZE LINK-RECORD
                           END-IF
                        END-IF
                        IF LINK-PROG-NAME > SPACES
                           MOVE LINK-RECORD TO MN-RECORD MENU-RECORD
                           MOVE 0 TO LNK-PERSON
                           MOVE MN-PROG-NAME TO X-CALLWORD
                           IF X-CALLWORD > SPACES
                              CALL X-CALLWORD USING LINK-V
                               EXCEPTION   
                                MOVE 1 TO INPUT-ERROR
                                MOVE "AA" TO LNK-AREA
                                MOVE 8 TO LNK-NUM
                                MOVE 0 TO LNK-POSITION
                                PERFORM DISPLAY-MESSAGE
                              END-CALL
                              CANCEL X-CALLWORD
                              PERFORM GET-INDEX-LEVEL
                              MOVE MN-LEV(IDX-LEVEL) TO CHOIX
                           END-IF
                        END-IF
                        CANCEL "2-MENU"
                        PERFORM AFFICHAGE-ECRAN 
                        PERFORM NEXT-MENU VARYING IDX FROM 1 
                        BY 1 UNTIL IDX  > 15
                WHEN 12 CALL CALL-WORD USING LINK-V
                     EXCEPTION 
                     MOVE 1 TO INPUT-ERROR
                     MOVE "AA" TO LNK-AREA
                     MOVE 8 TO LNK-NUM
                     PERFORM DISPLAY-MESSAGE
                     END-CALL
                     IF LNK-LANGUAGE = SPACES
                        MOVE FR-LANGUE TO LNK-LANGUAGE
                     END-IF
                     IF LNK-LANGUAGE = SPACES
                        MOVE "F" TO LNK-LANGUAGE
                     END-IF
                     CANCEL CALL-WORD
                     PERFORM AFFICHAGE-ECRAN
                WHEN 13 PERFORM FURTHER-ACTION
                WHEN 53 PERFORM STEP-ON
                WHEN 52 PERFORM STEP-ON
                WHEN 27 SUBTRACT 1 FROM IDX-LEVEL
                        IF IDX-LEVEL < 1 
                            MOVE 1 TO IDX-LEVEL 
                        END-IF
                        MOVE MN-LEV(IDX-LEVEL) TO CHOIX
                        PERFORM NEXT-MENU VARYING IDX FROM 1 
                        BY 1 UNTIL IDX  > 15
                WHEN OTHER MOVE 1 TO INPUT-ERROR.
           IF  EXC-KEY NOT = 27
           AND EXC-KEY NOT = 5 
           AND EXC-KEY NOT = 53
           AND EXC-KEY NOT = 52
               PERFORM DISPLAY-ECRAN VARYING IDX FROM 1 BY 1 
                                 UNTIL IDX > 15
           END-IF.
           IF  EXC-KEY NOT = 5 
           AND EXC-KEY NOT = 53
           AND EXC-KEY NOT = 52
              CALL "0-MGRR" USING MN-LEVEL LNK-LANGUAGE
           END-IF.
           IF EXC-KEY = 27
           AND IDX-LEVEL < 5
              COMPUTE LIN-IDX = 2 + IDX-LEVEL
              DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 40
           END-IF.
                
       LANGUE.
           MOVE LNK-LANGUAGE TO XX.
           ACCEPT LNK-LANGUAGE LINE 1 POSITION 2 SIZE 1
              TAB UPDATE NO BEEP CURSOR 1
              CONTROL "UPPER"
              ON EXCEPTION  EXC-KEY  CONTINUE.
           IF LNK-LANGUAGE = "F" OR = "D" OR = "E"
              CONTINUE
           ELSE
              MOVE XX TO LNK-LANGUAGE  
           END-IF.
           IF LNK-LANGUAGE NOT = XX
              EVALUATE LNK-LANGUAGE
                 WHEN "F" MOVE 1 TO LG-IDX
                 WHEN "D" MOVE 2 TO LG-IDX
                 WHEN "E" MOVE 3 TO LG-IDX
              END-EVALUATE
              PERFORM AFFICHAGE-ECRAN 
              PERFORM NEXT-MENU VARYING IDX FROM 1 BY 1 UNTIL IDX > 15
           END-IF.

       ACCEPT-MOIS.
           PERFORM F-KEYS.     
           MOVE LNK-MOIS TO COPIE-MOIS.
           ACCEPT LNK-MOIS
             LINE 1 POSITION 74 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 65
              AND LNK-MOIS > 1
              SUBTRACT 1 FROM LNK-MOIS.
           IF EXC-KEY = 66
              AND LNK-MOIS < 12
              ADD 1 TO LNK-MOIS.
           IF LNK-MOIS < 1
           OR LNK-MOIS > 12 
              MOVE COPIE-MOIS TO LNK-MOIS
              GO ACCEPT-MOIS
           END-IF.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              GO ACCEPT-MOIS
           END-IF.
           PERFORM STOPS.

       ACCEPT-ANNEE.
           PERFORM F-KEYS.     
           ACCEPT LNK-ANNEE
             LINE 1 POSITION 77 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 65
              SUBTRACT 1 FROM LNK-ANNEE.
           IF EXC-KEY = 66
              ADD 1 TO LNK-ANNEE.
           IF LNK-ANNEE < LNK-LIMITE
              MOVE LNK-LIMITE TO LNK-ANNEE
              GO ACCEPT-ANNEE
           END-IF.
           PERFORM STOPS.
           IF LNK-ANNEE > LNK-TOP
              MOVE LNK-TOP TO LNK-ANNEE
              GO ACCEPT-ANNEE
           END-IF.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              GO ACCEPT-ANNEE
           END-IF.
           PERFORM JOUR-SEMAINE.

       JOUR-SEMAINE.
           CALL   "0-JRSEM" USING LINK-V.
           CANCEL "0-JRSEM".
           CALL   "1-LINDEX" USING LINK-V.
           CANCEL "1-LINDEX".

       F-KEYS.     
           MOVE 0000000000 TO EXC-KFR (1).
           MOVE 0000000900 TO EXC-KFR (2).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0056000000 TO EXC-KFR (12).
           PERFORM DISPLAY-F-KEYS.

       STEP-ON.
           MOVE 0 TO NOT-FOUND.
           IF CHOIX > 15 MOVE 15 TO CHOIX.
           MOVE CHOIX TO IDX.
           IF EXC-KEY = 53 
              PERFORM SEARCH-FORWARD UNTIL NOT-FOUND = 1
           ELSE
              PERFORM SEARCH-BACKWARD UNTIL NOT-FOUND = 1
           END-IF    
           PERFORM DISPLAY-ECRAN.

           MOVE CHOIX TO IDX.
           PERFORM DISPLAY-ECRAN.

       SEARCH-FORWARD.
           ADD 1 TO CHOIX.
           IF CHOIX > 15 MOVE 1 TO CHOIX.
           MOVE HELP-TEST(CHOIX) TO NOT-FOUND.

       SEARCH-BACKWARD.
           SUBTRACT 1 FROM CHOIX.
           IF CHOIX < 1 MOVE 15 TO CHOIX.
           MOVE HELP-TEST(CHOIX) TO NOT-FOUND.

       FURTHER-ACTION.
           MOVE 0 TO HELP-01 IDX-3.
           IF CW-0 NUMERIC 
           MOVE CW-0 TO HELP-01.
           IF CW-1 NUMERIC 
           MOVE CW-1 TO HELP-01.
           INSPECT CALL-WORD TALLYING IDX-3 FOR ALL "-".
           IF  HELP-01 NOT = 0 
           AND HELP-01 < 16
           AND IDX-3 = 0
              MOVE HELP-01 TO CHOIX
           ELSE
              IF CALL-WORD NOT = SPACES
                 PERFORM CALL-BY-NAME
                 MOVE 1 TO INPUT-ERROR
               END-IF
           END-IF.
           IF CHOIX > 15 
           OR CHOIX < 1
              MOVE MN-LEV(IDX-LEVEL) TO CHOIX
              MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0 
              IF HELP-DESCRIPTION(CHOIX) = SPACES
                 MOVE 1 TO INPUT-ERROR CHOIX
              END-IF
           END-IF. 
           IF INPUT-ERROR = 0 
           IF LNK-COMPETENCE < HELP-COMPETENCE(CHOIX)
               MOVE 1 TO INPUT-ERROR
               MOVE "AA" TO LNK-AREA
               MOVE 4 TO LNK-NUM
               MOVE 0 TO LNK-POSITION
               PERFORM DISPLAY-MESSAGE.
           IF INPUT-ERROR = 0 
              MOVE HELP-PASSWORD(CHOIX) TO LNK-TEXT
              CALL "0-PASSW" USING LINK-V
              MOVE LNK-VAL TO INPUT-ERROR
           END-IF. 
           IF INPUT-ERROR = 0 
              IF HELP-PROG-NAME(CHOIX) NOT = SPACES
              PERFORM CALL-PROGRAM
              MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0 
              MOVE CHOIX TO MN-LEV(IDX-LEVEL)
              ADD 1 TO IDX-LEVEL
              IF IDX-LEVEL > 5 
                 MOVE 5 TO IDX-LEVEL
              END-IF
              MOVE 1 TO CHOIX
              MOVE 66 TO EXC-KEY
              PERFORM NEXT-MENU VARYING IDX FROM 1 BY 1 UNTIL IDX > 15.

       CALL-BY-NAME.
           MOVE 0 TO INPUT-ERROR.
           PERFORM READ-BY-NAME THRU READ-BY-NAME-END
           MOVE 1 TO INPUT-ERROR.
           
       READ-BY-NAME.
           MOVE CALL-WORD TO MN-CALL MN-PROG-NAME LNK-TEXT
           READ MENU KEY MN-CALL
                INVALID CONTINUE
                NOT INVALID GO READ-BY-NAME-1.
           READ MENU KEY MN-PROG-NAME
                INVALID CONTINUE
                NOT INVALID GO READ-BY-NAME-1.
           CALL "2-MENU" USING LINK-V LINK-RECORD
           IF LINK-PROG-NAME > SPACES
              MOVE LINK-PASSWORD TO LNK-TEXT
              CALL "0-PASSW" USING LINK-V
              IF LNK-VAL NOT = 0
                 INITIALIZE LINK-RECORD
              END-IF
           END-IF
           IF LINK-PROG-NAME > SPACES
              MOVE LINK-RECORD TO MN-RECORD MENU-RECORD
              MOVE 0 TO LNK-PERSON
              MOVE MN-PROG-NAME TO X-CALLWORD
              IF X-CALLWORD > SPACES
                 CALL X-CALLWORD USING LINK-V
                 EXCEPTION   
                   MOVE 1 TO INPUT-ERROR
                   MOVE "AA" TO LNK-AREA
                   MOVE 8 TO LNK-NUM
                   MOVE 0 TO LNK-POSITION
                   PERFORM DISPLAY-MESSAGE
                 END-CALL
                 CANCEL X-CALLWORD
                 PERFORM GET-INDEX-LEVEL
                 MOVE MN-LEV(IDX-LEVEL) TO CHOIX
              END-IF
           END-IF
           CANCEL "2-MENU"
           PERFORM AFFICHAGE-ECRAN.
           PERFORM NEXT-MENU VARYING IDX FROM 1 BY 1 UNTIL IDX  > 15.
           GO READ-BY-NAME-END.
       READ-BY-NAME-1.
           IF LNK-COMPETENCE < MN-COMPETENCE 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              MOVE 0 TO LNK-POSITION
              PERFORM DISPLAY-MESSAGE
              GO READ-BY-NAME-END.
           MOVE MN-PASSWORD TO LNK-TEXT.
           CALL "0-PASSW" USING LINK-V.
           IF LNK-VAL NOT = 0
              GO READ-BY-NAME-END.
           IF INPUT-ERROR = 0
              MOVE 0 TO LNK-STATUS
              MOVE 0 TO LNK-NUM
              MOVE MN-PROG-NAME TO X-CALLWORD
              IF X-CALLWORD NUMERIC
                 MOVE X-CALLWORD TO LNK-VAL
                 MOVE "0-BOOK" TO X-CALLWORD
              END-IF
              IF LNK-LANGUAGE NOT = "F"
                 CALL "0-MNTXT" USING LNK-LANGUAGE MN-RECORD
              END-IF
              MOVE MN-RECORD TO MENU-RECORD
           END-IF.
           MOVE 0 TO LNK-PERSON LNK-POSITION.
           CALL X-CALLWORD USING LINK-V
           EXCEPTION 
               MOVE "AA" TO LNK-AREA
               MOVE 8 TO LNK-NUM
               MOVE 0 TO LNK-POSITION
               PERFORM DISPLAY-MESSAGE
               GO READ-BY-NAME-END
           END-CALL.
           CANCEL X-CALLWORD.
           PERFORM GET-INDEX-LEVEL.
           MOVE MN-LEV(IDX-LEVEL) TO CHOIX.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM NEXT-MENU VARYING IDX FROM 1 BY 1 UNTIL IDX > 15.
       READ-BY-NAME-END.
           EXIT.

       GET-INDEX-LEVEL.
           MOVE 0 TO IDX-LEVEL.
           PERFORM COUNT-STEPS VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           
       COUNT-STEPS.
           IF MN-LEV(IDX) > 0 ADD 1 TO IDX-LEVEL.
           
       NEXT-MENU.
           PERFORM CLEAR-F-LEVEL VARYING IDX-1 FROM IDX-LEVEL BY 1 
           UNTIL IDX-1 > 5.
           MOVE IDX TO MN-LEV(IDX-LEVEL).
           READ MENU INVALID     
                INITIALIZE MN-BODY
           END-READ.
           IF LNK-LANGUAGE NOT = "F"
              CALL "0-MNTXT" USING LNK-LANGUAGE MN-RECORD.
           MOVE MN-RECORD TO HELP-MENU(IDX).
           IF HELP-DESCRIPTION(IDX) = SPACES 
              MOVE 0 TO HELP-TEST(IDX)
           ELSE
              MOVE 1 TO HELP-TEST(IDX). 
           PERFORM DISPLAY-ECRAN.

       DISPLAY-ECRAN.
           COMPUTE LIN-IDX = 6 + IDX.
           IF LNK-COMPETENCE < HELP-COMPETENCE (IDX)
              DISPLAY HELP-DESCRIPTION(IDX) 
              LINE LIN-IDX POSITION 16 LOW
           ELSE
              DISPLAY HELP-DESCRIPTION(IDX) 
              LINE LIN-IDX POSITION 16 HIGH
           END-IF.
           MOVE 0 TO IDX-2.
           INSPECT HELP-DESCRIPTION(IDX) TALLYING IDX-2 FOR CHARACTERS 
           BEFORE "  ".
           ADD 1 TO IDX-2.
           IF IDX = CHOIX
              IF LNK-COMPETENCE < HELP-COMPETENCE(IDX)
                 DISPLAY HELP-DESCRIPTION(IDX) 
                 LINE LIN-IDX POSITION 16 LOW REVERSE SIZE IDX-2
              ELSE
                 DISPLAY HELP-DESCRIPTION(IDX) 
                 LINE LIN-IDX POSITION 16 HIGH REVERSE SIZE IDX-2
              END-IF
           END-IF.
           DISPLAY HELP-CALL(IDX) LINE LIN-IDX POSITION 65.

       CLEAR-F-LEVEL.
           MOVE 0 TO MN-LEV(IDX-1).
               
       CALL-PROGRAM.
           MOVE 0 TO LNK-STATUS.
           MOVE 0 TO LNK-NUM.
           MOVE HELP-PROG-NAME(CHOIX) TO X-CALLWORD.
           IF X-CALLWORD NUMERIC
              MOVE X-CALLWORD TO LNK-VAL
              MOVE "0-BOOK" TO X-CALLWORD.
           MOVE HELP-MENU(CHOIX) TO MENU-RECORD.
           MOVE 0 TO LNK-PERSON.
           CALL X-CALLWORD USING LINK-V
                EXCEPTION 
                   MOVE 1 TO INPUT-ERROR
                   MOVE "AA" TO LNK-AREA
                   MOVE 8 TO LNK-NUM
                   MOVE 0 TO LNK-POSITION
                   PERFORM DISPLAY-MESSAGE
           END-CALL.
           IF LNK-LANGUAGE = SPACES
              MOVE FR-LANGUE TO LNK-LANGUAGE
           END-IF.
           IF LNK-LANGUAGE = SPACES
              MOVE "F" TO LNK-LANGUAGE
           END-IF.
           IF INPUT-ERROR = 0
              CANCEL X-CALLWORD
              PERFORM AFFICHAGE-ECRAN.
           
       AFFICHAGE-ECRAN.
           MOVE 1 TO LNK-VAL.
           MOVE "   " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY LNK-LANGUAGE LINE 1 POSITION 2.

       END-PROGRAM.
           CLOSE MENU.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".
           
        CHANGE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 SUBTRACT 1 FROM LNK-MOIS
             WHEN 58 ADD      1 TO   LNK-MOIS
             WHEN 65 SUBTRACT 1 FROM LNK-MOIS
             WHEN 66 ADD      1 TO   LNK-MOIS
           END-EVALUATE.
           IF LNK-MOIS < 1
           OR LNK-MOIS > 12
              PERFORM CHANGE-ANNEE.
           PERFORM STOPS.

        CHANGE-ANNEE.
           IF LNK-MOIS < 1
             SUBTRACT 1 FROM LNK-ANNEE
             MOVE 12 TO LNK-MOIS
           ELSE
             ADD  1 TO LNK-ANNEE
             MOVE 1 TO LNK-MOIS
           END-IF.
           MOVE 90 TO LNK-NUM
           MOVE "SL" TO LNK-AREA
           MOVE 0 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
           PERFORM AA.
           IF LNK-ANNEE < LNK-LIMITE
              MOVE LNK-LIMITE TO LNK-ANNEE
           END-IF.
           IF LNK-ANNEE > LNK-TOP
              MOVE LNK-TOP TO LNK-ANNEE
           END-IF.
           PERFORM JOUR-SEMAINE.

       STOPS.
           MOVE "STOP" TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           IF  PARMOD-PROG-NUMBER-1 > 0
           AND PARMOD-PROG-NUMBER-1 < LNK-ANNEE
              MOVE PARMOD-PROG-NUMBER-1 TO LNK-ANNEE 
           END-IF.
           IF  PARMOD-PROG-NUMBER-1 = LNK-ANNEE
           AND PARMOD-PROG-NUMBER-2 < LNK-MOIS
              MOVE PARMOD-PROG-NUMBER-2 TO LNK-MOIS 
           END-IF.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           DISPLAY LNK-ANNEE LINE 1 POSITION 77.
