      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-LINDEX RECHERCHE INDEX RECENT             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-LINDEX.

       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.

           COPY "INDEX.REC".
       01  IDX      PIC 99.
       01  ACTION   PIC X.
       01  NUL-KEY               PIC 9(4) COMP-1 VALUE 0.
       01  ESC-KEY               PIC 9(4) COMP-1 VALUE 0.
 
       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-LINDEX.
       
           INITIALIZE INDEX-RECORD INDEXES.
           COMPUTE  INDEX-ANNEE = LNK-ANNEE + 1.
           PERFORM GET-INDEX.

       GET-INDEX. 
           CALL "6-INDEX" USING LINK-V INDEX-RECORD NUL-KEY.
           IF INDEX-ANNEE = LNK-ANNEE 
              PERFORM LOAD-INDEX VARYING IDX FROM INDEX-MOIS
              BY 1 UNTIL IDX > 12
           ELSE
              PERFORM LOAD-INDEX VARYING IDX FROM 1 
              BY 1 UNTIL IDX > 12
           END-IF.
           IF MOIS-IDX(1) = 0
              SUBTRACT 1 FROM INDEX-MOIS
              GO GET-INDEX. 
           CANCEL "6-INDEX".
           EXIT PROGRAM.

       LOAD-INDEX. 
           IF MOIS-IDX(IDX) = 0
              MOVE INDEX-FACT TO MOIS-IDX(IDX) 
              MOVE SALMIN-100 TO MOIS-SALMIN(IDX) 
           END-IF.

           COPY "XACTION.CPY".
      
