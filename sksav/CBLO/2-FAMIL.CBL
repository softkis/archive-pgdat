      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-FAMIL RECHERCHE FAMILLE                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-FAMIL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "FAMILLE.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.

       01  CHOIX                 PIC 9(6).
       01  DATUM                 PIC 9(8).
       01  COMPTEUR              PIC 99.
       01  ARROW                 PIC X VALUE ">".

       01 HE-FAMILLE.
          02 H-C OCCURS 20.
             03 H-A PIC 9(16).

       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ.
           02 HE-Z3 PIC ZZZ.
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "FAMILLE.LNK".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD PR-RECORD REG-RECORD.

       START-DISPLAY SECTION.
             
       START-2-FAMIL.

           CALL "0-TODAY" USING TODAY.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE FAM-RECORD IDX-1 EXC-KEY COMPTEUR CHOIX HE-FAMILLE.
           MOVE 4 TO LIN-IDX.
           PERFORM READ-FAM THRU READ-FAM-END.
           IF EXC-KEY = 66 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-FAM.
           CALL "6-FAMIL" USING LINK-V REG-RECORD FAM-RECORD EXC-KEY.
           IF FAM-FIRME = 0
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-FAMILLE IDX-1
                 GO READ-FAM
              END-IF
              GO READ-FAM-END.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-FAMILLE IDX-1
                 GO READ-FAM
              END-IF
              IF CHOIX NOT = 0
                 GO READ-FAM-END
              END-IF
           END-IF.
           MOVE 66 TO EXC-KEY.
           GO READ-FAM.
       READ-FAM-END.
           IF CHOIX = 0
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82 AND NOT = 68
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX COMPTEUR IDX-1.
           MOVE FAM-KEY TO H-A(IDX-1).
           MOVE FAM-SUITE TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 2.
           DISPLAY FAM-NOM LINE LIN-IDX POSITION 5 SIZE 20. 
           DISPLAY FAM-PRENOM LINE LIN-IDX POSITION 27 SIZE 15. 
           MOVE FAM-NAISS-A     TO HE-AA.
           MOVE FAM-NAISS-M     TO HE-MM.
           MOVE FAM-NAISS-J     TO HE-JJ.
           DISPLAY HE-DATE LINE LIN-IDX POSITION 45. 
           MOVE FAM-RELATION TO LNK-NUM HE-Z2.
           MOVE "F" TO LNK-AREA.
           COMPUTE LNK-POSITION = 00601000.
           MOVE LIN-IDX TO LNK-LINE.
           CALL "0-DMESS" USING LINK-V.
           IF FAM-CODE-SEXE = 1 
              DISPLAY "M" LINE LIN-IDX POSITION 57
           ELSE
              DISPLAY "F" LINE LIN-IDX POSITION 57. 
           COMPUTE IDX-3 = TODAY-ANNEE - FAM-NAISS-A.
           IF FAM-NAISS-M > TODAY-MOIS
              SUBTRACT 1 FROM IDX-3
              COMPUTE IDX-2 = 12 + TODAY-MOIS - FAM-NAISS-M 
           ELSE 
              COMPUTE IDX-2 = TODAY-MOIS - FAM-NAISS-M  
           END-IF.
           MOVE IDX-3 TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 75.
           MOVE IDX-2 TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 78 LOW.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0000530000 TO EXC-KFR (11).
           MOVE 0000000065 TO EXC-KFR (13).
           MOVE 6667000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE CHOIX.
           ACCEPT CHOIX 
              LINE 3 POSITION 2 SIZE 1
              TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 66 
              MOVE 13 TO EXC-KEY
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 13
              INITIALIZE HE-FAMILLE
              GO INTERRUPT-END.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF CHOIX NOT = 0
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IDX-1.



       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IDX-1 + 4.
           MOVE ">" TO ARROW.
           ACCEPT ARROW
             LINE  LIN-IDX POSITION 1 SIZE 1
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 1.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN OTHER PERFORM END-PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-A(IDX-1) = 0
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.
                        
       AFFICHAGE-ECRAN.
           MOVE 2233 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           DISPLAY SPACES LINE 4 POSITION 1 SIZE 80.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 10.
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 20 SIZE 50.

       END-PROGRAM.
           INITIALIZE FAM-RECORD.
           IF IDX-1 > 0
              MOVE H-A(IDX-1) TO FAM-KEY
           END-IF.
           CALL "6-FAMIL" USING LINK-V REG-RECORD FAM-RECORD FAKE-KEY.
           IF FAM-KEY NOT = 0
              MOVE FAM-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
