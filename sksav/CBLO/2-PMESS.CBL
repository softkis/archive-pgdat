      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-PMESS AFFICHAGE PERSONNES PAR CODE MESSAGE�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-PMESS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.


       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "MESSAGE.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  CHOIX                 PIC X(10) VALUE SPACES.
       01  COMPTEUR              PIC 99.
       01  ACTION-CALL           PIC 9 VALUE 0.
       
       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-PMESS.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 .

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 .

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT MS-NUMBER
             LINE  3 POSITION 22 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           
      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE MENU-EXTENSION-1 TO LNK-AREA.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-MESS" USING LINK-V MS-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-MESSAGES
           END-EVALUATE.
           PERFORM AFFICHAGE-DETAIL.
           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.
           IF EXC-KEY = 13 MOVE 52 TO EXC-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       DIS-HE-01.
           MOVE MS-NUMBER TO HE-Z4 LNK-NUM.
           DISPLAY HE-Z4 LINE 3 POSITION 22.
           MOVE MENU-EXTENSION-1 TO LNK-AREA.
           COMPUTE LNK-POSITION = 03324000.
           CALL "0-DMESS" USING LINK-V.


       DIS-HE-END.
           EXIT.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE COMPTEUR CHOIX.
           MOVE 4 TO LIN-IDX.
           INITIALIZE REG-RECORD.
           PERFORM READ-MS THRU READ-MS-END.
       AFFICHE-END.
           EXIT.

       READ-MS.
           MOVE 66 TO EXC-KEY.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
           ELSE
      *       IF LIN-IDX > 4
      *          PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
      *          PERFORM INTERRUPT THRU INTERRUPT-END
      *       END-IF
              GO READ-MS-END.
           IF PRES-TOT(LNK-MOIS) = 0
              GO READ-MS.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF CAR-IN-ACTIF = 1
              GO READ-MS.
           EVALUATE MENU-EXTENSION-1
                WHEN "CG" IF CAR-CONGE NOT = MS-NUMBER
                             GO READ-MS
                          END-IF
                WHEN "ED" IF CAR-EDUCATION NOT = MS-NUMBER
                             GO READ-MS
                          END-IF
           END-EVALUATE.

           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              MOVE 4 TO LIN-IDX
           END-IF.
           IF EXC-KEY = 52 GO READ-MS-END.
           GO READ-MS.
       READ-MS-END.
           PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX.
           ADD 1 TO COMPTEUR.
           MOVE REG-PERSON TO HE-Z6.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.
           DISPLAY HE-Z6 LINE LIN-IDX POSITION 2 LOW.
           DISPLAY PR-NOM LINE LIN-IDX POSITION 10.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 12 TO COL-IDX.
           DISPLAY PR-PRENOM LINE LIN-IDX POSITION COL-IDX LOW.
           DISPLAY CAR-METIER LINE LIN-IDX POSITION 70.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052000000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.

           ACCEPT CHOIX 
           LINE 23 POSITION 30 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 66 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.

       INTERRUPT-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 2204 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CANCEL "2-MS".
           CANCEL "6-MS".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

