      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-E106  FORMULAIRE E106                     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-E106.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01   CHOIX-MAX         PIC 99 VALUE 4.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "CONTRAT.REC".
           COPY "METIER.REC".
           COPY "PAYS.REC".
           COPY "E106.LNK".
           COPY "PARMOD.REC".

       01  SIGNATAIRE            PIC X(30).

       01 HE-SEL.
          02 HE-DETAIL OCCURS 10.
              03 H-S         PIC X.
              03 H-NOM       PIC X(20).
              03 H-PRENOM    PIC X(20).
              03 H-DATE      PIC X(10).

       01  HE-ZP.
           02  KLAMOP              PIC X VALUE "(".
           02  HE-ZN PIC Z(8).
           02  KLAMZOU             PIC X VALUE ")".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2). 
           02 HE-Z3 PIC Z(3). 
           02 HE-Z4 PIC Z(4). 
           02 HE-Z6 PIC Z(6).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HP-DATE .
              03 HE-J PIC ZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HE-M PIC ZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HE-A PIC ZZZZ.
           02 HE-SNOCS.
              03 HS-A PIC ZZZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HS-M PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HS-J PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HS-S PIC 999.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-E106.


           CALL "0-TODAY" USING TODAY.
           INITIALIZE E106-RECORD PARMOD-RECORD.
           MOVE 1 TO LNK-PRESENCE.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-SETTINGS TO SIGNATAIRE.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0027000000 TO EXC-KFR(6).
           MOVE 0052530000 TO EXC-KFR(11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3     MOVE 0063000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN CHOIX-MAX
                      MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.
 
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN CHOIX-MAX PERFORM AVANT-DEC.


           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN CHOIX-MAX PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT REG-PERSON 
             LINE 4 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 4 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-3.
           ACCEPT SIGNATAIRE
             LINE 18 POSITION 30 SIZE 30
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE SIGNATAIRE TO PARMOD-SETTINGS.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.
           IF REG-PERSON > 0 
           AND PRES-ANNEE = 0
               MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.
           MOVE 0 TO IDX-1.
           MOVE 13 TO SAVE-KEY.

       APRES-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN  13 CONTINUE
           WHEN  OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           PERFORM AFFICHAGE-DETAIL.

           
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE "N" TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY
            WHEN  5 PERFORM TRANSMET
                    MOVE 1 TO DECISION 
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       
       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD A-N FAKE-KEY.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD PV-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 4 POSITION 15 SIZE 6
           DISPLAY PR-NOM LINE 4 POSITION 47 SIZE 33.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 48 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 4 POSITION COL-IDX SIZE IDX LOW.
        
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 4 POSITION 33.

       DIS-HE-END.

       TRANSMET.
           INITIALIZE E106-RECORD.
           MOVE PR-NOM      TO E106-PERS-NOM.
           MOVE PR-PRENOM   TO E106-PERS-PRENOM.
           MOVE REG-PERSON TO HE-ZN.
           MOVE 51 TO IDX.
           STRING HE-ZP DELIMITED BY SIZE INTO 
           E106-PERS-NOM POINTER IDX ON OVERFLOW CONTINUE END-STRING.
           MOVE PR-PAYS TO PAYS-CODE.

           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.

           MOVE PR-NAISS-A  TO HS-A HE-AA.
           MOVE PR-NAISS-M  TO HS-M HE-MM.
           MOVE PR-NAISS-J  TO HS-J HE-JJ.
           MOVE PR-SNOCS    TO HS-S.
           MOVE HE-SNOCS    TO E106-PERS-MAT.
           MOVE HE-DATE TO E106-PERS-DATENAIS.


           MOVE 1 TO IDX.
           STRING PR-MAISON DELIMITED BY " " INTO E106-PERS-ADRESSE(1)
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-RUE DELIMITED BY "   "  INTO E106-PERS-ADRESSE(1)
           WITH POINTER IDX.
               
           MOVE 1 TO IDX.
           STRING PR-PAYS DELIMITED BY " "  INTO E106-PERS-ADRESSE(2)
           WITH POINTER IDX.
           ADD 1 TO IDX.
           IF PR-PAYS = "D" OR "F" OR "CZ" OR "I"
              STRING PR-CP5 DELIMITED BY " "  INTO E106-PERS-ADRESSE(2)
              WITH POINTER IDX
           ELSE
              STRING PR-CP4 DELIMITED BY " "  INTO E106-PERS-ADRESSE(2)
              WITH POINTER IDX
           END-IF.
           ADD 1 TO IDX.
           STRING PR-LOCALITE DELIMITED BY SIZE INTO 
           E106-PERS-ADRESSE(2) WITH POINTER IDX.

           MOVE CON-DEBUT-A TO HE-A.
           MOVE CON-DEBUT-M TO HE-M.
           MOVE CON-DEBUT-J TO HE-J.
           MOVE HP-DATE     TO E106-CON-DEPUIS.

           MOVE TODAY-ANNEE TO HE-AA.
           MOVE TODAY-MOIS  TO HE-MM.
           MOVE TODAY-JOUR  TO HE-JJ.
           MOVE HE-DATE TO E106-DATE.
           MOVE FR-LOCALITE TO E106-LIEU.

           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           MOVE CAR-METIER TO MET-CODE.
           MOVE "F "       TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO E106-PERS-METIER .
           
           MOVE PAYS-NOM TO E106-PERS-ADRESSE(3) E106-PERS-PAYS.
           MOVE FR-NOM   TO E106-FIRME-ADRESSE(1).

           MOVE 1 TO IDX.
           STRING FR-PAYS DELIMITED BY " "  INTO E106-FIRME-ADRESSE(2)
           WITH POINTER IDX.
           ADD 1 TO IDX.
           MOVE FR-CODE-POST TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE INTO E106-FIRME-ADRESSE(2)
           WITH POINTER IDX.
           ADD 2 TO IDX.
           STRING FR-LOCALITE DELIMITED BY " " INTO 
           E106-FIRME-ADRESSE(2) WITH POINTER IDX.
           MOVE 1 TO IDX.
           STRING FR-RUE DELIMITED BY "   "  INTO E106-FIRME-ADRESSE(3)
              WITH POINTER IDX.
           ADD 1 TO IDX.
           IF IDX < 39
              STRING FR-MAISON DELIMITED BY "  " 
              INTO E106-FIRME-ADRESSE(3) WITH POINTER IDX.
           MOVE SIGNATAIRE TO E106-RESP-NOM.
           MOVE 0 TO IDX-4.
           MOVE 0 TO LNK-VAL.
           CALL "E-106" USING LINK-V E106-RECORD.
           MOVE 99 TO LNK-VAL.
           CALL "E-106" USING LINK-V E106-RECORD.
           MOVE 0 TO LNK-VAL.
           CANCEL "E-106".
           
       
       AFFICHAGE-ECRAN.
           MOVE 1225 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".
       

      *    컴컴컴컴컴컴컴컴컴컴컴컴

           
