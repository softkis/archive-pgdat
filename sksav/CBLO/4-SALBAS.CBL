      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-SALBAS BASES DE REMUNERATION              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-SALBAS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "POINTS.REC".
           COPY "BAREME.REC".
           COPY "CODSAL.REC".
           COPY "CODFIX.REC".
           COPY "CODPAIE.REC".
           COPY "REGISTRE.REC".
           COPY "JOURS.REC".
           COPY "ARRONDI.CPY".

       01  JOUR                 PIC 99.
       01  FACTEUR              PIC 999V99 VALUE 173.
       01  SAL-ARR1             PIC 9(7)V9.
       01  SAL-ARR2             PIC 9(7)V99.
       01  SAL-ARR3             PIC 9(7)V999.

       01  SAL-ACT              PIC 9(7)V9(5).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02 SAL-ACT-A         PIC 9(10).
           02 SAL-ACT-B         PIC 9(2).
       01  SAL-ACT-R1 REDEFINES SAL-ACT.
           02 SAL-ACT-A1        PIC 9(11).
           02 SAL-ACT-B1        PIC 9(1).
       01  SAL-ACT-R2 REDEFINES SAL-ACT.
           02 SAL-ACT-A2        PIC 9(7)V99.
           02 SAL-ACT-B2        PIC 9(3).
       01  TOTAL-HEURES.
           03 TOT-HRS-IDX       PIC 999V99 COMP-3 OCCURS 32.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4Z4 PIC Z(4),ZZZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "CCOL.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "HORSEM.REC".
           COPY "POSE.REC".
           COPY "V-BASES.REC".

       PROCEDURE DIVISION USING LINK-V
                                CCOL-RECORD
                                CAR-RECORD
                                PRESENCES 
                                POSE-RECORD 
                                HJS-RECORD 
                                BASES-REMUNERATION.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-SALBAS.
           
       BASES-SALAIRE.
           INITIALIZE BASES-REMUNERATION SAL-ACT TOTAL-HEURES
           PTS-RECORD JRS-RECORD.
           IF CAR-ECHELON < ,00001
              INITIALIZE CAR-BAREME.
           CALL "6-POINT" USING LINK-V PTS-RECORD.
           
           MOVE FR-KEY TO REG-FIRME.
           MOVE CAR-PERSON TO REG-PERSON.

           PERFORM HEURES-THEORIQUES VARYING
           JOUR FROM 1 BY 1 UNTIL JOUR > MOIS-JRS(LNK-MOIS).
           MOVE 99 TO JRS-OCCUPATION.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD FAKE-KEY.
           IF JRS-HRS(32) > 0
              MOVE JRS-HRS(32) TO TOT-HRS-IDX(32)
           END-IF.

           IF CAR-POURCENT = 0
              MOVE 100 TO CAR-POURCENT.

           IF CAR-COM-100 > 0
             COMPUTE SAL-ACT = CAR-COM-100 * MOIS-IDX(LNK-MOIS) + ,0001
             MOVE SAL-ACT TO BAS-COMPLEMENT
           END-IF.
           IF CAR-SAL-100 < ,00001
              MOVE 0 TO CAR-SAL-100 
           END-IF.
           IF CAR-BAREME NOT = SPACES
              INITIALIZE BAR-RECORD 
              MOVE CAR-BAREME TO BAR-BAREME
              MOVE CAR-GRADE  TO BAR-GRADE
              MOVE LNK-ANNEE  TO BAR-ANNEE
              MOVE LNK-MOIS   TO BAR-MOIS
              CALL "6-BAREME" USING LINK-V BAR-RECORD NUL-KEY
              COMPUTE SAL-ACT = (BAR-ECHELON-I100(CAR-ECHELON)
                                * MOIS-IDX(LNK-MOIS))
                                * CAR-POURCENT / 100 + ,00005
              MOVE SAL-ACT TO BAS-BAREME
           END-IF.
           IF CAR-SAL-100 = 0
              IF CAR-BAREME = SPACES
                 COMPUTE SAL-ACT = MOIS-SALMIN(LNK-MOIS) * 
                 MOIS-IDX(LNK-MOIS) 
                 + ,005
                 IF CAR-HOR-MEN = 0
                    COMPUTE SAL-ACT = SAL-ACT / 173 + ,00005
                    MOVE 0 TO SAL-ACT-B1
                 ELSE 
                    MOVE 0 TO SAL-ACT-B2
                 END-IF
              ELSE
                 MOVE BAR-POINTS(CAR-ECHELON) TO BAS-POINTS
                 IF BAR-POINT > 0
                    PERFORM 10-POINTS VARYING IDX FROM 1 BY 1 UNTIL IDX 
                    > 10
                    COMPUTE SAL-ACT = PTS-I100(BAR-POINT)
                              * MOIS-IDX(LNK-MOIS) 
                    COMPUTE SAL-ACT = SAL-ACT * BAR-POINTS(CAR-ECHELON) 
                 ELSE
                    COMPUTE SAL-ACT = (BAR-ECHELON-I100(80)
                                    * MOIS-IDX(LNK-MOIS) + ,00009)
                                    * CAR-POURCENT / 100 + ,00005
                    MOVE 0 TO SAL-ACT-B2
                    MOVE SAL-ACT TO BAS-BQ-00
                    COMPUTE SAL-ACT = (BAR-ECHELON-I100(CAR-ECHELON)
                                    * MOIS-IDX(LNK-MOIS) + ,00009)
                                    * CAR-POURCENT / 100 + ,00005
                    MOVE 0 TO SAL-ACT-B2
                    MOVE SAL-ACT TO BAS-BQ-ECH
                    COMPUTE SAL-ACT = ((BAR-ECHELON-I100(CAR-ECHELON)
                                    + BAR-ECHELON-I100(80))
                                    * MOIS-IDX(LNK-MOIS)  + ,00009)
                 END-IF
              END-IF
              COMPUTE SAL-ACT = SAL-ACT * CAR-POURCENT / 100
           ELSE
              COMPUTE SAL-ACT = CAR-SAL-100 * MOIS-IDX(LNK-MOIS) 
                              * CAR-POURCENT / 100 + ,0001
           END-IF.
           IF CAR-INDEXE = "N"
              MOVE CAR-SAL-ACT TO SAL-ACT
           END-IF.
           IF CAR-HOR-MEN = 0
              MOVE 0 TO SAL-ACT-B1
           ELSE
              COMPUTE SAL-ACT = SAL-ACT + ,005
              MOVE 0 TO SAL-ACT-B2
           END-IF.

           MOVE CAR-HOR-MEN TO IDX.
           IF SAL-ACT > 200
              MOVE 1 TO IDX.
           MOVE SAL-ACT TO BAS-HORAIRE BAS-PRORATA.
           IF IDX = 0 
              COMPUTE BAS-MOIS = SAL-ACT * CAR-HRS-MOIS + ,0001
           ELSE 
              MOVE SAL-ACT TO BAS-MOIS
              COMPUTE BAS-HORAIRE = SAL-ACT / CAR-HRS-MOIS 
              COMPUTE BAS-PRORATA = SAL-ACT / TOT-HRS-IDX(32)
           END-IF.

           COMPUTE BAS-JRS-CAL = BAS-MOIS / MOIS-JRS(LNK-MOIS).
           COMPUTE SAL-ARR2 = BAS-JRS-CAL * PRES-TOT(LNK-MOIS) + ,005.
           MOVE SAL-ARR2 TO BAS-PRO-CAL.
           MOVE BAS-MOIS TO BAS-HRS-SUPP BAS-GRAT.
           IF  CAR-SAL-100 = 0
           AND CAR-BAREME = SPACES
               COMPUTE SAL-ACT = (MOIS-SALMIN(LNK-MOIS) * 
               MOIS-IDX(LNK-MOIS)  + ,00005) 
               COMPUTE SAL-ACT = SAL-ACT / 100 * CAR-POURCENT
               MOVE SAL-ACT TO BAS-HRS-SUPP 
               PERFORM CODFIX
               MOVE 173 TO FACTEUR 
               IF  CAR-HOR-MEN = 1
               AND HJS-HEURES(8) < 40
                  COMPUTE FACTEUR = 173 * HJS-HEURES(8) / 40
               END-IF
               COMPUTE BAS-HRS-SUPP = BAS-HRS-SUPP / FACTEUR + ,00005
               IF IDX = 0 
                  COMPUTE SAL-ACT = SAL-ACT / 173 + ,00005
                  MOVE 0 TO SAL-ACT-B1
                  MOVE SAL-ACT TO BAS-HORAIRE 
               ELSE
                  COMPUTE BAS-HORAIRE = SAL-ACT / CAR-HRS-MOIS + ,00005
               END-IF
               MOVE 0 TO SAL-ACT-B1
           ELSE
              PERFORM CODFIX
              COMPUTE BAS-HRS-SUPP = BAS-HRS-SUPP / CAR-HRS-MOIS
           END-IF.

           IF  CAR-INDEXE  = "N"
           AND CAR-SAL-100 = 0
           AND CAR-SAL-ACT = 0
           AND CAR-BAREME  = SPACES
              INITIALIZE BAS-HORAIRE BAS-PRORATA BAS-MOIS
           END-IF.
           COMPUTE SAL-ACT = MOIS-SALMIN(LNK-MOIS) * 
                 MOIS-IDX(LNK-MOIS) * 5 + ,005.
           COMPUTE BAS-PLAF-MAL-M = SAL-ACT / TOT-HRS-IDX(32).
           COMPUTE SAL-ACT = SAL-ACT / 173 + ,00005
           MOVE 0 TO SAL-ACT-B1.
           MOVE SAL-ACT TO BAS-PLAF-MAL-H.
           MOVE CAR-SAL-100 TO BAS-I100.

           IF CAR-POURCENT = 100
              MOVE BAS-MOIS TO BAS-100PCT
           ELSE 
              COMPUTE BAS-100PCT = BAS-MOIS * 100 / CAR-POURCENT
           END-IF.
                         
           IF MENU-PROG-NUMBER = 77
              PERFORM AFFICHAGE-VALEURS.
           IF FR-ANNEE-ARRONDI >= LNK-ANNEE
              EVALUATE FR-ARRONDI 
              WHEN 1
              PERFORM ARR-1 VARYING IDX FROM 1 BY 1 UNTIL IDX > 21
              WHEN 2
              PERFORM ARR-2 VARYING IDX FROM 1 BY 1 UNTIL IDX > 21
              WHEN 3
              PERFORM ARR-3 VARYING IDX FROM 1 BY 1 UNTIL IDX > 21
              END-EVALUATE
           END-IF.

           EXIT PROGRAM.

       HEURES-THEORIQUES.
           MOVE SEM-IDX(LNK-MOIS, JOUR) TO IDX.
           IF POSE-FIRME > 0
              IF POSE-JOUR(LNK-MOIS, JOUR) > 0
                 MOVE CCOL-DUREE-JOUR TO TOT-HRS-IDX(JOUR) 
              END-IF
           ELSE
              MOVE HJS-HEURES(IDX) TO TOT-HRS-IDX(JOUR)
           END-IF.
           ADD TOT-HRS-IDX(JOUR) TO TOT-HRS-IDX(32).

       AFFICHAGE-VALEURS.
           MOVE 0 TO IDX-2.
           PERFORM DISPLAY-VAL VARYING IDX  FROM 0 BY 1 UNTIL 
                    IDX > 1.
           accept action no beep.

       DISPLAY-VAL.
           COMPUTE COL-IDX = 1 + IDX * 14.
           PERFORM DISPLAY-DET-VAL VARYING IDX-1 FROM 1 BY 1 UNTIL 
                    IDX-1 > 20.

       DISPLAY-DET-VAL.
           ADD 1 TO IDX-2.
           COMPUTE LIN-IDX = IDX-1.
           MOVE IDX-2 TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION COL-IDX LOW.
           MOVE BAS-SAL(IDX-2)  TO HE-Z4Z4.
           COMPUTE IDX-4 = COL-IDX + 2.
           DISPLAY HE-Z4Z4 LINE LIN-IDX POSITION IDX-4 HIGH.

       CODFIX.
           INITIALIZE CSF-RECORD.
           MOVE FR-KEY TO CSF-FIRME.
           MOVE CAR-PERSON TO LNK-PERSON.
           PERFORM READ-FIX THRU READ-FIX-END.
270103     IF CCOL-BASE-HRS-SUPP > 1
              COMPUTE BAS-HRS-SUPP = BAS-HRS-SUPP * CCOL-BASE-HRS-SUPP
           END-IF.

       READ-FIX.
           CALL "6-CODFIX" USING LINK-V CSF-RECORD NX-KEY.
           IF FR-KEY NOT = CSF-FIRME
           OR CAR-PERSON NOT = CSF-PERSON
              GO READ-FIX-END
           END-IF.
           IF CSF-CUMULS = "NNNNNNNN"  
              GO READ-FIX
           END-IF.
           MOVE CSF-CODE TO CS-NUMBER.
           CALL "6-CODSAL" USING LINK-V CS-RECORD FAKE-KEY.
           IF CS-PERMIS(CAR-STATUT) = 0
           OR CS-PERIODE(LNK-MOIS)  = 0
              GO READ-FIX
           END-IF.
           IF CAR-HOR-MEN = 1
              IF CS-PERMIS(10) = 1 
                 GO READ-FIX
              END-IF
           ELSE
              IF CS-PERMIS(10) = 2 
                 GO READ-FIX
              END-IF
           END-IF.
           IF CS-MALADIE = 2
              GO READ-FIX
           END-IF.
           INITIALIZE CSP-RECORD.
           PERFORM PREDEFINIE VARYING IDX-3 FROM 1 BY 1 UNTIL IDX-3 > 8.
           PERFORM RAJOUTE VARYING IDX-3 FROM 1 BY 1 UNTIL IDX-3 > 6.
           GO READ-FIX.
       READ-FIX-END.
           EXIT.

       PREDEFINIE.
           IF CS-VAL-PREDEFINIE(IDX-3) > 0
              MOVE CS-VAL-PREDEFINIE(IDX-3) TO CSP-DONNEE(IDX-3).
           IF CSF-DONNEE(IDX-3) > 0
              MOVE CSF-DONNEE(IDX-3) TO CSP-DONNEE(IDX-3).
           IF CS-VAL-MAX(IDX-3) NOT = 0
              IF CSP-DONNEE(IDX-3) > CS-VAL-MAX(IDX-3) 
                 MOVE CS-VAL-MAX(IDX-3) TO CSP-DONNEE(IDX-3)
              END-IF
           END-IF.
           IF CSP-DONNEE(IDX-3) < CS-VAL-MIN(IDX-3) 
              MOVE CS-VAL-MIN(IDX-3) TO CSP-DONNEE(IDX-3)
           END-IF.
           IF CS-PROCEDURE(IDX-3) NOT = 0
              IF  CS-PROCEDURE(IDX-3) > 500
              AND CS-PROCEDURE(IDX-3) < 511
                 COMPUTE IDX-4 = CS-PROCEDURE(IDX-3) - 500
                 COMPUTE SAL-ACT = PTS-I100(IDX-4)
                 * MOIS-IDX(LNK-MOIS) + ,00001
                 MOVE SAL-ACT TO CSP-DONNEE(IDX-3)
              END-IF
              IF  CS-PROCEDURE(IDX-3) > 510
              AND CS-PROCEDURE(IDX-3) < 521
                 COMPUTE IDX-4 = CS-PROCEDURE(IDX-3) - 510
                 COMPUTE SAL-ACT = PTS-I100(IDX-4)
                 * MOIS-IDX(LNK-MOIS) + ,00001
                 MOVE SAL-ACT TO CSP-DONNEE(IDX-3)
              END-IF

              IF  CS-PROCEDURE(IDX-3) > 1000
              AND CS-PROCEDURE(IDX-3) < 1201
                 MOVE 1 TO CSP-DONNEE(IDX-3)
              END-IF
           END-IF.
           IF CS-NOTPOL(IDX-3) > SPACES
              MOVE IDX-3 TO LNK-NUM
              CALL "4-NOTPOL" USING LINK-V CS-RECORD CSP-RECORD.
           IF CS-FORMULE(IDX-3) NOT = 0
              MOVE IDX-3 TO LNK-NUM
              CALL "4-FORMUL" USING LINK-V CS-RECORD CSP-RECORD.

       RAJOUTE.
           IF CSF-CUMUL(IDX-3) NOT = "N"
              COMPUTE IDX-1 = IDX-3 + 20
              IF IDX-3 NOT = 3
                 ADD CSP-TOTAL TO BAS-SAL(IDX-1)
              ELSE
                 IF BAS-SAL(IDX-1) > 0
                    ADD CSP-TOTAL TO BAS-SAL(IDX-1)
                 END-IF
              END-IF
              IF IDX-3 = 1
                 COMPUTE SAL-ACT = CSP-TOTAL / 173
                 PERFORM 10-POINTS-SUP VARYING IDX-1 FROM 51 BY 1 UNTIL 
                 IDX-1 > 60
              END-IF
           END-IF.

       10-POINTS.
           COMPUTE SAL-ACT = PTS-I100(IDX) * MOIS-IDX(LNK-MOIS).
           MOVE BAR-POINTS(CAR-ECHELON) TO SH-00.
           IF SH-00 > 362
              MOVE 362 TO SH-00.
           COMPUTE SAL-ACT = SAL-ACT * SH-00.
           COMPUTE IDX-1 = IDX + 40.
           MOVE SAL-ACT TO BAS-SAL(IDX-1).
           COMPUTE SAL-ACT = SAL-ACT / 173
           COMPUTE IDX-1 = IDX + 50.
           MOVE SAL-ACT TO BAS-SAL(IDX-1).

       10-POINTS-SUP.
           IF BAS-SAL(IDX-1) > 0
              ADD SAL-ACT TO BAS-SAL(IDX-1).
    
           COPY "XACTION.CPY".

       ARR-1.
           IF BAS-ARR(IDX) = 1
              COMPUTE SAL-ARR1 = BAS-SAL(IDX) + ,05
              MOVE SAL-ARR1 TO BAS-SAL(IDX).
       ARR-2.
           IF BAS-ARR(IDX) = 1
              COMPUTE SAL-ARR2 = BAS-SAL(IDX) + ,005
              MOVE SAL-ARR2 TO BAS-SAL(IDX).
       ARR-3.
           IF BAS-ARR(IDX) = 1
              COMPUTE SAL-ARR3 = BAS-SAL(IDX) + ,0005
              MOVE SAL-ARR3 TO BAS-SAL(IDX).
