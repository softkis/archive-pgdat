      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-DELFIR EFFACEMENT INCONDITIONEL FIRME     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-DELFIR.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "FIRME.FC".
           COPY "JOURNAL.FC".
           COPY "REGISTRE.FC".
           COPY "BANQF.FC".
           COPY "BANQP.FC".
           COPY "CODPAIE.FC".
           COPY "CODFIX.FC".
           COPY "CCOL.FC".
           COPY "COUT.FC".
           COPY "LIVRE.FC".
           COPY "CARRIERE.FC".
           COPY "FICHE.FC".
           COPY "CONGE.FC".
           COPY "JOURS.FC".
           COPY "HEURES.FC".
           COPY "HORSEM.FC".
           COPY "VIREMENT.FC".
           COPY "DIVISION.FC".
           COPY "CONTRAT.FC".
           COPY "FAMILLE.FC".
           COPY "EQUIPE.FC".
           COPY "POCL.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FIRME.FDE".
           COPY "JOURNAL.FDE".
           COPY "REGISTRE.FDE".
           COPY "BANQF.FDE".
           COPY "BANQP.FDE".
           COPY "CODPAIE.FDE".
           COPY "CODFIX.FDE".
           COPY "CCOL.FDE".
           COPY "COUT.FDE".
           COPY "LIVRE.FDE".
           COPY "CARRIERE.FDE".
           COPY "FICHE.FDE".
           COPY "CONGE.FDE".
           COPY "JOURS.FDE".
           COPY "HEURES.FDE".
           COPY "HORSEM.FDE".
           COPY "VIREMENT.FDE".
           COPY "DIVISION.FDE".
           COPY "CONTRAT.FDE".
           COPY "FAMILLE.FDE".
           COPY "EQUIPE.FDE".
           COPY "POCL.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".

       01  CHOIX-MAX             PIC 99 VALUE 2.

       01  HRS-NAME.
           02 FILLER             PIC X(9) VALUE "S-HEURES.".
           02 ANNEE-HEURES        PIC 999.
       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.
       01  LIVRE-NAME.
           02 FILLER             PIC X(8) VALUE "S-LIVRE.".
           02 ANNEE-LIVRE        PIC 999.
       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIR          PIC 999.
       01  CONGE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CONGE.".
           02 ANNEE-CONGE        PIC 999 VALUE 0.
       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.
       01  JOURNAL-NAME.
           02 PRECISION          PIC X(4) VALUE "S-JO".
           02 FIRME-JOURNAL      PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 ANNEE-JOURNAL      PIC 999.

       01  HE-Z4 PIC Z(4) BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                REGISTRE 
                FIRME
                CCOL
                COUTS
                BANQF
                CARRIERE 
                CODFIX
                CODPAIE
                LIVRE
                FICHE  
                BANQP 
                CONGE
                JOURS
                JOURNAL
                HEURES
                HORSEM 
                VIREMENT
                CONTRAT
                DIVIS
                POCL
                EQUIPE
                FAMILLE.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-DELFIRM .

           MOVE LNK-MOIS TO SAVE-MOIS.
           MOVE LNK-SUFFIX TO ANNEE-PAIE ANNEE-HEURES ANNEE-LIVRE 
           ANNEE-VIR ANNEE-JOURS ANNEE-CONGE ANNEE-JOURNAL.
           OPEN I-O FIRME.

           PERFORM AFFICHAGE-ECRAN.
           INITIALIZE FIRME-RECORD.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF ECRAN-IDX = 1 AND INDICE-ZONE < 1
              MOVE 1 TO INDICE-ZONE.
        
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000130000 TO EXC-KFR(3)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-1.
           ACCEPT FIRME-KEY 
             LINE  3 POSITION 15 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       APRES-1.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3 
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   ELSE
                      MOVE "N" TO LNK-A-N
                   END-IF
                   CALL "2-FIRMES" USING LINK-V FIRME-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   CANCEL "2-FIRMES"
           WHEN OTHER  PERFORM NEXT-FIRME.
           PERFORM DIS-HE-01.
           IF FIRME-KEY = FR-KEY
           OR FIRME-KEY = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.

           
       APRES-DEC.
            EVALUATE EXC-KEY 
            WHEN  8 PERFORM EFFACE-FIRME
                    MOVE 1 TO DECISION 
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-FIRME.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 3
              MOVE "A" TO A-N.
           CALL "6-FIRMES" USING LINK-V FIRME-RECORD A-N EXC-KEY.

       EFFACE-FIRME.
           INITIALIZE
           CCOL-RECORD
           REG-RECORD
           BQF-RECORD
           BQP-RECORD
           CSP-RECORD
           CAR-RECORD
           CSF-RECORD
           CONGE-RECORD
           JRS-RECORD
           HRS-RECORD
           HJS-RECORD
           COUT-RECORD
           FICHE-RECORD
           L-RECORD
           EQ-RECORD
           DIV-RECORD
           PC-RECORD
           VIR-RECORD.
       
           PERFORM EFFACE-BANQUE-F.
           PERFORM EFFACE-BANQUE.
           PERFORM EFFACE-CSPAI.
           PERFORM EFFACE-CSFIX.
           PERFORM EFFACE-CONGE.
           PERFORM EFFACE-FICHE.
           PERFORM EFFACE-JOURS.
           PERFORM EFFACE-HEURES.
           PERFORM EFFACE-HORSEM.
           PERFORM EFFACE-LIVRE.
           PERFORM EFFACE-VIREMENT.
           PERFORM EFFACE-CARRIERE.
           PERFORM EFFACE-CONTRAT.
           PERFORM EFFACE-FAMILLE.
           PERFORM EFFACE-REGISTRE
           PERFORM EFFACE-CCOL.
           PERFORM EFFACE-COUT.
           PERFORM EFFACE-EQUIPE.
           PERFORM EFFACE-DIVIS.
           PERFORM EFFACE-POCL.

           MOVE FIRME-KEY TO FIRME-JOURNAL.
           MOVE "S-JO" TO PRECISION
           DELETE FILE JOURNAL.
           MOVE "S-JM" TO PRECISION
           DELETE FILE JOURNAL.
           IF LNK-SQL = "Y" 
              CALL "9-FIRME" USING LINK-V FR-RECORD DEL-KEY 
           END-IF.
           DELETE FIRME INVALID CONTINUE END-DELETE.

       EFFACE-BANQUE-F.
           OPEN I-O   BANQF.
           MOVE FIRME-KEY TO BQF-FIRME.
           START BANQF KEY > BQF-KEY INVALID CLOSE BANQF
                NOT INVALID
           PERFORM DELETE-BQF THRU DELETE-BQF-END.
       DELETE-BQF.
           READ BANQF NEXT AT END 
                GO DELETE-BQF-END.
           IF FIRME-KEY NOT = BQF-FIRME
              GO DELETE-BQF-END.
           IF LNK-SQL = "Y" 
              CALL "9-BANQF" USING LINK-V BQF-RECORD DEL-KEY 
           END-IF.
           DELETE BANQF INVALID CONTINUE.
           GO DELETE-BQF.
       DELETE-BQF-END.
           CLOSE BANQF.

       EFFACE-BANQUE.
           OPEN I-O   BANQP.
           MOVE FIRME-KEY TO BQP-FIRME.
           START BANQP KEY > BQP-KEY INVALID CLOSE BANQP
                NOT INVALID
           PERFORM DELETE-BQP THRU DELETE-BQP-END.
       DELETE-BQP.
           READ BANQP NEXT AT END 
                GO DELETE-BQP-END.
           IF FIRME-KEY NOT = BQP-FIRME
              GO DELETE-BQP-END.
           IF LNK-SQL = "Y" 
              CALL "9-BANQP" USING LINK-V BQP-RECORD DEL-KEY 
           END-IF.
           DELETE BANQP INVALID CONTINUE.
           GO DELETE-BQP.
       DELETE-BQP-END.
           CLOSE BANQP.

       EFFACE-CARRIERE.
           OPEN I-O CARRIERE.
           MOVE FIRME-KEY TO CAR-FIRME.
           START CARRIERE KEY > CAR-KEY INVALID KEY CLOSE CARRIERE
                NOT INVALID
           PERFORM DELETE-CAR THRU DELETE-CAR-END.
       DELETE-CAR.
           READ CARRIERE NEXT AT END 
                GO DELETE-CAR-END.
           IF FIRME-KEY NOT = CAR-FIRME
              GO DELETE-CAR-END.
           IF LNK-SQL = "Y" 
              CALL "9-CARRI" USING LINK-V CAR-RECORD DEL-KEY 
           END-IF. 
           DELETE CARRIERE INVALID CONTINUE. 
           GO DELETE-CAR.
       DELETE-CAR-END.
           CLOSE CARRIERE.

       EFFACE-CSPAI.
           OPEN I-O CODPAIE.
           MOVE FIRME-KEY TO CSP-FIRME.
           START CODPAIE KEY > CSP-KEY INVALID KEY CLOSE CODPAIE
                NOT INVALID
           PERFORM DELETE-CSP THRU DELETE-CSP-END.
           
       DELETE-CSP.
           READ CODPAIE NEXT AT END 
                GO DELETE-CSP-END.
           IF FIRME-KEY NOT = CSP-FIRME
              GO DELETE-CSP-END.
           IF LNK-SQL = "Y" 
              CALL "9-CODPAI" USING LINK-V CSP-RECORD DEL-KEY 
           END-IF.
           DELETE CODPAIE INVALID CONTINUE.
           GO DELETE-CSP.
       DELETE-CSP-END.
           CLOSE CODPAIE.

       EFFACE-CSFIX.
           OPEN I-O CODFIX.
           MOVE FIRME-KEY TO CSF-FIRME.
           START CODFIX KEY > CSF-KEY INVALID KEY CLOSE CODFIX
                NOT INVALID
           PERFORM DELETE-CSF THRU DELETE-CSF-END.
       DELETE-CSF.
           READ CODFIX NEXT AT END 
                GO DELETE-CSF-END.
           IF FIRME-KEY NOT = CSF-FIRME
              GO DELETE-CSF-END.
           IF LNK-SQL = "Y" 
              CALL "9-CODFIX" USING LINK-V CSF-RECORD DEL-KEY 
           END-IF.
           DELETE CODFIX INVALID CONTINUE.
           GO DELETE-CSF.
       DELETE-CSF-END.
           CLOSE CODFIX.

       EFFACE-CONGE.
           OPEN I-O CONGE.
           MOVE FIRME-KEY TO CONGE-FIRME.
           START CONGE KEY > CONGE-KEY INVALID CLOSE CONGE
                NOT INVALID
           PERFORM DELETE-CONGE THRU DELETE-CONGE-END.
       DELETE-CONGE.
           READ CONGE NEXT AT END 
                GO DELETE-CONGE-END.
           IF FIRME-KEY NOT = CONGE-FIRME
              GO DELETE-CONGE-END.
           IF LNK-SQL = "Y" 
              CALL "9-CONGE" USING LINK-V CONGE-RECORD DEL-KEY 
           END-IF.
           DELETE CONGE INVALID CONTINUE.
           GO DELETE-CONGE.
       DELETE-CONGE-END.
           CLOSE CONGE.

       EFFACE-CCOL.
           OPEN I-O CCOL.
           MOVE FIRME-KEY TO CCOL-FIRME.
           START CCOL KEY > CCOL-KEY INVALID CLOSE CCOL
                NOT INVALID
           PERFORM DELETE-CCOL THRU DELETE-CCOL-END.
       DELETE-CCOL.
           READ CCOL NEXT AT END 
                GO DELETE-CCOL-END.
           IF FIRME-KEY NOT = CCOL-FIRME
              GO DELETE-CCOL-END.
           IF LNK-SQL = "Y" 
              CALL "9-CCOL" USING LINK-V CCOL-RECORD DEL-KEY 
           END-IF.
           DELETE CCOL INVALID CONTINUE.
           GO DELETE-CCOL.
       DELETE-CCOL-END.
           CLOSE CCOL.

       EFFACE-REGISTRE.
           OPEN I-O REGISTRE.
           MOVE FIRME-KEY TO REG-FIRME.
           START REGISTRE KEY > REG-KEY INVALID CLOSE REGISTRE
                NOT INVALID
           PERFORM DELETE-REGISTRE THRU DELETE-REG-END.
       DELETE-REGISTRE.
           READ REGISTRE NEXT AT END 
                GO DELETE-REG-END.
           IF FIRME-KEY NOT = REG-FIRME
              GO DELETE-REG-END.
           IF LNK-SQL = "Y" 
              CALL "9-REGIST" USING LINK-V REG-RECORD DEL-KEY 
           END-IF.
           DELETE REGISTRE INVALID CONTINUE.
           GO DELETE-REGISTRE.
       DELETE-REG-END.
           CLOSE REGISTRE.

       EFFACE-FICHE.
           OPEN I-O   FICHE.
           MOVE FIRME-KEY TO FICHE-FIRME.
           START FICHE KEY > FICHE-KEY INVALID KEY CLOSE FICHE
                NOT INVALID
           PERFORM DELETE-FICHE THRU DELETE-FICHE-END.
       DELETE-FICHE.
           READ FICHE NEXT AT END 
                GO DELETE-FICHE-END.
           IF FIRME-KEY NOT = FICHE-FIRME
              GO DELETE-FICHE-END.
           IF LNK-SQL = "Y" 
              CALL "9-FICHE" USING LINK-V FICHE-RECORD DEL-KEY 
           END-IF.
           DELETE FICHE INVALID CONTINUE.
           GO DELETE-FICHE.
       DELETE-FICHE-END.
           CLOSE FICHE.

       EFFACE-JOURS.
           OPEN I-O   JOURS.
           INITIALIZE JRS-RECORD.
           MOVE FIRME-KEY  TO JRS-FIRME-2.
           START JOURS KEY > JRS-KEY-2 INVALID CLOSE JOURS
                NOT INVALID
           PERFORM READ-JOURS THRU READ-JRS-END.
       READ-JOURS.
           READ JOURS NEXT AT END 
                GO READ-JRS-END.
           IF FIRME-KEY NOT = JRS-FIRME
              GO READ-JRS-END.
           IF LNK-SQL = "Y" 
              CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY 
           END-IF.
           DELETE JOURS INVALID CONTINUE.
           GO READ-JOURS.
       READ-JRS-END.
           CLOSE JOURS.

       EFFACE-HEURES.
           OPEN I-O   HEURES.
           INITIALIZE HRS-RECORD.
           MOVE FIRME-KEY  TO HRS-FIRME.
           START HEURES KEY > HRS-KEY INVALID KEY CLOSE HEURES
                NOT INVALID
           PERFORM READ-HEURES THRU READ-HRS-END.
       READ-HEURES.
           READ HEURES NEXT AT END 
                GO READ-HRS-END.
           IF FIRME-KEY NOT = HRS-FIRME
              GO READ-HRS-END.
           IF LNK-SQL = "Y" 
              CALL "9-HEURES" USING LINK-V HRS-RECORD DEL-KEY 
           END-IF.
           DELETE HEURES INVALID CONTINUE.
           GO READ-HEURES.
       READ-HRS-END.
           CLOSE HEURES.


       EFFACE-HORSEM.
           OPEN I-O   HORSEM.
           MOVE FIRME-KEY TO HJS-FIRME.
           START HORSEM KEY > HJS-KEY INVALID CLOSE HORSEM
                NOT INVALID
           PERFORM DELETE-HORSEM THRU DELETE-HJS-END.
       DELETE-HORSEM.
           READ HORSEM NEXT AT END 
                GO DELETE-HJS-END.
           IF FIRME-KEY NOT = HJS-FIRME
                GO DELETE-HJS-END.
           IF LNK-SQL = "Y" 
              CALL "9-HORSEM" USING LINK-V HJS-RECORD DEL-KEY 
           END-IF.
           DELETE HORSEM INVALID CONTINUE.
           GO DELETE-HORSEM.
       DELETE-HJS-END.
           CLOSE HORSEM.

       EFFACE-LIVRE.
           OPEN I-O   LIVRE.
           MOVE FIRME-KEY TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON.
           START LIVRE KEY > L-KEY INVALID CLOSE LIVRE
                NOT INVALID
           PERFORM DELETE-LP THRU DELETE-L-END.
       DELETE-LP.
           READ LIVRE NEXT AT END 
                GO DELETE-L-END.
           IF FIRME-KEY NOT = L-FIRME
              GO DELETE-L-END.
           IF LNK-SQL = "Y" 
              CALL "9-LIVRE" USING LINK-V L-RECORD DEL-KEY 
           END-IF.
           DELETE LIVRE INVALID CONTINUE.
           GO DELETE-LP.
       DELETE-L-END.
           CLOSE LIVRE.

       EFFACE-VIREMENT.
           OPEN I-O   VIREMENT.
           MOVE FIRME-KEY  TO VIR-FIRME.
           START VIREMENT KEY > VIR-KEY INVALID CLOSE VIREMENT
                NOT INVALID
               PERFORM DELETE-VIR THRU DELETE-VIR-END.
       DELETE-VIR.
           READ VIREMENT NEXT AT END 
                GO DELETE-VIR-END.
           IF FIRME-KEY NOT = VIR-FIRME
              GO DELETE-VIR-END.
           IF LNK-SQL = "Y" 
              CALL "9-VIREM" USING LINK-V VIR-RECORD DEL-KEY 
           END-IF.
           DELETE VIREMENT INVALID CONTINUE.
           GO DELETE-VIR.
       DELETE-VIR-END.
           CLOSE VIREMENT.

       EFFACE-CONTRAT.
           OPEN I-O   CONTRAT.
           MOVE FIRME-KEY TO CON-FIRME.
           DELETE CONTRAT INVALID CONTINUE
           END-DELETE.
           START CONTRAT KEY > CON-KEY INVALID KEY CLOSE CONTRAT
                NOT INVALID
           PERFORM DELETE-CON THRU DELETE-CON-END.
       DELETE-CON.
           READ CONTRAT NEXT AT END 
                GO DELETE-CON-END.
           IF FIRME-KEY NOT = CON-FIRME
              GO DELETE-CON-END.
           IF LNK-SQL = "Y" 
              CALL "9-CONTR" USING LINK-V CON-RECORD DEL-KEY 
           END-IF.
           DELETE CONTRAT INVALID CONTINUE.
           GO DELETE-CON.
       DELETE-CON-END.
           CLOSE CONTRAT.

       EFFACE-FAMILLE.
           OPEN I-O   FAMILLE.
           MOVE FIRME-KEY TO FAM-FIRME.
           DELETE FAMILLE INVALID CONTINUE END-DELETE.
           START FAMILLE KEY > FAM-KEY INVALID CLOSE FAMILLE
                NOT INVALID
           PERFORM DELETE-FAM THRU DELETE-FAM-END.
       DELETE-FAM.
           READ FAMILLE NEXT AT END 
                GO DELETE-FAM-END.
           IF FIRME-KEY     NOT = FAM-FIRME
              GO DELETE-FAM-END.
           IF LNK-SQL = "Y" 
              CALL "9-FAMIL" USING LINK-V FAM-RECORD DEL-KEY 
           END-IF.
           DELETE FAMILLE INVALID CONTINUE.
           GO DELETE-FAM.
       DELETE-FAM-END.
           CLOSE FAMILLE.




       EFFACE-COUT.
           OPEN I-O   COUTS.
           MOVE FIRME-KEY TO COUT-FIRME.
           START COUTS KEY > COUT-KEY INVALID KEY CLOSE COUTS
                NOT INVALID
           PERFORM DELETE-COUT THRU DELETE-COUT-END.
       DELETE-COUT.
           READ COUTS NEXT AT END 
                GO DELETE-COUT-END.
           IF FIRME-KEY NOT = COUT-FIRME
              GO DELETE-COUT-END.
           IF LNK-SQL = "Y" 
              CALL "9-COUT" USING LINK-V COUT-RECORD DEL-KEY 
           END-IF.
           DELETE COUTS INVALID CONTINUE.
           GO DELETE-COUT.
       DELETE-COUT-END.
           CLOSE COUTS.


       EFFACE-EQUIPE.
           OPEN I-O   EQUIPE.
           MOVE FIRME-KEY TO EQ-FIRME.
           DELETE EQUIPE INVALID CONTINUE END-DELETE.
           START EQUIPE KEY > EQ-KEY INVALID CLOSE EQUIPE
                NOT INVALID
           PERFORM DELETE-EQ THRU DELETE-EQ-END.
       DELETE-EQ.
           READ EQUIPE NEXT AT END 
                GO DELETE-EQ-END.
           IF FIRME-KEY     NOT = EQ-FIRME
              GO DELETE-EQ-END.
           IF LNK-SQL = "Y" 
              CALL "9-EQUIPE" USING LINK-V EQ-RECORD DEL-KEY 
           END-IF.
           DELETE EQUIPE INVALID CONTINUE.
           GO DELETE-EQ.
       DELETE-EQ-END.
           CLOSE EQUIPE.



       EFFACE-DIVIS.
           OPEN I-O   DIVIS.
           MOVE FIRME-KEY TO DIV-FIRME.
           DELETE DIVIS INVALID CONTINUE END-DELETE.
           START DIVIS KEY > DIV-KEY INVALID CLOSE DIVIS
                NOT INVALID
           PERFORM DELETE-DIV THRU DELETE-DIV-END.
       DELETE-DIV.
           READ DIVIS NEXT AT END 
                GO DELETE-DIV-END.
           IF FIRME-KEY     NOT = DIV-FIRME
              GO DELETE-DIV-END.
           IF LNK-SQL = "Y" 
              CALL "9-DIVIS" USING LINK-V DIV-RECORD DEL-KEY 
           END-IF.
           DELETE DIVIS INVALID CONTINUE.
           GO DELETE-DIV.
       DELETE-DIV-END.
           CLOSE DIVIS.

       EFFACE-POCL.
           OPEN I-O   POCL.
           MOVE FIRME-KEY TO PC-FIRME.
           DELETE POCL INVALID CONTINUE END-DELETE.
           START POCL KEY > PC-KEY INVALID CLOSE POCL
                NOT INVALID
           PERFORM DELETE-PC THRU DELETE-PC-END.
       DELETE-PC.
           READ POCL NEXT AT END 
                GO DELETE-PC-END.
           IF FIRME-KEY     NOT = PC-FIRME
              GO DELETE-PC-END.
           IF LNK-SQL = "Y" 
              CALL "9-POCL" USING LINK-V PC-RECORD DEL-KEY 
           END-IF.
           DELETE POCL INVALID CONTINUE.
           GO DELETE-PC.
       DELETE-PC-END.
           CLOSE POCL.

       DIS-HE-01.
           MOVE FIRME-KEY TO HE-Z4.
           DISPLAY HE-Z4 LINE 3 POSITION 15.
           DISPLAY FIRME-NOM   LINE  3 POSITION 20.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 190 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE 0 TO LNK-PERSON.
           MOVE SAVE-MOIS TO LNK-MOIS.
           CLOSE REGISTRE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
