      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-PERS PROGRAMME BATCH  PERSONNES           �
      *  � AVEC DETAILS - ROUTINES A PARTIR DU MENU              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-PERS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "TRIPR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 17.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "METIER.REC".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".
           COPY "EQUIPE.REC".
           COPY "POCL.REC".
           COPY "FICHE.REC".
           COPY "DIVISION.REC".
           COPY "REGIME.REC".
           COPY "PAYS.REC".

       01  ANCIEN                PIC 9(4) VALUE 0.
       01  NAISSANCE             PIC 9(4) VALUE 0.
       01  TEST-POSTE            PIC 9(8) VALUE 0.
       01  POST                  PIC 9(8) VALUE 0.
       01  DIV                   PIC 9(8) VALUE 0.
       01  METIER                PIC X(10) VALUE SPACES.
       01  SAVE-LANGUE           PIC X VALUE SPACES.
       01  ETAT                  PIC X VALUE SPACES.
       01  PAYS                  PIC X(10) VALUE SPACES.
       01  NATION                PIC X(10) VALUE SPACES.
       01  MOIS-DEBUT      PIC 99 VALUE 1.
       01  MOIS-FIN        PIC 99 VALUE 12.
       01  MOIS-DF.
           02 MOIS-ACT           PIC 9 OCCURS 12.

       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).


       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.

       01  TOUS                  PIC 9 VALUE 0.

       01  HE-SEL.
           02 HE-S PIC 9       OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-PERS.
       
           MOVE LNK-MOIS TO SAVE-MOIS MOIS-DEBUT MOIS-FIN.
           MOVE LNK-LANGUAGE TO SAVE-LANGUE.
           INITIALIZE TEST-EXTENSION.
           IF MENU-EXTENSION-1 = "3-STEUER"
              MOVE  1 TO MOIS-DEBUT
              MOVE 12 TO MOIS-FIN.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TEST-EXTENSION PC-RECORD.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" NX-KEY.
           MOVE PC-NUMBER TO TEST-POSTE.
           PERFORM DEF-DETAILS.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8 THRU 10
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 12 THRU 15 
                      MOVE 0000000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 16    MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 17    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF HE-S(INDICE-ZONE) = 0
              CONTINUE
           ELSE
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-10
           WHEN 11 PERFORM AVANT-11
           WHEN 12 PERFORM AVANT-12
           WHEN 13 PERFORM AVANT-13
           WHEN 14 PERFORM AVANT-14
           WHEN 15 PERFORM AVANT-15


           WHEN 16 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN 17 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           IF HE-S(INDICE-ZONE) = 0
              CONTINUE
           ELSE
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7 
           WHEN  8 PERFORM APRES-8 
           WHEN  9 PERFORM APRES-9 
           WHEN 10 PERFORM APRES-10
           WHEN 11 PERFORM APRES-11
           WHEN 12 PERFORM APRES-12
           WHEN 13 PERFORM APRES-13
           WHEN 14 PERFORM APRES-14
           WHEN 15 PERFORM APRES-15
           WHEN 16 PERFORM APRES-SORT
           WHEN 17 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-7.
           IF REG-PERSON NOT = END-NUMBER
           AND TEST-POSTE > 0
              ACCEPT POST
           LINE 12 POSITION 25 SIZE 8
           TAB UPDATE NO BEEP CURSOR  1 
           ON EXCEPTION EXC-KEY CONTINUE
           ELSE 
             MOVE 0 TO POST
           END-IF.

       AVANT-8.
           ACCEPT METIER
           LINE 13 POSITION 23 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-9.
           ACCEPT PAYS
           LINE 14 POSITION 23 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-10.
           ACCEPT NATION
           LINE 15 POSITION 23 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-11.
           ACCEPT ETAT
           LINE 16 POSITION 35 SIZE 1
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-12.
           ACCEPT MOIS-DEBUT
             LINE 17 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
                     PERFORM APRES-12
             WHEN 66 ADD      1 TO   MOIS-DEBUT
                     PERFORM APRES-12
             END-EVALUATE.

       AVANT-13.
           ACCEPT MOIS-FIN
             LINE 18 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
                     PERFORM APRES-13
             WHEN 66 ADD      1 TO   MOIS-FIN
                     PERFORM APRES-13
             END-EVALUATE.

       AVANT-14.
           ACCEPT ANCIEN
             LINE 19 POSITION 29 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM ANCIEN
                     PERFORM APRES-14
             WHEN 66 ADD      1 TO   ANCIEN
                     PERFORM APRES-14
             END-EVALUATE.

       AVANT-15.
           ACCEPT NAISSANCE
             LINE 20 POSITION 29 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM NAISSANCE
                     PERFORM APRES-15
             WHEN 66 ADD      1 TO   NAISSANCE
                     PERFORM APRES-15
             END-EVALUATE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-7.
           MOVE POST TO PC-NUMBER.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 5 CALL "1-POCL" USING LINK-V
                  MOVE LNK-VAL TO PC-NUMBER
                  CANCEL "1-POCL"
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
           WHEN  2 THRU 3 CALL "2-POCL" USING LINK-V PC-RECORD
                   MOVE PC-NUMBER TO POST
                   PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
           WHEN 13 IF POST > 0
                      PERFORM NEXT-POSTE
                   END-IF
           WHEN OTHER PERFORM NEXT-POSTE
           END-EVALUATE.
           IF PC-NUMBER > 0
              MOVE PC-NUMBER TO POST
           END-IF.
           PERFORM DIS-HE-07.


       APRES-8.
           MOVE METIER TO MET-CODE.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-METIER" USING LINK-V MET-RECORD
                    MOVE MET-CODE TO METIER
                    CANCEL "2-METIER"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER
                    PERFORM NEXT-METIER.
           MOVE MET-CODE TO METIER.
           PERFORM DIS-HE-08.

       APRES-9.
           MOVE PAYS TO PAYS-CODE.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-PAYS" USING LINK-V PAYS-RECORD
                    MOVE PAYS-CODE TO PAYS
                    CANCEL "2-PAYS"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER
                    PERFORM NEXT-PAYS
                    MOVE PAYS-CODE TO PAYS.
           PERFORM DIS-HE-09.

       APRES-10.
           MOVE NATION TO PAYS-CODE.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-PAYS" USING LINK-V PAYS-RECORD
                    MOVE PAYS-CODE TO NATION
                    CANCEL "2-PAYS"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER
                    PERFORM NEXT-PAYS
                    MOVE PAYS-CODE TO NATION.
           PERFORM DIS-HE-10.

       APRES-11.
           MOVE 0 TO INPUT-ERROR.
           EVALUATE ETAT
                WHEN " " CONTINUE
                WHEN "C" CONTINUE
                WHEN "D" CONTINUE
                WHEN "M" CONTINUE
                WHEN "V" CONTINUE
                WHEN "S" CONTINUE
                WHEN OTHER MOVE 1 TO INPUT-ERROR
                MOVE SPACES TO ETAT
           END-EVALUATE.
           PERFORM DIS-HE-11.

       APRES-12.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-12.

       APRES-13.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-12.
           PERFORM DIS-HE-13.
           INITIALIZE MOIS-DF.
           PERFORM ACT VARYING IDX FROM 
                   MOIS-DEBUT BY 1 UNTIL IDX > MOIS-FIN.

       APRES-14.
           COMPUTE SH-00 = LNK-ANNEE - 75
           IF ANCIEN > 0
              IF ANCIEN < SH-00
                 MOVE SH-00 TO ANCIEN
                 MOVE 1 TO INPUT-ERROR
              END-IF
              IF ANCIEN > LNK-ANNEE
                 MOVE LNK-ANNEE TO ANCIEN
                 MOVE 1 TO INPUT-ERROR
              END-IF
           END-IF.
           PERFORM DIS-HE-14.

       APRES-15.
           COMPUTE SH-00 = LNK-ANNEE - 90
           COMPUTE SH-01 = LNK-ANNEE - 14
           IF NAISSANCE > 0
              IF NAISSANCE < SH-00
                 MOVE SH-00 TO NAISSANCE
                 MOVE 1 TO INPUT-ERROR
              END-IF
              IF NAISSANCE > SH-01
                 MOVE SH-01 TO NAISSANCE
                 MOVE 1 TO INPUT-ERROR
              END-IF
           END-IF.
           PERFORM DIS-HE-15.

       APRES-DEC.
           MOVE 0 TO TOUS.
           IF EXC-KEY = 15
           OR EXC-KEY = 16
              MOVE 1 TO TOUS
              SUBTRACT 10 FROM EXC-KEY
           END-IF.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5  PERFORM TRAITEMENT
                   PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-POSTE.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" SAVE-KEY.

       NEXT-METIER.
           CALL "6-METIER" USING LINK-V MET-RECORD EXC-KEY.

       NEXT-PAYS.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           IF PR-NOM = SPACES
              GO READ-PERSON-2
           END-IF.
           IF  NATION NOT = SPACES
           AND NATION NOT = PR-NATIONALITE
              GO READ-PERSON-2
           END-IF.
           IF  PAYS NOT = SPACES
           AND PAYS NOT = PR-PAYS
              GO READ-PERSON-2
           END-IF.
           IF  NAISSANCE > 0
           AND NAISSANCE NOT = REG-NAISS-A
              GO READ-PERSON-2
           END-IF.
           IF  ANCIEN > 0
           AND ANCIEN NOT = REG-ANCIEN-A
              GO READ-PERSON-2
           END-IF.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-2
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           IF  DIV > 0 
           AND DIV NOT = CAR-DIVISION
               GO READ-PERSON-3
           END-IF.
           IF POST > 0 
           AND POST NOT = CAR-POSTE-FRAIS
              GO READ-PERSON-2
           END-IF.
           IF  METIER NOT = SPACES
           AND METIER NOT = CAR-METIER
              GO READ-PERSON-2
           END-IF.
           IF  ETAT NOT = SPACES
           AND ETAT NOT = FICHE-ETAT-CIVIL
              GO READ-PERSON-2
           END-IF.
           MOVE 0 TO SH-00.
           PERFORM PRESENT VARYING IDX FROM MOIS-DEBUT 
                   BY 1 UNTIL IDX > MOIS-FIN.
           IF SH-00 = 0
              GO READ-PERSON-2
           END-IF.
           PERFORM DIS-HE-01.
           CALL MENU-EXTENSION-1 USING LINK-V REG-RECORD PR-RECORD.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD FICHE-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD NUL-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                 AND TOUS = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.


       DIS-HE-01.
           DISPLAY A-N LINE 4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           IF A-N = "N"
              DISPLAY LNK-TEXT LINE 6 POSITION 27 SIZE 34
           ELSE
              DISPLAY SPACES   LINE 6 POSITION 37 SIZE 1
              DISPLAY LNK-TEXT LINE 6 POSITION 38 SIZE 23.
           MOVE END-NUMBER  TO HE-Z6.
           DISPLAY HE-Z6 LINE 7 POSITION 20 SIZE 6.
       DIS-HE-STAT.
           DISPLAY STATUT LINE 9 POSITION 32 SIZE 1.
           DISPLAY STAT-NOM  LINE 9 POSITION 35 SIZE 15.
       DIS-HE-05.
           MOVE COUT-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 10 POSITION 25.
           DISPLAY COUT-NOM LINE 10 POSITION 35 SIZE 15.
       DIS-HE-06.
           MOVE DIV-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 11 POSITION 25.
           DISPLAY DIV-NOM LINE 11 POSITION 35 SIZE 15.
       DIS-HE-07.
           MOVE POST TO HE-Z8.
           DISPLAY HE-Z8 LINE 12 POSITION 25.
           IF POST NOT = 0
              MOVE POST TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
           ELSE
              INITIALIZE PC-RECORD
           END-IF.
           DISPLAY PC-NOM  LINE 12 POSITION 35 SIZE 15.


       DIS-HE-08.
           INITIALIZE MET-RECORD.
           MOVE METIER TO MET-CODE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           DISPLAY MET-NOM(1)  LINE 13 POSITION 35 SIZE 15.
           MOVE MET-CODE TO METIER.
           DISPLAY METIER LINE 13 POSITION 23.

       DIS-HE-09.
           INITIALIZE PAYS-RECORD.
           MOVE PAYS TO PAYS-CODE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.
           DISPLAY PAYS-NOM  LINE 14 POSITION 35 SIZE 15.
           MOVE PAYS-CODE TO PAYS.
           DISPLAY PAYS LINE 14 POSITION 23.

       DIS-HE-10.
           INITIALIZE PAYS-RECORD.
           MOVE NATION TO PAYS-CODE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.
           DISPLAY PAYS-NOM  LINE 15 POSITION 35 SIZE 15.
           MOVE PAYS-CODE TO NATION.
           DISPLAY NATION LINE 15 POSITION 23.

       DIS-HE-11.
           DISPLAY ETAT   LINE 16 POSITION 35.

       DIS-HE-12.
           DISPLAY MOIS-DEBUT LINE 17 POSITION 31.
           MOVE MOIS-DEBUT TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 17351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-13.
           DISPLAY MOIS-FIN LINE 18 POSITION 31.
           MOVE MOIS-FIN TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 18351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-14.
           DISPLAY ANCIEN LINE 19 POSITION 29.
       DIS-HE-15.
           DISPLAY NAISSANCE LINE 20 POSITION 29.
       DIS-HE-END.
           EXIT.


       ACT.
           MOVE 1 TO MOIS-ACT(IDX).

       PRESENT.
           ADD PRES-TOT(IDX) TO SH-00.

       ECRAN-1.
           IF HE-S(INDICE-ZONE) = 0
              CONTINUE
           ELSE
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM DIS-HE-01
               WHEN  2 PERFORM DIS-HE-01
               WHEN  3 PERFORM DIS-HE-01 
               WHEN  4 PERFORM DIS-HE-STAT
               WHEN  5 PERFORM DIS-HE-05 
               WHEN  6 PERFORM DIS-HE-06 
               WHEN  7 PERFORM DIS-HE-07 
               WHEN  8 PERFORM DIS-HE-08 
               WHEN  9 PERFORM DIS-HE-09 
               WHEN 10 PERFORM DIS-HE-10
               WHEN 11 PERFORM DIS-HE-11 
               WHEN 12 PERFORM DIS-HE-12 
               WHEN 13 PERFORM DIS-HE-13 
               WHEN 14 PERFORM DIS-HE-14
               WHEN 15 PERFORM DIS-HE-15
           END-EVALUATE.


       DEF-DETAILS.
           INITIALIZE HE-SEL.
           MOVE 1 TO HE-S(1) HE-S(17).
SORT       IF MENU-BATCH > 0
              MOVE 1 TO HE-S(16).
           IF MENU-PROG-NUM(2) = 1 OR 3
              MOVE 1 TO HE-S(2).
           IF MENU-PROG-NUM(2) > 1
              MOVE 1 TO HE-S(3).

           IF MENU-PROG-NUM(3) = 1 OR 3
              MOVE 1 TO HE-S(4).
           IF MENU-PROG-NUM(3) > 1
              MOVE 1 TO HE-S(5).

           IF MENU-PROG-NUM(4) = 1 OR 3
              MOVE 1 TO HE-S(6).
           IF MENU-PROG-NUM(4) > 1
              MOVE 1 TO HE-S(7).

           IF MENU-PROG-NUM(5) = 1 OR 3
              MOVE 1 TO HE-S(8).
           IF MENU-PROG-NUM(5) > 1
              MOVE 1 TO HE-S(9).

           IF MENU-PROG-NUM(6) = 1 OR 3
              MOVE 1 TO HE-S(10).
           IF MENU-PROG-NUM(6) > 1
              MOVE 1 TO HE-S(11).

           IF MENU-PROG-NUM(7) = 1 OR 3
              MOVE 1 TO HE-S(12).
           IF MENU-PROG-NUM(7) > 1
              MOVE 1 TO HE-S(13).

           IF MENU-PROG-NUM(8) = 1 OR 3
              MOVE 1 TO HE-S(14).
           IF MENU-PROG-NUM(8) > 1
              MOVE 1 TO HE-S(15).
              
       AFFICHAGE-ECRAN.
           MOVE 1231 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           PERFORM AFFICHE-D VARYING IDX-1
               FROM 1 BY 1 UNTIL IDX-1 > 16.
           IF MENU-BATCH = 0
              DISPLAY SPACES LINE 22 POSITION 1 SIZE 80.


       AFFICHAGE-DETAIL.
           MOVE INDICE-ZONE TO IDX.
           PERFORM ECRAN-1 VARYING INDICE-ZONE
                FROM 1 BY 1 UNTIL INDICE-ZONE > 16.
           MOVE IDX TO INDICE-ZONE.

       AFFICHE-D.
           COMPUTE LIN-IDX = IDX-1 + 4
           IF IDX-1 > 3
              ADD 1 TO LIN-IDX.
           IF HE-S(IDX-1) = 0
              DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       END-PROGRAM.
           IF MENU-BATCH > 0
              MOVE 99 TO LNK-VAL
              CALL MENU-EXTENSION-1 USING LINK-V REG-RECORD PR-RECORD.
           CANCEL MENU-EXTENSION-1.
           CANCEL "4-SORT"
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-MOIS TO LNK-MOIS.
           MOVE SAVE-LANGUE TO LNK-LANGUAGE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDIV.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XSORT.CPY".
           COPY "XACTION.CPY".

