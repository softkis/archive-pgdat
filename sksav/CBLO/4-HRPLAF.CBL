      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-HRPLAF REPORT DES HEURES SUPPLEMENTAIRES  �
      *  � AVEC PLAFOND ALCUILUX                                 �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-HRPLAF.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CONGE.REC".
           COPY "CARRIERE.REC".
           COPY "CODPAR.REC".
           COPY "CODPAIE.REC".
           COPY "LIVRE.REC".

       01  SAVE-EXTENSION  PIC X(10).
       01  MOIS                  PIC 99 VALUE 0.
       01  SAL-ACT               PIC 9(6)V9(4).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02  SAL-ACT-A         PIC 9(8).
           02  SAL-ACT-B         PIC 9(2).
       01  VALEURS.
           02  RECUP-TOTAL       PIC S999V99.
           02  PLAFOND           PIC S999V99.
      
       LINKAGE SECTION.
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".

       PROCEDURE DIVISION USING LINK-V REG-RECORD.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-HRPLAF.
       
           IF LNK-SUITE > 0
              EXIT PROGRAM.
           INITIALIZE CAR-RECORD L-RECORD VALEURS CONGE-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           IF CAR-PLAF-RECUP <= 0
              EXIT PROGRAM.
           CALL "6-GCP" USING LINK-V CP-RECORD.
           INITIALIZE CSP-RECORD.
           MOVE COD-PAR(95, 1) TO CSP-CODE.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           IF  CSP-DONNEE-1 = 0
           AND CSP-UNITE    = 0
              EXIT PROGRAM.
           ADD CSP-DONNEE-1 TO CSP-UNITE.
           MOVE 0 TO CSP-DONNEE-1.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD FAKE-KEY.
           COMPUTE RECUP-TOTAL = CONGE-RECUP.
           PERFORM TOT VARYING MOIS FROM 1 BY 1 UNTIL MOIS = LNK-MOIS.
           COMPUTE PLAFOND = CAR-PLAF-RECUP - RECUP-TOTAL.
           IF PLAFOND < 0
              EXIT PROGRAM
           END-IF.
           IF CSP-UNITE <= PLAFOND
              MOVE CSP-UNITE TO CSP-DONNEE-1
              MOVE 0 TO CSP-UNITE
           ELSE
              COMPUTE CSP-UNITE = CSP-UNITE - PLAFOND
              MOVE PLAFOND TO CSP-DONNEE-1
           END-IF.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD DEL-KEY.
           COMPUTE SAL-ACT = CSP-UNITAIRE * CSP-UNITE.
           ADD ,005 TO SAL-ACT.
           MOVE 0 TO SAL-ACT-B.
           MOVE SAL-ACT TO CSP-TOTAL.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.
           EXIT PROGRAM.

       TOT.
           PERFORM READ-LP.
           ADD      L-UNI-RECUP-PLUS TO   RECUP-TOTAL.
           SUBTRACT L-UNI-RECUP-MIN  FROM RECUP-TOTAL.
           SUBTRACT L-UNI-RECUP      FROM RECUP-TOTAL.

       READ-LP.
           MOVE MOIS TO L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.

           COPY "XACTION.CPY".
