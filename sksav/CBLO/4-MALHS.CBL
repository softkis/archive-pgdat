      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-MALHS RECHERCHE HRS SUP POUR MALADIE      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-MALHS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "LIVRE.REC".

       01  TOT-HRS.
           02 TOT-HRS-IDX OCCURS 6.
              03 HRS     PIC 9(3)V99.
       01  SAVE-SUITE    PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
       01  TOT-OCCUP.
           02 TOT-OCC-IDX OCCURS 200.
              03 OCCUP-UNITE     PIC 9(8)V999 COMP-3.
              03 OCCUP-CODPAIE   PIC 99.
       01  DATE-3M.
           02 3M-A    PIC 9999.
           02 3M-M    PIC 99.
           02 3M-J    PIC 99.

       PROCEDURE DIVISION USING LINK-V REG-RECORD TOT-OCCUP DATE-3M.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-MALHS.
       
           MOVE LNK-MOIS  TO SAVE-MOIS.
           MOVE LNK-ANNEE TO SAVE-ANNEE.
           MOVE LNK-SUITE TO SAVE-SUITE.
           MOVE 0 TO LNK-SUITE.
           MOVE 3M-A    TO LNK-ANNEE.
           INITIALIZE TOT-HRS.
           PERFORM INIT-OCC VARYING IDX FROM 95 BY 1 UNTIL IDX > 99.
           PERFORM NXLP VARYING IDX FROM 0 BY 1 UNTIL IDX > 2.
           PERFORM END-PROGRAM.

       NXLP.
           COMPUTE LNK-MOIS = 3M-M + IDX.
           IF LNK-MOIS = 13
              MOVE 1 TO LNK-MOIS
              ADD 1 TO LNK-ANNEE
           END-IF.
           IF LNK-MOIS = 14
              MOVE 2 TO LNK-MOIS
           END-IF.
           IF  LNK-MOIS  = SAVE-MOIS
           AND LNK-ANNEE = SAVE-ANNEE
              PERFORM END-PROGRAM.
           INITIALIZE L-RECORD.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF  L-UNI-HRS-SUP   = 0
           AND L-UNI-SALAIRE   > 0
           AND L-UNI-MALADIE-1 = 0
              MOVE SAVE-MOIS  TO LNK-MOIS
              MOVE SAVE-ANNEE TO LNK-ANNEE
              MOVE SAVE-SUITE TO LNK-SUITE
              EXIT PROGRAM.
              
ELCO       IF L-UNI-MOYENNE-MAL > L-UNI-SALAIRE 
OCC 34        MOVE L-UNI-MOYENNE-MAL TO L-UNI-SALAIRE 
           END-IF.
           IF L-UNI-SALAIRE = 0
              COMPUTE L-UNI-SALAIRE = L-UNI-HRS-SUP.
           COMPUTE HRS(1) = HRS(1) + L-UNI-HRS-SUP.
           COMPUTE HRS(2) = HRS(2) + L-UNI-HRS-SUP-1.
           COMPUTE HRS(3) = HRS(3) + L-UNI-HRS-SUP-2.
           COMPUTE HRS(6) = HRS(6) + L-UNI-SALAIRE.

       INIT-OCC.
           MOVE 0 TO OCCUP-UNITE(IDX).

       END-PROGRAM.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           MOVE SAVE-ANNEE TO LNK-ANNEE.
           MOVE SAVE-SUITE TO LNK-SUITE.
           IF HRS(3) > 0 
              COMPUTE HRS(2) = HRS(1) - HRS(3)
           ELSE
              IF HRS(2) < HRS(1)
                 COMPUTE HRS(2) = HRS(1)
              END-IF
           END-IF.
           COMPUTE OCCUP-UNITE(95) = HRS(1) / HRS(6) * OCCUP-UNITE(100).
           COMPUTE OCCUP-UNITE(96) = HRS(2) / HRS(6) * OCCUP-UNITE(100).
           COMPUTE OCCUP-UNITE(97) = HRS(3) / HRS(6) * OCCUP-UNITE(100).
           EXIT PROGRAM.
           COPY "XACTION.CPY".
           