      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-ATTCG  ATTESTATION CONGE                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-ATTCG.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01   CHOIX-MAX         PIC 99 VALUE 21.
       01  S-KEY    PIC 9(4) COMP-1 VALUE 65.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CONTRAT.REC".
           COPY "PAYS.REC".
           COPY "ATT.LNK".
           COPY "FAMILLE.REC".

       01   COMPTEUR          PIC 99 VALUE 0.

       01 HE-SEL.
          02 HE-DETAIL OCCURS 10.
              03 H-S         PIC X.
              03 H-NOM       PIC X(20).
              03 H-PRENOM    PIC X(20).
              03 H-DATE      PIC X(10).

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2). 
           02 HE-Z3 PIC Z(3). 
           02 HE-Z4 PIC Z(4). 
           02 HE-Z6 PIC Z(6).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HP-DATE .
              03 HE-J PIC ZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HE-M PIC ZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HE-A PIC ZZZZ.
           02 HE-SNOCS.
              03 HS-A PIC ZZZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HS-M PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HS-J PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HS-S PIC 999.
       01  HE-ZP.
           02  KLAMOP              PIC X VALUE "(".
           02  HE-ZN PIC Z(8).
           02  KLAMZOU             PIC X VALUE ")".

       01  DATES.
              03 DATE-1   PIC 9(8).
              03 D1 REDEFINES DATE-1.
                 04 1-A   PIC 9999.
                 04 1-M   PIC 99.
                 04 1-J   PIC 99.
              03 DATE-2   PIC 9(8).
              03 D2 REDEFINES DATE-2.
                 04 2-A   PIC 9999.
                 04 2-M   PIC 99.
                 04 2-J   PIC 99.
              03 DATE-3   PIC 9(8).
              03 D3 REDEFINES DATE-3.
                 04 3-A   PIC 9999.
                 04 3-M   PIC 99.
                 04 3-J   PIC 99.
              03 DATE-4   PIC 9(8).
              03 D4 REDEFINES DATE-4.
                 04 4-A   PIC 9999.
                 04 4-M   PIC 99.
                 04 4-J   PIC 99.
              03 DATE-5   PIC 9(8).
              03 D5 REDEFINES DATE-5.
                 04 5-A   PIC 9999.
                 04 5-M   PIC 99.
                 04 5-J   PIC 99.
              03 DATE-6   PIC 9(8).
              03 D6 REDEFINES DATE-6.
                 04 6-A   PIC 9999.
                 04 6-M   PIC 99.
                 04 6-J   PIC 99.
              03 DATE-7   PIC 9(8).
              03 D7 REDEFINES DATE-7.
                 04 7-A   PIC 9999.
                 04 7-M   PIC 99.
                 04 7-J   PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-ATTCG.


           CALL "0-TODAY" USING TODAY.
           INITIALIZE ATT-RECORD.
           MOVE 1 TO LNK-PRESENCE.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0027000000 TO EXC-KFR(6).
           MOVE 0052530000 TO EXC-KFR(11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3     MOVE 0063000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN CHOIX-MAX
                      MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.
 
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-10 
           WHEN 11 THRU 20 PERFORM AVANT-ALL THRU AVANT-ALL-END
           WHEN CHOIX-MAX PERFORM AVANT-DEC.


           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN 11 THRU 20 PERFORM APRES-YN
           WHEN CHOIX-MAX PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-3.
           ACCEPT PAYS-CODE
             LINE 5 POSITION 30 SIZE 3
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM AFFICHAGE-DETAIL.


       AVANT-4.
           MOVE 06300000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-1
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO 1-A
              MOVE TODAY-MOIS  TO 1-M
              MOVE TODAY-JOUR  TO 1-J
              MOVE 0 TO LNK-NUM.
           PERFORM DIS-HE-04.

       AVANT-5.
           IF DATE-2 < DATE-1
           OR DATE-1 = 0
              MOVE DATE-1 TO DATE-2
           END-IF.
           IF DATE-1 > 0
           MOVE 06600000 TO LNK-POSITION
           CALL "0-GDATE" USING DATE-2
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           PERFORM DIS-HE-05.
       AVANT-6.
           MOVE 07300000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-3
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO 3-A
              MOVE TODAY-MOIS  TO 3-M
              MOVE TODAY-JOUR  TO 3-J
              MOVE 0 TO LNK-NUM.
           PERFORM DIS-HE-06.
       AVANT-7.
           IF DATE-4 < DATE-3
           OR DATE-3 = 0
              MOVE DATE-3 TO DATE-4
           END-IF.
           IF DATE-3 > 0
           MOVE 07600000 TO LNK-POSITION
           CALL "0-GDATE" USING DATE-4
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           PERFORM DIS-HE-07.
       AVANT-8.
           MOVE 08300000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-5
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO 5-A
              MOVE TODAY-MOIS  TO 5-M
              MOVE TODAY-JOUR  TO 5-J
              MOVE 0 TO LNK-NUM.
           PERFORM DIS-HE-08.
       AVANT-9.
           IF DATE-6 < DATE-5
           OR DATE-5 = 0
              MOVE DATE-5 TO DATE-6
           END-IF.
           IF DATE-5 > 0
           MOVE 08600000 TO LNK-POSITION
           CALL "0-GDATE" USING DATE-6
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           PERFORM DIS-HE-09.

       AVANT-10.
           MOVE 09300000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-7
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO 7-A
              MOVE TODAY-MOIS  TO 7-M
              MOVE TODAY-JOUR  TO 7-J
              MOVE 0 TO LNK-NUM.
           PERFORM DIS-HE-10.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.
           IF REG-PERSON > 0 
           AND PRES-ANNEE = 0
               MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.
           MOVE 0 TO IDX-1.
           MOVE 13 TO SAVE-KEY.
           PERFORM NEXT-FAMILLE THRU NEXT-FAMILLE-END.

       APRES-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN  13 CONTINUE
           WHEN  OTHER PERFORM NEXT-REGIS
              PERFORM NEXT-FAMILLE THRU NEXT-FAMILLE-END
           END-EVALUATE.                     
           PERFORM AFFICHAGE-DETAIL.

       APRES-3.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-PAYS
           WHEN  5 CALL "1-PAYS" USING LINK-V
                   IF LNK-TEXT NOT = SPACES
                      MOVE LNK-TEXT TO PAYS-CODE
                   END-IF
                   CANCEL "1-PAYS"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
           WHEN 2 CALL "2-PAYS" USING LINK-V PAYS-RECORD
                  CANCEL "2-PAYS"
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL.
           PERFORM DIS-HE-03.

           
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE "N" TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY
            WHEN  5 PERFORM TRANSMET
                    MOVE 1 TO DECISION 
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       
       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD A-N FAKE-KEY.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD S-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           DISPLAY PR-NOM LINE 3 POSITION 47 SIZE 33.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 48 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 3 POSITION COL-IDX SIZE IDX LOW.
        
           PERFORM NEXT-FAMILLE THRU NEXT-FAMILLE-END.

       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.

       DIS-HE-03.
           DISPLAY PAYS-CODE  LINE 5 POSITION 30.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.
           DISPLAY PAYS-NOM  LINE 5 POSITION 40.

       DIS-HE-04.
           MOVE 1-A     TO HE-AA.
           MOVE 1-M     TO HE-MM.
           MOVE 1-J     TO HE-JJ.
           DISPLAY HE-DATE LINE 6 POSITION 30.

       DIS-HE-05.
           MOVE 2-A     TO HE-AA.
           MOVE 2-M     TO HE-MM.
           MOVE 2-J     TO HE-JJ.
           DISPLAY HE-DATE LINE 6 POSITION 60.
       DIS-HE-06.
           MOVE 3-A     TO HE-AA.
           MOVE 3-M     TO HE-MM.
           MOVE 3-J     TO HE-JJ.
           DISPLAY HE-DATE LINE 7 POSITION 30.
       DIS-HE-07.
           MOVE 4-A     TO HE-AA.
           MOVE 4-M     TO HE-MM.
           MOVE 4-J     TO HE-JJ.
           DISPLAY HE-DATE LINE 7 POSITION 60.
       DIS-HE-08.
           MOVE 5-A     TO HE-AA.
           MOVE 5-M     TO HE-MM.
           MOVE 5-J     TO HE-JJ.
           DISPLAY HE-DATE LINE 8 POSITION 30.
       DIS-HE-09.
           MOVE 6-A     TO HE-AA.
           MOVE 6-M     TO HE-MM.
           MOVE 6-J     TO HE-JJ.
           DISPLAY HE-DATE LINE 8 POSITION 60.
       DIS-HE-10.
           MOVE 7-A     TO HE-AA.
           MOVE 7-M     TO HE-MM.
           MOVE 7-J     TO HE-JJ.
           DISPLAY HE-DATE LINE 9 POSITION 30.
       DIS-HE-END.
       NEXT-PAYS.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD EXC-KEY.

       TRANSMET.
           INITIALIZE ATT-RECORD.
           MOVE PR-NOM      TO ATT-NOMPERS.
           MOVE PR-PRENOM   TO ATT-PRENPERS.
           MOVE REG-PERSON TO HE-ZN.
           MOVE 51 TO IDX.
           STRING HE-ZP DELIMITED BY SIZE INTO 
           ATT-NOMPERS POINTER IDX ON OVERFLOW CONTINUE END-STRING.

           MOVE PR-NAISS-A  TO HS-A.
           MOVE PR-NAISS-M  TO HS-M.
           MOVE PR-NAISS-J  TO HS-J.
           MOVE PR-SNOCS    TO HS-S.
           MOVE HE-SNOCS    TO ATT-MATPERS.


           MOVE 1 TO IDX.
           STRING PR-MAISON DELIMITED BY " " INTO ATT-DOM1PERS
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-RUE DELIMITED BY "   "  INTO ATT-DOM1PERS
           WITH POINTER IDX.
               
           MOVE 1 TO IDX.
           STRING PR-PAYS DELIMITED BY " "  INTO ATT-DOM2PERS
           WITH POINTER IDX.
           ADD 1 TO IDX.
           IF PR-PAYS = "D" OR "F" OR "CZ" OR "I"
              STRING PR-CP5 DELIMITED BY " "  INTO ATT-DOM2PERS
              WITH POINTER IDX
           ELSE
              STRING PR-CP4 DELIMITED BY " "  INTO ATT-DOM2PERS
              WITH POINTER IDX
           END-IF.
           ADD 1 TO IDX.
           STRING PR-LOCALITE DELIMITED BY SIZE INTO ATT-DOM2PERS
           WITH POINTER IDX.

           MOVE CON-DEBUT-A TO HE-A.
           MOVE CON-DEBUT-M TO HE-M.
           MOVE CON-DEBUT-J TO HE-J.
           MOVE HP-DATE     TO ATT-ENTSERV.

           MOVE 1-A     TO HE-A.
           MOVE 1-M     TO HE-M.
           MOVE 1-J     TO HE-J.
           MOVE HP-DATE TO ATT-BENCCDU.
           MOVE 2-A     TO HE-A.
           MOVE 2-M     TO HE-M.
           MOVE 2-J     TO HE-J.
           MOVE HP-DATE TO ATT-BENCCAU.
           MOVE 3-A     TO HE-A.
           MOVE 3-M     TO HE-M.
           MOVE 3-J     TO HE-J.
           MOVE HP-DATE TO ATT-CONPAYDU.
           MOVE 4-A     TO HE-A.
           MOVE 4-M     TO HE-M.
           MOVE 4-J     TO HE-J.
           MOVE HP-DATE TO ATT-CONPAYAU.
           MOVE 5-A     TO HE-A.
           MOVE 5-M     TO HE-M.
           MOVE 5-J     TO HE-J.
           MOVE HP-DATE TO ATT-CONSSDU.
           MOVE 6-A     TO HE-A.
           MOVE 6-M     TO HE-M.
           MOVE 6-J     TO HE-J.
           MOVE HP-DATE TO ATT-CONSSAU.
           MOVE 7-A     TO HE-A.
           MOVE 7-M     TO HE-M.
           MOVE 7-J     TO HE-J.
           MOVE HP-DATE TO ATT-ASSVOLAU.

           MOVE PAYS-NOM TO ATT-PAYSDESI.
           MOVE FR-NOM   TO ATT-EMPLADR(2).

           MOVE 1 TO IDX.
           STRING FR-PAYS DELIMITED BY " "  INTO ATT-EMPLADR(3)
           WITH POINTER IDX.
           ADD 1 TO IDX.
           MOVE FR-CODE-POST TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE INTO ATT-EMPLADR(3)
           WITH POINTER IDX.
           ADD 2 TO IDX.
           STRING FR-LOCALITE DELIMITED BY " "  INTO ATT-EMPLADR(3)
           WITH POINTER IDX.
           MOVE 1 TO IDX.
           STRING FR-RUE DELIMITED BY "   "  INTO ATT-EMPLADR(4)
              WITH POINTER IDX.
           ADD 1 TO IDX.
           IF IDX < 39
              STRING FR-MAISON DELIMITED BY "  " INTO ATT-EMPLADR(4)
              WITH POINTER IDX.
           MOVE 0 TO IDX-4.
           PERFORM GOSSES VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           MOVE 0 TO LNK-VAL.
           CALL "ATT" USING LINK-V ATT-RECORD.
           MOVE 99 TO LNK-VAL.
           CALL "ATT" USING LINK-V ATT-RECORD.
           MOVE 0 TO LNK-VAL.
           CANCEL "ATT".
           
       
       AFFICHAGE-ECRAN.
           MOVE 1209 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE 0 TO LNK-PRESENCE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".
       

       NEXT-FAMILLE.
           MOVE 0 TO IDX-1
           MOVE 13 TO SAVE-KEY
           INITIALIZE FAM-RECORD HE-SEL.
           COMPUTE LIN-IDX = 11.
           PERFORM CLEAN-SCREEN.
       NEXT-FAMILLE-1.
           CALL "6-FAMIL" USING LINK-V REG-RECORD FAM-RECORD SAVE-KEY.
           IF FAM-NOM = SPACES
              GO NEXT-FAMILLE-END.
           ADD 1 TO IDX-1.
           MOVE FAM-NOM         TO H-NOM(IDX-1).
           MOVE FAM-PRENOM      TO H-PRENOM(IDX-1).
           MOVE FAM-NAISS-A     TO HE-AA.
           MOVE FAM-NAISS-M     TO HE-MM.
           MOVE FAM-NAISS-J     TO HE-JJ.
           MOVE HE-DATE         TO H-DATE(IDX-1).
           PERFORM AFFICHAGE-LIGNE.
           MOVE 66 TO SAVE-KEY.
           IF IDX-1 < 6
              GO NEXT-FAMILLE-1.
       NEXT-FAMILLE-END.
           COMPUTE LIN-IDX = IDX-1 + 12.
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX BY 1 
           UNTIL LIN-IDX > 22.

       AVANT-ALL.
           COMPUTE IDX-1 = INDICE-ZONE - 10.
           COMPUTE LIN-IDX = IDX-1 + 10.
           IF H-NOM(IDX-1) NOT = SPACES
           ACCEPT H-S(IDX-1)
             LINE LIN-IDX POSITION 55 SIZE 1
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "UPPER"
             ON EXCEPTION EXC-KEY CONTINUE.
       AVANT-ALL-END.
           EXIT.

       APRES-YN.
           COMPUTE IDX-1 = INDICE-ZONE - 10.
           IF H-S(IDX-1) = "Y" OR "J" OR "O" OR "N" OR " "
                MOVE 0 TO INPUT-ERROR
           ELSE
                MOVE SPACES TO H-S(IDX-1)
                MOVE 1 TO INPUT-ERROR.

       AFFICHAGE-LIGNE.
           COMPUTE LIN-IDX = IDX-1 + 10.
           PERFORM CLEAN-SCREEN.
           DISPLAY FAM-NOM LINE LIN-IDX POSITION 5.
           DISPLAY FAM-PRENOM LINE LIN-IDX POSITION 30.
           DISPLAY H-S(IDX-1) LINE LIN-IDX POSITION 55.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

           
       GOSSES.
           IF H-S(IDX) NOT = "N" 
              ADD 1 TO IDX-4
              MOVE H-NOM(IDX-4)    TO ATT-NOMACC(IDX-4)
              MOVE H-PRENOM(IDX-4) TO ATT-PNOMACC(IDX-4)
              MOVE H-DATE(IDX-4)   TO ATT-NAISACC(IDX-4)
           END-IF.
