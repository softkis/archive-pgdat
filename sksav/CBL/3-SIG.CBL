      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-SIG IMPRESSION DETAIL SIGNALETIQUE PERSONNE �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-SIG.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".
           COPY "TRIPR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80       ".
       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGIME.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "FICHE.REC".
           COPY "BANQP.REC".
           COPY "BANQUE.REC".
           COPY "CONTRAT.REC".
           COPY "CALEN.REC".
           COPY "CCOL.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "METIER.REC".
           COPY "EQUIPE.REC".
           COPY "DIVISION.REC".
           COPY "MESSAGE.REC".
           COPY "CONTYPE.REC".
           COPY "POCL.REC".
           COPY "PAYS.REC".
           COPY "BAREME.REC".
           COPY "BARDEF.REC".
           COPY "POINTS.REC".
           COPY "AGENCE.REC".


       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".SIG".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  CHOIX                 PIC 99 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-SIG.
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-SORT
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-2
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           PERFORM PREPARATION.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 1 TO COUNTER
           END-IF.
           PERFORM READ-FORM.
           PERFORM FILL-FILES VARYING IDX-4 FROM 1 BY 1 UNTIL
                                      IDX-4 > 61.
           PERFORM TRANSMET.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           MOVE CAR-METIER TO MET-CODE
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.

       PREPARATION.
           INITIALIZE CON-RECORD BQP-RECORD FICHE-RECORD SAVE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL" USING LINK-V CCOL-RECORD.
           MOVE CCOL-CALENDRIER TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD SAVE-KEY.
           MOVE CAR-REGIME TO REGIME-NUMBER.
           CALL "6-REGIME" USING LINK-V REGIME-RECORD FAKE-KEY.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD FAKE-KEY.
           MOVE CAR-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           MOVE CAR-STATUT TO STAT-CODE.
           CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.


       FILL-FILES.
           EVALUATE IDX-4
      * PERSONNE
               WHEN  1 MOVE REG-PERSON         TO VH-00
                       MOVE  9 TO LIN-NUM 
                       MOVE 26 TO COL-NUM
                       MOVE  6 TO CAR-NUM
                       MOVE  0 TO DEC-NUM
      *        WHEN  2 MOVE REG-MATCHCODE      TO ALPHA-TEXTE
      *                MOVE 10 TO LIN-NUM 
      *                MOVE 21 TO COL-NUM
               WHEN  3 MOVE 13 TO LIN-NUM 
                       MOVE 21 TO COL-NUM
                       CALL "4-PRLOC" USING LINK-V PR-RECORD
                       MOVE LNK-TEXT TO ALPHA-TEXTE
               WHEN  4 MOVE 13 TO LIN-NUM 
                       MOVE 35 TO COL-NUM
                       CALL "4-PRRUE" USING LINK-V PR-RECORD
                       MOVE LNK-TEXT TO ALPHA-TEXTE
               WHEN  5 MOVE PR-LOCALITE TO ALPHA-TEXTE
                       MOVE 14 TO LIN-NUM 
                       MOVE 35 TO COL-NUM
               WHEN  6 MOVE PR-NOM TO ALPHA-TEXTE
                       MOVE  9 TO LIN-NUM 
                       MOVE 35 TO COL-NUM
               WHEN  7 MOVE PR-PRENOM TO ALPHA-TEXTE
                       MOVE 10 TO LIN-NUM 
                       MOVE 35 TO COL-NUM
               WHEN  8 MOVE PR-NOM-JF TO ALPHA-TEXTE
                       MOVE 11 TO LIN-NUM 
                       MOVE 54 TO COL-NUM
               WHEN  9 MOVE PR-PRENOM-MARI TO ALPHA-TEXTE
                       MOVE 11 TO LIN-NUM 
                       MOVE 21 TO COL-NUM
               WHEN 10 MOVE PR-NAISS-A         TO VH-00
                       MOVE 20 TO LIN-NUM 
                       MOVE 27 TO COL-NUM
                       MOVE  4 TO CAR-NUM
                       PERFORM FILL-FORM
                       ADD   1 TO COL-NUM
                       MOVE PR-NAISS-M TO VH-00
                       MOVE  2 TO CAR-NUM
                       PERFORM FILL-FORM
                       ADD   1 TO COL-NUM
                       MOVE PR-NAISS-J TO VH-00
                       MOVE  2 TO CAR-NUM
                       PERFORM FILL-FORM
                       ADD   1 TO COL-NUM
                       MOVE PR-SNOCS TO VH-00
                       MOVE  3 TO CAR-NUM
               WHEN 11 MOVE "M" TO ALPHA-TEXTE
                       IF PR-CODE-SEXE = 2  
                          MOVE "F" TO ALPHA-TEXTE
                       END-IF
                       MOVE 12 TO LIN-NUM 
                       MOVE 46 TO COL-NUM
               WHEN 12 MOVE PR-NATIONALITE    TO ALPHA-TEXTE
                       MOVE 18 TO LIN-NUM 
                       MOVE 27 TO COL-NUM
               WHEN 13 MOVE PR-LANGUAGE       TO ALPHA-TEXTE
                       MOVE 12 TO LIN-NUM 
                       MOVE 21 TO COL-NUM
               WHEN 14 MOVE PR-LIEU-NAISSANCE TO ALPHA-TEXTE
                       MOVE 17 TO LIN-NUM 
                       MOVE 27 TO COL-NUM
               WHEN 15 MOVE PR-TELEPHONE TO ALPHA-TEXTE
                       MOVE 16 TO LIN-NUM 
                       MOVE 21 TO COL-NUM
               WHEN 16 MOVE PR-TELEPHONE-INT TO ALPHA-TEXTE
                       MOVE 16 TO LIN-NUM 
                       MOVE 55 TO COL-NUM
               WHEN 18 MOVE PR-MALADIE-AGENCE TO AG-KEY
                       CALL "6-AGENCE" USING LINK-V AG-RECORD FAKE-KEY
                       MOVE AG-NOM TO ALPHA-TEXTE
                       MOVE 24 TO LIN-NUM 
                       MOVE 35 TO COL-NUM
               WHEN 19 MOVE REG-ANCIEN-J TO VH-00
                       MOVE 22 TO LIN-NUM 
                       MOVE 21 TO COL-NUM
                       MOVE  2 TO CAR-NUM
                       PERFORM FILL-FORM
                       ADD   1 TO COL-NUM
                       MOVE REG-ANCIEN-M TO VH-00
                       MOVE  2 TO CAR-NUM
                       PERFORM FILL-FORM
                       ADD   1 TO COL-NUM
                       MOVE REG-ANCIEN-A TO VH-00
                       MOVE  4 TO CAR-NUM

               WHEN 20 MOVE CON-DEBUT-A        TO VH-00
                       MOVE 21 TO LIN-NUM 
                       MOVE 27 TO COL-NUM
                       MOVE  4 TO CAR-NUM
               WHEN 21 MOVE CON-DEBUT-M        TO VH-00
                       MOVE 21 TO LIN-NUM 
                       MOVE 24 TO COL-NUM
                       MOVE  2 TO CAR-NUM
               WHEN 22 MOVE CON-DEBUT-J        TO VH-00
                       MOVE 21 TO LIN-NUM 
                       MOVE 21 TO COL-NUM
                       MOVE  2 TO CAR-NUM
               WHEN 23 MOVE CON-FIN-A        TO VH-00
                       MOVE 21 TO LIN-NUM 
                       MOVE 60 TO COL-NUM
                       MOVE  4 TO CAR-NUM
               WHEN 24 MOVE CON-FIN-M        TO VH-00
                       MOVE 21 TO LIN-NUM 
                       MOVE 57 TO COL-NUM
                       MOVE  2 TO CAR-NUM
               WHEN 25 MOVE CON-FIN-J        TO VH-00
                       MOVE 21 TO LIN-NUM 
                       MOVE 54 TO COL-NUM
                       MOVE  2 TO CAR-NUM
      * FICHE
               WHEN 26 MOVE FICHE-ANNEE      TO VH-00
                       MOVE 50 TO LIN-NUM 
                       MOVE 30 TO COL-NUM
                       MOVE  4 TO CAR-NUM
               WHEN 27 MOVE FICHE-MOIS TO LNK-NUM
                       PERFORM MOIS-NOM-1
                       MOVE 41 TO COL-NUM
               WHEN 28 MOVE FICHE-COMMUNE    TO ALPHA-TEXTE
                       MOVE 51 TO LIN-NUM 
                       MOVE 26 TO COL-NUM
               WHEN 29 MOVE FICHE-NUMERO     TO ALPHA-TEXTE
                       MOVE 52 TO LIN-NUM 
                       MOVE 26 TO COL-NUM
               WHEN 30 MOVE FICHE-ETAT-CIVIL TO ALPHA-TEXTE
                       MOVE 53 TO LIN-NUM 
                       MOVE 17 TO COL-NUM
               WHEN 31 MOVE FICHE-TAUX       TO VH-00
                       MOVE 53 TO LIN-NUM 
                       MOVE 70 TO COL-NUM
                       MOVE  2 TO CAR-NUM
                       MOVE  2 TO DEC-NUM
               WHEN 32 MOVE FICHE-CLASSE     TO ALPHA-TEXTE
                       MOVE 53 TO LIN-NUM 
                       MOVE 40 TO COL-NUM
               WHEN 33 MOVE FICHE-ABAT-1     TO VH-00
                       MOVE 54 TO LIN-NUM 
                       MOVE 36 TO COL-NUM
                       MOVE  4 TO CAR-NUM
                       MOVE  2 TO DEC-NUM
               WHEN 34 MOVE FICHE-ABAT-2     TO VH-00
                       MOVE 55 TO LIN-NUM 
                       MOVE 36 TO COL-NUM
                       MOVE  4 TO CAR-NUM
                       MOVE  2 TO DEC-NUM
               WHEN 35 MOVE FICHE-ABAT-3     TO VH-00
                       MOVE 56 TO LIN-NUM 
                       MOVE 36 TO COL-NUM
                       MOVE  4 TO CAR-NUM
                       MOVE  2 TO DEC-NUM
               WHEN 36 MOVE FICHE-ABAT-4     TO VH-00
                       MOVE 57 TO LIN-NUM 
                       MOVE 36 TO COL-NUM
                       MOVE  4 TO CAR-NUM
                       MOVE  2 TO DEC-NUM
               WHEN 37 MOVE FICHE-ABAT-5     TO VH-00
                       MOVE 58 TO LIN-NUM 
                       MOVE 36 TO COL-NUM
                       MOVE  4 TO CAR-NUM
                       MOVE  2 TO DEC-NUM
      * CARRIERE
               WHEN 38 MOVE CAR-ANNEE      TO VH-00
                       MOVE 30 TO LIN-NUM 
                       MOVE 30 TO COL-NUM
                       MOVE  4 TO CAR-NUM
               WHEN 39 MOVE CAR-MOIS TO LNK-NUM
                       PERFORM MOIS-NOM-1
                       MOVE 41 TO COL-NUM
             WHEN 40   MOVE STAT-NOM TO ALPHA-TEXTE
                       MOVE 31 TO LIN-NUM 
                       MOVE 41 TO COL-NUM
             WHEN 41 MOVE CAR-STATEC TO ALPHA-TEXTE
                       MOVE 32 TO LIN-NUM 
                       MOVE 36 TO COL-NUM
             WHEN 42 MOVE CAR-IN-ACTIF     TO ALPHA-TEXTE
                       MOVE 32 TO LIN-NUM 
                       MOVE 76 TO COL-NUM
             WHEN 44   MOVE REGIME-NOM TO ALPHA-TEXTE
                       MOVE 44 TO LIN-NUM 
                       MOVE 19 TO COL-NUM
                       MOVE 41 TO COL-NUM
               WHEN 45 MOVE CAR-CONGE TO LNK-NUM
                       MOVE "CG" TO LNK-AREA
                       CALL "0-GMESS" USING LINK-V
                       MOVE LNK-TEXT TO ALPHA-TEXTE
                       MOVE 41 TO LIN-NUM 
                       MOVE 41 TO COL-NUM
               WHEN 46 MOVE CAR-TARIF-KM     TO VH-00
                       MOVE 42 TO LIN-NUM 
                       MOVE 36 TO COL-NUM
                       MOVE  1 TO CAR-NUM
               WHEN 47 MOVE  5 TO CAR-NUM
                       COMPUTE DEC-NUM = 4 - 2 * CAR-HOR-MEN  
                       PERFORM CALC-SAL
                       MOVE 36 TO LIN-NUM 
                       MOVE 27 TO COL-NUM
               WHEN 48 COMPUTE LNK-NUM = 111 + 3 * CAR-HOR-MEN 
                       MOVE 41 TO COL-NUM
                       MOVE "AA" TO LNK-AREA
                       CALL "0-GMESS" USING LINK-V
                       MOVE LNK-TEXT TO ALPHA-TEXTE
               WHEN 49 MOVE CAR-DEPLACEMENT  TO VH-00
                       MOVE 42 TO LIN-NUM 
                       MOVE 73 TO COL-NUM
                       MOVE  4 TO CAR-NUM
               WHEN 50 MOVE CAR-GRADE        TO VH-00
                       MOVE 37 TO LIN-NUM 
                       MOVE 34 TO COL-NUM
                       MOVE  3 TO CAR-NUM
               WHEN 51 MOVE CAR-ECHELON      TO VH-00
                       MOVE 38 TO LIN-NUM 
                       MOVE 35 TO COL-NUM
                       MOVE  2 TO CAR-NUM
               WHEN 52 MOVE CAR-JOURS-CONGE  TO VH-00
                       IF VH-00 = 0
                          MOVE CCOL-CONGE-STANDARD TO VH-00
                       END-IF
                       MOVE 41 TO LIN-NUM 
                       MOVE 35 TO COL-NUM
                       MOVE  2 TO CAR-NUM
                       MOVE  1 TO DEC-NUM
               WHEN 53 MOVE CAR-HRS-JOUR     TO VH-00
                       MOVE 39 TO LIN-NUM 
                       MOVE 35 TO COL-NUM
                       MOVE  2 TO CAR-NUM
                       MOVE  2 TO DEC-NUM
               WHEN 54 MOVE CAR-JRS-SEMAINE  TO VH-00
                       MOVE 39 TO LIN-NUM 
                       MOVE 76 TO COL-NUM
                       MOVE  1 TO CAR-NUM
               WHEN 55 MOVE CAR-HRS-MOIS     TO VH-00
                       MOVE 40 TO LIN-NUM 
                       MOVE 34 TO COL-NUM
                       MOVE  3 TO CAR-NUM
                       MOVE  1 TO DEC-NUM

               WHEN 56 MOVE BQP-BANQUE TO ALPHA-TEXTE BQ-CODE


                       MOVE 64 TO LIN-NUM 
                       MOVE 5 TO COL-NUM
                       PERFORM FILL-FORM
                       CALL "6-BANQUE" USING LINK-V BQ-RECORD FAKE-KEY
                       MOVE BQ-SWIFT TO ALPHA-TEXTE
                       ADD 5 TO COL-NUM
                       PERFORM FILL-FORM

                       MOVE BQP-PART(1) TO ALPHA-TEXTE
                       MOVE 35 TO COL-NUM
                       PERFORM FILL-FORM
                       MOVE BQP-PART(2) TO ALPHA-TEXTE
                       ADD 1 TO COL-NUM
                       PERFORM FILL-FORM
                       MOVE BQP-PART(3) TO ALPHA-TEXTE
                       ADD 1 TO COL-NUM
                       PERFORM FILL-FORM
                       MOVE BQP-PART(4) TO ALPHA-TEXTE
                       ADD 1 TO COL-NUM
                       PERFORM FILL-FORM
                       MOVE BQP-PART(5) TO ALPHA-TEXTE
                       ADD 1 TO COL-NUM
                       PERFORM FILL-FORM
                       ADD 1 TO COL-NUM
                       MOVE BQP-PART(6) TO ALPHA-TEXTE
                       PERFORM FILL-FORM
      * DEPARTMENT
               WHEN 57 MOVE COUT-NOM TO ALPHA-TEXTE
                       MOVE 33 TO LIN-NUM 
                       MOVE 41 TO COL-NUM
      * EQUIPE
               WHEN 58 MOVE EQ-NOM TO ALPHA-TEXTE
                       MOVE 34 TO LIN-NUM 
                       MOVE 41 TO COL-NUM
      * EMPLOI
               WHEN 59 MOVE 1 TO IDX-1
                       STRING MET-NOM(PR-CODE-SEXE) DELIMITED BY "  "  
                       INTO ALPHA-TEXTE WITH POINTER IDX-1
                       ADD 2 TO IDX-1
                       STRING CAR-POSITION DELIMITED BY "  "  INTO 
                       ALPHA-TEXTE WITH POINTER IDX-1

                       MOVE 35 TO LIN-NUM 
                       MOVE 5 TO COL-NUM
      * FIRME 
               WHEN 60 MOVE FR-KEY              TO VH-00
                       MOVE  2 TO LIN-NUM 
                       MOVE  5 TO COL-NUM
                       MOVE  4 TO CAR-NUM
               WHEN 61 MOVE FR-NOM  TO ALPHA-TEXTE
                       MOVE  2 TO LIN-NUM 
                       MOVE 11 TO COL-NUM
           END-EVALUATE.
           PERFORM FILL-FORM.

       PAGE-DATE.
           MOVE  6 TO LIN-NUM .
           PERFORM MOIS-NOM.
           MOVE 17 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 30 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  4 TO LIN-NUM .
           ADD 1 TO PAGE-NUMBER.
           MOVE  4 TO CAR-NUM.
           MOVE 73 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  6 TO LIN-NUM.
           MOVE 67 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 70 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 73 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       CALC-SAL.
           MOVE 0 TO VH-00 SAVE-KEY.
           IF CAR-POURCENT = 0
              MOVE 100 TO CAR-POURCENT 
           END-IF.
           IF CAR-BAREME NOT = SPACES
           AND CAR-ECHELON > 0
             MOVE CAR-BAREME TO BDF-CODE
             MOVE CAR-ANNEE  TO BAR-ANNEE
             MOVE CAR-MOIS   TO BAR-MOIS
             CALL "6-BARDEF" USING LINK-V BDF-RECORD SAVE-KEY
             INITIALIZE BAR-RECORD
             MOVE CAR-BAREME TO BAR-BAREME
             MOVE CAR-GRADE  TO BAR-GRADE
             MOVE CAR-ANNEE  TO BAR-ANNEE
             MOVE CAR-MOIS   TO BAR-MOIS
             CALL "6-BAREME" USING LINK-V BAR-RECORD SAVE-KEY
             IF BDF-POINTS > 0
                INITIALIZE PTS-RECORD
                MOVE CAR-ANNEE TO PTS-ANNEE
                MOVE CAR-MOIS  TO PTS-MOIS
                CALL "6-POINT" USING LINK-V PTS-RECORD
                COMPUTE VH-00 = BAR-POINTS(CAR-ECHELON) 
                                * PTS-I100(BDF-POINTS)
                                * MOIS-IDX(LNK-MOIS) + ,00009
             ELSE
                COMPUTE VH-00 = (BAR-ECHELON-I100(CAR-ECHELON)
                              + BAR-ECHELON-I100(80))
                                * MOIS-IDX(LNK-MOIS) + ,00009
                IF CAR-HOR-MEN = 0
                   COMPUTE VH-00 = VH-00 / 173
                ELSE
                   ADD ,005 TO VH-00
                END-IF
             END-IF
           ELSE
              IF CAR-INDEXE NOT = "N"
                 IF CAR-SAL-100 = 0
                    COMPUTE VH-00 = MOIS-SALMIN(LNK-MOIS) 
                                  * MOIS-IDX(LNK-MOIS)
                    + ,00009
                    IF CAR-HOR-MEN = 0
                       COMPUTE VH-00 = VH-00 / 173
                    END-IF
                 ELSE
                    COMPUTE VH-00 = CAR-SAL-100 * MOIS-IDX(LNK-MOIS)
                    + ,00009
                 END-IF
              END-IF
           END-IF.
           COMPUTE VH-00 = VH-00 / 100 * CAR-POURCENT.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1203 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF COUNTER > 0 
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE
           END-IF.
           CANCEL "P080".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XSORT.CPY".
