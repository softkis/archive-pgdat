      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-CSD RECHERCHE CODES SALAIRE CRITERES      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-CSD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CODTXT.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.

       01  CHOIX                 PIC 9(10).
       01  COMPTEUR              PIC 99.
       01  ARROW                 PIC X VALUE ">".

       01  CRITERES              PIC X(50) VALUE SPACES.
       01  CRITERES-1            PIC X(50).
       01  HLP-CRIT REDEFINES CRITERES-1.
           03 CRIT               PIC X OCCURS 50.

       01  CRITERES-M           PIC X(470).
       01  TST                  PIC X(4).

       01  ITEMS.
           02 ITEM-A OCCURS 10.
              03 ITEM-IDX        PIC 99.
              03 ITEM            PIC X(10).
              03 ITEM-A REDEFINES ITEM.
                 04 ITEM-2       PIC X(2).
                 04 ITEM-F2      PIC X(8).
              03 ITEM-B REDEFINES ITEM.
                 04 ITEM-3       PIC X(3).
                 04 ITEM-F3      PIC X(7).
              03 ITEM-C REDEFINES ITEM.
                 04 ITEM-4       PIC X(4).
                 04 ITEM-F4      PIC X(6).
              03 ITEM-D REDEFINES ITEM.
                 04 ITEM-5       PIC X(5).
                 04 ITEM-F5      PIC X(5).
              03 ITEM-E REDEFINES ITEM.
                 04 ITEM-6       PIC X(6).
                 04 ITEM-F6      PIC X(4).
              03 ITEM-F REDEFINES ITEM.
                 04 ITEM-7       PIC X(7).
                 04 ITEM-F7      PIC X(3).
              03 ITEM-G REDEFINES ITEM.
                 04 ITEM-8       PIC X(8).
                 04 ITEM-F8      PIC X(2).
              03 ITEM-H REDEFINES ITEM.
                 04 ITEM-9       PIC X(9).
                 04 ITEM-F9      PIC X(1).



       01 HE-CSTXT.
          02 H-R PIC X(470) OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-TEMP.
              03 HE-JJ    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-MM    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-AA    PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "CODTXT.LNK".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       START-DISPLAY SECTION.
             
       START-2-CSD.

           PERFORM AFFICHAGE-ECRAN .
           INITIALIZE ITEMS.
           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           PERFORM CRITERES.
           MOVE 4 TO LIN-IDX.
           INITIALIZE CTX-RECORD IDX-1 COMPTEUR CHOIX.
           PERFORM READ-CTX THRU READ-CTX-END.
           IF EXC-KEY = 52 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-CTX.
           CALL "6-CSTXT" USING LINK-V CTX-RECORD NX-KEY.
           IF CTX-NOM = SPACES
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 52
                 GO READ-CTX-END
              END-IF
              IF EXC-KEY = 65 
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-CSTXT IDX-1 
                 GO READ-CTX
              END-IF
              GO READ-CTX-END.
           MOVE CTX-RECORD TO CRITERES-M.
           INSPECT CRITERES-M CONVERTING
           "abcdefghijklmnopqrstuvwxyz뇪굤닀뀑뙎봺뼏꽇쉸,.;:" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZCEEEEEAIIOOUUUAOUA    ".
           MOVE 0 TO INPUT-ERROR IDX-3.
           PERFORM TEST-CRIT THRU TEST-CRIT-END.
           IF INPUT-ERROR > 0
              GO READ-CTX
           END-IF.
            
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 52
                 GO READ-CTX-END
              END-IF
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-CSTXT IDX-1 
                 GO READ-CTX
              END-IF
              IF CHOIX NOT = 0
                 GO READ-CTX-END
              END-IF
           END-IF.
           GO READ-CTX.
       READ-CTX-END.
           IF CHOIX = 0
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82 AND NOT = 68 AND NOT = 52
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX COMPTEUR IDX-1.
           MOVE CTX-RECORD TO H-R(IDX-1).
           DISPLAY CTX-NUMBER   LINE LIN-IDX POSITION 1.
           DISPLAY CTX-NOM    LINE LIN-IDX POSITION 6.
           DISPLAY CTX-COMMENTAIRE LINE LIN-IDX POSITION 30 SIZE 50 LOW.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0000530000 TO EXC-KFR (11).
           MOVE 5600000000 TO EXC-KFR (12).
           MOVE 0067680000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE TST.
           ACCEPT TST 
           LINE 3 POSITION 5 SIZE 1
           TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 82
              INITIALIZE LINK-RECORD
              EXIT PROGRAM
           END-IF.
           IF EXC-KEY = 68
              MOVE 13 TO EXC-KEY.

           DISPLAY SPACES LINE 24 POSITION 1 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY = 52 GO INTERRUPT-END.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF CHOIX NOT = 0
              MOVE CHOIX TO CTX-NUMBER
              MOVE 0 TO SAVE-KEY
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.

           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IDX-1.

       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IDX-1 + 4.
           MOVE ">" TO ARROW.
           ACCEPT ARROW
             LINE  LIN-IDX POSITION 5 SIZE 1
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           DISPLAY SPACES LINE LIN-IDX POSITION 5 SIZE 1.
           IF EXC-KEY = 82 
              MOVE H-R(IDX-1) TO LINK-RECORD
              EXIT PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN OTHER
                     MOVE H-R(IDX-1) TO LINK-RECORD
                     EXIT PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-R(IDX-1) = SPACES
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.

       CRITERES.
           ACCEPT CRITERES
           LINE 3 POSITION 25 SIZE 50
           TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 82
              INITIALIZE LINK-RECORD
              EXIT PROGRAM
           END-IF.
           IF CRITERES = SPACES
              GO CRITERES
           END-IF.
           PERFORM HOMOGENIZE.
           IF  IDX-2 = 1
           AND IDX-1 < 3
              GO CRITERES
           END-IF.


       HOMOGENIZE.
           MOVE CRITERES TO CRITERES-1.
           INSPECT CRITERES-1 CONVERTING
           "abcdefghijklmnopqrstuvwxyz뇪굤닀뀑뙎봺뼏꽇쉸,.;:?" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZCEEEEEAIIOOUUUAOUA     ".
           MOVE 1 TO IDX-1 IDX-2 IDX.
           PERFORM PROPER-E THRU PROPER-END.

        PROPER-E.
           IF CRIT(IDX) > SPACES 
           AND IDX-1 < 11
              STRING CRIT(IDX) DELIMITED BY SIZE INTO ITEM(IDX-2) WITH 
              POINTER IDX-1 ON OVERFLOW CONTINUE
           ELSE
              IF IDX-1 > 2
                 ADD 1 TO IDX-2
               END-IF
               MOVE 1 TO IDX-1
           END-IF.
           COMPUTE ITEM-IDX(IDX-2) = IDX-1 - 1.
           ADD 1 TO IDX.
           IF IDX < 51
           AND IDX-2 < 11
              GO PROPER-E.
        PROPER-END.
           EXIT.

        TEST-CRIT.
           ADD 1 TO IDX-3.
           IF ITEM-IDX(IDX-3) < 2
              GO TEST-CRIT-END
           END-IF.
           MOVE 0 TO IDX.
           EVALUATE ITEM-IDX(IDX-3)
              WHEN 2 INSPECT CRITERES-M TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-2(IDX-3)
              WHEN 3 INSPECT CRITERES-M TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-3(IDX-3)
              WHEN 4 INSPECT CRITERES-M TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-4(IDX-3)
              WHEN 5 INSPECT CRITERES-M TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-5(IDX-3)
              WHEN 6 INSPECT CRITERES-M TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-6(IDX-3)
              WHEN 7 INSPECT CRITERES-M TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-7(IDX-3)
              WHEN 8 INSPECT CRITERES-M TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-8(IDX-3)
              WHEN 9 INSPECT CRITERES-M TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-9(IDX-3)
              WHEN 10 INSPECT CRITERES-M TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM(IDX-3)
           END-EVALUATE.
           IF IDX > 460
              MOVE 1 TO INPUT-ERROR
              GO TEST-CRIT-END
           END-IF.
           IF IDX-3 < 10
              GO TEST-CRIT.
        TEST-CRIT-END.

       AFFICHAGE-ECRAN.
           MOVE 2130 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


