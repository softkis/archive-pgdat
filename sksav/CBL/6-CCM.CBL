      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 6-CCM MODULE GENERAL CENTRES DE COUT MULTIPLE�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-CCM.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CCM.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CCM.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "CCM.LNK".
           COPY "REGISTRE.REC".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD REG-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CCM.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-REG.
       
           IF NOT-OPEN = 0
              OPEN I-O CCM
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CCM-RECORD.
           MOVE FR-KEY      TO CCM-FIRME.
           MOVE REG-PERSON  TO CCM-PERSON.
           IF CCM-ANNEE = 0
              MOVE LNK-ANNEE TO CCM-ANNEE
              MOVE LNK-MOIS  TO CCM-MOIS
           END-IF.
           IF EXC-KEY = 99
              WRITE CCM-RECORD INVALID REWRITE CCM-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.
           IF EXC-KEY = 98
              DELETE CCM INVALID CONTINUE END-DELETE
              EXIT PROGRAM
           END-IF.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ CCM PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ CCM NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 0 PERFORM START-3
               READ CCM PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ CCM NO LOCK INVALID INITIALIZE CCM-REC-DET
                END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY     NOT = CCM-FIRME 
           OR REG-PERSON NOT = CCM-PERSON
              GO EXIT-1
           END-IF.
           MOVE CCM-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START CCM KEY < CCM-KEY INVALID GO EXIT-1.
       START-2.
           START CCM KEY > CCM-KEY INVALID GO EXIT-1.
       START-3.
           START CCM KEY <= CCM-KEY INVALID GO EXIT-1.


