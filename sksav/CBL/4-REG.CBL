      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-REG RECHERCHE REGISTRES PAR MATRICULE     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-REG.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "REGISTRE.REC".
           COPY "FIRME.REC".

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-4-REG.

           INITIALIZE REG-RECORD LNK-KEY LNK-PERSON.
           MOVE LNK-MATRICULE TO REG-MATRICULE.
           PERFORM READ-REG THRU READ-REG-END.

       READ-REG.
           CALL "6-REG" USING LINK-V REG-RECORD NX-KEY.
           IF REG-PERSON = 0
           OR REG-MATRICULE NOT = LNK-MATRICULE
              GO READ-REG-END
           END-IF.
           MOVE REG-FIRME TO FIRME-KEY
           CALL "6-FIRMES" USING LINK-V FIRME-RECORD "N" FAKE-KEY.
           IF FIRME-MATR = LNK-MATR
              MOVE REG-PERSON TO LNK-PERSON
              MOVE FIRME-KEY  TO LNK-KEY
              GO READ-REG-END
           END-IF.
           GO READ-REG.
       READ-REG-END.
           EXIT PROGRAM.