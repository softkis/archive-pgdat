      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-BAREME IMPORT / EXPORT  BAREMES           �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-BAREME.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "BAREME.FC".
           SELECT OPTIONAL TF-FILE ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.

           COPY "BAREME.FDE".

       FD  TF-FILE 
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.

      *  ENREGISTREMENT FICHIER DE TRANSFER ASCII BAREME
      
       01  TF-RECORD.
           02 TF-KEY.
              03 TF-BAREME       PIC X(10).
              03 TF-GRADE        PIC 999.
              03 TF-DATE.
                 04 TF-ANNEE     PIC 9999.
                 04 TF-MOIS      PIC 99.

           02 TF-KEY-1.  
              03 TF-BAREME-1     PIC X(10).
              03 TF-DATE-1.
                 04 TF-ANNEE-1   PIC 9999.
                 04 TF-MOIS-1    PIC 99.
              03 TF-GRADE-1      PIC 999.

           02 TF-REC-DET.
              03 TF-DESCRIPTION  PIC X(10).
              03 TF-POINT        PIC 9.
              03 TF-ECHELONS.
                 04 TF-ECHELON PIC 9(10) OCCURS 80.
              03 TF-STAMP.
                 04 TF-TIME.
                    05 TF-ST-ANNEE PIC 9999.
                    05 TF-ST-MOIS  PIC 99.
                    05 TF-ST-JOUR  PIC 99.
                    05 TF-ST-HEURE PIC 99.
                    05 TF-ST-MIN   PIC 99.
                 04 TF-USER        PIC X(10).


           

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
       01  CHOIX-MAX             PIC 99 VALUE 3.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PARMOD.REC".
           COPY "BARDEF.REC".

       01  NOT-OPEN              PIC 9 VALUE 0.
       01  BAR-DEF               PIC X(10).

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6).
           02 HE-Z2Z2 PIC ZZ,ZZ. 
           02 HE-Z6Z2 PIC Z(6),ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON BAREME TF-FILE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-BAREME.

           INITIALIZE PARMOD-RECORD BDF-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           OPEN I-O BAREME.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1  MOVE 0029000000 TO EXC-KFR (1)
                   MOVE 0000000065 TO EXC-KFR (13)
                   MOVE 6600000000 TO EXC-KFR (14)
                   
           WHEN 3  IF BDF-KEY NOT = SPACES
                      MOVE 0000000078 TO EXC-KFR(1)
                   ELSE
                      MOVE 1600000000 TO EXC-KFR(2)
                   END-IF
                   MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-PATH
           WHEN  3 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  3 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT BDF-KEY
             LINE  5 POSITION 30 SIZE 10 
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY CONTINUE.

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 10 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-1.
           EVALUATE EXC-KEY
            WHEN  2 CALL "2-BARDEF" USING LINK-V BDF-RECORD
                    MOVE BDF-KEY TO BAR-BAREME
                    CANCEL "2-BARDEF"
                    PERFORM AFFICHAGE-ECRAN 
            WHEN 13 PERFORM NEXT-BAREME
            WHEN 65 PERFORM NEXT-BAREME
            WHEN 66 PERFORM NEXT-BAREME
           END-EVALUATE.
           PERFORM AFFICHAGE-DETAIL.


       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN 5 PERFORM BAR-COPY THRU BAR-COPY-END
            WHEN 6 PERFORM READ-FILES THRU READ-FILES-END
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       NEXT-BAREME.
           CALL "6-BARDEF" USING LINK-V BDF-RECORD EXC-KEY.

       DIS-HE-01.
           DISPLAY BDF-KEY LINE 5 POSITION 30.
           DISPLAY BDF-NOM LINE 5 POSITION 45.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1044 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE BAREME.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       BAR-COPY.
           INITIALIZE BAR-RECORD.
           MOVE BDF-KEY TO BAR-BAREME.
           START BAREME KEY >= BAR-KEY INVALID
               GO BAR-COPY-END.

       BAR-COPY-1.
           READ BAREME NEXT AT END 
               GO BAR-COPY-END
           END-READ.
           IF BAR-BAREME NOT = BDF-KEY
               GO BAR-COPY-END.
           PERFORM FILL-FILES.
           GO BAR-COPY-1.
       BAR-COPY-END.
           PERFORM END-PROGRAM.

       FILL-FILES.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-FILE
              MOVE 1 TO NOT-OPEN
           END-IF.
           WRITE TF-RECORD FROM BAR-RECORD.

       READ-FILES.
           IF NOT-OPEN = 0
              OPEN INPUT TF-FILE
              MOVE 1 TO NOT-OPEN
           END-IF.
           READ TF-FILE AT END 
              GO READ-FILES-END
           END-READ.
           WRITE BAR-RECORD FROM TF-RECORD INVALID CONTINUE.
           GO READ-FILES.
       READ-FILES-END.
           PERFORM END-PROGRAM.

