      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-INDEX RECHERCHE INDEX RECENT              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-INDEX.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "INDEX.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "INDEX.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "INDEX.LNK".
       01  EXC-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON INDEX-VIE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-INDEX.
       
           IF NOT-OPEN = 0
              OPEN I-O INDEX-VIE
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO INDEX-RECORD.
           IF EXC-KEY = 99
              IF LNK-SQL = "Y" 
                 CALL "9-INDEX" USING LINK-V INDEX-RECORD EXC-KEY
              END-IF
              WRITE INDEX-RECORD INVALID REWRITE INDEX-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.

           IF INDEX-ANNEE = 0
              MOVE LNK-ANNEE TO INDEX-ANNEE
              MOVE LNK-MOIS  TO INDEX-MOIS
           END-IF.
           START INDEX-VIE KEY <= INDEX-KEY INVALID CONTINUE
              NOT INVALID 
              READ INDEX-VIE PREVIOUS NO LOCK
              AT END INITIALIZE INDEX-RECORD END-READ.

           MOVE INDEX-RECORD TO LINK-RECORD.
           
           EXIT PROGRAM.


      
