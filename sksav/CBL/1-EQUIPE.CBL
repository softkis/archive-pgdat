      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-EQUIPE GESTION DES EQUIPES                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-EQUIPE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "EQUIPE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "EQUIPE.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 7. 

       01   ECR-DISPLAY.
            02 HE-Z4 PIC ZZZZ.
            02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON EQUIPE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-1-EQUIPE.

           OPEN I-O EQUIPE.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0029000000 TO EXC-KFR (1)
                      MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
           WHEN 7     MOVE 0000000005 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2
           WHEN  3 PERFORM AVANT-DESCR 
           WHEN  4 PERFORM AVANT-DESCR
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN  9 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  7 PERFORM APRES-7.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT EQ-NUMBER
             LINE  3 POSITION 30 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT EQ-NOM
             LINE  4 POSITION 30 SIZE 50
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-DESCR.
           COMPUTE LIN-IDX = 2 + INDICE-ZONE.
           COMPUTE IDX = INDICE-ZONE - 2.
           ACCEPT EQ-TEXTE-IDX(IDX)
             LINE  LIN-IDX POSITION 30 SIZE 50
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *AVANT-VAL.
      *    COMPUTE LIN-IDX = 3 + INDICE-ZONE.
      *    COMPUTE IDX = INDICE-ZONE - 4.
      *    ACCEPT EQ-VAL(IDX)
      *      LINE  LIN-IDX POSITION 20 SIZE  8
      *      TAB UPDATE NO BEEP CURSOR  1
      *      ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE FR-KEY TO EQ-FIRME.
           EVALUATE EXC-KEY
           WHEN  65 thru 66 PERFORM NEXT-EQUIPE
           WHEN   2 CALL "2-EQUIPE" USING LINK-V EQ-RECORD
                    CANCEL "2-EQUIPE"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM DIS-HE-01 THRU DIS-HE-END
           WHEN   8 INITIALIZE EQ-RECORD.
           PERFORM READ-EQUIPE.
           PERFORM AFFICHAGE-DETAIL.

       APRES-7.
           MOVE FR-KEY TO EQ-FIRME.
           EVALUATE EXC-KEY 
            WHEN  5 CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO EQ-TIME
                    MOVE LNK-USER TO EQ-USER
                    IF LNK-SQL = "Y" 
                       CALL "9-EQUIPE" USING LINK-V EQ-RECORD WR-KEY 
                    END-IF
                    WRITE EQ-RECORD INVALID 
                        REWRITE EQ-RECORD
                    END-WRITE   
                    MOVE 1 TO DECISION
                    MOVE EQ-NUMBER TO LNK-VAL
            WHEN  8 DELETE EQUIPE INVALID CONTINUE END-DELETE
                    IF LNK-SQL = "Y" 
                       CALL "9-EQUIPE" USING LINK-V EQ-RECORD DEL-KEY 
                    END-IF
                INITIALIZE EQ-RECORD
                PERFORM AFFICHAGE-DETAIL
                MOVE 1 TO DECISION
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
               MOVE CHOIX-MAX TO INDICE-ZONE.
           

       READ-EQUIPE.
           MOVE EXC-KEY TO SAVE-KEY.
           MOVE 13 TO EXC-KEY.
           PERFORM NEXT-EQUIPE.
           MOVE SAVE-KEY TO EXC-KEY.

       NEXT-EQUIPE.
           CALL "6-EQUIPE" USING LINK-V EQ-RECORD EXC-KEY.

       DIS-HE-01.
           MOVE EQ-NUMBER TO HE-Z4 LNK-VAL.
           DISPLAY HE-Z4  LINE  3 POSITION 30.
       DIS-HE-02.
           DISPLAY EQ-NOM  LINE  4 POSITION 30.
       DIS-HE-03.
           DISPLAY EQ-TEXTE-IDX(1) LINE  5 POSITION 30.
       DIS-HE-04.
           DISPLAY EQ-TEXTE-IDX(2) LINE  6 POSITION 30.
      *DIS-HE-05.
      *    MOVE EQ-VAL(1) TO HE-Z8.
      *    DISPLAY HE-Z8  LINE  8 POSITION 20.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 131 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE EQUIPE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
