      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-QDET QUESTIONNAIRE DETACHEMENT AUTRE PAYS �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-QDET.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01   CHOIX-MAX         PIC 99 VALUE 21.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CONTRAT.REC".
           COPY "POCL.REC".
           COPY "PARMOD.REC".
           COPY "PAYS.REC".

       01  QDA-RECORD.
               10  QDA-LV.
                   15  QDA-ASS-NOM     PIC X(0050).
                   15  QDA-ASS-NATION  PIC X(0050).
                   15  QDA-ASS-RUE     PIC X(0050).
                   15  QDA-ASS-LOCALITE PIC X(0050).
                   15  QDA-ASS-MATRICULE PIC X(0050).
                   15  QDA-DATE-ENG    PIC X(0050).
                   15  QDA-CDI-OUI     PIC X(0001).
                   15  QDA-CDI-NON     PIC X(0001).
               10  QDA-LV.
                   15  QDA-PAT-NOM     PIC X(0050).
                   15  QDA-PAT-MATRICULE PIC X(0050).
                   15  QDA-PAT-RUE     PIC X(0050).
                   15  QDA-PAT-LOCALITE PIC X(0030).
                   15  QDA-PAT-TEL     PIC X(0020).
                   15  QDA-PAT-PAYS    PIC X(0050).
               10  QDA-DETACH-OUI      PIC X(0001).
               10  QDA-DETACH-NON      PIC X(0001).
               10  QDA-MULTIPAYS-OUI 
                                       PIC X(0001).
               10  QDA-MULTIPAYS-NON 
                                       PIC X(0001).
               10  QDA-ENTRANGER-DU 
                                       PIC X(0020).
               10  QDA-ENTRANGER-AU 
                                       PIC X(0020).
               10  QDA-ENTRU-NOM 
                                       PIC X(0050).
               10  QDA-ENTRU-RUE 
                                       PIC X(0050).
               10  QDA-ENTRU-LOCALITE 
                                       PIC X(0050).
               10  QDA-CHANTIER-NOM 
                                       PIC X(0050).
               10  QDA-CHANTIER-ADRESSE 
                                       PIC X(0050).
               10  QDA-CHANTIER-RUE 
                                       PIC X(0050).
               10  QDA-CHANTIER-LOCALITE 
                                       PIC X(0050).

       01  QDB-RECORD.
           02  QDB-A-1        PIC X.
           02  QDB-A-2        PIC X.
           02  QDB-A-3        PIC X.
           02  QDB-A-4        PIC X.
           02  QDB-A-5        PIC X.
           02  QDB-A-6        PIC X.
           02  QDB-A-7        PIC X.
           02  QDB-A-8        PIC X.
           02  QDB-A-9        PIC X.
           02  QDB-A-10       PIC X.
           02  QDB-A-11       PIC X.
           02  QDB-A-12       PIC X.
           02  QDB-A-13       PIC X.
           02  QDB-LIEU-DATE  PIC X(40).

       01  COMPTEUR          PIC 99 VALUE 0.
       01  X-TEXT            PIC X.
       01  X-LE              PIC X(4) VALUE ", le".

       01  LIGNES-TEXTE.
           03 L-T               PIC X(50) OCCURS 3.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2). 
           02 HE-Z3 PIC Z(3). 
           02 HE-Z4 PIC Z(4). 
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HP-DATE .
              03 HE-J PIC ZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HE-M PIC ZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HE-A PIC ZZZZ.
           02 HE-SNOCS.
              03 HS-A PIC ZZZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HS-M PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HS-J PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HS-S PIC 999.

       01  DATES.
              03 DATE-1   PIC 9(8).
              03 D1 REDEFINES DATE-1.
                 04 1-A   PIC 9999.
                 04 1-M   PIC 99.
                 04 1-J   PIC 99.
              03 DATE-2   PIC 9(8).
              03 D2 REDEFINES DATE-2.
                 04 2-A   PIC 9999.
                 04 2-M   PIC 99.
                 04 2-J   PIC 99.
              03 DATE-3   PIC 9(8).
              03 D3 REDEFINES DATE-3.
                 04 3-A   PIC 9999.
                 04 3-M   PIC 99.
                 04 3-J   PIC 99.

       01  HE-SEL.
           02 H-S PIC X       OCCURS 20.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-QDET.


           CALL "0-TODAY" USING TODAY.
           INITIALIZE QDA-RECORD.
           MOVE 1 TO LNK-PRESENCE.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0027000000 TO EXC-KFR(6).
           MOVE 0052530000 TO EXC-KFR(11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3     MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN CHOIX-MAX
                      MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.
 
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 THRU 6 PERFORM AVANT-TEXTE
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
      *    WHEN 10 PERFORM AVANT-10 
           WHEN CHOIX-MAX PERFORM AVANT-DEC.


           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN CHOIX-MAX PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-3.
           ACCEPT PC-NUMBER
             LINE 5 POSITION 30 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM AFFICHAGE-DETAIL.


       AVANT-7.
           MOVE 09300000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-1
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO 1-A
              MOVE TODAY-MOIS  TO 1-M
              MOVE TODAY-JOUR  TO 1-J
              MOVE 0 TO LNK-NUM.
           PERFORM DIS-HE-07.
           MOVE DATE-1 TO PARMOD-PROG-NUMBER-1.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       AVANT-8.
           IF DATE-2 < DATE-1
           OR DATE-1 = 0
              MOVE DATE-1 TO DATE-2
           END-IF.
           IF DATE-1 > 0
           MOVE 10300000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-2
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           PERFORM DIS-HE-08.
           MOVE DATE-2 TO PARMOD-PROG-NUMBER-2.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       AVANT-9.
           IF DATE-3 < 1
              MOVE TODAY-ANNEE TO 3-A
              MOVE TODAY-MOIS  TO 3-M
              MOVE TODAY-JOUR  TO 3-J
              MOVE 0 TO LNK-NUM.
           MOVE 11300000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-3
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO 3-A
              MOVE TODAY-MOIS  TO 3-M
              MOVE TODAY-JOUR  TO 3-J
              MOVE 0 TO LNK-NUM.
           PERFORM DIS-HE-09.
           MOVE DATE-3 TO PARMOD-PROG-NUMBER-3.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       AVANT-TEXTE.        
           COMPUTE IDX = INDICE-ZONE - 3.
           COMPUTE LIN-IDX = 5 + IDX.
           IF IDX = 1
             ACCEPT L-T(IDX) LINE LIN-IDX POSITION 30 SIZE 50
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE
           ELSE
             IF L-T(1) NOT = SPACES
             ACCEPT L-T(IDX) LINE LIN-IDX POSITION 30 SIZE 50
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE
             ELSE
                INITIALIZE LIGNES-TEXTE
             END-IF
           END-IF.
           MOVE LIGNES-TEXTE TO PARMOD-TEXT3.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.
           IF REG-PERSON > 0 
           AND PRES-ANNEE = 0
               MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN  13 CONTINUE
           WHEN  OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           PERFORM AFFICHAGE-DETAIL.

       APRES-3.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-POCL
           WHEN   2 THRU 3 
                    IF EXC-KEY = 2 
                       MOVE "A" TO LNK-A-N
                    ELSE
                       MOVE "N" TO LNK-A-N
                    END-IF
                    CALL "2-POCL" USING LINK-V PC-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                    CANCEL "2-POCL"
           END-EVALUATE.
           IF PC-NUMBER > 0
              PERFORM HISTORIQUE.

       HISTORIQUE.
           INITIALIZE PARMOD-RECORD.
           MOVE "QD" TO PARMOD-MODULE-3.
           MOVE PC-NUMBER TO PARMOD-MODULE-4.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-SETTINGS TO HE-SEL.
           MOVE PARMOD-TEXT3 TO LIGNES-TEXTE.
           MOVE PARMOD-PROG-NUMBER-1 TO DATE-1.
           MOVE PARMOD-PROG-NUMBER-2 TO DATE-2.
           MOVE PARMOD-PROG-NUMBER-3 TO DATE-3.
           PERFORM AFFICHAGE-DETAIL.

           
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE "N" TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY
            WHEN  5 PERFORM TRANSMET
                    MOVE 1 TO DECISION 
           WHEN 68 MOVE "QD" TO LNK-AREA
                  CALL "5-SEL" USING LINK-V PARMOD-RECORD
                  MOVE PARMOD-SETTINGS TO HE-SEL
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
                  PERFORM DISPLAY-F-KEYS
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       
       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD A-N FAKE-KEY.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD PV-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       NEXT-POCL.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" EXC-KEY.


       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           DISPLAY PR-NOM LINE 3 POSITION 47 SIZE 33.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 48 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 3 POSITION COL-IDX SIZE IDX LOW.
        
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.

       DIS-HE-03.
           MOVE PC-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 5 POSITION 30.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY.
           DISPLAY PC-NOM LINE 5 POSITION 40 SIZE 40.

       DIS-HE-07.
           MOVE 1-A     TO HE-AA.
           MOVE 1-M     TO HE-MM.
           MOVE 1-J     TO HE-JJ.
           DISPLAY HE-DATE LINE 9 POSITION 30.

       DIS-HE-08.
           MOVE 2-A     TO HE-AA.
           MOVE 2-M     TO HE-MM.
           MOVE 2-J     TO HE-JJ.
           DISPLAY HE-DATE LINE 10 POSITION 30.
       DIS-HE-09.
           MOVE 3-A     TO HE-AA.
           MOVE 3-M     TO HE-MM.
           MOVE 3-J     TO HE-JJ.
           DISPLAY HE-DATE LINE 11 POSITION 30.
       DIS-HE-LIGNES.
           PERFORM D-L VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 3.
       DIS-HE-END.
           MOVE 10 TO LIN-IDX.
           MOVE 0 TO COMPTEUR.
           PERFORM AFFICHE-D THRU AFFICHE-END.

       D-L.
           COMPUTE LIN-IDX = 5 + IDX-1.
           DISPLAY L-T(IDX-1) LINE LIN-IDX POSITION 30 SIZE 50.

       TRANSMET.
           INITIALIZE QDA-RECORD.
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO QDA-ASS-NOM.
           MOVE 1 TO IDX.
           STRING PR-NAISS-A DELIMITED BY SIZE INTO QDA-ASS-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-NAISS-M DELIMITED BY SIZE INTO QDA-ASS-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-NAISS-J DELIMITED BY SIZE INTO QDA-ASS-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-SNOCS DELIMITED BY SIZE INTO QDA-ASS-MATRICULE
           WITH POINTER IDX.

           CALL "4-PRRUE" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO QDA-ASS-RUE.

           CALL "4-PRLOC" USING LINK-V PR-RECORD
           MOVE LNK-TEXT TO QDA-ASS-LOCALITE

           MOVE PR-NATIONALITE TO PAYS-CODE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.
           MOVE PAYS-NATION TO QDA-ASS-NATION.

           MOVE CON-DEBUT-A TO HE-A.
           MOVE CON-DEBUT-M TO HE-M.
           MOVE CON-DEBUT-J TO HE-J.
           MOVE HP-DATE     TO QDA-DATE-ENG.
           IF CON-TYPE = 1
              MOVE "X" TO QDA-CDI-OUI
           ELSE
              MOVE "X" TO QDA-CDI-NON
           END-IF.

           MOVE 1-A     TO HE-A.
           MOVE 1-M     TO HE-M.
           MOVE 1-J     TO HE-J.
           MOVE HP-DATE TO QDA-ENTRANGER-DU.
           MOVE 2-A     TO HE-A.
           MOVE 2-M     TO HE-M.
           MOVE 2-J     TO HE-J.
           MOVE HP-DATE TO QDA-ENTRANGER-AU.

           MOVE FR-NOM   TO QDA-PAT-NOM.
           MOVE FR-PHONE TO QDA-PAT-TEL.
           MOVE "LUXEMBOURG" TO QDA-PAT-PAYS.

           MOVE 1 TO IDX.
           STRING FR-LOCALITE DELIMITED BY "  " INTO QDB-LIEU-DATE
           WITH POINTER IDX.
           STRING X-LE DELIMITED BY SIZE INTO QDB-LIEU-DATE
           WITH POINTER IDX.
           ADD 2 TO IDX.
           MOVE 3-A     TO HE-A.
           MOVE 3-M     TO HE-M.
           MOVE 3-J     TO HE-J.
           STRING HP-DATE DELIMITED BY SIZE INTO QDB-LIEU-DATE
           WITH POINTER IDX.

           MOVE L-T(1) TO QDA-CHANTIER-NOM.
           MOVE L-T(2) TO QDA-CHANTIER-LOCALITE.
           MOVE L-T(3) TO QDA-CHANTIER-RUE.
           
           MOVE 1 TO IDX.
           STRING FR-ETAB-A DELIMITED BY SIZE INTO QDA-PAT-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-ETAB-N DELIMITED BY SIZE INTO QDA-PAT-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-SNOCS DELIMITED BY SIZE INTO QDA-PAT-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-EXTENS DELIMITED BY SIZE INTO QDA-PAT-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.

           MOVE 1 TO IDX.
           STRING FR-PAYS DELIMITED BY " "  INTO QDA-PAT-LOCALITE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           MOVE FR-CODE-POST TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE INTO QDA-PAT-LOCALITE
           WITH POINTER IDX.
           ADD 2 TO IDX.
           STRING FR-LOCALITE DELIMITED BY " "  INTO QDA-PAT-LOCALITE
           WITH POINTER IDX.
           MOVE 1 TO IDX.
           STRING FR-RUE DELIMITED BY "   "  INTO QDA-PAT-RUE
              WITH POINTER IDX.
           ADD 1 TO IDX.
           IF IDX < 39
              STRING FR-MAISON DELIMITED BY "  " INTO QDA-PAT-RUE
              WITH POINTER IDX.

           MOVE PC-NOM   TO QDA-ENTRU-NOM.
      *    MOVE PC-COURRIER-1 TO QDA-ENTRU-ADRESSE.
           MOVE 1 TO IDX.
           STRING PC-PAYS DELIMITED BY " "  INTO QDA-ENTRU-LOCALITE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           MOVE PC-CODE-POST TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE INTO QDA-ENTRU-LOCALITE
           WITH POINTER IDX.
           ADD 2 TO IDX.
           STRING PC-LOCALITE DELIMITED BY SIZE INTO 
           QDA-ENTRU-LOCALITE WITH POINTER IDX.
           MOVE 1 TO IDX.
           STRING PC-RUE DELIMITED BY "   "  INTO QDA-ENTRU-RUE
              WITH POINTER IDX.
           ADD 1 TO IDX.
           IF IDX < 39
              STRING PC-MAISON DELIMITED BY "  " INTO QDA-ENTRU-RUE
              WITH POINTER IDX.

           PERFORM ART VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 13.
           MOVE 0 TO IDX-4.
           MOVE 0 TO LNK-VAL.
           CALL "QDETA" USING LINK-V QDA-RECORD QDB-RECORD.
           MOVE 99 TO LNK-VAL.
           CALL "QDETA" USING LINK-V QDA-RECORD QDB-RECORD.
           MOVE 0 TO LNK-VAL.
           CANCEL "QDETA".

       ART.
           IF H-S(IDX-1) NOT = "N" AND NOT = " "
              MOVE "X" TO X-TEXT
           ELSE
              MOVE " " TO X-TEXT.
           EVALUATE IDX-1
              WHEN  1 MOVE X-TEXT TO QDB-A-1
              WHEN  2 MOVE X-TEXT TO QDB-A-2
              WHEN  3 MOVE X-TEXT TO QDB-A-3
              WHEN  4 MOVE X-TEXT TO QDB-A-4
              WHEN  5 MOVE X-TEXT TO QDB-A-5
              WHEN  6 MOVE X-TEXT TO QDB-A-6
              WHEN  7 MOVE X-TEXT TO QDB-A-7
              WHEN  8 MOVE X-TEXT TO QDB-A-8
              WHEN  9 MOVE X-TEXT TO QDB-A-9
              WHEN 10 MOVE X-TEXT TO QDB-A-10
              WHEN 11 MOVE X-TEXT TO QDB-A-11
              WHEN 12 MOVE X-TEXT TO QDB-A-12
              WHEN 13 MOVE X-TEXT TO QDB-A-13
           END-EVALUATE.
                      
       
       AFFICHAGE-ECRAN.
           MOVE 1215 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHE-D.
           MOVE COMPTEUR TO LNK-NUM.
           ADD 1 TO LIN-IDX LNK-NUM.
           MOVE "QD" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           IF LNK-TEXT = SPACES
              GO AFFICHE-END.
           ADD 1 TO COMPTEUR.
           IF  H-S(COMPTEUR) NOT = "N"
           AND H-S(COMPTEUR) NOT = " "
              DISPLAY LNK-TEXT LINE LIN-IDX POSITION 63 SIZE 15 REVERSE
           ELSE
              DISPLAY LNK-TEXT LINE LIN-IDX POSITION 65 SIZE 15 LOW.
           GO AFFICHE-D.
       AFFICHE-END.
           EXIT.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".
       
