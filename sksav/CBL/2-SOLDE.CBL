      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-SOLDE INTERROGATION SOLDES SALAIRE        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-SOLDE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.


           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "LIVRE.REC".

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  MOIS                  PIC 99.
       01  SOLDES.
           03 SOLDE              PIC S999999V99 OCCURS 2.

       01  ECR-DISPLAY.
           02 HE-Z3Z2 PIC Z(3),ZZ.
           02 HE-Z5Z2 PIC -ZZZZZ,ZZ.
           02 HE-Z6 PIC Z(6).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-SOLDE.
       
           PERFORM AFFICHAGE-ECRAN .
           MOVE LNK-MOIS TO SAVE-MOIS.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions


           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           END-EVALUATE.


       TRAITEMENT-ECRAN-01.

           PERFORM DISPLAY-F-KEYS.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1-1 
           WHEN  2 PERFORM AVANT-1-2.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-1 
           WHEN  2 PERFORM APRES-1-2.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-1-1.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 PERFORM CHANGE-MOIS
             MOVE 99 TO EXC-KEY
             END-EVALUATE.

       AVANT-1-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       APRES-1-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   ELSE
                      MOVE "N" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN  OTHER PERFORM NEXT-REGIS.
           PERFORM DIS-E1-01.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE.
           PERFORM AFFICHAGE-DETAIL.
           IF PRES-ANNEE > 0
           AND EXC-KEY NOT = 53
              PERFORM AFFICHAGE-SOLDE.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              IF  SOLDE(1) = 0 
              AND SOLDE(2) = 0 
              AND REG-PERSON NOT = 0
                 GO APRES-1-1
              END-IF.

       APRES-1-2.
           MOVE "A" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
                   PERFORM AFFICHAGE-DETAIL
           WHEN 65 THRU 66 PERFORM NEXT-REGIS.
           PERFORM DIS-E1-01.
           IF EXC-KEY NOT = 27
           AND PRES-ANNEE > 0
              PERFORM AFFICHAGE-SOLDE
           END-IF.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              IF  SOLDE(1) = 0 
              AND SOLDE(2) = 0 
              AND REG-PERSON NOT = 0
                 GO APRES-1-2
              END-IF.

       AFFICHAGE-ECRAN.
           MOVE 2523 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DIS-E1-01.
           MOVE REG-PERSON  TO HE-Z6.
           IF REG-PERSON NOT = 0
              MOVE REG-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 
             LINE 3 POSITION 15 SIZE 6
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-E1-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-E1-END.


       AFFICHAGE-DETAIL.
           PERFORM DIS-E1-01 THRU DIS-E1-END.

       AFFICHAGE-SOLDE.
           INITIALIZE SOLDES LNK-SUITE.
           PERFORM DIS-MOIS VARYING MOIS FROM 1 BY 1 UNTIL MOIS > 12.
           PERFORM DIS-SOLDE VARYING IDX FROM 1 BY 1 UNTIL IDX > 2.
           COMPUTE SH-00 = SOLDE(1) + SOLDE(2).
           MOVE SH-00 TO HE-Z5Z2.
           DISPLAY HE-Z5Z2 LINE LIN-IDX POSITION 48.

       CHANGE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 SUBTRACT 1 FROM LNK-MOIS
             WHEN 58 ADD 1 TO LNK-MOIS
           END-EVALUATE.
           IF LNK-MOIS = 0
              MOVE 1 TO LNK-MOIS
           END-IF.
           IF LNK-MOIS > 12
              MOVE 12 TO LNK-MOIS
           END-IF.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           MOVE LNK-MOIS TO SAVE-MOIS.

       DIS-MOIS.
           COMPUTE LIN-IDX = 6 + MOIS.
           INITIALIZE L-RECORD.
           MOVE FR-KEY     TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON. 
           MOVE MOIS       TO L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           PERFORM DIS-LIGNE VARYING IDX FROM 1 BY 1 UNTIL IDX > 2.

       DIS-LIGNE.
           COMPUTE COL-IDX = 36 - ((IDX - 1) * 12).
           COMPUTE IDX-1   = 90 + IDX * 100.
           MOVE 0 TO SH-00.
           IF IDX = 1
              ADD L-DC(IDX-1) TO SH-00
           ELSE
              SUBTRACT L-DC(IDX-1) FROM SH-00
           END-IF.
           MOVE SH-00 TO HE-Z5Z2.
           ADD SH-00 TO SOLDE(IDX).
           DISPLAY HE-Z5Z2 LINE LIN-IDX POSITION COL-IDX.



       DIS-SOLDE.
           COMPUTE COL-IDX = 36 - ((IDX - 1) * 12).
           MOVE SOLDE(IDX) TO HE-Z5Z2.
           DISPLAY HE-Z5Z2 LINE 20 POSITION COL-IDX.
       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

           