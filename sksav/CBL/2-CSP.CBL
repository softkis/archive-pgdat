      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-CSP CONTROLE + EFFACEMENT   CODES PAIES   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  2-CSP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODPAIE.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CODPAIE.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  ARROW                 PIC X VALUE ">".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "V-VAR.CPY".
SU         COPY "CSDEF.REC".
SU         COPY "CODTXT.REC".
           COPY "POCL.REC".

       01  CSP-IX               PIC 99 COMP-1.
       01  HELP-1                PIC 9(6).
       01  SUITE-IDX             PIC 99 VALUE 2.
       01  POINT-IDX             PIC 99.

       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.

       01  ECR-DISPLAY.
            02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
            02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
            02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
            02 HE-Z4Z2 PIC Z(4),ZZ BLANK WHEN ZERO.
            02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.
            02 HE-DATE .
               03 HE-JJ PIC ZZ.
               03 FILLER PIC X VALUE ".".
               03 HE-MM PIC ZZ.
               03 FILLER PIC X VALUE ".".
               03 HE-AA PIC 99.

       01 HE-CSP.
          02 HE-CSP-RECORD OCCURS 40.
              03 HE-CSP-FIRME         PIC 9(6).
              03 HE-CSP-PERSON        PIC 9(8).
              03 HE-CSP-MOIS          PIC 99.
              03 HE-CSP-SUITE         PIC 99.
              03 HE-CSP-POSTE         PIC 9(8).
              03 HE-CSP-EXTENSION     PIC X(50).
              03 HE-CSP-CODE          PIC 9(4).

              03 HE-CSP-FIRME-2       PIC 9(6).
              03 HE-CSP-PERSON-2      PIC 9(8).
              03 HE-CSP-MOIS-2        PIC 99.
              03 HE-CSP-SUITE-2       PIC 99.
              03 HE-CSP-CODE-2        PIC 9(4).
              03 HE-CSP-POSTE-2       PIC 9(8).
              03 HE-CSP-EXTENSION-2   PIC X(50).

              03 HE-CSP-FIRME-3       PIC 9(6).
              03 HE-CSP-POSTE-3       PIC 9(8).
              03 HE-CSP-EXTENSION-3   PIC X(50).
              03 HE-CSP-MOIS-3        PIC 99.
              03 HE-CSP-SUITE-3       PIC 99.
              03 HE-CSP-PERSON-3      PIC 9(8).
              03 HE-CSP-CODE-3        PIC 9(4).

              03 HE-CSP-VALEURS.
                 04 HE-CSP-DONNEE     PIC 9(8)V9(5) OCCURS 8.
              03 HE-CSP-TEXTE         PIC X(30).
              03 HE-CSP-PRINT         PIC X.
              03 HE-CSP-SHOW          PIC X.
              03 HE-CSP-PROTECT       PIC X.
              03 HE-CSP-FILLER        PIC X(49).
              03 HE-CSP-STAMP.
                 04 HE-CSP-TIME.
                    05 HE-CSP-ST-ANNEE PIC 9999.
                    05 HE-CSP-ST-MOIS  PIC 99.
                    05 HE-CSP-ST-JOUR  PIC 99.
                    05 HE-CSP-ST-HEURE PIC 99.
                    05 HE-CSP-ST-MIN   PIC 99.
                 04 HE-CSP-USER        PIC X(10).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CODPAIE.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-CSP .

           CALL "0-TODAY" USING TODAY.
       
           MOVE LNK-SUFFIX TO ANNEE-PAIE.
           OPEN I-O   CODPAIE.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 


           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.


      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
              WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN OTHER MOVE 0000080000 TO EXC-KFR (2).
           IF INDICE-ZONE = CHOIX-MAX
                      MOVE 0052000000 TO EXC-KFR (11).


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  OTHER PERFORM AVANT-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  OTHER PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           MOVE FR-KEY TO REG-FIRME.
           IF LNK-PERSON NOT = 0 
               MOVE LNK-PERSON TO REG-PERSON 
               MOVE 13 TO EXC-KEY
               MOVE 0 TO LNK-PERSON
           ELSE    
           ACCEPT REG-PERSON 
             LINE  3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 
               PERFORM CHANGE-MOIS
               MOVE 27 TO EXC-KEY
           END-EVALUATE.

       AVANT-2.            
           ACCEPT REG-MATCHCODE
             LINE  3 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PAGE.
           MOVE 0000000000 TO EXC-KFR(1) EXC-KFR(2) 
           MOVE 0000680000 TO EXC-KFR(14) 
           PERFORM DISPLAY-F-KEYS
           ACCEPT ACTION 
             LINE  24 POSITION 70 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
           COMPUTE CSP-IX = INDICE-ZONE - 2.
           COMPUTE LIN-IDX = CSP-IX + 4.
           MOVE ">" TO ARROW.
           ACCEPT ARROW
             LINE  LIN-IDX POSITION 1 SIZE 1
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 1.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE FR-KEY TO REG-FIRME
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-HE-01 THRU DIS-HE-02
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE
              IF PRES-TOT(LNK-MOIS) = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           PERFORM DIS-HE-01 THRU DIS-HE-02.
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-NUM LNK-PERSON
           ELSE
              IF REG-PERSON NOT = 0
              AND EXC-KEY NOT = 53
                 PERFORM TOTAL-CODPAIE  THRU TOTAL-CODPAIE-END.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   PERFORM DIS-HE-01 THRU DIS-HE-02
                   MOVE 4 TO LIN-IDX
                   PERFORM TOTAL-CODPAIE  THRU TOTAL-CODPAIE-END
           END-EVALUATE.                     
           
       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-END.

           
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           MOVE HE-CSP-RECORD(CSP-IX) TO CSP-RECORD.
           EVALUATE EXC-KEY 
            WHEN  8 MOVE 2 TO DECISION
                    IF LNK-SQL = "Y" 
                       CALL "9-CODPAIE" USING LINK-V CSP-RECORD DEL-KEY 
                    END-IF
                    DELETE CODPAIE INVALID CONTINUE END-DELETE
                    CALL "4-A" USING LINK-V PR-RECORD REG-RECORD 
                    PERFORM TOTAL-CODPAIE THRU TOTAL-CODPAIE-END
             END-EVALUATE.


      *    HISTORIQUE DES CODPAIES
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       TOTAL-CODPAIE.
           MOVE 0 to  NOT-FOUND.
           MOVE 4 TO LIN-IDX.
           INITIALIZE CSP-RECORD CSP-IX.
      *    MOVE REG-FIRME  TO CSP-FIRME.
      *    MOVE REG-PERSON TO CSP-PERSON.
      *    MOVE LNK-MOIS   TO CSP-MOIS.
      *    START CODPAIE KEY >= CSP-KEY INVALID 
      *         MOVE 1 TO NOT-FOUND.
           MOVE REG-FIRME  TO CSP-FIRME-2.
           MOVE REG-PERSON TO CSP-PERSON-2.
           MOVE LNK-MOIS   TO CSP-MOIS-2.
           START CODPAIE KEY >= CSP-KEY-2 INVALID 
                MOVE 1 TO NOT-FOUND.
           PERFORM READ-CSP THRU READ-CSP-END.
           COMPUTE CHOIX-MAX = 2 + CSP-IX.
       TOTAL-CODPAIE-END.

       READ-CSP.
           IF NOT-FOUND = 1
              GO READ-CSP-END.
           READ CODPAIE NEXT AT END 
              GO READ-CSP-END.
           IF FR-KEY     NOT = CSP-FIRME
           OR REG-PERSON NOT = CSP-PERSON
           OR LNK-MOIS   NOT = CSP-MOIS 
              GO READ-CSP-END.
           ADD 1 TO CSP-IX.
           MOVE CSP-RECORD TO HE-CSP-RECORD(CSP-IX).
           PERFORM DIS-HIS-LIGNE.
           IF LIN-IDX > 21
              ADD 1 TO INDICE-ZONE
              PERFORM AVANT-PAGE
              IF EXC-KEY = 66
                 MOVE 68 TO EXC-KEY
              END-IF
              IF EXC-KEY NOT = 68
                 GO READ-CSP-END
              ELSE
                 INITIALIZE HE-CSP CSP-IX
                 PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM 5
                                   BY 1 UNTIL LIN-IDX > 24
                 MOVE 4 TO LIN-IDX
                 MOVE 0 TO INDICE-ZONE
              END-IF
           END-IF.
           GO READ-CSP.
       READ-CSP-END.
           ADD 1 TO LIN-IDX .
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 24.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       DIS-HIS-LIGNE.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES  LINE  LIN-IDX POSITION  1 SIZE 80.
           MOVE CSP-CODE   TO HE-Z4 CD-NUMBER.
           MOVE CSP-SUITE  TO HE-Z2.
SU         CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY.
           PERFORM CS-DATA.
           IF CSP-SUITE > 0
              DISPLAY HE-Z2 LINE  LIN-IDX POSITION 1 REVERSE.

           DISPLAY HE-Z4 LINE LIN-IDX POSITION  3.
           DISPLAY CSP-PROTECT LINE LIN-IDX POSITION 7 LOW.
           IF CSP-TEXTE = SPACES
               DISPLAY CTX-NOM LINE LIN-IDX POSITION 8
           ELSE
               DISPLAY CSP-TEXTE LINE LIN-IDX POSITION 8
           END-IF.
           IF CSP-POSTE NOT = 0
              MOVE CSP-POSTE TO PC-NUMBER HE-Z8
              IF CSP-CODE < 3
                 CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
                 DISPLAY HE-Z8 LINE LIN-IDX POSITION 1 REVERSE
                 DISPLAY SPACES LINE LIN-IDX POSITION 9 SIZE 1
                 DISPLAY PC-NOM LINE LIN-IDX POSITION 10 SIZE 20
              ELSE
                 DISPLAY HE-Z8 LINE LIN-IDX POSITION 22 LOW
              END-IF
           END-IF.

           MOVE CSP-DONNEE-1 TO HE-Z6Z2.
           INSPECT HE-Z6Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z6Z2   LINE  LIN-IDX POSITION 30.
           MOVE CSP-DONNEE-2 TO HE-Z6Z2
           INSPECT HE-Z6Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z6Z2   LINE  LIN-IDX POSITION 39.
           MOVE CSP-POURCENT TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z4Z2   LINE  LIN-IDX POSITION 48.
           MOVE CSP-UNITE    TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z4Z2   LINE  LIN-IDX POSITION 55.
           MOVE CSP-UNITAIRE TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z4Z2   LINE  LIN-IDX POSITION 63.
           MOVE CSP-TOTAL    TO HE-Z6Z2.
           INSPECT HE-Z6Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z6Z2   LINE  LIN-IDX POSITION 71.

       CS-DATA.
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.

       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS THRU SUBTRACT-END
             WHEN 58 PERFORM ADD-MOIS THRU ADD-END
           END-EVALUATE.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-DETAIL.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0 
           OR REG-PERSON = 0
              GO SUBTRACT-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO SUBTRACT-END.
           GO SUBTRACT-MOIS.
       SUBTRACT-END.
           IF LNK-MOIS = 0
              IF REG-PERSON = 0
                 MOVE 1 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.
       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
           OR REG-PERSON = 0
              GO ADD-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO ADD-END.
           GO ADD-MOIS.
       ADD-END.
           IF LNK-MOIS > 12
              IF REG-PERSON = 0
                 MOVE 12 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.


       AFFICHAGE-ECRAN.
           MOVE 2500 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.


       END-PROGRAM.
           MOVE REG-PERSON TO LNK-PERSON.
           CLOSE CODPAIE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


