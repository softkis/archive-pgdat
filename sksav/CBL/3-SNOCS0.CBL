      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-SNOCS0 LISTE SNOCS DONNEES NON TRANSMISES �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-SNOCS0.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "LIVRE.FC".
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "LIVRE.FDE".
           COPY "FORM80.FDE".


       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "FIRME.REC".
           COPY "V-VAR.CPY".
           COPY "IMPRLOG.REC".

       01  CHOIX-MAX     PIC 99 VALUE 1.
       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".SN0".

       01  LIVRE-NAME.
           02 FILLER             PIC X(8) VALUE "S-LIVRE.".
           02 ANNEE-LIVRE        PIC 999.

       77  LAST-FIRME            PIC 9(8).
       77  SAVE-FIRME            PIC 9(8).
       77  FLAG-FIRME            PIC 9 VALUE 0.
       77  TEST-FIRME            PIC 9(9).

       01  ECR-DISPLAY.
           02 HE-Z2    PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4    PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6    PIC Z(6) BLANK WHEN ZERO.
           02 HEZ8     PIC Z(8) BLANK WHEN ZERO.
           02 HEZ8Z2   PIC Z(8),ZZ BLANK WHEN ZERO.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
       
           USE AFTER ERROR PROCEDURE ON LIVRE FORM.  
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-SNOCS0.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE LNK-SUFFIX TO ANNEE-LIVRE.
           CALL "0-TODAY" USING TODAY.
           IF LNK-ANNEE > TODAY-ANNEE
              MOVE "SL" TO LNK-AREA
              MOVE 48 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              ACCEPT ACTION 
              EXIT PROGRAM
           END-IF.
           IF LNK-ANNEE = TODAY-ANNEE
              COMPUTE IDX = TODAY-MOIS - 1
              IF LNK-MOIS > IDX
                 MOVE "SL" TO LNK-AREA
                 MOVE 55 TO LNK-NUM
                 PERFORM DISPLAY-MESSAGE
                 ACCEPT ACTION 
                 EXIT PROGRAM
              END-IF
           END-IF.

           OPEN I-O LIVRE.

           
           MOVE FR-KEY TO SAVE-FIRME.
           PERFORM AFFICHAGE-ECRAN.
           MOVE 0 TO COUNTER.

           MOVE 1 TO INDICE-ZONE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
            UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions
           EVALUATE INDICE-ZONE
               WHEN 1 MOVE 0000000000 TO EXC-KFR (11)
                      MOVE 0000000025 TO EXC-KFR (1)
                      MOVE 1700000000 TO EXC-KFR (2).
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
           END-EVALUATE. 
           IF INDICE-ZONE > CHOIX-MAX
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           IF EXC-KEY = 5 
              PERFORM CUMUL-LIVRE
              PERFORM END-PROGRAM
           ELSE
              MOVE 1 TO INPUT-ERROR.

       CUMUL-LIVRE.
           INITIALIZE L-RECORD LIN-NUM.
           START LIVRE KEY > L-KEY-2 INVALID CONTINUE
                NOT INVALID
              PERFORM LECTURE-LIVRE THRU LECTURE-LIVRE-END.

       LECTURE-LIVRE.
           READ LIVRE NEXT AT END GO LECTURE-LIVRE-END.
           IF L-REGIME = 0 
           OR L-REGIME > 100
           OR L-SUITE  > 0
           OR L-MOIS   > LNK-MOIS
           OR L-DATE-SNOCS > 0 
              GO LECTURE-LIVRE
           END-IF.
           PERFORM TRAITEMENT THRU TRAITEMENT-END
           GO LECTURE-LIVRE.
       LECTURE-LIVRE-END.
           IF LIN-NUM NOT = 0
              PERFORM TRANSMET
           END-IF.

       TRAITEMENT.
           IF L-FIRME NOT = FR-KEY
              MOVE L-FIRME TO LAST-FIRME FR-KEY
              CALL "6-FIRME" USING LINK-V "N" FAKE-KEY
              MOVE 0 TO FLAG-FIRME
           END-IF.
           INITIALIZE REG-RECORD.
           MOVE FR-KEY   TO REG-FIRME.
           MOVE L-PERSON TO REG-PERSON.
           PERFORM GET-MATRICULE.
           IF REG-PERSON = 0
           OR PR-SNOCS     > 9
              GO TRAITEMENT-END.
           display l-key line 20 position 10.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE IDX = LIN-NUM + 5.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           ADD 1 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           MOVE 2 TO CAR-NUM.
           MOVE L-MOIS TO VH-00.

           PERFORM FILL-FORM.
           MOVE 5 TO COL-NUM.
           MOVE 6 TO CAR-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.

           PERFORM FILL-FORM.
           MOVE 12 TO COL-NUM.
           MOVE 6 TO CAR-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.

           MOVE 20 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 2 TO COL-NUM.
           MOVE PR-NOM-JF TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 2 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 65 TO COL-NUM.
           MOVE PR-NAISS-A TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 70 TO COL-NUM.
           MOVE PR-NAISS-M TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 73 TO COL-NUM.
           MOVE PR-NAISS-J TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 76 TO COL-NUM.
           MOVE PR-SNOCS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       TRAITEMENT-END.
           EXIT.

       GET-MATRICULE.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 32 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           EXIT.


       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           MOVE SAVE-FIRME TO FR-KEY FR-KEY.
           CALL "6-FIRME" USING LINK-V "N" FAKE-KEY.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
