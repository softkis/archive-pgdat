      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-CGEX INTERROGATION ABSENCES CONG�         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-CGEX.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "OCCUP.FC".
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴
           COPY "OCCUP.FDE".
           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "OCCTXT.REC".
           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  MOIS                  PIC 99.
       01  HELP-VAL              PIC 999V99 COMP-3.
       01  CONGE-HELP            PIC 999V99 COMP-3.
       01  CONGE-TOTAL           PIC 999V99 COMP-3.
       01  OCCUP-IDX             PIC 99 COMP-1.
       01  OCCUP-COUNTER         PIC 99 COMP-1.
       01  OCCUP-STORE.
           02 O-X OCCURS 20.
              03 OCC-STORE       PIC 99.
       
       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.
  
       01   ECR-DISPLAY.
            02 HE-Z3Z2 PIC Z(3),ZZ.
            02 HE-Z2 PIC ZZ.
            02 HE-Z3 PIC ZZZ.
            02 HE-Z4Z2 PIC Z(4),ZZ.
            02 HE-Z6 PIC Z(6).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                OCCUPATION
                JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-CGEX.
       
           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN INPUT OCCUPATION.
           OPEN INPUT JOURS.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 .

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 .

           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 09 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           MOVE FR-KEY TO REG-FIRME.
              ACCEPT REG-PERSON 
              LINE 3 POSITION 15 SIZE 6
              TAB UPDATE NO BEEP CURSOR 1
              ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 0 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.

           PERFORM DIS-HE-01.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-REGIS
                   PERFORM DIS-HE-01
                   PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.                     
           
       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.

       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.

       READ-OCCUP.
           INITIALIZE NOT-FOUND OCCUP-COUNTER OCC-KEY.
           START OCCUPATION KEY > OCC-KEY INVALID KEY
                INITIALIZE OCC-RECORD
                NOT INVALID
                PERFORM GET-OCCUP TEST BEFORE UNTIL NOT-FOUND = 1.
                
       GET-OCCUP.
           READ OCCUPATION NEXT NO LOCK AT END 
                INITIALIZE OCC-RECORD
                MOVE 1 TO NOT-FOUND
           END-READ.
           IF OCC-AFFICHAGE NOT = 0 
              ADD 1 TO OCCUP-COUNTER
              MOVE OCC-KEY  TO OCC-STORE(OCCUP-COUNTER)
              PERFORM DIS-DET-LIGNE
           END-IF.
           IF OCCUP-COUNTER = 17
              MOVE 1 TO NOT-FOUND
           END-IF.

       DIS-DET-LIGNE.
           COMPUTE LIN-IDX = 6 + OCCUP-COUNTER.
           MOVE OCC-KEY TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 2 LOW.
           MOVE OCC-KEY TO OT-NUMBER
           CALL "6-OCCTXT" USING LINK-V OT-RECORD FAKE-KEY.
           DISPLAY OT-NOM LINE LIN-IDX POSITION 5 LOW.

       AFFICHAGE-ECRAN.
           MOVE 2356 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           PERFORM READ-OCCUP.


       AFFICHAGE-DETAIL.
           INITIALIZE CONGE-TOTAL.
           PERFORM GET-MOIS VARYING MOIS FROM 1 BY 1 UNTIL MOIS > 12.
           MOVE CONGE-TOTAL TO HE-Z3.
           DISPLAY HE-Z3 LINE 23 POSITION 29 HIGH.
           
       GET-MOIS.
           COMPUTE COL-IDX = 29 + MOIS * 4.
           INITIALIZE CONGE-HELP JRS-RECORD.
           MOVE FR-KEY  TO JRS-FIRME.
           MOVE REG-PERSON  TO JRS-PERSON.
           MOVE MOIS TO JRS-MOIS. 
           PERFORM GET-HOURS VARYING OCCUP-IDX FROM 1 BY 1 UNTIL 
                                     OCCUP-IDX > OCCUP-COUNTER.
           MOVE CONGE-HELP TO HE-Z3.
           ADD CONGE-HELP TO CONGE-TOTAL.
           DISPLAY HE-Z3 LINE 23 POSITION COL-IDX HIGH.

       GET-HOURS.
           MOVE OCC-STORE(OCCUP-IDX) TO JRS-OCCUPATION.
           COMPUTE LIN-IDX = 6 + OCCUP-IDX.
           INITIALIZE HELP-VAL.
           READ JOURS NO LOCK INVALID CONTINUE
                       NOT INVALID 
           ADD JRS-HRS(32) TO HELP-VAL CONGE-HELP
           END-READ.
           MOVE HELP-VAL TO HE-Z3.
           DISPLAY HE-Z3 LINE LIN-IDX POSITION COL-IDX HIGH.

       END-PROGRAM.
           CLOSE OCCUPATION.
           CLOSE JOURS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

           