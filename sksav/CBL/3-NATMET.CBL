      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-NATMET IMPRESSION LISTE NOMBRE DE         �
      *  � PERSONNES PAR METIER + OPTION NATIONALITE             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-NATMET.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".

      *    Fichier interm괺iaire liste nombre de personnes par nationalit�
           SELECT OPTIONAL INTER ASSIGN TO RANDOM, "INTER.MET",
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is INTER-KEY,
                  STATUS FS-INTER.

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM80.FDE".
       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire liste nombre de personnes par m굏ier
       01  INTER-RECORD.

           02 INTER-KEY.
              03 INTER-MET    PIC X(10).
              03 INTER-RES    PIC XXX.
              03 INTER-NAT    PIC XXX.
           02 INTER-COUNTER   PIC 9(4) OCCURS 2.
               
       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  PRECISION             PIC 9 VALUE 0.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "FIRME.REC".
           COPY "PAYS.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "METIER.REC".
           COPY "IMPRLOG.REC".
           COPY "V-VAR.CPY".

       01  LANGUE                PIC 9.
       01  NATION-YN             PIC X VALUE "N".
       01  TOTAL-RECORD.
           02  TOTAL-COUNTER   PIC 9(4) OCCURS 2.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".MNA".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                FORM 
                INTER.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-NATMET.
       
           DELETE FILE INTER.
           OPEN I-O INTER.
           CALL "0-TODAY" USING TODAY.
           INITIALIZE LIN-NUM LIN-IDX TOTAL-RECORD.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
      
      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-YN
           WHEN  2 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
               WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
               WHEN 13 IF INPUT-ERROR = 0
                          ADD 1 TO INDICE-ZONE
                       END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-1.
           ACCEPT NATION-YN
             LINE  16 POSITION 35 SIZE 1 
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE NATION-YN TO ACTION.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-YN.
           IF ACTION = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.
       
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-PERSON
                    PERFORM READ-PERSON THRU READ-PERSON-END
                    PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       START-PERSON.
           INITIALIZE REG-RECORD.
           MOVE 66 TO EXC-KEY.
           MOVE 0  TO SAVE-KEY.

       READ-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" EXC-KEY.
           IF REG-PERSON = 0
              GO READ-PERSON-END
           END-IF.
           PERFORM PRESENCE.
           IF PRES-TOT(LNK-MOIS) = 0
              GO READ-PERSON
           END-IF.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           PERFORM DIS-HE-01.
           MOVE CAR-METIER TO INTER-MET.
           IF NATION-YN NOT = "N"
              MOVE PR-NATIONALITE TO INTER-NAT
              MOVE PR-PAYS        TO INTER-RES
           END-IF.
           READ INTER INVALID 
                MOVE 0 TO INTER-COUNTER(1) INTER-COUNTER(2).
           ADD 1 TO INTER-COUNTER(PR-CODE-SEXE) 
                    TOTAL-COUNTER(PR-CODE-SEXE).
           WRITE INTER-RECORD INVALID REWRITE INTER-RECORD.
           GO READ-PERSON.
        READ-PERSON-END.
           PERFORM START-INTER.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       START-INTER.
           INITIALIZE INTER-RECORD.
           START INTER KEY > INTER-KEY INVALID KEY CONTINUE
                NOT INVALID 
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END 
               GO READ-INTER-END
           END-READ.
           PERFORM READ-METIER.
           GO READ-INTER.
       READ-INTER-END.
           EXIT.

       READ-METIER.
           MOVE INTER-MET TO MET-CODE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE IDX = LIN-NUM + 3.
           IF IDX >= IMPL-MAX-LINE
              PERFORM PAGE-DATE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM FILL-FIRME
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           PERFORM FILL-FILES.

       DIS-HE-01.
           MOVE REG-PERSON   TO HE-Z6.
           DISPLAY HE-Z6     LINE 17 POSITION 35.
           DISPLAY PR-NOM    LINE 17 POSITION 45.
           DISPLAY PR-PRENOM LINE 18 POSITION 45.
       DIS-HE-END.
           EXIT.

       FILL-FIRME.
           MOVE  3 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM =  2.
           PERFORM MOIS-NOM.
           MOVE 45 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 55 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE LNK-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       FILL-FILES.
           ADD 1 TO LIN-NUM.

      * DONNEES METIER
           MOVE  5 TO COL-NUM.
           MOVE INTER-RES TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE INTER-NAT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 15 TO COL-NUM.
           MOVE MET-NOM(1) TO ALPHA-TEXTE.
           IF ALPHA-TEXTE = SPACES
              MOVE INTER-MET TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 50 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE INTER-COUNTER(1) TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE INTER-COUNTER(2) TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 70 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           COMPUTE VH-00 = INTER-COUNTER(1) + INTER-COUNTER(2)
           PERFORM FILL-FORM.

       TOTAUX.
           ADD 1 TO LIN-NUM.
           MOVE 50 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE TOTAL-COUNTER(1) TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE TOTAL-COUNTER(2) TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 70 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           COMPUTE VH-00 = TOTAL-COUNTER(1) + TOTAL-COUNTER(2)
           PERFORM FILL-FORM.

       PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  4 TO CAR-NUM.
           MOVE  2 TO LIN-NUM.
           MOVE 73 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  7 TO LIN-NUM.
           MOVE 67 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 70 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 73 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 1216 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE INTER.
           DELETE FILE INTER.
           IF TOTAL-COUNTER(1) > 0
           OR TOTAL-COUNTER(2) > 0
              PERFORM TOTAUX
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".

