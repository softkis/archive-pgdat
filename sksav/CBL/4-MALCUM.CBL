      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-MALCUM CUMUL MALADIES 12 MOIS             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-MALCUM.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

           COPY "JOURS.REC".
           COPY "LIVRE.REC".
       01  MOIS         PIC 99.
       01  IDX          PIC 9999 VALUE 0.
       01  IDX-1        PIC 9999 VALUE 0.
       01  action       PIC 9.
       01  NX-KEY       PIC 9(4) COMP-1 VALUE 66.
       01  FAKE-KEY     PIC 9(4) COMP-1 VALUE 13.
       01  SAVE-MOIS    PIC 99 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "MALADIE.CUM".

       PROCEDURE DIVISION USING 
                          LINK-V 
                          REG-RECORD 
                          MC-RECORD.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-MALCUM.

           MOVE LNK-MOIS   TO SAVE-MOIS.
           INITIALIZE MC-RECORD JRS-RECORD IDX-1.
           SUBTRACT 1 FROM LNK-SUFFIX.
           PERFORM READ-JRS THRU READ-JRS-END VARYING LNK-MOIS
           FROM LNK-MOIS BY 1 UNTIL LNK-MOIS > 12.
           ADD 1 TO LNK-SUFFIX.
           PERFORM READ-JRS THRU READ-JRS-END VARYING LNK-MOIS
           FROM 1 BY 1 UNTIL LNK-MOIS = SAVE-MOIS.
           PERFORM EXIT-PROGRAM.

       READ-JRS.
           MOVE LNK-MOIS TO JRS-MOIS.
           ADD 1 TO IDX-1.
           MOVE LNK-MOIS TO MC-MOIS(IDX-1).
       READ-JRS-1.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD NX-KEY.
           IF JRS-FIRME = 0
           OR LNK-MOIS NOT = JRS-MOIS
              GO READ-JRS-END
           END-IF.
           IF JRS-COMPLEMENT > 0
              GO READ-JRS-1.
           IF LNK-ANNEE < 2009
           EVALUATE JRS-OCCUPATION
             WHEN 1 THRU 9 PERFORM CUMUL
             WHEN 11 PERFORM CUMUL-JRS 
                     VARYING IDX FROM 1 BY 1 UNTIL IDX > 31
           END-EVALUATE.
           IF LNK-ANNEE > 2008
           EVALUATE JRS-OCCUPATION
             WHEN 1 THRU 5 PERFORM CUMUL
             WHEN 6 THRU 7 PERFORM CUMUL-JRS 
                     VARYING IDX FROM 1 BY 1 UNTIL IDX > 31
             WHEN 11 PERFORM CUMUL-JRS 
                     VARYING IDX FROM 1 BY 1 UNTIL IDX > 31
           END-EVALUATE.
           GO READ-JRS-1.
       READ-JRS-END.
           EXIT.

       CUMUL.
           MOVE JRS-OCCUPATION TO LNK-SUITE.
           INITIALIZE L-RECORD.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY
           IF  L-FLAG(4) = 0
           AND L-FLAG(6) = 0
           AND L-FLAG(7) = 0
              PERFORM CUMUL-JRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.

       CUMUL-JRS.
           IF JRS-HRS(IDX) NOT = 0
              ADD 1 TO MC-JRS(IDX-1) MC-JRS(13).
           IF JRS-HRS(IDX) > 0
              ADD JRS-HRS(IDX) TO MC-HRS(IDX-1) MC-HRS(13).

       EXIT-PROGRAM.
           MOVE 13 TO MC-MOIS(13).
           MOVE 0 TO LNK-SUITE.
           EXIT PROGRAM.
 
