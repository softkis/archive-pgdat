      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-BANQP MODULE GENERAL LECTURE BANQUE PERSON�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-BANQP.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "BANQP.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "BANQP.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  IDX-1    PIC 9 VALUE 0.
       01  IDX-2    PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "BANQP.LNK".
           COPY "REGISTRE.REC".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V REG-RECORD LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON BANQP.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-REG.
       
           IF NOT-OPEN = 0
              OPEN I-O BANQP
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO BQP-RECORD.
           MOVE FR-KEY TO BQP-FIRME.
           MOVE REG-PERSON TO BQP-PERSON.
           IF EXC-KEY = 99
              IF LNK-SQL = "Y" 
                 CALL "9-BANQP" USING LINK-V BQP-RECORD EXC-KEY 
              END-IF
              WRITE BQP-RECORD INVALID REWRITE BQP-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.
           MOVE 0 TO LNK-VAL.
           IF BQP-TYPE = 0
              MOVE 1 TO BQP-SUITE 
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ BANQP PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ BANQP NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 0 PERFORM START-3
               READ BANQP NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ BANQP NO LOCK INVALID 
                IF BQP-TYPE = 1 
                   PERFORM ACOMPTE
                ELSE
                   INITIALIZE BQP-REC-DET
                   MOVE 111111111111 TO BQP-PERIODE
                   MOVE "C" TO BQP-BANQUE
                END-IF
                END-READ
              GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY     NOT = BQP-FIRME 
           OR REG-PERSON NOT = BQP-PERSON
              GO EXIT-1
           END-IF.
           MOVE BQP-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START BANQP KEY < BQP-KEY INVALID GO EXIT-1.
       START-2.
           START BANQP KEY > BQP-KEY INVALID GO EXIT-1.
       START-3.
           START BANQP KEY >= BQP-KEY INVALID GO EXIT-1.

       ACOMPTE.
           MOVE BQP-TYPE  TO IDX-1.
           MOVE BQP-SUITE TO IDX-2.
           MOVE 0 TO BQP-TYPE.
           MOVE 1 TO BQP-SUITE.
           READ BANQP NO LOCK INVALID 
                   INITIALIZE BQP-REC-DET
                   MOVE 111111111111 TO BQP-PERIODE
                   MOVE "C" TO BQP-BANQUE END-READ.
           MOVE IDX-1 TO BQP-TYPE.
           MOVE IDX-2 TO BQP-SUITE.

