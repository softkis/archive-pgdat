      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-CSPMAL CREER CODE SALAIRE MALADIE CAISSE  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-CSPMAL.

       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

           COPY "CODPAR.REC".
           COPY "CODPAIE.REC".
       01  NOT-OPEN             PIC 9 VALUE 0.
       01  IDX                  PIC 9.
       01  ACTION               PIC X.
       01  HELP-1               PIC 9(10)V999 COMP-3.
       01  DEL-KEY              PIC 9(4) COMP-1 VALUE 98.
       01  ESC-KEY              PIC 9(4) COMP-1.
       01  WR-KEY               PIC 9(4) COMP-1 VALUE 99.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "LIVRE.REC".

       PROCEDURE DIVISION USING LINK-V REG-RECORD L-RECORD.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-CSPMAL.
       
           CALL "6-GCP" USING LINK-V CP-RECORD.
           INITIALIZE CSP-RECORD.
           MOVE COD-PAR(LNK-SUITE, 1) TO CSP-CODE
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD DEL-KEY.
           IF L-IMPOSABLE-BRUT = 0
              EXIT PROGRAM.
           IF LNK-SUITE = 10
              MOVE 1 TO L-FLAG-AVANCE 
           END-IF.
           MOVE L-JRS-SOC-TOT  TO CSP-DONNEE-1.
           MOVE L-JRS-IMP-TOT  TO CSP-DONNEE-2.
           IF L-JRS-SOC-PREF > 0
              MOVE L-JRS-SOC-PREF TO CSP-DONNEE-1.
           IF L-JRS-IMP-PREF > 0
              MOVE L-JRS-IMP-PREF TO CSP-DONNEE-2.
           MOVE L-IMPOSABLE-BRUT  TO CSP-UNITAIRE.
           COMPUTE CSP-UNITE = L-UNI-SALAIRE + L-UNI-HRS-SUP.
           IF L-FLAG-AVANCE = 0
              MOVE L-A-PAYER   TO CSP-TOTAL
              MOVE L-IMPOT     TO CSP-DONNEE(7)
           ELSE
              MOVE L-A-PAYER   TO CSP-DONNEE(8)
           END-IF.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.
           EXIT PROGRAM.

           COPY "XACTION.CPY".
