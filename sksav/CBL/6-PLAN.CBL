      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-PLAN MODULE GENERAL LECTURE PLAN COMPTABLE�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-PLAN.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PLAN.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "PLAN.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN  PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PLAN.LNK".

       01  EXC-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PLAN .
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-PLAN.
       
           IF NOT-OPEN = 0
              OPEN I-O PLAN
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO PLAN-RECORD.
           IF EXC-KEY = 99
              WRITE PLAN-RECORD INVALID REWRITE PLAN-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.
           
           EVALUATE EXC-KEY 
               WHEN 66 PERFORM START-1
               READ PLAN NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN 65 PERFORM START-2
               READ PLAN PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN OTHER GO EXIT-3
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE PLAN-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-3.
           READ PLAN NO LOCK INVALID INITIALIZE PLAN-REC-DET.
           GO EXIT-2.

       START-1.
           START PLAN KEY > PLAN-KEY INVALID GO EXIT-1.
       START-2.
           START PLAN KEY < PLAN-KEY INVALID GO EXIT-1.

