      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-PRIME1 IMPRESSION / CREATION              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-PRIME1.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM130.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  PRECISION             PIC 9 VALUE 0.
       01  JOB-STANDARD          PIC X(10) VALUE "130       ".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "TAUXCC.REC".
           COPY "STATUT.REC".
           COPY "LIVRE.REC".
           COPY "COUT.REC".
           COPY "HORSEM.REC".
           COPY "CODPAR.REC".
           COPY "CODPAIE.REC".
           COPY "CONTRAT.REC".
           COPY "IMPRLOG.REC".
           COPY "LIVRE.CUM".
           COPY "CCOL.REC".
           COPY "CALEN.REC".
           COPY "CONGE.REC".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  TIRET-TEXTE           PIC X VALUE "�".

       01  PERIODE               PIC 99 VALUE 0.    
       01  TIPE                  PIC 99 VALUE 1.    
       01  DETAILS.
           02 N-GRAND            PIC 9(4)V99.
           02 HRS-THEO           PIC 9(4)V99.
           02 HRS-TRAV           PIC 9(4)V99.
           02 FERIE              PIC 9(3)V99.
           02 CONGE              PIC 9(4)V99.
           02 MALADIE            PIC 9(4)V99.
           02 BASE               PIC 9(6)V99.
           02 PRIME              PIC 9(6)V99.
       01  CREATE-CODE           PIC X VALUE "N".


       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".PR1".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z2Z2 PIC ZZ,ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM. 

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-PRIME1.

           MOVE 1 TO STATUT.
           PERFORM AFFICHAGE-ECRAN .
           CALL "0-TODAY" USING TODAY.
           CALL "6-GCP" USING LINK-V CP-RECORD.
           MOVE FR-ACCIDENT TO LNK-NUM HE-Z2.
           DISPLAY HE-Z2 LINE 18 POSITION 4.
           MOVE "AC" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           DISPLAY LNK-TEXT LINE 18 POSITION  9 SIZE 40.
           INITIALIZE CC-RECORD LIN-NUM LIN-IDX.
           CALL "6-TAUXCC" USING LINK-V CC-RECORD.
           CANCEL "6-TAUXCC".
           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.

           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT CREATE-CODE
             LINE 16 POSITION 35 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           IF CREATE-CODE = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM READ-CONTRAT.
           IF LNK-MOIS NOT = 12
              IF  CON-FIN-A NOT = LNK-ANNEE
              AND CON-FIN-M NOT = LNK-MOIS
                  GO READ-PERSON-1
               END-IF
           END-IF.

           PERFORM CAR-RECENTE.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           PERFORM FULL-PROCESS.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.
           PERFORM END-PROGRAM.
                
       FULL-PROCESS.
           INITIALIZE DETAILS.
           PERFORM TEST-LINE.
           PERFORM FILL-FILES.
           PERFORM DIS-HE-01.

       TEST-LINE.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM FILL-FIRME
              COMPUTE LIN-NUM = LIN-IDX
           END-IF.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD CONGE-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD NUL-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           MOVE CCOL-CALENDRIER TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.


       LIVRE.
           ADD 1 TO LNK-MOIS.
           CALL "4-LPCUM" USING LINK-V REG-RECORD LCUM-RECORD.
           SUBTRACT 1 FROM LNK-MOIS.
MAL        ADD LCUM-UNITE(1, 150) TO MALADIE.
TRA        ADD LCUM-UNITE(1, 100) TO HRS-TRAV.
REP        ADD LCUM-UNITE(1,  31) TO HRS-TRAV.
SUP        ADD LCUM-UNITE(1,  95) TO HRS-TRAV.

TRAV       ADD LCUM-DEB(1, 100) TO BASE.
SUP        ADD LCUM-DEB(1,  95) TO BASE.
REC        ADD LCUM-DEB(1,  31) TO BASE.

           ADD CONGE-AN     TO CONGE.
           ADD CONGE-AJUSTE TO CONGE.

           MOVE LNK-MOIS TO IDX-1.
           PERFORM MOIS VARYING LNK-MOIS FROM 1 BY 1 UNTIL LNK-MOIS > 
           IDX-1.
           MOVE IDX-1 TO LNK-MOIS.
           COMPUTE N-GRAND = HRS-THEO - FERIE - CONGE.

           COMPUTE PRIME = BASE * HRS-TRAV / N-GRAND 
                                * CC-PA(TIPE) / 100 + ,005.

       MOIS.
           INITIALIZE CAR-RECORD HJS-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-GHJS"  USING LINK-V HJS-RECORD CAR-RECORD PRESENCES.
           PERFORM HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 
           MOIS-JRS(LNK-MOIS).

       HEURES.
           IF PRES-JOUR(LNK-MOIS, IDX) > 0
              PERFORM F-H
              IF CAL-JOUR(LNK-MOIS, IDX) = 1 
                 COMPUTE SH-00 = HJS-HEURES(8) / 5
                 ADD SH-00 TO FERIE
              END-IF
           END-IF.

       F-H.
           MOVE SEM-IDX(LNK-MOIS, IDX) TO IDX-3.
           ADD HJS-HEURES(IDX-3) TO HRS-THEO.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD PV-KEY.

       FILL-FILES.

      * DONNEES PERSONNE
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  4 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           PERFORM FILL-DECOMPTE.
           PERFORM LIVRE.

           COMPUTE VH-00 = BASE.
           MOVE 55 TO COL-NUM.
           MOVE 5  TO CAR-NUM.
           MOVE 2  TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = HRS-TRAV.
           MOVE 67 TO COL-NUM.
           MOVE 4  TO CAR-NUM.
           MOVE 2  TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = HRS-THEO.
           MOVE 78 TO COL-NUM.
           MOVE 4  TO CAR-NUM.
           MOVE 2  TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = FERIE.
           MOVE 89 TO COL-NUM.
           MOVE 4  TO CAR-NUM.
           MOVE 2  TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = CONGE.
           MOVE 100 TO COL-NUM.
           MOVE 4  TO CAR-NUM.
           MOVE 2  TO DEC-NUM.
           PERFORM FILL-FORM.

           MOVE CC-PA(TIPE) TO VH-00.
           MOVE 111 TO COL-NUM.
           MOVE 3 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

      *    absence volontaire
           IF LCUM-UNITE(1, 40) NOT = 0
              MOVE 0 TO PRIME 
              MOVE "- ! -" TO ALPHA-TEXTE
              MOVE 48 TO COL-NUM
              PERFORM FILL-FORM
           ELSE
              MOVE PRIME TO VH-00
              MOVE 121 TO COL-NUM
              MOVE 5 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              PERFORM FILL-FORM.

           MOVE 5 TO COL-NUM.
           ADD 1 TO LIN-NUM.
           IF LIN-NUM < 70
              PERFORM TIRET UNTIL COL-NUM > 130.
           ADD 1 TO LIN-NUM.
           IF  CREATE-CODE  NOT = "N" 
           AND COD-PAR(197, 1) NOT = 0
               PERFORM WRITE-CS.

       WRITE-CS.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY     TO CSP-FIRME. 
           MOVE LNK-MOIS   TO CSP-MOIS.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE COD-PAR(197, 1) TO CSP-CODE. 
           MOVE PRIME TO CSP-TOTAL.
           IF PRIME NOT = 0 
              CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY
           ELSE
              CALL "4-CSPDEL" USING LINK-V CSP-RECORD
           END-IF.
           CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.


       FILL-DECOMPTE.
           MOVE   4 TO CAR-NUM.
           MOVE 119 TO COL-NUM.
           COMPUTE VH-00 = PERIODE
           PERFORM FILL-FORM.

       FILL-FIRME.

      *    PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  3 TO CAR-NUM.
           MOVE  6 TO LIN-NUM.
           MOVE 118 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

           MOVE  2 TO LIN-NUM.
           MOVE 80 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.

           PERFORM FILL-FORM.
           MOVE  4 TO LIN-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE 111 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 114 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 117 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE 4 TO LIN-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE 8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           MOVE 15 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 5 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 6 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-CODE-POST TO VH-00.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 27 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 6 POSITION 47 SIZE 33.
           IF A-N = "N"
              DISPLAY SPACES LINE 6 POSITION 35 SIZE 10.
       DIS-HE-STAT.
           DISPLAY STAT-NOM  LINE 9 POSITION 35.
       DIS-HE-05.
           MOVE COUT TO HE-Z4.
           DISPLAY HE-Z4  LINE 10 POSITION 29.
           DISPLAY COUT-NOM LINE 10 POSITION 35.
       DIS-HE-END.
           EXIT.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 512 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY "*" LINE 1 POSITION  1.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF COUNTER > 0 
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".



