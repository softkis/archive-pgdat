      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-CSC IMPRESSION LISTE CODES SAISIS         �
      *  � PAR PERSONNE DE MOIS DEBUT A MOIS FIN                 �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CSC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".
           COPY "TRIPR.FC".
           COPY "CODPAIE.FC".
           SELECT OPTIONAL CODCUM ASSIGN TO RANDOM, CSC-NAME,
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is CSH-KEY,
                  STATUS FS-HELP.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM130.FDE".
           COPY "TRIPR.FDE".
           COPY "CODPAIE.FDE".
       FD  CODCUM 
           LABEL RECORD STANDARD
           DATA RECORD CSH-RECORD.
      *  ENREGISTREMENT FICHIER CODE SALAIRE CUMULES 
      
       01  CSH-RECORD.
           02 CSH-KEY.
              03 CSH-PERSON   PIC 9(6).
              03 CSH-CODE     PIC 9(4).
              03 CSH-CODE-R REDEFINES CSH-CODE. 
                 04 CSH-A     PIC 99.
                 04 CSH-B     PIC 99.
              03 CSH-STATUT   PIC 9.
              03 CSH-DEPT     PIC 9999.

           02 CSH-REC-DET.
              03 CSH-COMPTEUR PIC 9(4).
              03 CSH-UNITE    PIC 9(9)V999.
              03 CSH-TOTAL    PIC 9(9)V999.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "130       ".
       01  CHOIX-MAX             PIC 99 VALUE 11.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "LIVRE.REC".
           COPY "METIER.REC".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".
SU         COPY "CODTXT.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.
       01  LIMITEUR              PIC X(6).

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.
       01  CS-IDX                PIC 99 VALUE 00.
       01  UNITES                PIC X VALUE "N".
       01  RUN-NR                PIC 9 VALUE 0.
       01  TOTAL-YN              PIC X VALUE "N".
       01  COLONNE-BASE          PIC 999 VALUE 0.    
       01  POINT-IDX             PIC 99.
       01  COMPTEUR              PIC 9999 COMP-1.


       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

           COPY "V-VH00.CPY".
       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".CSC".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

       01  CSC-NAME.
           02 FILLER             PIC XXX VALUE "ICS".
           02 FIRME-CSC          PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-CSC           PIC XXX.

           COPY "V-VAR.CPY".
        
       01  PERSONNE              PIC 9(6) VALUE 0.
       01  BEG-PERSON            PIC 9(6).
       01  BEG-MATCHCODE         PIC X(10).
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       01  HELP-CS.
           02 H-CS OCCURS 120.
              03 H-CODE PIC 9999.

       01  HELP-CUMUL.
           02 HELP-CS-TEST PIC 9.
           02 HELP-CS-TOT  PIC 9(8)V99 OCCURS 60.
           02 HELP-CS-UNIT PIC 9(8)V99 OCCURS 60.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR
               CODPAIE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CSC.
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE LNK-SUFFIX TO ANNEE-PAIE.
           MOVE LNK-MOIS TO SAVE-MOIS MOIS-DEBUT MOIS-FIN.
           MOVE LNK-USER TO USER-CSC.
           MOVE FR-KEY   TO FIRME-CSC.
           OPEN INPUT CODPAIE.
           DELETE FILE CODCUM.
           OPEN I-O CODCUM.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TEST-EXTENSION.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7 THRU 8
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 10    MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 11    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN 11 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-8
           WHEN  9 PERFORM APRES-9
           WHEN 10 PERFORM APRES-SORT
           WHEN 11 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-6.
           ACCEPT TOTAL-YN 
             LINE 13 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           ACCEPT MOIS-DEBUT
             LINE 15 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT MOIS-FIN
             LINE 17 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-9.
           ACCEPT UNITES 
             LINE 20 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           IF TOTAL-YN = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-06.

       APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-07.

       APRES-8.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.

       APRES-9.
           IF UNITES = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-09.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 THRU 6
      *             MOVE 1 TO LNK-STATUS
                    INITIALIZE HELP-CUMUL 
                    IF EXC-KEY = 6
                       MOVE 1 TO UNITES
                    END-IF
                    PERFORM TRAITEMENT-RUN
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM DIS-HE-01.
           IF RUN-NR = 0
              PERFORM CAR-RECENTE
              IF CAR-IN-ACTIF = 1 
                 GO READ-PERSON-3
              END-IF
              IF  STATUT > 0
              AND STATUT NOT = CAR-STATUT  
                 GO READ-PERSON-3
              END-IF
              IF  COUT > 0 
              AND COUT NOT = CAR-COUT
                 GO READ-PERSON-3
              END-IF
              PERFORM GET-CODPAIE VARYING MOIS-COURANT FROM 
              MOIS-DEBUT BY 1 UNTIL MOIS-COURANT > MOIS-FIN
           ELSE
              PERFORM WRITE-LIST THRU WRITE-LIST-END
           END-IF.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.
           EXIT.


       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.


       METIER.
           MOVE CAR-METIER TO MET-CODE.
           MOVE "F "       TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           MOVE 20 TO LIN-NUM.
           MOVE 10 TO COL-NUM.
           PERFORM FILL-FORM.

       DETAIL-CHOIX.
           MOVE  5 TO LIN-NUM.
           MOVE 75 TO COL-NUM.
           MOVE TEST-NUMBER TO LNK-POSITION.
           MOVE TEST-ALPHA  TO LNK-TEXT.
           MOVE CHOIX       TO LNK-NUM.
           CALL "4-CHOIX" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       TRAITEMENT-RUN.
           MOVE REG-PERSON    TO BEG-PERSON.
           MOVE REG-MATCHCODE TO BEG-MATCHCODE.
           IF CHOIX NOT = 0
              PERFORM CALL-SORT 
              MOVE LNK-USER  TO USER-TRIPR
              MOVE FR-KEY TO REG-FIRME FIRME-TRIPR
              OPEN I-O TRIPR
              PERFORM START-TRI
              PERFORM READ-TRI-A THRU READ-TRI-A-END
              PERFORM GET-CS-I
              MOVE 1 TO RUN-NR
              PERFORM START-TRI
              PERFORM READ-TRI THRU READ-TRI-END
              PERFORM END-PROGRAM
           END-IF. 
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.
           PERFORM GET-CS-I.
           MOVE 1 TO RUN-NR.
           MOVE BEG-PERSON TO REG-PERSON.
           MOVE BEG-MATCHCODE TO REG-MATCHCODE.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.
           PERFORM END-PROGRAM.

       GET-CS-I.
           INITIALIZE CSH-RECORD HELP-CS.
           MOVE 999999 TO CSH-PERSON.
           START CODCUM KEY > CSH-KEY INVALID CONTINUE
           NOT INVALID
               PERFORM GET-CS-IDX THRU GET-CS-END.

       GET-CS-IDX.
           READ CODCUM NEXT AT END 
                GO GET-CS-END 
           END-READ.
           ADD 1 TO CS-IDX.
           MOVE CSH-CODE TO H-CODE(CS-IDX)
           GO GET-CS-IDX.
       GET-CS-END.
           EXIT.


       NOM-CS.
           ADD 1 TO IDX-1.
           IF IDX-1 > 59
              GO NOM-CS-END.
           IF H-CODE(IDX-1) = 0
              GO NOM-CS-END.
           MOVE H-CODE(IDX-1) TO CTX-NUMBER VH-00.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           ADD 1 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE  6 TO COL-NUM.
           MOVE CTX-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           GO NOM-CS.
       NOM-CS-END.
           EXIT.

       GET-LINE-IDX.
           ADD 1 TO IDX-2.
           IF H-CODE(IDX-2) = 0 
              MOVE 0 TO IDX-2
              GO GET-LINE-END.
           IF H-CODE(IDX-2) = CSH-CODE 
              GO GET-LINE-END.
           GO GET-LINE-IDX.
       GET-LINE-END.
           IF IDX-2 NOT = 0
              ADD CSH-TOTAL TO HELP-CS-TOT(IDX-2)
              ADD CSH-UNITE TO HELP-CS-UNIT(IDX-2)
              MOVE 1 TO HELP-CS-TEST
              ADD 11 TO IDX-2.    

       FILL-TOTAL.
           ADD 1 TO IDX-3.
           IF H-CODE(IDX-3) = 0 
              GO FILL-TOT-END.
           IF UNITES = "N"
              IF HELP-CS-TOT(IDX-3) = 0
                 GO FILL-TOTAL
              END-IF
           ELSE
              IF HELP-CS-UNIT(IDX-3) = 0
                 GO FILL-TOTAL
              END-IF
           END-IF.
           MOVE IDX-3 TO LIN-NUM.
           ADD 11 TO LIN-NUM.
           MOVE COLONNE-BASE TO COL-NUM.
           MOVE 9 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           IF UNITES = "N"
              MOVE HELP-CS-TOT(IDX-3) TO VH-00
           ELSE
              MOVE HELP-CS-UNIT(IDX-3) TO VH-00
           END-IF.
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.
           GO FILL-TOTAL.
       FILL-TOT-END.
           PERFORM TRANSMET.
           INITIALIZE HELP-CUMUL.
           MOVE 0 TO COLONNE-BASE.

       GET-CODPAIE.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY TO CSP-FIRME.
           MOVE MOIS-COURANT TO CSP-MOIS.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE IMPL-BODY-LINE TO LIN-NUM.
           START CODPAIE KEY > CSP-KEY INVALID
                PERFORM READ-CODPAIE-END
                NOT INVALID 
                PERFORM READ-CODPAIE THRU READ-CODPAIE-END.

       READ-CODPAIE.
           READ CODPAIE NEXT AT END GO READ-CODPAIE-END.
           IF FR-KEY       NOT = CSP-FIRME
           OR REG-PERSON   NOT = CSP-PERSON
           OR MOIS-COURANT NOT = CSP-MOIS 
              GO READ-CODPAIE-END
           END-IF.
           IF CSP-CODE > 700 AND < 1000
              GO READ-CODPAIE.
           IF CSP-SUITE NOT = 0
           OR CSP-CODE > 2200
              GO READ-CODPAIE.
           PERFORM COMPTER-ENREGISTRER.
           GO READ-CODPAIE.
       READ-CODPAIE-END.
           EXIT.
           
       COMPTER-ENREGISTRER.
           PERFORM DIS-HE-01.
           MOVE REG-PERSON TO CSH-PERSON.
           MOVE CSP-CODE TO CSH-CODE.
           IF CSP-CODE > 1000
              MOVE 0 TO CSH-B
           END-IF.
           PERFORM ENREGISTRER.
           MOVE 999999 TO CSH-PERSON.
           PERFORM ENREGISTRER.

       ENREGISTRER.
           READ CODCUM INVALID INITIALIZE CSH-REC-DET END-READ.
           ADD 1 TO CSH-COMPTEUR COMPTEUR.
           ADD CSP-UNITE TO CSH-UNITE.
           ADD CSP-TOTAL TO CSH-TOTAL.
           IF UNITES = "N"
              IF CSH-TOTAL > 0
                 PERFORM WRITE-CSH
              END-IF
           ELSE
              IF CSH-UNITE > 0
                 PERFORM WRITE-CSH
              END-IF
           END-IF.

       WRITE-CSH.
           WRITE CSH-RECORD INVALID REWRITE CSH-RECORD.

       WRITE-LIST.
           IF  CHOIX > 0
           AND CHOIX < 90
              IF TEST-ALPHA  NOT = SPACES
              OR TEST-NUMBER NOT = 0
                 IF TRIPR-CHOIX NOT = TEST-EXTENSION
                    PERFORM FILL-TOT
                    INITIALIZE TEST-EXTENSION
                 END-IF
              END-IF
              IF  TEST-ALPHA  = SPACES
              AND TEST-NUMBER = 0
                  MOVE TRIPR-CHOIX TO TEST-EXTENSION
              END-IF
           END-IF.
           INITIALIZE CSH-RECORD.
           MOVE REG-PERSON TO CSH-PERSON.
           START CODCUM KEY > CSH-KEY INVALID GO WRITE-LIST-END.
       READ-CODCUM.
           READ CODCUM NEXT AT END 
                GO WRITE-LIST-END
           END-READ.
           IF REG-PERSON NOT = CSH-PERSON
              GO WRITE-LIST-END
           END-IF.
           MOVE 0 TO IDX-2.
           PERFORM GET-LINE-IDX THRU GET-LINE-END.
           IF IDX-2 = 0
              GO READ-CODCUM.
           IF TOTAL-YN NOT = "N" 
              GO READ-CODCUM.
           IF REG-PERSON NOT = PERSONNE
              MOVE REG-PERSON TO PERSONNE
              PERFORM FILL-FILES.
           MOVE COLONNE-BASE TO COL-NUM.
           MOVE 9 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           IF UNITES = "N"
              MOVE CSH-TOTAL TO VH-00 
           ELSE
              MOVE CSH-UNITE TO VH-00 
           END-IF.
           MOVE 1 TO POINTS.
           MOVE IDX-2 TO LIN-NUM
           PERFORM FILL-FORM.
           GO READ-CODCUM.
       WRITE-LIST-END.
           EXIT.

       ENTETE.
           PERFORM PAGE-DATE.

      * DONNEES FIRME
           COMPUTE LIN-NUM = 2.
           MOVE  4 TO CAR-NUM.
           MOVE 75 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 80 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 3.
           MOVE 75 TO COL-NUM.
           MOVE FR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 80 TO COL-NUM.
           MOVE FR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 4.
           MOVE 75 TO COL-NUM.
           MOVE FR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  6 TO CAR-NUM.
           MOVE FR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           IF COUT > 0 
              MOVE FR-KEY TO COUT-FIRME 
              MOVE COUT TO COUT-NUMBER VH-00
              CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY
              COMPUTE LIN-NUM = 2
              MOVE  4 TO CAR-NUM
              MOVE 40 TO COL-NUM
              PERFORM FILL-FORM
              MOVE COUT-NOM TO ALPHA-TEXTE
              MOVE 50 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.

           IF STATUT > 0 
              MOVE STATUT TO STAT-CODE VH-00
              CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY
              COMPUTE LIN-NUM = 3
              MOVE STAT-NOM TO ALPHA-TEXTE
              MOVE 50 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.
      *    DATE EDITION
           COMPUTE LIN-NUM = 5.
           MOVE 19 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 22 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 25 TO COL-NUM.
           MOVE TODAY-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 11 TO LIN-NUM.
           MOVE 0 TO IDX-1.
           PERFORM NOM-CS THRU NOM-CS-END.

       FILL-FILES.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO COLONNE-BASE
              MOVE 1 TO COUNTER
           END-IF.
           IF COLONNE-BASE > 100
              PERFORM TRANSMET
              MOVE 0 TO COLONNE-BASE
           END-IF.
           IF COLONNE-BASE = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              ADD 12 TO COLONNE-BASE 
           END-IF.
           ADD 20 TO COLONNE-BASE.
           IF TOTAL-YN = "N" 
              PERFORM FILL-STATIC.

       FILL-TOT.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO COLONNE-BASE
              MOVE 1 TO COUNTER
           END-IF.
           IF COLONNE-BASE > 100
              PERFORM TRANSMET
              MOVE 0 TO COLONNE-BASE
           END-IF.
           IF COLONNE-BASE = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              ADD 12 TO COLONNE-BASE 
           END-IF.
           ADD 20 TO COLONNE-BASE.
           MOVE 0 TO IDX-3 HELP-CS-TEST.
           PERFORM FILL-TOTAL THRU FILL-TOT-END.

       FILL-STATIC.
      * DONNEES PERSONNE
           MOVE COLONNE-BASE TO COL-NUM.
           MOVE 6 TO CAR-NUM.
           MOVE 7 TO LIN-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.

           MOVE COLONNE-BASE TO COL-NUM.
           ADD 1 TO LIN-NUM.
           MOVE PR-NOM TO  ALPHA-TEXTE.
           MOVE 19 TO POINT-IDX.
           STRING STOPS DELIMITED BY SIZE
           INTO ALPHA-TEXTE POINTER POINT-IDX
           ON OVERFLOW CONTINUE END-STRING.
           PERFORM FILL-FORM.

           MOVE COLONNE-BASE TO COL-NUM.
           ADD 1 TO LIN-NUM.
           MOVE PR-PRENOM TO  ALPHA-TEXTE.
           MOVE 19 TO POINT-IDX.
           STRING STOPS DELIMITED BY SIZE
           INTO ALPHA-TEXTE POINTER POINT-IDX
           ON OVERFLOW CONTINUE END-STRING.
           PERFORM FILL-FORM.

       PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  4 TO CAR-NUM.
           MOVE  5 TO LIN-NUM.
           MOVE 64 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE 40 TO COL-NUM.
           IF UNITES NOT = "N" 
              MOVE "QUANT" TO ALPHA-TEXTE
           ELSE
              MOVE "EURO" TO ALPHA-TEXTE
           END-IF.
           PERFORM FILL-FORM.

           MOVE  3 TO LIN-NUM.
           MOVE MOIS-DEBUT TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           MOVE 17 TO COL-NUM.
           PERFORM FILL-FORM
           IF MOIS-DEBUT NOT = MOIS-FIN
              ADD  2 TO COL-NUM 
              MOVE "-" TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE MOIS-FIN TO LNK-NUM
              PERFORM MOIS-NOM-1
              ADD 2 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.
           MOVE LNK-ANNEE TO VH-00
           MOVE  3 TO LIN-NUM.
           MOVE  4 TO CAR-NUM
           ADD   2 TO COL-NUM.
           PERFORM FILL-FORM.
           IF CHOIX NOT = 0
              PERFORM DETAIL-CHOIX.

       READ-TRI-A.
           READ TRIPR NEXT AT END 
                GO READ-TRI-A-END
           END-READ.
           MOVE TRIPR-PERSON TO REG-PERSON.
           PERFORM NEXT-REGIS.
           PERFORM READ-PERSON-1 THRU READ-PERSON-2.
           GO READ-TRI-A.
       READ-TRI-A-END.
           EXIT.

           COPY "XDIS.CPY".
       DIS-HE-06.
           DISPLAY TOTAL-YN LINE 13 POSITION 32.
       DIS-HE-07.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 15 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 15351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-08.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 17 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 17351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-09.
           DISPLAY UNITES LINE 20 POSITION 32.
       DIS-HE-END.
           EXIT.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 1513 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF HELP-CS-TEST = 1
              IF TOTAL-YN NOT = "N" 
                 PERFORM FILL-FILES
              END-IF
              PERFORM FILL-TOT
           END-IF.
           CLOSE CODCUM.
           DELETE FILE CODCUM.
           CLOSE CODPAIE.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".
           COPY "XSORT.CPY".

