      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-INDEX INDEX COUT DE VIE                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-INDEX.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "INDEX.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "INDEX.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX         PIC 99 VALUE 5.

       01  HE-INPUT-PARAMETER.
      *    LINE COL SIZE HE-SCREEN    LLCCSZHELP
           02 IP-101 PIC 9(10)  VALUE 0328040000.
           02 IP-102 PIC 9(10)  VALUE 0430020000.
           02 IP-103 PIC 9(10)  VALUE 0529080000.
           02 IP-104 PIC 9(10)  VALUE 0729080000.

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
              02 I-PP OCCURS 4.
                 04 IP-LINE PIC 99.
                 04 IP-COL  PIC 99.
                 04 IP-SIZE PIC 99.
                 04 IP-HELP PIC 9999.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
       01  INDEX-TRF    PIC 9999V99 COMP-3.
       01  VH-00        PIC 9999V9999.
       01  SH-02        PIC 9999V99.


       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z3Z4 PIC ZZZ,ZZZZ BLANK WHEN ZERO.
           02 HE-Z4Z2 PIC ZZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-ZF PIC Z,Z.
           02 HE-Z4Z4 PIC ZZZZ,ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON INDEX-VIE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-INDEX.
       
           OPEN I-O   INDEX-VIE.

           PERFORM AFFICHAGE-ECRAN.
           PERFORM INDEX-RECENT.
           PERFORM AFFICHAGE-DETAIL.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions
           EVALUATE INDICE-ZONE
               WHEN 1     MOVE 0000000065 TO EXC-KFR (13)
                          MOVE 6600000000 TO EXC-KFR (14)
               WHEN CHOIX-MAX
                          MOVE 0000000005 TO EXC-KFR (1)
                          MOVE 0000080000 TO EXC-KFR (2)
                          MOVE 0052000000 TO EXC-KFR (11)
           END-EVALUATE.

           IF IP-HELP(INDICE-ZONE) > 0
              MOVE 1 TO EXC-KEY-FUN(1) 
           END-IF.
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-1-1 
               WHEN  2 PERFORM AVANT-1-2 
               WHEN  3 PERFORM AVANT-1-3 
               WHEN  4 PERFORM AVANT-1-4 
               WHEN CHOIX-MAX PERFORM AVANT-DEC
           END-EVALUATE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 1 
              MOVE IP-HELP(INDICE-ZONE) TO LNK-VAL
              PERFORM HELP-SCREEN
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              GO TRAITEMENT-ECRAN.
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM APRES-1-1 
               WHEN  2 PERFORM APRES-1-2 
               WHEN  3 PERFORM APRES-1-3 
               WHEN  4 PERFORM APRES-1-4 
               WHEN CHOIX-MAX PERFORM APRES-DEC
            END-EVALUATE.
            PERFORM ECRAN-1.

           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 ADD 1 TO INDICE-ZONE.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.
           ACCEPT INDEX-ANNEE
           LINE     IP-LINE (INDICE-ZONE) 
           POSITION IP-COL  (INDICE-ZONE) 
           SIZE     IP-SIZE (INDICE-ZONE) 
           TAB UPDATE NO BEEP CURSOR  1 HIGH
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           ACCEPT INDEX-MOIS  
           LINE     IP-LINE (INDICE-ZONE) 
           POSITION IP-COL  (INDICE-ZONE) 
           SIZE     IP-SIZE (INDICE-ZONE) 
           TAB UPDATE NO BEEP CURSOR  1 HIGH
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.
           MOVE INDEX-TAUX TO HE-Z3Z2.
           ACCEPT HE-Z3Z2
           LINE     IP-LINE (INDICE-ZONE) 
           POSITION IP-COL  (INDICE-ZONE) 
           SIZE     IP-SIZE (INDICE-ZONE) 
           TAB UPDATE NO BEEP CURSOR  1 HIGH
           ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z3Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z3Z2 TO INDEX-TAUX.
           IF EXC-KEY = 11
              COMPUTE SH-02 =  INDEX-TAUX * 1,025
              MOVE    SH-02 TO INDEX-TAUX 
              GO AVANT-1-3
           END-IF.

       AVANT-1-4.
           MOVE SALMIN-100 TO HE-Z3Z4.
           ACCEPT HE-Z3Z4
           LINE     IP-LINE (INDICE-ZONE) 
           POSITION IP-COL  (INDICE-ZONE) 
           SIZE     IP-SIZE (INDICE-ZONE) 
           TAB UPDATE NO BEEP CURSOR  1 HIGH
           ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z3Z4 REPLACING ALL "." BY ",".
           MOVE HE-Z3Z4 TO SALMIN-100.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

           
       APRES-1-1.
           EVALUATE EXC-KEY
           WHEN  65 PERFORM INDEX-PRECEDENT
           WHEN  66 PERFORM INDEX-SUIVANT
           WHEN OTHER READ INDEX-VIE NO LOCK INVALID CONTINUE END-READ.
           IF INDEX-ANNEE < 1950
              MOVE 1950 TO INDEX-ANNEE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-2.
           IF INDEX-MOIS < 1
              MOVE 1 TO INDEX-MOIS
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INDEX-MOIS > 12
              MOVE 12 TO INDEX-MOIS
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR = 0
              READ INDEX-VIE NO LOCK INVALID 
              MOVE "  " TO LNK-AREA
              MOVE 13 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              END-READ.
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-3.
           COMPUTE INDEX-FACT = INDEX-TAUX / 100.

       APRES-1-4.
           IF SALMIN-100 < 200
              MOVE 1 TO INPUT-ERROR
              MOVE "SL" TO LNK-AREA
              MOVE 64 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
           END-IF.
           PERFORM DIS-E1-4.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO INDEX-TIME
                    MOVE LNK-USER TO INDEX-USER
                    IF LNK-SQL = "Y" 
                       CALL "9-INDEX" USING LINK-V INDEX-RECORD WR-KEY
                    END-IF
                    WRITE INDEX-RECORD INVALID REWRITE INDEX-RECORD
                    END-WRITE   
                    MOVE 1 TO DECISION
            WHEN 8  DELETE INDEX-VIE INVALID CONTINUE END-DELETE
                    IF LNK-SQL = "Y" 
                       CALL "9-INDEX" USING LINK-V INDEX-RECORD DEL-KEY
                    END-IF
                    PERFORM INDEX-PRECEDENT
                    MOVE 1 TO DECISION
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       INDEX-RECENT.
           INITIALIZE INDEX-RECORD NOT-FOUND.
           MOVE 9999 TO INDEX-ANNEE
           PERFORM INDEX-PRECEDENT.

       INDEX-SUIVANT.
           MOVE 0 TO NOT-FOUND .
           START INDEX-VIE KEY > INDEX-KEY INVALID 
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ INDEX-VIE NEXT NO LOCK AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.

       INDEX-PRECEDENT.
           MOVE 0 TO NOT-FOUND.
           START INDEX-VIE KEY < INDEX-KEY INVALID 
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ INDEX-VIE PREVIOUS NO LOCK AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.

       DIS-E1-1.
           DISPLAY INDEX-ANNEE
           LINE     IP-LINE(INDICE-ZONE) 
           POSITION IP-COL (INDICE-ZONE) HIGH.
           IF EXC-KEY NOT = 5 MOVE INDEX-KEY TO LNK-TEXT.

       DIS-E1-2.
           MOVE INDEX-MOIS TO HE-Z2.
           DISPLAY HE-Z2
           LINE     IP-LINE(INDICE-ZONE) 
           POSITION IP-COL (INDICE-ZONE) HIGH.

       DIS-E1-3.
           MOVE INDEX-TAUX TO HE-Z3Z2.
           DISPLAY HE-Z3Z2
           LINE     IP-LINE(INDICE-ZONE) 
           POSITION IP-COL (INDICE-ZONE) HIGH.

       DIS-E1-4.
           MOVE SALMIN-100 TO HE-Z3Z4.
           DISPLAY HE-Z3Z4
           LINE     IP-LINE(INDICE-ZONE) 
           POSITION IP-COL (INDICE-ZONE) HIGH.
           COMPUTE SH-00 = INDEX-FACT * SALMIN-100.
           MOVE SH-00 TO HE-Z4Z4.
           DISPLAY HE-Z4Z4 LINE IP-LINE(INDICE-ZONE) POSITION 45.
           COMPUTE VH-00 = SH-00 / 173 + ,00005.
           MOVE VH-00 TO HE-Z4Z4.
           DISPLAY HE-Z4Z4 LINE 8 POSITION 45.


           COMPUTE SH-00 = SH-00 / 4 + ,005.
           MOVE SH-00 TO HE-Z4Z4.
           DISPLAY HE-Z4Z4 LINE IP-LINE(INDICE-ZONE) POSITION 62 LOW.
           COMPUTE SH-00 = SH-00 / 173.
           MOVE SH-00 TO HE-Z4Z2
           DISPLAY HE-Z4Z2 LINE IP-LINE(INDICE-ZONE) POSITION 72 LOW.

       AFFICHAGE-ECRAN.
           MOVE 5 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           MOVE INDICE-ZONE TO DECISION.
           PERFORM ECRAN-1 VARYING INDICE-ZONE FROM
               1 BY 1 UNTIL INDICE-ZONE = CHOIX-MAX.
           MOVE DECISION TO INDICE-ZONE.

       ECRAN-1.
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM DIS-E1-1 
               WHEN  2 PERFORM DIS-E1-2 
               WHEN  3 PERFORM DIS-E1-3 
               WHEN  4 PERFORM DIS-E1-4 
           END-EVALUATE.

       END-PROGRAM.
           CLOSE INDEX-VIE.
           CALL "1-LINDEX" USING LINK-V.
           CANCEL "1-LINDEX".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
             IF DECISION = 99 
                DISPLAY INDEX-USER LINE 24 POSITION 5
                DISPLAY INDEX-ST-JOUR  LINE 24 POSITION 15
                DISPLAY INDEX-ST-MOIS  LINE 24 POSITION 18
                DISPLAY INDEX-ST-ANNEE LINE 24 POSITION 21
                DISPLAY INDEX-ST-HEURE LINE 24 POSITION 30
                DISPLAY INDEX-ST-MIN   LINE 24 POSITION 33
                GO AVANT-DEC
             END-IF.
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
