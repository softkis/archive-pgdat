      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-BANQUE  GESTION DES BANQUES               �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-BANQUE.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "BANQUE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "BANQUE.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "C-P.REC".
           COPY "PAYS.REC".
           COPY "MESSAGE.REC".

       01  CHOIX-MAX             PIC 99 VALUE 14. 

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
      *袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴�
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON BANQUE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-BANQUE.
       
           OPEN I-O BANQUE.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     IF BQ-PAYS = "L"
                         MOVE 0029000028 TO EXC-KFR(1)
                         MOVE 0000000065 TO EXC-KFR(13) 
                         MOVE 6600000000 TO EXC-KFR(14)
                      END-IF
           WHEN 12    MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 14    MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
           WHEN OTHER MOVE 0012130000 TO EXC-KFR(3)
                      MOVE 0052005400 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-10
      *    WHEN 11 PERFORM AVANT-11
           WHEN 12 PERFORM AVANT-12
           WHEN 13 PERFORM AVANT-13
           WHEN 14 PERFORM AVANT-DEC.


           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN 12 PERFORM APRES-12
           WHEN 14 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT BQ-CODE 
             LINE  4 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT BQ-NOM 
             LINE  5 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.            
           IF BQ-PAYS = SPACES 
              MOVE "L" TO BQ-PAYS
           END-IF.
           ACCEPT BQ-PAYS
             LINE  6 POSITION 30 SIZE  3
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4.            
           ACCEPT BQ-CODE-POST
             LINE  7 POSITION 30 SIZE 8 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-5.            
           ACCEPT BQ-NUMERO  
             LINE  8 POSITION 12 SIZE  5 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-6.            
           ACCEPT BQ-RUE     
             LINE  8 POSITION 30 SIZE  40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.            
           ACCEPT BQ-LOCALITE
             LINE  9 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.            
           ACCEPT BQ-PHONE
             LINE 10 POSITION 30 SIZE 20
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-9.            
           ACCEPT BQ-FAX
             LINE 11 POSITION 30 SIZE 20
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-10.
           ACCEPT BQ-SWIFT
             LINE 12 POSITION 30 SIZE 14
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-11.
           IF BQ-ABBL = SPACES 
              MOVE BQ-SWIFT TO BQ-ABBL
           END-IF.
           ACCEPT BQ-ABBL
             LINE 13 POSITION 30 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-12.
           ACCEPT BQ-FICT 
             LINE 14 POSITION 30 SIZE 1 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-13.
           IF BQ-SWIFT = SPACES
           AND BQ-FICT = 0
           AND BQ-PAYS NOT = "L"
             ACCEPT BQ-BLZ
             LINE 15 POSITION 30 SIZE 12
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-BANQUE
           WHEN   2 CALL "2-BANQUE" USING LINK-V BQ-RECORD
                    PERFORM AFFICHAGE-ECRAN.
           READ BANQUE INVALID INITIALIZE BQ-REC-DET END-READ.
           IF BQ-CODE = SPACES 
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-01 THRU DIS-HE-END.


       APRES-3.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 MOVE BQ-PAYS TO PAYS-CODE
                   PERFORM NEXT-PAYS
                   MOVE PAYS-CODE TO BQ-PAYS
           WHEN  5 MOVE BQ-PAYS TO LNK-TEXT
                   CALL "1-PAYS" USING LINK-V
                   MOVE LNK-TEXT TO BQ-PAYS
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
           WHEN 2 CALL "2-PAYS" USING LINK-V PAYS-RECORD
                   IF PAYS-CODE NOT = SPACES
                   AND PAYS-ISO NOT = SPACES
                      MOVE PAYS-CODE TO BQ-PAYS
                   END-IF
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
           WHEN 3 CALL "2-PAYSC" USING LINK-V PAYS-RECORD
                  IF PAYS-CODE NOT = SPACES
                  AND PAYS-ISO NOT = SPACES
                      MOVE PAYS-CODE TO BQ-PAYS
                  END-IF
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL.
           PERFORM DIS-HE-03.
           IF PAYS-ISO = SPACES
              MOVE 1 TO INPUT-ERROR
           ELSE
              MOVE PAYS-CODE TO BQ-PAYS.

           
       APRES-4.
           IF BQ-PAYS = SPACES
              MOVE "L" TO BQ-PAYS.
           EVALUATE EXC-KEY
           WHEN  2 MOVE "A" TO LNK-A-N
                   CALL "2-CDPOST" USING LINK-V CP-RECORD
                   CANCEL "2-CDPOST"
                   PERFORM AFFICHAGE-ECRAN 
                   IF CP-CODE > 0
                      MOVE CP-CODE TO BQ-CODE-POST 
                      MOVE CP-RUE TO BQ-RUE
                      MOVE CP-LOCALITE TO BQ-LOCALITE
                   END-IF
                   PERFORM AFFICHAGE-DETAIL
           WHEN 65 THRU 66 
                   IF BQ-CODE-POST  NOT = CP-CODE
                      INITIALIZE CP-RECORD
                      MOVE BQ-CODE-POST TO CP-CODE
                      MOVE BQ-LOCALITE TO CP-LOCALITE
                   CALL "6-CDPOST" USING LINK-V CP-RECORD "N" FAKE-KEY
                   END-IF
                   PERFORM NEXT-CDPOST
                   IF CP-CODE > 0
                      MOVE CP-CODE TO BQ-CODE-POST 
                      MOVE CP-RUE TO BQ-RUE
                      MOVE CP-LOCALITE TO BQ-LOCALITE
                   END-IF
                   PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.
           IF BQ-LOCALITE = SPACES
           OR EXC-KEY = 5
              MOVE BQ-CODE-POST TO CP-CODE
              CALL "6-CDPOST" USING LINK-V CP-RECORD "N" FAKE-KEY
              MOVE CP-RUE TO BQ-RUE
              MOVE CP-LOCALITE TO BQ-LOCALITE
              CANCEL "6-CDPOST" 
           END-IF.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       APRES-12.
           IF BQ-FICT > 2
              MOVE 1 TO INPUT-ERROR
              MOVE 2 TO BQ-FICT 
           END-IF.
           MOVE BQ-FICT TO MS-NUMBER.
           MOVE "BQS" TO LNK-AREA.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-MESSAGES
           WHEN   2 CALL "2-MESS" USING LINK-V MS-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.
           MOVE MS-NUMBER TO BQ-FICT.
           PERFORM AFFICHAGE-DETAIL.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 IF MENU-BATCH = 1
                       CALL "L-BANQUE" USING LINK-V BQ-RECORD
                       CANCEL "L-BANQUE"
                    END-IF
                    CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO BQ-TIME
                    MOVE LNK-USER TO BQ-USER
                    IF LNK-SQL = "Y" 
                       CALL "9-BANQUE" USING LINK-V BQ-RECORD WR-KEY
                    END-IF
                    WRITE BQ-RECORD INVALID 
                        REWRITE BQ-RECORD
                    END-WRITE   
                    MOVE BQ-CODE TO LNK-TEXT
                    MOVE 1 TO DECISION
                    PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN  8 IF LNK-SQL = "Y" 
                       CALL "9-BANQUE" USING LINK-V BQ-RECORD DEL-KEY
                    END-IF
                    DELETE BANQUE INVALID CONTINUE END-DELETE
                    INITIALIZE BQ-RECORD
                    MOVE 1 TO DECISION
                    PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       READ-BANQUE.
           MOVE EXC-KEY TO SAVE-KEY.
           MOVE 13 TO EXC-KEY.
           PERFORM NEXT-BANQUE.
           MOVE SAVE-KEY TO EXC-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.
          
       NEXT-PAYS.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD EXC-KEY.
           IF EXC-KEY = 65 OR = 66
              IF PAYS-CODE NOT = SPACES
              AND PAYS-ISO = SPACES
                 GO NEXT-PAYS
              END-IF
           END-IF.

       NEXT-BANQUE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD EXC-KEY.

       NEXT-CDPOST.
           CALL "6-CDPOST" USING LINK-V CP-RECORD "N" EXC-KEY.
                
       DIS-HE-01.
           DISPLAY BQ-CODE LINE  4 POSITION 30.
           IF EXC-KEY NOT = 5 MOVE BQ-CODE TO LNK-TEXT.
       DIS-HE-02.
           DISPLAY BQ-NOM LINE  5 POSITION 30.
       DIS-HE-03.
           DISPLAY BQ-PAYS LINE  6 POSITION 30.
           MOVE BQ-PAYS TO PAYS-CODE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.
           DISPLAY PAYS-NOM LINE 6 POSITION 42 SIZE 24 HIGH.
       DIS-HE-Z8.
           MOVE BQ-CODE-POST   TO HE-Z8.
           DISPLAY HE-Z8  LINE  7 POSITION 30.
       DIS-HE-05.
           DISPLAY BQ-NUMERO LINE  8 POSITION 12.
       DIS-HE-06.
           DISPLAY BQ-RUE LINE  8 POSITION 30.
       DIS-HE-07. 
           DISPLAY BQ-LOCALITE LINE  9 POSITION 30.
       DIS-HE-08. 
           DISPLAY BQ-PHONE LINE 10 POSITION 30.
       DIS-HE-09. 
           DISPLAY BQ-FAX LINE 11 POSITION 30.
       DIS-HE-10.
           DISPLAY BQ-SWIFT LINE 12 POSITION 30.
      *DIS-HE-11.
      *    DISPLAY BQ-ABBL LINE 13 POSITION 30.
       DIS-HE-12.
           MOVE BQ-FICT  TO HE-Z2.
           DISPLAY HE-Z2  LINE 14 POSITION 29.
           MOVE "BQS" TO LNK-AREA.
           MOVE BQ-FICT TO LNK-NUM.
           MOVE 14333000 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-13.
           DISPLAY BQ-BLZ LINE 15 POSITION 30.
       DIS-HE-END.
            EXIT.
       AFFICHAGE-ECRAN.
           MOVE 11 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY SPACES LINE 13 POSITION 1 SIZE 20.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE BANQUE.
           CANCEL "2-BANQUE"
           CANCEL "6-BANQUE"
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
           
