      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 3-DHL IMPRESSION D륳AIL JOURS POSTES DE FRAIS�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-DHL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM215.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM215.FDE".
           COPY "TRIPR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "215       ".
       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CALEN.REC".
           COPY "CCOL.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "LIVRE.REC".
           COPY "METIER.REC".
           COPY "JOURS.REC".
           COPY "OCCUP.REC".
           COPY "OCCTXT.REC".
           COPY "OCCOM.REC".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".
           COPY "POCL.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(215) OCCURS 45.
       01  LIMITEUR              PIC X(7).

       01  COMPTEUR              PIC 99 VALUE 0.
       01  COMPTEUR-1            PIC 99 VALUE 0.
       01  PROD-HEURES.
           04 PROD-HRS PIC S9(6)V99 COMP-3 OCCURS 32.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".HDL".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  DETAIL-YN.
           02 DETAIL-POSTES      PIC X VALUE "N".
           02 DETAIL-OCCUP       PIC X VALUE " ".
       01  DETAIL-YN-R REDEFINES DETAIL-YN.
           02 DETAIL-H           PIC X OCCURS 2.
       01  CHOIX                 PIC 99 VALUE 0.

       01  CHOIX-OCCOM.
           02 C-O OCCURS 14.
              03 COMPL-IDX       PIC 99. 
              03 COMPL-NOM       PIC X(6).

       01  COUNTER-OCO           PIC 99 VALUE 0. 

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-DHL.
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN .
           PERFORM CODES-OCCOM.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 THRU 7 PERFORM AVANT-YN
           WHEN  8 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 THRU 7 PERFORM APRES-YN
           WHEN  8 PERFORM APRES-SORT
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-YN.
           COMPUTE IDX     = INDICE-ZONE - 5.
           COMPUTE LIN-IDX = INDICE-ZONE + 10.
           ACCEPT DETAIL-H(IDX)
             LINE LIN-IDX POSITION 32 SIZE 1 
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE DETAIL-H(IDX) TO ACTION.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-YN.
           IF ACTION = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-3
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-3
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-3
           END-IF.
           PERFORM PREPARATION.
           PERFORM JRS.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.

       PREPARATION.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           MOVE CCOL-CALENDRIER TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.


       METIER.
           MOVE CAR-METIER TO MET-CODE.
           MOVE LNK-LANGUAGE TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 1330 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L215" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.


       END-PROGRAM.
           IF COUNTER > 0 
              MOVE 99 TO LNK-VAL
              CALL "L215" USING LINK-V FORMULAIRE
           END-IF.
           CANCEL "L215".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".
           COPY "XSORT.CPY".

       JRS.
           PERFORM READ-IMPRIMANTE.
           PERFORM READ-FORM.
      *    PERFORM TITRE-OCCOM.
           INITIALIZE JRS-RECORD COMPTEUR COMPTEUR-1 SAVE-KEY
           PROD-HEURES.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE LNK-MOIS   TO JRS-MOIS.
           MOVE REG-PERSON TO JRS-PERSON.
           PERFORM NEXT-JOUR THRU NEXT-JOUR-END.

       NEXT-JOUR.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD SAVE-KEY.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
              GO NEXT-JOUR-END
           END-IF.
           MOVE 66 TO SAVE-KEY.
           IF JRS-POSTE = 0
           AND JRS-COMPLEMENT = 0
              PERFORM ADD-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 32.

           IF  DETAIL-OCCUP = "N"
           AND JRS-POSTE = 0
              GO NEXT-JOUR
           END-IF.
           IF  DETAIL-POSTES = "N"
           AND JRS-POSTE > 0
              GO NEXT-JOUR
           END-IF.
           ADD 1 TO COMPTEUR
           PERFORM FILL-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 32.
           PERFORM FILL-CODE-OCC.
           IF COMPTEUR = 24
              PERFORM FULL-PROCESS
              INITIALIZE COMPTEUR COMPTEUR-1
              PERFORM READ-FORM
           END-IF.
           MOVE 66 TO SAVE-KEY.
           GO NEXT-JOUR.
       NEXT-JOUR-END.
           IF COMPTEUR = 24
              PERFORM FULL-PROCESS
              INITIALIZE COMPTEUR COMPTEUR-1
              PERFORM READ-FORM
           END-IF.
           IF PROD-HRS(32) > 0
              ADD 1 TO COMPTEUR
              MOVE PROD-HEURES TO JRS-HEURES
              PERFORM FILL-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 32
              COMPUTE LIN-NUM =  9
              COMPUTE COL-NUM = COMPTEUR * 8 - 3
              MOVE 7 TO IDX-1
              MOVE " TOTAL" TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.
           IF COMPTEUR > 0
              PERFORM FULL-PROCESS
           END-IF.

       ADD-HRS.
           IF JRS-HRS(IDX) > 0
              ADD JRS-HRS(IDX) TO PROD-HRS(IDX) 
           END-IF.

       FILL-HRS.
           IF JRS-HRS(IDX) > 0
              COMPUTE LIN-NUM = 11 + IDX
              IF IDX = 32 
                 ADD 1 TO LIN-NUM
              END-IF
              COMPUTE COL-NUM = COMPTEUR * 8 - 2
              MOVE JRS-HRS(IDX) TO VH-00
              MOVE 2 TO DEC-NUM
              MOVE 3 TO CAR-NUM
              PERFORM FILL-FORM
              IF CAL-JOUR(LNK-MOIS, IDX) = 1
              AND JRS-OCCUPATION = 0 
              AND JRS-COMPLEMENT = 0
              AND IDX < 32 
                 COMPUTE COL-NUM = COMPTEUR * 8 - 2
                 MOVE "*" TO ALPHA-TEXTE
                 PERFORM FILL-FORM
              END-IF
           END-IF.


       FILL-CODE-OCC.
           IF JRS-COMPLEMENT = 0
              IF JRS-POSTE > 0
                 MOVE JRS-POSTE TO PC-NUMBER VH-00
                 CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
                 MOVE PC-NOM TO OCC-NOM
              ELSE
                 MOVE JRS-OCCUPATION TO OCC-KEY VH-00
                 CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY
                 MOVE OCC-KEY TO OT-NUMBER
                 CALL "6-OCCTXT" USING LINK-V OT-RECORD FAKE-KEY
                 MOVE OT-NOM TO OCC-NOM
              END-IF
           ELSE
              MOVE JRS-COMPLEMENT TO OCO-NUMBER 
              CALL "6-OCCOM" USING LINK-V OCO-RECORD FAKE-KEY
              MOVE OCO-NOM TO OCC-NOM
              MOVE 0 TO VH-00
           END-IF.
           COMPUTE LIN-NUM = 8.
           COMPUTE COL-NUM = COMPTEUR * 8 - 3.
           COMPUTE CAR-NUM = 7.
           PERFORM FILL-FORM.

           MOVE 1 TO IDX-4.
           INITIALIZE LIMITEUR.
           STRING OCC-NOM DELIMITED BY " " INTO LIMITEUR WITH POINTER
           IDX-4.
           COMPUTE LIN-NUM =  9.
           COMPUTE COL-NUM = COMPTEUR * 8 - 3.
           MOVE LIMITEUR TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 0 TO IDX-4.
           INSPECT OCC-NOM TALLYING IDX-4 FOR CHARACTERS BEFORE " ".
           ADD 2 TO IDX-4.
           IF IDX-4 < 20
              UNSTRING OCC-NOM INTO LIMITEUR WITH POINTER IDX-4
              COMPUTE LIN-NUM =  10
              COMPUTE COL-NUM = COMPTEUR * 8 - 3
              MOVE LIMITEUR TO ALPHA-TEXTE
              PERFORM FILL-FORM.
    

       FULL-PROCESS.
           PERFORM METIER.
           PERFORM FILL-FILES.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
           END-IF.
           PERFORM TRANSMET.
           PERFORM DIS-HE-01.


       FILL-FILES.
           COMPUTE LIN-NUM =  2.
           MOVE 45 TO COL-NUM.
           PERFORM MOIS-NOM.
           PERFORM FILL-FORM.
           MOVE 55 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

      * DATE EDITION
           MOVE 108 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 111 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 114 TO COL-NUM.
           MOVE TODAY-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

      * DONNEES FIRME
           COMPUTE LIN-NUM =  4.
           MOVE  3 TO COL-NUM.
           MOVE  4 TO COL-NUM.
           MOVE FR-KEY TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

      * DONNEES PERSONNE

           COMPUTE LIN-NUM = 4.
           MOVE  6 TO CAR-NUM.
           MOVE 60 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           
           PERFORM FILL-FORM.
           MOVE 67 TO COL-NUM.
           MOVE PR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 5.
           MOVE 67 TO COL-NUM.
           MOVE PR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 71 TO COL-NUM.
           MOVE PR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD  6 TO COL-NUM.
           MOVE PR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  5 TO CAR-NUM.
           ADD 2 TO COL-NUM.
           MOVE PR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           ADD 2 TO COL-NUM.
           MOVE PR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 4.
           MOVE 100 TO COL-NUM.
           MOVE MET-NOM(PR-CODE-SEXE) TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           PERFORM READ-CONTRAT.

           COMPUTE LIN-NUM = 6.
           MOVE 78 TO COL-NUM.
           MOVE CON-DEBUT-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 81 TO COL-NUM.
           MOVE CON-DEBUT-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 84 TO COL-NUM.
           MOVE CON-DEBUT-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 100 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE CON-FIN-J TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 103 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE CON-FIN-M TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 106 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE CON-FIN-A TO  VH-00.
           PERFORM FILL-FORM.


       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.

       CODES-OCCOM.
           INITIALIZE COUNTER-OCO OCO-RECORD.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-OCCOM THRU NEXT-OCCOM-END.

       NEXT-OCCOM.
           CALL "6-OCCOM" USING LINK-V OCO-RECORD EXC-KEY.
           IF OCO-FIRME NOT = FR-KEY
           OR COUNTER-OCO > 13
              GO NEXT-OCCOM-END
           END-IF.
           ADD 1 TO COUNTER-OCO.
           MOVE OCO-NUMBER TO COMPL-IDX(COUNTER-OCO).
           MOVE OCO-NOM TO COMPL-NOM(COUNTER-OCO).
           GO NEXT-OCCOM.
       NEXT-OCCOM-END.
           EXIT.

       XTITRE-OCCOM.
           MOVE 47 TO LIN-NUM.
           PERFORM TXT-OCC VARYING IDX FROM 1 BY 1 UNTIL IDX > 
           COUNTER-OCO.

       TXT-OCC.
           COMPUTE COL-NUM = IDX * 7 + 33.
           MOVE COMPL-NOM(IDX) TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       TOT-OCO.
           COMPUTE LIN-NUM = 48 + COMPTEUR-1.
           PERFORM TST-OCC VARYING IDX FROM 1 BY 1 UNTIL IDX > 
           COUNTER-OCO.

       TST-OCC.
           IF JRS-COMPLEMENT = COMPL-IDX(IDX)
              COMPUTE COL-NUM = IDX * 7 + 33
              MOVE JRS-HRS(32) TO VH-00
              MOVE 2 TO DEC-NUM
              MOVE 3 TO CAR-NUM
              PERFORM FILL-FORM
           END-IF.


