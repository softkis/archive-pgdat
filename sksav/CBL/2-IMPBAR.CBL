      ******************************************************************
      *
      *  AFFICHAGE TEST IMPOTS
      *
      ******************************************************************

       IDENTIFICATION DIVISION.

       PROGRAM-ID.      2-IMPBAR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           SELECT OPTIONAL TF-FILE ASSIGN TO DISK PARMOD-PATH-REAL,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.

       FD  TF-FILE 
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.
       01  TF-RECORD   PIC X(80).

       WORKING-STORAGE SECTION.

       01  ADRES    PIC X(50) VALUE "X:\PATH\IMPOTAAAA".

           COPY "V-VAR.CPY".
           COPY "PARMOD.REC".

       01 DONNEES-BASE.
           02 IMPOSABLE          PIC 9(8)V99 VALUE 0.
           02 JOURS-IMPOSABLE    PIC 999 VALUE 25.
           02 IMPOT              PIC 9(8)V9.
           02 CLASSE-IMPOT.
              03 CLASSE          PIC 9.
              03 GROUPE          PIC X.
              03 ENFANTS         PIC 99 VALUE 0.
           02 REVENU             PIC X VALUE SPACES.


       01 DONNEES-TEST.
           02 SALAIRE            PIC 9(9).
           02 IMPOTS             PIC 9(9)V99 OCCURS 6.

       01 DONNEES-PRT.
          02 IMPOS               PIC Z(9).
          02 S-C1                PIC X VALUE ";".
          02 RESULTS  OCCURS 6.
             03 IMP              PIC Z(7),ZZ.
             03 IMP-A REDEFINES IMP  PIC Z(9).
             03 S-C              PIC X.

       01 NOT-OPEN               PIC 9 VALUE 0.
       01 IMPOSABLE-SAVE         PIC 9(8) COMP-3 VALUE 0.
       01 IMPOSABLE-ARR          PIC 9(8) COMP-3 VALUE 0.
       01 IMPOSABLE-EURO         PIC 9(6)V99 COMP-3 VALUE 0.
       01 TABLEAU-LIMITE         PIC 9(8).
       01 RONN                   PIC 9(8).
       01 RONN-1                 PIC 9(8)V9.
       01 RONN-A REDEFINES       RONN-1.
          02 R-A                 PIC 9(8).
          02 R-B                 PIC 9.

       01 CL-A.
          02 C-1                 PIC X(8) VALUE "       1".
          02 C-1A                PIC X(8) VALUE "      1a".
          02 C-1A1               PIC X(8) VALUE "     1a1".
          02 C-1A2               PIC X(8) VALUE "     1a2".
          02 C-2                 PIC X(8) VALUE "       2".
          02 C-21                PIC X(8) VALUE "      21".
          02 C-22                PIC X(8) VALUE "      22".
          02 C-23                PIC X(8) VALUE "      23".
          02 C-24                PIC X(8) VALUE "      24".
       01 CLASSES-A REDEFINES CL-A  PIC X(72).

       01 CL-B.
          02 IM                  PIC X(11) VALUE "Imposable;".
          02 C-1                 PIC X(11) VALUE "        1;".
          02 C-1A                PIC X(11) VALUE "       1a;".
          02 C-2                 PIC X(11) VALUE "        2;".
          02 C-1P                PIC X(11) VALUE "Pension 1;".
          02 C-1AP               PIC X(11) VALUE "Pension1a;".
          02 C-2P                PIC X(11) VALUE "Pension 2;".
       01 CLASSES-B REDEFINES CL-B  PIC X(77).

       01 MODULE-IMPOT.
           02 FILLER             PIC XX VALUE "0-".
           02 IMPOT-TYPE         PIC X VALUE "I".
           02 ANNEE-MODULE       PIC 9999.

       01  HE-Z8                   PIC Z(8).
       01  HE-Z6Z1                 PIC Z(6),Z BLANK WHEN ZERO.
       01  HE-Z5Z2                 PIC Z(5),ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TF-FILE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-WORK SECTION.

       INIT-PARAMETER.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           PERFORM AFFICHAGE-ECRAN.
           
           MOVE LNK-ANNEE TO ANNEE-MODULE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 
              MOVE 1 TO INDICE-ZONE.
           INITIALIZE EXC-KEY-CTL.

           MOVE 2834323100 TO EXC-KFR(1).
           MOVE 0000000092 TO EXC-KFR(2)
           MOVE 1112131400 TO EXC-KFR(3).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           PERFORM AVANT-1 .

           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.

           PERFORM APRES-1 .

       TRAITEMENT-ECRAN-END.
           GO TRAITEMENT-ECRAN.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-1.     
           MOVE IMPOSABLE TO HE-Z6Z1.
           ACCEPT HE-Z6Z1
             LINE 23 POSITION 1 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z6Z1 REPLACING ALL "." BY ",".
           MOVE HE-Z6Z1 TO IMPOSABLE.

       AVANT-PATH.
           IF PARMOD-PATH-PROTO = SPACES
              MOVE ADRES TO PARMOD-PATH-PROTO
           END-IF.
           MOVE 24350000 TO LNK-POSITION.
           MOVE SPACES TO LNK-LOW.
           CALL "0-GPATH" USING LINK-V PARMOD-RECORD EXC-KEY.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 12
              MOVE ADRES TO PARMOD-PATH-PROTO
              GO AVANT-PATH
           END-IF.
           IF LNK-LOW = "!" 
              PERFORM AFFICHAGE-ECRAN
              GO AVANT-PATH
           END-IF.
           IF LNK-NUM > 0 
              MOVE  0 TO LNK-POSITION
              PERFORM DISPLAY-MESSAGE
              GO AVANT-PATH
           END-IF.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-1.
           MOVE 1 TO LIN-IDX.
           IF EXC-KEY = 1
              IF REVENU = " "
                 MOVE "P" TO REVENU
              ELSE
                 MOVE " " TO REVENU
              END-IF
           END-IF.
           EVALUATE EXC-KEY 
                WHEN  1 IF IMPOT-TYPE = "I"
                        AND LNK-ANNEE < 2008
                           MOVE "P" TO IMPOT-TYPE
                        ELSE
                           MOVE "I" TO IMPOT-TYPE
                        END-IF
                WHEN  2 MOVE    1 TO JOURS-IMPOSABLE
                WHEN  3 MOVE   25 TO JOURS-IMPOSABLE
                WHEN  4 MOVE  300 TO JOURS-IMPOSABLE
                WHEN 10 IF JOURS-IMPOSABLE < 25
                           MOVE 25 TO JOURS-IMPOSABLE
                        END-IF
                        IF JOURS-IMPOSABLE = 25
                           MOVE 6000 TO TABLEAU-LIMITE
                           MOVE 1000 TO IMPOSABLE   
                           MOVE "Mensuel  ;" TO IM
                        ELSE
                           MOVE 60000 TO TABLEAU-LIMITE
                           MOVE 10000 TO IMPOSABLE    
                           MOVE "Annuel   ;" TO IM
                        END-IF
                        PERFORM TABLEAU
                WHEN 11 PERFORM CONTROLE-TEST
                        PERFORM AA
                WHEN 12 PERFORM CONTROLE-TEST
                        PERFORM AA
                WHEN 14 PERFORM CONTROLE-TEST
                        PERFORM AA
           END-EVALUATE.
           EVALUATE JOURS-IMPOSABLE
            WHEN  1 MOVE IMPOSABLE TO RONN-1
                    COMPUTE RONN = R-B / 2
                    COMPUTE R-B = 2 * RONN
                    MOVE RONN-1 TO IMPOSABLE
            WHEN 25 COMPUTE RONN = IMPOSABLE / 5
                    COMPUTE IMPOSABLE = 5 * RONN 
            WHEN 300 COMPUTE RONN = IMPOSABLE / 50
                    COMPUTE IMPOSABLE = 50 * RONN 
           END-EVALUATE.
                    
      *    IF EXC-KEY NOT = 5
           PERFORM DIS-LIGNE VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.
      *    END-IF.

           DISPLAY REVENU LINE 23 POSITION 20.

       DIS-LIGNE.
           ADD  1 TO LIN-IDX.
           MOVE 1 TO COL-IDX.
           EVALUATE JOURS-IMPOSABLE
                WHEN   1 DIVIDE IMPOSABLE BY ,2 GIVING IMPOT 
                                REMAINDER ENFANTS 
                         COMPUTE IMPOSABLE = IMPOT * ,2
                WHEN  25 DIVIDE IMPOSABLE BY 5 GIVING IMPOT
                                REMAINDER ENFANTS
                         COMPUTE IMPOSABLE = IMPOT * 5
                WHEN 300 DIVIDE IMPOSABLE BY 50 GIVING IMPOSABLE-ARR
                                REMAINDER SH-00
                         COMPUTE IMPOSABLE = IMPOSABLE-ARR * 50
           END-EVALUATE.
           MOVE IMPOSABLE TO HE-Z6Z1.
           INSPECT HE-Z6Z1 REPLACING ALL ",0" BY "  ".
           DISPLAY HE-Z6Z1 LINE LIN-IDX POSITION COL-IDX.
           IF LNK-ANNEE < 2008
              PERFORM DIS-COL VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 9
           ELSE
              PERFORM DIS-08 VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 6.
           EVALUATE JOURS-IMPOSABLE
                WHEN   1 ADD   ,2 TO IMPOSABLE
                WHEN  25 ADD    5 TO IMPOSABLE
                WHEN 300 ADD   50 TO IMPOSABLE
           END-EVALUATE.

       DIS-COL.
           ADD 8 TO COL-IDX.
           INITIALIZE CLASSE-IMPOT.
           IF IDX-1 < 5 
               MOVE 1 TO CLASSE
               IF IDX-1 > 1
                   MOVE "A" TO GROUPE
                   COMPUTE ENFANTS = IDX-1 - 2
               END-IF
           ELSE 
               MOVE 2 TO CLASSE
               COMPUTE ENFANTS = IDX-1 - 5
           END-IF.
           CALL MODULE-IMPOT USING DONNEES-BASE.
           MOVE IMPOT TO HE-Z6Z1.
           INSPECT HE-Z6Z1 REPLACING ALL ",0" BY "  ".
           DISPLAY HE-Z6Z1 LINE LIN-IDX POSITION COL-IDX.

       DIS-08.
           ADD 11 TO COL-IDX.
           INITIALIZE CLASSE-IMPOT REVENU.
           IF IDX-1 = 3 OR = 6 
              MOVE 2 TO CLASSE
           ELSE
              MOVE 1 TO CLASSE
           END-IF.
           IF IDX-1 = 2 OR = 5
              MOVE "A" TO GROUPE
           END-IF.
           IF IDX-1 > 3
              MOVE "P" TO REVENU
           END-IF.
           CALL MODULE-IMPOT USING DONNEES-BASE.
           MOVE IMPOT TO HE-Z6Z1.
           INSPECT HE-Z6Z1 REPLACING ALL ",0" BY "  ".
           DISPLAY HE-Z6Z1 LINE LIN-IDX POSITION COL-IDX.


       AFFICHAGE-ECRAN.
           DISPLAY " " LINE 1 POSITION 1 ERASE EOS.
           IF LNK-ANNEE < 2008
              DISPLAY CLASSES-A LINE 1 POSITION 8
           ELSE
              DISPLAY CLASSES-B LINE 1 POSITION 1
              DISPLAY LNK-ANNEE LINE 1 POSITION 10
           END-IF.

       END-PROGRAM.
           CANCEL MODULE-IMPOT.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
QQ
       CONTROLE-TEST.
           PERFORM AFFICHAGE-ECRAN.
           INITIALIZE DONNEES-TEST IDX-4.
           IF EXC-KEY = 14
              MOVE 1 TO JOURS-IMPOSABLE
              PERFORM CONTROLE-M VARYING SH-00 FROM 41,2 BY ,2
              UNTIL SH-00 > 252
           END-IF.                 
           IF JOURS-IMPOSABLE = 25
              IF EXC-KEY = 12
                 IF LNK-ANNEE < 2009
                    PERFORM CONTROLE-M VARYING SH-00 FROM 1030 BY 5
                    UNTIL SH-00 > 6290
                 ELSE
      *             PERFORM CONTROLE-M VARYING SH-00 FROM 1060 BY 5
      *             UNTIL SH-00 > 6915
PENSIO              PERFORM CONTROLE-M VARYING SH-00 FROM 1005 BY 5
                    UNTIL SH-00 > 6865
                 END-IF
              ELSE
                 IF LNK-ANNEE < 2009
                    PERFORM CONTROLE-M VARYING SH-00 FROM  980 BY 5
                    UNTIL SH-00 > 6235
                 ELSE
                    PERFORM CONTROLE-M VARYING SH-00 FROM 1005 BY 5
                    UNTIL SH-00 > 6865
                 END-IF
              END-IF
           ELSE
              IF EXC-KEY = 12
                 PERFORM CONTROLE-M VARYING SH-00 FROM 12400 BY 50
                 UNTIL SH-00 > 77000
              ELSE
                 PERFORM CONTROLE-M VARYING SH-00 FROM 11750 BY 5
                 UNTIL SH-00 > 76350
              END-IF
           END-IF.
           MOVE 4 TO COL-IDX.
           PERFORM CONTROLE-C VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.

       CONTROLE-M.
           ADD SH-00 TO SALAIRE.
           ADD 1 TO IDX-4.
           MOVE SALAIRE TO HE-Z8.
           DISPLAY HE-Z8 LINE 3 POSITION 1.
           MOVE SH-00 TO HE-Z8.
           DISPLAY HE-Z8 LINE 2 POSITION 1.
           DISPLAY IDX-4 LINE 2 POSITION 12.
           PERFORM CONTROLE-B VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.

       CONTROLE-B.
           INITIALIZE CLASSE-IMPOT REVENU.
           MOVE SH-00 TO IMPOSABLE.
           EVALUATE IDX 
            WHEN 1 MOVE 1 TO CLASSE
            WHEN 2 MOVE 1 TO CLASSE
                   MOVE "A" TO GROUPE
            WHEN 3 MOVE 2 TO CLASSE
            WHEN 4 MOVE 1 TO CLASSE
                   MOVE "P" TO REVENU
            WHEN 5 MOVE 1 TO CLASSE
                   MOVE "A" TO GROUPE
                   MOVE "P" TO REVENU
            WHEN 6 MOVE 2 TO CLASSE
                   MOVE "P" TO REVENU
           END-EVALUATE.
           CALL MODULE-IMPOT USING DONNEES-BASE.
           ADD IMPOT TO IMPOTS(IDX).

       CONTROLE-C.
           MOVE IMPOTS(IDX) TO HE-Z6Z1 HE-Z8.
           COMPUTE COL-IDX = (IDX * 11).
           IF JOURS-IMPOSABLE = 300
              DISPLAY HE-Z8 LINE 3 POSITION COL-IDX
           ELSE           
              INSPECT HE-Z6Z1 REPLACING ALL ",0" BY "  "
              DISPLAY HE-Z6Z1 LINE 3 POSITION COL-IDX.

       TABLEAU.
           EVALUATE JOURS-IMPOSABLE
                WHEN  25 ADD    5 TO IMPOSABLE
                WHEN 300 ADD   50 TO IMPOSABLE
           END-EVALUATE.
           MOVE IMPOSABLE TO IMPOS.
           INITIALIZE SH-00.
           PERFORM TAB-08 VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 6.
           IF SH-00 > 0
              PERFORM FILL-FILES.
           IF IMPOSABLE <= TABLEAU-LIMITE
              GO TABLEAU.
           CANCEL MODULE-IMPOT.
           EXIT PROGRAM.

       TAB-08.
           INITIALIZE CLASSE-IMPOT REVENU.
           IF IDX-1 = 3 OR = 6 
              MOVE 2 TO CLASSE
           ELSE
              MOVE 1 TO CLASSE
           END-IF.
           IF IDX-1 = 2 OR = 5
              MOVE "A" TO GROUPE
           END-IF.
           IF IDX-1 > 3
              MOVE "P" TO REVENU
           END-IF.
           CALL MODULE-IMPOT USING DONNEES-BASE.
           ADD  IMPOT TO SH-00.
           IF JOURS-IMPOSABLE = 25
              MOVE IMPOT TO IMP(IDX-1)
           ELSE
              MOVE IMPOT TO IMP-A(IDX-1)
           END-IF.
              
           MOVE ";"   TO S-C(IDX-1).

       FILL-FILES.
           IF NOT-OPEN = 0
              PERFORM AVANT-PATH
              OPEN OUTPUT TF-FILE
              MOVE 1 TO NOT-OPEN
              WRITE TF-RECORD FROM CL-B
              INITIALIZE TF-RECORD
           END-IF.
           WRITE TF-RECORD FROM DONNEES-PRT.

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           