      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-DPI DECLARATION INCAPACITE DE TRAVAIL     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-DPI.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "LIVRE.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "LIVRE.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "JOURS.REC".
           COPY "DPI.LNK".

       01  LIVRE-NAME.
           02 LIVRE-ID           PIC X(8) VALUE "S-LIVRE.".
           02 ANNEE-LIVRE        PIC 999.

           COPY "V-VAR.CPY".
        
       01  LAST-DAY              PIC 99   VALUE 0.
       01  NEW-TYPE              PIC 9    VALUE 0.
       01  LAST-TYPE             PIC 9    VALUE 0.
       01  HELP-DAY              PIC 99   VALUE 0.
       01  NEXT-DAY              PIC 99   VALUE 0.
       01  NEXT-MONTH            PIC 99   VALUE 0.
       01  COUNTER               PIC 999  VALUE 0.
       01  JOUR                  PIC 99  VALUE 0.
       01  JOUR-D                PIC 99  VALUE 0.
       01  JOUR-F                PIC 99  VALUE 0.
       01  JOURS                 PIC 99V99  VALUE 0.
       01  SAVE-FIRME            PIC 9(6).
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".

       01  TOTAL-HEURES.
           02 TOT-H OCCURS 2.
              03 TOT-HRS   PIC 999V99 OCCURS 32.


       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HE-TEMP PIC ZZ.ZZ BLANK WHEN ZERO.
           02 HE-FIN.
              03 HE-X PIC X VALUE ":".
              03 HE-F PIC ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON LIVRE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-DPI.
       

           IF LNK-ANNEE > 2008
              MOVE 96 TO LNK-NUM
              MOVE "SL" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
              ACCEPT ACTION 
              EXIT PROGRAM
           END-IF.

           MOVE FR-KEY TO SAVE-FIRME.
           MOVE 0 TO FR-KEY.
           MOVE LNK-SUFFIX TO ANNEE-LIVRE.
           OPEN INPUT LIVRE.
           COMPUTE NEXT-MONTH = LNK-MOIS + 1.
           COMPUTE NEXT-DAY   = MOIS-JRS(LNK-MOIS) + 1.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0000000000 TO EXC-KFR(1)
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PR.
           INITIALIZE IDX-1 REG-RECORD LINK-RECORD.
           MOVE FR-KEY TO REG-FIRME-A.
           PERFORM READ-PERSON THRU READ-EXIT.

       READ-PERSON.
           PERFORM NEXT-REGIS.
           INITIALIZE TOTAL-HEURES.
           IF REG-PERSON > 0
              PERFORM TOTAL-LIVRE THRU TOTAL-LIVRE-END.
      *    PERFORM DIS-HE-00.
           IF REG-PERSON > 0
              GO READ-PERSON.
       READ-EXIT.


       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD "A" NX-KEY.
           IF REG-SNOCS-YN = "N"
              GO NEXT-REGIS
           END-IF.
           IF REG-PERSON > 0
              CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES
              IF PRES-TOT(LNK-MOIS) = 0
                 GO NEXT-REGIS
              END-IF
           END-IF.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       ENTREE-SORTIE.
           IF PRES-ANNEE = 0
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR.


       TOTAL-LIVRE.
           PERFORM DIS-HE-00.
           INITIALIZE L-RECORD 
           MOVE REG-FIRME  TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON.
           MOVE LNK-MOIS TO L-MOIS.
           MOVE 0 TO L-SUITE.
           START LIVRE KEY >= L-KEY INVALID GO TOTAL-LIVRE-END.
           PERFORM READ-LIVRE THRU READ-LP-END.
       TOTAL-LIVRE-END.
           EXIT.

       READ-LIVRE.
           READ LIVRE NEXT AT END 
                GO READ-LP-END.
           IF FR-KEY     NOT = L-FIRME
           OR REG-PERSON NOT = L-PERSON
           OR L-MOIS   > NEXT-MONTH 
      *    OR L-REGIME = 0
           OR L-REGIME > 99
              GO READ-LP-END.
           IF L-MOIS = LNK-MOIS  
              IF L-SUITE = 0
                 IF L-UNI-MALADIE-P NOT = 0
                    INITIALIZE JRS-RECORD 
                    MOVE 11 TO JRS-OCCUPATION LNK-SUITE
                    CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD 
                    FAKE-KEY
                    IF JRS-HRS(32) > 0
                       PERFORM HEURES
                    END-IF
                 END-IF
                 GO READ-LIVRE
              END-IF
           END-IF.
           IF L-MOIS = NEXT-MONTH  
              IF L-SUITE = 0
                 GO READ-LIVRE
              END-IF
              IF NEXT-DAY NOT = LAST-DAY
                 GO READ-LP-END
              ELSE
                 PERFORM GET-TYPE
                 IF NEW-TYPE = LAST-TYPE
                 AND L-JOUR-DEBUT = 1
      *          AND L-FLAG-LONG-TERME = 0
                    MOVE "00" TO LINK-AU(IDX-1)
                 END-IF
              END-IF
              GO READ-LP-END
           END-IF.
      *    IF L-FLAG-LONG-TERME > 0
      *       GO READ-LIVRE
      *    END-IF.
           IF IDX-1 = 15
              PERFORM TRANSMET
           END-IF.
           PERFORM FILL-FILES.
           IF  L-MOIS     = 12
           AND L-JOUR-FIN = 31
              PERFORM NEXT-YEAR THRU NEXT-YEAR-END
              GO READ-LP-END
           END-IF.
           PERFORM DIS-HE-00.
           GO READ-LIVRE.
       READ-LP-END.
           EXIT.

       FILL-FILES.
           PERFORM GET-TYPE.
           IF  REG-PERSON = LAST-PERSON
           AND NEW-TYPE   = LAST-TYPE
           AND LAST-DAY   = L-JOUR-DEBUT 
              MOVE L-JOUR-FIN   TO HELP-DAY
              MOVE HELP-DAY TO LINK-AU(IDX-1)
           ELSE
              PERFORM FILL-LINE.
           MOVE REG-PERSON TO LAST-PERSON.
           COMPUTE LAST-DAY = L-JOUR-FIN + 1. 
           MOVE NEW-TYPE TO LAST-TYPE.

       GET-TYPE.
           MOVE 1 TO NEW-TYPE.
fam        IF L-FLAG(4) NOT = 0
              MOVE 2 TO NEW-TYPE.
disp       IF L-FLAG(6) NOT = 0
mat        OR L-SUITE = 10
              MOVE 3 TO NEW-TYPE.
acc        IF  L-FLAG(4) NOT = 0
mat        AND L-SUITE = 10
              MOVE 4 TO NEW-TYPE.

       FILL-LINE.
           ADD 1 TO IDX-1.
           MOVE PR-NOM TO LINK-ASS-NOM(IDX-1).
           MOVE PR-PRENOM TO LINK-ASS-PRENOM(IDX-1).
           MOVE PR-MATRICULE TO LINK-ASSURE(IDX-1).
           MOVE L-JOUR-DEBUT TO HELP-DAY.
           MOVE HELP-DAY TO LINK-DU(IDX-1).
           MOVE L-JOUR-FIN   TO HELP-DAY.
           MOVE HELP-DAY TO LINK-AU(IDX-1).
           MOVE NEW-TYPE TO LINK-T(IDX-1).

       TRANSMET.
           MOVE FR-MATRICULE TO LINK-MATRICULE.
           MOVE FR-NOM TO LINK-DENOMINATION.
           MOVE LNK-MOIS TO LINK-REF-MOIS.
           MOVE LNK-ANNEE TO LINK-REF-ANNEE.
           MOVE TODAY-JOUR  TO HE-JJ.
           MOVE TODAY-MOIS  TO HE-MM.
           MOVE TODAY-ANNEE TO HE-AA.
           MOVE HE-DATE TO  LINK-FORM-DATE.

           CALL "DPIT" USING LINK-V LINK-RECORD.
           ADD 1 TO COUNTER.
           INITIALIZE IDX-1 LINK-RECORD.

        NEXT-YEAR.
           INITIALIZE L-RECORD.
           ADD 1 TO LNK-ANNEE.
           MOVE REG-FIRME  TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON.
           MOVE 1 TO L-MOIS.
        NEXT-YEAR-1.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD NX-KEY.
           IF FR-KEY     NOT = L-FIRME
           OR REG-PERSON NOT = L-PERSON
           OR 1          NOT = L-MOIS 
              GO NEXT-YEAR-END.
           IF L-SUITE = 0
              GO NEXT-YEAR-1.
           IF L-JOUR-DEBUT > 1
      *    OR L-FLAG-LONG-TERME > 0
              GO NEXT-YEAR-END.
           PERFORM GET-TYPE.
           IF NEW-TYPE = LAST-TYPE
              MOVE "00" TO LINK-AU(IDX-1)
           END-IF.
       NEXT-YEAR-END.
           SUBTRACT 1 FROM LNK-ANNEE.

       HEURES.
           INITIALIZE JRS-RECORD LNK-SUITE.
           MOVE 13 TO SAVE-KEY.
           PERFORM READ-HEURES THRU READ-HEURES-END.
           PERFORM MALADIE-LIBRE.

       READ-HEURES.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD SAVE-KEY.
           IF SAVE-KEY = 66
              IF FR-KEY     NOT = JRS-FIRME
              OR REG-PERSON NOT = JRS-PERSON
              OR LNK-MOIS   NOT = JRS-MOIS 
              OR JRS-OCCUPATION = 99
                 GO READ-HEURES-END
              END-IF
           ELSE
              MOVE 66 TO SAVE-KEY
              MOVE LNK-MOIS TO JRS-MOIS
           END-IF.
           IF JRS-COMPLEMENT > 0
              GO READ-HEURES.
           PERFORM ADD-HEURES VARYING JOUR FROM 1 BY 1 UNTIL
              JOUR > MOIS-JRS(LNK-MOIS).
           GO READ-HEURES.
       READ-HEURES-END.
           EXIT.

       ADD-HEURES.
           IF JRS-OCCUPATION = 11
              MOVE 1 TO IDX-2
           ELSE
              MOVE 2 TO IDX-2
            END-IF.
           MOVE JRS-HRS(JOUR) TO JOURS.
           ADD JOURS TO TOT-HRS(IDX-2, JOUR) TOT-HRS(IDX-2, 32).


       MALADIE-LIBRE.
           MOVE 0 TO JOUR-D JOUR-F.
           PERFORM DEF-PERIODE VARYING JOUR FROM 1 BY 1 UNTIL JOUR > 
           MOIS-JRS(LNK-MOIS).
           IF JOUR-D > 0
              IF JOUR-F = MOIS-JRS(LNK-MOIS)
               INITIALIZE JRS-RECORD LNK-SUITE
               MOVE FR-KEY     TO JRS-FIRME
               MOVE REG-PERSON TO JRS-PERSON 
               MOVE LNK-MOIS   TO JRS-MOIS
               ADD 1           TO JRS-MOIS
               MOVE 11         TO JRS-OCCUPATION
               CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD FAKE-KEY
               IF JRS-HRS(1) NOT = 0
                  MOVE 0 TO JOUR-F
               END-IF
              END-IF
              PERFORM REG-PERIODE
           END-IF.

       DEF-PERIODE.
           IF JOUR-D = 0
           AND TOT-HRS(1, JOUR) NOT = 0
              MOVE JOUR TO JOUR-D.
           IF TOT-HRS(1, JOUR) NOT = 0
              MOVE JOUR TO JOUR-F
           ELSE
              IF JOUR-F > 0
                 PERFORM REG-PERIODE
              END-IF
           END-IF.
           IF JOUR-F > 0
           AND TOT-HRS(2, JOUR) NOT = 0
              PERFORM REG-PERIODE.

       REG-PERIODE.
           IF IDX-1 = 15
              PERFORM TRANSMET
           END-IF.
           MOVE JOUR-D TO L-JOUR-DEBUT.
           MOVE JOUR-F TO L-JOUR-FIN.
           MOVE 1 TO NEW-TYPE.
           PERFORM FILL-FILES.
           MOVE 0 TO JOUR-D JOUR-F.


       DIS-HE-00.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 16 POSITION 27 SIZE 6
           MOVE 0 TO COL-IDX
           IF  FR-NOM-JF = SPACES
           AND PR-NOM-JF = SPACES
              DISPLAY PR-NOM LINE 16 POSITION 47 SIZE 33
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "   "
           ELSE
              DISPLAY PR-NOM-JF LINE 16 POSITION 47 SIZE 33
           INSPECT PR-NOM-JF TALLYING COL-IDX FOR CHARACTERS BEFORE "  "
           END-IF.
           ADD 48 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 16 POSITION COL-IDX SIZE IDX LOW.
           IF A-N = "N"
              DISPLAY SPACES LINE 16 POSITION 35 SIZE 10.
       DIS-HE-01.
           MOVE FR-KEY    TO HE-Z6.
           DISPLAY HE-Z6  LINE  6 POSITION 17.
           DISPLAY FR-NOM LINE  6 POSITION 25 SIZE 30.
       DIS-HE-END.

       AFFICHAGE-ECRAN.
           MOVE 1552 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF COUNTER > 0 
              MOVE 99 TO LNK-VAL
              CALL "DPIT" USING LINK-V LINK-RECORD
           END-IF.
           MOVE SAVE-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V A-N FAKE-KEY.
           CANCEL "DPIT".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXF".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

       TRAITEMENT.
           PERFORM START-FIRME.
           IF INPUT-ERROR = 0 
              PERFORM READ-FIRME THRU READ-FIRME-END.

       START-FIRME.
           IF FR-KEY > 0 
              SUBTRACT 1 FROM FR-KEY.
           IF A-N = "A"
              MOVE 9999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-FIRME.
           PERFORM NEXT-FIRME.
           IF FR-KEY = 0
           OR FR-KEY > END-NUMBER
           OR FR-MATCHCODE > END-MATCHCODE
              GO READ-FIRME-END
           END-IF.
           IF FR-FIN-A > 0 AND < LNK-ANNEE
              GO READ-FIRME
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM START-PR.
           IF IDX-1 > 0 
              PERFORM TRANSMET.
           GO READ-FIRME.
       READ-FIRME-END.
           EXIT.

       NEXT-FIRME.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.
           