      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-DEMMAT DEMANDE MATRICULE                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-DEMMAT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.

      *    Fichier DEMMAT

           SELECT OPTIONAL DEMMAT ASSIGN TO DISK PARMOD-PATH-REAL
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-HELP.

       DATA DIVISION.


       FILE SECTION.
      *컴컴컴컴컴컴

       FD  DEMMAT
           RECORD VARYING DEPENDING IDX-4
           DATA RECORD IS DEMMAT-REC.
       01  DEMMAT-REC.
           02  DEMMAT-X  PIC X OCCURS 1 TO 500 DEPENDING IDX-4.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 5.
       01  PRECISION             PIC 9 VALUE 1.
       01  ADRES.
           02 A-1 PIC X(55)
           VALUE "C:\CETREL\SOFIE\DATA\123456789\TO_CRYPT\DEMMAT.DTA".

       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  HE-INPUT-PARAMETER.
      *    LINE COL SIZE HE-SCREEN    LLCCSZECRA
           02 IP-101 PIC 9(10)  VALUE 0405250030.
           02 IP-102 PIC 9(10)  VALUE 0605250035.
           02 IP-103 PIC 9(10)  VALUE 0705250036.
           02 IP-104 PIC 9(10)  VALUE 0905250100.
           02 IP-105 PIC 9(10)  VALUE 0450150010.
           02 IP-106 PIC 9(10)  VALUE 0550150013.
           02 IP-DEC PIC 9(10)  VALUE 2305250099.

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
            03 I-PP OCCURS 7.
               04 IP-LINE  PIC 99.
               04 IP-COL   PIC 99.
               04 IP-SIZE  PIC 99.
               04 IP-ECRAN PIC 9999.


      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".
           COPY "FICHE.REC".
           COPY "PAYS.REC".
           COPY "PARMOD.REC".

       01  REC-MATR.
           02  REC-MAT PIC X(28).
           02  REC-MATR-R REDEFINES REC-MAT.
               10  MATR-ID1         PIC 9(13).
               10  MATR-CONVENTION  PIC X(6).
               10  MATR-SECULINE    PIC 9(9).


       01  COMPTEUR              PIC 99 COMP-1.

       01  NOT-OPEN              PIC 9 VALUE 0.
       01  SAVE-FIRME            PIC 9(6).

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

       01  CART-NAME.
           02 CART-TETE        PIC X(6) VALUE "CARTID".
           02 CART-DATE-NAISS  PIC 9(8).
           02 CART-SUITE       PIC 9.
           02 CART-DELIM       PIC X VALUE ".".
           02 CART-MATR        PIC 9(13).
           02 CART-DELIM       PIC X VALUE "_".
           02 CART-CONVENTION  PIC 9(6).
           02 CART-FIN         PIC X(4) VALUE ".JPG".

       01  HEADER.
           02 HD-TYPE        PIC 9 VALUE 0.
           02 HD-DELIM       PIC X VALUE ";".
           02 HD-MATR        PIC 9(13).
           02 HD-DELIM       PIC X VALUE ";".
           02 HD-CONVENTION  PIC 9(6).

       01  DEMMAT-BODY.
           02 DEMMAT-TYPE           PIC 9 VALUE 1.
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-NOM            PIC X(150).
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-NOM-MARI       PIC X(70).
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-PRENOM         PIC X(70).
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-ETAT           PIC 9 VALUE 1.
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-DATE-NAISS     PIC 9(8).
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-SEXE           PIC X.
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-MAISON         PIC X(5).
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-RUE            PIC X(60).
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-LOCALITE       PIC X(50).
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-PAYS           PIC XXX.
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-CODE-POST      PIC 9(5).
           02 DEMMAT-FILLER         PIC X(10) VALUE SPACES.
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-FIRME          PIC 9(6).
           02 DEMMAT-PERSON         PIC 9(8).
           02 DEMMAT-FILLER         PIC X(16) VALUE SPACES.
           02 DEMMAT-DELIM          PIC X VALUE ";".
           02 DEMMAT-CART           PIC X VALUE "N".

           COPY "V-VH00.CPY".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-X8 PIC 9(8).
           02 HE-XX REDEFINES HE-X8.
              03 HE-XA PIC 9.
              03 HE-XB PIC 9.
              03 HE-XC PIC 9.
              03 HE-XD PIC 9.
              03 HE-XE PIC 9.
              03 HE-XF PIC 9.
              03 HE-XG PIC 9.
              03 HE-XH PIC 9.
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.


           02 HE-DEMMAT.
              03 HE-A PIC ZZZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HE-M PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HE-J PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HE-S PIC ZZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
       
           USE AFTER ERROR PROCEDURE ON DEMMAT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-DEMMAT.
       
           INITIALIZE PARMOD-RECORD.


           MOVE "SECULINE" TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-MATR TO REC-MATR.

           INITIALIZE PARMOD-RECORD LIN-NUM FORMULAIRE DEC-NUM.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           IF MENU-PROG-NUMBER = 1 
              MOVE LNK-USER TO PARMOD-USER.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           IF PARMOD-PATH3 = SPACES
              MOVE PARMOD-SETTINGS TO PARMOD-MATR
              MOVE PARMOD-PATH2 TO PARMOD-PATH1
              MOVE SPACES TO PARMOD-PATH2 
           END-IF.
           MOVE LNK-LANGUAGE TO SAVE-LANGUAGE.
           CALL "0-TODAY" USING TODAY.
           MOVE FR-KEY TO SAVE-FIRME.
           PERFORM AFFICHAGE-ECRAN .
           INITIALIZE FR-RECORD.
           PERFORM AFFICHAGE-DETAIL.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-PATH
           WHEN  5 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-PATH
           WHEN  5 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-PATH.
           IF PARMOD-PATH-PROTO = SPACES
              MOVE ADRES TO PARMOD-PATH-PROTO
              INSPECT PARMOD-PATH-PROTO REPLACING ALL "123456789"
              BY MATR-SECULINE
           END-IF.
           MOVE 10057000 TO LNK-POSITION.
           MOVE SPACES TO LNK-LOW.
           CALL "0-GPATH" USING LINK-V PARMOD-RECORD EXC-KEY.
           IF EXC-KEY = 12
              MOVE ADRES TO PARMOD-PATH-PROTO
              INSPECT PARMOD-PATH-PROTO REPLACING ALL "123456789"
              BY MATR-SECULINE
              GO AVANT-PATH
           END-IF.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-PATH.
           IF LNK-LOW = "!" 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF LNK-NUM > 0 
              MOVE  0 TO LNK-POSITION
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE "N" TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       TRAITEMENT.
           IF A-N = "A"
              MOVE 9999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           IF FR-KEY = 0
              MOVE 66 TO EXC-KEY
           ELSE
              MOVE 13 TO EXC-KEY.
           PERFORM READ-FIRME THRU READ-FIRME-END.
           PERFORM END-PROGRAM.

       READ-FIRME.
           PERFORM NEXT-FIRME.
           MOVE 66 TO EXC-KEY.
           IF FR-KEY = 0
           OR FR-KEY > END-NUMBER
           OR FR-MATCHCODE > END-MATCHCODE
              GO READ-FIRME-END
           END-IF.
           IF FR-FIN-A > 0 AND < LNK-ANNEE
              GO READ-FIRME
           END-IF.
           IF FR-SNOCS-YN = "N" 
           OR FR-EXTENS   = 0
              GO READ-FIRME
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM START-PERSON.
           GO READ-FIRME.
       READ-FIRME-END.
           PERFORM END-PROGRAM.

       START-PERSON.
           INITIALIZE REG-RECORD.
           PERFORM READ-PERSON THRU READ-EXIT.

       READ-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY.
           IF REG-PERSON = 0
              GO READ-EXIT.
           IF REG-SNOCS-YN = "N"
              GO READ-PERSON
           END-IF.
           PERFORM DIS-HE-00.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF PRES-ANNEE = 0
              GO READ-PERSON
           END-IF.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           IF PR-NOM = SPACES
           OR PR-SNOCS > 0
              GO READ-PERSON
           END-IF.
           PERFORM FILL-FILES.
           GO READ-PERSON.
       READ-EXIT.
           EXIT.

       NEXT-FIRME.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.

       FILL-FILES.
           IF NOT-OPEN = 0 
              PERFORM OPEN-FILE
           END-IF.
           PERFORM DEMMAT-WRITE.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.


       OPEN-FILE.
           IF IDX = 1
              OPEN EXTEND DEMMAT
           ELSE
              OPEN OUTPUT DEMMAT
              MOVE MATR-ID1 TO HD-MATR 
              MOVE MATR-CONVENTION TO HD-CONVENTION
              MOVE 22 TO IDX-4
              WRITE DEMMAT-REC FROM HEADER
              DISPLAY DEMMAT-REC LINE 24 POSITION 25 SIZE 35
              IF FS-HELP = "30"
                 MOVE 1 TO INPUT-ERROR
              END-IF
           END-IF.
           MOVE 1 TO NOT-OPEN.

       DEMMAT-WRITE.
           INITIALIZE FICHE-RECORD.
           MOVE 478 TO IDX-4.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD NUL-KEY.
           MOVE REG-FIRME  TO DEMMAT-FIRME.
           MOVE REG-PERSON TO DEMMAT-PERSON.
           MOVE PR-NOM TO DEMMAT-NOM.
           MOVE SPACES TO DEMMAT-NOM-MARI.
           IF PR-NOM-JF NOT = SPACES
              MOVE PR-NOM-JF TO DEMMAT-NOM
              MOVE PR-NOM    TO DEMMAT-NOM-MARI.
           IF PR-DOC-ID NOT = SPACES
              MOVE "Y" TO DEMMAT-CART
           ELSE
              MOVE "N" TO DEMMAT-CART
           END-IF.
           MOVE PR-PRENOM       TO DEMMAT-PRENOM.
           MOVE PR-DATE-NAISS   TO DEMMAT-DATE-NAISS CART-DATE-NAISS.
           MOVE PR-SNOCS        TO CART-SUITE.
           MOVE MATR-ID1        TO CART-MATR.
           MOVE MATR-CONVENTION TO CART-CONVENTION.
           IF PR-CODE-SEXE = 1
              MOVE "M" TO DEMMAT-SEXE
           ELSE
              MOVE "F" TO DEMMAT-SEXE.
           EVALUATE FICHE-ETAT-CIVIL 
              WHEN "C" MOVE 1 TO DEMMAT-ETAT
              WHEN "M" MOVE 2 TO DEMMAT-ETAT
              WHEN "S" MOVE 3 TO DEMMAT-ETAT
              WHEN "V" MOVE 4 TO DEMMAT-ETAT
              WHEN "D" MOVE 5 TO DEMMAT-ETAT
              WHEN "R" MOVE 6 TO DEMMAT-ETAT
           END-EVALUATE.
           MOVE PR-MAISON        TO DEMMAT-MAISON.
           MOVE PR-LOCALITE      TO DEMMAT-LOCALITE.
           MOVE PR-DATE-NAISS    TO DEMMAT-DATE-NAISS.
           MOVE PR-PAYS          TO DEMMAT-PAYS.
           MOVE PR-RUE           TO DEMMAT-RUE.
           MOVE PR-CODE-POST     TO DEMMAT-CODE-POST.
           WRITE DEMMAT-REC FROM DEMMAT-BODY.
              
           IF LIN-NUM >= 65
              PERFORM TRANSMET
              INITIALIZE  LIN-NUM FORMULAIRE
           END-IF.
           IF LIN-NUM = 0
              MOVE 3 TO LIN-NUM
              MOVE 50 TO COL-NUM
              MOVE TODAY-JOUR  TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE TODAY-MOIS  TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE TODAY-ANNEE TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 3 TO COL-NUM
              MOVE TODAY-HEURE TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE TODAY-MIN   TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 2 TO LIN-NUM
              MOVE 1000 TO LNK-NUM
              MOVE FR-LANGUE TO LNK-LANGUAGE
              MOVE "SL" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE 15 TO COL-NUM
              MOVE LNK-TEXT TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.
           PERFORM DIS-HE-00.
           ADD 2 TO LIN-NUM.
           MOVE FR-KEY TO VH-00
           MOVE 3 TO COL-NUM
           MOVE 6 TO CAR-NUM
           PERFORM FILL-FORM
           MOVE 10 TO COL-NUM
           MOVE FR-NOM TO ALPHA-TEXTE
           PERFORM FILL-FORM.
      * DONNEES REFERENCE
           MOVE REG-PERSON TO VH-00.
           MOVE 40 TO COL-NUM.
           MOVE 6 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE 48 TO COL-NUM.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           MOVE 10 TO COL-NUM.
           MOVE CART-NAME TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 5 TO COL-NUM.
           MOVE PR-DOC-ID TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       DIS-HE-01.
           MOVE FR-KEY    TO HE-Z6.
           DISPLAY HE-Z6  LINE  6 POSITION 17.
           DISPLAY FR-NOM LINE  6 POSITION 25 SIZE 30.
           MOVE END-NUMBER    TO HE-Z6.
           DISPLAY HE-Z6  LINE  7 POSITION 17.
       DIS-HE-END.
           EXIT.

       DIS-HE-00.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 8 POSITION 17.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 8 POSITION 25 SIZE 50.

       AFFICHAGE-ECRAN.
           MOVE 32 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           MOVE "EE" TO LNK-AREA.
           MOVE 00042500 TO LNK-POSITION.
           PERFORM LIN VARYING IDX FROM 1 BY 1 UNTIL IDX > 7.

       LIN.
           MOVE IDX TO HE-Z2.
           IF IDX < 5
              DISPLAY HE-Z2 LINE IP-LINE(IDX) POSITION 1 LOW.
           MOVE IP-ECRAN(IDX) TO LNK-NUM.
           MOVE IP-LINE(IDX)  TO LNK-LINE.
           MOVE IP-COL(IDX)   TO LNK-COL.
           MOVE IP-SIZE(IDX)  TO LNK-SIZE.
           MOVE "L" TO LNK-LOW.
           CALL "0-DMESS" USING LINK-V.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
           DISPLAY MATR-ID1        LINE 4 POSITION 65.
           DISPLAY MATR-CONVENTION LINE 5 POSITION 65.
           DISPLAY PARMOD-PATH-PROTO LINE 10 POSITION 5.

       END-PROGRAM.
           IF LIN-NUM > 0
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           MOVE SAVE-LANGUAGE TO LNK-LANGUAGE.
           MOVE SAVE-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V A-N FAKE-KEY.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXF".
           IF  INPUT-ERROR = 0
           AND EXC-KEY = 13
               MOVE CHOIX-MAX TO INDICE-ZONE.
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XFILL2.CPY".
