      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-CHMET LISTE PERSONNES CHAMBRE DES METIERS �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CHMET.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".

           SELECT TRANSFERT ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-PICKUP.

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM130.FDE".


       FD  TRANSFERT
           LABEL RECORD STANDARD
           DATA RECORD IS TRANS-RECORD.

       01  TRANS-RECORD.
           02 TRANS-CODE            PIC 9(5).
           02 TRANS-FILLER          PIC X.
           02 TRANS-ANNEE           PIC 9999.
           02 TRANS-FILLER          PIC X.
           02 TRANS-SEXE            PIC 9.
           02 TRANS-FILLER          PIC X.
           02 TRANS-ANCIEN          PIC 99.
           02 TRANS-FILLER          PIC X.
           02 TRANS-QUAL            PIC 9.
           02 TRANS-FILLER          PIC X.
           02 TRANS-SAL-H           PIC 9999.
           02 TRANS-FILLER          PIC X.
           02 TRANS-SAL-M           PIC 9(4)V99.
           02 TRANS-FILLER          PIC X.
           02 TRANS-SAL-T           PIC 9(4)V99.
           02 TRANS-FILLER          PIC X.
           02 TRANS-SAL-P           PIC 9(4)V99.
           02 TRANS-FILLER          PIC X.
           02 TRANS-SAL-N           PIC 9(4)V99.
           02 TRANS-FILLER          PIC X.
           02 TRANS-CLASSE          PIC X(4).
           02 TRANS-FILLER          PIC X.
           02 TRANS-POSITION        PIC X(6).
           02 TRANS-FILLER          PIC X.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  JOB-STANDARD          PIC X(10) VALUE "130       ".
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "PRESENCE.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "METIER.REC".
           COPY "LIVRE.REC".
           COPY "IMPRLOG.REC".
           COPY "PARMOD.REC".
           COPY "V-VAR.CPY".

       01  NOM-TRANSFERT         PIC X(30) VALUE SPACES.
       01  TABLEAU               PIC 9 VALUE 0.

       01  HELP-CUMUL.           
           02 O-E OCCURS 3.
              03 OUV-EMP PIC 9999.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-EXTENSION    PIC X(4) VALUE ".CHM".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CHMET.
       

           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE LNK-MOIS TO SAVE-MOIS.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1   MOVE 0000000025 TO EXC-KFR(1)
                    MOVE 1700000092 TO EXC-KFR(2)
                    MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY > 0 
              IF EXC-KEY-FUN(EXC-KEY) = 0 
                 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\CHMET.TXT" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 18 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           IF EXC-KEY = 10
              MOVE 1 TO TABLEAU
              PERFORM AVANT-PATH
              OPEN OUTPUT TRANSFERT
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-OUV-EMP 
                    PERFORM START-PERSON
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME.
           MOVE 0 TO REG-PERSON.
           PERFORM READ-PERSON THRU READ-PERSON-END.

       READ-PERSON.
           PERFORM NEXT-PERS.
           IF REG-PERSON = 0
              GO READ-PERSON-END
           END-IF.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF CAR-STATUT > 2
           OR CAR-STATEC = 1
           OR CAR-STATEC = 3
           OR CAR-REGIME = 0
           OR CAR-REGIME = 5
           OR CAR-REGIME = 6
           OR CAR-REGIME = 7
           OR CAR-REGIME = 8
           OR CAR-REGIME = 9
           OR CAR-QUALIFICATION-CM <= 0
              GO READ-PERSON
           END-IF.
           
           IF CAR-METIER-CM <= 0
              MOVE CAR-METIER TO MET-CODE
              CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY
              MOVE MET-CM TO CAR-METIER-CM 
           END-IF.
           IF CAR-METIER-CM <= 0
              GO READ-PERSON
           END-IF.
           INITIALIZE L-RECORD.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           PERFORM DIS-HE-01.
           IF TABLEAU = 0
              PERFORM GO-PRINTER
           ELSE
              PERFORM GO-ASCII.
           GO READ-PERSON.
       READ-PERSON-END.
           IF TABLEAU = 0
           IF LIN-NUM > LIN-IDX
              PERFORM PAGE-DATE
              PERFORM TRANSMET.
           PERFORM END-PROGRAM.

       GO-PRINTER.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 1 TO COUNTER
              MOVE 0 TO LIN-NUM
           END-IF.
           COMPUTE IDX = LIN-NUM.
           IF IDX >= IMPL-MAX-LINE
              PERFORM PAGE-DATE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM FILL-FIRME
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           PERFORM FILL-FILES.

       START-OUV-EMP.
           MOVE 6 TO LNK-MOIS.
           MOVE FR-KEY TO REG-FIRME.
           MOVE 0 TO REG-PERSON OUV-EMP(1) OUV-EMP(2).
           PERFORM READ-OUV-EMP THRU READ-OUV-EMP-END.

       READ-OUV-EMP.
           PERFORM NEXT-PERS.
           IF REG-PERSON = 0
              GO READ-OUV-EMP-END
           END-IF.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           ADD 1 TO OUV-EMP(CAR-STATUT).
           GO READ-OUV-EMP.
       READ-OUV-EMP-END.
           EXIT.

       NEXT-PERS.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF REG-PERSON = 0
              CONTINUE
           ELSE
              IF PRES-TOT(LNK-MOIS) NOT = MOIS-JRS(LNK-MOIS)
                 GO NEXT-PERS
              END-IF
           END-IF.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       DIS-HE-01.
           MOVE REG-PERSON    TO HE-Z6.
           DISPLAY HE-Z6     LINE 17 POSITION 35.
           DISPLAY PR-NOM    LINE 17 POSITION 45.
           DISPLAY PR-PRENOM LINE 18 POSITION 45.
       DIS-HE-END.
           EXIT.

       FILL-FIRME.
           MOVE  5 TO LIN-NUM.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           MOVE 10 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD   1 TO LIN-NUM.
           MOVE 10 TO COL-NUM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD   1 TO LIN-NUM.
           MOVE 10 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE FR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 3.
           MOVE 74 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

       FILL-FILES.
           MOVE  0 TO DEC-NUM.
           MOVE  5 TO CAR-NUM.
           MOVE  5 TO COL-NUM.
           MOVE CAR-METIER-CM TO VH-00.
           PERFORM FILL-FORM.
           MOVE PR-NAISS-A TO VH-00.
           MOVE  4 TO CAR-NUM.
           MOVE 11 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 18 TO COL-NUM.
           MOVE PR-CODE-SEXE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = LNK-ANNEE - REG-ANCIEN-A.
           MOVE  2 TO CAR-NUM.
           MOVE 23 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 27 TO COL-NUM.
           MOVE 0 TO IDX-2.
           MOVE CAR-POSITION TO ALPHA-TEXTE.
           INSPECT CAR-POSITION 
           TALLYING IDX-2 FOR CHARACTERS BEFORE "  ".
           IF CAR-CONGE = 8
           AND IDX-2 < 3
              PERFORM FILL-FORM.

           MOVE 30 TO COL-NUM.
           MOVE CAR-QUALIFICATION-CM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
    
           IF L-HOR-MEN = 0
              MOVE 34 TO COL-NUM
              MOVE  3 TO CAR-NUM
              MOVE  4 TO DEC-NUM
              MOVE L-SAL-HOR TO VH-00
           ELSE
              MOVE 43 TO COL-NUM
              MOVE  4 TO CAR-NUM
              MOVE  2 TO DEC-NUM
              MOVE L-SAL-MOIS TO VH-00
           END-IF.
           PERFORM FILL-FORM.

           IF VH-00 = 0
              PERFORM DEBIT VARYING IDX-1 FROM 11 BY 1 UNTIL IDX-1 > 50
              MOVE 100 TO IDX-1
              PERFORM DEBIT 
              MOVE 52 TO COL-NUM
              MOVE  4 TO CAR-NUM
              MOVE  2 TO CAR-NUM
              PERFORM FILL-FORM
           END-IF.


           MOVE 0 TO VH-00.
           PERFORM DEBIT VARYING IDX-1 FROM 61 BY 1 UNTIL IDX-1 > 83.
           PERFORM DEBIT VARYING IDX-1 FROM 91 BY 1 UNTIL IDX-1 > 99.
           PERFORM DEBIT VARYING IDX-1 FROM 88 BY 1 UNTIL IDX-1 > 89.
           MOVE 61 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           PERFORM FILL-FORM.

           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 70 TO COL-NUM.
           MOVE 0 TO VH-00.
           PERFORM DEBIT VARYING IDX-1 FROM 84 BY 1 UNTIL IDX-1 > 85.
           PERFORM FILL-FORM.

           MOVE 81 TO COL-NUM.
           MOVE L-CLASSE-I TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE  6 TO CAR-NUM.
           MOVE 90 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.

           ADD 1 TO LIN-NUM.

       DEBIT.
           ADD L-DEB(IDX-1) TO VH-00.

       PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  3 TO CAR-NUM.
           MOVE  7 TO LIN-NUM.
           MOVE 75 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  6 TO LIN-NUM.
           MOVE 68 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 71 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 74 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE  4 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE 74 TO COL-NUM.
           MOVE OUV-EMP(1) TO VH-00.
           PERFORM FILL-FORM.
           MOVE  5 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE 74 TO COL-NUM.
           MOVE OUV-EMP(2) TO VH-00.
           PERFORM FILL-FORM.


       AFFICHAGE-ECRAN.
           MOVE 1711 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       GO-ASCII.
           INITIALIZE TRANS-RECORD.
           MOVE CAR-METIER-CM TO TRANS-CODE.
           MOVE PR-NAISS-A TO TRANS-ANNEE
           MOVE PR-CODE-SEXE TO TRANS-SEXE.
           COMPUTE VH-00 = LNK-ANNEE - REG-ANCIEN-A.
           MOVE  VH-00 TO TRANS-ANCIEN.
           MOVE CAR-QUALIFICATION-CM TO TRANS-QUAL.
           IF L-HOR-MEN = 0
              MOVE L-SAL-HOR TO TRANS-SAL-H
           ELSE
              MOVE L-SAL-MOIS TO TRANS-SAL-M
           END-IF.
           IF  TRANS-SAL-H = 0
           AND TRANS-SAL-M = 0
              MOVE 0 TO VH-00
              PERFORM DEBIT VARYING IDX-1 FROM 11 BY 1 UNTIL IDX-1 > 50
              MOVE 100 TO IDX-1
              PERFORM DEBIT 
              MOVE VH-00 TO TRANS-SAL-T
           END-IF.


           MOVE 0 TO VH-00.
           PERFORM DEBIT VARYING IDX-1 FROM 61 BY 1 UNTIL IDX-1 > 83.
           PERFORM DEBIT VARYING IDX-1 FROM 91 BY 1 UNTIL IDX-1 > 99.
           PERFORM DEBIT VARYING IDX-1 FROM 88 BY 1 UNTIL IDX-1 > 89.
           MOVE VH-00 TO TRANS-SAL-P.

           MOVE 0 TO VH-00.
           PERFORM DEBIT VARYING IDX-1 FROM 84 BY 1 UNTIL IDX-1 > 85.
           MOVE VH-00 TO TRANS-SAL-N.
           INSPECT TRANS-RECORD REPLACING ALL " " BY ";".
           MOVE L-CLASSE-I TO TRANS-CLASSE.
           INITIALIZE TRANS-POSITION.
           IF CAR-CONGE = 8
              MOVE CAR-POSITION TO TRANS-POSITION.
           WRITE TRANS-RECORD.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01.

       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XACTION.CPY".


