      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-PRNOM STRING NOM PERSONNE                 �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-PRNOM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

       01  POINT-IDX      PIC 99.
       01  TIRET          PIC XXX VALUE " - ".
       01  EPOUSE         PIC X(5) VALUE " 굋. ".

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "PERSON.REC".

       PROCEDURE DIVISION USING LINK-V PR-RECORD.

       START-DISPLAY SECTION.
             
       START-0-PRNOM.
           INITIALIZE LNK-TEXT.
           MOVE 1 TO POINT-IDX.

           IF  PR-NOM-JF NOT = SPACES
           AND FR-NOM-JF NOT = SPACES
             PERFORM NOM-JF
           END-IF.
           
           STRING PR-NOM DELIMITED BY "   "  INTO LNK-TEXT 
           WITH POINTER POINT-IDX.
           IF PR-NOM-JF NOT = SPACES
              STRING TIRET DELIMITED BY SIZE 
              INTO LNK-TEXT WITH POINTER POINT-IDX
              STRING PR-NOM-JF DELIMITED BY "   " 
              INTO LNK-TEXT WITH POINTER POINT-IDX.
           ADD 2 TO POINT-IDX.
           STRING PR-PRENOM DELIMITED BY SIZE INTO LNK-TEXT 
           WITH POINTER POINT-IDX.
           EXIT PROGRAM.

       NOM-JF.
           STRING PR-NOM-JF DELIMITED BY "   "  INTO LNK-TEXT 
           WITH POINTER POINT-IDX.
           IF PR-LANGUAGE = "F"
             PERFORM NOM-JF-F
           END-IF.
           STRING TIRET DELIMITED BY SIZE 
           INTO LNK-TEXT WITH POINTER POINT-IDX.
           STRING PR-NOM DELIMITED BY "   " 
           INTO LNK-TEXT WITH POINTER POINT-IDX.
           ADD 2 TO POINT-IDX.
           STRING PR-PRENOM DELIMITED BY SIZE INTO LNK-TEXT 
           WITH POINTER POINT-IDX.
           EXIT PROGRAM.

       NOM-JF-F.
           ADD 1 TO POINT-IDX.
           STRING PR-PRENOM DELIMITED BY "  " INTO LNK-TEXT 
           WITH POINTER POINT-IDX.
           STRING EPOUSE DELIMITED BY SIZE 
           INTO LNK-TEXT WITH POINTER POINT-IDX.
           STRING PR-NOM DELIMITED BY "  " 
           INTO LNK-TEXT WITH POINTER POINT-IDX.
           EXIT PROGRAM.

       