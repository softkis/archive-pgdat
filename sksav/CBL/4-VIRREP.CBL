      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-VIRREP REPORT PAYEMENTS SAISIE + CESSIONS �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  4-VIRREP.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "VIREMENT.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "VIREMENT.FDE".

       WORKING-STORAGE SECTION.

       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIR          PIC 999.

       01  NOT-OPEN              PIC 9 VALUE 0.
       01  ACTION                PIC X.
       01  WR-KEY                PIC 9(4) COMP-1 VALUE 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "VIREMENT.LNK".
           COPY "REGISTRE.REC".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD REG-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON VIREMENT.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-VIRREP .

           MOVE LNK-SUFFIX TO ANNEE-VIR.
           SUBTRACT 1 FROM ANNEE-VIR.
           OPEN INPUT VIREMENT.
           MOVE LINK-RECORD TO VIR-RECORD.
           START VIREMENT KEY >= VIR-KEY INVALID CONTINUE
                  NOT INVALID
               PERFORM READ-VIREM THRU READ-VIR-END.
           CLOSE VIREMENT.
           ADD 1 TO ANNEE-VIR.
           OPEN I-O VIREMENT.
           MOVE LINK-FIRME  TO LINK-FIRME-B LINK-FIRME-S LINK-FIRME-N.
           MOVE LINK-PERSON TO LINK-PERSON-B LINK-PERSON-S LINK-PERSON-N.
           MOVE LINK-TYPE   TO LINK-TYPE-B LINK-TYPE-S LINK-TYPE-N.
           MOVE LINK-SUITE  TO LINK-SUITE-B LINK-SUITE-S LINK-SUITE-N.
           MOVE LINK-RECORD TO VIR-RECORD.
           IF LNK-SQL = "Y" 
              CALL "9-VIREM" USING LINK-V VIR-RECORD WR-KEY 
           END-IF.
           WRITE VIR-RECORD INVALID CONTINUE.
           CLOSE VIREMENT.
           EXIT PROGRAM.

       READ-VIREM.
           READ VIREMENT NEXT AT END 
                GO READ-VIR-END.
           IF LINK-FIRME  NOT = VIR-FIRME
           OR LINK-PERSON NOT = VIR-PERSON
              GO READ-VIR-END.
           IF LINK-TYPE   NOT = VIR-TYPE 
           OR LINK-SUITE  NOT = VIR-SUITE
              GO READ-VIREM.
           ADD VIR-A-PAYER TO LINK-A-PAYER.
           GO READ-VIREM.
       READ-VIR-END.
           EXIT.





           