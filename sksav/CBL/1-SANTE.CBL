      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-SANTE GESTION DU CODE SALAIRE RV SANTE    �
      *  � DU TRAVAIL                                            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-SANTE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
       01  CHOIX-MAX  PIC 99 VALUE 9. 

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
SU         COPY "CSDEF.REC".
SU    *    COPY "CSDET.REC".
SU         COPY "CODTXT.REC".
           COPY "LIVRE.REC".
           COPY "CODPAR.REC".
           COPY "CODPAIE.REC".
           COPY "CONTRAT.REC".

       01  RECHERCHE  PIC 9. 
       01  SAVE-SUFF  PIC 999 VALUE 0.
       01  PRECISION  PIC 9 VALUE 1.
       01  ANNEE      PIC 9999 VALUE 0.
       01  ANNEE-SUFF REDEFINES ANNEE.
           02 MIL     PIC 9.
           02 SIECLE  PIC 999.
       01  MOIS       PIC 99 VALUE 0.
       01  JOUR       PIC 99 VALUE 0.
       01  HEURE      PIC 99 VALUE 0.
       01  MINUTES    PIC 99 VALUE 0.
       01  MOTIF      PIC X(25).

       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ.
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-SANTE.

           MOVE LNK-SUFFIX TO SAVE-SUFF.
           MOVE LNK-ANNEE TO ANNEE.
           MOVE LNK-MOIS  TO MOIS SAVE-MOIS.

           CALL "6-GCP" USING LINK-V CP-RECORD.

           PERFORM AFFICHAGE-ECRAN .

           MOVE COD-PAR(200, 2) TO CD-NUMBER.
           CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY.
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           IF CTX-NOM = SPACES
              MOVE "SL" TO LNK-AREA
              MOVE  39  TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              ACCEPT ACTION 
              EXIT PROGRAM
           END-IF.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 9  MOVE 0000000005 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7 
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT REG-PERSON 
             LINE  3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
            MOVE 0 TO RECHERCHE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           ACCEPT ANNEE
             LINE  6 POSITION 30 SIZE  4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4.
           ACCEPT MOIS
             LINE  7 POSITION 32 SIZE  2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-5.
           ACCEPT JOUR
             LINE  8 POSITION 32 SIZE  2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
       AVANT-6.
           IF JOUR > 0
           ACCEPT HEURE
             LINE  9 POSITION 32 SIZE  2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE 
             MOVE 0 TO HEURE.    
       AVANT-7.
           IF HEURE > 0
           ACCEPT MINUTES
             LINE 10 POSITION 32 SIZE  2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE 
             MOVE 0 TO MINUTES.    

       AVANT-8.
           ACCEPT MOTIF
             LINE 11 POSITION 30 SIZE  25
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 0 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              IF PRES-ANNEE = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           IF LNK-COMPETENCE < REG-COMPETENCE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR = 0
              PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF REG-PERSON = 0 
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-3.
           COMPUTE IDX-1 = LNK-ANNEE + 10.
           IF ANNEE < LNK-ANNEE
           OR ANNEE > IDX-1
              MOVE LNK-ANNEE TO ANNEE 
              MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0
              COMPUTE LNK-SUFFIX = SIECLE
              PERFORM PRESENCE
              IF PRES-ANNEE = 0
                 MOVE 1 TO INPUT-ERROR
                 PERFORM CONTRAT
                 MOVE SAVE-SUFF TO LNK-SUFFIX
                 MOVE LNK-ANNEE TO ANNEE 
              END-IF
           END-IF.

       APRES-4.
           IF MOIS = 0
           OR MOIS > 12
            MOVE LNK-MOIS TO MOIS
            MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0
              IF PRES-TOT(MOIS) = 0
                 MOVE 1 TO INPUT-ERROR
                 PERFORM CONTRAT
              END-IF
           END-IF.
           IF INPUT-ERROR = 0
              PERFORM LECTURE
              PERFORM AFFICHAGE-DETAIL.

       APRES-5.
           IF JOUR > 0
              IF JOUR > MOIS-JRS(MOIS)
                 MOVE MOIS-JRS(MOIS) TO JOUR
                 MOVE 1 TO INPUT-ERROR
              END-IF
              IF PRES-JOUR(MOIS, JOUR) = 0
                 MOVE 1 TO INPUT-ERROR
                 PERFORM CONTRAT
              END-IF
           END-IF.

       APRES-6.
           IF HEURE > 23
            MOVE 23 TO HEURE
            MOVE 1 TO INPUT-ERROR.

       APRES-7.
           IF MINUTES > 59
            MOVE 59 TO MINUTES
            MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM CREATION
                    MOVE 1 TO INDICE-ZONE
            WHEN  8 PERFORM EFFACEMENT
                    MOVE 1 TO INDICE-ZONE
           END-EVALUATE.
           IF DECISION NOT = 0
             COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
               MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.

       CREATION.
           PERFORM MAKE-KEY.
           COMPUTE LNK-SUFFIX = SIECLE.
           MOVE MOIS TO LNK-MOIS.
           MOVE JOUR      TO CSP-DONNEE-1.
           MOVE MOTIF     TO CSP-TEXTE.
           MOVE HEURE     TO CSP-UNITE.
           MOVE MINUTES   TO CSP-UNITAIRE.
           MOVE 1         TO CSP-TOTAL.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.

       EFFACEMENT.
           PERFORM MAKE-KEY.
           MOVE 98 TO SAVE-KEY.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD SAVE-KEY.

       MAKE-KEY.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY     TO CSP-FIRME.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE MOIS       TO CSP-MOIS.
           MOVE CD-NUMBER  TO CSP-CODE.

       LECTURE.
           PERFORM MAKE-KEY.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           MOVE CSP-DONNEE-1 TO JOUR.
           MOVE CSP-TEXTE    TO MOTIF.
           MOVE CSP-UNITE    TO HEURE.
           MOVE CSP-UNITAIRE TO MINUTES.

       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-03.
           MOVE ANNEE  TO HE-Z4.
           DISPLAY HE-Z4  LINE  6 POSITION 30.
       DIS-HE-04.
           MOVE MOIS TO HE-Z2.
           DISPLAY HE-Z2  LINE  7 POSITION 32.
       DIS-HE-05.
           MOVE JOUR TO HE-Z2.
           DISPLAY HE-Z2  LINE  8 POSITION 32.
       DIS-HE-05.
           MOVE HEURE TO HE-Z2.
           DISPLAY HE-Z2  LINE  9 POSITION 32.
       DIS-HE-06.
           MOVE MINUTES TO HE-Z2.
           DISPLAY HE-Z2  LINE 10 POSITION 32.
       DIS-HE-08.
           DISPLAY MOTIF LINE 11 POSITION 30.

       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 583 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CANCEL "6-CODPAI".
           MOVE SAVE-SUFF TO LNK-SUFFIX.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

       CONTRAT.
           CALL "2-CONTR" USING LINK-V CON-RECORD PR-RECORD REG-RECORD.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.
