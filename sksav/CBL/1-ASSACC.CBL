      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-ASSACC GESTION DES TAUX ASSURANCE ACCIDENT�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-ASSACC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "TXACCID.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "TXACCID.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 999 VALUE 100.
       01  HELP-VAL              PIC ZZ,ZZ BLANK WHEN ZERO.
       01  HELP-1                PIC 9999.
       01  HELP-2                PIC 9.
       
       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TXACC.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-1-ASSACC.

           MOVE LNK-ANNEE TO TXA-ANNEE.

           OPEN I-O TXACC.

           PERFORM AFFICHAGE-ECRAN .
           READ TXACC INVALID 
                INITIALIZE TXA-REC-DET 
                END-READ.

           PERFORM AFFICHAGE-VALEURS.
           PERFORM TRAITEMENT-ECRAN THRU TRAIT-ECR-END.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0100000005 TO EXC-KFR(1).
           MOVE 0022130000 TO EXC-KFR(3).
           MOVE 0027000000 TO EXC-KFR(6).
           MOVE 0052530000 TO EXC-KFR(11).
           MOVE 0000000065 TO EXC-KFR(13).
           MOVE 6600000000 TO EXC-KFR(14).

           PERFORM DISPLAY-F-KEYS.

       TRAIT-ECR-01.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           MOVE 1 TO INDICE-ZONE.
           PERFORM DISPLAY-F-KEYS.
           PERFORM ACCEPT-VALUES.
       TRAIT-ECR-END.
      *=================================================================
       ACCEPT-VALUES.
           DIVIDE INDICE-ZONE BY 10 GIVING COL-IDX REMAINDER LIN-IDX.
           IF LIN-IDX = 0 
              MOVE 10 TO LIN-IDX
              SUBTRACT 1 FROM COL-IDX.
           ADD 6 TO LIN-IDX.
           COMPUTE COL-IDX = COL-IDX * 7 + 6.
           MOVE TXA-VALUE(INDICE-ZONE) TO HELP-VAL.
           ACCEPT HELP-VAL LINE LIN-IDX POSITION COL-IDX SIZE 5
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO ACCEPT-VALUES.
           INSPECT HELP-VAL REPLACING ALL "." BY ",".
           MOVE HELP-VAL TO TXA-VALUE(INDICE-ZONE) .
           MOVE TXA-VALUE(INDICE-ZONE) TO HELP-VAL.
           DISPLAY HELP-VAL LINE LIN-IDX POSITION COL-IDX.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.
           IF EXC-KEY = 27 MOVE 52 TO EXC-KEY.
           EVALUATE EXC-KEY
                WHEN  1 MOVE INDICE-ZONE TO IDX-4
                        MOVE 00504001 TO LNK-VAL
                        PERFORM HELP-SCREEN
                        PERFORM DISPLAY-F-KEYS
                        MOVE IDX-4 TO INDICE-ZONE 
                WHEN 52 SUBTRACT 1 FROM INDICE-ZONE
                WHEN  5 WRITE TXA-RECORD INVALID 
                        REWRITE TXA-RECORD END-WRITE    
                        PERFORM END-PROGRAM
                WHEN 12 IF TXA-VALUE(1) = 0
                           SUBTRACT 1 FROM TXA-ANNEE
                           READ TXACC INVALID CONTINUE END-READ
                           ADD 1 TO TXA-ANNEE
                           PERFORM AFFICHAGE-VALEURS 
                           MOVE 1 TO INDICE-ZONE
                        END-IF
                WHEN 13 ADD 1 TO INDICE-ZONE
                WHEN 65 IF INDICE-ZONE > 10
                           SUBTRACT 10 FROM INDICE-ZONE
                        END-IF
                WHEN 66 IF INDICE-ZONE < 91
                           ADD 10 TO INDICE-ZONE
                        END-IF
           END-EVALUATE.
           IF INDICE-ZONE > 100
              MOVE 100 TO INDICE-ZONE.
           IF INDICE-ZONE < 1
              MOVE 1 TO INDICE-ZONE.
           GO ACCEPT-VALUES.


       AFFICHAGE-VALEURS.
           PERFORM DISPLAY-VAL VARYING IDX FROM 0 BY 1 UNTIL IDX > 9.
       DISPLAY-VAL.
           COMPUTE COL-IDX = 6 + IDX * 7.
           PERFORM DISPLAY-DET-VAL VARYING IDX-1 FROM 1 BY 1 UNTIL 
                    IDX-1 > 10.
       DISPLAY-DET-VAL.
           COMPUTE LIN-IDX = 6 + IDX-1.
           COMPUTE INDICE-ZONE = IDX * 10 + IDX-1.
           MOVE    TXA-VALUE(INDICE-ZONE)  TO HELP-VAL.
           DISPLAY HELP-VAL LINE LIN-IDX POSITION COL-IDX HIGH.

       AFFICHAGE-ECRAN.
           MOVE 21 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM AFFICHAGE-VALEURS.

       END-PROGRAM.
           CLOSE TXACC.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
      