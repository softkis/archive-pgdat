      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-RECAL RECALCUL DES SALAIRES               �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-RECAL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 10.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "TXACCID.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "FICHE.REC".
           COPY "LIVRE.REC".
           COPY "LIVRE.CUM".
           COPY "CONTRAT.REC".
           COPY "CODPAIE.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
           COPY "V-BASES.REC".
           COPY "CCOL.REC".
           COPY "HORSEM.REC".
           COPY "POSE.REC".

       01  MOIS-DEBUT      PIC 99 VALUE 1.
       01  MOIS-FIN        PIC 99 VALUE 12.
       01  MOIS-LIMITE     PIC 99.
       01  AJUSTE-YN       PIC X VALUE "N".
       01  LAST-FIRME      PIC 9(6) VALUE 0.
       01  LAST-MOIS       PIC 9(2) VALUE 0.
       01  LAST-ANNEE      PIC 9(4) VALUE 0.
       01  ARCHIVER        PIC 9 VALUE 0.
       01  CORRIGER        PIC 9 VALUE 0.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  HELP-1                PIC 99999V99.
       01  HELP-2                PIC 9.
       01  COMPTEUR              PIC 9(3) VALUE 0.
       01  COMPTEUR-2            PIC 9(3) VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-RECAL.

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-MOIS TO SAVE-MOIS MOIS-DEBUT MOIS-FIN.
           PERFORM AFFICHAGE-ECRAN .
           INITIALIZE TXA-RECORD CS-RECORD.
           CALL "6-ASSACC" USING LINK-V TXA-RECORD.
           IF TXA-VALUE(1) = 0
              MOVE "SL" TO LNK-AREA
              MOVE 0 TO LNK-POSITION
              MOVE 61 TO LNK-NUM
              CALL "0-DMESS" USING LINK-V
              ACCEPT ACTION
           END-IF.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5 
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 10    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0000000025 TO EXC-KFR(3)
                      IF MENU-BATCH = 1
                         MOVE 0000000037 TO EXC-KFR(2)
                      END-IF
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-YN
           WHEN  7 PERFORM APRES-7 
           WHEN  8 PERFORM APRES-8 
           WHEN  9 PERFORM APRES-9 
           WHEN 10 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT AJUSTE-YN 
             LINE 13 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           ACCEPT MOIS-DEBUT
             LINE 15 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
                     PERFORM APRES-7
             WHEN 66 ADD      1 TO   MOIS-DEBUT
                     PERFORM APRES-7
             END-EVALUATE.

       AVANT-8.
           ACCEPT MOIS-FIN
             LINE 17 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
                     PERFORM APRES-8
             WHEN 66 ADD      1 TO   MOIS-FIN
                     PERFORM APRES-8
             END-EVALUATE.

       AVANT-9.
           ACCEPT MOIS-LIMITE
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-LIMITE
                     PERFORM APRES-9
             WHEN 66 ADD      1 TO   MOIS-LIMITE
                     PERFORM APRES-9
             END-EVALUATE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-YN.
           IF AJUSTE-YN = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.
       
       APRES-7.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-07.

       APRES-8.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.

       APRES-9.
           IF MOIS-LIMITE > 12
              MOVE 12 TO MOIS-LIMITE
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-09.

       APRES-DEC.
           MOVE 0 TO ARCHIVER.
           IF EXC-KEY = 10
              MOVE 1 TO ARCHIVER
              MOVE 5 TO EXC-KEY
           END-IF.
           IF EXC-KEY = 15
              MOVE 1 TO ARCHIVER
              MOVE 1 TO CORRIGER
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           MOVE SAVE-MOIS TO LNK-MOIS.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF STATUT > 0 AND NOT = CAR-STATUT  
              GO READ-PERSON
           END-IF.
           IF COUT > 0 AND NOT = CAR-COUT
              GO READ-PERSON
           END-IF.
           IF MOIS-LIMITE > 0
              PERFORM READ-CONTRAT
              IF  CON-FIN-A = LNK-ANNEE
              AND CON-FIN-M < MOIS-LIMITE
                 GO READ-PERSON
              END-IF
           END-IF.
           PERFORM DIS-HE-01.
           MOVE 0 TO COMPTEUR.
           IF ARCHIVER = 1
              PERFORM ARCHIVES VARYING LNK-MOIS FROM MOIS-DEBUT BY 1 
              UNTIL LNK-MOIS > MOIS-FIN.
           PERFORM TEST-MOIS VARYING LNK-MOIS FROM MOIS-DEBUT BY 1 
           UNTIL LNK-MOIS > MOIS-FIN.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE SAVE-MOIS TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD PV-KEY.

       ARCHIVES.
           CALL "4-ARCHIV" USING LINK-V REG-RECORD.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       TEST-MOIS.
           DISPLAY LNK-MOIS LINE 15 POSITION 31.
           MOVE LNK-MOIS TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 15351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
           INITIALIZE L-RECORD.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-PERSON NOT = 0
              PERFORM CALC-MOIS.

       CALC-MOIS.
           IF AJUSTE-YN NOT = "N"
              MOVE 0 TO COMPTEUR-2
              INITIALIZE CSP-RECORD
              PERFORM READ-CODPAI THRU READ-CSP-END.
           CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES.
           IF LNK-YN = "E"
              INITIALIZE LNK-YN
              EXIT PROGRAM.

       READ-CODPAI.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD NX-KEY.
           IF FR-KEY     NOT = CSP-FIRME
           OR REG-PERSON NOT = CSP-PERSON
           OR CSP-MOIS   NOT = LNK-MOIS 
           OR CSP-SUITE  NOT = 0 
              GO READ-CSP-END.
           IF  CSP-PROTECT = "X"
           AND CORRIGER = 0
              GO READ-CODPAI.
           MOVE CSP-CODE TO CD-NUMBER.
           CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY.
SU         CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
           IF LAST-FIRME  NOT = REG-FIRME
           OR LAST-PERSON NOT = REG-PERSON
           OR LAST-MOIS   NOT = LNK-MOIS
              PERFORM HISTORIQUE
              MOVE REG-FIRME  TO LAST-FIRME
              MOVE REG-PERSON TO LAST-PERSON 
              MOVE LNK-MOIS   TO LAST-MOIS
           END-IF.
           MOVE 0 TO COMPTEUR.
           PERFORM MINI-MAX VARYING IDX-3 FROM 1 BY 1 UNTIL IDX-3 > 8.
           IF COMPTEUR > 0
              CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY
              ADD 1 TO COMPTEUR-2.
           GO READ-CODPAI.
       READ-CSP-END.
           EXIT.
           
       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

           COPY "XDIS.CPY".
       DIS-HE-06.
           DISPLAY AJUSTE-YN LINE 13 POSITION 32.
       DIS-HE-07.
           DISPLAY MOIS-DEBUT LINE 15 POSITION 31.
           MOVE MOIS-DEBUT TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 15351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-08.
           DISPLAY MOIS-FIN LINE 17 POSITION 31.
           MOVE MOIS-FIN TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 17351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-09.
           DISPLAY MOIS-LIMITE LINE 19 POSITION 31.
           MOVE MOIS-LIMITE TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 504 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           CANCEL "4-RECAL".
           CANCEL "4-ARCHIV".
           CANCEL "4-CALCUL".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       HISTORIQUE.
           INITIALIZE CAR-RECORD POSE-RECORD HJS-RECORD FICHE-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD NUL-KEY.
           CALL "6-GHJS" USING LINK-V HJS-RECORD  CAR-RECORD PRESENCES.
           MOVE CAR-EQUIPE TO POSE-CODE.
           CALL "6-POSE" USING LINK-V POSE-RECORD FAKE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           CALL "4-SALBAS" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 POSE-RECORD 
                                 HJS-RECORD 
                                 BASES-REMUNERATION.
           CALL "4-LPCUM" USING LINK-V 
                                REG-RECORD
                                LCUM-RECORD.
           CALL "4-SALMOY" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 HJS-RECORD 
                                 BASES-REMUNERATION
                                 LCUM-RECORD.

       MINI-MAX.
           IF CS-PROCEDURE(IDX-3) NOT = 0
              IF CS-PROCEDURE(IDX-3) < 1000
              OR CS-PROCEDURE(IDX-3) > 1200
                 MOVE CS-PROCEDURE(IDX-3) TO LNK-NUM
                 MOVE IDX-3 TO LNK-INDEX
                 CALL "4-PROC" USING LINK-V 
                                     CCOL-RECORD
                                     REG-RECORD
                                     CAR-RECORD
                                     FICHE-RECORD 
                                     PRESENCES 
                                     POSE-RECORD 
                                     HJS-RECORD 
                                     BASES-REMUNERATION
                                     CSP-RECORD
                                     LCUM-RECORD
                 ADD 1 TO COMPTEUR
              END-IF
           END-IF.
           IF CS-NOTPOL(IDX-3) > SPACES
              MOVE IDX-3 TO LNK-NUM
              CALL "4-NOTPOL" USING LINK-V CS-RECORD CSP-RECORD.
           IF CS-FORMULE(IDX-3) NOT = 0
              MOVE IDX-3 TO LNK-NUM
              CALL "4-FORMUL" USING LINK-V 
                                  CS-RECORD
                                  CSP-RECORD.
           IF CS-VAL-MAX(IDX-3) NOT = 0
              IF CSP-DONNEE(IDX-3) > CS-VAL-MAX(IDX-3) 
                 MOVE CS-VAL-MAX(IDX-3) TO CSP-DONNEE(IDX-3)
              END-IF
           END-IF.
           IF CSP-DONNEE(IDX-3) < CS-VAL-MIN(IDX-3) 
              MOVE CS-VAL-MIN(IDX-3) TO CSP-DONNEE(IDX-3)
           END-IF.
           IF CS-VALEUR(CAR-STATUT) = 0
              MOVE 0 TO CSP-UNITAIRE CSP-TOTAL
           END-IF.
           IF CS-VALEUR(10) = 1 AND CAR-HOR-MEN = 1
              MOVE 0 TO CSP-UNITAIRE CSP-TOTAL
           END-IF.
           IF CS-VALEUR(10) = 2 AND CAR-HOR-MEN = 0
              MOVE 0 TO CSP-UNITAIRE CSP-TOTAL
           END-IF.
