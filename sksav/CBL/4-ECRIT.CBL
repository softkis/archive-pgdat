      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-ECRIT.CBL TRANSFORMATION CHIFFRES LETTRES �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.             4-ECRIT.

       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  IDX-1            PIC 9  COMP-4.
       01  IDX-2            PIC 99 COMP-4.
       01  IDX-3            PIC 99 COMP-4.
       01  IDX-4            PIC 99 COMP-4.
       01  ESC-KEY          PIC 9(4) COMP-1.
       01  action           pic x.

       01  ZERONULL         PIC X(5)  VALUE "ZERO ".
       01  UN               PIC X(3)  VALUE "UN ".
       01  DEUX             PIC X(5)  VALUE "DEUX ".
       01  TROIS            PIC X(6)  VALUE "TROIS ".
       01  QUATRE           PIC X(7)  VALUE "QUATRE ".
       01  CINQ             PIC X(5)  VALUE "CINQ ".
       01  SIX              PIC X(4)  VALUE "SIX ".
       01  SEPT             PIC X(5)  VALUE "SEPT ".
       01  HUIT             PIC X(5)  VALUE "HUIT ".
       01  NEUF             PIC X(5)  VALUE "NEUF ".
       01  DIX              PIC X(4)  VALUE "DIX ".
       01  ONZE             PIC X(5)  VALUE "ONZE ".
       01  DOUZE            PIC X(6)  VALUE "DOUZE ".
       01  TREIZE           PIC X(7)  VALUE "TREIZE ".
       01  QUATORZE         PIC X(9)  VALUE "QUATORZE ".
       01  QUINZE           PIC X(7)  VALUE "QUINZE ".
       01  SEIZE            PIC X(6)  VALUE "SEIZE ".
       01  VINGT            PIC X(6)  VALUE "VINGT ".
       01  TRENTE           PIC X(7)  VALUE "TRENTE ".
       01  QUARANTE         PIC X(9)  VALUE "QUARANTE ".
       01  CINQUANTE        PIC X(10) VALUE "CINQUANTE ".
       01  SOIXANTE         PIC X(9)  VALUE "SOIXANTE ".
       01  CENT             PIC X(5)  VALUE "CENT ".
       01  MIL              PIC X(4)  VALUE "MIL ".
       01  MILLE            PIC X(6)  VALUE "MILLE ".
       01  MILLION          PIC X(8)  VALUE "MILLION ".
       01  ET               PIC X(3)  VALUE "ET ".
       01  TRAIT            PIC X     VALUE "-".
       01  ESPACE           PIC X     VALUE " ".
       01  VIRGULE          PIC X(2)  VALUE ", ".
                       
       LINKAGE SECTION. 

       01 LINK-ECRIT.
           02 ALPHA-TEXTE    PIC X(100).
           02 NUMERIC-0      PIC 9(9).
           02 NUM-B REDEFINES NUMERIC-0.
              03 NUM-IDX2    PIC 999 OCCURS 3.
           02 NUM-C REDEFINES NUMERIC-0.
              03 N-C OCCURS 3.
                 04 NUM-IDX3 PIC 9 OCCURS 3.
           02 DECIMALES      PIC 9999.
           02 DEC-R REDEFINES DECIMALES.
              03 DEC-1      PIC 9.
              03 DEC-2      PIC 9.
              03 DEC-3      PIC 9.
              03 DEC-4      PIC 9.
           02 DEC-R1 REDEFINES DECIMALES.
              03 DEC-5      PIC 99.
              03 DEC-6      PIC 99.

           02 POINT-IDX     PIC 999 COMP-4.

       PROCEDURE DIVISION USING LINK-ECRIT.
      
       PASSWORD-CONVERT SECTION.
             
       CONVERSION-CHIFFRES-LETTRES.

           MOVE 1 TO POINT-IDX IDX-1.
           INITIALIZE ALPHA-TEXTE.
           IF NUM-IDX2(1) = 0 ADD 1 TO IDX-1.
           IF NUM-IDX2(2) = 0 ADD 1 TO IDX-1.

           PERFORM UNITES VARYING IDX-2 FROM IDX-1 BY 1 UNTIL
           IDX-2 > 3.
           IF DECIMALES > 0
              PERFORM DECIMALES.

           EXIT PROGRAM.
           
       UNITES.
           EVALUATE NUM-IDX3 (IDX-2, 1)
                WHEN 2 PERFORM STR-2
                WHEN 3 PERFORM STR-3
                WHEN 4 PERFORM STR-4
                WHEN 5 PERFORM STR-5
                WHEN 6 PERFORM STR-6
                WHEN 7 PERFORM STR-7
                WHEN 8 PERFORM STR-8
                WHEN 9 PERFORM STR-9
           END-EVALUATE.
           IF NUM-IDX3 (IDX-2, 1) > 0
              PERFORM STR-100
           END-IF.
           EVALUATE NUM-IDX3 (IDX-2, 2)
                WHEN 0 PERFORM 1-A-9
                WHEN 1 PERFORM 10-A-19
                WHEN 2 PERFORM STR-20
                       PERFORM 1-A-9
                WHEN 3 PERFORM STR-30
                       PERFORM 1-A-9
                WHEN 4 PERFORM STR-40
                       PERFORM 1-A-9
                WHEN 5 PERFORM STR-50
                       PERFORM 1-A-9
                WHEN 6 PERFORM STR-60
                       PERFORM 1-A-9
                WHEN 7 PERFORM STR-60
                       PERFORM 10-A-19
                WHEN 8 PERFORM STR-80
                       PERFORM 1-A-9
                WHEN 9 PERFORM STR-80
                       PERFORM 10-A-19
           END-EVALUATE.
           IF IDX-2 = 1 
              IF NUM-IDX2(1) > 0 
                 PERFORM STR-MIO
              END-IF 
            END-IF .
           IF IDX-2 = 2 
              IF NUM-IDX2(2) = 1 
                 SUBTRACT 3 FROM POINT-IDX
                 PERFORM STR-M
              END-IF 
              IF NUM-IDX2(2) > 1 
                 PERFORM STR-ML
              END-IF 
            END-IF.

       1-A-9.
           EVALUATE NUM-IDX3 (IDX-2, 3)
                WHEN 1 IF NUM-IDX2(IDX-2) > 20 
                          PERFORM STR-ET
                       END-IF
                       PERFORM STR-1
                WHEN 2 PERFORM STR-2
                WHEN 3 PERFORM STR-3
                WHEN 4 PERFORM STR-4
                WHEN 5 PERFORM STR-5
                WHEN 6 PERFORM STR-6
                WHEN 7 PERFORM STR-7
                WHEN 8 PERFORM STR-8
                WHEN 9 PERFORM STR-9
           END-EVALUATE.

       10-A-19.
           EVALUATE NUM-IDX3 (IDX-2, 3)
               WHEN 0 PERFORM STR-10
               WHEN 1 PERFORM STR-11
               WHEN 2 PERFORM STR-12
               WHEN 3 PERFORM STR-13
               WHEN 4 PERFORM STR-14
               WHEN 5 PERFORM STR-15
               WHEN 6 PERFORM STR-16
               WHEN 7 PERFORM STR-10
                      PERFORM STR-TRAIT
                      PERFORM STR-7
               WHEN 8 PERFORM STR-10
                      PERFORM STR-TRAIT
                      PERFORM STR-8
               WHEN 9 PERFORM STR-10
                      PERFORM STR-TRAIT
                      PERFORM STR-9
           END-EVALUATE.

       DECIMALES.
           SUBTRACT 1 FROM POINT-IDX.
           STRING VIRGULE  DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
           PERFORM DEC.
           IF DEC-6 > 0
              STRING ET DELIMITED BY SIZE INTO ALPHA-TEXTE
              WITH POINTER POINT-IDX END-STRING
              MOVE DEC-6 TO DEC-5
              PERFORM DEC
           END-IF.


       DEC.
           EVALUATE DEC-1
                WHEN 0 PERFORM STR-0
                       PERFORM DEC-1-9
                WHEN 1 PERFORM DEC-10-19
                WHEN 2 PERFORM STR-20
                       PERFORM DEC-1-9
                WHEN 3 PERFORM STR-30
                       PERFORM DEC-1-9
                WHEN 4 PERFORM STR-40
                       PERFORM DEC-1-9
                WHEN 5 PERFORM STR-50
                       PERFORM DEC-1-9
                WHEN 6 PERFORM STR-60
                       PERFORM DEC-1-9
                WHEN 7 PERFORM STR-60
                       PERFORM DEC-10-19
                WHEN 8 PERFORM STR-80
                       PERFORM DEC-1-9
                WHEN 9 PERFORM STR-80
                       PERFORM DEC-10-19
           END-EVALUATE.

       DEC-1-9.
           EVALUATE DEC-2
               WHEN 1 IF DEC-1 > 1 
                         PERFORM STR-ET
                      END-IF
                      PERFORM STR-1
               WHEN 2 PERFORM STR-2
               WHEN 3 PERFORM STR-3
               WHEN 4 PERFORM STR-4
               WHEN 5 PERFORM STR-5
               WHEN 6 PERFORM STR-6
               WHEN 7 PERFORM STR-7
               WHEN 8 PERFORM STR-8
               WHEN 9 PERFORM STR-9
           END-EVALUATE.

       DEC-10-19.
           EVALUATE DEC-2
               WHEN 0 PERFORM STR-10
               WHEN 1 PERFORM STR-11
               WHEN 2 PERFORM STR-12
               WHEN 3 PERFORM STR-13
               WHEN 4 PERFORM STR-14
               WHEN 5 PERFORM STR-15
               WHEN 6 PERFORM STR-16
               WHEN 7 PERFORM STR-10
                       PERFORM STR-TRAIT
                       PERFORM STR-7
               WHEN 8 PERFORM STR-10
                       PERFORM STR-TRAIT
                       PERFORM STR-8
               WHEN 9 PERFORM STR-10
                       PERFORM STR-TRAIT
                       PERFORM STR-9
           END-EVALUATE.

       STR-0.
           STRING ZERONULL DELIMITED BY SIZE INTO ALPHA-TEXTE 
           WITH POINTER POINT-IDX.
       STR-1.
           STRING UN DELIMITED BY SIZE INTO ALPHA-TEXTE 
           WITH POINTER POINT-IDX.
       STR-2.
           STRING DEUX    DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-3.
           STRING TROIS   DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-4.
           STRING QUATRE  DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-5.
           STRING CINQ    DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-6.
           STRING SIX     DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-7.
           STRING SEPT    DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-8.
           STRING HUIT    DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-9.
           STRING NEUF    DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-10.
           STRING DIX     DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-11.
           STRING ONZE    DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-12.
           STRING DOUZE   DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-13.
           STRING TREIZE  DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-14.
           STRING QUATORZE DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-15.
           STRING QUINZE  DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-16.
           STRING SEIZE   DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-20.
           STRING VINGT   DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-30.
           STRING TRENTE  DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-40.
           STRING QUARANTE DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-50.
           STRING CINQUANTE  DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-60.
           STRING SOIXANTE DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-80.
           PERFORM STR-4.
           PERFORM STR-TRAIT.
           PERFORM STR-20.
       STR-100.
           STRING CENT    DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-M.
           STRING MIL     DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-ML.
           STRING MILLE   DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-MIO.
           STRING MILLION DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-ET.
           STRING ET      DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-TRAIT.
           SUBTRACT 1 FROM POINT-IDX.
           STRING TRAIT   DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.
       STR-ESPACE.
           STRING ESPACE  DELIMITED BY SIZE INTO ALPHA-TEXTE
           WITH POINTER POINT-IDX.


           COPY "XACTION.CPY".
           