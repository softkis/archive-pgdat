      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-CHOMS2 IMPRESSION RELEVE ANNEXE CHOMAGE   �
      *  � STRUCTUREL                                            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CHOMS2.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK PARMOD-PATH-REAL,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM130.FDE".
       FD  TF-TRANS
           RECORD VARYING DEPENDING ON IDX-4
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 130 DEPENDING IDX-4.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 6.
       01  PRECISION             PIC 9 VALUE 0.
       01  JOB-STANDARD          PIC X(10) VALUE "130       ".
       01  ADRES    PIC X(50) VALUE "X:\PATH\CHOMANNEXE".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "STATUT.REC".
           COPY "LIVRE.REC".
           COPY "COUT.REC".
           COPY "CODPAR.REC".
           COPY "CODPAIE.REC".
           COPY "IMPRLOG.REC".
           COPY "PARMOD.REC".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  NOT-OPEN              PIC 9 VALUE 0. 
       01  ASCII                 PIC 9 VALUE 0. 


       01  LIN-COUNTER           PIC 99.
       01  TOTAUX.
           02  TOT-HRS-CHOM         PIC 99999V99.
           02  PERS-COUNTER         PIC 999.
           02  TOT-HRS-ETAT         PIC 99999V99.
           02  TOT-MNT-ETAT         PIC 9(6)V99.

       01  TXT-RECORD.
           02 TXT-NUMERO            PIC ZZZZZ.
           02 FILLER                PIC X VALUE ";".
           02 TXT-NOM               PIC X(50).
           02 FILLER                PIC X VALUE ";".
           02 TXT-MATR              PIC Z(11).
           02 FILLER                PIC X VALUE ";".
           02 TXT-SEXE              PIC X(4).
           02 FILLER                PIC X VALUE ";".
           02 TXT-PAYS              PIC X(4).
           02 FILLER                PIC X VALUE ";".
           02 TXT-NATION            PIC X(6).
           02 FILLER                PIC X VALUE ";".
           02 TXT-HEURES            PIC ZZZZZ,ZZ.
           02 FILLER                PIC X VALUE ";".
           02 TXT-ETAT              PIC ZZZZZ,ZZ.
           02 FILLER                PIC X VALUE ";".
           02 TXT-SALAIRE           PIC ZZZ,ZZZZ.
           02 FILLER                PIC X VALUE ";".
           02 TXT-MONTANT           PIC ZZZZZ,ZZ.

       01  HEAD-RECORD.
           02 HEAD-NUMERO           PIC X(6) VALUE "No   ;".
           02 HEAD-NOM              PIC X(43).
           02 HEAD-MOIS             PIC 99.
           02 FILLER                PIC X VALUE " ".
           02 HEAD-ANNEE            PIC 9(4).
           02 FILLER                PIC X VALUE ";".
           02 HEAD-MATR             PIC 9(11).
           02 FILLER                PIC X VALUE ";".
           02 HEAD-SEXE             PIC X(4) VALUE "SEXE".
           02 FILLER                PIC X VALUE ";".
           02 HEAD-PAYS             PIC X(4) VALUE "PAYS".
           02 FILLER                PIC X VALUE ";".
           02 HEAD-NATION           PIC X(6) VALUE "NATION".
           02 FILLER                PIC X VALUE ";".
           02 HEAD-CHOMAGE          PIC X(8) VALUE "CHOMAGE".
           02 FILLER                PIC X VALUE ";".
           02 HEAD-ETAT             PIC X(8) VALUE "ETAT   ".
           02 FILLER                PIC X VALUE ";".
           02 HEAD-SALAIRE          PIC X(8) VALUE "SALAIRE".
           02 FILLER                PIC X VALUE ";".
           02 HEAD-MONTANT          PIC X(7) VALUE "MONTANT".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME            PIC X(8) VALUE "FORM.CC2".


       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z2Z2 PIC ZZ,ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TF-TRANS FORM. 

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CHOMS2.
       
           INITIALIZE PARMOD-RECORD TOTAUX.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           PERFORM AFFICHAGE-ECRAN .
           CALL "6-GCP" USING LINK-V CP-RECORD.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000078 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.

           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-PATH.
           MOVE "AA" TO LNK-AREA.
           MOVE 32 TO LNK-NUM.
           MOVE "L" TO LNK-LOW.
           MOVE 20022000 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
           IF PARMOD-PATH-PROTO = SPACES
              MOVE ADRES TO PARMOD-PATH-PROTO
           END-IF.
           MOVE 20350000 TO LNK-POSITION.
           MOVE SPACES TO LNK-LOW.
           CALL "0-GPATH" USING LINK-V PARMOD-RECORD EXC-KEY.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 12
              MOVE ADRES TO PARMOD-PATH-PROTO
              GO AVANT-PATH
           END-IF.
           IF LNK-LOW = "!" 
              PERFORM AFFICHAGE-ECRAN
              GO AVANT-PATH
           END-IF.
           IF LNK-NUM > 0 
              MOVE  0 TO LNK-POSITION
              PERFORM DISPLAY-MESSAGE
              GO AVANT-PATH
           END-IF.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           WHEN 10 PERFORM AVANT-PATH
                   MOVE 1 TO ASCII
                   PERFORM TRAITEMENT
                   PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           INITIALIZE L-RECORD LNK-SUITE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-UNI-CHOM-C-E = 0
           OR L-UNI-CHOM-C-E = 0
              GO READ-PERSON-1
           END-IF.
           INITIALIZE CSP-RECORD.
           MOVE COD-PAR(152, 1) TO CSP-CODE.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           IF ASCII = 0
              PERFORM FULL-PROCESS
           ELSE
              PERFORM WRITE-TEXT.

       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.
           IF TOT-MNT-ETAT > 0
           AND ASCII = 0
              PERFORM TEST-LINE
              PERFORM FILL-TOTAL
              PERFORM TRANSMET.
           PERFORM END-PROGRAM.
                
       FULL-PROCESS.
           PERFORM TEST-LINE.
           PERFORM FILL-FILES.
           PERFORM DIS-HE-01.

       TEST-LINE.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 1 TO COUNTER
              MOVE 0 TO LIN-NUM LIN-COUNTER
           END-IF.
           IF LIN-NUM >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM LIN-COUNTER
           END-IF.
           IF LIN-COUNTER = 0
              PERFORM FILL-ENTETE
              COMPUTE LIN-NUM = LIN-IDX
           END-IF.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       FILL-ENTETE.
           PERFORM READ-FORM.
           COMPUTE LIN-NUM = 3.
           MOVE 37 TO COL-NUM.
           MOVE FR-ETAB-A TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 42 TO COL-NUM.
           MOVE FR-ETAB-N TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 48 TO COL-NUM.
           MOVE FR-SNOCS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 52 TO COL-NUM.
           MOVE FR-EXTENS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 4.
           MOVE 37 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 6.
           MOVE 37 TO COL-NUM.
           MOVE FR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 2 TO COL-NUM.
           MOVE FR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 8.
           MOVE 37 TO COL-NUM.
           MOVE FR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  6 TO CAR-NUM.
           ADD 1 TO COL-NUM.
           MOVE FR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 10.
           MOVE 37 TO COL-NUM.
           PERFORM MOIS-NOM.
           PERFORM FILL-FORM.
           MOVE 47 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

       FILL-FILES.
           ADD 1 TO PERS-COUNTER LIN-COUNTER LIN-NUM.
      *    COMPUTE LIN-NUM =  LIN-COUNTER.
           MOVE  3 TO CAR-NUM.
           MOVE 10 TO COL-NUM.
           MOVE PERS-COUNTER TO  VH-00.
           PERFORM FILL-FORM.

           MOVE 15 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 56 TO COL-NUM.
           MOVE PR-NAISS-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-NAISS-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-NAISS-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-SNOCS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 72 TO COL-NUM.
           MOVE "H" TO  ALPHA-TEXTE.
           IF PR-CODE-SEXE = 2
              MOVE "F" TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 74 TO COL-NUM.
           MOVE "RE" TO  ALPHA-TEXTE.
           IF PR-PAYS NOT = "L"
              MOVE "FR" TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 77 TO COL-NUM.
           MOVE PR-NATIONALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = 
           L-UNI-CHOM-C-S + L-UNI-CHOM-C-P + L-UNI-CHOM-C-E.
           ADD VH-00  TO TOT-HRS-CHOM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 80 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE CSP-UNITE TO VH-00.
           ADD VH-00  TO TOT-HRS-ETAT.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 89 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 96 TO COL-NUM.
           MOVE "h x 80 % x" TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE CSP-UNITAIRE TO VH-00.
           MOVE  3 TO CAR-NUM.
           MOVE  4 TO DEC-NUM.
           MOVE 110 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 120 TO COL-NUM.
           MOVE "=" TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
 
           MOVE CSP-TOTAL TO VH-00.
           ADD VH-00  TO TOT-MNT-ETAT.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE 121 TO COL-NUM.
           PERFORM FILL-FORM.

       FILL-TOTAL.
           ADD 1 TO LIN-NUM.
           MOVE TOT-HRS-CHOM TO VH-00.
           MOVE  5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 78 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE TOT-HRS-ETAT TO VH-00.
           MOVE  5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 87 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE TOT-MNT-ETAT TO VH-00.
           MOVE  5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 121 TO COL-NUM.
           PERFORM FILL-FORM.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 1925 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".

       WRITE-TEXT.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-TRANS
              MOVE FR-NOM    TO HEAD-NOM
              MOVE FR-PERS   TO HEAD-MATR
              MOVE LNK-MOIS  TO HEAD-MOIS
              MOVE LNK-ANNEE TO HEAD-ANNEE
              MOVE 121 TO IDX-4
              WRITE TF-RECORD FROM HEAD-RECORD
              MOVE 1 TO NOT-OPEN 
           END-IF.
           ADD 1 TO PERS-COUNTER.
           MOVE PERS-COUNTER TO TXT-NUMERO.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO TXT-NOM
           MOVE PR-MATRICULE TO TXT-MATR
           IF PR-CODE-SEXE = 1
              MOVE "H" TO  TXT-SEXE
           ELSE
              MOVE "F" TO  TXT-SEXE.
           MOVE "RE" TO TXT-PAYS.
           IF PR-PAYS NOT = "L"
              MOVE "FR" TO TXT-PAYS.
           MOVE PR-NATIONALITE TO TXT-NATION.
           COMPUTE VH-00 = 
           L-UNI-CHOM-C-S + L-UNI-CHOM-C-P + L-UNI-CHOM-C-E.
           ADD VH-00  TO TOT-HRS-CHOM.
           MOVE VH-00 TO TXT-HEURES.
           MOVE CSP-UNITE TO TXT-ETAT.
           MOVE CSP-UNITAIRE TO TXT-SALAIRE.
           MOVE CSP-TOTAL TO TXT-MONTANT
           WRITE TF-RECORD FROM TXT-RECORD.

