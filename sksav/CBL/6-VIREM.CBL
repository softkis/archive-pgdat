      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-VIREM RECHERCHE VIREMENT DE PAIE          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-VIREM.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "VIREMENT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "VIREMENT.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN     PIC 9 VALUE 0.
       01  NOT-FOUND    PIC 9.
       01  ACTION       PIC X.

       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIREMENT        PIC 999.

       01  TODAY.
           02 TODAY-TIME.
              03 TODAY-DATE-A    PIC 9(8).
              03 TODAY-TEMPS-A   PIC 9(4).
           02 TODAY-FILLER.
              03 TODAY-FILL   PIC 9999.
      

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "VIREMENT.LNK".

       01  EXC-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING 
                          LINK-V 
                          REG-RECORD 
                          LINK-RECORD
                          EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON VIREMENT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-VIREM.

           IF NOT-OPEN = 1 
              IF  LNK-SUFFIX-D NOT = 0
              AND LNK-SUFFIX-D NOT = ANNEE-VIREMENT
                 CLOSE VIREMENT
                 MOVE 0 TO NOT-OPEN
              ELSE
                 IF LNK-SUFFIX NOT = ANNEE-VIREMENT
                    CLOSE VIREMENT
                    MOVE 0 TO NOT-OPEN
                  END-IF
              END-IF
           END-IF.

           IF NOT-OPEN = 0
              IF LNK-SUFFIX-D NOT = 0
                 MOVE LNK-SUFFIX-D TO ANNEE-VIREMENT
              ELSE
                 MOVE LNK-SUFFIX TO ANNEE-VIREMENT
              END-IF
              OPEN I-O VIREMENT
              MOVE 1 TO NOT-OPEN
           END-IF.

           MOVE LINK-RECORD TO VIR-RECORD.
           IF FR-KEY NOT =   VIR-FIRME
              MOVE FR-KEY TO VIR-FIRME   VIR-FIRME-B 
                             VIR-FIRME-N VIR-FIRME-S
           END-IF.
           IF REG-PERSON NOT =   VIR-PERSON
              MOVE REG-PERSON TO VIR-PERSON   VIR-PERSON-B 
                                 VIR-PERSON-N VIR-PERSON-S
           END-IF.
           IF VIR-MOIS = 0
           AND EXC-KEY NOT = 66
           AND EXC-KEY NOT =  1
              MOVE LNK-MOIS TO VIR-MOIS VIR-MOIS-B 
                               VIR-MOIS-N VIR-MOIS-S
           END-IF.
           IF  VIR-SUITE = 0
           AND LNK-SUITE > 0
           AND EXC-KEY NOT = 66
               MOVE LNK-SUITE TO VIR-SUITE VIR-SUITE-B 
                                VIR-SUITE-N VIR-SUITE-S
           END-IF.
           IF VIR-TYPE = 0
           AND LNK-NUM > 0
           AND EXC-KEY NOT = 66
              MOVE LNK-NUM TO VIR-TYPE VIR-TYPE-B 
                              VIR-TYPE-N VIR-TYPE-S
           END-IF.
           IF EXC-KEY = 99
              CALL "0-TODAY" USING TODAY 
              MOVE TODAY-TIME TO VIR-TIME
              MOVE LNK-USER TO VIR-USER
              WRITE VIR-RECORD INVALID REWRITE VIR-RECORD
              EXIT PROGRAM
           END-IF.
           IF EXC-KEY = 98
              DELETE VIREMENT INVALID CONTINUE EXIT PROGRAM
           END-IF.
           MOVE 0 TO NOT-FOUND.
           EVALUATE EXC-KEY 
               WHEN 65 PERFORM PREV-VIREMENT
               WHEN 66 PERFORM NEXT-VIREMENT
               WHEN 13 PERFORM READ-VIREMENT
               WHEN 1  PERFORM READ-VIREMENT
               WHEN  0 PERFORM START-VIREMENT
                       MOVE 66 TO EXC-KEY
           END-EVALUATE.
           MOVE VIR-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-VIREMENT.
           START VIREMENT KEY >= VIR-KEY-PERSON INVALID 
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ VIREMENT NEXT NO LOCK AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           PERFORM TEST-VIREMENT.

       NEXT-VIREMENT.
           START VIREMENT KEY > VIR-KEY-PERSON INVALID 
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ VIREMENT NEXT NO LOCK AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           PERFORM TEST-VIREMENT.

       PREV-VIREMENT.
           START VIREMENT KEY < VIR-KEY-PERSON INVALID 
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ VIREMENT PREVIOUS NO LOCK AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           PERFORM TEST-VIREMENT.

       READ-VIREMENT.
           READ VIREMENT NO LOCK INVALID INITIALIZE VIR-RECORD.

       TEST-VIREMENT.
           IF FR-KEY NOT = VIR-FIRME
           OR REG-PERSON NOT = VIR-PERSON
              MOVE 1 TO NOT-FOUND.
           IF NOT-FOUND = 1 
              INITIALIZE VIR-RECORD.

