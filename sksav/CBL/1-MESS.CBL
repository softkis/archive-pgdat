      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-MESS  GESTION DES MESSAGES                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-MESS.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "MESSAGE.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.

           COPY "MESSAGE.FDE".
       FD  TF-TRANS
           RECORD CONTAINS 83 CHARACTERS
           LABEL RECORD STANDARD
           DATA RECORD IS TF-RECORD.
       01  TF-RECORD.
           03  TF-AREA                 PIC XXXX.
           03  TF-NUMBER               PIC 9999.
           03  TF-DESCRIPTION          PIC X(75).

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PARMOD.REC".

       01  CHOIX-MAX             PIC 99 VALUE 5.  

       01   ECR-DISPLAY.
            02 HE-03 PIC ZZZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MESSAGES TF-TRANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-MESS.

           INITIALIZE MS-RECORD. 
           MOVE LNK-LANGUAGE TO SAVE-LANGUAGE MS-LANGUAGE.
           OPEN I-O MESSAGES.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           PERFORM AFFICHAGE-ECRAN .
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0000000016 TO EXC-KFR(2)
           WHEN 2     MOVE 0100000000 TO EXC-KFR(1)
           WHEN 3     MOVE 0129002200 TO EXC-KFR(1)
                      MOVE 2100200000 TO EXC-KFR(2)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0100000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 23 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  5 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT MS-LANGUAGE
             LINE  4 POSITION 30 SIZE 1 
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF EXC-KEY = 10
              PERFORM AVANT-PATH
              PERFORM READ-TEXT.

       AVANT-2.
           ACCEPT MS-AREA 
             LINE  5 POSITION 30 SIZE 4 
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           ACCEPT MS-NUMBER  
             LINE  6 POSITION 25 SIZE  4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4.
           ACCEPT MS-DESCRIPTION
             LINE  8 POSITION 5 SIZE 75
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PATH.
           MOVE "AA" TO LNK-AREA.
           MOVE 32 TO LNK-NUM.
           MOVE "L" TO LNK-LOW.
           PERFORM DISPLAY-MESSAGE.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 24 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
                    display ms-language line 1 position 15.
           EVALUATE MS-LANGUAGE 
                WHEN "F"   MOVE 0 TO INPUT-ERROR 
                WHEN "D"   MOVE 0 TO INPUT-ERROR 
                WHEN "E"   MOVE 0 TO INPUT-ERROR 
                WHEN OTHER MOVE 1 TO INPUT-ERROR 
                           MOVE LNK-LANGUAGE TO MS-LANGUAGE.
       
       APRES-2.
           IF EXC-KEY = 1
              MOVE 1003001 TO LNK-VAL
              PERFORM HELP-SCREEN
              MOVE 1 TO INPUT-ERROR
           END-IF.
           
       APRES-3.
           MOVE MS-AREA TO LNK-AREA.
           MOVE MS-LANGUAGE TO LNK-LANGUAGE.
           EVALUATE EXC-KEY
           WHEN   4 PERFORM COPY-MESSAGES
           WHEN  65 THRU 66 PERFORM NEXT-MESSAGES
           WHEN   2 MOVE MS-AREA TO LNK-AREA
                    MOVE MS-LANGUAGE TO LNK-LANGUAGE
                    CALL "2-MESS" USING LINK-V MS-RECORD
                    MOVE SAVE-LANGUAGE TO LNK-LANGUAGE
                    PERFORM AFFICHAGE-ECRAN 
            WHEN  8 DELETE MESSAGES  INVALID CONTINUE
                    END-DELETE.
           IF EXC-KEY NOT = 4
              PERFORM READ-MESSAGES.
           PERFORM DIS-HE-01 THRU DIS-HE-04.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 WRITE MS-RECORD INVALID 
                        REWRITE MS-RECORD
                    END-WRITE   
                    MOVE 4 TO DECISION
                    PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN  8 DELETE MESSAGES  INVALID CONTINUE
                    END-DELETE
                    INITIALIZE MS-NUMBER MS-DESCRIPTION
                    MOVE 4 TO DECISION
                   PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

           
       COPY-MESSAGES.   
           MOVE MS-NUMBER TO IDX-2.
           ACCEPT MS-NUMBER
             LINE  6 POSITION 60 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           READ MESSAGES INVALID CONTINUE
                NOT INVALID
                MOVE IDX-2 TO MS-NUMBER
                WRITE MS-RECORD INVALID CONTINUE END-WRITE
                END-READ.


       READ-MESSAGES.
           MOVE EXC-KEY TO SAVE-KEY.
           MOVE 13 TO EXC-KEY.
           PERFORM NEXT-MESSAGES.
           MOVE SAVE-KEY TO EXC-KEY.

       NEXT-MESSAGES.
           MOVE MS-LANGUAGE TO LNK-LANGUAGE.
           MOVE MS-AREA TO LNK-AREA.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.
           MOVE SAVE-LANGUAGE TO LNK-LANGUAGE.

       NEXT-NUMBER .
           MOVE 0 TO NOT-FOUND.
           START MESSAGES KEY > MS-KEY INVALID KEY
                INITIALIZE MS-NUMBER MS-DESCRIPTION NOT INVALID 
                PERFORM NEXT-FNUMBER-1 UNTIL NOT-FOUND = 1.
           INITIALIZE MS-DESCRIPTION.
           ADD 1 TO MS-NUMBER.
           MOVE 3 TO INDICE-ZONE.
                
       NEXT-FNUMBER-1.
           READ MESSAGES NEXT AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
                
       DIS-HE-01.
           DISPLAY MS-LANGUAGE LINE 4 POSITION 30.
       DIS-HE-02.
           DISPLAY MS-AREA LINE 5 POSITION 30.
       DIS-HE-03.
           MOVE MS-NUMBER TO HE-03.
           DISPLAY HE-03  LINE  6 POSITION 25.
       DIS-HE-04.
           DISPLAY MS-DESCRIPTION LINE  8 POSITION 5.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 4 TO LNK-VAL.
           MOVE "   " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-04.

       END-PROGRAM.
           CLOSE MESSAGES.
           INITIALIZE MS-RECORD. 
           CALL "0-TODAY" USING TODAY
           MOVE TODAY-DATE TO MS-DATE.
           MOVE "F"   TO MS-LANGUAGE.
           MOVE "SM3" TO MS-AREA.
           CALL "6-MESS" USING LINK-V MS-RECORD WR-KEY.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".
           
       READ-TEXT.
           OPEN INPUT TF-TRANS
           PERFORM LECT-TRANS THRU END-TRANS.
           CLOSE TF-TRANS.

       LECT-TRANS.
           INITIALIZE TF-RECORD.
           READ TF-TRANS AT END GO END-TRANS.
           MOVE "F" TO MS-LANGUAGE.
           MOVE TF-AREA        TO MS-AREA.
           MOVE TF-NUMBER      TO MS-NUMBER.
           MOVE TF-DESCRIPTION TO MS-DESCRIPTION.
           WRITE MS-RECORD INVALID REWRITE MS-RECORD END-WRITE   
           GO LECT-TRANS.
       END-TRANS.
           EXIT.

