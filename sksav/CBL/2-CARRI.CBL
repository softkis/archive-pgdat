      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-CARRI RECHERCHE CARRIERE                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-CARRI.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

       01 DES-HOR-MENS.
           02 H-F PIC X VALUE "H".
           02 M-F PIC X VALUE "M".
           02 H-D PIC X VALUE "S".
           02 M-D PIC X VALUE "M".
           02 H-E PIC X VALUE "H".
           02 M-E PIC X VALUE "M".
       01 DES-HOR-MENS-R REDEFINES DES-HOR-MENS.
           02 D-HM OCCURS 3.
              03 HOR-MENS PIC X OCCURS 2.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CARRIERE.REC".
           COPY "STATUT.REC".
           COPY "COUT.REC".
           COPY "METIER.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.

       01  CHOIX                 PIC 9(6).
       01  DATUM                 PIC 9(8).
       01  COMPTEUR              PIC 99.

       01 HE-CARRI.
          02 H-C OCCURS 20.
             03 H-A PIC 9(4).
             03 H-M PIC 99.


       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ.
           02 HE-Z3 PIC ZZZ.
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z4Z2 PIC Z(4),ZZ BLANK WHEN ZERO.
           02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8).
           02 HE-ACCEPT.
              03 HE-A     PIC ZZZZ.
              03 HE-M     PIC ZZ.
           02 HE-TEMP.
              03 HE-JJ    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-MM    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-AA    PIC ZZZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "CARRIERE.LNK".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD PR-RECORD REG-RECORD.

       START-DISPLAY SECTION.
             
       START-2-CARRI.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE CAR-RECORD IDX-1 EXC-KEY COMPTEUR CHOIX HE-CARRI.
           MOVE 4 TO LIN-IDX.
           MOVE 9999 TO CAR-ANNEE.
           PERFORM READ-CAR THRU READ-CAR-END.
           IF EXC-KEY = 66 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-CAR.
           MOVE 65 TO EXC-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD EXC-KEY.
           IF CAR-FIRME = 0
           OR CAR-ANNEE = 0
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-CARRI IDX-1
                 GO READ-CAR
              END-IF
              GO READ-CAR-END.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-CARRI IDX-1
                 IF EXC-KEY = 65 
                    MOVE 9999 TO CAR-ANNEE
                 END-IF
                 GO READ-CAR
              END-IF
              IF CHOIX NOT = 0
                 GO READ-CAR-END
              END-IF
           END-IF.
           GO READ-CAR.
       READ-CAR-END.
           IF CHOIX = 0
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82 AND NOT = 68
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX COMPTEUR IDX-1.
           MOVE CAR-MOIS  TO HE-Z2 H-M(IDX-1).
           MOVE CAR-ANNEE TO H-A(IDX-1).
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 2.
           DISPLAY CAR-ANNEE LINE LIN-IDX POSITION 5.
           MOVE CAR-METIER TO MET-CODE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           DISPLAY MET-NOM(PR-CODE-SEXE) LINE LIN-IDX 
                                         POSITION 10 SIZE 10. 
           MOVE CAR-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           DISPLAY COUT-NOM LINE LIN-IDX POSITION 21 SIZE 15.
           MOVE CAR-STATUT TO STAT-CODE.
           CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY.
           DISPLAY STAT-NOM LINE LIN-IDX POSITION 37 SIZE 3. 
           COMPUTE IDX-4 = CAR-HOR-MEN  + 1.
           DISPLAY HOR-MENS(LG-IDX, IDX-4) LINE LIN-IDX POSITION 41. 
           MOVE CAR-SAL-ACT  TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE LIN-IDX POSITION 44.
           IF CAR-SAL-ACT = 0
             MOVE CAR-POURCENT TO HE-Z4
             DISPLAY HE-Z4 LINE LIN-IDX POSITION 45 
             DISPLAY "%" LINE LIN-IDX POSITION 51 
           END-IF.
           MOVE CAR-GRADE  TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 53.
           MOVE CAR-ECHELON TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 57.
           MOVE CAR-POSTE-FRAIS TO HE-Z8.
           IF CAR-GRADE = 0
              DISPLAY HE-Z8 LINE LIN-IDX POSITION 53.

           MOVE CAR-CONGE TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 62. 
           MOVE CAR-JOURS-CONGE TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 65. 
           MOVE CAR-REGIME TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 68. 
           MOVE CAR-EQUIPE TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 72. 
           MOVE CAR-HRS-MOIS  TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 76.


       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0000530000 TO EXC-KFR (11).
           MOVE 0000000065 TO EXC-KFR (13).
           MOVE 6667000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE CHOIX.
           ACCEPT CHOIX 
              LINE 3 POSITION 2 SIZE 1
              TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 66 
              MOVE 13 TO EXC-KEY
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 13
              INITIALIZE HE-CARRI
              GO INTERRUPT-END.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF CHOIX NOT = 0
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IDX-1.

       AFFICHAGE-ECRAN.
           MOVE 2233 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 10.
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 20 SIZE 50.

       END-PROGRAM.
           INITIALIZE CAR-RECORD.
           IF IDX-1 > 0
              MOVE H-A(IDX-1) TO CAR-ANNEE
              MOVE H-M(IDX-1) TO CAR-MOIS
           END-IF.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD FAKE-KEY.
           IF CAR-ANNEE NOT = 0
              MOVE CAR-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IDX-1 + 4.
           MOVE H-M(IDX-1) TO HE-M.
           ACCEPT HE-M
             LINE  LIN-IDX POSITION 2 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           MOVE H-M(IDX-1) TO HE-M.
           DISPLAY HE-M  LINE LIN-IDX POSITION 2.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN OTHER
                     COMPUTE CHOIX = H-A(IDX-1) * 100 + H-M(IDX-1)
                     PERFORM END-PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-A(IDX-1) = 0
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.
                        