      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-CARRI MODULE GENERAL LECTURE CARRIERE     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-CARRI.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CARRIERE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CARRIERE.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  ACTION   PIC X.
       01  DEL-KEY  PIC 9(4) COMP-1 VALUE 98.
       01  WR-KEY   PIC 9(4) COMP-1 VALUE 99.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "CARRIERE.LNK".
           COPY "REGISTRE.REC".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V REG-RECORD LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CARRIERE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-CARRI.
       
           IF NOT-OPEN = 0
              OPEN I-O CARRIERE
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CAR-RECORD.
           IF CAR-ANNEE = 0
              MOVE LNK-ANNEE TO CAR-ANNEE
              MOVE LNK-MOIS  TO CAR-MOIS
           END-IF.
           MOVE FR-KEY     TO CAR-FIRME.
           MOVE REG-PERSON TO CAR-PERSON.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 99
              PERFORM WRITE-CAR
              EXIT PROGRAM
           END-IF.

           IF EXC-KEY = 98
              IF LNK-SQL = "Y" 
                 CALL "9-CARRI" USING LINK-V CAR-RECORD DEL-KEY 
              END-IF 
              IF LNK-LOG = "Y" 
                 CALL "6-ACARRI" USING LINK-V CAR-RECORD DEL-KEY 
              END-IF
              DELETE CARRIERE INVALID CONTINUE END-DELETE
              EXIT PROGRAM
           END-IF.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ CARRIERE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ CARRIERE NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 0 PERFORM START-3
               READ CARRIERE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ CARRIERE NO LOCK INVALID INITIALIZE 
                CAR-REC-DET END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY     NOT = CAR-FIRME 
           OR REG-PERSON NOT = CAR-PERSON
              GO EXIT-1
           END-IF.
           IF CAR-POURCENT <= 0
              PERFORM WRITE-CAR
              MOVE 100 TO CAR-POURCENT.
           IF CAR-OCCUPATION <= 0
           OR CAR-OCCUPATION > 14
              IF  CAR-REGIME > 0
              AND CAR-REGIME < 100
                 EVALUATE CAR-REGIME
                    WHEN 1 MOVE  1 TO CAR-OCCUPATION 
                    WHEN 2 MOVE  3 TO CAR-OCCUPATION 
                    WHEN 3 IF CAR-STATUT = 3
                              MOVE 10 TO CAR-OCCUPATION 
                           ELSE
                              MOVE 13 TO CAR-OCCUPATION 
                           END-IF
                    WHEN 4 MOVE  9 TO CAR-OCCUPATION 
                    WHEN 5 MOVE  5 TO CAR-OCCUPATION 
                    WHEN 9 MOVE  9 TO CAR-OCCUPATION 
                 END-EVALUATE 
                 IF  FR-ETAB-A = 0
                 AND FR-ETAB-N = 5000
                     MOVE 9 TO CAR-OCCUPATION 
                 END-IF
                 IF CAR-STATUT >= 1
                    PERFORM WRITE-CAR
                 END-IF
              END-IF
           END-IF.
           MOVE CAR-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START CARRIERE KEY < CAR-KEY INVALID GO EXIT-1.
       START-2.
           START CARRIERE KEY > CAR-KEY INVALID GO EXIT-1.
       START-3.
           START CARRIERE KEY <= CAR-KEY INVALID GO EXIT-1.

       WRITE-CAR.
           IF CAR-STATUT >= 1
              PERFORM WRITE-CAR-1.

       WRITE-CAR-1.
           IF LNK-SQL = "Y" 
              CALL "9-CARRI" USING LINK-V CAR-RECORD EXC-KEY 
           END-IF.
           IF LNK-LOG = "Y" 
              CALL "6-ACARRI" USING LINK-V CAR-RECORD WR-KEY 
           END-IF.
           WRITE CAR-RECORD INVALID REWRITE CAR-RECORD END-WRITE.

           COPY "XMESSAGE.CPY".
