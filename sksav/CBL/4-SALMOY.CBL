      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-SALMOY BASES DE REMUNERATION              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-SALMOY.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "POINTS.REC".
           COPY "BAREME.REC".
           COPY "ARRONDI.CPY".

       01  JOUR                 PIC 99.
       01  SAL-ACT              PIC 9(7)V9(5).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02 SAL-ACT-A         PIC 9(7).
           02 SAL-ACT-B         PIC 9(5).
       01  SAL-ACT-R1 REDEFINES SAL-ACT.
           02 SAL-ACT-A1        PIC 9(9).
           02 SAL-ACT-B1        PIC 9(3).
       01  SAL-ARR1             PIC 9(7)V9.
       01  SAL-ARR2             PIC 9(7)V99.
       01  SAL-ARR3             PIC 9(7)V999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4Z4 PIC Z(4),ZZZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "CCOL.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "HORSEM.REC".
           COPY "V-BASES.REC".
           COPY "LIVRE.CUM".

       PROCEDURE DIVISION USING LINK-V
                                CCOL-RECORD
                                CAR-RECORD
                                PRESENCES 
                                HJS-RECORD 
                                BASES-REMUNERATION
                                LCUM-RECORD.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-SALMOY.
       
       BASES-SALAIRE.
           COMPUTE BAS-HOR-MOY = BAS-HORAIRE.
           
           EVALUATE CAR-CONGE
               WHEN 2 COMPUTE BAS-CONGE-MOYENNE = 
                      LCUM-MOY(3, 1) / LCUM-UNITE(3, 111)
               WHEN OTHER MOVE LCUM-MOY(2, 1) TO SH-00
                    IF CCOL-3-MOIS-CONGE NOT = "N" 
                       ADD LCUM-MOY(2, 4) TO SH-00
                    ELSE 
                       IF LCUM-FLAG(2, 9) = 3
                          ADD LCUM-MOY(2, 4) TO SH-00
                       END-IF
                    END-IF
                    COMPUTE BAS-MOYENNE = SH-00 / LCUM-UNITE(2, 111)
                    COMPUTE BAS-HOR-MOY = BAS-MOYENNE + BAS-HORAIRE
                    IF CAR-CONGE = 1 
                    OR CAR-CONGE = 7 
                       MOVE BAS-MOYENNE TO BAS-CONGE-MOYENNE 
                    END-IF
           END-EVALUATE.                      

           COMPUTE BAS-MALADIE-MOYENNE = LCUM-MOY(2, 2)
                                       / LCUM-UNITE(2, 112).
           COMPUTE BAS-CHOM-I = (LCUM-MOY(2, 1) / LCUM-UNITE(2, 111)) 
                              + BAS-HORAIRE.

           IF CAR-HOR-MEN = 0
              MOVE BAS-HOR-MOY TO BAS-CONGE
           ELSE
              MOVE BAS-CONGE-MOYENNE TO BAS-CONGE
           END-IF.
           IF FR-ANNEE-ARRONDI >= LNK-ANNEE
              EVALUATE FR-ARRONDI 
              WHEN 1
              PERFORM ARR-1 VARYING IDX FROM 1 BY 1 UNTIL IDX > 21
              WHEN 2
              PERFORM ARR-2 VARYING IDX FROM 1 BY 1 UNTIL IDX > 21
              WHEN 3
              PERFORM ARR-3 VARYING IDX FROM 1 BY 1 UNTIL IDX > 21
              END-EVALUATE
           END-IF.

      *    perform affichage-valeurs.
           EXIT PROGRAM.

       ARR-1.
           IF BAS-ARR(IDX) = 1
              COMPUTE SAL-ARR1 = BAS-SAL(IDX) + ,05
              MOVE SAL-ARR1 TO BAS-SAL(IDX).
       ARR-2.
           IF BAS-ARR(IDX) = 1
              COMPUTE SAL-ARR2 = BAS-SAL(IDX) + ,005
              MOVE SAL-ARR2 TO BAS-SAL(IDX).
       ARR-3.
           IF BAS-ARR(IDX) = 1
              COMPUTE SAL-ARR3 = BAS-SAL(IDX) + ,0005
              MOVE SAL-ARR3 TO BAS-SAL(IDX).


       AFFICHAGE-VALEURS.
           MOVE 0 TO IDX-2.
           PERFORM DISPLAY-VAL VARYING IDX  FROM 0 BY 1 UNTIL 
                    IDX > 1.
                    
           accept action no beep.
       DISPLAY-VAL.
           COMPUTE COL-IDX = 1 + IDX * 14.
           PERFORM DISPLAY-DET-VAL VARYING IDX-1 FROM 1 BY 1 UNTIL 
                    IDX-1 > 20.

       DISPLAY-DET-VAL.
           ADD 1 TO IDX-2.
           COMPUTE LIN-IDX = 2 + IDX-1.
           MOVE IDX-2 TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION COL-IDX LOW.
           MOVE BAS-SAL(IDX-2)  TO HE-Z4Z4.
           COMPUTE IDX-4 = COL-IDX + 2.
           DISPLAY HE-Z4Z4 LINE LIN-IDX POSITION IDX-4 HIGH.
