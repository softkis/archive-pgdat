      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 2-FRRM INTERROGATION FERIE RECUP REPOS MOBILE�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-FRRM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "LAYOUT.REC".
           COPY "CONGE.REC".
           COPY "LIVRE.REC".

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  MOIS                  PIC 99.
       01  SOLDE                 PIC S9999V99.

       01  ECR-DISPLAY.
           02 HE-Z2Z2 PIC ZZ,ZZ.
           02 HE-Z3Z2 PIC Z(3),ZZ.
           02 HE-Z4Z2 PIC -ZZZZ,ZZ.
           02 HE-Z6 PIC Z(6).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-FRRM.

           IF MENU-PROG-NUMBER < 2
           OR MENU-PROG-NUMBER > 5
              DISPLAY "MENU-PROG-NUMBER 2 <-> 5 !" LINE 24 POSITION 5
              DISPLAY MENU-PROG-NUMBER LINE 24 POSITION 40
              PERFORM AA
              EXIT PROGRAM
           END-IF.
           COMPUTE IDX-1 = 120 + MENU-PROG-NUMBER.
           COMPUTE IDX-2 = 130 + MENU-PROG-NUMBER.
       
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions


           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           END-EVALUATE.


       TRAITEMENT-ECRAN-01.

           PERFORM DISPLAY-F-KEYS.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1-1 
           WHEN  2 PERFORM AVANT-1-2.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN  9 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-1 
           WHEN  2 PERFORM APRES-1-2.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-1-1.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 
               PERFORM CHANGE-MOIS
               PERFORM IND-MOIS
               MOVE 27 TO EXC-KEY
           END-EVALUATE.

       AVANT-1-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       APRES-1-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM PRESENCE
                   PERFORM GET-PERS
           WHEN  OTHER PERFORM NEXT-REGIS.
           PERFORM DIS-E1-01.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE.
           PERFORM AFFICHAGE-DETAIL.
           IF PRES-ANNEE > 0
              PERFORM AFFICHAGE-PRES.
           
       APRES-1-2.
           MOVE "A" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   IF REG-PERSON > 0 
                      PERFORM AFFICHAGE-PRES
                   END-IF
           PERFORM DIS-E1-01.
           IF REG-PERSON = 0 
              MOVE 1 TO INPUT-ERROR
           END-IF.


       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DIS-E1-01.
           MOVE REG-PERSON  TO HE-Z6.
           IF REG-PERSON NOT = 0
              MOVE REG-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 
             LINE 3 POSITION 15 SIZE 6
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-E1-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-E1-END.

       AFFICHAGE-ECRAN.
           MOVE 2235 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE MENU-PROG-NUMBER TO LAY-NUMBER.
           ADD 10 TO LAY-NUMBER.
           MOVE LNK-LANGUAGE TO LAY-LANGUAGE.
           MOVE "FP" TO LNK-AREA.
           CALL "6-LAYOUT" USING LINK-V LAY-RECORD FAKE-KEY.
           DISPLAY LAY-DESCRIPTION LINE 1 POSITION 3 LOW SIZE 15.
           PERFORM IND-MOIS.

       IND-MOIS.
           MOVE "MO" TO LNK-AREA.
           PERFORM MOIS VARYING MOIS FROM 1 BY 1 UNTIL MOIS > 
           LNK-MOIS.
           MOVE MOIS TO IDX.
           PERFORM EFFACE VARYING MOIS FROM IDX BY 1 UNTIL MOIS > 12.

       MOIS.
           MOVE 00031200 TO LNK-POSITION.
           COMPUTE LNK-LINE =   9 + MOIS.
           COMPUTE LNK-NUM  = 100 + MOIS.
           MOVE "L" TO LNK-LOW.
           CALL "0-DMESS" USING LINK-V.

       EFFACE.
           COMPUTE LIN-IDX = 9 + MOIS.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       AFFICHAGE-DETAIL.
           PERFORM DIS-E1-01 THRU DIS-E1-END.

       AFFICHAGE-PRES.
           INITIALIZE SOLDE.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD FAKE-KEY.
           PERFORM DIS-REPORT.
           PERFORM DIS-MOIS VARYING MOIS FROM 1 BY 1 UNTIL MOIS > 
           LNK-MOIS.
           PERFORM DIS-SOLDE.

       DIS-MOIS.
           COMPUTE LIN-IDX = 9 + MOIS.
           INITIALIZE L-RECORD.
           MOVE FR-KEY     TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON. 
           MOVE MOIS       TO L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           PERFORM DIS-LIGNE.

       DIS-LIGNE.
           COMPUTE SH-00 = L-UNITE(IDX-1) - L-UNITE(IDX-2).
           MOVE SH-00 TO HE-Z4Z2.
           ADD SH-00 TO SOLDE.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z4Z2 LINE LIN-IDX POSITION 14.
           MOVE L-UNITE(IDX-1) TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z4Z2 LINE LIN-IDX POSITION 24.

           MOVE L-UNITE(IDX-2) TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z4Z2 LINE LIN-IDX POSITION 34.

       DIS-REPORT.
           MOVE CONGE-IDX(5) TO HE-Z4Z2 SOLDE.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z4Z2 LINE 7 POSITION 14.

       DIS-SOLDE.
           MOVE SOLDE TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z4Z2 LINE 22 POSITION 14.

       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS THRU SUBTRACT-END
             WHEN 58 PERFORM ADD-MOIS THRU ADD-END
           END-EVALUATE.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-DETAIL.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0 
           OR REG-PERSON = 0
              GO SUBTRACT-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO SUBTRACT-END.
           GO SUBTRACT-MOIS.
       SUBTRACT-END.
           IF LNK-MOIS = 0
              IF REG-PERSON = 0
                 MOVE 1 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.
       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
           OR REG-PERSON = 0
              GO ADD-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO ADD-END.
           GO ADD-MOIS.
       ADD-END.
           IF LNK-MOIS > 12
              IF REG-PERSON = 0
                 MOVE 12 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.

       END-PROGRAM.
           CANCEL "6-CONGE".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

           