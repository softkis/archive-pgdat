      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-POSE IMPRESSION POSES PAR EQUIPE          �
      *  � EX        7-AUTO REMPLISSAGE HEURES + CALCUL SALAIRE  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-POSE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".
           COPY "TRIPR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 8.
       01  PRECISION             PIC 9 VALUE 0.
       01  JOB-STANDARD          PIC X(10) VALUE "80       ".
       01  TAB-LIN        PIC 99 VALUE 10.
       01  TAB-COL        PIC 99 VALUE 17.
       01  ESP-LIN        PIC 99 VALUE 10.
       01  ESP-COL        PIC 99 VALUE  8.
       01  POSE-DEF      PIC X(10) VALUE "L M AMN N2".
       01  POSE-DEF-R REDEFINES POSE-DEF.
           02 POSE       PIC XX OCCURS 5.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "FICHE.REC".
           COPY "PRESENCE.REC".
           COPY "STATUT.REC".
           COPY "OCCUP.REC".
           COPY "CONGE.REC".
           COPY "COUT.REC".
           COPY "CCOL.REC".
           COPY "CALEN.REC".
           COPY "HORSEM.REC".
           COPY "JOURS.REC".
           COPY "LIVRE.REC".
           COPY "POSE.REC".
           COPY "METIER.REC".
           COPY "MESSAGE.REC".
           COPY "EQUIPE.REC".
           COPY "POCL.REC".
           COPY "IMPRLOG.REC".


           COPY "V-VAR.CPY".
       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.
       01  JOUR                  PIC 99.
       01  CHOIX                 PIC 99 VALUE 3.
       01  EQUIPE                PIC 99 VALUE 0.

       01  COMPTEUR              PIC 999V99 VALUE 0.
       01  COMPTEUR-1            PIC 99 VALUE 0.


       01  HEURES-THEO.
           03  THEO-NORM       PIC 999V99.
           03  THEO-NUIT       PIC 999V99.
           03  THEO-DIM        PIC 999V99.
           03  THEO-FERIE-T    PIC 999V99.
           03  THEO-NUIT-2     PIC 999V99.

       01  SOLDES.
           04 SOLDE              PIC S9999V99 OCCURS 4.
      * 1  CONGE   
      * 2  FERIE   
      * 3  RECUP   
      * 4  REPOS-C 

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".POS".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".

       01  JOUR-CAL              PIC 99 VALUE 0.
       01  JOUR-DEB              PIC 99.
       01  JOUR-FIN              PIC 99.

       01  JOURS-CUMUL.
           02 HR-C OCCURS 10.
              03 HR-TOT-MOIS  PIC S999V99 COMP-3 OCCURS 32.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z2Z2 PIC ZZ,ZZ. 

       01  TOT-OCCUP.
           02 TOT-OCC-IDX OCCURS 200.
              03 OCCUP-UNITE     PIC 9(8)V999 COMP-3.
              03 OCCUP-JOURS     PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-POSE.
       
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR (14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-SORT
           WHEN  8 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-SORT
           WHEN  8 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 MOVE "AA" TO LNK-AREA
                  MOVE 27 TO LNK-NUM
                  PERFORM DISPLAY-MESSAGE
                  PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.

           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.
           
       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 13 TO SAVE-KEY.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           INITIALIZE HEURES-THEO SOLDES.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
           OR CAR-EQUIPE = 0
           OR POSE-JRS(LNK-MOIS) = 0
              GO READ-PERSON-2
           END-IF.
           IF STATUT > 0 AND NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF COUT > 0 AND NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           IF EQUIPE > 0 AND NOT = CAR-EQUIPE
              GO READ-PERSON-2
           END-IF.
           PERFORM DEB-FIN.
           INITIALIZE OCC-RECORD JRS-RECORD
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD FAKE-KEY.
           IF JRS-PERSON = 0
              PERFORM NEXT-OCCUP 
              PERFORM HRFILL
              IF CCOL-JRF-AUTOMAT NOT = "N"
              AND JOUR-CAL > 0
              CALL "4-FERIE" USING LINK-V 
                             REG-RECORD
                             PRESENCES
                             CAR-RECORD
                             CCOL-RECORD
                             POSE-RECORD
                             HJS-RECORD
                             CAL-RECORD
                             JOUR-DEB
                             JOUR-FIN
              END-IF
           END-IF.

           PERFORM DIS-HE-01.
           PERFORM CALCULE.
           PERFORM IMPRIME.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       HRFILL.
           MOVE 0 TO LNK-VAL LNK-VAL-2.
           CALL "4-HRFILL" USING LINK-V 
                                 REG-RECORD
                                 PRESENCES
                                 CAR-RECORD
                                 CCOL-RECORD
                                 POSE-RECORD
                                 HJS-RECORD
                                 CAL-RECORD
                                 OCC-RECORD
                                 JOUR-DEB
                                 JOUR-FIN.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DEB-FIN.
           MOVE 0 TO JOUR-DEB JOUR-FIN JOUR-CAL.
           PERFORM TEST-FIN VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.

       TEST-FIN.
           IF PRES-JOUR(LNK-MOIS, IDX) = 1
              IF JOUR-DEB = 0
                 MOVE IDX TO JOUR-DEB 
              END-IF
              MOVE IDX TO JOUR-FIN 
              IF CAL-JOUR(LNK-MOIS, IDX) > 0
                 ADD 1 TO JOUR-CAL 
              END-IF
           END-IF.

       CALCULE.
           MOVE 0 TO LNK-SUITE.
           CALL "0-SDFAUT" USING LINK-V REG-RECORD CAR-RECORD 
                                 CCOL-RECORD CAL-RECORD.
           IF PRES-TOT(LNK-MOIS) < MOIS-JRS(LNK-MOIS)
              CALL "4-CTLJRS" USING LINK-V REG-RECORD PRESENCES
              CANCEL "4-CTLJRS"
           END-IF.
           PERFORM CUMUL-HRS THRU CUMUL-END
           VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > 9.
           MOVE 0 TO IDX-4 LNK-SUITE.
           PERFORM C-H.

       CUMUL-HRS.
           INITIALIZE L-RECORD JRS-RECORD.
           MOVE IDX-4 TO LNK-SUITE L-SUITE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY
           IF L-DATE-EDITION NOT = 0
              GO CUMUL-END
           END-IF.
           MOVE 98 TO SAVE-KEY.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD SAVE-KEY.
           CALL "4-CSPMAL" USING LINK-V REG-RECORD L-RECORD.
           IF L-IMPOSABLE-BRUT = 0
              CALL "4-CSPDEL" USING LINK-V REG-RECORD.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE LNK-MOIS   TO JRS-MOIS.
           MOVE REG-PERSON TO JRS-PERSON.
           MOVE IDX-4 TO LNK-SUITE JRS-OCCUPATION.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD FAKE-KEY
           IF JRS-PERSON = 0
              GO CUMUL-END.
           PERFORM C-H.
       CUMUL-END.
           CANCEL "4-CSPMAL".
           CANCEL "4-CSPDEL".

       C-H.
           CALL "4-CUMHRS" USING LINK-V REG-RECORD CAR-RECORD 
           CCOL-RECORD TOT-OCCUP PRESENCES POSE-RECORD HJS-RECORD
           CAL-RECORD.
           CANCEL "4-CUMHRS".
           CALL "4-AUTO" USING LINK-V 
                               PR-RECORD 
                               REG-RECORD 
                               CAR-RECORD 
                               FICHE-RECORD
                               CCOL-RECORD 
                               TOT-OCCUP 
                               PRESENCES 
                               POSE-RECORD 
                               HJS-RECORD.
           IF IDX-4 > 0
              INITIALIZE L-RECORD
              MOVE IDX-4 TO L-SUITE
              CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY
              CALL "4-CSPMAL" USING LINK-V REG-RECORD L-RECORD
           END-IF.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD POSE-RECORD FICHE-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD SAVE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           INITIALIZE POSE-RECORD HJS-RECORD.
           CALL "6-GHJS" USING LINK-V HJS-RECORD  CAR-RECORD PRESENCES.
           MOVE CAR-EQUIPE TO POSE-CODE.
           CALL "6-POSE" USING LINK-V POSE-RECORD FAKE-KEY.
           MOVE CCOL-CALENDRIER TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.
           PERFORM CONGES.

       NEXT-OCCUP.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD SAVE-KEY.
           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE EQ-NUMBER TO HE-Z2.
           DISPLAY HE-Z2  LINE 11 POSITION 31.
           DISPLAY EQ-NOM LINE 11 POSITION 35.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1358 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE LNK-MOIS TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 04471500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
           DISPLAY LNK-ANNEE LINE 04 POSITION 57.
           CANCEL "0-DMESS".

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF COUNTER > 0 
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE
           END-IF.
           CANCEL "P080".
           CANCEL "6-OCCUP".
           CANCEL "6-JOUR".
           CANCEL "6-CONGE".
           CANCEL "6-OCCOM".
           CANCEL "6-COUT".
           CANCEL "6-CCOL".
           CANCEL "2-OCCUP".
           CANCEL "4-AUTO".
           CANCEL "4-LPCUM".
           CANCEL "4-CALCUL".
           CANCEL "6-GCP".
           CANCEL "0-SDFAUT".
           CANCEL "4-CSPDEL".
           CANCEL "6-CODPAI".
           CANCEL "4-CSPMAL".
           CANCEL "4-CSADD".
           CANCEL "4-LPVENT".
           CANCEL "4-HRFILL".
           CANCEL "4-FORMUL".
           CANCEL "4-PROC".
           CANCEL "4-NXLP".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XEQUIPE.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XSORT.CPY".


       IMPRIME.
           INITIALIZE JOURS-CUMUL JRS-RECORD.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE LNK-MOIS   TO JRS-MOIS.
           MOVE REG-PERSON TO JRS-PERSON.
           MOVE 13 TO SAVE-KEY.
           PERFORM NEXT-JOUR THRU NEXT-JOUR-END.

       NEXT-JOUR.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD SAVE-KEY
           MOVE 66 TO SAVE-KEY.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
              GO NEXT-JOUR-END
           END-IF.
           IF JRS-COMPLEMENT > 0
           OR JRS-POSTE > 0
              GO NEXT-JOUR
           END-IF.
           ADD JRS-HRS(32) TO COMPTEUR.
           PERFORM FILL-HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 32.
           MOVE 66 TO SAVE-KEY.
           GO NEXT-JOUR.
       NEXT-JOUR-END.
           IF COMPTEUR > 0
              PERFORM FULL-PROCESS.



       FULL-PROCESS.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 1 TO COUNTER
           END-IF.
           MOVE PR-LANGUAGE TO FORM-LANGUE.
           PERFORM READ-FORM.
           PERFORM ENTETE.
           PERFORM DESSINE-CAL.
           PERFORM TRANSMET.
           PERFORM DIS-HE-01.

       FILL-HEURES.
           IF JRS-HRS(IDX) > 0
              EVALUATE JRS-OCCUPATION
               WHEN  0         ADD JRS-HRS(IDX) TO HR-TOT-MOIS(1, IDX) 
               WHEN  1 THRU 12 ADD JRS-HRS(IDX) TO HR-TOT-MOIS(3, IDX) 
               WHEN 32         ADD JRS-HRS(IDX) TO HR-TOT-MOIS(2, IDX) 
               WHEN 15         ADD JRS-HRS(IDX) TO HR-TOT-MOIS(4, IDX) 
               WHEN 20         ADD JRS-HRS(IDX) TO HR-TOT-MOIS(4, IDX) 
               WHEN OTHER      ADD JRS-HRS(IDX) TO HR-TOT-MOIS(5, IDX) 
              END-EVALUATE
           END-IF.

       ENTETE.
      * PAGE-DATE.
           MOVE  2 TO LIN-NUM .
           PERFORM MOIS-NOM.
           MOVE 45 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 58 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
      * PERSONNE
           MOVE REG-PERSON TO VH-00.
           MOVE 6 TO LIN-NUM. 
           MOVE 3 TO COL-NUM.
           MOVE 6 TO CAR-NUM.
           MOVE 0 TO DEC-NUM.
           PERFORM FILL-FORM.

           MOVE PR-NOM TO ALPHA-TEXTE.
           MOVE  6 TO LIN-NUM.
           MOVE 11 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE CAR-PRIME-1 TO VH-00.
           MOVE  6 TO LIN-NUM.
           MOVE  4 TO CAR-NUM. 
           MOVE 58 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE CAR-POSTE-FRAIS TO VH-00 PC-NUMBER.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY.
           MOVE  7 TO LIN-NUM.
           MOVE  6 TO CAR-NUM. 
           MOVE 56 TO COL-NUM.
           PERFORM FILL-FORM.

      * EQUIPE
           MOVE CAR-EQUIPE TO EQ-NUMBER.
           CALL "6-EQUIPE" USING LINK-V EQ-RECORD FAKE-KEY.
           MOVE  5 TO LIN-NUM .
           MOVE EQ-NOM TO ALPHA-TEXTE.
           MOVE 45 TO COL-NUM.
           PERFORM FILL-FORM.
      * DEPT 
           MOVE  4 TO LIN-NUM .
           MOVE CAR-COUT TO VH-00 COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           MOVE 42 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE COUT-NOM TO ALPHA-TEXTE.
           MOVE 45 TO COL-NUM.
           PERFORM FILL-FORM.

      * FIRME 
           MOVE FR-KEY TO VH-00.
           MOVE  2 TO LIN-NUM. 
           MOVE  5 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           MOVE  2 TO LIN-NUM. 
           MOVE 11 TO COL-NUM.
           PERFORM FILL-FORM.
           PERFORM HRS-THEO VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.

      * HEURES THEORIQUES
           MOVE  2 TO LIN-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE 72 TO COL-NUM.
           MOVE THEO-NORM  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  3 TO LIN-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE 72 TO COL-NUM.
           MOVE THEO-NUIT  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO LIN-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE 72 TO COL-NUM.
           MOVE THEO-NUIT-2 TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO LIN-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE 76 TO COL-NUM.

           MOVE THEO-DIM   TO VH-00.
           PERFORM FILL-FORM.
           MOVE  5 TO LIN-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE 72 TO COL-NUM.
           MOVE THEO-FERIE-T TO VH-00.
           PERFORM FILL-FORM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 72 TO COL-NUM.
           COMPUTE VH-00 = SOLDE(2) / 8.
           PERFORM FILL-FORM.
           MOVE  6 TO LIN-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 72 TO COL-NUM.
           COMPUTE VH-00 = SOLDE(4) / 8.
           PERFORM FILL-FORM.
           MOVE  7 TO LIN-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 72 TO COL-NUM.
           COMPUTE VH-00 = SOLDE(1) / 8.
           PERFORM FILL-FORM.

       DESSINE-CAL.
           PERFORM CALENDRIER VARYING 
           JOUR FROM 1 BY 1 UNTIL JOUR > MOIS-JRS(LNK-MOIS).
            
       CALENDRIER.
           COMPUTE LIN-NUM = SEM-LINE(LNK-MOIS, JOUR) 
                           * ESP-LIN + TAB-LIN - 11
           COMPUTE COL-NUM = SEM-IDX(LNK-MOIS, JOUR) 
                           * ESP-COL + TAB-COL.
           MOVE 2 TO CAR-NUM.
           MOVE JOUR TO VH-00.
           PERFORM FILL-FORM.

      * HEURES PLANIFIEES
           ADD 4 TO LIN-NUM.
      * HEURES TRAVAIL
           IF HR-TOT-MOIS(1, JOUR) > 0
              COMPUTE COL-NUM = SEM-IDX(LNK-MOIS, JOUR) 
                              * ESP-COL + TAB-COL + 1
              MOVE 2 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              MOVE HR-TOT-MOIS(1, JOUR) TO VH-00
              PERFORM FILL-FORM
           END-IF.
           ADD 2 TO LIN-NUM.
      * HEURES REPOS
           IF HR-TOT-MOIS(2, JOUR) > 0
              COMPUTE COL-NUM = SEM-IDX(LNK-MOIS, JOUR) 
                              * ESP-COL + TAB-COL - 1
               MOVE 1 TO CAR-NUM
               MOVE 2 TO DEC-NUM
               MOVE HR-TOT-MOIS(2, JOUR) TO VH-00
               PERFORM FILL-FORM
           END-IF.
      * HEURES MALADIE
           IF HR-TOT-MOIS(3, JOUR) > 0
              COMPUTE COL-NUM = SEM-IDX(LNK-MOIS, JOUR) 
                              * ESP-COL + TAB-COL + 3
              MOVE 1 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              MOVE HR-TOT-MOIS(3, JOUR) TO VH-00
              PERFORM FILL-FORM
           END-IF.
      * HEURES CONGE
           ADD 2 TO LIN-NUM.
           IF HR-TOT-MOIS(4, JOUR) > 0
              COMPUTE COL-NUM = SEM-IDX(LNK-MOIS, JOUR) 
                              * ESP-COL + TAB-COL - 1
              MOVE 1 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              MOVE HR-TOT-MOIS(4, JOUR) TO VH-00
              PERFORM FILL-FORM
           END-IF.
      * HEURES CONGE EXTRA
           IF HR-TOT-MOIS(5, JOUR) > 0
              COMPUTE COL-NUM = SEM-IDX(LNK-MOIS, JOUR) 
                              * ESP-COL + TAB-COL + 3
              MOVE 1 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              MOVE HR-TOT-MOIS(5, JOUR) TO VH-00
              PERFORM FILL-FORM
           END-IF.

           PERFORM TEXTE-POSE.

       TEXTE-POSE.
           COMPUTE LIN-NUM = SEM-LINE(LNK-MOIS, JOUR) 
                           * ESP-LIN + TAB-LIN - 9.
           COMPUTE COL-NUM = SEM-IDX(LNK-MOIS, JOUR) 
                           * ESP-COL + TAB-COL + 3.
           COMPUTE IDX-3 = POSE-JOUR(LNK-MOIS, JOUR) + 1.
           MOVE POSE(IDX-3) TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

        HRS-THEO.
           IF PRES-JOUR(LNK-MOIS, IDX) = 1
              IF  POSE-JOUR(LNK-MOIS, IDX) > 0
              AND POSE-JOUR(LNK-MOIS, IDX) < 5
                  ADD 8 TO THEO-NORM
                  IF  SEM-IDX(LNK-MOIS, IDX) = 7 
                  AND CAL-JOUR(LNK-MOIS, IDX) = 0
                      ADD 8 TO THEO-DIM
                  END-IF
                  IF  CAL-JOUR(LNK-MOIS, IDX) > 0
                  AND CAL-JOUR(LNK-MOIS, IDX) < 4
                      ADD 8 TO THEO-FERIE-T
                  END-IF
                  IF  POSE-JOUR(LNK-MOIS, IDX) = 3
                      ADD 8 TO THEO-NUIT
                  END-IF
                  IF  POSE-JOUR(LNK-MOIS, IDX) = 4
                      ADD 8 TO THEO-NUIT-2
                  END-IF
               END-IF
           END-IF.

       CONGES.
           INITIALIZE SOLDES.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD FAKE-KEY.
           PERFORM LOAD-REPORT VARYING IDX FROM 1 BY 1 UNTIL IDX > 4.
           PERFORM R-MOIS VARYING IDX FROM 1 BY 1 UNTIL IDX > LNK-MOIS.

       LOAD-REPORT.
           MOVE CONGE-IDX(IDX) TO SOLDE(IDX).

       R-MOIS.
           INITIALIZE L-RECORD.
           MOVE FR-KEY     TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON. 
           MOVE IDX        TO L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           PERFORM R-LIGNE VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 4.
           COMPUTE SOLDE(1) = SOLDE(1) + CONGE-AJUSTE + CONGE-TOTAL.
           SUBTRACT L-UNI-CONGE   FROM SOLDE(1).
           SUBTRACT L-UNI-FERIE   FROM SOLDE(2).
           SUBTRACT L-UNI-RECUP   FROM SOLDE(3).
           SUBTRACT L-UNI-REPOS-C FROM SOLDE(4).

       R-LIGNE.
           COMPUTE IDX-2 = 10 + IDX-1.
           ADD L-UNITE-ST(IDX-2) TO SOLDE(IDX-1).
           COMPUTE IDX-2 = 20 + IDX-1.
           SUBTRACT L-UNITE-ST(IDX-2) FROM SOLDE(IDX-1).

