      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-TRJEUN IMPRESSION MISE AU TRAVAIL DES     �
      *  � JEUNES RELEVE-ANNEXE ET DECLARATION DE CREANCE        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-TRJEUN.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "LIVRE.REC".
           COPY "CODPAIE.REC".
           COPY "BANQF.REC".
           COPY "BANQUE.REC".
           COPY "PARMOD.REC".
           COPY "CONTRAT.REC".
           COPY "MOTDEP.REC".
           COPY "TXACCID.REC".
           COPY "TAUXSS.REC".
           COPY "MUT.REC".

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  COUNTER               PIC 9999 VALUE 0.
       01  TRIMESTER             PIC 9 VALUE 0.
       01  ARR                   PIC 9(8)V99.

       01 HELP-CUMUL.
          02 HELP-INDIVIDUEL.
             03 HELP-VAL             PIC 9(8)V99 OCCURS 13.
          02 HELP-MONTANT.
             03 HELP-BASE            PIC 9(8)V99.
             03 HELP-POURCENT        PIC 9(8)V99.
             03 HELP-TAUX            PIC 9(8)V99.
             03 HELP-PP              PIC 9(8)V99.
             03 HELP-COUT-TOTAL      PIC 9(8)V99.

           COPY "V-VAR.CPY".
        
       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-TRJEUN.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-PATH TO BQF-CODE.
           MOVE LNK-MOIS TO SAVE-MOIS.
           DIVIDE LNK-MOIS BY 3 GIVING TRIMESTER REMAINDER IDX-2.
           IF IDX-2 > 0
              ADD 1 TO TRIMESTER
           END-IF.
           COMPUTE MOIS-FIN = 3 * TRIMESTER.
           COMPUTE MOIS-DEBUT = MOIS-FIN - 2.
           CALL "0-TODAY" USING TODAY.
           INITIALIZE TXA-RECORD MUT-RECORD.
           CALL "6-ASSACC" USING LINK-V TXA-RECORD.
           IF FR-ACCIDENT = 0
              MOVE 1 TO FR-ACCIDENT 
           END-IF.
           IF LNK-ANNEE > 2008
               CALL "6-MUT" USING LINK-V MUT-RECORD NUL-KEY
               IF MUT-CLASSE = 0
                  MOVE 1 TO MUT-CLASSE
               END-IF
               IF CAR-REGIME > 99
                  MOVE 5 TO MUT-CLASSE
               END-IF
           END-IF.
           INITIALIZE HELP-CUMUL.

           PERFORM AFFICHAGE-ECRAN .
           MOVE 66 TO SAVE-KEY.
           IF BQF-CODE = SPACES
              PERFORM NEXT-BANQF.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5 THRU 6
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-4
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-4.
           ACCEPT BQF-CODE
             LINE 15 POSITION 25 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-5.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
                     PERFORM APRES-5 THRU APRES-6
             WHEN 66 ADD      1 TO   MOIS-DEBUT
                     PERFORM APRES-5 THRU APRES-6
             END-EVALUATE.

       AVANT-6.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
                     PERFORM APRES-5 THRU APRES-6
             WHEN 66 ADD      1 TO   MOIS-FIN
                     PERFORM APRES-5 THRU APRES-6
             END-EVALUATE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-4.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-BANQF" USING LINK-V BQF-RECORD
                   IF BQF-CODE NOT = SPACES
                      MOVE BQF-CODE TO BQ-CODE
                   END-IF
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER MOVE EXC-KEY TO SAVE-KEY
                      PERFORM NEXT-BANQF
                      MOVE BQF-CODE TO BQ-CODE
           END-EVALUATE.
           IF BQF-KEY = SPACES MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0
              MOVE BQF-CODE TO PARMOD-PATH
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
           PERFORM DIS-HE-04.

       APRES-5.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-05.
           PERFORM DIS-HE-06.

       APRES-6.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-05.
           PERFORM DIS-HE-06.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-PERSON 
                    PERFORM READ-PERSON THRU READ-EXIT
                    PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM DIS-HE-01.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-3
           END-IF.
           INITIALIZE HELP-CUMUL.
           PERFORM CUMUL-LIVRE.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.
           EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           MOVE 27 TO COL-IDX.
           IF A-N = "A"
              ADD 11 TO COL-IDX
              DISPLAY SPACES LINE 6 POSITION 37 SIZE 1.
           COMPUTE IDX = 80 - COL-IDX.
           MOVE 0 TO SIZ-IDX IDX-1.
           INSPECT PR-NOM TALLYING  SIZ-IDX FOR CHARACTERS BEFORE "  ".
           INSPECT PR-NOM-JF TALLYING IDX-1 FOR CHARACTERS BEFORE "  ".

           IF FR-NOM-JF = SPACES
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX  = COL-IDX + SIZ-IDX + 1
              IF PR-NOM-JF NOT = SPACES
                 COMPUTE IDX = 80 - COL-IDX
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
           ELSE
              IF PR-NOM-JF NOT = SPACES
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX = COL-IDX + SIZ-IDX + 1
           END-IF.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 6 POSITION COL-IDX SIZE IDX LOW.
       DIS-HE-04.
           MOVE BQF-CODE TO BQ-CODE.
           PERFORM READ-BANQUE.
           DISPLAY BQF-CODE   LINE 15 POSITION 25.
           DISPLAY BQ-NOM     LINE 15 POSITION 40.
           DISPLAY BQF-COMPTE LINE 16 POSITION 40.
       DIS-HE-05.
           DISPLAY MOIS-DEBUT LINE 19 POSITION 31.
           MOVE MOIS-DEBUT TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-06.
           DISPLAY MOIS-FIN LINE 20 POSITION 31.
           MOVE MOIS-FIN TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       READ-BANQUE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD EXC-KEY.
       NEXT-BANQF.
           CALL "6-BANQF" USING LINK-V BQF-RECORD SAVE-KEY.

       CUMUL-LIVRE.
           PERFORM GET-CS VARYING LNK-MOIS
           FROM MOIS-DEBUT BY 1 UNTIL LNK-MOIS > MOIS-FIN.
           IF HELP-VAL(13) > 0 
              PERFORM TRANSMET
           END-IF.

       GET-CS.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY     TO CSP-FIRME.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE 9010       TO CSP-CODE.
           MOVE LNK-MOIS   TO CSP-MOIS.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           IF CSP-UNITE NOT = 0
              PERFORM NOTER
           END-IF.

       NOTER.
           PERFORM TAUX
           MOVE CSP-UNITE TO HELP-POURCENT
           INITIALIZE L-RECORD
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           COMPUTE SH-00 = L-SAL-MOIS / L-UNI-REGUL-THEO.
           COMPUTE ARR = SH-00 * L-UNI-REGUL-LIM + ,005.
           IF ARR > L-COT-BRUT-MAL
              MOVE L-COT-BRUT-MAL TO ARR
           END-IF.
      *    MOVE L-COT-BRUT-MAL TO ARR

           PERFORM PP.
           ADD ARR TO HELP-VAL(LNK-MOIS) HELP-VAL(13) HELP-BASE.
           ADD L-COTIS-MAL-PAT TO HELP-PP.
           ADD L-COTIS-PEN-PAT TO HELP-PP.
           ADD L-COTIS-FAM-PAT TO HELP-PP.
           ADD L-COTIS-SAN-PAT TO HELP-PP.
           ADD L-COTIS-ACC-PAT TO HELP-PP.
           ADD L-COTIS-ESP-PAT TO HELP-PP.
           ADD L-COTIS-MUT-PAT TO HELP-PP.
           PERFORM READ-CONTRAT.

       PP.
           COMPUTE L-COTIS-MAL-PAT = ARR * SS-PP-M / 100 + ,005.
           COMPUTE L-COTIS-PEN-PAT = ARR * SS-PP-P / 100 + ,005.
           COMPUTE L-COTIS-FAM-PAT = ARR * SS-PP-F / 100 + ,005.
           COMPUTE L-COTIS-SAN-PAT = ARR * SS-PP-S / 100 + ,005.
           COMPUTE IDX = MUT-CLASSE + 90
           MOVE TXA-VALUE(IDX) TO SS-PP-U.
           COMPUTE L-COTIS-MUT-PAT = ARR * SS-PP-U / 100 + ,005.
           COMPUTE L-COTIS-ESP-PAT = ARR * SS-PP-E / 100 + ,005.
           COMPUTE L-COTIS-ACC-PAT = ARR * TXA-VALUE(FR-ACCIDENT)
           / 100 + ,005.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.

        TAUX.
           INITIALIZE SS-RECORD.
           MOVE CAR-REGIME TO SS-REGIME.
           CALL "6-TSS" USING LINK-V SS-RECORD.
           MOVE TXA-VALUE(FR-ACCIDENT) TO HELP-TAUX.
           COMPUTE IDX = MUT-CLASSE + 90
           ADD TXA-VALUE(IDX) TO HELP-TAUX.
           ADD SS-PP-M TO HELP-TAUX.
           ADD SS-PP-P TO HELP-TAUX.
           ADD SS-PP-F TO HELP-TAUX.
           ADD SS-PP-S TO HELP-TAUX.
           ADD SS-PP-E TO HELP-TAUX.
           ADD SS-PP-U TO HELP-TAUX.

       TRANSMET.
           INITIALIZE MD-RECORD.
           MOVE CON-MOTIF-DEPART TO MD-CODE.
           CALL "6-MOTDEP" USING LINK-V MD-RECORD FAKE-KEY.
           MOVE  0 TO LNK-VAL.
           CALL "MTJ" USING LINK-V 
                            HELP-CUMUL 
                            PR-RECORD
                            CON-RECORD
                            MD-RECORD
                            BQF-RECORD.
           ADD 1 TO COUNTER.

       AFFICHAGE-ECRAN.
           MOVE 1302 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

           DISPLAY "CS 9010" LINE 4 POSITION 70 LOW.
           DISPLAY "TRIMESTER" LINE 10 POSITION 5 LOW.
           DISPLAY TRIMESTER LINE 10 POSITION 17.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
           
       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "MTJ" USING LINK-V 
                               HELP-CUMUL 
                               PR-RECORD
                               CON-RECORD
                               MD-RECORD
                               BQF-RECORD.
           CANCEL "MTJ".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

