      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-CODPAI LECTURE CODES SALAIRES PAIE        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-CODPAI.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODPAIE.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CODPAIE.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  ESC-KEY     PIC 9(4) COMP-1.
       01  NOT-OPEN    PIC 9 VALUE 0.
       01  INPUT-ERROR PIC 9 VALUE 0.
       01  IDX-1       PIC 9 VALUE 0.
       01  IDX                   PIC 9(4)  VALUE 0.
       01  SH-00  PIC S9(10)V999999 COMP-3.
       01  action      PIC 9.
       01  TODAY.
           02 TODAY-TIME     PIC 9(12).
           02 TODAY-FILLER   PIC 9999.

       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".
           COPY "CODPAIE.LNK".
           COPY "REGISTRE.REC".
       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD REG-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CODPAIE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-CODPAI.

           IF NOT-OPEN = 1
           AND LNK-SUFFIX NOT = ANNEE-PAIE
              CLOSE CODPAIE
              MOVE 0 TO NOT-OPEN
           END-IF.

           IF NOT-OPEN = 0
              MOVE LNK-SUFFIX TO ANNEE-PAIE
              OPEN I-O CODPAIE
              MOVE 1 TO NOT-OPEN
           END-IF.

           MOVE LINK-RECORD TO CSP-RECORD.
           MOVE FR-KEY      TO CSP-FIRME CSP-FIRME-2.
           MOVE REG-PERSON  TO CSP-PERSON CSP-PERSON-2.
           IF CSP-MOIS = 0
              MOVE LNK-MOIS TO CSP-MOIS CSP-MOIS-2.
           PERFORM RECHERCHE.

       RECHERCHE.
           EVALUATE EXC-KEY 
           WHEN 1 START CODPAIE KEY > CSP-KEY-2 INVALID GO EXIT-1
               NOT INVALID
               READ CODPAIE NEXT NO LOCK AT END GO EXIT-1 END-READ
               END-START
           WHEN 65 START CODPAIE KEY < CSP-KEY INVALID GO EXIT-1
               NOT INVALID
               READ CODPAIE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               END-START
           WHEN 66 START CODPAIE KEY > CSP-KEY INVALID GO EXIT-1
               NOT INVALID
               READ CODPAIE NEXT NO LOCK AT END GO EXIT-1 END-READ
               END-START
           WHEN 0 START CODPAIE KEY <= CSP-KEY INVALID GO EXIT-1
               NOT INVALID
               READ CODPAIE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               END-START
           WHEN 98 PERFORM KEY-CSP
                   DELETE CODPAIE INVALID CONTINUE END-DELETE
                   IF LNK-SQL = "Y" 
                      CALL "9-CODPAI" USING LINK-V CSP-RECORD EXC-KEY 
                   END-IF
           WHEN 99 MOVE 0 TO SH-00
                   PERFORM TEST-0 VARYING IDX FROM 1 BY 1 UNTIL IDX > 7
                   IF SH-00 > 0
                      PERFORM KEY-CSP
                      CALL "0-TODAY" USING TODAY
                      MOVE TODAY-TIME TO CSP-TIME
                      MOVE LNK-USER TO CSP-USER
                      IF LNK-SQL = "Y" 
                         CALL "9-CODPAI" USING LINK-V CSP-RECORD EXC-KEY 
                      END-IF
                      WRITE CSP-RECORD INVALID REWRITE CSP-RECORD 
                      END-WRITE
                   ELSE
                      IF LNK-SQL = "Y" 
                         CALL "9-CODPAI" USING LINK-V CSP-RECORD EXC-KEY 
                      END-IF
                      DELETE CODPAIE INVALID CONTINUE END-DELETE
                   END-IF
           WHEN OTHER READ CODPAIE NO LOCK INVALID GO EXIT-2 END-READ
           END-EVALUATE.
           IF FR-KEY     NOT = CSP-FIRME 
           OR REG-PERSON NOT = CSP-PERSON
           OR LNK-MOIS   NOT = CSP-MOIS
              GO EXIT-1
           END-IF.
           MOVE CSP-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           INITIALIZE LINK-REC-DET.
           EXIT PROGRAM.


       KEY-CSP.
           MOVE CSP-FIRME  TO CSP-FIRME-2  CSP-FIRME-3.
           MOVE CSP-PERSON TO CSP-PERSON-2 CSP-PERSON-3.
           MOVE CSP-CODE   TO CSP-CODE-2   CSP-CODE-3.
           MOVE CSP-MOIS   TO CSP-MOIS-2   CSP-MOIS-3.
           MOVE CSP-SUITE  TO CSP-SUITE-2  CSP-SUITE-3.
           MOVE CSP-POSTE  TO CSP-POSTE-2  CSP-POSTE-3.
           MOVE CSP-EXTENSION TO CSP-EXTENSION-2 CSP-EXTENSION-3.

       TEST-0.
           ADD CSP-DONNEE(IDX) TO SH-00.

           COPY "XACTION.CPY".
           