      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-SIGREG VISUALISATION SIGNALETIQUE PERSONNE�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.   2-SIGREG.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  ECRAN-SUITE.
           02 ECR-S1        PIC 999 VALUE 240.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S         PIC 999 OCCURS 1.
       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1  PIC 99 VALUE 2.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX    PIC 99 OCCURS 1.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "BANQP.REC".
           COPY "MESSAGE.REC".
           COPY "CARRIERE.REC".
           COPY "METIER.REC".
           COPY "INDEX.REC".
           COPY "COUT.REC".
           COPY "EQUIPE.REC".
           COPY "BAREME.REC".
           COPY "BARDEF.REC".
           COPY "POINTS.REC".
           COPY "POCL.REC".
           COPY "REGIME.REC".
           COPY "STATUT.REC".
           COPY "DIVISION.REC".
           COPY "HORSEM.REC".
           COPY "FICHE.REC".
           COPY "CONTRAT.REC".
           COPY "PROF.REC".
           COPY "V-VAR.CPY".

       01  ACTION-CALL           PIC 9 VALUE 0.
       01  SAL-ACT               PIC 9(8)V9(4).

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z5 PIC Z(5) BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z6Z6 PIC Z(6),Z(6) BLANK WHEN ZERO.
           02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
           02 HE-Z1Z2 PIC Z,ZZ.
           02 HE-Z2Z2 PIC ZZ,ZZ.
           02 HE-Z2Z3 PIC ZZ,ZZZ.
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z3Z4 PIC Z(3),Z(4).
           02 HE-Z4Z4 PIC Z(4),Z(4).
           02 HE-Z6Z4 PIC Z(6),Z(4).
           02 HE-Z8Z2 PIC Z(8),ZZ.
           02 HE-Z8Z3 PIC Z(8),ZZZ.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
      *袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴�

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-SIGREG .

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 2     MOVE 0063000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1-1 
           WHEN  2 PERFORM AVANT-1-2.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-1 
           WHEN  2 PERFORM APRES-1-2.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 30 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-1-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 0 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  OTHER PERFORM NEXT-REGIS.
           PERFORM AFFICHAGE-DETAIL.
           IF LNK-COMPETENCE >= REG-COMPETENCE
              PERFORM DONNEES-PERSONNE.
           
       APRES-1-2.
           MOVE "A" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.
           IF REG-PERSON = 0 
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           ELSE
              PERFORM DONNEES-PERSONNE.
           
       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           
       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           MOVE 0 TO LNK-PERSON LNK-NUM.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.


       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

           
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-E1-01.
           MOVE REG-PERSON  TO HE-Z6.
           IF REG-PERSON NOT = 0
              MOVE REG-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6

           DISPLAY REG-INTL  LINE 4 POSITION 5.
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.


       DIS-E1-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 30.
       DIS-E1-END.

       AFFICHAGE-ECRAN.
           MOVE 2201 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-E1-01 THRU DIS-E1-END.

       END-PROGRAM.
           CANCEL "6-BANQP".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

       DONNEES-PERSONNE.
           INITIALIZE CAR-RECORD HJS-RECORD FICHE-RECORD
                      BQP-RECORD STAT-RECORD CON-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD NUL-KEY.
           CALL "6-GHJS" USING LINK-V HJS-RECORD  CAR-RECORD PRESENCES.
           CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD PV-KEY.
           INITIALIZE BQP-RECORD.
           MOVE 1 TO BQP-SUITE.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD NUL-KEY.

           DISPLAY PR-MAISON LINE 4 POSITION 18.
           DISPLAY PR-RUE LINE 4 POSITION 30.

           DISPLAY PR-PAYS LINE 5 POSITION 18.
           MOVE PR-CODE-POST TO HE-Z6.
           DISPLAY HE-Z6 LINE 5 POSITION 21.
           DISPLAY PR-LOCALITE  LINE 5 POSITION 30.
           DISPLAY PR-TELEPHONE LINE 5 POSITION 60.

           MOVE PR-NAISS-A TO HE-AA.
           MOVE PR-NAISS-M TO HE-MM.
           MOVE PR-NAISS-J TO HE-JJ.
           DISPLAY HE-DATE  LINE 6 POSITION 18.
           DISPLAY PR-SNOCS LINE 6 POSITION 42.

           MOVE REG-ANCIEN-A TO HE-AA.
           MOVE REG-ANCIEN-M TO HE-MM.
           MOVE REG-ANCIEN-J TO HE-JJ.
           DISPLAY HE-DATE  LINE 7 POSITION 18.

           MOVE CON-DEBUT-A TO HE-AA.
           MOVE CON-DEBUT-M TO HE-MM.
           MOVE CON-DEBUT-J TO HE-JJ.
           DISPLAY HE-DATE LINE 7 POSITION 42.

           MOVE CON-FIN-A TO HE-AA.
           MOVE CON-FIN-M TO HE-MM.
           MOVE CON-FIN-J TO HE-JJ.
           DISPLAY HE-DATE LINE 7 POSITION 62.

           MOVE CAR-STATUT TO STAT-CODE HE-Z8.
           DISPLAY HE-Z8    LINE 8 POSITION 20.
           CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY.
           DISPLAY STAT-NOM LINE 8 POSITION 30 SIZE 40.

           MOVE CAR-REGIME TO HE-Z4 REGIME-NUMBER.
           DISPLAY HE-Z4 LINE 9 POSITION 24.
           CALL "6-REGIME" USING LINK-V REGIME-RECORD FAKE-KEY.
           DISPLAY REGIME-NOM LINE 9 POSITION 30 SIZE 40.

           MOVE CAR-COUT TO HE-Z8 COUT-NUMBER.
           DISPLAY HE-Z8   LINE 10 POSITION 20.
           MOVE FR-KEY TO COUT-FIRME.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY
           DISPLAY COUT-NOM  LINE 10 POSITION 30 SIZE 40.

      *    DISPLAY CAR-METIER LINE 11 POSITION 5.
           MOVE CAR-METIER TO MET-CODE
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY
           DISPLAY MET-NOM(PR-CODE-SEXE) LINE 11 POSITION 5 SIZE 40.
           MOVE 0 TO COL-IDX.
           INSPECT MET-NOM(PR-CODE-SEXE) TALLYING COL-IDX FOR 
           CHARACTERS BEFORE "    ".
           ADD  8 TO COL-IDX.
           COMPUTE IDX = 80 - COL-IDX.
           DISPLAY CAR-POSITION LINE 11 POSITION COL-IDX SIZE IDX LOW.
           IF MET-PROF > 0
           OR CAR-PROF > 0
              PERFORM DIS-CITP-88
           ELSE
              DISPLAY SPACES LINE 12 POSITION 5 SIZE 75
           END-IF.

           MOVE CAR-EQUIPE TO HE-Z8 EQ-NUMBER.
           DISPLAY HE-Z8 LINE 13 POSITION 20.
           MOVE FR-KEY TO EQ-FIRME. 
           CALL "6-EQUIPE" USING LINK-V EQ-RECORD FAKE-KEY
           DISPLAY EQ-NOM LINE 13 POSITION 30 SIZE 40.

           MOVE CAR-DIVISION TO HE-Z8 DIV-NUMBER.
           DISPLAY HE-Z8 LINE 14 POSITION 20.
           MOVE FR-KEY TO DIV-FIRME.
           CALL "6-DIV" USING LINK-V DIV-RECORD FAKE-KEY
           DISPLAY DIV-NOM LINE 14 POSITION 30 SIZE 40.

           MOVE CAR-POSTE-FRAIS TO HE-Z8 PC-NUMBER.
           DISPLAY HE-Z8 LINE 15 POSITION 20.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
           DISPLAY PC-NOM LINE 15 POSITION 30 SIZE 40.

           DISPLAY BQP-BANQUE  LINE 22 POSITION 5
           DISPLAY BQP-PART(1) LINE 22 POSITION 15.
           DISPLAY BQP-PART(2) LINE 22 POSITION 20.
           DISPLAY BQP-PART(3) LINE 22 POSITION 25.
           DISPLAY BQP-PART(4) LINE 22 POSITION 30.
           DISPLAY BQP-PART(5) LINE 22 POSITION 35.
           DISPLAY BQP-PART(6) LINE 22 POSITION 40.

           DISPLAY BQP-LIBELLE LINE 23 POSITION 15.

           MOVE CAR-HRS-JOUR TO HE-Z2Z2.
           INSPECT HE-Z2Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z2Z2 LINE 18 POSITION 5.
           MOVE CAR-JRS-SEMAINE TO HE-Z1Z2.
           INSPECT HE-Z1Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z1Z2 LINE 18 POSITION 23.

           MOVE CAR-HRS-MOIS TO HE-Z3Z2.
           INSPECT HE-Z3Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z3Z2 LINE 18 POSITION 45.
           PERFORM DIS-H-DET VARYING IDX FROM 1 BY 1 UNTIL IDX > 8.
           PERFORM DIS-FICHE.
           PERFORM DIS-SAL.

       DIS-H-DET.
           COMPUTE COL-IDX = 9 * IDX - 2.
           MOVE HJS-HEURES(IDX) TO HE-Z2Z2.
           INSPECT HE-Z2Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z2Z2 LINE 20 POSITION COL-IDX.


       DIS-FICHE.
           DISPLAY FICHE-ETAT-CIVIL LINE 17 POSITION 5.
           DISPLAY FICHE-CLASSE-A LINE 17 POSITION 8.
           MOVE FICHE-CLASSE-B TO HE-Z2.
           DISPLAY HE-Z2  LINE 17 POSITION 10.
           DISPLAY FICHE-GROUPE LINE 17 POSITION 13.

           MOVE FICHE-TAUX TO HE-Z2Z2.
           INSPECT HE-Z2Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z2Z2 LINE 17 POSITION 18.
           IF FICHE-IMPOSABLE-YN = "N"
              DISPLAY "0" LINE 17 POSITION 19.
           PERFORM DIS-ABAT VARYING IDX-1 FROM 1 BY 1 UNTIL 
           IDX-1 > 5.

       DIS-ABAT.
           COMPUTE COL-IDX = 18 + IDX-1 * 10.
           MOVE FICHE-ABAT(IDX-1) TO HE-Z3Z2.
           INSPECT HE-Z3Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z3Z2 LINE 17 POSITION COL-IDX.

       DIS-SAL.
           DISPLAY SPACES LINE 16 POSITION 1 SIZE 80.
           IF CAR-BAREME NOT = SPACES
           AND CAR-ECHELON > 0
              PERFORM DIS-E1-BAR
           END-IF.
           IF CAR-SAL-ACT > 0
              MOVE CAR-SAL-ACT TO HE-Z6Z4
              INSPECT HE-Z6Z4 REPLACING ALL ",0000" BY "     "
              DISPLAY HE-Z6Z4 LINE 16 POSITION 5 REVERSE
           END-IF.

           IF CAR-POURCENT = 0
              MOVE 100 TO CAR-POURCENT 
           END-IF.
           IF CAR-INDEXE NOT = "N"
              IF CAR-SAL-100 = 0
              AND CAR-ECHELON = 0
                 COMPUTE SAL-ACT = MOIS-SALMIN(LNK-MOIS) 
                                 * MOIS-IDX(LNK-MOIS) + ,00009
                 IF CAR-HOR-MEN = 0
                    COMPUTE SAL-ACT = SAL-ACT / 173
                 END-IF
              ELSE
                 COMPUTE SAL-ACT = CAR-SAL-100 * MOIS-IDX(LNK-MOIS)
                    + ,00009
              END-IF
           END-IF.
          
           COMPUTE SAL-ACT = SAL-ACT / 100 * CAR-POURCENT.
           IF SAL-ACT > 0
              MOVE SAL-ACT TO HE-Z6Z4.
           INSPECT HE-Z6Z4 REPLACING ALL ",0000" BY "     ".
           DISPLAY HE-Z6Z4 LINE 16 POSITION 5 REVERSE.
           MOVE 0 TO LNK-NUM.
           MOVE CAR-POURCENT TO HE-Z3Z2.
           INSPECT HE-Z3Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z3Z2 LINE 16 POSITION 73.
           DISPLAY "%" LINE 16 POSITION 79.

       DIS-E1-BAR.
           INITIALIZE BAR-RECORD
           MOVE CAR-BAREME TO BDF-CODE BAR-BAREME
           MOVE CAR-GRADE  TO BAR-GRADE
           MOVE LNK-ANNEE  TO BAR-ANNEE
           MOVE LNK-MOIS   TO BAR-MOIS
           CALL "6-BARDEF" USING LINK-V BDF-RECORD NUL-KEY
           CALL "6-BAREME" USING LINK-V BAR-RECORD NUL-KEY
           IF BDF-POINTS > 0
              INITIALIZE PTS-RECORD
              MOVE CAR-ANNEE TO PTS-ANNEE
              MOVE CAR-MOIS  TO PTS-MOIS
              CALL "6-POINT" USING LINK-V PTS-RECORD
              COMPUTE SAL-ACT = BAR-POINTS(CAR-ECHELON) 
                                * PTS-I100(BDF-POINTS)
                                * MOIS-IDX(LNK-MOIS) + ,00009
           ELSE
              COMPUTE SAL-ACT = (BAR-ECHELON-I100(CAR-ECHELON)
                              + BAR-ECHELON-I100(80))
                              * MOIS-IDX(LNK-MOIS) + ,00009
           END-IF.
           COMPUTE SAL-ACT = SAL-ACT / 100 * CAR-POURCENT.
           MOVE SAL-ACT TO HE-Z6Z4.
           INSPECT HE-Z6Z4 REPLACING ALL ",0000" BY "     ".
           DISPLAY HE-Z6Z4 LINE 16 POSITION 5 REVERSE.
           DISPLAY CAR-BAREME LINE 16 POSITION 18. 
           MOVE CAR-GRADE   TO HE-Z4.
           DISPLAY HE-Z4 LINE 16 POSITION 28.
           MOVE CAR-ECHELON TO HE-Z2.
           DISPLAY HE-Z2 LINE 16 POSITION 33.
           DISPLAY BAR-DESCRIPTION  LINE 16 POSITION 36 SIZE 20. 
           DISPLAY BDF-NOM LINE 16 POSITION 56 SIZE 16. 

       DIS-CITP-88.
           INITIALIZE PROF-RECORD.
           MOVE MET-PROF TO HE-Z5 PROF-CODE.
           IF CAR-PROF > 0
              MOVE CAR-PROF TO PROF-CODE HE-Z5
           END-IF.
           DISPLAY HE-Z5 LINE 12 POSITION 5 REVERSE.
           CALL "6-PROF" USING LINK-V PROF-RECORD FAKE-KEY.
           DISPLAY PROF-NOM(1) LINE 12 POSITION 12 SIZE 69.
