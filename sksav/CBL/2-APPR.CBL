      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-APPR INTERROGATION INDEMNITES D'APPRENTISSAGE   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-APPR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 2.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "LIVRE.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "PAYS.REC".
           COPY "TXACCID.REC".
           COPY "TAUXSS.REC".
           COPY "BANQP.REC".
           COPY "BANQF.REC".
           COPY "CARRIERE.REC".
           COPY "METIER.REC".

       01  DOA-RECORD.
               10  DOA-APP-NOM         PIC X(0040).
               10  DOA-ENTR-NOM        PIC X(0040).
               10  DOA-APP-ADRESS      PIC X(0040).
               10  DOA-ENTR-ADRESS     PIC X(0040).
               10  DOA-APP-LOCALITE    PIC X(0040).
               10  DOA-ENTR-LOCALITE   PIC X(0040).
               10  DOA-APP-NATION      PIC X(0040).
               10  DOA-APP-MATRICULE   PIC X(0040).
               10  DOA-ENTR-MATRICULE  PIC X(0040).
               10  DOA-ENTR-TEL        PIC X(0020).
               10  DOA-CODE-ENTR       PIC X(0040).
               10  DOA-APP-CCP         PIC X(0040).
               10  DOA-ENTR-CCP        PIC X(0040).
               10  DOA-APP-TITULAIRE   PIC X(0040).
               10  DOA-ENTR-TITULAIRE  PIC X(0040).
               10  FILLER OCCURS 3.
                   15  DOA-APP-PROF PIC X(0030).
               10  DOA-LV.
                   15  DOA-ANNEE-SCOL  PIC X(0010).
               10  FILLER OCCURS 12.
                   15  DOA-COL1        PIC X(0012).
               10  FILLER OCCURS 12.
                   15  DOA-COL2        PIC X(0012).
               10  FILLER OCCURS 12.
                   15  DOA-COL3        PIC X(0012).
               10  FILLER OCCURS 12.
                   15  DOA-COL4        PIC X(0012).
               10  FILLER OCCURS 12.
                   15  DOA-COL5        PIC X(0012).
               10  FILLER OCCURS 12.
                   15  DOA-COL6        PIC X(0012).
               10  FILLER OCCURS 12.
                   15  DOA-COL7        PIC X(0012).
               10  DOA-TOT5            PIC X(0012).
               10  DOA-DATE            PIC X(0012).
       01  SH-02                 PIC S9(10)V999999 COMP-3.

       01  ECR-DISPLAY.
           02 HE-Z3Z2 PIC Z(3),ZZ.
           02 HE-Z4Z2 PIC Z(4),ZZ.
           02 HE-Z6Z2 PIC Z(6),ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z4 PIC 9(4).
           02 HE-ZX REDEFINES HE-Z4.
               03 HE-M PIC 9.
               03 HE-F PIC 9.
               03 HE-A PIC 99.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-APPR.
       
           CALL "0-TODAY" USING TODAY.
           PERFORM AFFICHAGE-ECRAN.
           MOVE LNK-MOIS TO SAVE-MOIS.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0027000000 TO EXC-KFR(6).
           MOVE 0052530000 TO EXC-KFR(11).
           MOVE 0000000065 TO EXC-KFR(13).
           MOVE 6600000000 TO EXC-KFR(14).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640425 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 .

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 .

           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT REG-PERSON 
             LINE  3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT REG-MATCHCODE
             LINE  3 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           MOVE FR-KEY TO REG-FIRME.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   ELSE
                      MOVE "N" TO LNK-A-N
                   END-IF
                   MOVE 0 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM PRESENCE
           WHEN 5  PERFORM TRANSMET
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.
           PERFORM DIS-HE-01 THRU DIS-HE-02.
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 0 TO LNK-NUM
              MOVE 1 TO INPUT-ERROR
           ELSE
              PERFORM AFFICHAGE-DETAIL
           END-IF.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   PERFORM DIS-HE-01 THRU DIS-HE-02
                   PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.                     
           
       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.

       ENTREE-SORTIE.
           MOVE "AA" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.

       AFFICHAGE-ECRAN.
           MOVE 2245 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           INITIALIZE DOA-RECORD IDX-3 SH-01.
           MOVE 9 TO LIN-IDX.
           SUBTRACT 1 FROM LNK-ANNEE.
           MOVE LNK-ANNEE TO DOA-ANNEE-SCOL.
           INITIALIZE TXA-RECORD.
           CALL "6-ASSACC" USING LINK-V TXA-RECORD.
           PERFORM GET-LIVRE VARYING IDX-1 FROM 9 BY 1 UNTIL IDX-1 > 12.
           ADD 1 TO LNK-ANNEE.
           MOVE 6 TO IDX.
           STRING LNK-ANNEE DELIMITED BY SIZE INTO DOA-ANNEE-SCOL
           WITH POINTER IDX.
           INITIALIZE TXA-RECORD.
           CALL "6-ASSACC" USING LINK-V TXA-RECORD.
           PERFORM GET-LIVRE VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 8.

       GET-LIVRE.
           INITIALIZE L-RECORD SH-02.
           MOVE IDX-1 TO L-MOIS LNK-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           PERFORM DISPLAY-MOIS.

           
       DISPLAY-MOIS.
           ADD 1 TO LIN-IDX IDX-3.
           MOVE L-MOIS TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO DOA-COL1(IDX-3).
           DISPLAY SPACES LINE LIN-IDX POSITION 22 SIZE 55.
           PERFORM HRS-PAYES.
           MOVE SH-02 TO HE-Z4Z2.
           ADD  SH-02 TO SH-01.
           DISPLAY HE-Z4Z2 LINE LIN-IDX POSITION 65.
           MOVE SH-01 TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 23 POSITION 63.

5          MOVE HE-Z4Z2 TO DOA-COL5(IDX-3).
           MOVE SH-00 TO HE-Z3Z2.
           DISPLAY HE-Z3Z2 LINE LIN-IDX POSITION 22.
2          MOVE HE-Z3Z2 TO DOA-COL2(IDX-3).
           COMPUTE SH-00 = SH-02 / SH-00.
           MOVE SH-00 TO HE-Z3Z2.
           DISPLAY HE-Z3Z2 LINE LIN-IDX POSITION 53.
4          MOVE HE-Z3Z2 TO DOA-COL4(IDX-3).
           PERFORM HRS-MALADIE.
           MOVE SH-00 TO HE-Z3Z2.
           DISPLAY HE-Z3Z2 LINE LIN-IDX POSITION 40.
3          MOVE HE-Z3Z2 TO DOA-COL3(IDX-3).
           IF SH-02 > 0
              PERFORM HISTORIQUE
              MOVE SH-00 TO HE-Z3Z2
              DISPLAY HE-Z3Z2 LINE LIN-IDX POSITION 74
              MOVE HE-Z3Z2 TO DOA-COL6(IDX-3).

       HRS-PAYES.
           MOVE 0 TO SH-00 SH-02.
           PERFORM HR VARYING IDX-2 FROM 11 BY 1 UNTIL IDX-2 > 29.
           PERFORM HR VARYING IDX-2 FROM 31 BY 1 UNTIL IDX-2 > 49.
           ADD L-UNITE(100) TO SH-00.
           ADD L-DC(100) TO SH-02.

       HR.
           ADD L-UNITE(IDX-2) TO SH-00.
           ADD L-DC(IDX-2) TO SH-02.

       HRS-MALADIE.
           MOVE 0 TO SH-00.
           PERFORM HR VARYING IDX-2 FROM 1 BY 1 UNTIL IDX-2 > 9.


        HISTORIQUE.
           INITIALIZE SS-RECORD.
           MOVE TXA-VALUE(FR-ACCIDENT) TO SH-00.
           MOVE L-REGIME TO SS-REGIME.
           CALL "6-TSS" USING LINK-V SS-RECORD.
           PERFORM SS VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.

       SS. 
           ADD SS-PP(IDX) TO SH-00.

       TRANSMET.

           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO DOA-APP-NOM.
           MOVE 1 TO IDX.
           STRING PR-NAISS-A DELIMITED BY SIZE INTO DOA-APP-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-NAISS-M DELIMITED BY SIZE INTO DOA-APP-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-NAISS-J DELIMITED BY SIZE INTO DOA-APP-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-SNOCS DELIMITED BY SIZE INTO DOA-APP-MATRICULE
           WITH POINTER IDX.

           CALL "4-PRRUE" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO DOA-APP-ADRESS.

           CALL "4-PRLOC" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO DOA-APP-LOCALITE.
               
           MOVE PR-NATIONALITE TO PAYS-CODE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.
           MOVE PAYS-NATION TO DOA-APP-NATION.

           INITIALIZE CAR-RECORD BQF-RECORD BQP-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-BANQF" USING LINK-V BQF-RECORD NX-KEY.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD FAKE-KEY.
           MOVE 1 TO IDX.
           STRING BQP-BANQUE DELIMITED BY " "  INTO DOA-APP-CCP
           WITH POINTER IDX.
           ADD 2 TO IDX.
           STRING BQP-COMPTE DELIMITED BY " "  INTO DOA-APP-CCP
           WITH POINTER IDX.
           MOVE 1 TO IDX.
           STRING BQF-CODE DELIMITED BY " "  INTO DOA-ENTR-CCP
           WITH POINTER IDX.
           ADD 2 TO IDX.
           STRING BQF-COMPTE DELIMITED BY " "  INTO DOA-ENTR-CCP
           WITH POINTER IDX.

           IF BQP-BENEFIC-NOM > SPACES
              MOVE BQP-BENEFIC-NOM TO DOA-APP-TITULAIRE
           ELSE
              MOVE DOA-APP-NOM TO DOA-APP-TITULAIRE.

           IF BQF-TITULAIRE > SPACES
              MOVE BQF-TITULAIRE TO DOA-ENTR-TITULAIRE
           ELSE
              MOVE DOA-ENTR-NOM TO DOA-ENTR-TITULAIRE.

           MOVE CAR-METIER TO MET-CODE.
           MOVE "F"        TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO DOA-APP-PROF(1).
           MOVE CAR-POSITION          TO DOA-APP-PROF(3).

           MOVE FR-NOM   TO DOA-ENTR-NOM.
           MOVE FR-PHONE TO DOA-ENTR-TEL.
           MOVE TODAY-ANNEE TO HE-AA.
           MOVE TODAY-MOIS  TO HE-MM.
           MOVE TODAY-JOUR  TO HE-JJ.

           MOVE HE-DATE TO DOA-DATE.
           MOVE SH-01 TO HE-Z6Z2.
           MOVE HE-Z6Z2 TO DOA-TOT5.

           MOVE 1 TO IDX.
           STRING FR-ETAB-A DELIMITED BY SIZE INTO DOA-ENTR-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-ETAB-N DELIMITED BY SIZE INTO DOA-ENTR-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-SNOCS DELIMITED BY SIZE INTO DOA-ENTR-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-EXTENS DELIMITED BY SIZE INTO DOA-ENTR-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.

           MOVE 1 TO IDX.
           STRING FR-PAYS DELIMITED BY " "  INTO DOA-ENTR-LOCALITE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           MOVE FR-CODE-POST TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE INTO DOA-ENTR-LOCALITE
           WITH POINTER IDX.
           ADD 2 TO IDX.
           STRING FR-LOCALITE DELIMITED BY " "  INTO DOA-ENTR-LOCALITE
           WITH POINTER IDX.
           MOVE 1 TO IDX.
           STRING FR-RUE DELIMITED BY "   "  INTO DOA-ENTR-ADRESS
              WITH POINTER IDX.
           ADD 1 TO IDX.
           IF IDX < 39
              STRING FR-MAISON DELIMITED BY "  " INTO DOA-ENTR-ADRESS
              WITH POINTER IDX.
            

           MOVE 0 TO LNK-VAL.
           CALL "DOAP" USING LINK-V DOA-RECORD.
           MOVE 99 TO LNK-VAL.
           CALL "DOAP" USING LINK-V DOA-RECORD.
           MOVE 0 TO LNK-VAL.
           CANCEL "DOAP".

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

