      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-CDPOST  SIGNALETIQUE CODES POSTAUX        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-CDPOST.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "C-P.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "C-P.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01  ECRAN-SUITE.
           02 ECR-S1             PIC 999 VALUE 50.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S              PIC 999 OCCURS 1.
       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE 5.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "MESSAGE.REC".

       77  WHELP-1    PIC S9(6).

       01   ECR-DISPLAY.
            02 HE-Z2 PIC Z(2). 
            02 HE-Z3 PIC Z(3). 
            02 HE-Z4 PIC Z(4). 
            02 HE-Z6 PIC Z(6). 
            02 HE-Z8 PIC Z(8).
            02 HE-10 PIC Z(10). 
            02 HE-DATE .
               03 HE-JJ PIC ZZ.
               03 FILLER PIC X VALUE ".".
               03 HE-MM PIC ZZ.
               03 FILLER PIC X VALUE ".".
               03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                CDPOST.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-CDPOST.

           OPEN I-O   CDPOST.

           PERFORM AFFICHAGE-ECRAN .
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           IF ECRAN-IDX = 1
      *       MOVE 0000680000 TO EXC-KFR (14) 
           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640421 TO EXC-KFR (1)
                      MOVE 0006130000 TO EXC-KFR (3)
                      MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
                      MOVE 0052530000 TO EXC-KFR (11)
           WHEN CHOIX-MAX(1)
                      MOVE 0000000005 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11)
           END-IF.

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.
 
           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1-1 
           WHEN  2 PERFORM AVANT-1-2 
           WHEN  3 PERFORM AVANT-1-3 
           WHEN  4 PERFORM AVANT-1-4 
           WHEN CHOIX-MAX(1)
                PERFORM AVANT-DEC.

           IF EXC-KEY = 1 
              MOVE 01101000 TO LNK-VAL
              PERFORM HELP-SCREEN
              MOVE 98 TO EXC-KEY
           END-IF.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF  EXC-KEY > 66 
           AND EXC-KEY < 69 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
              GO TRAITEMENT-ECRAN
           END-IF.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-1 
           WHEN  2 PERFORM APRES-1-2 
           WHEN  3 PERFORM APRES-1-3 
           WHEN  4 PERFORM APRES-1-4 
           WHEN CHOIX-MAX(1) PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.


           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.
       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.
           ACCEPT CP-CODE 
             LINE  4 POSITION 22 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           ACCEPT CP-RUE 
             LINE  5 POSITION 22 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT CP-RUE CONVERTING
           "릠뒋뎲땶뱮걭뾼솞�" TO "EEEEEAIIOOUUUAOUA".


       AVANT-1-3.
           ACCEPT CP-LOCALITE
             LINE  6 POSITION 22 SIZE  40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT CP-LOCALITE CONVERTING
           "릠뒋뎲땶뱮걭뾼솞�" TO "EEEEEAIIOOUUUAOUA".
     
       AVANT-1-4.
           IF CP-MATCHCODE-2 = SPACES 
              MOVE CP-RUE TO CP-MATCHCODE-2.
           ACCEPT CP-MATCHCODE-2
             LINE  7 POSITION 22 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT CP-MATCHCODE-2 CONVERTING
           "릠뒋뎲땶뱮걭뾼솞�" TO "EEEEEAIIOOUUUAOUA".



      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           MOVE "N" TO A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3 
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   ELSE
                      MOVE "N" TO LNK-A-N
                   END-IF
                   CALL "2-CDPOST" USING LINK-V CP-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN 13 INITIALIZE CP-MATCHCODE-2
                   PERFORM NEXT-CDPOST
           WHEN OTHER PERFORM NEXT-CDPOST
           END-EVALUATE.
           IF CP-CODE  = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-2.
           IF CP-RUE = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-1-3.
           IF CP-LOCALITE = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-4.
           IF CP-MATCHCODE-2 = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.
           

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO CP-TIME
                    MOVE LNK-USER TO CP-USER
                    CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO CP-TIME
                    MOVE LNK-USER TO CP-USER
                    MOVE CP-LOCALITE TO CP-MATCHCODE-3
                    WRITE CP-RECORD INVALID 
                        REWRITE CP-RECORD
                    END-WRITE   
                    MOVE 0 TO LNK-STATUS
                    MOVE 1 TO DECISION ECRAN-IDX
                    PERFORM AFFICHAGE-ECRAN
            WHEN 8 DELETE CDPOST INVALID CONTINUE END-DELETE
                   INITIALIZE CP-RECORD
               PERFORM AFFICHAGE-DETAIL
               MOVE 1 TO DECISION
            WHEN 52 COMPUTE DECISION = CHOIX-MAX(ECRAN-IDX) + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       NEXT-CDPOST.
           CALL "6-CDPOST" USING LINK-V CP-RECORD A-N EXC-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.


       DIS-E1-01.
           MOVE CP-CODE TO HE-Z4.
           DISPLAY HE-Z4 LINE 4 POSITION 22.
       DIS-E1-02.
           DISPLAY CP-RUE LINE 5 POSITION 22.
       DIS-E1-03.
           DISPLAY CP-LOCALITE LINE 6 POSITION 22.
       DIS-E1-04.
           DISPLAY CP-MATCHCODE-2 LINE 7 POSITION 22.
       DIS-E1-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE ECR-S(ECRAN-IDX) TO LNK-VAL.
           MOVE "    " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           MOVE INDICE-ZONE TO DECISION.
           EVALUATE ECRAN-IDX
               WHEN 1 PERFORM ECRAN-1 VARYING INDICE-ZONE FROM
               1 BY 1 UNTIL INDICE-ZONE = CHOIX-MAX(ECRAN-IDX)
           END-EVALUATE.
           MOVE DECISION TO INDICE-ZONE.

       ECRAN-1.
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM DIS-E1-01 
               WHEN  2 PERFORM DIS-E1-02 
               WHEN  3 PERFORM DIS-E1-03 
               WHEN  4 PERFORM DIS-E1-04 
           END-EVALUATE.


       END-PROGRAM.
           CLOSE CDPOST.
           CANCEL "2-CDPOST".
           MOVE CP-CODE TO LNK-VAL.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

                
