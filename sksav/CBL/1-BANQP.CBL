      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-BANQP VENTILATION PAYEMENTS PERSONNES     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.   1-BANQP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "BANQP.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "BANQP.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  ECRAN-SUITE.
           02 ECR-S1        PIC 999 VALUE 240.
           02 ECR-S2        PIC 999 VALUE 241.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S         PIC 999 OCCURS 2.
       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1  PIC 99 VALUE 17.
           02  CHOIX-MAX-2  PIC 99 VALUE  6.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX    PIC 99 OCCURS 2.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "BANQUE.REC".
           COPY "BANQF.REC".
           COPY "SYNDICAT.REC".
           COPY "INDEX.REC".
           COPY "MESSAGE.REC".
           COPY "V-VAR.CPY".

       01  ACTION-CALL           PIC 9 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z6Z6 PIC Z(6),Z(6) BLANK WHEN ZERO.
           02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
           02 HE-Z12 PIC X(12).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
      *袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴�
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON BANQP.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-BANQP .

           OPEN I-O BANQP.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0163640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 2     MOVE 0129000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 3     MOVE 0129172400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 4     MOVE 0110002421 TO EXC-KFR(1)
           WHEN 5     MOVE 0129000015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 12    MOVE 0000000073 TO EXC-KFR (2)
           WHEN CHOIX-MAX(1)
                      IF BQP-TYPE > 0
                      OR BQP-SUITE > 0
                         MOVE 0000680000 TO EXC-KFR (14) 
                      END-IF
                      MOVE 0100000005 TO EXC-KFR (1)
                      MOVE 0700080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           IF ECRAN-IDX = 2
           MOVE 0067000000 TO EXC-KFR (14) 
           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0129000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6667000000 TO EXC-KFR(14)
           WHEN CHOIX-MAX(2)
                      MOVE 0100000005 TO EXC-KFR (1)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1-1 
           WHEN  2 PERFORM AVANT-1-2 
           WHEN  3 PERFORM AVANT-1-3 
           WHEN  4 PERFORM AVANT-1-4 
           WHEN  5 PERFORM AVANT-1-5 
           WHEN  6 PERFORM AVANT-1-6 
           WHEN  7 PERFORM AVANT-1-7 
           WHEN  8 PERFORM AVANT-1-8 
           WHEN  9 PERFORM AVANT-1-9 
           WHEN 10 PERFORM AVANT-1-10
           WHEN 11 PERFORM AVANT-1-11
           WHEN 12 PERFORM AVANT-1-12
           WHEN 13 PERFORM AVANT-1-13
           WHEN 14 PERFORM AVANT-1-14
           WHEN 15 PERFORM AVANT-1-15
           WHEN 16 PERFORM AVANT-1-16
           WHEN CHOIX-MAX(1) PERFORM AVANT-DEC.


           IF ECRAN-IDX = 2
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-2-1 
           WHEN  2 PERFORM AVANT-2-2 
           WHEN  3 PERFORM AVANT-2-3 
           WHEN  4 PERFORM AVANT-2-4 
           WHEN  5 PERFORM AVANT-2-5 
           WHEN CHOIX-MAX(2) PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 1 
              MOVE 01204000 TO LNK-VAL
              CALL "0-BOOK" USING LINK-V
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              GO TRAITEMENT-ECRAN
           END-IF.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF EXC-KEY > 66 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              IF ECRAN-IDX = 1
                 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
              ELSE
                 MOVE 0 TO INDICE-ZONE
              END-IF
              GO TRAITEMENT-ECRAN
           END-IF.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-1 
           WHEN  2 PERFORM APRES-1-2 
           WHEN  3 PERFORM APRES-1-3 
           WHEN  4 PERFORM APRES-1-4 
           WHEN  5 PERFORM APRES-1-5 
           WHEN  6 PERFORM APRES-1-6 
           WHEN  9 PERFORM APRES-1-9 
           WHEN 10 PERFORM APRES-1-10
           WHEN 11 PERFORM APRES-1-11
           WHEN 12 PERFORM APRES-1-12
           WHEN 13 PERFORM APRES-1-13
           WHEN 14 PERFORM APRES-1-14
           WHEN 15 PERFORM APRES-1-15
           WHEN 16 PERFORM APRES-1-16
           WHEN CHOIX-MAX(1) PERFORM APRES-DEC.

           IF ECRAN-IDX = 2
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-2-1
           WHEN  2 PERFORM APRES-2-2
           WHEN  3 PERFORM APRES-2-3
           WHEN CHOIX-MAX(2) PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.
           INITIALIZE BQP-RECORD.
           MOVE FR-KEY TO REG-FIRME BQP-FIRME.
           IF LNK-PERSON NOT = 0
              MOVE LNK-PERSON TO REG-PERSON BQP-PERSON
              MOVE  1 TO ACTION-CALL BQP-SUITE
              MOVE 13 TO EXC-KEY
              PERFORM NEXT-REGIS
              PERFORM DIS-E1-01
              MOVE 4 TO INDICE-ZONE
              MOVE 0 TO LNK-PERSON 
           ELSE 
              MOVE 0 TO ACTION-CALL
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.
           ACCEPT BQP-TYPE
             LINE  4 POSITION 23 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-4.
           IF BQP-TYPE  = 0
              MOVE 1 TO BQP-SUITE
           ELSE
              IF BQP-SUITE = 0
                 MOVE 1 TO BQP-SUITE
              END-IF
              ACCEPT BQP-SUITE
              LINE  5 POSITION 23 SIZE 2 
              TAB UPDATE NO BEEP CURSOR 1
              ON EXCEPTION EXC-KEY CONTINUE
           END-IF.

       AVANT-1-5.
           IF BQP-TYPE = 9
              PERFORM AVANT-1-SYN
           ELSE
           ACCEPT BQP-BANQUE
             LINE  6 POSITION 23 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-6.
           IF BQP-TYPE < 9
           ACCEPT BQP-COMPTE
             LINE  7 POSITION 23 SIZE 35
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE
             INITIALIZE BQP-COMPTE
           END-IF.

       AVANT-1-7.
      *    IF BQP-TYPE < 9
           ACCEPT BQP-LIBELLE
             LINE  9 POSITION 20 SIZE 50
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-8.
           IF BQP-TYPE NOT = 9
           ACCEPT BQP-BENEFIC-NOM
             LINE 11 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE
           ELSE
             INITIALIZE BQP-BENEFIC-NOM BQP-BENEFIC-LOC BQP-BENEFIC-RUE
           END-IF.

       AVANT-1-SYN.
           ACCEPT BQP-SYNDICAT
             LINE 6 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-9.
           IF BQP-TYPE = 9
              CONTINUE
           ELSE
              IF BQP-BENEFIC-NOM > SPACES
              ACCEPT BQP-BENEFIC-RUE
              LINE 12 POSITION 30 SIZE 40
              TAB UPDATE NO BEEP CURSOR  1
              ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-10.
           IF BQP-TYPE = 9
              CONTINUE
           ELSE
              IF BQP-BENEFIC-NOM > SPACES
           ACCEPT BQP-BENEFIC-LOC LINE 13 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-11.
           MOVE BQP-TOTAL TO HE-Z6Z2.
           IF BQP-TYPE > 1
           AND BQP-TYPE < 7
           ACCEPT HE-Z6Z2
             LINE 15 POSITION 32 SIZE  9
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z6Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z6Z2 TO BQP-TOTAL.
           IF EXC-KEY = 15
              COMPUTE BQP-TOTAL = BQP-TOTAL / 40,3399 
              MOVE BQP-TOTAL TO HE-Z6Z2  
              MOVE 13 TO EXC-KEY
           END-IF.
           DISPLAY HE-Z6Z2 LINE 15 POSITION 32.

       AVANT-1-12.
           MOVE BQP-PROPOSITION TO HE-Z6Z2.
           IF BQP-TYPE > 0
           OR BQP-SUITE > 1
           ACCEPT HE-Z6Z2
             LINE 16 POSITION 32 SIZE  9
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z6Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z6Z2 TO BQP-PROPOSITION.
           IF EXC-KEY = 15
              COMPUTE BQP-PROPOSITION = BQP-PROPOSITION / 40,3399 
              MOVE BQP-TOTAL TO HE-Z6Z2  
              MOVE 13 TO EXC-KEY
           END-IF.
           DISPLAY HE-Z6Z2 LINE 16 POSITION 32.

       AVANT-1-13.
           IF BQP-TYPE > 0
           OR BQP-SUITE > 1
              IF BQP-DEBUT-A = 0
                 MOVE LNK-ANNEE TO BQP-DEBUT-A
              END-IF
           ACCEPT BQP-DEBUT-A
             LINE 19 POSITION 14 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-14.
           IF BQP-TYPE > 0
           OR BQP-SUITE > 1
              IF BQP-DEBUT-M = 0
                 MOVE LNK-MOIS TO BQP-DEBUT-M
              END-IF
           ACCEPT BQP-DEBUT-M 
             LINE 20 POSITION 16 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-15.
           IF BQP-TYPE > 0
           OR BQP-SUITE > 1
           ACCEPT BQP-FIN-A
             LINE 19 POSITION 46 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-16.
           IF BQP-TYPE > 0
           OR BQP-SUITE > 1
           ACCEPT BQP-FIN-M
             LINE 20 POSITION 48 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-2-1.
           ACCEPT BQP-BANQUE-PREF 
             LINE 15 POSITION 32 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-2-2.
           ACCEPT BQP-COMPTA 
             LINE 16 POSITION 32 SIZE 35
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-2-3.
           MOVE BQP-PERIODE TO HE-Z12.

           IF BQP-TYPE > 0
           OR BQP-SUITE > 1
           ACCEPT HE-Z12
             LINE  18 POSITION 32 SIZE 12
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "NUMERIC"
             ON EXCEPTION EXC-KEY CONTINUE.
           MOVE HE-Z12 TO BQP-PERIODE.

       AVANT-2-4.            
           MOVE 20320000 TO LNK-POSITION.
           CALL "0-GDATE" USING BQP-DATE-NOTIF
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-ANNEE TO BQP-NOTIF-A
              MOVE TODAY-MOIS  TO BQP-NOTIF-M
              MOVE TODAY-JOUR  TO BQP-NOTIF-J
              MOVE 0 TO LNK-NUM
              MOVE 13 TO EXC-KEY
              PERFORM DIS-E2-04.

       AVANT-2-5.            
           MOVE 21320000 TO LNK-POSITION.
           CALL "0-GDATE" USING BQP-DATE-LEVEE
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-ANNEE TO BQP-LEVEE-A
              MOVE TODAY-MOIS  TO BQP-LEVEE-M
              MOVE TODAY-JOUR  TO BQP-LEVEE-J
              MOVE 0 TO LNK-NUM
              MOVE 13 TO EXC-KEY
              PERFORM DIS-E2-04.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-1-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  OTHER PERFORM NEXT-REGIS.
           IF INPUT-ERROR = 0
           AND REG-PERSON > 0 
              INITIALIZE BQP-RECORD SAVE-KEY
              PERFORM NEXT-BANQP 
              MOVE BQP-TYPE TO MS-NUMBER.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-1-2.
           MOVE "A" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-E1-01.
           IF REG-PERSON = 0 
              MOVE 1 TO INPUT-ERROR
           END-IF.

           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR = 0
           AND EXC-KEY > 64
              INITIALIZE BQP-RECORD SAVE-KEY
              PERFORM NEXT-BANQP 
              MOVE BQP-TYPE TO MS-NUMBER.
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-3.
           MOVE BQP-TYPE TO MS-NUMBER.
           MOVE "VV" TO LNK-AREA.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-MESSAGES
           WHEN   2 CALL "2-MESS" USING LINK-V MS-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
            WHEN  3 CALL "2-BANQP" USING LINK-V BQP-RECORD
                    PR-RECORD REG-RECORD 
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    PERFORM DISPLAY-F-KEYS
                    MOVE 1 TO INPUT-ERROR
            WHEN  4 MOVE 66 TO SAVE-KEY
                    PERFORM NEXT-BANQP 
                    MOVE BQP-TYPE TO MS-NUMBER
           END-EVALUATE.
           MOVE MS-NUMBER TO BQP-TYPE.
           PERFORM READ-BANQP.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-1-4.
           IF BQP-TYPE = 1
              IF BQP-SUITE > 3
                 MOVE 3 TO BQP-SUITE
                 MOVE 1 TO INPUT-ERROR
              END-IF
            END-IF.
           IF BQP-TYPE = 0
               MOVE 1 TO BQP-SUITE
            END-IF.
            IF BQP-SUITE < 1
               MOVE 1 TO BQP-SUITE
               MOVE 1 TO INPUT-ERROR
            END-IF.
            EVALUATE EXC-KEY
            WHEN  2 CALL "2-VIRREP" USING LINK-V BQP-RECORD
                    PR-RECORD REG-RECORD 
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    PERFORM DISPLAY-F-KEYS
                    MOVE 1 TO INPUT-ERROR
            WHEN  4 MOVE 66 TO SAVE-KEY
                    PERFORM NEXT-BANQP 
           WHEN  5 PERFORM NEXT-NUMBER
           END-EVALUATE.
           PERFORM READ-BANQP.
           PERFORM DIS-E1-01 THRU DIS-E1-END.

       APRES-1-5.
           IF BQP-TYPE = 9
              PERFORM APRES-1-SYN
           ELSE
              PERFORM APRES-1-BQ.
              
       APRES-1-BQ.
           MOVE BQP-BANQUE TO BQ-CODE.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN  5 MOVE BQP-BANQUE TO LNK-TEXT
                   CALL "1-BANQUE" USING LINK-V
                   MOVE LNK-TEXT TO BQP-BANQUE BQ-CODE
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-E1-01 THRU DIS-E1-END
           WHEN  2 CALL "2-BANQUE" USING LINK-V BQ-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-E1-01 THRU DIS-E1-END
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER PERFORM NEXT-BANQUE 
           END-EVALUATE.
           IF BQ-CODE = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           MOVE BQ-CODE TO BQP-BANQUE.
           PERFORM DIS-E1-05.
           IF  BQ-FICT     = 1
           AND INPUT-ERROR = 0
           AND EXC-KEY     = 13
              MOVE CHOIX-MAX-1 TO INDICE-ZONE
           END-IF.

      *  BQP-BANQUE-PREF



       APRES-1-SYN.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-SYNDIC" USING LINK-V SYN-RECORD
                   IF SYN-KEY NOT = SPACES
                      MOVE SYN-KEY TO BQP-SYNDICAT
                   END-IF
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER MOVE BQP-SYNDICAT TO SYN-KEY
                      MOVE EXC-KEY TO SAVE-KEY
                      PERFORM NEXT-SYNDICAT
                      MOVE SYN-KEY TO BQP-SYNDICAT
           END-EVALUATE.
           PERFORM DIS-E2-01.

       APRES-1-6.
           MOVE 0 TO LNK-NUM.
           IF BQP-COMPTE-PAYS NUMERIC 
           OR BQP-COMPTE-PAYS = SPACES 
              IF BQ-PAYS = "NL"
              AND BQ-SWIFT-BQ NOT = "PSTB"
                 CALL "0-NL11" USING LINK-V BQP-COMPTE
                 IF LNK-NUM NOT = 0
                    MOVE "SL" TO LNK-AREA
                    PERFORM DISPLAY-MESSAGE
                    MOVE 1 TO INPUT-ERROR
                 END-IF
              END-IF
           ELSE
              CALL "0-IBAN" USING LINK-V BQP-COMPTE
              IF LNK-NUM NOT = 0
                 MOVE "SL" TO LNK-AREA
                 PERFORM DISPLAY-MESSAGE
                 MOVE 1 TO INPUT-ERROR
              END-IF
              MOVE 0 TO IDX
              INSPECT BQP-COMPTE TALLYING IDX FOR CHARACTERS BEFORE " "
              IF IDX < 15
                 MOVE 54 TO LNK-NUM
                 MOVE "SL" TO LNK-AREA
                 PERFORM DISPLAY-MESSAGE
                 MOVE 1 TO INPUT-ERROR
              END-IF
              IF BQ-SWIFT-PAYS NOT = BQP-COMPTE-PAYS
                 MOVE 77 TO LNK-NUM
                 MOVE "SL" TO LNK-AREA
                 PERFORM DISPLAY-MESSAGE
                 MOVE 1 TO INPUT-ERROR
               END-IF
           END-IF.
           PERFORM DIS-E1-06.

       APRES-1-9.
           IF BQP-BENEFIC-NOM = SPACES
              INITIALIZE BQP-BENEFIC-RUE
           END-IF.
           PERFORM DIS-E1-09.

       APRES-1-10.
           IF BQP-BENEFIC-NOM = SPACES
              INITIALIZE BQP-BENEFIC-LOC
           END-IF.
           PERFORM DIS-E1-10.

       APRES-1-11.
           IF BQP-TYPE > 1
           AND BQP-TYPE < 6
           AND BQP-TOTAL = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-1-12.
           IF BQP-PROPOSITION > BQP-TOTAL AND BQP-TOTAL > 0
              MOVE  BQP-TOTAL TO BQP-PROPOSITION 
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF BQP-PROPOSITION = 0
              MOVE BQP-PROPOSITION TO BQP-I100
           END-IF.
           EVALUATE EXC-KEY 
            WHEN 10 IF BQP-I100 = 0
                       COMPUTE BQP-I100 = BQP-PROPOSITION / 
                       MOIS-IDX(LNK-MOIS)
                    ELSE
                       MOVE 0 TO BQP-I100 
                    END-IF
           END-EVALUATE.
           IF BQP-TYPE > 4
           AND BQP-TYPE < 9
           AND BQP-PROPOSITION = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.

           PERFORM DIS-E1-12.

       APRES-1-13.
           COMPUTE IDX-4 = LNK-ANNEE + 5.
           IF BQP-DEBUT-A NOT = 0
           IF BQP-DEBUT-A > IDX-4
              MOVE 1 TO INPUT-ERROR 
              MOVE IDX-4 TO BQP-DEBUT-A.
           IF BQP-DEBUT-A NOT = 0
           IF BQP-DEBUT-A < 1990
              MOVE 1 TO INPUT-ERROR 
              MOVE 1990 TO BQP-DEBUT-A .
           IF BQP-TYPE > 0
              IF BQP-DEBUT-A = 0
              MOVE 1 TO INPUT-ERROR.

       APRES-1-14.
           IF BQP-DEBUT-A  = 0 
              MOVE 0 TO BQP-DEBUT-M.
           IF BQP-DEBUT-A  NOT = 0
           IF BQP-DEBUT-M  > 12
              MOVE  1 TO INPUT-ERROR 
              MOVE 12 TO BQP-DEBUT-M. 
           IF BQP-DEBUT-A  NOT = 0
           IF BQP-DEBUT-M  < 1
              MOVE 1 TO INPUT-ERROR 
              MOVE 1 TO BQP-DEBUT-M.
           IF BQP-TYPE > 0
              IF BQP-DEBUT-M = 0
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-E1-14.

       APRES-1-15.
           IF BQP-FIN-A  NOT = 0
           IF BQP-FIN-A  < BQP-DEBUT-A
              MOVE 1 TO INPUT-ERROR 
              MOVE BQP-DEBUT-A TO BQP-FIN-A .

       APRES-1-16.
           IF BQP-FIN-A  = 0 
              MOVE 0 TO BQP-FIN-M.
           IF BQP-FIN-A NOT = 0
           IF BQP-FIN-M > 12
              MOVE  1 TO INPUT-ERROR 
              MOVE 12 TO BQP-FIN-M. 
           IF BQP-FIN-A NOT = 0
           IF BQP-FIN-M < 1
              MOVE 1 TO INPUT-ERROR 
              MOVE 1 TO BQP-FIN-M.
           IF BQP-FIN-A = BQP-DEBUT-A
           IF BQP-FIN-M < BQP-DEBUT-M
              MOVE 1 TO INPUT-ERROR 
              MOVE BQP-DEBUT-M TO BQP-FIN-M.
           PERFORM DIS-E1-16.


       APRES-2-1.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-BANQF" USING LINK-V BQF-RECORD
                   IF BQF-CODE NOT = SPACES
                      MOVE BQF-CODE TO BQP-BANQUE-PREF
                   END-IF
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER MOVE BQP-BANQUE-PREF TO BQF-CODE
                      MOVE EXC-KEY TO SAVE-KEY
                      PERFORM NEXT-BANQF
                      MOVE BQF-CODE TO BQP-BANQUE-PREF
           END-EVALUATE.
           PERFORM DIS-E2-01.

       APRES-2-2.
           IF BQP-ARRONDI > 2
              MOVE 2 TO BQP-ARRONDI.
           PERFORM DIS-E2-02.

       APRES-2-3.
           PERFORM DIS-E2-03.

       INDEX-RECENT.
           MOVE BQP-DEBUT-A TO INDEX-ANNEE.
           MOVE BQP-DEBUT-M TO INDEX-MOIS.
           CALL "1-GINDEX" USING LINK-V INDEX-RECORD.
           COMPUTE BQP-I100 = BQP-PROPOSITION / INDEX-FACT.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 CALL "0-TODAY" USING TODAY
                  MOVE TODAY-TIME TO BQP-TIME
                  MOVE LNK-USER TO BQP-USER
                  IF LNK-SQL = "Y" 
                     CALL "9-BANQP" USING LINK-V BQP-RECORD WR-KEY 
                  END-IF
                  IF LNK-LOG = "Y" 
                     CALL "6-ABQP" USING LINK-V BQP-RECORD WR-KEY 
                  END-IF
                  WRITE BQP-RECORD INVALID 
                  REWRITE BQP-RECORD END-WRITE   
                  IF ACTION-CALL = 1
                     PERFORM END-PROGRAM
                  END-IF
                  MOVE 1 TO ECRAN-IDX
                  PERFORM AFFICHAGE-ECRAN
                  PERFORM AFFICHAGE-DETAIL
                  MOVE 4 TO DECISION
           WHEN 6 MOVE "Y" TO LNK-YN
                  CALL "3-VIRHIS" USING LINK-V PR-RECORD 
                                        REG-RECORD BQP-RECORD
                  CANCEL "3-VIRHIS" 
                  PERFORM AFFICHAGE-ECRAN
                  PERFORM AFFICHAGE-DETAIL
                  PERFORM DISPLAY-F-KEYS
           WHEN 8 IF LNK-SQL = "Y" 
                     CALL "9-BANQP" USING LINK-V BQP-RECORD DEL-KEY 
                  END-IF
                  IF LNK-LOG = "Y" 
                     CALL "6-ABQP" USING LINK-V BQP-RECORD DEL-KEY 
                  END-IF
                  DELETE BANQP INVALID CONTINUE END-DELETE
                  MOVE 1 TO INDICE-ZONE ECRAN-IDX
                  PERFORM AFFICHAGE-ECRAN
                  PERFORM AFFICHAGE-DETAIL
                  INITIALIZE BQP-REC-DET  BQ-RECORD
                  MOVE 4 TO DECISION
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.
           
       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           
       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           MOVE 0 TO LNK-PERSON LNK-NUM.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           IF  REG-PERSON > 0
           AND PRES-TOT(LNK-MOIS) = 0
              MOVE "SL" TO LNK-AREA
              MOVE 0 TO LNK-POSITION
              MOVE 6 TO LNK-NUM
              CALL "0-DMESS" USING LINK-V
           ELSE
              DISPLAY SPACES LINE 24 POSITION 2 SIZE 70
           END-IF.

       NEXT-BANQUE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD SAVE-KEY.
           MOVE BQ-CODE TO BQP-BANQUE.

       NEXT-BANQF.
           CALL "6-BANQF" USING LINK-V BQF-RECORD SAVE-KEY.

       READ-BANQP.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD FAKE-KEY.
           
       NEXT-BANQP.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD SAVE-KEY.
           MOVE BQP-BANQUE TO BQ-CODE.
           MOVE 13 TO SAVE-KEY.
           PERFORM NEXT-BANQUE.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-SYNDICAT.
           CALL "6-SYNDIC" USING LINK-V SYN-RECORD SAVE-KEY.

       NEXT-NUMBER.
           MOVE BQP-TYPE TO IDX-1.
           MOVE FR-KEY TO BQP-FIRME.
           MOVE REG-PERSON TO BQP-PERSON.
           MOVE 99 TO BQP-SUITE.
           MOVE 0 TO NOT-FOUND.
           START BANQP KEY < BQP-KEY INVALID 
                INITIALIZE BQP-REC-DET
                NOT INVALID 
                PERFORM NEXT-NUMBER-1. 
           ADD 1 TO BQP-SUITE.

       NEXT-NUMBER-1.
           READ BANQP PREVIOUS AT END 
               MOVE 1 TO NOT-FOUND
           END-READ.
           IF FR-KEY NOT = BQP-FIRME
           OR REG-PERSON NOT = BQP-PERSON
           OR BQP-TYPE NOT = IDX-1
              MOVE 1 TO NOT-FOUND.
           IF NOT-FOUND = 1
              INITIALIZE BQP-RECORD
              MOVE FR-KEY TO BQP-FIRME 
              MOVE REG-PERSON TO BQP-PERSON
              MOVE IDX-1 TO BQP-TYPE.
           
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-E1-01.
           MOVE REG-PERSON  TO HE-Z6.
           IF REG-PERSON NOT = 0
              MOVE REG-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 
             LINE 3 POSITION 15 SIZE 6
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.

       DIS-E1-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.

       DIS-E1-03.
           DISPLAY BQP-TYPE LINE  4 POSITION 23.
           MOVE "VV" TO LNK-AREA.
           MOVE BQP-TYPE TO LNK-NUM.
           MOVE 04333000 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       DIS-E1-04.
           DISPLAY BQP-SUITE LINE 5 POSITION 23.

       DIS-E1-05.
           IF BQ-NOM = SPACES
              MOVE "???" TO BQ-NOM
           END-IF.
           IF BQP-TYPE = 9
              DISPLAY BQP-SYNDICAT LINE 6 POSITION 23
              DISPLAY SPACES  LINE 6 POSITION 33 SIZE 40
              DISPLAY SPACES  LINE 7 POSITION 70 SIZE 10
           ELSE
              DISPLAY BQP-BANQUE LINE 6 POSITION 23
              DISPLAY BQ-NOM     LINE 6 POSITION 33 SIZE 40
              DISPLAY BQ-SWIFT   LINE 7 POSITION 70 SIZE 10.
       DIS-E1-06.
           DISPLAY BQP-COMPTE LINE 7 POSITION 23 SIZE 35.
       DIS-E1-07.
           DISPLAY BQP-LIBELLE LINE 9 POSITION 20.
       DIS-E1-08.
           DISPLAY BQP-BENEFIC-NOM LINE 11 POSITION 30 SIZE 50.
       DIS-E1-09.
           DISPLAY BQP-BENEFIC-RUE LINE 12 POSITION 30 SIZE 50.
       DIS-E1-10.
           DISPLAY BQP-BENEFIC-LOC LINE 13 POSITION 30 SIZE 50.
       DIS-E1-11.
           MOVE BQP-TOTAL TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 15 POSITION 32.
       DIS-E1-12.
           MOVE BQP-PROPOSITION TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 16 POSITION 32.
           MOVE BQP-I100 TO HE-Z6Z6.
           DISPLAY HE-Z6Z6 LINE 16 POSITION 60.
       DIS-E1-13.
           MOVE BQP-DEBUT-A  TO HE-Z4.
           DISPLAY HE-Z4  LINE 19 POSITION 14.
       DIS-E1-14.
           MOVE BQP-DEBUT-M   TO HE-Z4 LNK-NUM.
           DISPLAY HE-Z4  LINE 20 POSITION 14.
           MOVE "MO" TO LNK-AREA.
           MOVE 20201200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-E1-15.
           MOVE BQP-FIN-A  TO HE-Z4.
           DISPLAY HE-Z4  LINE 19 POSITION 46.
       DIS-E1-16.
           MOVE BQP-FIN-M   TO HE-Z4  LNK-NUM.
           DISPLAY HE-Z4  LINE 20 POSITION 46.
           MOVE "MO" TO LNK-AREA.
           MOVE 20521200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-E1-END.
           EXIT.
       
       DIS-E2-01.
           DISPLAY BQP-BANQUE-PREF LINE 15 POSITION 32.
       DIS-E2-02.
           DISPLAY BQP-COMPTA  LINE 16 POSITION 32.
       DIS-E2-03.
           DISPLAY BQP-PERIODE LINE 18 POSITION 32.
       DIS-E2-04.
           MOVE BQP-NOTIF-A TO HE-AA.
           MOVE BQP-NOTIF-M TO HE-MM.
           MOVE BQP-NOTIF-J TO HE-JJ.
           DISPLAY HE-DATE LINE 20 POSITION 32.
       DIS-E2-05.
           MOVE BQP-LEVEE-A TO HE-AA.
           MOVE BQP-LEVEE-M TO HE-MM.
           MOVE BQP-LEVEE-J TO HE-JJ.
           DISPLAY HE-DATE LINE 21 POSITION 32.
       DIS-E2-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE ECR-S(ECRAN-IDX) TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           EVALUATE ECRAN-IDX
               WHEN 1 PERFORM DIS-E1-01 THRU DIS-E1-END
               WHEN 2 PERFORM DIS-E1-01 THRU DIS-E1-10
                      PERFORM DIS-E2-01 THRU DIS-E2-END.

       END-PROGRAM.
           CLOSE BANQP.
           CANCEL "2-BANQUE".
           CANCEL "2-BANQP".
           CANCEL "2-VIRREP".
           CANCEL "6-BANQUE".
           CANCEL "6-BANQP".
           CANCEL "0-IBAN".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
             IF DECISION = 99 
                DISPLAY BQP-USER LINE 24 POSITION 5
                DISPLAY BQP-ST-JOUR  LINE 24 POSITION 15
                DISPLAY BQP-ST-MOIS  LINE 24 POSITION 18
                DISPLAY BQP-ST-ANNEE LINE 24 POSITION 21
                DISPLAY BQP-ST-HEURE LINE 24 POSITION 30
                DISPLAY BQP-ST-MIN   LINE 24 POSITION 33
                GO AVANT-DEC
             END-IF.
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


