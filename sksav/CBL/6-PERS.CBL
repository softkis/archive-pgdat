      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-PERS MODULE LECTURE PERSONNES FICHIER X   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-PERS.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
      *    Fichier des personnes X
           SELECT OPTIONAL PERSONNE ASSIGN TO RANDOM, "X-PERSON"
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is PR-MATRICULE,
                  ALTERNATE RECORD KEY is PR-KEY,
                  STATUS FS-HELP.

       DATA DIVISION.

       FILE SECTION.

           COPY "PERSON.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN              PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PERSON.LNK".
                 
       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PERSONNE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-PERSON.
       
           IF NOT-OPEN = 0
              OPEN INPUT PERSONNE
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO PR-RECORD.
           READ PERSONNE NO LOCK KEY PR-MATRICULE INVALID EXIT PROGRAM 
               END-READ.
           MOVE PR-RECORD TO LINK-RECORD.
           EXIT PROGRAM.



