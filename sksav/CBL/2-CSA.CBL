      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-CSA   AFFICHAGE PERSONNE  AVEC UN CODE    �
      *  � SALAIRE DONN� /DETAIL ANNUEL                          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-CSA.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODPAIE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CODPAIE.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
SU         COPY "CODTXT.REC".

       01  CHOIX-MAX             PIC 99 VALUE 3.
       01  HEL-VAL              PIC ZZ BLANK WHEN ZERO.
       01  HEL-1                PIC 9999.
       01  HEL-2                PIC 9.
       01  CHOIX                 PIC X.

       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.
       01  TOTAL-VALEUR.
           02 TOT-UNITES         PIC 9(8)V99 COMP-3.
           02 TOT-TOTAL          PIC 9(8)V99 COMP-3.
           02 TOT-PERS           PIC 9(4) COMP-3.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
           02 HE-Z4Z2 PIC Z(4),ZZ BLANK WHEN ZERO.
           02 HE-Z4Z2R REDEFINES HE-Z4Z2.
              03 HE-Z4A2 PIC ZZZZ.
              03 HE-Z4B2 PIC ZZZ.
           02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.
           02 HE-Z6Z2R REDEFINES HE-Z6Z2.
              03 HE-Z6A2 PIC Z(6).
              03 HE-Z6B2 PIC ZZZ.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CODPAIE.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-CSA.
       
           MOVE LNK-SUFFIX TO ANNEE-PAIE.
           OPEN INPUT CODPAIE.
           PERFORM AFFICHAGE-ECRAN .
           INITIALIZE CS-RECORD.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0027000000 TO EXC-KFR(6).
           MOVE 0052530000 TO EXC-KFR(11).
           MOVE 0000000065 TO EXC-KFR(13).
           MOVE 6600000000 TO EXC-KFR(14).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1)
              WHEN 3  MOVE 0063160000 TO EXC-KFR(1).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2
           WHEN  3 PERFORM AVANT-3.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.


           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2
           WHEN  3 PERFORM APRES-3.

           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
                WHEN 53 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.          
           MOVE FR-KEY TO REG-FIRME.
           IF LNK-PERSON NOT = 0 
              MOVE LNK-PERSON TO REG-PERSON 
              MOVE 13 TO EXC-KEY
              PERFORM NEXT-REGIS
              MOVE 3 TO INDICE-ZONE
              MOVE 0 TO LNK-PERSON
           ELSE 
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           ACCEPT CD-NUMBER
             LINE  4 POSITION 17 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE
              IF PRES-ANNEE = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           IF LNK-COMPETENCE < REG-COMPETENCE
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-PERSON
           END-IF.
           PERFORM DIS-HE-01 THRU DIS-HE-03.
           IF INPUT-ERROR = 0
           AND CD-NUMBER > 0
              PERFORM AFFICHE
           END-IF.

       APRES-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   PERFORM DIS-HE-01 THRU DIS-HE-02
                   IF REG-PERSON > 0
                   AND CD-NUMBER > 0
                     PERFORM AFFICHE
                   END-IF
           END-EVALUATE.
           IF REG-PERSON = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-3.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 2 CALL "2-CSDEF" USING LINK-V CD-RECORD
                  IF CD-NUMBER > 0
                     PERFORM CS-DATA 
                  END-IF
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
           WHEN 3 CALL "2-CSD" USING LINK-V CTX-RECORD
                  IF CTX-NUMBER > 0
                     MOVE CTX-NUMBER TO CD-NUMBER
                     PERFORM CS-DATA 
                  END-IF
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
                  CANCEL "2-CSD"
           WHEN 27 CONTINUE
           WHEN OTHER PERFORM NEXT-CS 
           END-EVALUATE.
           PERFORM DIS-HE-03.
           IF EXC-KEY NOT = 27 
              IF CTX-NOM > SPACES
                 PERFORM AFFICHE
              END-IF
           END-IF.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       NEXT-CS.
           CALL "6-CSDEF" USING LINK-V CD-RECORD EXC-KEY.
           PERFORM CS-DATA.

       CS-DATA.
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
SU         CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
           
       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-03.
           MOVE CD-NUMBER TO HE-Z4.
           DISPLAY HE-Z4  LINE 4 POSITION 17.
           DISPLAY CTX-NOM LINE 4 POSITION 22.
           DISPLAY CTX-DESCRIPTION(1) LINE 5 POSITION 30 SIZE 9.
           DISPLAY CTX-DESCRIPTION(2) LINE 5 POSITION 39 SIZE 9.
           DISPLAY CTX-DESCRIPTION(3) LINE 5 POSITION 48 SIZE 9.
           DISPLAY CTX-DESCRIPTION(4) LINE 5 POSITION 56 SIZE 9.
           DISPLAY CTX-DESCRIPTION(5) LINE 5 POSITION 65 SIZE 9.
           DISPLAY CTX-DESCRIPTION(6) LINE 5 POSITION 74 SIZE 7.
       DIS-HE-END.
           EXIT.


       AFFICHE.
           INITIALIZE CSP-RECORD TOTAL-VALEUR.
           MOVE 6 TO LIN-IDX.
           MOVE FR-KEY     TO CSP-FIRME.
           MOVE REG-PERSON TO CSP-PERSON.
           START CODPAIE KEY > CSP-KEY INVALID CONTINUE
                NOT INVALID PERFORM READ-CSP THRU READ-END.
                
       READ-CSP.
           READ CODPAIE NEXT NO LOCK AT END GO READ-END.
           IF FR-KEY     NOT = CSP-FIRME
           OR REG-PERSON NOT = CSP-PERSON
              GO READ-END
           END-IF.
           IF CD-NUMBER NOT = CSP-CODE
           OR CSP-SUITE NOT = 0
              GO READ-CSP
           END-IF.
           PERFORM DIS-HIS-LIGNE.
           IF LIN-IDX > 20
              PERFORM INTERRUPT THRU INTERRUPT-END
              MOVE 6 TO LIN-IDX
           END-IF.
           GO READ-CSP.
       READ-END.
           PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21.
           PERFORM DIS-TOT-LIGNE.

       DIS-HIS-LIGNE.
           ADD 1 TO LIN-IDX.
           COMPUTE LNK-NUM = CSP-MOIS + 100.
           MOVE "MO" TO LNK-AREA.
           COMPUTE LNK-POSITION = LIN-IDX * 1000000 + 031100
           CALL "0-DMESS" USING LINK-V.

           MOVE CSP-DONNEE-1 TO HE-Z6Z2.
           MOVE 30 TO COL-IDX.
           PERFORM DIS-Z6.
           MOVE CSP-DONNEE-2 TO HE-Z6Z2
           MOVE 39 TO COL-IDX.
           DISPLAY HE-Z6Z2   LINE  LIN-IDX POSITION 39.
           MOVE CSP-POURCENT TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z4Z2   LINE  LIN-IDX POSITION 48 LOW.
           MOVE CSP-UNITE    TO HE-Z4Z2.
           MOVE 55 TO COL-IDX.
           PERFORM DIS-Z4.

           MOVE CSP-UNITAIRE TO HE-Z6Z2.
           INSPECT HE-Z6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z6Z2   LINE  LIN-IDX POSITION 63 LOW.
           MOVE CSP-TOTAL    TO HE-Z6Z2.
           MOVE 72 TO COL-IDX.
           PERFORM DIS-Z6.
           ADD CSP-UNITE TO TOT-UNITES.
           ADD CSP-TOTAL TO TOT-TOTAL.

       DIS-TOT-LIGNE.
           MOVE 22 TO LIN-IDX.
           MOVE TOT-PERS TO HE-Z4.
           DISPLAY HE-Z4   LINE  LIN-IDX POSITION 2.
           MOVE TOT-UNITES TO HE-Z6Z2.
           INSPECT HE-Z6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z6Z2   LINE  LIN-IDX POSITION 53.
           MOVE TOT-TOTAL TO HE-Z6Z2.
           MOVE 72 TO COL-IDX.
           PERFORM DIS-Z6.

       DIS-Z6.
           IF HE-Z6B2 = ",00"
              DISPLAY HE-Z6A2   LINE  LIN-IDX POSITION COL-IDX
           ELSE
              DISPLAY HE-Z6Z2   LINE  LIN-IDX POSITION COL-IDX.
       DIS-Z4.
           IF HE-Z4B2 = ",00"
              DISPLAY HE-Z4A2   LINE  LIN-IDX POSITION COL-IDX
           ELSE
              DISPLAY HE-Z4Z2   LINE  LIN-IDX POSITION COL-IDX.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000030000 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.

           ACCEPT CHOIX 
           LINE 23 POSITION 30 SIZE 1
           TAB UPDATE NO BEEP CURSOR 1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
       INTERRUPT-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 2506 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

