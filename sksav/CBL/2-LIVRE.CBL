      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-LIVRE                                     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-LIVRE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  ECRAN-SUITE.
           02 ECR-S1  PIC 9999 VALUE 2501.
           02 ECR-S2  PIC 9999 VALUE 2502.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S   PIC 9999 OCCURS 2.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "LIVRE.LNK".
           COPY "LIVRE.REC".

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE 4. 
           02  CHOIX-MAX-2       PIC 99 VALUE 1. 
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 2. 
       01  HELP-1                PIC 9(8).
       01  MALADIE               PIC 9 VALUE 0.

       01  LIVRE-NAME.
           02 FILLER             PIC X(8) VALUE "S-LIVRE.".
           02 ANNEE-LIVRE        PIC 999.

       01  TOT-COT.
           02  T-C               PIC 9(8)V99 OCCURS 4. 

       01   ECR-DISPLAY.
            02 HE-Z2 PIC Z(2).
            02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4).
            02 HE-Z6 PIC Z(6).
            02 HEZ6Z2 PIC -Z(7),ZZ.
            02 HE-Z2Z2 PIC ZZ,ZZ. 
            02 HEZ8   PIC -Z(8).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-LIVRE.
       
           PERFORM AFFICHAGE-ECRAN.
           INITIALIZE L-RECORD.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
            UNTIL INDICE-ZONE > 100.
      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions
           IF ECRAN-IDX = 1
              MOVE 0067680000 TO EXC-KFR (14) 
              EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 3  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
                      MOVE 0000007400 TO EXC-KFR(1)
                      IF MENU-BATCH = 1
                         MOVE 0000007437 TO EXC-KFR(1)
                      END-IF
              WHEN 4  IF LNK-PERSON = 0 
                         MOVE 0052000000 TO EXC-KFR(11)
                      END-IF
           END-IF.        

           IF ECRAN-IDX = 2
              MOVE 0067000000 TO EXC-KFR (14) 
              EVALUATE INDICE-ZONE
              WHEN  1  MOVE 0000000000 TO EXC-KFR (1)
                       MOVE 0052000000 TO EXC-KFR (11)
           END-IF.    

           PERFORM DISPLAY-F-KEYS.
           
       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-1-1 
               WHEN  2 PERFORM AVANT-1-2 
               WHEN  3 PERFORM AVANT-1-3 
               WHEN  4 PERFORM AVANT-DEC.

           IF ECRAN-IDX = 2
              PERFORM AVANT-DEC
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF ECRAN-IDX = 1 AND EXC-KEY = 67 PERFORM END-PROGRAM.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF EXC-KEY > 66 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
              INITIALIZE DECISION
              GO TRAITEMENT-ECRAN
           END-IF.
           
           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
              WHEN  1 PERFORM APRES-1-1 
              WHEN  2 PERFORM APRES-1-2 
              WHEN  3 PERFORM APRES-1-3 THRU APRES-1-3-1
              WHEN  4 PERFORM APRES-DEC.

           IF ECRAN-IDX = 2
              EVALUATE INDICE-ZONE
               WHEN  1 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
           END-EVALUATE. 
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.          
           MOVE FR-KEY TO REG-FIRME.
           IF LNK-PERSON NOT = 0 
              MOVE LNK-PERSON TO REG-PERSON 
              MOVE LNK-MOIS   TO L-MOIS 
              MOVE 13 TO EXC-KEY
              PERFORM NEXT-REGIS
              PERFORM AFFICHAGE-DETAIL
              MOVE 3 TO INDICE-ZONE
              MOVE 0 TO LNK-PERSON
           ELSE 
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.            
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.            
           IF L-MOIS  = 0 
               MOVE LNK-MOIS  TO L-MOIS.
           ACCEPT L-MOIS  
             LINE  4 POSITION 19 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 MOVE 65 TO EXC-KEY
             WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           MOVE L-MOIS TO HE-Z2.
           DISPLAY HE-Z2 LINE 4 POSITION 19.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           IF L-MOIS = 0 
              MOVE LNK-MOIS TO L-MOIS.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  65 THRU 66 PERFORM NEXT-REGIS
                   PERFORM AFFICHAGE-MOIS
           WHEN 5 CALL "1-REGIST" USING LINK-V
                  IF LNK-PERSON > 0
                     MOVE LNK-PERSON TO REG-PERSON
                     PERFORM NEXT-REGIS
                  END-IF
                  CANCEL "1-REGIST"  
                  PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE
              IF PRES-TOT(LNK-MOIS) = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           IF LNK-COMPETENCE < REG-COMPETENCE
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-PERSON
           END-IF.
           IF INPUT-ERROR = 1
              MOVE L-RECORD TO LINK-RECORD
              PERFORM DIS-E1
           ELSE
              PERFORM AFFICHAGE-DETAIL
              PERFORM AFFICHAGE-MOIS
           END-IF.

       APRES-1-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   PERFORM DIS-HE-01
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM AFFICHAGE-MOIS
           END-EVALUATE.                     

       APRES-1-3.
           MOVE 0 TO MALADIE.
           EVALUATE EXC-KEY
               WHEN 65 SUBTRACT 1 FROM L-MOIS
               WHEN 66 ADD 1 TO L-MOIS
               WHEN  4 MOVE 1 TO MALADIE
               WHEN  5 MOVE L-MOIS TO LNK-MOIS
                       CALL "4-ARCHIV" USING LINK-V REG-RECORD
                       CANCEL "4-ARCHIV" 
           END-EVALUATE.

       APRES-1-3-1.
           IF L-MOIS > 12
              MOVE 12 TO L-MOIS.
           IF L-MOIS = 0
              PERFORM AFFICHAGE-ANNEE
           ELSE
              PERFORM AFFICHAGE-MOIS
           END-IF.


       APRES-DEC.
           EVALUATE EXC-KEY 
              WHEN 27 COMPUTE DECISION = INDICE-ZONE + 1
           END-EVALUATE.
           IF DECISION > 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX (ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           

       ENTREE-SORTIE.
           MOVE "AA" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           PERFORM DISPLAY-MESSAGE.

       AFFICHAGE-MOIS.
           MOVE LNK-MOIS TO IDX-1.
           MOVE LNK-SUITE TO LNK-VAL.
           MOVE L-MOIS TO LNK-MOIS.
           INITIALIZE LINK-RECORD.
           CALL "4-LPADD" USING LINK-V REG-RECORD LINK-RECORD.
           MOVE IDX-1 TO LNK-MOIS.
           PERFORM DIS-HE-03.
           PERFORM DIS-E1.

       AFFICHAGE-ANNEE.
           MOVE MALADIE TO LNK-VAL.
           INITIALIZE LINK-RECORD.
           MOVE LNK-MOIS TO IDX-1.
           PERFORM CUM-ANNEE VARYING LNK-MOIS FROM 1 BY 1 UNTIL LNK-MOIS
                                > IDX-1.
           MOVE IDX-1 TO LNK-MOIS.
           PERFORM DIS-E1.

       CUM-ANNEE.
           CALL "4-LPADD" USING LINK-V REG-RECORD LINK-RECORD.
           
       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-03.
           MOVE L-MOIS TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 4 POSITION 19.
           MOVE "MO" TO LNK-AREA.
           MOVE 04231200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       DIS-E1.
230507*    COMPUTE SH-00 = LINK-IMPOS(1) + LINK-IMPOS(5) + LINK-DC(205).  
           COMPUTE SH-00 = LINK-IMPOS(1) + LINK-DC(205).  

           MOVE SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE  8 POSITION 32.
           COMPUTE SH-00 = LINK-IMPOS(13) 
                         + LINK-IMPOS(14)
                         + LINK-IMPOS(15)
                         + LINK-IMPOS(16)
                         + LINK-IMPOS(17)
                         + LINK-IMPOS(18)
                         + LINK-IMPOS(19).


           MOVE SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE  9 POSITION 32.
           COMPUTE SH-00 = LINK-IMPOS(20) 
                         + LINK-IMPOS(28)
                         + LINK-IMPOS(29).


           MOVE SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE  9 POSITION 69.

           MOVE  LINK-IMPOS(24) TO HEZ6Z2.
           IF HEZ6Z2 > SPACES
              INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   "
              DISPLAY HEZ6Z2  LINE 10 POSITION 22 REVERSE
           ELSE
              DISPLAY SPACES  LINE 10 POSITION 22 SIZE 11.
           MOVE  LINK-IMPOS(12) TO HEZ6Z2.
           IF HEZ6Z2 > SPACES
              INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   "
              DISPLAY HEZ6Z2  LINE 11 POSITION 22 REVERSE
           ELSE
              DISPLAY SPACES  LINE 11 POSITION 22 SIZE 11.

           COMPUTE SH-00 = LINK-IMPOS(21) + LINK-DC(235)
           MOVE SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 12 POSITION 32.

           MOVE  LINK-IMPOS(21) TO HEZ6Z2.
           IF HEZ6Z2 > SPACES
              INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   "
              DISPLAY HEZ6Z2  LINE 12 POSITION 22 REVERSE
           ELSE
              DISPLAY SPACES  LINE 12 POSITION 22 SIZE 11.
           COMPUTE SH-00 = LINK-IMPOS(22).
           MOVE SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 13 POSITION 32.
           COMPUTE SH-00 = LINK-IMPOS(13) + LINK-IMPOS(14).
           MOVE SH-00 TO HEZ6Z2.
           IF HEZ6Z2 > SPACES
              INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   "
              DISPLAY HEZ6Z2  LINE 13 POSITION 22 REVERSE
           ELSE
              DISPLAY SPACES  LINE 13 POSITION 22 SIZE 11.

           MOVE LINK-DC(205) TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 14 POSITION 32.
           COMPUTE SH-00 = LINK-IMPOS(5) + LINK-DC(205).  
           MOVE SH-00 TO HEZ6Z2.
           IF HEZ6Z2 > SPACES
              INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   "
              DISPLAY HEZ6Z2  LINE 14 POSITION 22 REVERSE
           ELSE
              DISPLAY SPACES  LINE 14 POSITION 22 SIZE 11.

           MOVE  LINK-DC(201)  TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 15 POSITION 32.

           MOVE  LINK-IMPOS(25) TO HEZ6Z2.
           IF HEZ6Z2 > SPACES
              INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   "
              DISPLAY HEZ6Z2  LINE 15 POSITION 22 REVERSE
           ELSE
              DISPLAY SPACES  LINE 15 POSITION 22 SIZE 11.
           MOVE  LINK-JRS(55)   TO HE-Z3.
           DISPLAY HE-Z3 LINE 15 POSITION 15.

           MOVE  LINK-IMPOS(31) TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 16 POSITION 32.

           MOVE  LINK-IMPOS(2) TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE  8 POSITION 69.


           COMPUTE SH-00 = LINK-IMPOS(26) + LINK-DC(245)
           MOVE SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 12 POSITION 69.

           MOVE  LINK-IMPOS(26) TO HEZ6Z2.
           IF HEZ6Z2 > SPACES
              INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   "
              DISPLAY HEZ6Z2  LINE 12 POSITION 58 REVERSE
           ELSE
              DISPLAY SPACES  LINE 12 POSITION 58 SIZE 11.


           MOVE  LINK-IMPOS(27) TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 13 POSITION 58.

           MOVE  LINK-IMPOS(30) TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           IF HEZ6Z2 > SPACES
              INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   "
              DISPLAY HEZ6Z2  LINE 15 POSITION 58 REVERSE
           ELSE
              DISPLAY SPACES  LINE 15 POSITION 58 SIZE 11.

           COMPUTE SH-00 = LINK-DC(202) + LINK-DC(207).
           MOVE  SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 15 POSITION 69.
           MOVE  LINK-IMPOS(32) TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 16 POSITION 69.
           MOVE  LINK-DC(133) TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 17 POSITION 32.
           COMPUTE SH-00 = LINK-IMPOS(35).

           MOVE  SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 18 POSITION 32.
           MOVE 0 TO SH-00.
           ADD LINK-DC(199) TO SH-00.
           PERFORM TOT-PLUS VARYING IDX FROM 181 BY 1 UNTIL IDX > 197.
           PERFORM TOT-PLUS VARYING IDX FROM   1 BY 1 UNTIL IDX > 9.
           MOVE  SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 19 POSITION 32.
           MOVE 0 TO SH-00.
           PERFORM TOT-MINUS VARYING IDX FROM 266 BY 1 UNTIL IDX > 299.
           MOVE  SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 20 POSITION 32.


           MOVE LINK-DC(198) TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 21 POSITION 32.

           COMPUTE SH-00 = LINK-DC(300) - LINK-DC(200).
           MOVE SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 22 POSITION 32.

           COMPUTE SH-00 = LINK-IMPOS(25) + LINK-IMPOS(30)
                         + LINK-DC(205) + LINK-IMPOS(5).
           MOVE  SH-00 TO HEZ6Z2.
           IF HEZ6Z2 > SPACES
              INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   "
              DISPLAY HEZ6Z2  LINE 21 POSITION 58 REVERSE
           ELSE
              DISPLAY SPACES  LINE 21 POSITION 58 SIZE 11.

           COMPUTE SH-00 = LINK-DC(201) + LINK-DC(202) 
                         + LINK-DC(205) + LINK-DC(207).
           MOVE  SH-00 TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2 LINE 21 POSITION 69.

       TOT-PLUS.
           ADD LINK-DC(IDX) TO SH-00.

       TOT-MINUS.
           ADD LINK-DC(IDX) TO SH-00.

       DIS-E2.
           MOVE 4 TO LIN-IDX.
           PERFORM ABAT-MOIS VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           INITIALIZE TOT-COT.
           MOVE 11 TO LIN-IDX.
           PERFORM COT-MOIS VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           PERFORM TOT-COT VARYING IDX FROM 1 BY 1 UNTIL IDX > 4.


       COT-MOIS.
           ADD 1 TO LIN-IDX.
           IF LINK-COT-NET(IDX) > 0
              MOVE  LINK-COT-NET(IDX) TO HEZ6Z2
              INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   "
              DISPLAY HEZ6Z2  LINE LIN-IDX POSITION 12 REVERSE
           ELSE
              DISPLAY SPACES  LINE LIN-IDX POSITION 12 SIZE 10.
           COMPUTE IDX-1 = 230 + IDX.
           MOVE  LINK-DC(IDX-1) TO HEZ6Z2.
           ADD   LINK-DC(IDX-1) TO T-C(1).
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE LIN-IDX POSITION 24.
           COMPUTE IDX-1 = 210 + IDX.
           MOVE  LINK-DC(IDX-1) TO HEZ6Z2.
           ADD   LINK-DC(IDX-1) TO T-C(2).
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE LIN-IDX POSITION 35.
           IF LINK-COT-NET-NP(IDX) > 0
              MOVE  LINK-COT-NET-NP(IDX) TO HEZ6Z2
              INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   "
              DISPLAY HEZ6Z2  LINE LIN-IDX POSITION 46 REVERSE
           ELSE
              DISPLAY SPACES  LINE LIN-IDX POSITION 46 SIZE 11.
           COMPUTE IDX-1 = 240 + IDX.
           MOVE  LINK-DC(IDX-1) TO HEZ6Z2.
           ADD   LINK-DC(IDX-1) TO T-C(3).
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE LIN-IDX POSITION 57.
           COMPUTE IDX-1 = 220 + IDX.
           MOVE  LINK-DC(IDX-1) TO HEZ6Z2.
           ADD   LINK-DC(IDX-1) TO T-C(4).
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE LIN-IDX POSITION 68.

       TOT-COT.
           MOVE  T-C(IDX) TO HEZ6Z2.
           EVALUATE IDX
              WHEN 1 MOVE 24 TO COL-IDX
              WHEN 2 MOVE 35 TO COL-IDX
              WHEN 3 MOVE 57 TO COL-IDX
              WHEN 4 MOVE 68 TO COL-IDX
           END-EVALUATE.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE 22 POSITION COL-IDX.

       ABAT-MOIS.
           ADD 1 TO LIN-IDX.
           MOVE  LINK-ABAT(IDX) TO HEZ6Z2.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2  LINE LIN-IDX POSITION 35.

       AFFICHAGE-ECRAN.
           MOVE ECR-S(ECRAN-IDX) TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           EVALUATE ECRAN-IDX
               WHEN 1 PERFORM DIS-HE-01 THRU DIS-HE-03
                      PERFORM DIS-E1 
               WHEN 2 PERFORM DIS-HE-01 THRU DIS-HE-03
                      PERFORM DIS-E2
           END-EVALUATE.

       END-PROGRAM.
           CANCEL "4-LPADD".
           MOVE REG-PERSON TO LNK-PERSON.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".
           