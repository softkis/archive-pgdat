      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-MNTXT RECHERCHE DES TEXTES MENUS          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-MNTXT.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MENUTEXT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "MENUTEXT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  NOT-OPEN              PIC 9 VALUE 0.
       01  ACTION                PIC X.

       01  MENUTEXT-NAME.
           02 FILLER             PIC X(7) VALUE "S-MENU.".
           02 MENUTEXT-LANGUE    PIC XX.

       01  FS-MENUTEXT  PIC XX.

       LINKAGE SECTION.
      
       01  LG-IDX   PIC XX.
           COPY "MENU.REC".

       PROCEDURE DIVISION USING LG-IDX MN-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MENUTEXT.
       
       FILE-ERROR-PROC.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-MENU.
       
           IF NOT-OPEN = 1
              IF LG-IDX NOT = MENUTEXT-LANGUE
              CLOSE MENUTEXT
              MOVE 0 TO NOT-OPEN.

           IF NOT-OPEN = 0
              MOVE LG-IDX TO MENUTEXT-LANGUE
              OPEN INPUT MENUTEXT
              MOVE 1 TO NOT-OPEN.

           MOVE MN-KEY TO MNT-KEY.
           READ MENUTEXT INVALID CONTINUE
           NOT INVALID MOVE MNT-DESCRIPTION TO MN-DESCRIPTION
           EXIT PROGRAM.

           IF MN-PROG-NAME NOT = SPACES
              MOVE MN-PROG-NAME TO MNT-KEY.
           IF MN-PROG-NAME = "4-LOOP"
              MOVE MN-EXTENSION-1 TO MNT-KEY.
           READ MENUTEXT INVALID CONTINUE
           NOT INVALID 
               IF MNT-DESCRIPTION NOT = SPACES
                  MOVE MNT-DESCRIPTION TO MN-DESCRIPTION
               END-IF
           END-READ.

           EXIT PROGRAM.

           
      