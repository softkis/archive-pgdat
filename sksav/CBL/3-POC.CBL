      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-POC  IMPRESSION SCHEMA OCCUPATION DU MOIS �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-POC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".
           COPY "FORM215.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "JOURS.FDE".
           COPY "FORM215.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "215       ".
       01  CHOIX-MAX             PIC 99 VALUE 10.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "OCCUP.REC".
           COPY "OCCTXT.REC".
           COPY "OCCOM.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(215) OCCURS 45.

       01  JOURS-NAME.
           02 JOURS-ID           PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".POC".

       01  CHOIX           PIC 99 VALUE 0.

           COPY "V-VH00.CPY".
           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 999999.
       01  SAVE-PERSON           PIC 9(8) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  OCC-DEBUT             PIC 99   VALUE 0.
       01  OCC-FIN               PIC 99   VALUE 0.
       01  OCO-DEBUT             PIC 99   VALUE 0.
       01  OCO-FIN               PIC 99   VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HE-TEMP PIC ZZ.ZZ BLANK WHEN ZERO.
           02 HE-FIN.
              03 HE-X PIC X VALUE ":".
              03 HE-F PIC ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-POC.
       
           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN INPUT JOURS.
           INITIALIZE LIN-NUM.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-MOIS TO SAVE-MOIS.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8    MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 10    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-8
           WHEN  9 PERFORM APRES-9
           WHEN 10 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.            
           ACCEPT OCC-DEBUT
             LINE 16 POSITION 32 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.            
           ACCEPT OCC-FIN
             LINE 17 POSITION 32 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.            
           ACCEPT OCO-NUMBER
             LINE 19 POSITION 32 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-9.            
           ACCEPT OCO-FIN
             LINE 20 POSITION 32 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           MOVE OCC-DEBUT TO OCC-KEY.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-OCCUP" USING LINK-V OCC-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-OCCUP 
           END-EVALUATE.
           MOVE OCC-KEY TO OCC-DEBUT.
           IF OCC-NOM = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-06.

       APRES-7.
           IF OCC-FIN > 50
              MOVE 50 TO OCC-FIN
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF OCC-FIN < OCC-KEY
              MOVE OCC-KEY TO OCC-FIN
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-07.

       APRES-8.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-OCCOM" USING LINK-V OCO-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-OCCOM 
           END-EVALUATE.
           PERFORM DIS-HE-08.

       APRES-9.
           IF OCO-FIN > 20
              MOVE 20 TO OCO-FIN
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF OCO-FIN < OCO-NUMBER
              MOVE OCO-NUMBER TO OCO-FIN
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-09.
       
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM START-PR
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PR.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           PERFORM READ-PERSON THRU READ-EXIT.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-1
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           PERFORM TOTAL-JOURS THRU TOTAL-JOURS-END.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.
           EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF PRES-ANNEE = 0
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR.

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE OCC-DEBUT TO HE-Z2 OCC-KEY.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY.
           MOVE OCC-KEY TO OT-NUMBER
           CALL "6-OCCTXT" USING LINK-V OT-RECORD FAKE-KEY
           DISPLAY HE-Z2  LINE 16 POSITION 32.
           DISPLAY OT-NOM LINE 16 POSITION 40 SIZE 40.
       DIS-HE-07.
           MOVE OCC-FIN TO HE-Z2 OCC-KEY.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY.
           MOVE OCC-KEY TO OT-NUMBER
           CALL "6-OCCTXT" USING LINK-V OT-RECORD FAKE-KEY
           DISPLAY HE-Z2  LINE 17 POSITION 32.
           DISPLAY OT-NOM LINE 17 POSITION 40 SIZE 40.
       DIS-HE-08.
           MOVE OCO-NUMBER TO HE-Z2.
           DISPLAY HE-Z2    LINE 19 POSITION 32.
           DISPLAY OCO-NOM  LINE 19 POSITION 40.
       DIS-HE-09.
           MOVE OCO-FIN TO HE-Z2.
           DISPLAY HE-Z2  LINE 20 POSITION 32.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1353 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L215" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           IF LIN-NUM > 0
              PERFORM TRANSMET
           IF COUNTER > 0 
              MOVE 99 TO LNK-VAL
              CALL "L215" USING LINK-V FORMULAIRE
           END-IF.
           CANCEL "L215".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XSEMNOM.CPY".


       FILL-FILES.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM LAST-PERSON
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= 43
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM LAST-PERSON
           END-IF.
           IF LIN-NUM = 0 
              PERFORM READ-FORM
              PERFORM ENTETE
              MOVE LIN-IDX TO LIN-NUM
              PERFORM SEMAINE-NOM
           END-IF.
           ADD 1 TO LIN-NUM.
           IF REG-PERSON NOT = LAST-PERSON
           OR LAST-PERSON = 0
              PERFORM DIS-HE-01
              PERFORM DONNEES-PERSONNE.
           IF JRS-COMPLEMENT > 0
              MOVE JRS-COMPLEMENT TO OCO-NUMBER
              PERFORM DONNEES-OCCOM
           ELSE
              MOVE JRS-OCCUPATION TO OCC-KEY
              PERFORM DONNEES-OCCUP
           END-IF.
           PERFORM FILL-LINE.

       NEXT-OCCUP.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD EXC-KEY.
           MOVE OCC-KEY TO OT-NUMBER
           CALL "6-OCCTXT" USING LINK-V OT-RECORD FAKE-KEY.

       NEXT-OCCOM.
           CALL "6-OCCOM" USING LINK-V OCO-RECORD EXC-KEY.

       TOTAL-JOURS.
           INITIALIZE JRS-RECORD.
           MOVE REG-FIRME  TO JRS-FIRME.
           MOVE REG-PERSON TO JRS-PERSON.
           MOVE LNK-MOIS   TO JRS-MOIS.
           MOVE OCC-DEBUT  TO JRS-OCCUPATION.
           START JOURS KEY >= JRS-KEY INVALID GO TOTAL-JOURS-END.
           PERFORM READ-JOURS THRU READ-JR-END.
       TOTAL-JOURS-END.
           EXIT.

       READ-JOURS.
           READ JOURS NEXT AT END 
                GO READ-JR-END.
                
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR JRS-MOIS   NOT = LNK-MOIS
           OR JRS-OCCUPATION > OCC-FIN
              GO READ-JR-END.
           IF OCO-DEBUT > 0
           AND JRS-COMPLEMENT > 0
           AND JRS-COMPLEMENT < OCO-DEBUT 
              GO READ-JOURS.
           IF JRS-COMPLEMENT > OCO-FIN 
           OR JRS-POSTE > 0
              GO READ-JOURS.

           PERFORM FILL-FILES.
           PERFORM DIS-HE-01.
           GO READ-JOURS.
       READ-JR-END.
           EXIT.


       SEMAINE-NOM.
           MOVE 32 TO COL-NUM.
           PERFORM SEMAINE-INDEX VARYING IDX FROM 1 BY 1 UNTIL 
                    IDX > MOIS-JRS(LNK-MOIS).

       SEMAINE-INDEX.
           COMPUTE LNK-NUM = SEM-IDX(LNK-MOIS, IDX) + 200.
           PERFORM SEM-NOM.
           ADD 2 TO COL-NUM.
           PERFORM FILL-FORM.

       ENTETE.
      *    PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  3 TO CAR-NUM.
           MOVE  4 TO LIN-NUM.
           MOVE 168 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

           MOVE  2 TO LIN-NUM.
           MOVE 110 TO COL-NUM.
           PERFORM MOIS-NOM.
           PERFORM FILL-FORM.
           MOVE 120 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 70 TO COL-NUM.
           MOVE MENU-DESCRIPTION TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE  2 TO LIN-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE 161 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 164 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 167 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE 2 TO LIN-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE 8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           MOVE 15 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 3 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 4 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-CODE-POST TO VH-00.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           IF CHOIX > 0 
      *        MOVE TRIPR-A TO VH-00
               MOVE   6 TO CAR-NUM
               MOVE   2 TO LIN-NUM
               MOVE 115 TO COL-NUM
               PERFORM FILL-FORM
           END-IF.

       DONNEES-OCCUP.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY.
           MOVE OCC-KEY TO OT-NUMBER
           CALL "6-OCCTXT" USING LINK-V OT-RECORD FAKE-KEY
           MOVE 5 TO COL-NUM.
           MOVE OT-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       DONNEES-OCCOM.
           MOVE JRS-COMPLEMENT TO OCO-NUMBER.
           CALL "6-OCCOM" USING LINK-V OCO-RECORD FAKE-KEY.
           MOVE 5 TO COL-NUM.
           MOVE OCO-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       DONNEES-PERSONNE.
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  6 TO COL-NUM.
           MOVE REG-PERSON TO VH-00 LAST-PERSON.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.

       FILL-LINE.
           PERFORM FILL-HRS VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 31.
           COMPUTE COL-NUM = 188.
           MOVE JRS-HRS(32) TO VH-00.
           MOVE 3 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           IF LIN-NUM < 44
              PERFORM X
           END-IF.

       FILL-HRS.
           IF JRS-HRS(IDX-1) > 0
              COMPUTE COL-NUM = 33 + (IDX-1 - 1) * 5
              MOVE JRS-HRS(IDX-1) TO VH-00
              MOVE 2 TO DEC-NUM
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
           END-IF.

       X.
           PERFORM COLONNES VARYING COL-NUM FROM 32 BY 4 UNTIL COL-NUM
           > 188.

       COLONNES.
           MOVE "+" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.


