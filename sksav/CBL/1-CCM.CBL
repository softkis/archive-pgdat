      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-CCM CENTRES DE COUT MULTIPLES             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-CCM.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CCM.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CCM.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CONTRAT.REC".
           COPY "COUT.REC".
           COPY "PRESENCE.REC".
           COPY "V-VAR.CPY".

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE  4. 
           02  CHOIX-MAX-2       PIC 99 VALUE 80.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 2.


       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-Z2 PIC ZZ BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6). 
           02 HE-Z8 PIC Z(8). 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
      *袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴�

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CCM. 
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-CCM.

           OPEN I-O   CCM .

           CANCEL "6-CCM".
           PERFORM AFFICHAGE-ECRAN .
           INITIALIZE CCM-RECORD.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF  ECRAN-IDX = 1
           AND INDICE-ZONE < 1
              MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0163640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 4
                      MOVE 0017000000 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           END-EVALUATE.

           IF ECRAN-IDX = 2
           EVALUATE INDICE-ZONE
           WHEN 1 THRU 40 
                      MOVE 0017000005 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN OTHER MOVE 0000000005 TO EXC-KFR(1)
           END-EVALUATE.

       TRAITEMENT-ECRAN-01.

           PERFORM DISPLAY-F-KEYS.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           END-EVALUATE.
           IF ECRAN-IDX = 2
           EVALUATE INDICE-ZONE
           WHEN  1 THRU 40 PERFORM AVANT-COUT
           WHEN 41 THRU 80 PERFORM AVANT-TAUX
           END-EVALUATE.

           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 1 
              MOVE 1 TO LNK-VAL
              CALL "0-BOOK" USING LINK-V
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              GO TRAITEMENT-ECRAN
           END-IF.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           END-EVALUATE.
           IF ECRAN-IDX = 2
           EVALUATE INDICE-ZONE
           WHEN  1 THRU 40 PERFORM APRES-COUT
           WHEN 41 THRU 80 PERFORM APRES-TAUX
           END-EVALUATE.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE 1 TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF ECRAN-IDX = 1
              IF INDICE-ZONE > CHOIX-MAX(1)
                 ADD 1 TO ECRAN-IDX
                 MOVE 1 TO INDICE-ZONE 
              END-IF
           ELSE
              IF INDICE-ZONE = 0
                 SUBTRACT 1 FROM ECRAN-IDX
                 MOVE 3 TO INDICE-ZONE
              END-IF
           END-IF.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-1.
           IF REG-PERSON = 0
              INITIALIZE CCM-RECORD.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 2 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 2 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           IF CCM-ANNEE = 0 
              IF REG-ANCIEN-A < LNK-ANNEE
                 MOVE REG-ANCIEN-A TO CCM-ANNEE
                 MOVE 1 TO CCM-MOIS
              ELSE
                 MOVE REG-ANCIEN-A TO CCM-ANNEE
                 MOVE REG-ANCIEN-M TO CCM-MOIS
              END-IF
           END-IF.
           ACCEPT CCM-ANNEE
             LINE  3 POSITION 17 SIZE  4
             TAB UPDATE NO BEEP CURSOR 1 
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4. 
           ACCEPT CCM-MOIS  
             LINE  3 POSITION 33 SIZE 2 
             TAB UPDATE NO BEEP CURSOR 1 
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-COUT.
           COMPUTE IDX = INDICE-ZONE.
           COMPUTE LIN-IDX = IDX + 3
           IF IDX < 21
              MOVE 1 TO COL-IDX
           ELSE
              COMPUTE LIN-IDX = LIN-IDX - 20
              MOVE 41 TO COL-IDX
           END-IF.
           ACCEPT CCM-COUT(IDX)
           LINE  LIN-IDX POSITION COL-IDX SIZE 8
           TAB UPDATE NO BEEP CURSOR  1 
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 53
           AND CCM-COUT(IDX) = 0
              MOVE 40 TO INDICE-ZONE.

       AVANT-TAUX.
           COMPUTE IDX = INDICE-ZONE - 40.
              COMPUTE LIN-IDX = IDX + 3
           IF IDX < 21
              MOVE 35 TO COL-IDX
           ELSE
              COMPUTE LIN-IDX = LIN-IDX - 20
              MOVE 75 TO COL-IDX
           END-IF.
           IF CCM-COUT(IDX) NOT = 0
              ACCEPT CCM-TAUX(IDX)
              LINE LIN-IDX POSITION COL-IDX SIZE 4
              TAB UPDATE NO BEEP CURSOR 1 
              ON EXCEPTION EXC-KEY CONTINUE
           ELSE 
              SUBTRACT 2 FROM INDICE-ZONE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  OTHER PERFORM NEXT-REGIS.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM CCM-RECENT.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-2.
           MOVE "A" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN 65 THRU 66 PERFORM NEXT-REGIS.
           PERFORM DIS-HE-01 THRU DIS-HE-02.
           IF REG-PERSON > 0 
              PERFORM CONTRAT-RECENT
              PERFORM CCM-RECENT
              PERFORM AFFICHAGE-DETAIL
           END-IF.

       APRES-3.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 
                   MOVE EXC-KEY TO SAVE-KEY
                   PERFORM CCM-NEXT
                   PERFORM AFFICHAGE-DETAIL
                   MOVE 1 TO INPUT-ERROR
           WHEN  8 PERFORM DEL-CCM
           WHEN  2 CALL "2-CCM" USING LINK-V CCM-RECORD PR-RECORD
                   REG-RECORD 
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           END-EVALUATE.
           IF REG-ANCIEN-A > CCM-ANNEE
              MOVE 1 TO INPUT-ERROR
              MOVE REG-ANCIEN-A TO CCM-ANNEE.

       APRES-4.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 
                   MOVE EXC-KEY TO SAVE-KEY
                   PERFORM CCM-NEXT
                   MOVE 1 TO INPUT-ERROR
           WHEN  2 CALL "2-CCM" USING LINK-V CCM-RECORD PR-RECORD
                   REG-RECORD 
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN  8 PERFORM DEL-CCM
           END-EVALUATE.
           IF REG-ANCIEN-A = CCM-ANNEE
           IF REG-ANCIEN-M > CCM-MOIS 
              MOVE 1 TO INPUT-ERROR
              MOVE REG-ANCIEN-M TO CCM-MOIS.
           IF CCM-MOIS > 12
              MOVE 12 TO CCM-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF CCM-MOIS < 1
              MOVE 1 TO CCM-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0
              READ CCM INVALID 
                MOVE "AA" TO LNK-AREA
                MOVE 13 TO LNK-NUM
                PERFORM DISPLAY-MESSAGE
              END-READ
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-COUT.
           COMPUTE IDX = INDICE-ZONE.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 
                MOVE EXC-KEY TO SAVE-KEY
                MOVE CCM-COUT(IDX) TO COUT-NUMBER 
                PERFORM NEXT-COUT
                MOVE COUT-NUMBER TO CCM-COUT(IDX)
           WHEN   5 PERFORM SAVE-CCM
                    PERFORM AFFICHAGE-DETAIL
                    MOVE 1 TO INDICE-ZONE ECRAN-IDX
           WHEN   2 CALL "2-COUT" USING LINK-V COUT-RECORD
                    MOVE COUT-NUMBER TO CCM-COUT(IDX) 
                    CANCEL "2-COUT"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.
           IF INDICE-ZONE > 1
           AND CCM-COUT(IDX) > 0
           AND EXC-KEY NOT = 65
           AND EXC-KEY NOT = 66
              PERFORM DA-CAPO VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 
              = INDICE-ZONE
           END-IF.
           IF  CCM-COUT(IDX) = 0
           AND IDX = 1
               MOVE 1 TO INPUT-ERROR
           END-IF.
           COMPUTE IDX = INDICE-ZONE.
           PERFORM DIS-COUT.
           IF  CCM-COUT(IDX) > 0
           AND COUT-NUMBER = 0
               MOVE 1 TO INPUT-ERROR.
           IF  CCM-COUT(IDX) > 0
           AND CCM-TAUX(IDX) = 0
           AND INPUT-ERROR   = 0
               MOVE 1 TO CCM-TAUX(IDX)
           END-IF.

       DA-CAPO.
           IF CCM-COUT(INDICE-ZONE) = CCM-COUT(IDX-1)  
              MOVE 0 TO CCM-COUT(INDICE-ZONE)
              MOVE 1 TO INPUT-ERROR
           END-IF.

       DOUBLON.
           IF IDX-1 NOT = IDX
              IF CCM-COUT(IDX) = CCM-COUT(IDX-1)  
                 MOVE 0 TO CCM-COUT(IDX-1)
                 MOVE IDX TO IDX-2
                 MOVE IDX-1 TO IDX
                 PERFORM DIS-COUT
                 MOVE IDX-2 TO IDX
              END-IF
           END-IF.

       REMPLACE.
           COMPUTE IDX-2 = IDX + 1.
           IF IDX < 40
              MOVE CCM-COUT(IDX-2) TO CCM-COUT(IDX)
              MOVE CCM-TAUX(IDX-2) TO CCM-TAUX(IDX)
              MOVE 0 TO CCM-COUT(IDX-2) CCM-TAUX(IDX-2)
           ELSE
              MOVE 0 TO CCM-COUT(IDX) CCM-TAUX(IDX)
           END-IF
           PERFORM DIS-COUT.

       APRES-TAUX.
           COMPUTE IDX = INDICE-ZONE - 40.
           IF  CCM-COUT(IDX) > 0
           AND CCM-TAUX(IDX) = 0
               MOVE 1 TO CCM-TAUX(IDX) INPUT-ERROR.
           EVALUATE EXC-KEY
           WHEN   5 IF INPUT-ERROR = 0
                       PERFORM SAVE-CCM
                       PERFORM AFFICHAGE-DETAIL
                       MOVE 1 TO INDICE-ZONE ECRAN-IDX
                    END-IF
           END-EVALUATE.
           COMPUTE IDX = INDICE-ZONE - 40.
           PERFORM DIS-TAUX.
           IF IDX = 1
           AND EXC-KEY = 27
              COMPUTE INDICE-ZONE = 1 + CCM-OCCURS
           END-IF.

       SAVE-CCM.
           MOVE FR-KEY TO CCM-FIRME.
           MOVE REG-PERSON TO CCM-PERSON.
           MOVE 0 TO CCM-TOTAL CCM-OCCURS IDX-1.
           PERFORM TEST-VIDE VARYING IDX FROM 1 BY 1 UNTIL IDX > 40.
           IF CCM-OCCURS = 0 
              PERFORM DEL-CCM
           ELSE
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-TIME TO CCM-TIME
              MOVE LNK-USER TO CCM-USER
              WRITE CCM-RECORD INVALID REWRITE CCM-RECORD END-WRITE 
              MOVE "X" TO FR-CCM
              CALL "6-FIRME" USING LINK-V "N" WR-KEY
           END-IF.

       DEL-CCM.
           DELETE CCM INVALID CONTINUE END-DELETE.
           MOVE 1  TO DECISION.
           PERFORM CCM-RECENT.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       CCM-RECENT.
           INITIALIZE CCM-RECORD.
           MOVE 9999 TO CCM-ANNEE.
           PERFORM CCM-NEXT.

       CCM-NEXT.
           CALL "6-CCM" USING LINK-V CCM-RECORD REG-RECORD NUL-KEY.
             
       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
                
       ENTREE-SORTIE.
           MOVE "AA" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           PERFORM DISPLAY-MESSAGE.
           MOVE 0 TO LNK-NUM.

       TEST-VIDE.
           IF IDX-1 > 0 
              MOVE 0 TO CCM-COUT(IDX) CCM-TAUX(IDX) 
           END-IF.
           IF  CCM-COUT(IDX) > 0 
           AND CCM-TAUX(IDX) > 0 
              ADD 1 TO CCM-OCCURS
              ADD CCM-TAUX(IDX) TO CCM-TOTAL
           ELSE
              MOVE 0 TO CCM-COUT(IDX) CCM-TAUX(IDX) 
              MOVE 1 TO IDX-1
           END-IF.

       NEXT-COUT.
           CALL "6-COUT" USING LINK-V COUT-RECORD SAVE-KEY.
           IF FR-KEY NOT = COUT-FIRME
              MOVE 1 TO NOT-FOUND.
           
       CONTRAT-RECENT.
           INITIALIZE CON-RECORD.
           CALL "1-GCONTR" USING LINK-V REG-RECORD CON-RECORD.
           
       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           IF REG-PERSON NOT = 0
              MOVE REG-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 LINE 2 POSITION 15.
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 2 POSITION 44 SIZE 35.

       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 2 POSITION 33.

       DIS-HE-03.
           DISPLAY CCM-ANNEE LINE 3 POSITION 17.
       DIS-HE-04.
           MOVE CCM-MOIS TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 3 POSITION 33.
           MOVE "MO" TO LNK-AREA.
           MOVE 03441200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-COUT.
           PERFORM DIS-COUT VARYING IDX FROM 1 BY 1 UNTIL IDX > 40.
       DIS-HE-TAUX.
           PERFORM DIS-TAUX VARYING IDX FROM 1 BY 1 UNTIL IDX > 40.
       DIS-HE-END.
           EXIT.

       DIS-COUT.
           MOVE CCM-COUT(IDX) TO HE-Z8.
           IF IDX < 21
              COMPUTE LIN-IDX = IDX + 3
              MOVE 1 TO COL-IDX
           ELSE
              COMPUTE LIN-IDX = IDX + 3 - 20
              MOVE 41 TO COL-IDX
           END-IF.
           DISPLAY HE-Z8 LINE LIN-IDX POSITION COL-IDX.
           ADD 10 TO COL-IDX.
           IF CCM-COUT(IDX) NOT = 0
              MOVE FR-KEY TO COUT-FIRME 
              MOVE CCM-COUT(IDX) TO COUT-NUMBER
              CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY
           ELSE
              INITIALIZE COUT-RECORD
           END-IF.
           DISPLAY COUT-NOM LINE LIN-IDX POSITION COL-IDX SIZE 20.

       DIS-TAUX. 
           IF IDX < 21
              COMPUTE LIN-IDX = IDX + 3
              MOVE 35 TO COL-IDX
           ELSE
              COMPUTE LIN-IDX = IDX + 3 - 20
              MOVE 75 TO COL-IDX
           END-IF.
           MOVE CCM-TAUX(IDX) TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION COL-IDX.

       AFFICHAGE-ECRAN.
           MOVE  171 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE CCM.
           CANCEL "2-CCM".
           CANCEL "2-COUT".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".
           
