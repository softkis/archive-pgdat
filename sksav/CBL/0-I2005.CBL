      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-I2005 CALCUL DES IMPOTS - SALAIRES        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-I2005.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CONST-B.
           02 B-02         PIC 9(5)V99 VALUE  78,44.
           02 B-03         PIC 9(5)V99 VALUE 100,80.
           02 B-04         PIC 9(5)V99 VALUE 125,91.
           02 B-05         PIC 9(5)V99 VALUE 153,77.
           02 B-06         PIC 9(5)V99 VALUE 184,38.
           02 B-07         PIC 9(5)V99 VALUE 217,74.
           02 B-08         PIC 9(5)V99 VALUE 253,85.
           02 B-09         PIC 9(5)V99 VALUE 292,71.
           02 B-10         PIC 9(5)V99 VALUE 334,32.
           02 B-11         PIC 9(5)V99 VALUE 378,68.
           02 B-12         PIC 9(5)V99 VALUE 425,79.
           02 B-13         PIC 9(5)V99 VALUE 475,65.
           02 B-14         PIC 9(5)V99 VALUE 528,26.
           02 B-15         PIC 9(5)V99 VALUE 583,62.
           02 B-16         PIC 9(5)V99 VALUE 641,73.
           02 B-17         PIC 9(5)V99 VALUE 702,59.

       01  CONST-B-R REDEFINES CONST-B.
           02 B-IDX        PIC 9(5)V99  OCCURS 16.

       01  CONST-R.
           02 R-01         PIC 9(5)V99 VALUE   980,5.
           02 R-02         PIC 9(5)V99 VALUE  1118.
           02 R-03         PIC 9(5)V99 VALUE  1255,5.
           02 R-04         PIC 9(5)V99 VALUE  1393.
           02 R-05         PIC 9(5)V99 VALUE  1530,5.
           02 R-06         PIC 9(5)V99 VALUE  1668.
           02 R-07         PIC 9(5)V99 VALUE  1805,5.
           02 R-08         PIC 9(5)V99 VALUE  1943.
           02 R-09         PIC 9(5)V99 VALUE  2080,5.
           02 R-10         PIC 9(5)V99 VALUE  2218.
           02 R-11         PIC 9(5)V99 VALUE  2355,5.
           02 R-12         PIC 9(5)V99 VALUE  2493.
           02 R-13         PIC 9(5)V99 VALUE  2630,5.
           02 R-14         PIC 9(5)V99 VALUE  2768.
           02 R-15         PIC 9(5)V99 VALUE  2905,5.
           02 R-16         PIC 9(5)V99 VALUE  3043.

       01  CONST-R-R REDEFINES CONST-R.
           02 R-VALEUR     PIC 9(5)V99  OCCURS 16.


      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  IDX             PIC 99.
       01  R-IDX           PIC 99.
       01  R-PRIM-VAL      PIC 9(8)V99 COMP-3.
       01  A-VAL           PIC 9V99.
       01  MULTIPLE-INF    PIC 99.
       01  R-VAL           PIC 9(12)V9999.
       01  R-VAL-R1 REDEFINES R-VAL.
           02 R-VAL-01     PIC 9(12)V9.
           02 R-VAL-02     PIC 999.
       01  HELP-01         PIC S9(10)V9999 COMP-3.
       01  HELP-02         PIC S9(6)V99.
       01  HELP-3          PIC 9(9).
       01  C-HELP-1        PIC 9(8)V99   COMP-3.
       01  P-HELP          PIC 9(5)   COMP-3.
       01  R-HELP          PIC 9(5)V99   COMP-3.
       01  SH-00           PIC 9(5)V99   COMP-3.
       01  ACTION          PIC X.

       LINKAGE SECTION.
      *----------------
       01 DONNEES-BASE.
           02 IMPOSABLE       PIC 9(8)V99.
           02 JOURS-IMPOSABLE PIC 999.
           02 IMPOT           PIC 9(8)V9.
           02 CLASSE-IMPOT.
              03 CLASSE    PIC 9.
              03 GROUPE    PIC X.
              03 ENFANTS   PIC 99.
           02 REVENU       PIC X.

       PROCEDURE DIVISION USING DONNEES-BASE. 

       START-CALC SECTION.
             
       START-PROGRAMME-0-I2005.
                     
           MOVE 0 TO IMPOT C-HELP-1.
           MOVE IMPOSABLE TO R-VAL.
 
           IF JOURS-IMPOSABLE < 27
              COMPUTE R-VAL = IMPOSABLE * 25 / JOURS-IMPOSABLE
              MOVE 5 TO MULTIPLE-INF
              MOVE 1 TO IDX
           ELSE
              IF JOURS-IMPOSABLE < 300          
                 COMPUTE R-VAL = IMPOSABLE * 300 / JOURS-IMPOSABLE
              END-IF
              MOVE 50 TO MULTIPLE-INF
              MOVE 12 TO IDX
           END-IF.
           DIVIDE R-VAL BY MULTIPLE-INF GIVING HELP-3 REMAINDER SH-00.
           COMPUTE R-VAL = HELP-3 * MULTIPLE-INF / IDX.

           IF R-VAL <= R-VALEUR(1)
              EXIT PROGRAM.
         

           COMPUTE P-HELP = 75 * ENFANTS.
                
           IF CLASSE = 1 AND GROUPE = SPACES
              MOVE R-VAL TO R-PRIM-VAL
              PERFORM ECHELON THRU ECHELON-END 
              COMPUTE HELP-01 = (R-VAL * A-VAL) - B-IDX(R-IDX)
              GO ARRONDI
           END-IF.

           IF GROUPE = "A" AND R-VAL >= 2620
              COMPUTE HELP-01 = R-VAL * ,38 - 796,84 
              GO ARRONDI
           END-IF.

           IF CLASSE = 1
              MOVE 1709 TO C-HELP-1 
              COMPUTE R-PRIM-VAL = R-VAL * 1,5 - C-HELP-1 
              PERFORM ECHELON THRU ECHELON-END
              COMPUTE HELP-01 = ((R-VAL * 1,5 - C-HELP-1)
                * A-VAL) - B-IDX(R-IDX) 
              GO ARRONDI
           END-IF.

      *  Classe 2

           MOVE 168 TO C-HELP-1.
           COMPUTE R-PRIM-VAL = (R-VAL + C-HELP-1 ) / 2.
           PERFORM ECHELON THRU ECHELON-END.
           COMPUTE HELP-01 = ((R-VAL + C-HELP-1) * A-VAL) 
                           - (B-IDX(R-IDX) * 2).
           GO ARRONDI.

       ECHELON.
           MOVE 0 TO R-IDX
           IF R-PRIM-VAL < R-VALEUR(1)
              EXIT PROGRAM
           END-IF.
           IF R-PRIM-VAL > R-VALEUR(16)
              MOVE 17 TO R-IDX
              GO ECHELON-END
           END-IF.
       ECHELON-1.
           ADD 1 TO R-IDX.
           IF R-IDX = 17 
              GO ECHELON-END
           END-IF.
           IF R-PRIM-VAL > R-VALEUR(R-IDX)
              GO ECHELON-1
           END-IF.
       ECHELON-END.
           SUBTRACT 1 FROM R-IDX.
           COMPUTE A-VAL = ,06 + (R-IDX * ,02).

       ARRONDI.
           COMPUTE HELP-01 = HELP-01 - P-HELP.
           IF HELP-01 < 0 MOVE 0 TO HELP-01.
           IF JOURS-IMPOSABLE = 300
              COMPUTE HELP-3 = HELP-01 * 12 
      *       COMPUTE HELP-3 = HELP-3 * 12 
              COMPUTE HELP-3 = HELP-3 * 1,025 
              COMPUTE IMPOT  = HELP-3
              IF IMPOT < 12
                 MOVE 0 TO IMPOT
              END-IF
              EXIT PROGRAM
           END-IF.     
           IF JOURS-IMPOSABLE = 25
              MOVE HELP-01 TO IMPOT
              COMPUTE IMPOT = IMPOT * 1,025
              IF IMPOT < 1
                 MOVE 0 TO IMPOT
              END-IF
              EXIT PROGRAM
           END-IF.     

           COMPUTE HELP-02 = HELP-01 / 25.
           COMPUTE HELP-02 = HELP-02 * 1,025.
           COMPUTE HELP-02 = HELP-02 * JOURS-IMPOSABLE.
           COMPUTE SH-00 = JOURS-IMPOSABLE * ,04.
           IF HELP-02 < SH-00
              MOVE 0 TO HELP-02
           END-IF.     
           MOVE HELP-02 TO IMPOT.
           EXIT PROGRAM.
           