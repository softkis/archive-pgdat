      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-CODFIX CONTROLE + EFFACEMENT CODES FIXES  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  2-CODFIX.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODFIX.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CODFIX.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  ARROW                 PIC X VALUE ">".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "V-VAR.CPY".
SU         COPY "CSDEF.REC".
SU    *    COPY "CSDET.REC".
SU         COPY "CODTXT.REC".

       01  CSF-IX               PIC 99 COMP-1.

       01  ECR-DISPLAY.
            02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
            02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
            02 HE-Z4Z2 PIC Z(4),ZZ BLANK WHEN ZERO.
            02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.
            02 HE-DATE .
               03 HE-JJ PIC ZZ.
               03 FILLER PIC X VALUE ".".
               03 HE-MM PIC ZZ.
               03 FILLER PIC X VALUE ".".
               03 HE-AA PIC 99.

       01 HE-CSF.
          02 HE-CSF-RECORD OCCURS 30.
             03 HE-CSF-FIRME             PIC 9(6).
             03 HE-CSF-PERSON            PIC 9(8).
             03 HE-CSF-CODE              PIC 9(4).
             03 HE-CSF-DATE-DEBUT.
                04 HE-CSF-ANNEE-D        PIC 9999.
                04 HE-CSF-MOIS-D         PIC 99.
             03 HE-CSF-DATE-FIN.
                04 HE-CSF-ANNEE-F        PIC 9999.
                04 HE-CSF-MOIS-F         PIC 99.
             03 HE-CSF-PERIODE           PIC 9(12).
             03 HE-CSF-STATUT            PIC 9.
             03 HE-CSF-VALEURS.
                04 HE-CSF-DONNEE-1   PIC 9(8)V9(6).
                04 HE-CSF-DONNEE-2   PIC 9(8)V9(6).
                04 HE-CSF-POURCENT   PIC 9(8)V9(6).
                04 HE-CSF-UNITE      PIC 9(8)V9(6).
                04 HE-CSF-UNITAIRE   PIC 9(8)V9(6).
                04 HE-CSF-TOTAL      PIC 9(8)V9(6).
                04 HE-CSF-ANNEXE-1   PIC 9(8)V9(6).
                04 HE-CSF-ANNEXE-2   PIC 9(8)V9(6).
             03 HE-CSF-INDEXES.
                04 HE-CSF-IDX        PIC 9(5)V9(6) OCCURS 8.
             03 HE-CSF-TEXTE         PIC X(30).
             03 HE-CSF-REDEFINE      PIC X.
             03 HE-CSF-CUMULS.
                04 HE-CSF-CUMUL      PIC X OCCURS 8.
             03 HE-CSF-FILLER        PIC X(100).
             03 HE-CSF-STAMP.
                04 HE-CSF-TIME.
                   05 HE-CSF-ST-ANNEE PIC 9999.
                   05 HE-CSF-ST-MOIS  PIC 99.
                   05 HE-CSF-ST-JOUR  PIC 99.
                   05 HE-CSF-ST-HEURE PIC 99.
                   05 HE-CSF-ST-MIN   PIC 99.
                04 HE-CSF-USER        PIC X(10).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CODFIX.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-CODFIX .

           CALL "0-TODAY" USING TODAY.
       
           OPEN I-O   CODFIX.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 


           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.


      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
              WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN OTHER MOVE 0000080000 TO EXC-KFR (2).
           IF INDICE-ZONE = CHOIX-MAX
                      MOVE 0052000000 TO EXC-KFR (11).


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  OTHER PERFORM AVANT-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  OTHER PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           MOVE FR-KEY TO REG-FIRME.
           IF LNK-PERSON NOT = 0 
               MOVE LNK-PERSON TO REG-PERSON 
               MOVE 13 TO EXC-KEY
               MOVE 0 TO LNK-PERSON
           ELSE    
           ACCEPT REG-PERSON 
             LINE  3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT REG-MATCHCODE
             LINE  3 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PAGE.
           MOVE 0000000000 TO EXC-KFR(1) EXC-KFR(2) 
           MOVE 0000680000 TO EXC-KFR(14) 
           PERFORM DISPLAY-F-KEYS
           ACCEPT ACTION 
             LINE  24 POSITION 70 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
           COMPUTE CSF-IX = INDICE-ZONE - 2.
           COMPUTE LIN-IDX = CSF-IX + 6.
           MOVE ">" TO ARROW.
           ACCEPT ARROW
             LINE  LIN-IDX POSITION 1 SIZE 1
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 1.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE FR-KEY TO REG-FIRME
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-HE-01 THRU DIS-HE-02
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE
              IF PRES-TOT(LNK-MOIS) = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           PERFORM DIS-HE-01 THRU DIS-HE-02.
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-NUM LNK-PERSON
           ELSE
              IF REG-PERSON NOT = 0
              AND EXC-KEY NOT = 53
                 PERFORM TOTAL-CODFIX  THRU TOTAL-CODFIX-END.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   PERFORM DIS-HE-01 THRU DIS-HE-02
                   MOVE 6 TO LIN-IDX
                   PERFORM TOTAL-CODFIX  THRU TOTAL-CODFIX-END
           END-EVALUATE.                     
           
       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-END.

           
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           MOVE HE-CSF-RECORD(CSF-IX) TO CSF-RECORD.
           EVALUATE EXC-KEY 
            WHEN  8 MOVE 2 TO DECISION
                    IF LNK-SQL = "Y" 
                       CALL "9-CODFIX" USING LINK-V CSF-RECORD DEL-KEY 
                    END-IF
                    DELETE CODFIX INVALID CONTINUE END-DELETE
                    PERFORM TOTAL-CODFIX THRU TOTAL-CODFIX-END
             END-EVALUATE.


      *    HISTORIQUE DES CODFIXS
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       TOTAL-CODFIX.
           MOVE 0 to  NOT-FOUND.
           MOVE 6 TO LIN-IDX.
           INITIALIZE CSF-RECORD CSF-IX.
           MOVE REG-FIRME  TO CSF-FIRME.
           MOVE REG-PERSON TO CSF-PERSON.
           START CODFIX KEY >= CSF-KEY INVALID 
                MOVE 1 TO NOT-FOUND.
           PERFORM READ-CSF THRU READ-CSF-END.
           COMPUTE CHOIX-MAX = 2 + CSF-IX.
       TOTAL-CODFIX-END.

       READ-CSF.
           IF NOT-FOUND = 1
              GO READ-CSF-END.
           READ CODFIX NEXT AT END 
              GO READ-CSF-END.
           IF FR-KEY     NOT = CSF-FIRME
           OR REG-PERSON NOT = CSF-PERSON
              GO READ-CSF-END.
           ADD 1 TO CSF-IX.
           MOVE CSF-RECORD TO HE-CSF-RECORD(CSF-IX).
           PERFORM DIS-HIS-LIGNE.
           IF LIN-IDX > 21
              ADD 1 TO INDICE-ZONE
              PERFORM AVANT-PAGE
              IF EXC-KEY NOT = 68
                 GO READ-CSF-END
              ELSE
                 INITIALIZE HE-CSF CSF-IX
                 PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM 7
                                   BY 1 UNTIL LIN-IDX > 24
                 MOVE 6 TO LIN-IDX
                 MOVE 0 TO INDICE-ZONE
              END-IF
           END-IF.
           GO READ-CSF.
       READ-CSF-END.
           ADD 1 TO LIN-IDX .
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 24.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       DIS-HIS-LIGNE.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES  LINE  LIN-IDX POSITION  1 SIZE 80.
           MOVE CSF-CODE   TO HE-Z4 CD-NUMBER.
           CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY.
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
SU    *    CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
           DISPLAY HE-Z4       LINE  LIN-IDX POSITION  2.
           IF CSF-TEXTE = SPACES
               DISPLAY CTX-NOM LINE LIN-IDX POSITION 7
           ELSE
               DISPLAY CSF-TEXTE LINE LIN-IDX POSITION 7
           END-IF.
           MOVE CSF-DONNEE-1 TO HE-Z6Z2.
           DISPLAY HE-Z6Z2   LINE  LIN-IDX POSITION 30.
           MOVE CSF-DONNEE-2 TO HE-Z6Z2
           DISPLAY HE-Z6Z2   LINE  LIN-IDX POSITION 39.
           MOVE CSF-POURCENT TO HE-Z4Z2.
           DISPLAY HE-Z4Z2   LINE  LIN-IDX POSITION 48.
           MOVE CSF-UNITE    TO HE-Z4Z2.
           DISPLAY HE-Z4Z2   LINE  LIN-IDX POSITION 55.
           MOVE CSF-UNITAIRE TO HE-Z4Z2.
           DISPLAY HE-Z4Z2   LINE  LIN-IDX POSITION 63.
           MOVE CSF-TOTAL    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2   LINE  LIN-IDX POSITION 71.


       AFFICHAGE-ECRAN.
           MOVE 2210 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.


       END-PROGRAM.
           CLOSE CODFIX.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


