      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-DEMAT DEMANDE NUMERO IMMATRICULATION      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-DEMAT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".
           COPY "FICHE.REC".
           COPY "PAYS.REC".
           COPY "DIM.LNK".

       01  COMPTEUR              PIC 99 COMP-1.


           COPY "V-VH00.CPY".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".


       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-X8 PIC 9(8).
           02 HE-XX REDEFINES HE-X8.
              03 HE-XA PIC 9.
              03 HE-XB PIC 9.
              03 HE-XC PIC 9.
              03 HE-XD PIC 9.
              03 HE-XE PIC 9.
              03 HE-XF PIC 9.
              03 HE-XG PIC 9.
              03 HE-XH PIC 9.
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.


           02 HE-SNOCS.
              03 HE-A PIC ZZZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HE-M PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HE-J PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HE-S PIC ZZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-DEMAT.
       
           MOVE LNK-LANGUAGE TO SAVE-LANGUAGE.
           CALL "0-TODAY" USING TODAY.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE "N" TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM START-PERSON
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE LNK-LANGUAGE TO SAVE-LANGUAGE.
           MOVE "F" TO LNK-LANGUAGE.
           
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           PERFORM READ-PERSON THRU READ-EXIT.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF  CAR-STATUT = 1
           AND CAR-REGIME = 0
              GO READ-PERSON
           END-IF.

           PERFORM DIS-HE-01.
           IF REG-SNOCS > 9
              GO READ-PERSON
           END-IF.
           PERFORM FILL-FILES.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.
           EXIT.


       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       FILL-FILES.
           IF COUNTER = 0 
              MOVE 1 TO COUNTER 
           END-IF.
           INITIALIZE LINK-RECORD FICHE-RECORD.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD NUL-KEY.

           MOVE PR-NOM TO LINK-NOMSAL.
           MOVE PR-PRENOM TO LINK-PRENSAL.
      *    MOVE PR-MATRICULE TO LINK-CONTMAT.

           MOVE FR-NOM TO  LINK-NOMEMPL.
           MOVE FR-LOCALITE TO  LINK-LOCAEMPL LINK-LIEU.
           MOVE 1 TO IDX.
           STRING FR-MAISON DELIMITED BY "  " INTO LINK-RUEEMPL
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-RUE DELIMITED BY SIZE INTO LINK-RUEEMPL
           WITH POINTER IDX.

           MOVE 1 TO IDX.
           STRING FR-PAYS DELIMITED BY " " INTO LINK-CPOSEMPL
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-CP-LUX DELIMITED BY SIZE INTO LINK-CPOSEMPL
           WITH POINTER IDX.

           MOVE 1 TO IDX.
           STRING PR-PAYS DELIMITED BY " "  INTO LINK-CPOSSAL
           WITH POINTER IDX.
           ADD 1 TO IDX.
           IF PR-PAYS = "D" OR "F" OR "CZ" OR "I" OR "SK" OR "PL"
              STRING PR-CP5 DELIMITED BY " "  INTO LINK-CPOSSAL
              WITH POINTER IDX
           ELSE
              STRING PR-CP4 DELIMITED BY " "  INTO LINK-CPOSSAL
              WITH POINTER IDX
           END-IF.
           MOVE PR-LOCALITE TO LINK-LOCASAL.


           CALL "4-PRRUE" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO LINK-RUESAL.

           IF PR-CODE-SEXE = 1 
              MOVE "M" TO LINK-SEXE
           ELSE
              MOVE "F" TO LINK-SEXE
           END-IF.
           MOVE PR-NATIONALITE TO LINK-NATSAL.
           MOVE FICHE-ETAT-CIVIL TO LINK-ETACIV.
           MOVE PR-DOC-ID TO LINK-IDNUMSAL.
           MOVE PR-LIEU-NAISSANCE TO LINK-LIEUNAISS.
           MOVE PR-NAISS-A  TO HE-AA.
           MOVE PR-NAISS-M  TO HE-MM.
           MOVE PR-NAISS-J  TO HE-JJ.
           MOVE HE-DATE TO LINK-DATENAISS.
           MOVE TODAY-JOUR  TO HE-JJ.
           MOVE TODAY-MOIS  TO HE-MM.
           MOVE TODAY-ANNEE TO HE-AA.
           MOVE HE-DATE TO LINK-DATE.
              
           CALL "DEMAT" USING LINK-V LINK-RECORD.
           ADD 1 TO COUNTER.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           MOVE 27 TO COL-IDX.
           IF A-N = "A"
              ADD 11 TO COL-IDX
              DISPLAY SPACES LINE 6 POSITION 37 SIZE 1.
           COMPUTE IDX = 80 - COL-IDX.
           MOVE 0 TO SIZ-IDX IDX-1.
           INSPECT PR-NOM TALLYING  SIZ-IDX FOR CHARACTERS BEFORE "  ".
           INSPECT PR-NOM-JF TALLYING IDX-1 FOR CHARACTERS BEFORE "  ".

           IF FR-NOM-JF = SPACES
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX  = COL-IDX + SIZ-IDX + 1
              IF PR-NOM-JF NOT = SPACES
                 COMPUTE IDX = 80 - COL-IDX
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
           ELSE
              IF PR-NOM-JF NOT = SPACES
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX = COL-IDX + SIZ-IDX + 1
           END-IF.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 6 POSITION COL-IDX SIZE IDX LOW.
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 1218 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.


       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "DEMAT" USING LINK-V LINK-RECORD.
           CANCEL "DEMAT".
           MOVE SAVE-LANGUAGE TO LNK-LANGUAGE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".
           COPY "XMOISNOM.CPY".

