      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-CAR LISTE CARRIERE PERSONNELLE            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CAR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM215.FC".
           COPY "CARRIERE.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM215.FDE".
           COPY "CARRIERE.FDE".
       FD  TF-TRANS
           RECORD VARYING DEPENDING ON IDX-4
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 600 DEPENDING IDX-4.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 8.
       01  JOB-STANDARD          PIC X(10) VALUE "215       ".
       01  PRECISION             PIC 9 VALUE 1.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CCOL.REC".
           COPY "BAREME.REC".
           COPY "BARDEF.REC".
           COPY "POINTS.REC".
           COPY "POCL.REC".
           COPY "INDEX.REC".
           COPY "METIER.REC".
           COPY "EQUIPE.REC".
           COPY "COUT.REC".
           COPY "IMPRLOG.REC".
           COPY "V-BASES.REC".
           COPY "POSE.REC".
           COPY "HORSEM.REC".
           COPY "STATUT.REC".
           COPY "PARMOD.REC".

       01  LANGUE                PIC 9.
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAL-ACT               PIC 9(7)V9(5).
       01  FEUILLE               PIC X VALUE "N".
       01  AUTEUR                PIC X VALUE "N".
       01  ASCII                 PIC 9 VALUE 0.
       01  NOT-OPEN              PIC 9 VALUE 0.

       01  T1-RECORD.
           02 T1-A.
              03 T1-FIRME     PIC 9(6).
              03 T1-DELIM0    PIC X VALUE ";".
              03 T1-PERSON    PIC 9(8).
              03 T1-DELIM1    PIC X VALUE ";".
              03 T1-NOM       PIC X(40).
              03 T1-DELIM2    PIC X VALUE ";".
              03 T1-ANNEE     PIC ZZZZZ.
              03 T1-DELIM3    PIC X VALUE ";".
              03 T1-MOIS      PIC ZZZZZ.
              03 T1-DELIM4    PIC X VALUE ";".
              03 T1-METIER    PIC X(10).
              03 T1-DELIM5    PIC X VALUE ";".
              03 T1-MET-NOM   PIC X(50).
              03 T1-DELIM6    PIC X VALUE ";".
              03 T1-POSITION  PIC X(50).
              03 T1-DELIM6    PIC X VALUE ";".
              03 T1-COUT      PIC Z(10).
              03 T1-DELIM7    PIC X VALUE ";".
              03 T1-COUT-NOM  PIC X(50).
              03 T1-DELIM8    PIC X VALUE ";".

              03 T1-INDEX       PIC ZZZZZ,ZZZZ.
              03 T1-DELIM13     PIC X VALUE ";".
              03 T1-INDEX-100   PIC ZZZZZ,ZZZZ.
              03 T1-DELIM13     PIC X VALUE ";".
              03 T1-INDEX-HIST  PIC ZZZZZ,ZZZZ.
              03 T1-DELIM14     PIC X VALUE ";".
              03 T1-INDEX-ACT   PIC ZZZZZ,ZZZZ.
              03 T1-DELIM14     PIC X VALUE ";".
              03 T1-GRADE      PIC X(10).
              03 T1-DELIM15    PIC X VALUE ";".
              03 T1-ECHELON    PIC Z(10).
              03 T1-DELIM16    PIC X VALUE ";".

              03 T1-EQUIPE    PIC Z(10).
              03 T1-DELIM9    PIC X VALUE ";".
              03 T1-EQ-NOM    PIC X(50).
              03 T1-DELIM10   PIC X VALUE ";".
              03 T1-POSTE     PIC Z(10).
              03 T1-DELIM11   PIC X VALUE ";".
              03 T1-PF-NOM    PIC X(50).
              03 T1-DELIM12   PIC X VALUE ";".

       01  TXT-RECORD.
           02 TXT-A.
              03 TXT-FIRME   PIC X(6).
              03 TXT-DELIM0  PIC X VALUE ";".
              03 TXT-PERSON  PIC X(8).
              03 TXT-DELIM1  PIC X VALUE ";".
              03 TXT-NOM     PIC X(40).
              03 TXT-DELIM2  PIC X VALUE ";".
              03 TXT-ANNEE   PIC XXXXX.
              03 TXT-DELIM3  PIC X VALUE ";".
              03 TXT-MOIS    PIC XXXXX.
              03 TXT-DELIM4  PIC X VALUE ";".
              03 TXT-METIER  PIC X(10).
              03 TXT-DELIM5  PIC X VALUE ";".
              03 TXT-MET-NOM PIC X(50).
              03 TXT-DELIM6  PIC X VALUE ";".
              03 TXT-POSITION  PIC X(50).
              03 TXT-DELIM6    PIC X VALUE ";".

              03 TXT-COUT    PIC X(10).
              03 TXT-DELIM7  PIC X VALUE ";".
              03 TXT-COUT-NOM PIC X(50).
              03 TXT-DELIM8  PIC X VALUE ";".

              03 TXT-INDEX      PIC X(10) VALUE "INDEX".
              03 TXT-DELIM13    PIC X VALUE ";".
              03 TXT-INDEX-100  PIC X(10).
              03 TXT-DELIM13    PIC X VALUE ";".
              03 TXT-INDEX-HIST PIC X(10).
              03 TXT-DELIM14    PIC X VALUE ";".
              03 TXT-INDEX-ACT  PIC X(10).
              03 TXT-DELIM14    PIC X VALUE ";".
              03 TXT-GRADE      PIC X(10).
              03 TXT-DELIM15    PIC X VALUE ";".
              03 TXT-ECHELON    PIC X(10).
              03 TXT-DELIM16    PIC X VALUE ";".
              03 TXT-EQUIPE  PIC X(10).
              03 TXT-DELIM9  PIC X VALUE ";".
              03 TXT-EQ-NOM  PIC X(50).
              03 TXT-DELIM10 PIC X VALUE ";".
              03 TXT-POSTE   PIC X(10).
              03 TXT-DELIM11 PIC X VALUE ";".
              03 TXT-PF-NOM  PIC X(50).
              03 TXT-DELIM12 PIC X VALUE ";".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(215) OCCURS 45.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(5) VALUE "FORME".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".CAR".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z6Z2 PIC Z(6),ZZ.
           02 HE-Z6Z4 PIC Z(6),ZZZZ.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                FORM 
                TF-TRANS
                CARRIERE.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CAR.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           OPEN INPUT CARRIERE.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           INITIALIZE LIN-NUM LIN-IDX LIN-NUM.

           CALL "0-TODAY" USING TODAY.
           MOVE MOIS-IDX(LNK-MOIS) TO SH-00.
           MOVE LNK-ANNEE TO SAVE-ANNEE.
           MOVE LNK-MOIS  TO SAVE-MOIS.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000092 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT FEUILLE
             LINE 12 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY CONTINUE.

       AVANT-7.
           ACCEPT AUTEUR
             LINE 14 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY CONTINUE.

       AVANT-PATH.
           MOVE "AA" TO LNK-AREA.
           INITIALIZE LNK-POSITION.
           MOVE 32 TO LNK-NUM.
           MOVE "L" TO LNK-LOW.
           PERFORM DISPLAY-MESSAGE.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 24 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-6.
           IF FEUILLE = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-7.
           IF AUTEUR = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
              WHEN  5 PERFORM TRAITEMENT
              WHEN 10 MOVE 1 TO ASCII
                      PERFORM AVANT-PATH
                      PERFORM OPEN-FILE
                      PERFORM TRAITEMENT
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-PERSON-END.
           PERFORM END-PROGRAM.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           MOVE SAVE-ANNEE TO LNK-ANNEE.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-PERSON-END
           END-IF.

       READ-PERSON-1.
           PERFORM DIS-HE-01.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF STATUT > 0 AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF COUT > 0 AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           MOVE LNK-LANGUAGE TO PR-LANGUAGE.
           PERFORM START-CAR.
           IF FEUILLE NOT = "N"
           AND ASCII = 0
             PERFORM TRANSMET
           END-IF.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
               GO READ-PERSON.
       READ-PERSON-END.
           PERFORM END-PROGRAM.

       START-CAR.
           INITIALIZE CAR-RECORD.
           MOVE FR-KEY  TO CAR-FIRME.
           MOVE REG-PERSON TO CAR-PERSON.
           MOVE REG-ANCIEN-A TO CAR-ANNEE.
           MOVE REG-ANCIEN-M TO CAR-MOIS.
           START CARRIERE KEY >= CAR-KEY INVALID CONTINUE
                NOT INVALID PERFORM NEXT-CAR THRU NEXT-CAR-END.

       NEXT-CAR.
           READ CARRIERE NEXT NO LOCK AT END 
                GO NEXT-CAR-END
           END-READ.

           IF FR-KEY     NOT = CAR-FIRME
           OR REG-PERSON NOT = CAR-PERSON
              GO NEXT-CAR-END.
           MOVE CAR-ANNEE TO INDEX-ANNEE LNK-ANNEE.
           MOVE CAR-MOIS  TO INDEX-MOIS  LNK-MOIS.

           INITIALIZE T1-PF-NOM T1-POSTE. 
           IF ASCII = 1
           AND CAR-POSTE-FRAIS > 0
              MOVE CAR-POSTE-FRAIS TO PC-NUMBER T1-POSTE 
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              MOVE PC-NOM TO T1-PF-NOM
           END-IF.

           MOVE CAR-METIER TO MET-CODE T1-METIER.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO T1-MET-NOM.
           MOVE CAR-POSITION TO T1-POSITION.

           MOVE CAR-COUT TO COUT-NUMBER T1-COUT.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           MOVE COUT-NOM TO T1-COUT-NOM.

           MOVE CAR-EQUIPE TO EQ-NUMBER T1-EQUIPE.
           CALL "6-EQUIPE" USING LINK-V EQ-RECORD FAKE-KEY.
           MOVE EQ-NOM TO T1-EQ-NOM.

           INITIALIZE BDF-RECORD BAR-RECORD.
           IF LNK-COMPETENCE >= REG-COMPETENCE 
              IF CAR-BAREME NOT = SPACES
                 MOVE CAR-BAREME TO BAR-BAREME BDF-CODE 
                 MOVE CAR-GRADE  TO BAR-GRADE 
                 MOVE CAR-ECHELON TO T1-ECHELON
                 MOVE CAR-ANNEE  TO BAR-ANNEE
                 MOVE CAR-MOIS   TO BAR-MOIS
                 CALL "6-BAREME" USING LINK-V BAR-RECORD NUL-KEY
                 CALL "6-BARDEF" USING LINK-V BDF-RECORD FAKE-KEY
                 MOVE BAR-DESCRIPTION TO T1-GRADE
              END-IF
              PERFORM BASES-SALAIRE
           END-IF.

           IF COUNTER = 0 
           AND ASCII  = 0
              PERFORM READ-IMPRIMANTE 
              ADD 1 TO COUNTER
           END-IF.
           IF ASCII  = 0
              PERFORM TEST-IMPRIMANTE
              PERFORM FILL-FILES
           ELSE
              PERFORM WRITE-TEXT.
           GO NEXT-CAR.
       NEXT-CAR-END.
           ADD 1 TO LIN-NUM.

       TEST-IMPRIMANTE.
           COMPUTE IDX = LIN-NUM + 5.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM LAST-PERSON
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM FILL-FIRME
              MOVE LIN-IDX TO LIN-NUM
              MOVE 0 TO LAST-PERSON
           END-IF.
           IF REG-PERSON NOT = LAST-PERSON
              PERFORM DIS-HE-01
              PERFORM DONNEES-PERSONNE.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       BASES-SALAIRE.
           CALL "1-LINDEX" USING LINK-V.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           CALL "6-GHJS" USING LINK-V HJS-RECORD CAR-RECORD PRESENCES.
           MOVE CAR-EQUIPE TO POSE-CODE.
           CALL "6-POSE" USING LINK-V POSE-RECORD FAKE-KEY.
           CALL "4-SALBAS" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 POSE-RECORD 
                                 HJS-RECORD 
                                 BASES-REMUNERATION.
       ENTREE-SORTIE.
           IF LNK-VAL > 0 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       FILL-FIRME.
      *    PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  3 TO CAR-NUM.
           MOVE  3 TO LIN-NUM.
           MOVE 195 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE  4 TO LIN-NUM.
           MOVE 77 TO COL-NUM.
           COMPUTE VH-00 = SH-00 * 100.
           PERFORM FILL-FORM.

           MOVE  4 TO LIN-NUM.
           MOVE 50 TO COL-NUM.
           MOVE SAVE-MOIS TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE SAVE-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE  2 TO LIN-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE 196 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 199 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 202 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE  4 TO CAR-NUM.
           MOVE 48 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.

           MOVE FR-NOM TO ALPHA-TEXTE.
           MOVE 8 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 3 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 4 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-CODE-POST TO VH-00.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       DONNEES-PERSONNE.
           ADD 1 TO LIN-NUM.
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  6 TO COL-NUM.
           MOVE REG-PERSON TO VH-00 LAST-PERSON.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       FILL-FILES.
           ADD 1 TO LIN-NUM.
           MOVE 5 TO COL-NUM.
           MOVE 2 TO CAR-NUM.
           MOVE CAR-MOIS TO VH-00.
           PERFORM FILL-FORM.

           MOVE 8 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE CAR-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE 13 TO COL-NUM.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 3 TO COL-NUM.
           MOVE CAR-POSITION TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 50 TO COL-NUM.
           MOVE  8 TO CAR-NUM.
           MOVE COUT-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE 59 TO COL-NUM.
           MOVE COUT-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.


           MOVE 88 TO COL-NUM.
           MOVE  5 TO CAR-NUM.
           MOVE  4 TO DEC-NUM.
           MOVE  1 TO POINTS.
           MOVE CAR-SAL-100 TO VH-00.
           PERFORM FILL-FORM.

           MOVE 102 TO COL-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE  1 TO POINTS.
           IF CAR-HOR-MEN = 0 
              MOVE 4 TO DEC-NUM
              MOVE BAS-HORAIRE TO VH-00
           ELSE
              MOVE BAS-MOIS TO VH-00
           END-IF.
           PERFORM FILL-FORM.

           MOVE SAVE-ANNEE TO LNK-ANNEE.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           PERFORM BASES-SALAIRE.

           MOVE 118 TO COL-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE  1 TO POINTS.
           IF CAR-HOR-MEN = 0 
              MOVE 4 TO DEC-NUM
              MOVE BAS-HORAIRE TO VH-00
           ELSE
              MOVE BAS-MOIS TO VH-00
           END-IF.
           PERFORM FILL-FORM.

ext
   

           IF CAR-BAREME NOT = SPACES
              MOVE CAR-ECHELON TO VH-00
              MOVE   2 TO CAR-NUM
              MOVE 133 TO COL-NUM
              PERFORM FILL-FORM
              MOVE 136 TO COL-NUM
              MOVE BAR-DESCRIPTION TO ALPHA-TEXTE
              PERFORM FILL-FORM
              IF CAR-ECHELON > 0
              AND BAR-POINT > 0
                 INITIALIZE PTS-RECORD
                 MOVE CAR-ANNEE TO PTS-ANNEE
                 MOVE CAR-MOIS  TO PTS-MOIS
                 CALL "6-POINT" USING LINK-V PTS-RECORD
                 COMPUTE VH-00 = BAR-POINTS(CAR-ECHELON) 
                 MOVE   4 TO CAR-NUM
                 MOVE 146 TO COL-NUM
                 PERFORM FILL-FORM
              END-IF
           END-IF.

           MOVE 150 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE EQ-NUMBER TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 155 TO COL-NUM.
           MOVE EQ-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 170 TO COL-NUM.
           MOVE  8 TO CAR-NUM.
           MOVE CAR-POSTE-FRAIS TO VH-00.
           PERFORM FILL-FORM.

           IF CAR-POSTE-FRAIS > 0
              MOVE CAR-POSTE-FRAIS TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              MOVE 180 TO COL-NUM
              MOVE PC-NOM TO ALPHA-TEXTE
              PERFORM FILL-FORM.

           IF AUTEUR NOT = "N" 
              PERFORM 3-LIGNE.

       3-LIGNE.
           ADD 1 TO LIN-NUM.
           MOVE 13 TO COL-NUM.
           MOVE CAR-USER TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 35 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE CAR-ST-JOUR TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 38 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE CAR-ST-MOIS TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 41 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE CAR-ST-ANNEE TO  VH-00.
           PERFORM FILL-FORM.

           PERFORM FILL-FORM.
           MOVE 47 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE CAR-ST-HEURE TO VH-00.
           PERFORM FILL-FORM.
           PERFORM FILL-FORM.
           MOVE 50 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE CAR-ST-MIN TO VH-00.
           PERFORM FILL-FORM.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L215" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

        OPEN-FILE.
           OPEN OUTPUT TF-TRANS.
           PERFORM HEAD-LINE.
           MOVE 423 TO IDX-4.
           WRITE TF-RECORD FROM Txt-RECORD.

       HEAD-LINE.
           MOVE "AA" TO LNK-AREA.
           MOVE 117 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-ANNEE.
           MOVE "AY" TO LNK-AREA.
           MOVE  5  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-PERSON.
           MOVE "FI" TO LNK-AREA.
           MOVE  1  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-FIRME.
           MOVE "PR" TO LNK-AREA.
           MOVE  3  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-NOM.

           MOVE "FR" TO LNK-AREA.
           MOVE  25 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-COUT-NOM.

           MOVE "AA" TO LNK-AREA.
           MOVE 114 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-MOIS.
           MOVE "AA" TO LNK-AREA.
           MOVE 50 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-METIER TXT-EQUIPE TXT-POSTE TXT-COUT.

           MOVE "SO" TO LNK-AREA.
           MOVE  9 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-MET-NOM.
           MOVE "SO" TO LNK-AREA.
           MOVE  3 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-EQ-NOM.
           MOVE "SO" TO LNK-AREA.
           MOVE  22 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-PF-NOM.
           MOVE "CA" TO LNK-AREA.
           MOVE 36 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-GRADE.
           MOVE 38 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-ECHELON.
           MOVE 26 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-POSITION.

           MOVE "SA" TO LNK-AREA.
           MOVE  1 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-INDEX-100.
           MOVE  2 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-INDEX-HIST.

           MOVE 1 TO COL-NUM.
           STRING LNK-ANNEE DELIMITED BY SIZE
           INTO TXT-INDEX-ACT POINTER COL-NUM.
           ADD 1 TO COL-NUM.
           STRING LNK-MOIS DELIMITED BY SIZE
           INTO TXT-INDEX-ACT POINTER COL-NUM.

       WRITE-TEXT.
           MOVE FR-KEY TO T1-FIRME.
           MOVE LNK-ANNEE TO T1-ANNEE.
           MOVE LNK-MOIS  TO T1-MOIS.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO T1-NOM.
           MOVE REG-PERSON TO T1-PERSON.

           MOVE CAR-ANNEE TO LNK-ANNEE.
           MOVE CAR-MOIS  TO LNK-MOIS.
           PERFORM BASES-SALAIRE.

           MOVE CAR-SAL-100 TO T1-INDEX-100.
           IF CAR-HOR-MEN = 0 
              MOVE BAS-HORAIRE TO T1-INDEX-HIST
           ELSE
              MOVE BAS-MOIS TO T1-INDEX-HIST
           END-IF.
           COMPUTE SH-01 = MOIS-IDX(CAR-MOIS) * 100.
           MOVE SH-01 TO T1-INDEX.

           MOVE SAVE-ANNEE TO LNK-ANNEE.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           PERFORM BASES-SALAIRE.

           IF CAR-HOR-MEN = 0 
              MOVE BAS-HORAIRE TO T1-INDEX-ACT
           ELSE
              MOVE BAS-MOIS TO T1-INDEX-ACT
           END-IF.
           WRITE TF-RECORD FROM T1-RECORD.

       AFFICHAGE-ECRAN.
           MOVE 1207 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF ASCII = 0
              IF LIN-NUM > LIN-IDX
                 PERFORM TRANSMET
              END-IF
              IF COUNTER > 0
                 MOVE 99 TO LNK-VAL
                 CALL "L215" USING LINK-V FORMULAIRE
                 CANCEL "L215"
               END-IF
           END-IF.
           MOVE SAVE-ANNEE TO LNK-ANNEE.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           CALL "1-LINDEX" USING LINK-V.
           CLOSE CARRIERE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".

