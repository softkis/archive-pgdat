      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 3-CERTE EDITION CERTIFICAT R륪UN륱ATION ETENDU     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CERTE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       DATA DIVISION.

       WORKING-STORAGE SECTION.

       01  FID-RECORD.
           02 FID-1 PIC X(30).
           02 FID-2.
              03 FID-T PIC X(20).
              03 DELIM PIC X.
           

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 10.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "STATUT.REC".
           COPY "FICHE.REC".
           COPY "LIVRE.REC".
           COPY "LIVRE.CUM".
           COPY "V-VAR.CPY".
           COPY "PARMOD.REC".

       01  END-NUMBER            PIC 9(8) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  COUNTER               PIC 9999 VALUE 0.

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  SAVE-FIRME            PIC 9999.
       01  ACTION-CALL           PIC 9 VALUE 0.
       01  TEST-EDITION          PIC 9 VALUE 0.
       01  REEDITION             PIC X VALUE "N".
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  COMPTEUR              pic 9(5) VALUE 0.
       01  VH-00                 PIC S9(9).
       01  ALPHA-TEXTE           PIC X(30).

       01  DEBUT-FIN.
           02 DEBUT-D.
              03 DEBUT-J PIC 99.
              03 DEBUT-M PIC 99.
           02 FIN-D.
              03 FIN-J PIC 99.
              03 FIN-M PIC 99.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).


       01  TF-RECORD.
           02 TF-FIRME        PIC 9999.
           02 TF-NUMBER       PIC 9(6).

           02 TF-DATE.
              03 TF-DATE-J       PIC ZZ.
              03 TF-FILLER       PIC X.
              03 TF-DATE-M       PIC ZZ.
              03 TF-FILLER       PIC X.
              03 TF-DATE-A       PIC 9999.

           02 TF-D1.
              03 TF-JOUR-DEBUT   PIC ZZ.
              03 TF-FILLER       PIC X.
              03 TF-MOIS-DEBUT   PIC X(10).

           02 TF-D2.
              03 TF-JOUR-FIN     PIC ZZ.
              03 TF-FILLER       PIC X.
              03 TF-MOIS-FIN     PIC X(10).

           02 TF-COMMUNE      PIC X(30).
           02 TF-NUMERO       PIC X(10).
           02 TF-TAUX         PIC ZZ,ZZ.
           02 TF-TAUX-NUL     PIC X.
           02 TF-CLASSE.
              03 TF-CLASSE-A  PIC 9.
              03 TF-GROUPE    PIC X.
              03 TF-CLASSE-B  PIC ZZ.
           02 TF-ABAT-FD      PIC ZZZZZZ,ZZ.
           02 TF-ABAT-AC      PIC ZZZZZZ,ZZ.
           02 TF-ABAT-FO      PIC ZZZZZZ,ZZ.
           02 TF-ABAT-DS      PIC ZZZZZZ,ZZ.
           02 TF-ABAT-CE      PIC ZZZZZZ,ZZ.

           02 TF-PR-NOM       PIC X(40).
           02 TF-PR-PRENOM    PIC X(54).
           02 TF-REGISTRE     PIC Z(6).
           02 TF-MAISON.
              03 TF-PR-MAISON    PIC X(6).
              03 TF-PR-RUE       PIC X(29).
           02 TF-PR-ADRESSE.
              03 TF-PR-PAYS      PIC XXX.
              03 TF-PR-CODE-POST PIC X(5).
              03 TF-FILLER       PIC X.
              03 TF-PR-LOCALITE  PIC X(40).

           02 TF-PR-MATRICULE PIC 9(11).
           02 TF-FR-MATRICULE PIC 9(11).
           02 TF-FR-NOM       PIC X(30).
           02 TF-MAISON.
              03 TF-FR-MAISON    PIC X(5).
              03 TF-FR-RUE       PIC X(30).

           02 TF-FR-ADRESSE.
              03 TF-FR-PAYS      PIC XXX.
              03 TF-FR-CODE-POST PIC Z(5).
              03 TF-FILLER       PIC X.
              03 TF-FR-LOCALITE  PIC X(40).

           02 TF-FR-PHONE     PIC X(20).

           02 TF-FID-1        PIC X(30).
           02 TF-FID-2        PIC X(21).
           02 TF-DECOMPTE-OUI PIC X.
           02 TF-DECOMPTE-NON PIC X.
           02 TF-TYPE PIC X.

           02 TF-BASE         PIC Z(8),ZZ.
           02 TF-GRAT         PIC Z(8),ZZ.
           02 TF-IND          PIC Z(8),ZZ.
           02 TF-NATURE       PIC Z(8),ZZ.
           02 TF-MALADIE      PIC Z(8),ZZ.
           02 TF-AUTRE        PIC Z(8),ZZ.
           02 TF-TOTAL        PIC Z(8),ZZ.
           02 TF-COTISABLE    PIC Z(8),ZZ.
           02 TF-SHS-FRANCHISE PIC Z(8),ZZ.
           02 TF-NDF-FRANCHISE PIC Z(8),ZZ.
           02 TF-FRANCHISES.
              03 TF-FRANCHISE PIC Z(8),ZZ OCCURS 6.
              03 TF-FR-TEXTE  PIC X(40) OCCURS 6.
           02 TF-IMPOSABLE    PIC Z(8),ZZ.
           02 TF-DECOMPTE     PIC Z(8),ZZ.
           02 TF-IMPOT        PIC Z(8),ZZ.

           02 TF-CNAMOL-FLAG  PIC XXX.
           02 TF-CNAMOL-PLUS  PIC XXXX.
           02 TF-CNAMOL OCCURS 3.
              03 TF-MAL-DEBUT   PIC ZZ.
              03 TF-FILLER      PIC X.
              03 TF-MAL-MOIS    PIC X(10).
           02 TF-CNAMOL-FIN OCCURS 3.
              03 TF-MAL-FIN     PIC ZZ.
              03 TF-FILLER      PIC X.
              03 TF-MAL-MOIS-F  PIC X(10).

           02 TF-IMPOS-FIX    PIC Z(8),ZZ.
           02 TF-IMPOT-FIX    PIC Z(8),ZZ.


       01  LC-RECORD.
           02 LC-ABAT-FD      PIC 9(6)V99.
           02 LC-ABAT-AC      PIC 9(6)V99.
           02 LC-ABAT-FO      PIC 9(6)V99.
           02 LC-ABAT-DS      PIC 9(6)V99.
           02 LC-ABAT-CE      PIC 9(6)V99.

           02 LC-FLAG-DECOMPTE  PIC 9.

           02 LC-BASE         PIC 9(8)V99.
           02 LC-GRAT         PIC 9(8)V99.
           02 LC-IND          PIC 9(8)V99.
           02 LC-NATURE       PIC 9(8)V99.
           02 LC-MALADIE      PIC 9(8)V99.
           02 LC-AUTRE        PIC 9(8)V99.
           02 LC-TOTAL        PIC 9(8)V99.
           02 LC-COTISABLE    PIC 9(8)V99.
           02 LC-SHS-FRANCHISE PIC 9(8)V99.
           02 LC-NDF-FRANCHISE PIC 9(8)V99.
           02 LC-FRANCHISES.
              03 LC-FRANCHISE PIC 9(8)V99 OCCURS 10.
           02 LC-IMPOSABLE    PIC 9(8)V99.
           02 LC-DECOMPTE     PIC 9(8)V99.
           02 LC-IMPOT        PIC S9(8)V99.
           02 LC-IMPOS-FIX    PIC 9(8)V99.
           02 LC-IMPOT-FIX    PIC 9(8)V99.

           02 LC-CMO-IDX   PIC 99.
           02 LC-CMO OCCURS 3.
              03 LC-MAL-DEBUT   PIC 99.
              03 LC-MAL-MOIS    PIC 99.
              03 LC-MAL-FIN     PIC 99.


       LINKAGE SECTION.
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CERTE.
       
           IF FR-MATRICULE = 0000500095592
              EXIT PROGRAM.
           IF LNK-ANNEE > 2008
              CALL "3-CERT9" USING LINK-V 
              CANCEL "3-CERT9"
              EXIT PROGRAM
           END-IF.

           MOVE FR-KEY TO SAVE-FIRME.
           MOVE LNK-MOIS TO SAVE-MOIS.

           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-SETTINGS TO FID-RECORD.
           MOVE PARMOD-EXTENSION-1 TO REEDITION.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5 THRU 6
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 10    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5
           WHEN  6 PERFORM AVANT-6
           WHEN  7 PERFORM AVANT-7
           WHEN  8 PERFORM AVANT-8
           WHEN  9 PERFORM AVANT-9
           WHEN 10 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 1 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-SPECIAL
           WHEN  2 PERFORM APRES-2
           WHEN  3 PERFORM APRES-3
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5
           WHEN  6 PERFORM APRES-6
           WHEN  9 PERFORM APRES-YN
           WHEN 10 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-5.            
           ACCEPT MOIS-DEBUT
             LINE 13 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
                     PERFORM APRES-5
             WHEN 66 ADD      1 TO   MOIS-DEBUT
                     PERFORM APRES-5
             END-EVALUATE.

       AVANT-6.
           ACCEPT MOIS-FIN
             LINE 14 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
                     PERFORM APRES-6
             WHEN 66 ADD      1 TO   MOIS-FIN
                     PERFORM APRES-6
             END-EVALUATE.

       AVANT-7.            
           ACCEPT FID-1
             LINE 18 POSITION 30 SIZE 30
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE FID-RECORD TO PARMOD-SETTINGS.

       AVANT-8.            
           ACCEPT FID-2
             LINE 19 POSITION 30 SIZE 21
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE FID-RECORD TO PARMOD-SETTINGS.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       AVANT-9.
           ACCEPT REEDITION
             LINE 21 POSITION 32 SIZE 1 
             TAB UPDATE CONTROL "UPPER" NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.
           MOVE REEDITION TO ACTION.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-SPECIAL.
           IF  A-N NOT = "A" 
           AND A-N NOT = "N"
               MOVE 1 TO INPUT-ERROR.


       APRES-5.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-05.

       APRES-6.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-06.

       APRES-YN.
           IF REG-PERSON = END-NUMBER
              MOVE " " TO REEDITION ACTION
           END-IF.
           IF  MOIS-DEBUT > 1
           AND MOIS-FIN   < 12 
              MOVE " " TO REEDITION ACTION
           END-IF.
           IF ACTION = "Y" OR "J" OR "O" OR "N" OR "S" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE "N" TO REEDITION
              MOVE 1 TO INPUT-ERROR.
           MOVE REEDITION TO PARMOD-EXTENSION-1.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       APRES-DEC.
           CALL "0-TODAY" USING TODAY.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 MOVE 12 TO LNK-MOIS
                    PERFORM START-PERSON
                    PERFORM READ-PERSON THRU READ-PERSON-END
                    GO END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-PERSON-END
           END-IF.
           INITIALIZE LC-RECORD DEBUT-FIN SAVE-KEY CAR-RECORD
           FICHE-RECORD TEST-EDITION.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF CAR-STATUT = 0
              GO READ-PERSON
           END-IF.
           IF  CAR-STATUT = 1
           AND CAR-REGIME = 0
              GO READ-PERSON
           END-IF.
           IF STATUT > 0 AND NOT = CAR-STATUT  
              GO READ-PERSON
           END-IF.

           PERFORM CUMUL-LIVRE THRU CUMUL-LIVRE-END.
           IF DEBUT-J = 0
              GO READ-PERSON
           END-IF.
           IF REEDITION = "N"
           OR REEDITION = "S"
              IF TEST-EDITION =  0
                 GO READ-PERSON
              END-IF
           END-IF.

           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD NUL-KEY.

           PERFORM FULL-PROCESS.

           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
               GO READ-PERSON.
       READ-PERSON-END.
           EXIT.
                
       FULL-PROCESS.
           PERFORM FILL-FILES.
           IF TF-TOTAL NOT = 0
           OR TF-MAL-DEBUT(1) NOT = 0
              ADD 1 TO COUNTER
              MOVE 0 TO LNK-VAL
              CALL "CERTE" USING LINK-V TF-RECORD
              IF MENU-BATCH > 0
                 MOVE 0 TO LNK-VAL
                 CALL "CERTE" USING LINK-V TF-RECORD
              END-IF
           END-IF.
           INITIALIZE TF-RECORD.
           PERFORM DIS-HE-01.
           ADD 1 TO COMPTEUR.
           MOVE COMPTEUR TO HE-Z6.
           DISPLAY HE-Z6 LINE  7 POSITION 70.
           MOVE 0 TO DEBUT-J DEBUT-M.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
           PERFORM DIS-HE-01.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       CUMUL-LIVRE.

           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD NX-KEY.
           IF L-PERSON = 0
              GO CUMUL-LIVRE-END. 
           IF L-MOIS < MOIS-DEBUT
           OR L-MOIS > MOIS-FIN
              GO CUMUL-LIVRE. 
           IF REEDITION = "S"
           AND L-DATE-CERT-IMP > 1
              GO CUMUL-LIVRE. 

           IF L-SUITE = 0 
              IF L-DATE-CERT-IMP < 1
                 MOVE 1 TO TEST-EDITION
              END-IF
      *       IF L-IMPOSABLE-FIXE-NP = L-FRANCHISE-NP
      *          SUBTRACT L-IMPOSABLE-FIXE-NP FROM L-FRANCHISE-NP
      *       END-IF
              IF DEBUT-J  = 0 
                 MOVE L-JOUR-DEBUT TO DEBUT-J
                 MOVE L-MOIS       TO DEBUT-M
              END-IF
              MOVE L-JOUR-FIN TO FIN-J
              MOVE L-MOIS     TO FIN-M
              IF L-MOIS  = 12
                 MOVE L-FLAG-DECOMPTE TO LC-FLAG-DECOMPTE
              END-IF
              COMPUTE LC-BASE = LC-BASE + L-IMPOSABLE-BRUT
              COMPUTE LC-IND  = LC-IND  + L-DEB(117) 
                                        + L-DEB(118) 
                                        + L-DEB(119) 
              COMPUTE LC-GRAT = LC-GRAT + L-IMPOSABLE-BRUT-NP 
                                        - L-DEB(117) 
                                        - L-DEB(118) 
                                        - L-DEB(119) 
              ADD L-COTISATIONS    TO LC-COTISABLE
              ADD L-COTISATIONS-NP TO LC-COTISABLE
           ELSE
              IF L-FLAG-AVANCE = 0
                 COMPUTE LC-MALADIE = LC-MALADIE + L-IMPOSABLE-BRUT
                 - L-COTISATIONS
              ELSE
                 ADD 1 TO LC-CMO-IDX
                 IF LC-CMO-IDX < 4
                    MOVE L-JOUR-DEBUT TO LC-MAL-DEBUT(LC-CMO-IDX)
                    MOVE L-JOUR-FIN   TO LC-MAL-FIN(LC-CMO-IDX)
                    MOVE L-MOIS       TO LC-MAL-MOIS(LC-CMO-IDX)
                 END-IF
                 GO CUMUL-LIVRE
              END-IF
           END-IF.
           ADD L-ABAT(1) TO LC-ABAT-FD.
           ADD L-ABAT(2) TO LC-ABAT-AC.
           ADD L-ABAT(3) TO LC-ABAT-FO.
           ADD L-ABAT(4) TO LC-ABAT-DS.
           ADD L-ABAT(5) TO LC-ABAT-CE.

           ADD L-IMPOSABLE-NET     TO LC-IMPOSABLE.
           ADD L-IMPOSABLE-NET-NP  TO LC-IMPOSABLE.
           ADD L-IMPOSABLE-SPECIAL TO LC-IMPOSABLE.
           ADD L-IMPOT-SPECIAL     TO LC-IMPOSABLE.
           ADD L-IMPOT TO LC-IMPOT.
           ADD L-IMPOT-NP TO LC-IMPOT.
           ADD L-IMPOT-SPECIAL TO LC-IMPOT.
           ADD L-IMPOT-FIXE    TO LC-IMPOT-FIX.
           ADD L-IMPOT-FIXE-NP TO LC-IMPOT-FIX.
           SUBTRACT L-IMPOT-FORFAIT-PP FROM LC-IMPOT-FIX.
           SUBTRACT L-DECOMPTE-IMPOT FROM LC-IMPOT.
           ADD L-SHS-AJUSTE TO LC-SHS-FRANCHISE.
           ADD L-NDF-FRANCHISE TO LC-NDF-FRANCHISE.
           ADD L-HYP-FRANCHISE TO LC-FRANCHISE(1).
           ADD L-CON-FRANCHISE TO LC-FRANCHISE(2).
           ADD L-SOC-FRANCHISE TO LC-FRANCHISE(3).
           ADD L-ETR-FRANCHISE TO LC-FRANCHISE(4).
           ADD L-SEJ-FRANCHISE TO LC-FRANCHISE(5).
           ADD L-DOM-FRANCHISE TO LC-FRANCHISE(6).
           ADD L-AGR-FRANCHISE TO LC-FRANCHISE(7).
           ADD L-ANN-FRANCHISE TO LC-FRANCHISE(8).
           ADD L-FRANCHISE-NP1 TO LC-FRANCHISE(9).
           ADD L-FRANCHISE-NP2 TO LC-FRANCHISE(10).

      *    ADD L-SOC-FRANCHISE TO LC-COTISABLE.

      *    MOVE 0 TO LC-FLAG-DECOMPTE.
           IF L-IMPOSABLE-SPECIAL NOT = 0
              ADD L-IMPOSABLE-SPECIAL TO LC-BASE
              ADD L-IMPOT-SPECIAL TO LC-BASE.

           ADD L-ETALEMENT          TO LC-IMPOS-FIX.
           ADD L-IMPOT-ETALEMENT    TO LC-IMPOS-FIX.
           ADD L-IMPOSABLE-AVANTAGE TO LC-NATURE.
           ADD L-VOITURE            TO LC-NATURE.
           ADD L-NATURE             TO LC-NATURE.
           SUBTRACT L-VOITURE FROM LC-BASE.
           SUBTRACT L-NATURE FROM LC-BASE.
           ADD L-IMPOT-ETALEMENT-C  TO LC-IMPOT-FIX.
           ADD L-IMPOSABLE-FIXE     TO LC-IMPOS-FIX.
           ADD L-IMPOSABLE-FIXE-NP  TO LC-IMPOS-FIX.
           IF  MOIS-DEBUT = 1
           AND MOIS-FIN   = 12
           AND L-SUITE    = 0 
              MOVE TODAY-DATE TO L-DATE-CERT-IMP
              CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD WR-KEY.
           GO CUMUL-LIVRE. 

       CUMUL-LIVRE-END. 
           EXIT.

       DIS-HE-00.
           MOVE FR-KEY    TO HE-Z4.
           DISPLAY HE-Z4     LINE  6 POSITION 35.
           DISPLAY FR-NOM    LINE  6 POSITION 40.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           MOVE 27 TO COL-IDX.
           IF A-N = "A"
              ADD 11 TO COL-IDX
              DISPLAY SPACES LINE 6 POSITION 37 SIZE 1.
           COMPUTE IDX = 80 - COL-IDX.
           MOVE 0 TO SIZ-IDX IDX-1.
           INSPECT PR-NOM TALLYING  SIZ-IDX FOR CHARACTERS BEFORE "  ".
           INSPECT PR-NOM-JF TALLYING IDX-1 FOR CHARACTERS BEFORE "  ".

           IF FR-NOM-JF = SPACES
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX  = COL-IDX + SIZ-IDX + 1
              IF PR-NOM-JF NOT = SPACES
                 COMPUTE IDX = 80 - COL-IDX
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
           ELSE
              IF PR-NOM-JF NOT = SPACES
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX = COL-IDX + SIZ-IDX + 1
           END-IF.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 6 POSITION COL-IDX SIZE IDX LOW.

       DIS-HE-STAT.
           DISPLAY STATUT    LINE 9 POSITION 32.
           DISPLAY STAT-NOM  LINE 9 POSITION 35.
       DIS-HE-05.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 13 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 13351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-06.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 14 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 14351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       DIS-HE-07.
           DISPLAY FID-1 LINE 18 POSITION 30.
       DIS-HE-08.
           DISPLAY FID-2 LINE 19 POSITION 30.
       DIS-HE-09.
           DISPLAY REEDITION LINE 21 POSITION 32.
       DIS-HE-END.
           EXIT.

       FILL-FILES.
           INITIALIZE TF-RECORD.

      *DEBUT.
           MOVE DEBUT-J TO TF-JOUR-DEBUT.
           MOVE DEBUT-M TO LNK-NUM.
           PERFORM MOIS-NOM.
           MOVE LNK-TEXT TO TF-MOIS-DEBUT.

           MOVE FIN-J TO TF-JOUR-FIN.
           MOVE FIN-M TO LNK-NUM.
           PERFORM MOIS-NOM.
           MOVE LNK-TEXT TO TF-MOIS-FIN.

      * DONNEES CLASSE
           MOVE FICHE-COMMUNE TO TF-COMMUNE.
           MOVE FICHE-NUMERO  TO TF-NUMERO.
           MOVE FICHE-CLASSE-A TO TF-CLASSE-A.
           MOVE FICHE-GROUPE   TO TF-GROUPE.
           MOVE FICHE-CLASSE-B TO TF-CLASSE-B.
           MOVE FICHE-TAUX     TO TF-TAUX.
           IF FICHE-IMPOSABLE-YN = "N"
              MOVE "0" TO TF-TAUX-NUL
           ELSE
              MOVE " " TO TF-TAUX-NUL
           END-IF.
           IF FICHE-PENSION = "P"
              MOVE "P" TO TF-TYPE
           ELSE
              MOVE "S" TO TF-TYPE
           END-IF.
           
           
           INSPECT TF-TAUX REPLACING ALL "," BY ".".

      * DONNEES PERSONNE
           MOVE REG-FIRME  TO TF-FIRME.
           MOVE REG-PERSON TO TF-NUMBER.
           MOVE 1 TO IDX-1.
           STRING PR-NOM DELIMITED BY "   "  INTO TF-PR-NOM
           WITH POINTER IDX-1.

           IF PR-NOM-JF NOT = SPACES
              ADD 1 TO IDX-1
              STRING PR-NOM-JF DELIMITED BY "   " INTO TF-PR-NOM
              WITH POINTER IDX-1.


           MOVE REG-PERSON TO TF-REGISTRE.
           MOVE PR-PRENOM TO TF-PR-PRENOM.
           MOVE PR-RUE TO TF-PR-RUE.
           MOVE PR-MAISON TO TF-PR-MAISON.
           MOVE PR-PAYS TO TF-PR-PAYS.
           IF PR-PAYS = "D"
           OR PR-PAYS = "F"
           OR PR-PAYS = "I"
           OR PR-PAYS = "CZ"
           OR PR-PAYS = "PL"
           OR PR-PAYS = "SK"
              MOVE PR-CP5 TO TF-PR-CODE-POST
           ELSE
              MOVE PR-CP4 TO TF-PR-CODE-POST
           END-IF.

           MOVE PR-LOCALITE TO TF-PR-LOCALITE.
           MOVE REG-MATRICULE TO TF-PR-MATRICULE.

      * DONNEES FIRME
           IF FR-FISC-N > 0
              MOVE FR-FISC TO FR-DATE-ETAB.
           MOVE FR-DATE-ETAB TO TF-FR-MATRICULE.
           MOVE FR-NOM TO TF-FR-NOM.
           MOVE FR-RUE TO TF-FR-RUE.
           MOVE FR-MAISON TO TF-FR-MAISON.
           MOVE FR-PAYS TO TF-FR-PAYS.
           MOVE FR-CODE-POST TO TF-FR-CODE-POST.
           MOVE FR-LOCALITE TO TF-FR-LOCALITE.
           MOVE FR-PHONE    TO TF-FR-PHONE.

           MOVE TODAY-JOUR  TO TF-DATE-J.
           MOVE TODAY-MOIS  TO TF-DATE-M.
           MOVE TODAY-ANNEE TO TF-DATE-A.

           MOVE FID-1 TO TF-FID-1.
           MOVE FID-T TO TF-FID-2.


           MOVE LC-ABAT-FD       TO TF-ABAT-FD.
           MOVE LC-ABAT-AC       TO TF-ABAT-AC.
           MOVE LC-ABAT-FO       TO TF-ABAT-FO.
           MOVE LC-ABAT-DS       TO TF-ABAT-DS.
           MOVE LC-ABAT-CE       TO TF-ABAT-CE.
           MOVE LC-BASE          TO TF-BASE.
           MOVE LC-GRAT          TO TF-GRAT.
           MOVE LC-IND           TO TF-IND.
           MOVE LC-NATURE        TO TF-NATURE.
           MOVE LC-MALADIE       TO TF-MALADIE.
           COMPUTE LC-TOTAL = LC-TOTAL + 
               LC-BASE + LC-NATURE + LC-MALADIE + LC-GRAT + LC-IND.
           MOVE LC-TOTAL         TO TF-TOTAL.
           MOVE LC-COTISABLE     TO TF-COTISABLE.
           MOVE LC-SHS-FRANCHISE TO TF-SHS-FRANCHISE.
           MOVE LC-NDF-FRANCHISE TO TF-NDF-FRANCHISE.
           MOVE 0 TO IDX-1.
           PERFORM FRANCHISE VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.

           MOVE LC-IMPOSABLE     TO TF-IMPOSABLE.
           MOVE LC-DECOMPTE      TO TF-DECOMPTE.
           MOVE LC-IMPOT         TO TF-IMPOT.
           MOVE LC-IMPOS-FIX     TO TF-IMPOS-FIX.
           MOVE LC-IMPOT-FIX     TO TF-IMPOT-FIX.

           MOVE LC-DECOMPTE TO TF-DECOMPTE.
           IF LC-DECOMPTE  > 0
           OR LC-FLAG-DECOMPTE = 1
              MOVE "X" TO TF-DECOMPTE-OUI
              MOVE " " TO TF-DECOMPTE-NON
           ELSE   
              MOVE "X" TO TF-DECOMPTE-NON
              MOVE " " TO TF-DECOMPTE-OUI
           END-IF.

           PERFORM CMO VARYING IDX FROM 1 BY 1 UNTIL IDX > 3.

           IF LC-CMO-IDX > 3 
              MOVE "etc." TO TF-CNAMOL-PLUS
           ELSE
              MOVE "    " TO TF-CNAMOL-PLUS.
           IF LC-CMO-IDX > 0 
              MOVE "OUI" TO TF-CNAMOL-FLAG
           ELSE
              MOVE "NON" TO TF-CNAMOL-FLAG
           END-IF.

        CMO.
           IF LC-MAL-DEBUT(IDX) > 0 
              MOVE LC-MAL-DEBUT(IDX) TO TF-MAL-DEBUT(IDX)
              MOVE LC-MAL-FIN(IDX) TO TF-MAL-FIN(IDX)
              MOVE LC-MAL-MOIS(IDX) TO LNK-NUM
              PERFORM MOIS-NOM
              MOVE LNK-TEXT TO TF-MAL-MOIS(IDX) TF-MAL-MOIS-F(IDX).

       TRAITEMENT.
           MOVE 0 TO FR-KEY.
           MOVE 999999 TO END-NUMBER.
           MOVE "ZZZZZZZZZZ" TO END-MATCHCODE.
           MOVE "N" TO A-N.
           MOVE 66 TO EXC-KEY.
           PERFORM READ-FIRME THRU READ-FIRME-END.

       READ-FIRME.
           CALL "6-FIRME" USING LINK-V FR-RECORD "N" EXC-KEY.
           IF FR-KEY = 0
              GO READ-FIRME-END
           END-IF.
           IF  FR-FIN-A > 0
           AND FR-FIN-A < LNK-ANNEE
              GO READ-FIRME
           END-IF.

           PERFORM DIS-HE-00.
           INITIALIZE REG-RECORD.
           MOVE FR-KEY TO REG-FIRME.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-PERSON-END.
           GO READ-FIRME.
       READ-FIRME-END.
           PERFORM END-PROGRAM.

       AFFICHAGE-ECRAN.
           MOVE 1560 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "CERTE" USING LINK-V TF-RECORD
           END-IF.
           CANCEL "CERTE".
           MOVE SAVE-FIRME TO FR-KEY.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

       MOIS-NOM.
           MOVE "MO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.

       FRANCHISE.
           IF IDX-1 > 5
              CONTINUE
           ELSE
              IF LC-FRANCHISE(IDX) NOT = 0
                 ADD 1 TO IDX-1
                 MOVE LC-FRANCHISE(IDX) TO TF-FRANCHISE(IDX-1)
                 MOVE IDX TO LNK-NUM
                 MOVE "CR" TO LNK-AREA
                 CALL "0-GMESS" USING LINK-V
                 MOVE LNK-TEXT TO TF-FR-TEXTE(IDX-1)
              END-IF
           END-IF.
           