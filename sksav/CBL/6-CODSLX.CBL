      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-CODSLX MODULE GENERAL LECTURE CODSAL      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-CODSLX.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
      *    Fichier des PARAMETRES Codes salaires Salaire-minute
           SELECT OPTIONAL CODSAL ASSIGN TO RANDOM, "X-CODSAL",
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is CS-KEY,
                  STATUS FS-CODCOPY.

       DATA DIVISION.

       FILE SECTION.

           COPY "CODSAL.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "CODSAL.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CODSAL.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-CODSLX.
       
           IF NOT-OPEN = 0
              OPEN INPUT CODSAL
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CS-RECORD.
           MOVE 0 TO LNK-VAL.
           EVALUATE EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN  9 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ CODSAL PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ CODSAL NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ CODSAL NO LOCK INVALID INITIALIZE CS-RECORD
                      END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE CS-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START CODSAL KEY < CS-KEY INVALID GO EXIT-1.
       START-2.
           START CODSAL KEY > CS-KEY INVALID GO EXIT-1.


               