      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-CCMP CONTROLE REPARTITION CCM             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CCMP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".
           COPY "TRIPR.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH-REAL,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM130.FDE".
           COPY "TRIPR.FDE".
       FD  TF-TRANS
           RECORD VARYING DEPENDING ON MAX-LONGUEUR
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 1240 DEPENDING MAX-LONGUEUR.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD      PIC X(10) VALUE "130       ".
       01  CHOIX-MAX         PIC 99 VALUE 7.
       01  MAX-LIGNES        PIC 9 VALUE 2.
       01  PRECISION         PIC 9 VALUE 1.
       01  ADRES             PIC X(50) VALUE "C:\PATH\FILENAME".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "CCM.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".
           COPY "PARMOD.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.
       01  LIMITEUR              PIC X(6).

       01  MAX-LONGUEUR          PIC 9999 VALUE 800.
       01  ASCII-FILE            PIC 9 VALUE 0.
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  AJOUTE                PIC 9 VALUE 0.
       01  TABLEAU               PIC 9 VALUE 0.

       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".CCM".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.

       01  T1-RECORD.
           02 T1-A.
              03 T1-FIRME  PIC 9(6).
              03 T1-DELIM0 PIC X VALUE ";".
              03 T1-PERSON PIC 9(8).
              03 T1-DELIM1 PIC X VALUE ";".
              03 T1-NOM    PIC X(50).
              03 T1-DELIM2 PIC X VALUE ";".
              03 T1-ANNEE  PIC Z(6).
              03 T1-DELIM4 PIC X VALUE ";".
              03 T1-MOIS   PIC Z(6).
              03 T1-DELIM4 PIC X VALUE ";".
              03 T1-TOTAL PIC Z(9).
              03 T1-DELIM4 PIC X VALUE ";".

              03 T1-M OCCURS 20.
                 05 T1-COUT   PIC Z(8).
                 05 T1-DEL1   PIC X VALUE ";".
                 05 T1-TEXT   PIC X(30).
                 05 T1-DEL2   PIC X VALUE ";".
                 05 T1-VAL    PIC ZZZZZZ.
                 05 T1-DEL3   PIC X.

       01  TXT-RECORD.
           02 TXT-A.
              03 TXT-FIRME  PIC X(6).
              03 TXT-DELIM0 PIC X VALUE ";".
              03 TXT-PERSON PIC X(8).
              03 TXT-DELIM1 PIC X VALUE ";".
              03 TXT-NOM    PIC X(50).
              03 TXT-DELIM2 PIC X VALUE ";".
              03 TXT-ANNEE  PIC X(6).
              03 TXT-DELIM4 PIC X VALUE ";".
              03 TXT-MOIS   PIC X(6).
              03 TXT-DELIM4 PIC X VALUE ";".
              03 TXT-TOTAL  PIC X(9).
              03 TXT-DELIM5 PIC X VALUE ";".
              03 TXT-CENTRE OCCURS 20.
                 05 TXT-COUT   PIC X(8).
                 05 TXT-DEL1   PIC X VALUE ";".
                 05 TXT-TEXT   PIC X(30).
                 05 TXT-DEL2   PIC X VALUE ";".
                 05 TXT-VAL    PIC X(6).
                 05 TXT-DEL3   PIC X.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR
               TF-TRANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CCMP.
           MOVE 70 TO IMPL-MAX-LINE.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TEST-EXTENSION.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700009278 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR (14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-SORT
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-PATH.
           IF PARMOD-PATH-PROTO = SPACES
              MOVE ADRES TO PARMOD-PATH-PROTO
           END-IF.
           MOVE 24255000 TO LNK-POSITION.
           MOVE SPACES TO LNK-LOW.
           CALL "0-GPATH" USING LINK-V PARMOD-RECORD EXC-KEY.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 12
              MOVE ADRES TO PARMOD-PATH-PROTO
              GO AVANT-PATH
           END-IF.
           IF LNK-LOW = "!" 
              PERFORM AFFICHAGE-ECRAN
              GO AVANT-PATH
           END-IF.
           IF LNK-NUM > 0 
              MOVE  0 TO LNK-POSITION
              PERFORM DISPLAY-MESSAGE
              GO AVANT-PATH
           END-IF.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           IF EXC-KEY = 9 
              MOVE 1 TO TABLEAU
              MOVE 10 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           WHEN 10 MOVE 1 TO ASCII-FILE
                   PERFORM AVANT-PATH
                   PERFORM TRAITEMENT
                   PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-2
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           IF CHOIX > 0
              IF CHOIX < 90
                 IF TEST-ALPHA  NOT = SPACES
                 OR TEST-NUMBER NOT = 0
                    IF TRIPR-CHOIX NOT = TEST-EXTENSION
                       PERFORM TRANSMET
                    END-IF
                 END-IF
              MOVE TRIPR-CHOIX TO TEST-EXTENSION
           END-IF.
           INITIALIZE LAST-PERSON.
           INITIALIZE CCM-RECORD.
           CALL "6-CCM" USING LINK-V CCM-RECORD REG-RECORD NUL-KEY.
           IF CCM-PERSON = 0
              MOVE CAR-ANNEE TO CCM-ANNEE
              MOVE CAR-MOIS  TO CCM-MOIS 
              MOVE CAR-COUT  TO CCM-COUT(1)
              MOVE 1         TO CCM-TAUX(1) CCM-TOTAL CCM-OCCURS
              CALL "6-CCM" USING LINK-V CCM-RECORD REG-RECORD WR-KEY
           END-IF.
           IF ASCII-FILE = 1 
              PERFORM WRITE-TEXT
           ELSE
              PERFORM FULL-PROCESS.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

       ENTETE.
           ADD 1 TO PAGE-NUMBER.
           MOVE 4 TO CAR-NUM.
           MOVE 2 TO LIN-NUM.
           MOVE 122 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE 2 TO CAR-NUM.
           MOVE 100 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE 2 TO CAR-NUM.
           ADD 2 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE 4 TO CAR-NUM.
           ADD 2 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.


           MOVE  3 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           ADD 2 TO COL-NUM.
           PERFORM FILL-FORM.
           IF STATUT NOT = 0
              MOVE "-" TO ALPHA-TEXTE
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM
              MOVE STAT-NOM TO ALPHA-TEXTE
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM.

           IF COUT NOT = 0
              MOVE "-" TO ALPHA-TEXTE
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM
              MOVE COUT-NUMBER TO VH-00 
              MOVE 8 TO CAR-NUM
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE COUT-NOM TO ALPHA-TEXTE
              PERFORM FILL-FORM.

           MOVE 2 TO LIN-NUM.
           MOVE 85 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE LNK-ANNEE TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO LIN-NUM.

       DETAIL-CHOIX.
           MOVE  3 TO LIN-NUM.
           MOVE 65 TO COL-NUM.
           MOVE TEST-NUMBER TO LNK-POSITION.
           MOVE TEST-ALPHA  TO LNK-TEXT.
           MOVE CHOIX       TO LNK-NUM.
           CALL "4-CHOIX" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       FULL-PROCESS.
           PERFORM TEST-LINE.
           MOVE 0 TO IDX-1.
           PERFORM FILL-CCM THRU FILL-CCM-END.

       FILL-CCM.
           ADD 1 TO IDX-1.
           IF IDX-1 > 20
              GO FILL-CCM-END.
           IF CCM-COUT(IDX-1) = 0
              GO FILL-CCM-END.
           PERFORM TEST-LINE.
           COMPUTE COL-NUM = 12
           MOVE CCM-COUT(IDX-1) TO VH-00.
           MOVE 8 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE CCM-TAUX(IDX-1) TO VH-00.
           COMPUTE COL-NUM = 118.
           MOVE 5 TO CAR-NUM.
           PERFORM FILL-FORM.
           COMPUTE COL-NUM = 22.
           MOVE CCM-COUT(IDX-1) TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           MOVE COUT-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           GO FILL-CCM.
       FILL-CCM-END.
           ADD 1 TO LIN-NUM.

       TEST-LINE.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE IDX = LIN-NUM + 1.
           IF REG-PERSON NOT = LAST-PERSON
           OR LAST-PERSON = 0
              ADD 1 TO IDX.
           IF IDX >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM LAST-PERSON
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              COMPUTE LIN-NUM = LIN-IDX 
           END-IF.
           IF REG-PERSON NOT = LAST-PERSON
           OR LAST-PERSON = 0
              PERFORM DONNEES-PERSONNE.

           COPY "XDIS.CPY".
       DIS-HE-END.


       AFFICHAGE-ECRAN.
           MOVE 1203 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           IF CHOIX NOT = 0 
              PERFORM DETAIL-CHOIX.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF ASCII-FILE = 0
              IF COUNTER > 0 
                 PERFORM TRANSMET
              END-IF
              IF COUNTER > 0
                 MOVE 99 TO LNK-VAL
                 CALL "P130" USING LINK-V FORMULAIRE
                 CANCEL "P130"
              END-IF
           END-IF.
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XSORT.CPY".
           COPY "XACTION.CPY".


       WRITE-TEXT.
           IF NOT-OPEN = 0
              IF AJOUTE = 1
                 OPEN EXTEND TF-TRANS
              ELSE
                 OPEN OUTPUT TF-TRANS
              END-IF
              MOVE 1 TO NOT-OPEN
              PERFORM HEAD-LINE
              IF TABLEAU = 1
              AND AJOUTE = 0
                  WRITE TF-RECORD FROM Txt-RECORD
              END-IF
           END-IF.
           MOVE LNK-ANNEE TO T1-ANNEE.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT   TO T1-NOM.
           MOVE FR-KEY     TO T1-FIRME.
           MOVE REG-PERSON TO T1-PERSON.
           MOVE CCM-MOIS   TO T1-MOIS.
           MOVE CCM-TOTAL  TO T1-TOTAL.
           PERFORM LIGNE VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 20.
           WRITE TF-RECORD FROM T1-RECORD.

       LIGNE.
           MOVE CCM-COUT(IDX-1) TO T1-COUT(IDX-1) COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           MOVE COUT-NOM TO T1-TEXT(IDX-1).
           MOVE CCM-TAUX(IDX-1) TO T1-VAL(IDX-1).
           MOVE ";" TO T1-DEL1(IDX-1) T1-DEL2(IDX-1) T1-DEL3(IDX-1).

       HEAD-LINE.
           MOVE "AA" TO LNK-AREA.
           MOVE 117 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-ANNEE.
           MOVE 114 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-MOIS.
           MOVE "MO" TO LNK-AREA.
           MOVE 113 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-TOTAL.

           MOVE "AY" TO LNK-AREA.
           MOVE  5  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-PERSON.
           MOVE "FI" TO LNK-AREA.
           MOVE  1  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-FIRME.
           MOVE "PR" TO LNK-AREA.
           MOVE  3  TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TXT-NOM.
           MOVE "FR" TO LNK-AREA.
           MOVE  25 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           PERFORM TX VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 20.
           MOVE "AA" TO LNK-AREA.
           MOVE  50 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           PERFORM COD VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 20.
           MOVE "AA" TO LNK-AREA.
           MOVE  51 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           PERFORM BAS VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 20.
      *    COMPUTE MAX-LONGUEUR = 124 + IDX * 21.

        TX.
           MOVE LNK-TEXT TO TXT-TEXT(IDX-1)
           MOVE ";" TO TXT-DEL1(IDX-1).
        COD.
           MOVE LNK-TEXT TO TXT-COUT(IDX-1)
           MOVE ";" TO TXT-DEL2(IDX-1).
        BAS.
           MOVE LNK-TEXT TO TXT-VAL(IDX-1)
           MOVE ";" TO TXT-DEL3(IDX-1).


        DONNEES-PERSONNE.
           MOVE  0 TO DEC-NUM POINTS.
           MOVE  3 TO COL-NUM.
           MOVE 6 TO CAR-NUM
           MOVE REG-PERSON TO VH-00 LAST-PERSON.
           PERFORM FILL-FORM.

           ADD 1 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           COMPUTE COL-NUM = 125.
           COMPUTE VH-00 = CCM-TOTAL.
           MOVE 5 TO CAR-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
