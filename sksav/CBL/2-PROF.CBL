      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-PROF RECHERCHE PROFESSIONS                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-PROF.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PROF.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  LANGUAGE PIC XX VALUE SPACES.

       01  CHOIX                 PIC 9(4).
       01  COMPTEUR              PIC 99.

       01 HE-PROF.
          02 H-R PIC 9(5) OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z5 PIC Z(5).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "PROF.LNK".


       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       START-DISPLAY SECTION.
             
       START-2-PROF.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE PROF-RECORD IDX-1 EXC-KEY COMPTEUR CHOIX.
           MOVE 4 TO LIN-IDX.
           MOVE LINK-LANGUE TO PROF-LANGUE.
           PERFORM READ-PR THRU READ-PROF-END.
           IF EXC-KEY = 66 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-PR.
           MOVE 66 TO EXC-KEY.
           CALL "6-PROF" USING LINK-V PROF-RECORD EXC-KEY.
           IF PROF-CODE = 0
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 INITIALIZE HE-PROF IDX-1
                 GO READ-PR
              END-IF
              GO READ-PROF-END.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-PROF IDX-1
                 GO READ-PR
              END-IF
              IF CHOIX NOT = 0
                 GO READ-PROF-END
              END-IF
           END-IF.
           GO READ-PR.
       READ-PROF-END.
           IF CHOIX = 0
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82 AND NOT = 68
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX COMPTEUR IDX-1.
           MOVE PROF-CODE TO HE-Z5 H-R(IDX-1).
           DISPLAY HE-Z5  LINE LIN-IDX POSITION 2.
           DISPLAY PROF-NOM(1) LINE LIN-IDX POSITION 10.
           ADD 1 TO LIN-IDX.
           DISPLAY PROF-NOM(2) LINE LIN-IDX POSITION 10.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0025000000 TO EXC-KFR(1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0000530000 TO EXC-KFR (11).
           MOVE 5600000000 TO EXC-KFR (12).
           MOVE 0067000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE CHOIX.
           ACCEPT CHOIX 
           LINE 3 POSITION 30 SIZE 4
           TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF CHOIX < 1000
           AND CHOIX NOT = 100
           AND CHOIX > 0
              MOVE 2 TO EXC-KEY
           END-IF.
           IF EXC-KEY = 2 
              IF CHOIX < 1000
                 COMPUTE CHOIX = CHOIX * 10
              END-IF
              IF CHOIX < 1000
                 COMPUTE CHOIX = CHOIX * 10
              END-IF
              IF CHOIX < 1000
                 COMPUTE CHOIX = CHOIX * 10
              END-IF
              MOVE CHOIX TO PROF-CODE
              MOVE 0 TO CHOIX
           END-IF.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF CHOIX NOT = 0
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IDX-1.

       AFFICHAGE-ECRAN.
           MOVE 2132 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       END-PROGRAM.
           MOVE CHOIX TO PROF-CODE.
           CALL "6-PROF" USING LINK-V PROF-RECORD FAKE-KEY.
           IF PROF-CODE NOT = 0
              MOVE PROF-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = (IDX-1 * 2) + 3.
           MOVE H-R(IDX-1) TO HE-Z5.
           ACCEPT HE-Z5
             LINE  LIN-IDX POSITION 2 SIZE 5
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           MOVE H-R(IDX-1) TO HE-Z5.
           DISPLAY HE-Z5 LINE LIN-IDX POSITION 2.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN OTHER
                     MOVE H-R(IDX-1) TO CHOIX
                     PERFORM END-PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-R(IDX-1) = 0
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.
                        