      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-ENQSTR STATEC ASCII -> EXCELL             �
      *  � ENQUETE SUR LA STRUCTURE DES SALAIRES version 2006    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-ENQSTR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

       FD  TF-TRANS
           LABEL RECORD STANDARD
           DATA RECORD T-RECORD.
      *  ENREGISTREMENT FICHIER DE TRANSFERT ASCII LIVRE DE PAIE
      
       01  T-RECORD.
ligne      02 T-LIGNE           PIC 9(5).
           02 T-FILLER          PIC X.
B1         02 T-MATR            PIC X(11).
           02 T-FILLER          PIC X.
PERS       02 T-PERS            PIC Z(6).
           02 T-SEPARE          PIC X.
           02 T-NOM             PIC X(25).
           02 T-FILLER          PIC X.
B2         02 T-CIVIL           PIC 999.
           02 T-FILLER          PIC X.
B3         02 T-PAYS            PIC XXX.
           02 T-FILLER          PIC X.
B4         02 T-NATION          PIC XXX.
           02 T-FILLER          PIC X.
B5         02 T-STATUT          PIC 999. 
           02 T-FILLER          PIC X.
B6         02 T-CONTRAT         PIC 999. 
           02 T-FILLER          PIC X.
B7         02 T-PARTIEL         PIC 999. 
           02 T-FILLER          PIC X.
B7a        02 T-HRS-SEM         PIC 999,99.
           02 T-FILLER          PIC X.
B8         02 T-ANC-AN          PIC 9999.
           02 T-FILLER          PIC X.
B9         02 T-ANC-MOIS        PIC 999.
           02 T-FILLER          PIC X.
B10        02 T-FIN-JOUR        PIC 999.
           02 T-FILLER          PIC X.
B11        02 T-FIN-MOIS        PIC 999.
           02 T-FILLER          PIC X.
B12        02 T-INTERRUPTION    PIC 999.
           02 T-FILLER          PIC X.
B13        02 T-CCOL            PIC 999. 
           02 T-FILLER          PIC X.
B14        02 T-ETUDES          PIC 999.
           02 T-FILLER          PIC X.
B15        02 T-METIER          PIC X(30).
           02 T-FILLER          PIC X.
B16        02 T-SUPERVISION     PIC 999.
           02 T-FILLER          PIC X.
B17        02 T-CITP-88         PIC 9999.
           02 T-FILLER          PIC X.
B18        02 T-HRS-TRAV        PIC 999.
           02 T-FILLER          PIC X.
B19        02 T-HRS-PAYE        PIC 999.
           02 T-FILLER          PIC X.
B20        02 T-HRS-SUP         PIC 9999.
           02 T-FILLER          PIC X.
B21        02 T-BRUT            PIC 9(7),99.
           02 T-FILLER          PIC X.
B22        02 T-SUPP            PIC 9999,99.
           02 T-FILLER          PIC X.
B23        02 T-COMP            PIC 9999,99.
           02 T-FILLER          PIC X.
B24        02 T-COTIS           PIC 9999,99.
           02 T-FILLER          PIC X.
B25        02 T-SEMAINE         PIC 999.
           02 T-FILLER          PIC X.
B26        02 T-BRUT-AN         PIC 9(7),99.
           02 T-FILLER          PIC X.
B27        02 T-GRAT-AN         PIC 9(7),99.
           02 T-FILLER          PIC X.
B28        02 T-PRIME-AN        PIC 9(7),99.
           02 T-FILLER          PIC X.
B29        02 T-BENEFICE        PIC 9(7),99.
           02 T-FILLER          PIC X.
B30        02 T-CONGE           PIC 999.
           02 T-FILLER          PIC X.
B31        02 T-PARENTAL        PIC 999.
           02 T-FILLER          PIC X.
B31A       02 T-PAR-MOIS        PIC 9999.
           02 T-FILLER          PIC X.
B32        02 T-JRS-MAL         PIC 999.
           02 T-FILLER          PIC X.
B33        02 T-JRS-MAL-PP      PIC 999.
           02 T-FILLER          PIC X.
B34        02 T-JRS-MATERNITE   PIC 999.
           02 T-FILLER          PIC X.
B35        02 T-FORMATION       PIC 999.
           02 T-FILLER          PIC X.
 
       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  PRECISION             PIC 9 VALUE 0.
       01  SEXES.
           02 D-SEX              PIC XX VALUE "MF".
       01  SEXS REDEFINES SEXES.
           02 SEXE               PIC X OCCURS 2.

       01  TXT-RECORD.
           02 TXT-LIGNE           PIC X(5) VALUE "Ligne".
           02 TXT-FILLER          PIC X VALUE ";".
B1         02 TXT-MATR            PIC X(11) VALUE "B1".
           02 TXT-FILLER          PIC X VALUE ";".
NOM        02 TXT-PERSONNE        PIC X(32) VALUE "Personne".
           02 TXT-FILLER          PIC X VALUE ";".

B2         02 TXT-CIVIL           PIC XXX VALUE "B2".
           02 TXT-FILLER          PIC X VALUE ";".
B3         02 TXT-PAYS            PIC XXX VALUE "B3".
           02 TXT-FILLER          PIC X VALUE ";".
B4         02 TXT-NATION          PIC XXX VALUE "B4".
           02 TXT-FILLER          PIC X VALUE ";".
B5         02 TXT-STATUT          PIC XXX VALUE "B5". 
           02 TXT-FILLER          PIC X VALUE ";".
B6         02 TXT-CONTRAT         PIC XXX VALUE "B6". 
           02 TXT-FILLER          PIC X VALUE ";".
B7         02 TXT-PARTIEL         PIC XXX VALUE "B7". 
           02 TXT-FILLER          PIC X VALUE ";".
B7a        02 TXT-HRS-SEM         PIC X(6) VALUE "B7A".
           02 TXT-FILLER          PIC X VALUE ";".
B8         02 TXT-ANC-AN          PIC XXXX VALUE "B8".
           02 TXT-FILLER          PIC X VALUE ";".
B9         02 TXT-ANC-MOIS        PIC XXX VALUE "B9".
           02 TXT-FILLER          PIC X VALUE ";".
B10        02 TXT-FIN-JOUR        PIC XXX VALUE "B10".
           02 TXT-FILLER          PIC X VALUE ";".
B11        02 TXT-FIN-MOIS        PIC XXX VALUE "B11".
           02 TXT-FILLER          PIC X VALUE ";".
B12        02 TXT-INTERRUPTION    PIC XXX VALUE "B12".
           02 TXT-FILLER          PIC X VALUE ";".
B13        02 TXT-CCOL            PIC XXX VALUE "B13". 
           02 TXT-FILLER          PIC X VALUE ";".
B14        02 TXT-ETUDES          PIC XXX VALUE "B14".
           02 TXT-FILLER          PIC X VALUE ";".
B15        02 TXT-METIER          PIC X(30) VALUE "B15".
           02 TXT-FILLER          PIC X VALUE ";".
B16        02 TXT-SUPERVISION     PIC XXX VALUE "B16".
           02 TXT-FILLER          PIC X VALUE ";".
B17        02 TXT-CITP-88         PIC XXXX VALUE "B17".
           02 TXT-FILLER          PIC X VALUE ";".
B18        02 TXT-HRS-TRAV        PIC XXX VALUE "B18".
           02 TXT-FILLER          PIC X VALUE ";".
B19        02 TXT-HRS-PAYE        PIC XXX VALUE "B19".
           02 TXT-FILLER          PIC X VALUE ";".
B20        02 TXT-HRS-SUP         PIC XXX9 VALUE "B20".
           02 TXT-FILLER          PIC X VALUE ";".
B21        02 TXT-BRUT            PIC X(10) VALUE "B21".
           02 TXT-FILLER          PIC X VALUE ";".
B22        02 TXT-SUPP            PIC X(7) VALUE "B22".
           02 TXT-FILLER          PIC X VALUE ";".
B23        02 TXT-COMP            PIC X(7) VALUE "B23".
           02 TXT-FILLER          PIC X VALUE ";".
B24        02 TXT-COTIS           PIC X(7) VALUE "B24".
           02 TXT-FILLER          PIC X VALUE ";".
B25        02 TXT-SEMAINE         PIC XXX VALUE "B25".
           02 TXT-FILLER          PIC X VALUE ";".
B26        02 TXT-BRUTXT-AN        PIC X(10) VALUE "B26".
           02 TXT-FILLER          PIC X VALUE ";".
B27        02 TXT-GRATXT-AN        PIC X(10) VALUE "B27".
           02 TXT-FILLER          PIC X VALUE ";".
B28        02 TXT-PRIME-AN        PIC X(10) VALUE "B28".
           02 TXT-FILLER          PIC X VALUE ";".
B29        02 TXT-BENEFICE        PIC X(10) VALUE "B29".
           02 TXT-FILLER          PIC X VALUE ";".
B30        02 TXT-CONGE           PIC XXX VALUE "B30".
           02 TXT-FILLER          PIC X VALUE ";".
B31        02 TXT-PARENTAL        PIC XXX VALUE "B31".
           02 TXT-FILLER          PIC X VALUE ";".
B31A       02 TXT-PAR-MOIS        PIC XXXX VALUE "B31A".
           02 TXT-FILLER          PIC X VALUE ";".
B32        02 TXT-JRS-MAL         PIC XXX VALUE "B32".
           02 TXT-FILLER          PIC X VALUE ";".
B33        02 TXT-JRS-MAL-PP      PIC XXX VALUE "B33".
           02 TXT-FILLER          PIC X VALUE ";".
B34        02 TXT-JRS-MATERNITE   PIC XXX VALUE "B34".
           02 TXT-FILLER          PIC X VALUE ";".
B35        02 TXT-FORMATION       PIC XXX VALUE "B35".
           02 TXT-FILLER          PIC X VALUE ";".


      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "FICHE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "CONTRAT.REC".
           COPY "PARMOD.REC".
           COPY "LIVRE.REC".
           COPY "LIVRE.LNK".
           COPY "HORSEM.REC".
           COPY "JOURS.REC".
           COPY "METIER.REC".
           COPY "PAYS.REC".
           COPY "MOTDEP.REC".
           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  DET-ABS.
           02 TYP-OCC OCCURS 50.
              03 MOIS-OCC PIC 99.
              03 JRS-OCC PIC 999V99.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       01  NOT-OPEN              PIC 99 VALUE 0.
       01  EXTENDING             PIC  9 VALUE 0.
       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TF-TRANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-ENQSTR.
       
      *    DIVIDE LNK-ANNEE BY 4 GIVING IDX-1 REMAINDER IDX-2.
      *    IF IDX-2 NOT = 2
      *       MOVE "SL" TO LNK-AREA
      *       MOVE 48   TO LNK-NUM  
      *       MOVE 0 TO LNK-POSITION
      *       CALL "0-DMESS" USING LINK-V
      *       PERFORM AA
      *       EXIT PROGRAM
      *    END-IF.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           CALL "0-TODAY" USING TODAY.
           MOVE LNK-MOIS  TO SAVE-MOIS.
           MOVE LNK-ANNEE TO SAVE-ANNEE.
           MOVE 10 TO LNK-MOIS.

           PERFORM AFFICHAGE-ECRAN .
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0144000000 TO EXC-KFR(1)
           WHEN 2     MOVE 0129000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0100000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-PATH
           WHEN  4 PERFORM AVANT-DEC.

           IF EXC-KEY = 1 
              MOVE 700001 TO LNK-VAL
              PERFORM HELP-SCREEN
              MOVE 98 TO EXC-KEY
           END-IF.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-1.
           ACCEPT PARMOD-PROG-NUMBER-1
             LINE 4 POSITION 50 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF PARMOD-PROG-NUMBER-1 > 1
              GO AVANT-1.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       AVANT-2.
           IF PARMOD-PROG-NUMBER-2 > 0
              MOVE PARMOD-PROG-NUMBER-2 TO MD-CODE
           ELSE
              MOVE 11 TO MD-CODE.

           ACCEPT MD-CODE
             LINE 6 POSITION 49 SIZE 2 
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY CONTINUE.
           MOVE MD-CODE TO PARMOD-PROG-NUMBER-2.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 8 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY
           WHEN  2 MOVE PRECISION TO LNK-PRESENCE
                   MOVE "X" TO LNK-A-N
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-MOTDEP" USING LINK-V MD-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN  65 THRU 66 PERFORM NEXT-MOTIF
           END-EVALUATE.
           PERFORM DIS-HE-02.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE 1 TO EXTENDING
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-MOTIF.
           CALL "6-MOTDEP" USING LINK-V MD-RECORD EXC-KEY.
           
       TRAITEMENT.
           INITIALIZE REG-RECORD SH-01.
           MOVE "X" TO A-N.
           PERFORM READ-PERSON THRU READ-EXIT.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           MOVE REG-PERSON  TO HE-Z6.

           IF REG-PERSON = 0
              GO READ-EXIT.
           IF PRES-TOT(LNK-MOIS) < 31
           OR PR-NOM = SPACES
              GO READ-PERSON
           END-IF.
           PERFORM WRITE-FILES.
           GO READ-PERSON.
       READ-EXIT.
           EXIT.

       WRITE-FILES.
           IF NOT-OPEN = 0
              IF EXTENDING = 0
                 OPEN OUTPUT TF-TRANS
                 WRITE T-RECORD FROM TXT-RECORD
              ELSE
                 OPEN EXTEND TF-TRANS
              END-IF
              MOVE 1 TO NOT-OPEN
           END-IF.
           MOVE REG-PERSON TO HE-Z6.
           DISPLAY HE-Z6  LINE 20 POSITION 2.
           DISPLAY PR-NOM LINE 20 POSITION 25.
           DISPLAY REG-MATRICULE LINE 20 POSITION 10.
           INITIALIZE T-RECORD.
           ADD 1 TO SH-01.
           INSPECT T-RECORD REPLACING ALL " " BY ";".
           MOVE SH-01      TO T-LIGNE.
PERSON     MOVE REG-MATRICULE TO T-MATR.
           MOVE REG-PERSON TO T-PERS.
           MOVE PR-NOM     TO T-NOM.
           MOVE SPACES     TO T-SEPARE.

B2         EVALUATE FICHE-ETAT-CIVIL
               WHEN "C" MOVE 1 TO T-CIVIL
               WHEN "M" MOVE 2 TO T-CIVIL
               WHEN "V" MOVE 3 TO T-CIVIL
               WHEN OTHER MOVE 4 TO T-CIVIL
           END-EVALUATE.
B3         MOVE PR-PAYS TO T-PAYS.
B4         MOVE PR-NATIONALITE TO T-NATION.
           EVALUATE PR-NATIONALITE
               WHEN "L"  CONTINUE
               WHEN "I"  CONTINUE
               WHEN "P"  CONTINUE
               WHEN "F"  CONTINUE
               WHEN "D"  CONTINUE
               WHEN OTHER MOVE PR-NATIONALITE TO PAYS-CODE
                    CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY
                    IF PAYS-EU = "E"
                       MOVE PAYS-EU TO T-NATION
                    ELSE
                       MOVE "N" TO T-NATION
                    END-IF
           END-EVALUATE.
B5         MOVE CAR-STATUT TO T-STATUT.
           IF CAR-STATEC = 1
              MOVE 4 TO T-STATUT.
B6         MOVE 1 TO T-CONTRAT.
           EVALUATE CON-TYPE
               WHEN 2 MOVE 2 TO T-CONTRAT
               WHEN 3 MOVE 2 TO T-CONTRAT
               WHEN 5 MOVE 4 TO T-CONTRAT
           END-EVALUATE.
           IF CAR-STATEC = 1
              MOVE 3 TO T-CONTRAT.
B7         MOVE 1 TO T-PARTIEL.
B7A        IF HJS-HEURES(8) < 35
              MOVE 2 TO T-PARTIEL
              COMPUTE SH-00 = HJS-HEURES(8) * 2,5
              MOVE SH-00 TO T-HRS-SEM.
B8         MOVE REG-ANCIEN-A TO T-ANC-AN.
B9         MOVE REG-ANCIEN-M TO T-ANC-MOIS.
B10        MOVE CON-FIN-M  TO T-FIN-MOIS.
B11        MOVE CON-FIN-J  TO T-FIN-JOUR.
B12        PERFORM INTERRUPTIONS.
           MOVE SH-00 TO T-INTERRUPTION    
B13        MOVE PARMOD-PROG-NUMBER-1 TO T-CCOL.
B14        IF CAR-EDUCATION < 1
              MOVE 1 TO CAR-EDUCATION
           END-IF.
           MOVE CAR-EDUCATION TO T-ETUDES.
B15        MOVE MET-NOM(PR-CODE-SEXE) TO T-METIER.
B16        IF CAR-STATEC = 2
              MOVE 1 TO T-SUPERVISION
           END-IF.
B17        IF MET-PROF > 0
              MOVE MET-PROF TO CAR-PROF 
           END-IF.
           MOVE CAR-PROF TO T-CITP-88.
           IF CAR-PROF = 0
              MOVE "SL" TO LNK-AREA
              MOVE 63 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              PERFORM AA
              PERFORM END-PROGRAM
           END-IF.

           PERFORM LIVRE.
           PERFORM JOURS.
B18        MOVE L-UNI-REGUL-LIM TO T-HRS-TRAV.
B19        MOVE L-SNOCS-HEURES-OUVREES TO T-HRS-PAYE.
B20        COMPUTE SH-00 = L-UNI-HRS-SUP + L-UNI-MOBIL-PLUS.
           MOVE SH-00 TO T-HRS-SUP.
B21        MOVE L-IMPOSABLE-BRUT TO T-BRUT.
B22        COMPUTE SH-00 = L-SHS-FRANCHISE + L-MNT-HRS-SUPP.
           MOVE SH-00 TO T-SUPP. 
B23        MOVE L-NDF-FRANCHISE TO T-COMP.
B24        COMPUTE SH-00 = L-CAISSE-DEP-SAL + L-COTISATIONS.
           MOVE SH-00 TO T-COTIS.
B25        PERFORM PRES.
           MOVE SH-00 TO T-SEMAINE.
B26        COMPUTE SH-00 = LINK-IMPOS(1) + LINK-IMPOS(2)
           - LINK-DC(118) - LINK-DC(119) - LINK-DC(120)
           - LINK-DC(270) - LINK-DC(284).
           MOVE SH-00 TO T-BRUT-AN.
B27        COMPUTE SH-00 = LINK-DC(111) + LINK-DC(112) + LINK-DC(113).
           MOVE SH-00 TO T-GRAT-AN.
B28        COMPUTE SH-00 = LINK-DC(114) + LINK-DC(81) + LINK-DC(82)
           + LINK-DC(83) + LINK-DC(88) + LINK-DC(89).
           MOVE SH-00 TO T-PRIME-AN.
B29        COMPUTE SH-00 = LINK-DC(116) + LINK-DC(115).
           MOVE SH-00 TO T-BENEFICE.
B30        MOVE JRS-OCC(20) TO T-CONGE.
B31        IF JRS-OCC(48) > 0
              MOVE 2 TO T-PARENTAL
              MOVE MOIS-OCC(48) TO T-PAR-MOIS
           END-IF.
B31A       INITIALIZE CON-RECORD. 
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE 12        TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           PERFORM CONTRAT.
           IF CON-MOTIF-DEPART = MD-CODE
              MOVE 1 TO T-PARENTAL
              COMPUTE T-PAR-MOIS = 13 - CON-FIN-M
           END-IF.
           IF LNK-ANNEE = CON-DEBUT-A
              MOVE CON-DEBUT-M TO SH-00
              PERFORM CONTRAT
              IF CON-MOTIF-DEPART = MD-CODE
                 MOVE 1 TO T-PARENTAL
                 COMPUTE T-PAR-MOIS = SH-00
                 IF LNK-ANNEE = CON-FIN-A
                    COMPUTE T-PAR-MOIS = SH-00 - CON-FIN-M
                 END-IF
               END-IF
           END-IF.
B32        COMPUTE SH-00 = JRS-OCC(1) + JRS-OCC(10) + JRS-OCC(11).
           MOVE SH-00 TO T-JRS-MAL.
B33        MOVE JRS-OCC(11) TO T-JRS-MAL-PP.
B34        MOVE JRS-OCC(10) TO T-JRS-MATERNITE.
B35        COMPUTE SH-00 = JRS-OCC(13) + JRS-OCC(14).
           MOVE SH-00 TO T-FORMATION.
           WRITE T-RECORD.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD "X" EXC-KEY.
           IF REG-PERSON > 0
              IF REG-FIRME NOT = FR-KEY
              OR REG-FLAG NOT = "X"
                 GO NEXT-REGIS
              END-IF
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           INITIALIZE CAR-RECORD HJS-RECORD FICHE-RECORD CON-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD NUL-KEY.
           CALL "6-GHJS"  USING LINK-V HJS-RECORD CAR-RECORD PRESENCES.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           PERFORM CONTRAT.
           INITIALIZE MET-RECORD.
           IF CAR-METIER NOT = SPACES
              MOVE CAR-METIER TO MET-CODE
              CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY
           END-IF.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CONTRAT.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD PV-KEY.

       LIVRE.
           INITIALIZE L-RECORD LINK-RECORD LNK-VAL.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           PERFORM AN VARYING LNK-MOIS FROM 1 BY 1 UNTIL LNK-MOIS > 12.
           MOVE 10 TO LNK-MOIS.

       AN.
           CALL "4-LPADD" USING LINK-V REG-RECORD LINK-RECORD.

       PRES.
           MOVE 0 TO SH-00.
           PERFORM MOIS VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 12.
           COMPUTE SH-00 = SH-00 / 7.

       MOIS.
           PERFORM JOUR VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.

       JOUR.
           ADD PRES-JOUR(IDX-1, IDX) TO SH-00.

       JOURS.
           INITIALIZE JRS-RECORD DET-ABS.
           MOVE FR-KEY     TO JRS-FIRME-2.
           MOVE REG-PERSON TO JRS-PERSON-2.
           PERFORM NEXT-JOUR THRU NEXT-JOUR-END.

       NEXT-JOUR.
           CALL "6-JOUR2" USING LINK-V JRS-RECORD NX-KEY.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
              GO NEXT-JOUR-END
           END-IF.
           IF JRS-COMPLEMENT NOT = 0
           OR JRS-OCCUPATION     = 0
           OR JRS-POSTE      NOT = 0
           OR JRS-EXTENSION  NOT = SPACES
              GO NEXT-JOUR.
           MOVE JRS-OCCUPATION TO IDX-1.
           IF IDX-1 > 0 AND < 10
              MOVE 1 TO IDX-1.
           ADD 1 TO MOIS-OCC(IDX-1).
           IF JRS-JOURS = 0
              PERFORM A-H VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.
           ADD JRS-JOURS TO JRS-OCC(IDX-1).
           GO NEXT-JOUR.
       NEXT-JOUR-END.
           EXIT.

       A-H.
           IF JRS-HRS(IDX) > 0
              ADD 1 TO JRS-JOURS.



       INTERRUPTIONS.
           MOVE 0 TO SH-00.
           MOVE REG-ANCIEN-A TO LNK-ANNEE.
           PERFORM RECHERCHE VARYING LNK-ANNEE FROM REG-ANCIEN-A
           BY 1 UNTIL LNK-ANNEE > SAVE-ANNEE.
           COMPUTE SH-00 = SH-00 / 365.
           MOVE SAVE-ANNEE TO LNK-ANNEE.

       RECHERCHE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           MOVE  1 TO IDX-1.
           MOVE 12 TO IDX-2.
           IF REG-ANCIEN-A = LNK-ANNEE
              MOVE REG-ANCIEN-M TO IDX-1
           END-IF.
           PERFORM MOIS-P VARYING IDX-1 FROM IDX-1 BY 1 UNTIL IDX-1 > 
           IDX-2.

       MOIS-P.
           MOVE 1 TO IDX.
           IF  REG-ANCIEN-A = LNK-ANNEE
           AND REG-ANCIEN-M = IDX-1
              MOVE REG-ANCIEN-J TO IDX
           END-IF.
           PERFORM JOUR-P VARYING IDX FROM IDX BY 1 UNTIL IDX > 
           MOIS-JRS(IDX-1).

       JOUR-P.
           IF PRES-JOUR(IDX-1, IDX) = 0
              ADD 1 TO SH-00.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       DIS-HE-01.
           DISPLAY PARMOD-PATH LINE 8 POSITION 40.
           MOVE PARMOD-PROG-NUMBER-1 TO HE-Z2.
           DISPLAY HE-Z2 LINE 4 POSITION 49.
       DIS-HE-02. 
           MOVE MD-CODE TO HE-Z2.
           DISPLAY HE-Z2 LINE 6 POSITION 49.
           CALL "6-MOTDEP" USING LINK-V MD-RECORD FAKE-KEY.
           DISPLAY MD-NOM LINE 6 POSITION 52 SIZE 30.
       DIS-HE-END.
           EXIT.
       DIS-HE-IDX.

       AFFICHAGE-ECRAN.
           MOVE 2704 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           CANCEL "2-REGIS".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".

