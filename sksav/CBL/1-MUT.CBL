      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-MUT MUTUELLE                              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-MUT.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MUT.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "MUT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
             
       01  HE-INPUT-PARAMETER.
      *    LINE COL SIZE HE-SCREEN    LLCCSZECRA
           02 IP-101 PIC 9(10)  VALUE 0305250009.
           02 IP-102 PIC 9(10)  VALUE 0405250057.
           02 IP-103 PIC 9(10)  VALUE 0505251023.
           02 IP-DEC PIC 9(10)  VALUE 2305250099.

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
            03 I-PP OCCURS 4.
               04 IP-LINE  PIC 99.
               04 IP-COL   PIC 99.
               04 IP-SIZE  PIC 99.
               04 IP-ECRAN PIC 9999.

       01  CHOIX-MAX       PIC 99 VALUE 4.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
       01  SAVE-FIRME      PIC 9(6).

       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4). 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
      *袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴�

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MUTUELLE. 
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-MUT.

           OPEN I-O  MUTUELLE .

           MOVE FR-KEY   TO SAVE-FIRME.
           PERFORM AFFICHAGE-ECRAN .
           PERFORM MUT-RECENTE.
           PERFORM AFFICHAGE-DETAIL.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN CHOIX-MAX
                      MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11)
           END-EVALUATE.


       TRAITEMENT-ECRAN-01.

           PERFORM DISPLAY-F-KEYS.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1-1 
           WHEN  2 PERFORM AVANT-1-2 
           WHEN  3 PERFORM AVANT-1-3 
           WHEN CHOIX-MAX PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-1 
           WHEN  2 PERFORM APRES-1-2 
           WHEN CHOIX-MAX PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-1-1.
           ACCEPT FR-KEY 
             LINE 3 POSITION 17 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           IF MUT-ANNEE = 0 
              MOVE LNK-ANNEE TO MUT-ANNEE
              MOVE 1 TO MUT-MOIS
           END-IF.
           IF MUT-ANNEE < 2009
              MOVE 2009 TO MUT-ANNEE
              MOVE 1 TO MUT-MOIS
           END-IF.
           ACCEPT MUT-ANNEE
             LINE  4 POSITION 17 SIZE  4
             TAB UPDATE NO BEEP CURSOR 1 
             ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-1-W. 
           ACCEPT MUT-MOIS  
             LINE  5 POSITION 19 SIZE 2 
             TAB UPDATE NO BEEP CURSOR 1 
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.
           IF MUT-CLASSE = 0
              MOVE 1 TO MUT-CLASSE
           END-IF.
           ACCEPT MUT-CLASSE
             LINE 5 POSITION 20 SIZE 1 
             TAB UPDATE NO BEEP CURSOR 1 
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   CALL "2-FIRME" USING LINK-V 
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  OTHER PERFORM NEXT-FIRME.
           PERFORM MUT-RECENTE.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-1-2.
           EVALUATE EXC-KEY
           WHEN 65 PERFORM MUT-NEXT
                   PERFORM AFFICHAGE-DETAIL
                   MOVE 1 TO INPUT-ERROR
           WHEN 66 PERFORM MUT-NEXT
                   PERFORM AFFICHAGE-DETAIL
                   MOVE 1 TO INPUT-ERROR
           WHEN  2 CALL "2-MUT" USING LINK-V MUT-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           END-EVALUATE.
           IF MUT-ANNEE < 2009
              MOVE 2009 TO MUT-ANNEE.
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-3.
           IF MUT-CLASSE < 1
              MOVE 1 TO INPUT-ERROR
              MOVE 1 TO MUT-CLASSE.
           IF MUT-CLASSE > 5
              MOVE 1 TO INPUT-ERROR
              MOVE 5 TO MUT-CLASSE.
           PERFORM DIS-E1-03.


       APRES-DEC.
            MOVE FR-KEY TO MUT-FIRME.
            EVALUATE EXC-KEY 
            WHEN  5 CALL "0-TODAY" USING TODAY
                   MOVE TODAY-TIME TO MUT-TIME
                   MOVE LNK-USER TO MUT-USER
                   WRITE MUT-RECORD INVALID REWRITE MUT-RECORD
                   END-WRITE   
                   MOVE 1 TO DECISION 
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX
              MOVE CHOIX-MAX TO INDICE-ZONE.


       NEXT-FIRME.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.

       MUT-RECENTE.
           MOVE 999999 TO MUT-DATE.
           MOVE EXC-KEY TO SAVE-KEY.
           MOVE 65 TO EXC-KEY.
           PERFORM MUT-NEXT.
           MOVE SAVE-KEY TO EXC-KEY.

       MUT-NEXT.
           CALL "6-MUT" USING LINK-V MUT-RECORD EXC-KEY.

       ENTREE-SORTIE.
           MOVE "AA" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           PERFORM DISPLAY-MESSAGE.
           MOVE 0 TO LNK-NUM.
           
       DIS-E1-01.
           MOVE FR-KEY  TO HE-Z4.
           DISPLAY HE-Z4  LINE 3 POSITION 17.
           DISPLAY FR-NOM LINE 3 POSITION 25.

       DIS-E1-02.
           DISPLAY MUT-ANNEE  LINE  4 POSITION 17.

       DIS-E1-03.
           MOVE MUT-CLASSE  TO HE-Z2.
           DISPLAY HE-Z2 LINE 5 POSITION 19.

       DIS-E1-END.

       AFFICHAGE-ECRAN.
           MOVE 32 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           MOVE "EE" TO LNK-AREA.
           PERFORM LIN VARYING IDX FROM 1 BY 1 UNTIL IDX > 4.

       LIN.
           MOVE IDX TO HE-Z2.
           IF IDX < 4
              DISPLAY HE-Z2 LINE IP-LINE(IDX) POSITION 1 LOW.
           MOVE IP-ECRAN(IDX) TO LNK-NUM.
           MOVE IP-LINE(IDX)  TO LNK-LINE.
           MOVE IP-COL(IDX)   TO LNK-COL.
           MOVE IP-SIZE(IDX)  TO LNK-SIZE.
           MOVE "L" TO LNK-LOW.
           CALL "0-DMESS" USING LINK-V.

       AFFICHAGE-DETAIL.
           PERFORM DIS-E1-01 THRU DIS-E1-END.

       END-PROGRAM.
           CLOSE MUTUELLE.
           CANCEL "2-MUT".
           CANCEL "6-MUT".
           MOVE SAVE-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V "N" FAKE-KEY.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
             IF DECISION = 99 
                DISPLAY MUT-USER LINE 24 POSITION 5
                DISPLAY MUT-ST-JOUR  LINE 24 POSITION 15
                DISPLAY MUT-ST-MOIS  LINE 24 POSITION 18
                DISPLAY MUT-ST-ANNEE LINE 24 POSITION 21
                DISPLAY MUT-ST-HEURE LINE 24 POSITION 30
                DISPLAY MUT-ST-MIN   LINE 24 POSITION 33
                GO AVANT-DEC
             END-IF.
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
