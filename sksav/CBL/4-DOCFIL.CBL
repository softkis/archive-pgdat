      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-DOCFIL COMPLETER DOCUMENTS                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-DOCFIL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

       01  TEST-TEXT             PIC X(6).
       01  TEST-TEXT-R REDEFINES TEST-TEXT.
           02 TEST-TEXT-1       PIC X.
           02 TEST-TEXT-2       PIC X.
           02 TEST-TEXT-3       PIC X(4).
           COPY "MESSAGE.REC".
           COPY "PAYS.REC".
           COPY "BAREME.REC".
           COPY "V-VAR.CPY".
       01  LINE-NUMBER           PIC 999.
       01  LIN-NUM               PIC 999.
       01  DEC-NUM               PIC 999.
       01  EFFACER               PIC X(6) VALUE SPACES.
       01  REMPLACE              PIC X VALUE "�".
       01  ALPHA-TEXTE PIC X(60).
           COPY "EQUIPE.REC".
           COPY "COUT.REC".
       01  IDX-A                 PIC 9(6).
       01  IDX-B                 PIC 9(6).

       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ  BLANK WHEN ZERO.
           02 HE-Z2Z2 PIC ZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z3Z2 PIC ZZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).
           02 HE-Z4Z4 PIC Z.ZZZ,ZZZZ.
           02 HE-Z6Z2 PIC ZZZ.ZZZ,ZZ.
           02 HE-Z8 PIC Z.ZZZ.ZZZ.
           02 HE-DATE.
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HE-MATR.
              03 HM-AA PIC 9999.
              03 FILLER PIC X VALUE " ".
              03 HM-MM PIC 99.
              03 FILLER PIC X VALUE " ".
              03 HM-JJ PIC 99.
              03 FILLER PIC X VALUE " ".
              03 HM-SN PIC 999.

       01  LINK-ECRIT.
           02  TOTAL-CHIFFRE     PIC X(100).
           02  REDEF-CHIFFRE REDEFINES TOTAL-CHIFFRE.
               03 CHIFFRE   PIC X OCCURS 100.
           02  TOTAL-VAL         PIC 9(9)V9999.
           02  CHIFFRE-POINT     PIC 999 COMP-4.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "CCOL.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CONTRAT.REC".
           COPY "FICHE.REC".
           COPY "METIER.REC".
           COPY "V-BASES.REC".
           COPY "DOC.REC".

       01  TEXTES.
           02 FORM-LINE PIC X(19200).

       PROCEDURE DIVISION USING LINK-V
                                PR-RECORD
                                REG-RECORD
                                CAR-RECORD
                                CON-RECORD
                                CCOL-RECORD
                                FICHE-RECORD
                                MET-RECORD
                                BASES-REMUNERATION
                                DOC-RECORD
                                TEXTES.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-DOCFIL .


           MOVE 0 TO IDX-A.
           PERFORM TEST-FORM THRU TEST-FORM-END.
           EXIT PROGRAM.

       TEST-FORM.
           INSPECT TEXTES TALLYING IDX-A FOR CHARACTERS BEFORE "<".
           IF  IDX-A NOT = 0 
           AND IDX-A < 19200
              PERFORM FILL-IN
           ELSE
              GO TEST-FORM-END
           END-IF.
           MOVE 0 TO IDX-A.
           GO TEST-FORM.
       TEST-FORM-END.
           IF PR-CODE-SEXE = 2
              INSPECT TEXTES REPLACING ALL "#" BY "e"
           ELSE
              INSPECT TEXTES REPLACING ALL "#" BY " "
           END-IF.                
           INSPECT TEXTES REPLACING ALL "�" BY "<".

       FILL-IN.
           ADD 1 TO IDX-A.
           UNSTRING TEXTES INTO TEST-TEXT WITH POINTER IDX-A.
           SUBTRACT 6 FROM IDX-A.
           MOVE IDX-A TO IDX-B.
           IF  TEST-TEXT-2 NOT = " "
           AND TEST-TEXT-2 NOT = "-"
               PERFORM ERASE-IT.
           EVALUATE TEST-TEXT
                WHEN "<DT   " PERFORM DOC-TODAY
                              PERFORM DOC-DAT-N
                WHEN "<DD   " PERFORM DOC-DAT
                WHEN "<DDN  " PERFORM DOC-DAT-N
                WHEN "<DA   " PERFORM DOC-AUT
                WHEN "<D1   " PERFORM DOC-TXT-1
                WHEN "<D2   " PERFORM DOC-TXT-2
                WHEN "<D3   " PERFORM DOC-TXT-3
                WHEN "<D4   " PERFORM DOC-TXT-4
                WHEN "<D5   " PERFORM DOC-TXT-5
                WHEN "<D6   " PERFORM DOC-TXT-6
                WHEN "<VAL  " PERFORM VALEUR

                WHEN "<ANNEE" PERFORM ANNEE
                WHEN "<MOIS " PERFORM MOIS
                WHEN "<MOIS1" MOVE 1 TO DEC-NUM
                              PERFORM MOIS-N
                WHEN "<MOIS2" MOVE 2 TO DEC-NUM
                              PERFORM MOIS-N
                WHEN "<MOIS3" MOVE 3 TO DEC-NUM
                              PERFORM MOIS-N
                WHEN "<MOIS4" MOVE 4 TO DEC-NUM
                              PERFORM MOIS-N

                WHEN "<FNR  " PERFORM FIRM-NR 
                WHEN "<FN   " PERFORM FIRM-NOM
                WHEN "<FR   " PERFORM FIRM-RUE
                WHEN "<FCP  " PERFORM FIRM-POSTE
                WHEN "<FL   " PERFORM FIRM-LOCALITE 
                WHEN "<FTEL " PERFORM FIRM-PHONE
                WHEN "<FFAX " PERFORM FIRM-FAX
                WHEN "<FM   " PERFORM FIRM-MATR
                WHEN "<FRESP" PERFORM FIRM-RESP
                WHEN "<FACT " PERFORM FIRM-ACTIVITE

                WHEN "<PNR  " PERFORM PERS-NR 
                WHEN "<PT   " PERFORM PERS-TITRE
                WHEN "<PN   " PERFORM PERS-NOM
                WHEN "<PJF  " PERFORM PERS-NOM-JF
                WHEN "<PP   " PERFORM PERS-PRENOM
                WHEN "<PPN  " PERFORM PERS-PRENOM
                              PERFORM PERS-NOM
                WHEN "<PNP  " PERFORM PERS-NOM
                              PERFORM PERS-PRENOM
                WHEN "<PNN  " PERFORM PERS-NOM
                              PERFORM PERS-NOM-JF
                WHEN "<PNNP " PERFORM PERS-NOM
                              PERFORM PERS-NOM-JF
                              PERFORM PERS-PRENOM
                WHEN "<PR   " PERFORM PERS-RUE
                WHEN "<PCP  " PERFORM PERS-POSTE
                WHEN "<PL   " PERFORM PERS-LOCALITE 
                WHEN "<PM   " PERFORM PERS-MATR
                WHEN "<PMET " PERFORM PERS-METIER
                WHEN "<PMP  " PERFORM PERS-METIER
                              ADD 2 TO IDX-A
                              PERFORM PERS-POSITION
                WHEN "<PEQ  " PERFORM PERS-EQUIPE
                WHEN "<PEQN " PERFORM PERS-EQUIPE-NOM
                WHEN "<PCT  " PERFORM PERS-COUT
                WHEN "<PCTN " PERFORM PERS-COUT-NOM
                WHEN "<PPOS " PERFORM PERS-POSITION
                WHEN "<PLN  " PERFORM PERS-LIEU-NAISS
                WHEN "<PTEL " PERFORM PERS-PHONE
                WHEN "<PNAI " PERFORM PERS-NAISS
                WHEN "<PNAT " PERFORM PERS-NATION
                WHEN "<PEC  " PERFORM PERS-ETCIVIL
                WHEN "<PDEB " PERFORM PERS-DEBUT
                WHEN "<PANC " PERFORM PERS-ANCIEN
                WHEN "<PFIN " PERFORM PERS-FIN
                WHEN "<PAVI " PERFORM PERS-PREAVIS
                WHEN "<PESS " PERFORM PERS-ESSAI 
                WHEN "<PCON " PERFORM PERS-CONTRAT
                WHEN "<PSAL " PERFORM PERS-SAL
                WHEN "<PHJ  " PERFORM PERS-HJ
                WHEN "<PHS  " PERFORM PERS-HS
                WHEN "<PHM  " PERFORM PERS-HM
                WHEN "<MET "  PERFORM METIER
                WHEN "<INDEX" PERFORM INDEX-TAUX
                WHEN "<SALH " PERFORM SAL-HR
                WHEN "<SALM " PERFORM SAL-MOIS
                WHEN "<SHT  " PERFORM SAL-HR-TEXT
                WHEN "<SMT  " PERFORM SAL-MOIS-TEXT
                WHEN "<MAN  " PERFORM MANUEL
                WHEN "<INT  " PERFORM INTEL
                WHEN "<BAR  " PERFORM BAREME
                WHEN OTHER    PERFORM REMPLACE
           END-EVALUATE.

       DOC-TODAY.
           CALL "0-TODAY" USING TODAY.
           MOVE TODAY-JOUR  TO DOC-J.
           MOVE TODAY-MOIS  TO DOC-M.
           MOVE TODAY-ANNEE TO DOC-A.

       DOC-DAT.
           MOVE DOC-A TO HE-AA.
           MOVE DOC-M TO HE-MM.
           MOVE DOC-J TO HE-JJ.
           PERFORM STRING-DATE.

       DOC-DAT-N.
           MOVE DOC-A TO HE-AA.
           MOVE DOC-M TO HE-MM.
           MOVE DOC-J TO HE-JJ.
           STRING HE-JJ DELIMITED BY SIZE 
           INTO TEXTES POINTER IDX-A.
           ADD 1 TO IDX-A.
           MOVE DOC-M TO LNK-NUM.
050703     ADD 100 TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           STRING ALPHA-TEXTE DELIMITED BY " " INTO TEXTES
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-A.
           STRING HE-AA DELIMITED BY SIZE INTO TEXTES POINTER IDX-A.

       BAREME.
           IF CAR-BAREME NOT = SPACES
           AND CAR-ECHELON > 0
             INITIALIZE BAR-RECORD
             MOVE CAR-BAREME TO BAR-BAREME
             MOVE CAR-GRADE  TO BAR-GRADE
             MOVE CAR-ANNEE  TO BAR-ANNEE
             MOVE CAR-MOIS   TO BAR-MOIS
             CALL "6-BAREME" USING LINK-V BAR-RECORD NUL-KEY
             STRING BAR-DESCRIPTION DELIMITED BY "  "
             INTO TEXTES POINTER IDX-A END-STRING
             ADD 1 TO IDX-A
             STRING CAR-ECHELON DELIMITED BY SIZE 
             INTO TEXTES POINTER IDX-A END-STRING
           END-IF.

       DOC-AUT.
           STRING DOC-AUTEUR DELIMITED BY SIZE
           INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       DOC-TXT-1.
           STRING DOC-T(1) DELIMITED BY "   " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.
       DOC-TXT-2.
           STRING DOC-T(2) DELIMITED BY "   " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.
       DOC-TXT-3.
           STRING DOC-T(3) DELIMITED BY "   " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.
       DOC-TXT-4.
           STRING DOC-T(4) DELIMITED BY "   " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.
       DOC-TXT-5.
           STRING DOC-T(5) DELIMITED BY "   " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.
       DOC-TXT-6.
           STRING DOC-T(6) DELIMITED BY "   " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.


       FIRM-NR.
           MOVE FR-KEY TO HE-Z4.
           STRING HE-Z4 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       FIRM-NOM.
           STRING FR-NOM DELIMITED BY "  " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       FIRM-RUE.
           STRING FR-MAISON DELIMITED BY "  " INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-A.
           STRING FR-RUE DELIMITED BY "  " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.
  
       FIRM-POSTE.
           STRING FR-PAYS DELIMITED BY " " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-A.
           MOVE FR-CODE-POST TO HE-Z5.
           STRING HE-Z5 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       FIRM-LOCALITE. 
           STRING FR-LOCALITE DELIMITED BY "   " INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.

       FIRM-PHONE.
           STRING FR-PHONE DELIMITED BY "   " INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.
       FIRM-FAX.
           STRING FR-FAX DELIMITED BY "   " INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.

       FIRM-MATR.
           STRING FR-ETAB-A DELIMITED BY SIZE 
           INTO TEXTES POINTER IDX-A.
           ADD 1 TO IDX-A.
           STRING FR-ETAB-N DELIMITED BY SIZE 
           INTO TEXTES POINTER IDX-A.
           ADD 1 TO IDX-A.
           STRING FR-SNOCS DELIMITED BY SIZE 
           INTO TEXTES POINTER IDX-A.
           ADD 1 TO IDX-A.
           STRING FR-EXTENS DELIMITED BY SIZE 
           INTO TEXTES POINTER IDX-A.

       FIRM-RESP.
           STRING FR-CORRESPONDANT DELIMITED BY "  " INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.

       FIRM-ACTIVITE.
           STRING FR-ACTIVITE DELIMITED BY "  " INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.

       PERS-NR.
           MOVE REG-PERSON TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       PERS-TITRE.
           MOVE PR-POLITESSE TO LNK-NUM.
           IF LNK-NUM = 0
              MOVE PR-CODE-SEXE TO LNK-NUM.
           MOVE "P" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           STRING LNK-TEXT DELIMITED BY "  " INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.

       PERS-NOM.
           STRING PR-NOM DELIMITED BY "  " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-A.

       PERS-NOM-JF.
           STRING PR-NOM-JF DELIMITED BY "  " INTO TEXTES POINTER IDX-A
           ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-A.

       PERS-PRENOM.
           STRING PR-PRENOM DELIMITED BY "  " INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-A.

       PERS-RUE.
           STRING PR-MAISON DELIMITED BY "  " INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-A.
           STRING PR-RUE DELIMITED BY "   " INTO TEXTES POINTER IDX-A
           ON OVERFLOW CONTINUE END-STRING.

       PERS-POSTE.
           STRING PR-PAYS DELIMITED BY " " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-A.
           IF PR-PAYS = "D" OR "F" OR "CZ" OR "SK" OR "I" OR "PL"
              STRING PR-CP5 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
              ON OVERFLOW CONTINUE END-STRING
           ELSE
              STRING PR-CP4 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
              ON OVERFLOW CONTINUE END-STRING
           END-IF.

       PERS-LOCALITE. 
           STRING PR-LOCALITE DELIMITED BY "   " INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.

       PERS-LIEU-NAISS. 
           STRING PR-LIEU-NAISSANCE DELIMITED BY SIZE INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.

       PERS-MATR.
           INITIALIZE HE-MATR.
           MOVE PR-NAISS-A TO HM-AA.
           MOVE PR-NAISS-M TO HM-MM.
           MOVE PR-NAISS-J TO HM-JJ.
           IF PR-SNOCS > 9
               MOVE PR-SNOCS TO HM-SN.
           STRING HE-MATR DELIMITED BY SIZE INTO TEXTES POINTER IDX-A.


       PERS-METIER.
           MOVE PR-LANGUAGE TO MET-LANGUE.
           MOVE CAR-METIER TO MET-CODE
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           
           STRING MET-NOM(PR-CODE-SEXE) DELIMITED BY "  "
           INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       METIER.
           STRING MET-NOM(PR-CODE-SEXE) DELIMITED BY "  "
           INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       PERS-EQUIPE.
           MOVE CAR-EQUIPE TO HE-Z4.
           STRING HE-Z4 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       PERS-EQUIPE-NOM.
           MOVE CAR-EQUIPE TO EQ-NUMBER.
           CALL "6-EQUIPE" USING LINK-V EQ-RECORD FAKE-KEY.
           STRING EQ-NOM DELIMITED BY "  " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       PERS-COUT.
           MOVE CAR-COUT TO HE-Z4.
           STRING HE-Z4 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       PERS-COUT-NOM.
           MOVE CAR-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           STRING COUT-NOM DELIMITED BY "  " INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       PERS-POSITION.
           STRING CAR-POSITION DELIMITED BY "   "
           INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       PERS-NAISS.
           MOVE PR-NAISS-A TO HE-AA.
           MOVE PR-NAISS-M TO HE-MM.
           MOVE PR-NAISS-J TO HE-JJ.
           PERFORM STRING-DATE.

       PERS-DEBUT.
           MOVE CON-DEBUT-A TO HE-AA.
           MOVE CON-DEBUT-M TO HE-MM.
           MOVE CON-DEBUT-J TO HE-JJ.
           PERFORM STRING-DATE.

       PERS-ANCIEN.
           IF REG-ANCIEN-A = 0
              MOVE CON-DATE-DEBUT TO REG-DATE-ANCIEN.
           MOVE REG-ANCIEN-A TO HE-AA.
           MOVE REG-ANCIEN-M TO HE-MM.
           MOVE REG-ANCIEN-J TO HE-JJ.
           PERFORM STRING-DATE.

       PERS-FIN.
           MOVE CON-FIN-A TO HE-AA.
           MOVE CON-FIN-M TO HE-MM.
           MOVE CON-FIN-J TO HE-JJ.
           PERFORM STRING-DATE.

       PERS-ESSAI.
           MOVE CON-ESSAI-A TO HE-AA.
           MOVE CON-ESSAI-M TO HE-MM.
           MOVE CON-ESSAI-J TO HE-JJ.
           PERFORM STRING-DATE.

       PERS-CONTRAT.
           MOVE CON-CONTRAT-A TO HE-AA.
           MOVE CON-CONTRAT-M TO HE-MM.
           MOVE CON-CONTRAT-J TO HE-JJ.
           PERFORM STRING-DATE.

       PERS-PREAVIS.
           MOVE CON-PREAVIS-A TO HE-AA.
           MOVE CON-PREAVIS-M TO HE-MM.
           MOVE CON-PREAVIS-J TO HE-JJ.
           PERFORM STRING-DATE.

       STRING-DATE.
           STRING HE-DATE DELIMITED BY SIZE INTO TEXTES POINTER IDX-A.

       PERS-NATION.
           MOVE PR-NATIONALITE TO PAYS-CODE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.
           STRING PAYS-NATION DELIMITED BY SIZE
           INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.


       PERS-SAL.
           IF CAR-SAL-100 = 0
              PERFORM SAL-MIN.
           MOVE CAR-SAL-ACT TO HE-Z6Z2.
           PERFORM STRING-SAL.

       SAL-HR.
           MOVE BAS-HORAIRE TO HE-Z4Z4.
           STRING HE-Z4Z4 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE.

       SAL-MOIS.
           MOVE BAS-MOIS TO HE-Z6Z2.
           PERFORM STRING-SAL.

       STRING-SAL.
           STRING HE-Z6Z2 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE.


       SAL-HR-TEXT.
           MOVE BAS-HORAIRE TO TOTAL-VAL.
           PERFORM STRING-SALT.

       SAL-MOIS-TEXT.
           MOVE BAS-MOIS TO TOTAL-VAL.
           PERFORM STRING-SALT.

       STRING-SALT.
           CALL "4-ECRIT" USING LINK-ECRIT.
           INSPECT TOTAL-CHIFFRE CONVERTING
           "ABCDEFGHIJKLMNOPQRSTUVWXYZ릾�" TO
           "abcdefghijklmnopqrstuvwxyz굙�".
           STRING TOTAL-CHIFFRE DELIMITED BY "   " INTO TEXTES POINTER 
           IDX-A ON OVERFLOW CONTINUE.

       PERS-HJ.
           MOVE CAR-HRS-JOUR TO HE-Z2Z2.
           STRING HE-Z2Z2 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A.

       PERS-HS.
           COMPUTE SH-00 = CAR-HRS-JOUR * CAR-JRS-SEMAINE.
           MOVE SH-00 TO HE-Z3.
           STRING HE-Z3 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A.

       PERS-HM.
           MOVE CAR-HRS-MOIS TO HE-Z3Z2.
           STRING HE-Z3Z2 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A.

       PERS-PHONE.
           STRING PR-TELEPHONE DELIMITED BY "   " INTO TEXTES 
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.

       INDEX-TAUX.
           COMPUTE SH-00 = MOIS-IDX(LNK-MOIS) * 100.
           MOVE SH-00 TO HE-Z3Z2.
           STRING HE-Z3Z2 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       ANNEE.
           MOVE LNK-ANNEE TO HE-Z4.
           STRING HE-Z4 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       MOIS.
           MOVE LNK-MOIS TO HE-Z2.
           STRING HE-Z2 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       VALEUR.
           MOVE LNK-VAL  TO HE-Z6Z2.
           STRING HE-Z6Z2 DELIMITED BY SIZE INTO TEXTES POINTER IDX-A 
           ON OVERFLOW CONTINUE END-STRING.

       STRING-ALPHA.
           STRING ALPHA-TEXTE DELIMITED BY "  " INTO TEXTES
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.

       MANUEL.
           IF CAR-STATUT = 1
              MOVE "Man" TO ALPHA-TEXTE
              PERFORM STRING-ALPHA.
       INTEL.
           IF CAR-STATUT > 1
              MOVE "Int" TO ALPHA-TEXTE
              PERFORM STRING-ALPHA.

       MOIS-N.
           PERFORM MOIS-NOM.
           STRING ALPHA-TEXTE DELIMITED BY " " INTO TEXTES
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.

       ERASE-IT.
           STRING EFFACER DELIMITED BY SIZE INTO TEXTES POINTER IDX-B 
           ON OVERFLOW CONTINUE END-STRING.

       REMPLACE.
           STRING REMPLACE DELIMITED BY SIZE INTO TEXTES POINTER IDX-B 
           ON OVERFLOW CONTINUE END-STRING.


           COPY "XMOISNOM.CPY".

       PERS-ETCIVIL.
           EVALUATE FICHE-ETAT-CIVIL
                WHEN "C" MOVE 1 TO MS-NUMBER 
                WHEN "M" MOVE 2 TO MS-NUMBER 
                WHEN "V" MOVE 3 TO MS-NUMBER 
                WHEN "D" MOVE 4 TO MS-NUMBER 
                WHEN "S" MOVE 5 TO MS-NUMBER 
           END-EVALUATE.
           IF PR-CODE-SEXE = 2
           AND LNK-LANGUAGE = "F"
               ADD 10 TO MS-NUMBER.
           MOVE "EC" TO LNK-AREA.
           CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY.
           STRING MS-DESCRIPTION DELIMITED BY " " INTO TEXTES
           POINTER IDX-A ON OVERFLOW CONTINUE END-STRING.




       SAL-MIN.
           COMPUTE CAR-SAL-ACT = MOIS-SALMIN(CON-DEBUT-M)
           IF CAR-HOR-MEN = 0
              COMPUTE CAR-SAL-ACT = CAR-SAL-ACT / 173
           END-IF.
           IF CAR-POURCENT <= 0
              MOVE 100 TO CAR-POURCENT
           END-IF.
           COMPUTE CAR-SAL-ACT = CAR-SAL-ACT / 100 * CAR-POURCENT.

           COPY "XACTION.CPY".

