      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-LPX CONTROLE DETAIL FICHIER LIVRE PAIE    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-LPX.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  ECRAN-SUITE.
           02 ECR-S1             PIC  9999 COMP-1 VALUE 2511.
           02 ECR-S2             PIC  9999 COMP-1 VALUE 2512.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S              PIC  9999 COMP-1 OCCURS 2.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "LIVRE.REC".
           COPY "LIVREX.REC".

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE 5.
           02  CHOIX-MAX-2       PIC 99 VALUE 1. 
           02  CHOIX-MAX-3       PIC 99 VALUE 1. 
           02  CHOIX-MAX-4       PIC 99 VALUE 1. 
           02  CHOIX-MAX-5       PIC 99 VALUE 1. 
           02  CHOIX-MAX-6       PIC 99 VALUE 1. 
           02  CHOIX-MAX-7       PIC 99 VALUE 1. 
           02  CHOIX-MAX-8       PIC 99 VALUE 1. 
           02  CHOIX-MAX-9       PIC 99 VALUE 1. 
           02  CHOIX-MAX-10      PIC 99 VALUE 1. 
           02  CHOIX-MAX-11      PIC 99 VALUE 1. 
           02  CHOIX-MAX-12      PIC 99 VALUE 1. 
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 12.

       01  AJUSTE                PIC 99 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-DATE.
              03 HEJJ PIC ZZ  BLANK WHEN ZERO.
              03 FILLER PIC X VALUE ".".
              03 HEMM PIC ZZ  BLANK WHEN ZERO.
              03 FILLER PIC X VALUE ".".
              03 HEAA PIC ZZZZ  BLANK WHEN ZERO.
           02 HEZ2    PIC ZZ.
           02 HEZ3    PIC ZZZ.
           02 HEZ4    PIC ZZZZ.
           02 HEZ6    PIC Z(6).
           02 HEZ3Z2  PIC ZZZ,ZZ BLANK WHEN ZERO.
           02 HEZ6Z2  PIC Z(7),ZZ BLANK WHEN ZERO.
           02 HEZ6Z6  PIC Z(7),Z(6) BLANK WHEN ZERO.
           02 HEZ8    PIC -Z(8) BLANK WHEN ZERO.
           02 HEZ8Z2  PIC Z(8),ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-LPX.

           INITIALIZE L-RECORD.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
            UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions
           IF ECRAN-IDX = 1
              MOVE 0000680000 TO EXC-KFR (14) 
              EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0163604015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2 THRU 4
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           END-IF.        

           IF ECRAN-IDX > 1 
              MOVE 0029000000 TO EXC-KFR(1)
              MOVE 0000000065 TO EXC-KFR(13)
              MOVE 6667680000 TO EXC-KFR(14) 
              MOVE 0052000000 TO EXC-KFR(11)
           END-IF.    

           IF ECRAN-IDX = 12
              MOVE 0100000000 TO EXC-KFR (1)
              MOVE 0067000000 TO EXC-KFR (14) 
           END-IF.    

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
              WHEN  1 PERFORM AVANT-1-1 
              WHEN  2 PERFORM AVANT-1-2 
              WHEN  3 PERFORM AVANT-1-3 
              WHEN  4 PERFORM AVANT-1-4 
              WHEN  5 PERFORM AVANT-DEC.

           IF ECRAN-IDX > 1 AND ECRAN-IDX < 13
              PERFORM AVANT-DEC
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF ECRAN-IDX = 1 AND EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.


           IF EXC-KEY > 66 
              IF DECISION > 12
                 MOVE 12 TO DECISION
              END-IF
              IF DECISION > 0 
                 MOVE DECISION TO ECRAN-IDX
              ELSE
                 IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX END-IF
                 IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX END-IF
              END-IF
              MOVE 0 TO DECISION
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
              INITIALIZE DECISION
              GO TRAITEMENT-ECRAN
           END-IF.
           
           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
              WHEN  1 PERFORM APRES-1-1 
              WHEN  2 PERFORM APRES-1-2 
              WHEN  3 PERFORM APRES-1-3 
              WHEN  4 PERFORM APRES-1-4 
              WHEN  5 PERFORM APRES-DEC.

           IF ECRAN-IDX > 1 
           AND ECRAN-IDX < 13
           AND INDICE-ZONE = 1
              PERFORM APRES-DEC
           END-IF.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 52 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
                WHEN 53 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
           END-EVALUATE. 
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.            
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE  3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.            
           ACCEPT REG-MATCHCODE
             LINE  3 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.            
           IF L-MOIS  = 0 
              MOVE LNK-MOIS  TO L-MOIS.
           ACCEPT L-MOIS  
             LINE  6 POSITION 19 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE L-MOIS TO HEZ2.
           DISPLAY HEZ2 LINE  6 POSITION 19.

       AVANT-1-4. 
           ACCEPT L-SUITE 
             LINE  7 POSITION 19 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE L-SUITE TO HEZ2.
           DISPLAY HEZ2 LINE  7 POSITION 19.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-1-1.
           MOVE FR-KEY TO REG-FIRME
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-HE-01 THRU DIS-HE-02
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.
           IF PRES-ANNEE = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-01 THRU DIS-HE-02.
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-NUM LNK-PERSON
           ELSE
              PERFORM GET-LIVRE
              PERFORM DIS-HE-01 THRU DIS-E1-01.
           
       APRES-1-2.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-REGIS
           END-EVALUATE.                     
           PERFORM GET-LIVRE.
           PERFORM DIS-HE-01 THRU DIS-E1-01.
           
       APRES-1-3.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-LIVRE
                    PERFORM DIS-HE-01 THRU DIS-HE-END.
           IF L-MOIS > 12
              MOVE 12 TO L-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF L-MOIS < 1
              MOVE 1 TO L-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0
           PERFORM READ-LIVRE.
           PERFORM DIS-HE-01 THRU DIS-E1-01.

       APRES-1-4.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-LIVRE
           WHEN OTHER PERFORM READ-LIVRE.
           PERFORM DIS-E1-01.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  1 MOVE 01001009 TO LNK-VAL
                    PERFORM HELP-SCREEN
                    MOVE 1 TO INPUT-ERROR
           WHEN   2 CALL "2-LEX" USING LINK-V LEX-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                     PERFORM AFFICHAGE-DETAIL
            WHEN  65 THRU 66 PERFORM NEXT-LIVRE
                     PERFORM AFFICHAGE-DETAIL
            WHEN 27 COMPUTE DECISION = INDICE-ZONE + 1
            WHEN 52 COMPUTE DECISION = INDICE-ZONE + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.
           
       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       GET-LIVRE.
           MOVE FR-KEY TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON.
           MOVE LNK-MOIS  TO L-MOIS.
           MOVE 0 TO L-SUITE.
           PERFORM READ-LIVRE.

       NEXT-LIVRE.
           CALL "4-NXLP" USING LINK-V 
                               REG-RECORD 
                               L-RECORD
                               EXC-KEY.
       READ-LIVRE.
           CALL "4-NXLP" USING LINK-V 
                               REG-RECORD 
                               L-RECORD
                               FAKE-KEY.

       DIS-HE-01.
           MOVE REG-PERSON  TO HEZ6.
           DISPLAY HEZ6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.

       DIS-HE-03.
           MOVE L-MOIS     TO HEZ2.
           DISPLAY HEZ2  LINE  6 POSITION 19.
       DIS-HE-05.
           MOVE L-SUITE    TO HEZ2.
           DISPLAY HEZ2  LINE  7 POSITION 19.
       DIS-HE-END.
           EXIT.

       DIS-E1-01.
           MOVE L-JOUR-DEBUT  TO HEZ4.
           DISPLAY HEZ4 LINE  8 POSITION 35.
           MOVE L-JOUR-FIN    TO HEZ4.
           DISPLAY HEZ4 LINE  8 POSITION 40.
           MOVE L-EDIT-A TO HEAA.
           MOVE L-EDIT-M TO HEMM.
           MOVE L-EDIT-J TO HEJJ.
           DISPLAY HE-DATE LINE 10 POSITION 40.
           MOVE L-CERT-IMP-A TO HEAA.
           MOVE L-CERT-IMP-M TO HEMM.
           MOVE L-CERT-IMP-J TO HEJJ.
           DISPLAY HE-DATE LINE 11 POSITION 40.


      *    MOVE L-ANNEE-A1 TO HEAA.
      *    MOVE L-MOIS-A1  TO HEMM.
      *    MOVE L-JOUR-A1  TO HEJJ.
      *    DISPLAY HE-DATE LINE 11 POSITION 40.
      *    MOVE L-ANNEE-A2 TO HEAA.
      *    MOVE L-MOIS-A2  TO HEMM.
      *    MOVE L-JOUR-A2  TO HEJJ.
      *    DISPLAY HE-DATE LINE 12 POSITION 40.
           MOVE L-SNOCS-A TO HEAA.
           MOVE L-SNOCS-M TO HEMM.
           MOVE L-SNOCS-J TO HEJJ.
           DISPLAY HE-DATE LINE 15 POSITION 40.
           MOVE L-DERNIER-A TO HEAA.
           MOVE L-DERNIER-M TO HEMM.
           MOVE L-DERNIER-J TO HEJJ.
           DISPLAY HE-DATE LINE 16 POSITION 40.
           MOVE L-REPRISE-A TO HEAA.
           MOVE L-REPRISE-M TO HEMM.
           MOVE L-REPRISE-J TO HEJJ.
           DISPLAY HE-DATE LINE 17 POSITION 40.
           MOVE L-3M-A TO HEZ4.
           DISPLAY HEZ4 LINE 18 POSITION 40.
           MOVE L-3M-M TO HEZ4.
           DISPLAY HEZ4 LINE 18 POSITION 45.


           DISPLAY L-STATUT   LINE  8 POSITION 65.
           DISPLAY L-CODE-SEXE LINE 8 POSITION 76.
           DISPLAY L-REGIME   LINE  9 POSITION 72.
           MOVE L-CLASSE-I    TO HEZ4.
           DISPLAY HEZ4        LINE 10 POSITION 70.
           MOVE L-COUT          TO HEZ4.
           DISPLAY HEZ4        LINE 11 POSITION 70.
           MOVE L-EQUIPE        TO HEZ4.
           DISPLAY HEZ4        LINE 12 POSITION 70.
           MOVE L-DIVISION      TO HEZ4.
           DISPLAY HEZ4        LINE 13 POSITION 70.

           MOVE    L-SAL-HOR      TO HEZ6Z6.
           DISPLAY HEZ6Z6 LINE 14 POSITION 62.
           MOVE    L-SAL-HOR-P    TO HEZ6Z6.
           DISPLAY HEZ6Z6 LINE 15 POSITION 62.
           MOVE    L-SAL-MOIS     TO HEZ6Z2
           DISPLAY HEZ6Z2 LINE 16 POSITION 64.
           MOVE    L-SAL-HOR-CONGE TO HEZ8Z2.
           DISPLAY HEZ8Z2 LINE 17 POSITION 62.
           MOVE    L-SAL-MOY-MAL   TO HEZ8Z2.
           DISPLAY HEZ8Z2 LINE 18 POSITION 62.

           DISPLAY L-STATEC LINE 19 POSITION 70.
           DISPLAY L-TEMPS-PARTIEL LINE 20 POSITION 70.


       AFFICHAGE-UNITES.
           MOVE 0 TO IDX-2.
           PERFORM DISPLAY-VAL VARYING IDX  FROM 0 BY 1 UNTIL 
                    IDX > 9.
       DISPLAY-VAL.
           COMPUTE COL-IDX = 1 + IDX * 8.
           PERFORM DISPLAY-DET-VAL VARYING IDX-1 FROM 1 BY 1 UNTIL 
                    IDX-1 > 20.

       DISPLAY-DET-VAL.
           ADD 1 TO IDX-2.
           COMPUTE LIN-IDX = 2 + IDX-1.
           MOVE IDX-2 TO HEZ2.
           DISPLAY HEZ2 LINE LIN-IDX POSITION COL-IDX LOW.
           MOVE L-UNITE(IDX-2)  TO HEZ3Z2.
           COMPUTE IDX-4 = COL-IDX + 2.
           DISPLAY HEZ3Z2 LINE LIN-IDX POSITION IDX-4 HIGH.

       TABS-1.
           SUBTRACT 100 FROM IDX.
           PERFORM TABS.
           ADD 100 TO IDX.

       TABS.
           COMPUTE COL-IDX = (IDX - 1) / 20.
           COMPUTE LIN-IDX = IDX - (20 * COL-IDX) + 2.
           COMPUTE COL-IDX = COL-IDX  * 16 + 1.
           MOVE IDX  TO HEZ2.
           DISPLAY HEZ2 LINE LIN-IDX POSITION COL-IDX LOW.

       TABS-2.
           COMPUTE COL-IDX = (IDX - 1) / 20.
           COMPUTE LIN-IDX = IDX - (20 * COL-IDX) + 2.
           COMPUTE COL-IDX = COL-IDX  * 20 + 1.
           MOVE IDX  TO HEZ2.
           DISPLAY HEZ2 LINE LIN-IDX POSITION COL-IDX LOW.

       DIS-DEB.
           PERFORM TABS.
           IF L-DEB(IDX) > 0
              MOVE IDX TO LEX-NUMBER
              MOVE  L-DEB(IDX) TO HEZ6Z2
              PERFORM DIS-LEX.
       DIS-DEB-2.
           PERFORM TABS-1.
           IF L-DEB(IDX) > 0
              MOVE IDX TO LEX-NUMBER
              MOVE  L-DEB(IDX) TO HEZ6Z2
              PERFORM DIS-LEX.

       DIS-LEX.
           CALL "6-LEX" USING LINK-V LEX-RECORD FAKE-KEY.
           MOVE 0 TO IDX-3.
           INSPECT HEZ6Z2 TALLYING IDX-3 FOR ALL " ".
           ADD 5 TO IDX-3.
           MOVE COL-IDX TO IDX-1.
           ADD 6 TO COL-IDX
           ADD AJUSTE TO IDX-3 COL-IDX.
           INSPECT HEZ6Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ6Z2 LINE LIN-IDX POSITION COL-IDX HIGH.
           DISPLAY LEX-DESCRIPTION LINE LIN-IDX POSITION IDX-1
           SIZE IDX-3 REVERSE.

       DIS-CRE.
           PERFORM TABS.
           IF L-CRE(IDX) > 0
              MOVE IDX TO LEX-NUMBER
              ADD  200 TO LEX-NUMBER
              MOVE  L-CRE(IDX) TO HEZ6Z2
              PERFORM DIS-LEX.

       DIS-COT.
           PERFORM TABS-2.
           IF L-COT(IDX) > 0
              MOVE IDX TO LEX-NUMBER
              ADD  400 TO LEX-NUMBER
              MOVE  L-COT(IDX) TO HEZ6Z2
              PERFORM DIS-LEX.

       DIS-ABAT.
           PERFORM TABS-2.
           IF L-ABAT(IDX) > 0
              MOVE IDX TO LEX-NUMBER
              ADD  300 TO LEX-NUMBER
              MOVE  L-ABAT(IDX) TO HEZ6Z2
              PERFORM DIS-LEX.

       DIS-IMP.
           PERFORM TABS-2.
           IF L-IMPOS(IDX) > 0
              MOVE IDX TO LEX-NUMBER
              ADD  500 TO LEX-NUMBER
              MOVE  L-IMPOS(IDX) TO HEZ6Z2
              PERFORM DIS-LEX.

       DIS-MOY.
           PERFORM TABS-2.
           IF L-MOY(IDX) > 0
              MOVE IDX TO LEX-NUMBER
              ADD  600 TO LEX-NUMBER
              MOVE  L-MOY(IDX) TO HEZ6Z2
              PERFORM DIS-LEX.

       DIS-SNOCS.
           PERFORM TABS-2.
           IF L-SNOCS(IDX) > 0
              MOVE IDX TO LEX-NUMBER
              ADD  700 TO LEX-NUMBER
              MOVE  L-SNOCS(IDX) TO HEZ6Z2
              PERFORM DIS-LEX.

       DIS-JOURS.
           PERFORM TABS-2.
           IF L-JRS(IDX) > 0
              MOVE IDX TO LEX-NUMBER
              ADD  1000 TO LEX-NUMBER
              MOVE  L-JRS(IDX) TO HEZ6Z2
              PERFORM DIS-LEX.

       DIS-FLAG.
           PERFORM TABS-2.
           IF L-FLAG(IDX) > 0
              MOVE IDX TO LEX-NUMBER
              ADD  800 TO LEX-NUMBER
              MOVE  L-FLAG(IDX) TO HEZ6Z2
              PERFORM DIS-LEX.

       AFFICHAGE-ECRAN.
           IF ECRAN-IDX = 1
              MOVE ECR-S(1) TO LNK-VAL
           ELSE
              MOVE ECR-S(2) TO LNK-VAL
           END-IF.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           MOVE 0 TO AJUSTE.
           EVALUATE ECRAN-IDX
               WHEN 1 PERFORM DIS-HE-01 THRU DIS-E1-01
               WHEN 2 PERFORM AFFICHAGE-UNITES
               WHEN 3 PERFORM DIS-DEB VARYING IDX FROM 1 BY 1
                      UNTIL IDX > 100
                      DISPLAY "DEB" LINE 1 POSITION 40
               WHEN 4 PERFORM DIS-DEB-2 VARYING IDX FROM 101 BY 1
                      UNTIL IDX > 200
                      DISPLAY "DEB 2" LINE 1 POSITION 40
               WHEN 5 PERFORM DIS-CRE VARYING IDX FROM 1 BY 1
                      UNTIL IDX > 100
                      DISPLAY "CRE" LINE 1 POSITION 40
               WHEN 6 MOVE 4 TO AJUSTE
                      PERFORM DIS-COT VARYING IDX FROM 1 BY 1
                      UNTIL IDX > 70
                      DISPLAY "COT" LINE 1 POSITION 40
               WHEN 7 MOVE 15 TO AJUSTE
                      PERFORM DIS-ABAT VARYING IDX FROM 1 BY 1
                      UNTIL IDX > 10
                      DISPLAY "ABAT" LINE 1 POSITION 40
               WHEN 8 MOVE 4 TO AJUSTE
                      PERFORM DIS-IMP VARYING IDX FROM 1 BY 1
                      UNTIL IDX > 40
                      DISPLAY "IMP" LINE 1 POSITION 40
               WHEN 9 MOVE 15 TO AJUSTE
                      PERFORM DIS-MOY VARYING IDX FROM 1 BY 1
                      UNTIL IDX > 10
                      DISPLAY "MOY" LINE 1 POSITION 40
               WHEN 10 MOVE 15 TO AJUSTE
                      PERFORM DIS-SNOCS VARYING IDX FROM 1 BY 1
                      UNTIL IDX > 10
                      DISPLAY "SNOCS" LINE 1 POSITION 40
               WHEN 11 MOVE 4 TO AJUSTE
                      PERFORM DIS-JOURS VARYING IDX FROM 1 BY 1
                      UNTIL IDX > 70
                      DISPLAY "JOURS" LINE 1 POSITION 40
               WHEN 12 MOVE 15 TO AJUSTE
                       PERFORM DIS-FLAG VARYING IDX FROM 1 BY 1
                      UNTIL IDX > 10
                      DISPLAY "FLAG" LINE 1 POSITION 40
           END-EVALUATE.
           IF ECRAN-IDX > 1
              PERFORM DIS-HE-MOIS. 

       DIS-HE-MOIS.
           MOVE REG-PERSON  TO HEZ6.
           DISPLAY HEZ6 LINE 24 POSITION 5 SIZE 6
           DISPLAY PR-NOM LINE 24 POSITION 12 SIZE 40.
           MOVE L-MOIS TO HEZ2.
           DISPLAY HEZ2 LINE  24 POSITION 69.
           MOVE L-SUITE TO HEZ2.
           DISPLAY HEZ2 LINE  24 POSITION 72.
           

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
           
