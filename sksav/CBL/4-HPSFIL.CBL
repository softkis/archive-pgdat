      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-HPSFIL GENERER HEURES CONTRATS            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-HPSFIL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "HOPSEM.REC".
           COPY "V-VAR.CPY".

       01  JOUR-IDX          PIC 99.
       01  JOUR-DEB          PIC 99.
       01  JOUR-FIN          PIC 99.
       01  JOUR              PIC 99.
       01  NOT-OPEN          PIC 9 VALUE 0.
       01  COMPLEMENT        PIC 99 VALUE 0.
       77  HELP-5     PIC S999V99 COMP-3.

       01  TOTAL-GLOBAL.
           03 T-G PIC 9 OCCURS 31.

       01  PLANS.
           03 T-P PIC 999V99 OCCURS 32.

       01  JOURS-NAME.
           02 JOURS-ID           PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "CCOL.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "CALEN.REC".

       PROCEDURE DIVISION USING LINK-V
                                REG-RECORD
                                PRESENCES
                                CAR-RECORD
                                CCOL-RECORD
                                CAL-RECORD.
       
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-4-HPSFIL.

           INITIALIZE HPS-RECORD.
           CALL "6-GHPS" USING LINK-V HPS-RECORD REG-RECORD.

           IF HPS-FIRME = 0
              EXIT PROGRAM
           END-IF.              
       
           IF NOT-OPEN = 0
              MOVE LNK-SUFFIX TO ANNEE-JOURS
              OPEN I-O JOURS
              MOVE 1 TO NOT-OPEN.

           PERFORM CLEAN-OLD-TOTAL.

           INITIALIZE TOTAL-GLOBAL HPS-RECORD PLANS.
           PERFORM INIT-HEURES THRU INIT-HEURES-END.
           PERFORM HPS THRU HPS-END.

       HPS.
           CALL "6-GHPS" USING LINK-V HPS-RECORD REG-RECORD.
           IF HPS-FIRME = 0
              GO HPS-END.
           MOVE 1 TO JOUR-DEB.
           MOVE MOIS-JRS(LNK-MOIS) TO JOUR-FIN.
           IF  HPS-F-A = LNK-ANNEE
           AND HPS-F-M = LNK-MOIS
               MOVE HPS-F-J TO JOUR-FIN.
           IF  HPS-A = LNK-ANNEE
           AND HPS-M = LNK-MOIS
               MOVE HPS-J TO JOUR-DEB.
           MOVE 0 TO IDX.
           PERFORM JRS-KEYS.
           PERFORM FILL-HEURE VARYING JOUR-IDX FROM JOUR-DEB BY 1 
               UNTIL JOUR-IDX > JOUR-FIN.
           PERFORM WRITE-JOURS.
           IF HPS-NUIT(8) > 0
              MOVE 1 TO IDX
              PERFORM JRS-KEYS
              PERFORM FILL-NUIT VARYING JOUR-IDX FROM JOUR-DEB BY 1 
              UNTIL JOUR-IDX > JOUR-FIN
              PERFORM WRITE-JOURS
           END-IF.
           IF HPS-TRAV-FERIE NOT = "N"
           AND CCOL-TAUX-LIBRE(1) > 0
           AND CCOL-FACT-REPOS(1) > 0
              MOVE 11 TO IDX
              PERFORM JRS-KEYS
              PERFORM FILL-FERIE VARYING JOUR-IDX FROM JOUR-DEB BY 1 
              UNTIL JOUR-IDX > JOUR-FIN
              PERFORM WRITE-JOURS
           END-IF.
           IF CCOL-TAUX-LIBRE(10) > 0
           OR CCOL-FACT-REPOS(10) > 0
              MOVE 20 TO IDX
              PERFORM JRS-KEYS
              PERFORM FILL-DIM VARYING JOUR-IDX FROM JOUR-DEB BY 1 
              UNTIL JOUR-IDX > JOUR-FIN
              PERFORM WRITE-JOURS
           END-IF.
           GO HPS.
       HPS-END.
           MOVE 0 TO IDX.
           PERFORM JRS-KEYS.
           PERFORM FILL-PLAN VARYING JOUR-IDX FROM 1 BY 1 UNTIL 
                   JOUR-IDX > 32.
           MOVE 99 TO JRS-OCCUPATION   JRS-OCCUPATION-1 
                      JRS-OCCUPATION-2 JRS-OCCUPATION-3.
           INITIALIZE HPS-RECORD.
           PERFORM WRITE-JOURS.
           MOVE REG-PERSON TO LNK-PERSON.
           CALL "4-HPADD" USING LINK-V.
           MOVE 0 TO LNK-PERSON.
           EXIT PROGRAM.

       FILL-HEURE.
           MOVE SEM-IDX(LNK-MOIS, JOUR-IDX) TO IDX-3.
           MOVE HPS-HEURES(IDX-3) TO HELP-5.
           ADD HELP-5 TO T-P(JOUR-IDX).
           IF CAL-JOUR(LNK-MOIS, JOUR-IDX) > 0 
           AND HPS-TRAV-FERIE = "N"
              MOVE 0 TO HELP-5
           END-IF.
           PERFORM ADD-UP.

       FILL-NUIT.
           MOVE SEM-IDX(LNK-MOIS, JOUR-IDX) TO IDX-3.
           MOVE HPS-NUIT(IDX-3) TO HELP-5.
           IF CAL-JOUR(LNK-MOIS, JOUR-IDX) > 0 
           AND HPS-TRAV-FERIE = "N"
              MOVE 0 TO HELP-5
           END-IF.
           PERFORM ADD-UP.

       FILL-FERIE.
           MOVE SEM-IDX(LNK-MOIS, JOUR-IDX) TO IDX-3.
           IF CAL-JOUR(LNK-MOIS, JOUR-IDX) > 0 
              MOVE HPS-HEURES(IDX-3) TO HELP-5
              PERFORM ADD-UP
           END-IF.

       FILL-DIM.
           MOVE SEM-IDX(LNK-MOIS, JOUR-IDX) TO IDX-3.
           IF SEM-IDX(LNK-MOIS, JOUR-IDX) = 7 
              MOVE HPS-HEURES(IDX-3) TO HELP-5
              PERFORM ADD-UP
           END-IF.

       ADD-UP.
           IF T-G(JOUR-IDX) = 1 
           OR PRES-JOUR(LNK-MOIS, JOUR-IDX) = 0
              MOVE 0 TO HELP-5
           END-IF.
           ADD HELP-5 TO JRS-HRS(JOUR-IDX) JRS-HRS(32).

       FILL-PLAN.
           IF PRES-JOUR(LNK-MOIS, JOUR-IDX) > 0
              ADD T-P(JOUR-IDX) TO JRS-HRS(JOUR-IDX) JRS-HRS(32).

       JRS-KEYS.
           INITIALIZE JRS-RECORD.
           MOVE FR-KEY TO JRS-FIRME JRS-FIRME-1 JRS-FIRME-2 JRS-FIRME-3.
           MOVE REG-PERSON TO 
                JRS-PERSON JRS-PERSON-1 JRS-PERSON-2 JRS-PERSON-3.
           MOVE LNK-MOIS TO JRS-MOIS JRS-MOIS-1 JRS-MOIS-2 JRS-MOIS-3.

       INIT-HEURES.
           INITIALIZE TOTAL-GLOBAL.
           PERFORM JRS-KEYS.
           START JOURS KEY >= JRS-KEY INVALID GO INIT-HEURES-END.

       INIT-HEURES-1.
           READ JOURS NEXT NO LOCK AT END GO INIT-HEURES-END.
           IF FR-KEY     NOT = JRS-FIRME 
           OR LNK-MOIS   NOT = JRS-MOIS 
           OR REG-PERSON NOT = JRS-PERSON 
              GO INIT-HEURES-END.
           MOVE 0 TO IDX.
           IF  JRS-OCCUPATION > 0
           AND JRS-OCCUPATION < 12
              MOVE 1 TO IDX.
           IF  JRS-OCCUPATION > 19
           AND JRS-OCCUPATION < 31
              MOVE 1 TO IDX.
           IF JRS-OCCUPATION = 16
              MOVE 1 TO IDX.
           IF IDX > 0
              PERFORM ADD-GLOBAL VARYING JOUR-IDX FROM 1 BY 1 
              UNTIL JOUR-IDX > 31.
           GO INIT-HEURES-1.
       INIT-HEURES-END.
           EXIT.
       
       ADD-GLOBAL.
           IF JRS-HRS(JOUR-IDX) NOT = 0
              MOVE 1 TO T-G(JOUR-IDX)
           END-IF.

       WRITE-JOURS.
           MOVE HPS-POSTE TO 
                JRS-POSTE JRS-POSTE-1 JRS-POSTE-2 JRS-POSTE-3.
           MOVE IDX TO JRS-COMPLEMENT   JRS-COMPLEMENT-1
                       JRS-COMPLEMENT-2 JRS-COMPLEMENT-3.
           IF JRS-HRS(32) NOT = 0
              IF LNK-SQL = "Y" 
                 CALL "9-JOURS" USING LINK-V JRS-RECORD WR-KEY 
              END-IF
              WRITE JRS-RECORD INVALID CONTINUE END-WRITE
           END-IF.

       CLEAN-OLD-TOTAL.
           PERFORM JRS-KEYS.
           START JOURS KEY >= JRS-KEY INVALID CONTINUE
           NOT INVALID
                PERFORM DELETE-JOURS THRU DELETE-JRS-END.
       DELETE-JOURS.
           READ JOURS NEXT AT END
               GO DELETE-JRS-END
           END-READ.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
           OR JRS-OCCUPATION > 0
              GO DELETE-JRS-END
           END-IF.
           IF LNK-SQL = "Y" 
              CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY 
           END-IF.
           DELETE JOURS INVALID CONTINUE.
           GO DELETE-JOURS.
       DELETE-JRS-END.
           PERFORM JRS-KEYS.
           MOVE 99 TO JRS-OCCUPATION.
           IF LNK-SQL = "Y" 
              CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY 
           END-IF.
           DELETE JOURS INVALID CONTINUE.

           COPY "XACTION.CPY".
