      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-MATR LISTE CONTROLE MATRICULES            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-MATR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "PERSON.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM80.FDE".
           COPY "PERSON.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "IMPRLOG.REC".
           COPY "REGISTRE.REC".
           COPY "V-VAR.CPY".

       01  LANGUE                PIC 9.
       01  SAVE-FIRME  PIC 9999.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".MAT".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).


       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                PERSONNE
                FORM.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-MATR.
       
           OPEN INPUT PERSONNE.

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           INITIALIZE LIN-IDX LIN-NUM.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

           MOVE 0000000025 TO EXC-KFR(1).
           MOVE 1700000000 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.

           PERFORM APRES-DEC.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
            EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-PERSON
                    PERFORM READ-PERSON THRU READ-PERSON-END
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       START-PERSON.
           INITIALIZE PR-RECORD.
           START PERSONNE KEY > PR-KEY INVALID PERFORM END-PROGRAM.

       READ-PERSON.
           READ PERSONNE NEXT AT END 
                GO READ-PERSON-END
           END-READ.
           IF PR-SNOCS > 9
              GO READ-PERSON
           END-IF.
           IF PR-DOC-ID = "TEST"
              GO READ-PERSON
           END-IF.

           MOVE 1 TO INPUT-ERROR.
           PERFORM FICEL.
           IF INPUT-ERROR = 1 
              GO READ-PERSON
           END-IF.
           PERFORM DIS-HE-01.
           IF COUNTER = 0 
              MOVE 70 TO IMPL-MAX-LINE
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE IDX = LIN-NUM + 3.
           IF IDX >= IMPL-MAX-LINE
              PERFORM PAGE-DATE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           PERFORM FILL-FILES.
           GO READ-PERSON.
       READ-PERSON-END.
           IF LIN-NUM > LIN-IDX
              PERFORM PAGE-DATE
              PERFORM TRANSMET.
           PERFORM END-PROGRAM.
                
       DIS-HE-01.
           DISPLAY PR-MATRICULE  LINE  7 POSITION 35.
           DISPLAY PR-NOM    LINE  7 POSITION 45.
           DISPLAY PR-PRENOM LINE  8 POSITION 45.
       DIS-HE-END.
           EXIT.

       FILL-FILES.

      * DONNEES PERSONNE
           MOVE 1 TO COL-NUM.
           MOVE PR-NAISS-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 6 TO COL-NUM.
           MOVE PR-NAISS-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 9 TO COL-NUM.
           MOVE PR-NAISS-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 18 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           IF PR-NOM-JF NOT = SPACES
               ADD 1 TO COL-NUM
               MOVE PR-NOM-JF TO ALPHA-TEXTE
               PERFORM FILL-FORM.

           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.


           ADD 1 TO LIN-NUM.
           MOVE 18 TO COL-NUM.
           CALL "4-PRRUE" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           ADD 1 TO LIN-NUM.
           MOVE 18 TO COL-NUM.
           CALL "4-PRLOC" USING LINK-V PR-RECORD
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 2 TO LIN-NUM.

       PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  4 TO CAR-NUM.
           MOVE  2 TO LIN-NUM.
           MOVE 73 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 52 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 55 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 58 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 1290 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".

       FICEL.
           INITIALIZE REG-RECORD.
           MOVE PR-MATRICULE TO REG-MATRICULE.
           PERFORM READ-REG THRU READ-REG-END.
           
       READ-REG.
           CALL "6-REG" USING LINK-V REG-RECORD NX-KEY.
           IF PR-MATRICULE NOT = REG-MATRICULE
              GO READ-REG-END.
           MOVE REG-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V "N" FAKE-KEY.
           IF FR-REMARQUE = "TEST"
              GO READ-REG
           ELSE
              MOVE 0 TO INPUT-ERROR
           END-IF.
       READ-REG-END.
           EXIT.