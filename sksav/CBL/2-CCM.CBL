      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-CCM RECHERCHE CENTRES DE COUT MULTIPLES   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-CCM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CCM.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.

       01  CHOIX                 PIC 9(6).
       01  DATUM                 PIC 9(8).
       01  COMPTEUR              PIC 99.

       01 HE-CCM.
          02 H-C OCCURS 20.
             03 H-A PIC 9(4).
             03 H-M PIC 99.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ.
           02 HE-Z3 PIC ZZZ.
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z2Z2 PIC ZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8).
           02 HE-ACCEPT.
              03 HE-A     PIC Z(4).
              03 HE-M     PIC ZZ.
           02 HE-TEMP.
              03 HE-JJ    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-MM    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-AA    PIC ZZZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "CCM.LNK".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD PR-RECORD REG-RECORD.

       START-DISPLAY SECTION.
             
       START-2-CCM.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE CCM-RECORD IDX-1 EXC-KEY COMPTEUR CHOIX HE-CCM.
           MOVE 3 TO LIN-IDX.
           MOVE 9999 TO CCM-ANNEE.
           PERFORM READ-CCM THRU READ-CCM-END.
           IF EXC-KEY = 66 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-CCM.
           MOVE 65 TO EXC-KEY.
           CALL "6-CCM" USING LINK-V CCM-RECORD REG-RECORD EXC-KEY.
           IF CCM-FIRME = 0
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-CCM IDX-1
                 GO READ-CCM
              END-IF
              GO READ-CCM-END.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-CCM IDX-1
                 IF EXC-KEY = 65 
                    MOVE 9999 TO CCM-ANNEE
                 END-IF
                 GO READ-CCM
              END-IF
              IF CHOIX NOT = 0
                 GO READ-CCM-END
              END-IF
           END-IF.
           GO READ-CCM.
       READ-CCM-END.
           IF CHOIX = 0
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82 AND NOT = 68
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           PERFORM CLEAN-SCREEN.
           ADD 1 TO COMPTEUR IDX-1.
           MOVE CCM-MOIS  TO HE-Z2 H-M(IDX-1).
           MOVE CCM-ANNEE TO H-A(IDX-1).
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 2.
           DISPLAY CCM-ANNEE LINE LIN-IDX POSITION 5.
           PERFORM DIS-H-DET VARYING IDX FROM 1 BY 1 UNTIL IDX > 4.
           PERFORM CLEAN-SCREEN.
           PERFORM DIS-H-DET VARYING IDX FROM 5 BY 1 UNTIL IDX > 8.
           
       DIS-H-DET.
           MOVE IDX TO IDX-2.
           IF IDX-2 > 4
              SUBTRACT 4 FROM IDX-2.
           COMPUTE COL-IDX = 15 * IDX-2.
           MOVE CCM-COUT(IDX) TO HE-Z8.
           DISPLAY HE-Z8 LINE LIN-IDX POSITION COL-IDX.
           COMPUTE COL-IDX = 15 * IDX-1 + 10.
           MOVE CCM-TAUX(IDX) TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION COL-IDX LOW.


       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000065 TO EXC-KFR (13).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0000530000 TO EXC-KFR (11).
           MOVE 6667000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE CHOIX.
           ACCEPT CHOIX 
              LINE 3 POSITION 2 SIZE 1
              TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82
           OR EXC-KEY = 67
              EXIT PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 13
              INITIALIZE HE-CCM
              GO INTERRUPT-END.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF CHOIX NOT = 0
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 3 TO LIN-IDX.
           MOVE 0 TO IDX-1.

       AFFICHAGE-ECRAN.
           MOVE 2170 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE REG-PERSON TO HE-Z6
           DISPLAY HE-Z6 LINE 3 POSITION 10
           CALL "0-PRNOM" USING LINK-V PR-RECORD
           DISPLAY LNK-TEXT LINE 3 POSITION 20 SIZE 50.

       END-PROGRAM.
           INITIALIZE CCM-RECORD.
           IF IDX-1 > 0
              MOVE H-A(IDX-1) TO CCM-ANNEE
              MOVE H-M(IDX-1) TO CCM-MOIS
           END-IF.
           CALL "6-CCM" USING LINK-V CCM-RECORD REG-RECORD FAKE-KEY.
           IF CCM-ANNEE NOT = 0
              MOVE CCM-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = (IDX-1 * 2) + 2.
           MOVE H-M(IDX-1) TO HE-M.
           ACCEPT HE-M
             LINE  LIN-IDX POSITION 2 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           MOVE H-M(IDX-1) TO HE-M.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           DISPLAY HE-M  LINE LIN-IDX POSITION 2.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN OTHER
                     COMPUTE CHOIX = H-A(IDX-1) * 100 + H-M(IDX-1)
                     PERFORM END-PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-A(IDX-1) = 0
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.
                        