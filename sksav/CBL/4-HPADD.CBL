      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-HPADD CUMUL  DES HEURES PAR POSTES        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-HPADD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  JOUR-IDX              PIC 99 VALUE 0.

       01  TOTAL-COMPL.
           02 TOT-COMPL OCCURS 21.
              03 TOT-HRS PIC S9(6)V99 COMP-3 OCCURS 32.
           02 T-V OCCURS 10.
              03 TOT-VAL PIC S9(6)V99 COMP-3 OCCURS 32.

       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999 VALUE 0.

       77  NOT-OPEN     PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-4-HPADD.

           IF  ANNEE-JOURS > 0 
           AND LNK-SUFFIX NOT = ANNEE-JOURS
               CLOSE JOURS
           END-IF.

           IF ANNEE-JOURS = 0 
              MOVE LNK-SUFFIX TO ANNEE-JOURS
              OPEN I-O JOURS
           END-IF.

           PERFORM CUMUL-JRS THRU CUMUL-END.
           EXIT PROGRAM.

*****************************************************************

       CUMUL-JRS.
           INITIALIZE JRS-RECORD TOTAL-COMPL.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE LNK-PERSON TO JRS-PERSON.
           MOVE LNK-MOIS   TO JRS-MOIS.
           IF LNK-SQL = "Y" 
              CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY 
           END-IF.
           DELETE JOURS INVALID CONTINUE.
           START JOURS KEY > JRS-KEY INVALID EXIT PROGRAM.

       CUMUL-1.
           READ JOURS NEXT NO LOCK AT END
                GO CUMUL-END
           END-READ.
           IF JRS-FIRME      NOT = FR-KEY       
           OR JRS-PERSON     NOT = LNK-PERSON
           OR JRS-MOIS       NOT = LNK-MOIS     
           OR JRS-OCCUPATION NOT = 0
              GO CUMUL-END
           END-IF.
           IF JRS-OCCUPATION = 0
           AND JRS-POSTE = 0
              IF LNK-SQL = "Y" 
                 CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY 
              END-IF
              DELETE JOURS INVALID CONTINUE END-DELETE
           GO CUMUL-1.
           MOVE JRS-COMPLEMENT TO IDX-1.
           IF JRS-COMPLEMENT = 0
              MOVE 21 TO IDX-1
           END-IF.
           IF JRS-COMPLEMENT < 21
              PERFORM FILL-OCC VARYING JOUR-IDX FROM 1 BY 1 UNTIL 
              JOUR-IDX > 32
           END-IF.
           IF JRS-COMPLEMENT > 20
              COMPUTE IDX-1 = JRS-COMPLEMENT - 20
              PERFORM FILL-VAL VARYING JOUR-IDX
                     FROM 1 BY 1 UNTIL JOUR-IDX > 32
           END-IF.
           GO CUMUL-1.
       CUMUL-END.
           PERFORM OCCUPATIONS.

       OCCUPATIONS.
           INITIALIZE JRS-RECORD.
           MOVE FR-KEY TO JRS-FIRME JRS-FIRME-1 JRS-FIRME-2 JRS-FIRME-3.
           MOVE LNK-PERSON TO JRS-PERSON   JRS-PERSON-1 
                              JRS-PERSON-2 JRS-PERSON-3.
           MOVE LNK-MOIS TO JRS-MOIS JRS-MOIS-1 JRS-MOIS-2 JRS-MOIS-3.
           PERFORM WRITE-OCC THRU WRITE-OCC-END
           VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 21.

           PERFORM WRITE-VAL THRU WRITE-VAL-END
           VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 10.

       FILL-OCC.
           ADD JRS-HRS(JOUR-IDX) TO TOT-HRS(IDX-1, JOUR-IDX).

       FILL-VAL.
           ADD JRS-VAL(JOUR-IDX) TO TOT-VAL(IDX-1, JOUR-IDX).

       WRITE-OCC.
           IF TOT-HRS(IDX-1, 32) = 0
              GO WRITE-OCC-END.
           IF IDX-1 = 21
              MOVE 0 TO JRS-COMPLEMENT   JRS-COMPLEMENT-1
                        JRS-COMPLEMENT-2 JRS-COMPLEMENT-3
           ELSE
              MOVE IDX-1 TO JRS-COMPLEMENT   JRS-COMPLEMENT-1
                            JRS-COMPLEMENT-2 JRS-COMPLEMENT-3
           END-IF.
           MOVE TOT-COMPL(IDX-1) TO JRS-HEURES.
           MOVE 0 TO JRS-JOURS.
           PERFORM ADD-JRS VARYING IDX-2 FROM 1 BY 1 UNTIL IDX-2 > 31.
           CALL "0-TODAY" USING TODAY
           MOVE TODAY-TIME TO JRS-TIME
           MOVE LNK-USER TO JRS-USER.
           IF LNK-SQL = "Y" 
              CALL "9-JOURS" USING LINK-V JRS-RECORD WR-KEY 
           END-IF.
           WRITE JRS-RECORD INVALID REWRITE JRS-RECORD.
       WRITE-OCC-END.
           EXIT.

       ADD-JRS.
           IF JRS-HRS(IDX-2) > 0
              ADD 1 TO JRS-JOURS.

       WRITE-VAL.
           IF TOT-VAL(IDX-1, 32) = 0
              GO WRITE-VAL-END.
           MOVE T-V(IDX-1) TO JRS-VALEUR.
           COMPUTE SH-00 = IDX-1 + 20.
           MOVE SH-00 TO JRS-COMPLEMENT JRS-COMPLEMENT-1
                         JRS-COMPLEMENT-2 JRS-COMPLEMENT-3.
           WRITE JRS-RECORD INVALID REWRITE JRS-RECORD.
       WRITE-VAL-END.
           EXIT.

                      