      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-FISC  GESTION DES BUREAUX FISCAUX         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-FISC.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FISC.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FISC.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "C-P.REC".
           COPY "BANQUE.REC".
       01  TEST-COMPTE           PIC X(50). 

       01  CHOIX-MAX             PIC 99 VALUE 13.

       01   ECR-DISPLAY.
            02 HE-Z8 PIC Z(8).
            02 HE-Z3Z2 PIC ZZZ,ZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
      *袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴�
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FISC.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-FISC.
       
           OPEN I-O FISC.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0129000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3     MOVE 0029000028 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 11    MOVE 0017000015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 13    MOVE 0100000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052005400 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-10
           WHEN 11 PERFORM AVANT-11
           WHEN 12 PERFORM AVANT-12
           WHEN 13 PERFORM AVANT-DEC.


           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  3 PERFORM APRES-3 
           WHEN 11 PERFORM APRES-11
           WHEN 12 PERFORM APRES-12
           WHEN 13 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT FISC-KEY 
             LINE  4 POSITION 30 SIZE 20
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT FISC-NOM 
             LINE  5 POSITION 30 SIZE 50
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-3.            
           ACCEPT FISC-CODE-POST
             LINE  6 POSITION 30 SIZE 4 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-4.            
           ACCEPT FISC-MAISON  
             LINE  7 POSITION 12 SIZE  6 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-5.            
           ACCEPT FISC-RUE     
             LINE  7 POSITION 30 SIZE  40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-6.            
           ACCEPT FISC-LOCALITE
             LINE  8 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.            
           ACCEPT FISC-POB
             LINE  9 POSITION 30 SIZE  40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.            
           ACCEPT FISC-PHONE
             LINE 10 POSITION 30 SIZE 20
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-9.            
           ACCEPT FISC-FAX
             LINE 11 POSITION 30 SIZE 20
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-10.
           ACCEPT FISC-CORRESPONDANT
             LINE 12 POSITION 30 SIZE 30
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-11.
           ACCEPT FISC-BANQUE
             LINE 13 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-12.
           ACCEPT FISC-COMPTE
             LINE 14 POSITION 30 SIZE 30
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-FISC
           WHEN   2 CALL "2-FISC" USING LINK-V FISC-RECORD
                    PERFORM AFFICHAGE-ECRAN.
           READ FISC INVALID INITIALIZE FISC-REC-DET END-READ.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

           
       APRES-3.
           MOVE "L" TO FISC-PAYS.
           EVALUATE EXC-KEY
           WHEN  2 MOVE "A" TO LNK-A-N
                   CALL "2-CDPOST" USING LINK-V CP-RECORD
                   CANCEL "2-CDPOST"
                   PERFORM AFFICHAGE-ECRAN 
                   IF CP-CODE > 0
                      MOVE CP-CODE TO FISC-CODE-POST 
                      MOVE CP-RUE TO FISC-RUE
                      MOVE CP-LOCALITE TO FISC-LOCALITE
                   END-IF
                   PERFORM AFFICHAGE-DETAIL
           WHEN 65 THRU 66 
                   IF FISC-CODE-POST  NOT = CP-CODE
                      INITIALIZE CP-RECORD
                      MOVE FISC-CODE-POST TO CP-CODE
                      MOVE FISC-LOCALITE TO CP-LOCALITE
                   CALL "6-CDPOST" USING LINK-V CP-RECORD "N" FAKE-KEY
                   END-IF
                   PERFORM NEXT-CDPOST
                   IF CP-CODE > 0
                      MOVE CP-CODE TO FISC-CODE-POST 
                      MOVE CP-RUE TO FISC-RUE
                      MOVE CP-LOCALITE TO FISC-LOCALITE
                   END-IF
                   PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.
           IF FISC-LOCALITE = SPACES
           OR EXC-KEY = 5
              MOVE FISC-CODE-POST TO CP-CODE
              CALL "6-CDPOST" USING LINK-V CP-RECORD "N" FAKE-KEY
              MOVE CP-RUE TO FISC-RUE
              MOVE CP-LOCALITE TO FISC-LOCALITE
              CANCEL "6-CDPOST" 
           END-IF.
           PERFORM AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.


       APRES-11.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-BANQUE" USING LINK-V BQ-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-HE-01 THRU DIS-HE-END
                   PERFORM DISPLAY-F-KEYS
                   IF BQ-CODE NOT = SPACES
                      MOVE BQ-CODE TO FISC-BANQUE 
                   END-IF
                   MOVE 1 TO INPUT-ERROR
           WHEN   5 MOVE FISC-BANQUE TO LNK-TEXT
                    CALL "1-BANQUE" USING LINK-V
                    IF LNK-TEXT NOT = SPACES
                       MOVE LNK-TEXT TO FISC-BANQUE BQ-CODE
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
           END-EVALUATE.
           MOVE FISC-BANQUE TO BQ-CODE.
           PERFORM NEXT-BANQUE.
           IF BQ-CODE NOT = SPACES
              MOVE BQ-CODE TO FISC-BANQUE.
           IF FISC-BANQUE NOT = SPACES
           AND BQ-CODE = SPACES 
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       APRES-12.
           MOVE FISC-COMPTE TO TEST-COMPTE
           CALL "0-IBAN" USING LINK-V TEST-COMPTE
           MOVE TEST-COMPTE TO FISC-COMPTE
           IF LNK-NUM NOT = 0
              MOVE "SL" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           MOVE 0 TO IDX.
           INSPECT FISC-COMPTE TALLYING IDX FOR CHARACTERS BEFORE " ".
           IF IDX < 20
               MOVE 54 TO LNK-NUM
               MOVE "SL" TO LNK-AREA
               PERFORM DISPLAY-MESSAGE
               MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-12.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO FISC-TIME
                    MOVE LNK-USER TO FISC-USER
                    IF LNK-SQL = "Y" 
                       CALL "9-FISC" USING LINK-V FISC-RECORD WR-KEY
                    END-IF
                    WRITE FISC-RECORD INVALID 
                        REWRITE FISC-RECORD
                    END-WRITE   
                    MOVE FISC-KEY TO LNK-TEXT
                    MOVE 1 TO DECISION
                    PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN  8 DELETE FISC  INVALID CONTINUE
                    END-DELETE
                    IF LNK-SQL = "Y" 
                       CALL "9-FISC" USING LINK-V FISC-RECORD DEL-KEY
                    END-IF
                    INITIALIZE FISC-RECORD
                    MOVE 1 TO DECISION
                    PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION > 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       READ-FISC.
           MOVE EXC-KEY TO SAVE-KEY.
           MOVE 13 TO EXC-KEY.
           PERFORM NEXT-FISC.
           MOVE SAVE-KEY TO EXC-KEY.
           
       NEXT-FISC.
           CALL "6-FISC" USING LINK-V FISC-RECORD EXC-KEY.
                
       NEXT-BANQUE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD EXC-KEY.

       NEXT-CDPOST.
           CALL "6-CDPOST" USING LINK-V CP-RECORD "N" EXC-KEY.


       DIS-HE-01.
           DISPLAY FISC-KEY LINE  4 POSITION 30.
           IF EXC-KEY NOT = 5 MOVE FISC-KEY TO LNK-TEXT.
       DIS-HE-02.
           DISPLAY FISC-NOM LINE  5 POSITION 30.
       DIS-HE-03.
           DISPLAY FISC-CODE-POST LINE 6 POSITION 30.
       DIS-HE-04.
           DISPLAY FISC-MAISON LINE  7 POSITION 12.
       DIS-HE-05.
           DISPLAY FISC-RUE LINE  7 POSITION 30.
       DIS-HE-06. 
           DISPLAY FISC-LOCALITE LINE  8 POSITION 30.
       DIS-HE-07.
           DISPLAY FISC-POB  LINE  9 POSITION 30.
       DIS-HE-08. 
           DISPLAY FISC-PHONE LINE 10 POSITION 30.
       DIS-HE-09. 
           DISPLAY FISC-FAX LINE 11 POSITION 30.
       DIS-HE-10.
           DISPLAY FISC-CORRESPONDANT LINE 12 POSITION 30.
       DIS-HE-11.
           DISPLAY FISC-BANQUE LINE 13 POSITION 30.
       DIS-HE-12.
           DISPLAY FISC-COMPTE LINE 14 POSITION 30.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 17 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE FISC.
           CANCEL "2-FISC"
           CANCEL "6-FISC"
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
           
