      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-STATEC RAPPORT STATEC MENSUEL             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-STATEC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "LIVRE.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "LIVRE.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CCOL.REC".
           COPY "CALEN.REC".

       01  JOUR-IDX              PIC 9.
       01  JOUR                  PIC 99.
       01  CHOIX-MAX             PIC 99 VALUE 1 . 

       01  LIVRE-NAME.
           02 FILLER             PIC X(8) VALUE "S-LIVRE.".
           02 ANNEE-LIVRE        PIC 999.

       77  HELP-1                PIC 9(8) COMP-1.
       77  HELP-2                PIC 9(8) COMP-1.
       77  COMPTEUR              PIC 9(8) COMP-1.

       01  HELP-CUMUL.           
           02 HELP-701  PIC 9(8)V99 .
           02 HELP-702  PIC 9(8)V99 .
           02 HELP-703  PIC 9(8)V99 .
           02 HELP-704  PIC 9(8)V99 .
           02 HELP-711  PIC 9(8)V99 .
           02 HELP-712  PIC 9(8)V99 .
           02 HELP-713  PIC 9(8)V99 .
           02 HELP-721  PIC 9(8)V99 .
           02 HELP-722  PIC 9(8)V99 .
           02 HELP-723  PIC 9(8)V99 .
           02 HELP-731  PIC 9(8)V99 .
           02 HELP-732  PIC 9(8)V99 .
           02 HELP-733  PIC 9(8)V99 .
           02 HELP-734  PIC 9(8)V99 .

       01  ECR-DISPLAY.
           02 HE-Z2    PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4    PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6    PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z8    PIC Z(8) BLANK WHEN ZERO.
           02 HE-Z8Z2  PIC Z(8),ZZ BLANK WHEN ZERO.

       01  JOURS-TRAVAIL    PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON LIVRE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-STATEC.
       
           MOVE LNK-SUFFIX TO ANNEE-LIVRE.
           OPEN INPUT LIVRE.

           PERFORM AFFICHAGE-ECRAN .
           PERFORM DIS-FIRME.
           MOVE 1 TO INDICE-ZONE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           
           PERFORM APRES-DEC.

           IF INDICE-ZONE > CHOIX-MAX
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-DEC.
           PERFORM READ-CALENDRIER.
           PERFORM CUMUL-LIVRE.
           PERFORM DIS-E1-01.
           ACCEPT ACTION  
             LINE 23 POSITION 27 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           EXIT.

       CUMUL-LIVRE.
           INITIALIZE L-RECORD HELP-CUMUL COMPTEUR.
           MOVE FR-KEY TO L-FIRME-2.
           MOVE LNK-MOIS  TO L-MOIS-2.
           START LIVRE KEY > L-KEY-2 INVALID CONTINUE
                NOT INVALID
           PERFORM LECTURE-LIVRE THRU LECTURE-LIVRE-END.
      *    COMPUTE HELP-711 = HELP-711 / 1000.
      *    COMPUTE HELP-712 = HELP-712 / 1000.
      *    COMPUTE HELP-713 = HELP-713 / 1000.
      *    COMPUTE HELP-721 = HELP-721 / 1000.
      *    COMPUTE HELP-722 = HELP-722 / 1000.
      *    COMPUTE HELP-723 = HELP-723 / 1000.
           COMPUTE HELP-704 = HELP-701 + HELP-702 + HELP-703.
           COMPUTE HELP-731 = HELP-732 + HELP-733 + HELP-734.
           PERFORM DIS-E1-01.

       LECTURE-LIVRE.
           READ LIVRE NEXT AT END GO LECTURE-LIVRE-END.
           IF FR-KEY   NOT = L-FIRME 
           OR LNK-MOIS NOT = L-MOIS  
              GO LECTURE-LIVRE-END.
           IF L-STATEC > 2
           OR L-SUITE  > 0
           OR L-REGIME = 0
           OR L-REGIME = 4
           OR L-REGIME = 5
           OR L-REGIME = 9
             GO LECTURE-LIVRE.
           PERFORM CUMUL-MOIS.
           GO LECTURE-LIVRE.
       LECTURE-LIVRE-END.
           EXIT.
                 
       CUMUL-MOIS. 
          
           IF L-STATUT = 1 AND L-STATEC NOT = 2
              IF L-STATEC = 1
                 ADD 1              TO HELP-702
              ELSE
                 ADD 1              TO HELP-701
              END-IF
              MOVE 0 TO SH-00
              PERFORM FRAIS VARYING IDX FROM 11 BY 1 UNTIL IDX > 110
              ADD SH-00             TO HELP-711
              MOVE 0 TO SH-00
              PERFORM FRAIS VARYING IDX FROM 111 BY 1 UNTIL IDX > 130
              ADD SH-00             TO HELP-712
              MOVE 0 TO SH-00
              PERFORM FRAIS VARYING IDX FROM 161 BY 1 UNTIL IDX > 180
              ADD SH-00             TO HELP-713
              ADD L-UNI-SALAIRE     TO HELP-734
              MOVE 0 TO SH-00
              PERFORM HRS VARYING IDX FROM 15 BY 1 UNTIL IDX > 16
              PERFORM HRS VARYING IDX FROM 20 BY 1 UNTIL IDX > 30
              ADD SH-00             TO HELP-732
              MOVE 0 TO SH-00
              PERFORM HRS VARYING IDX FROM 41 BY 1 UNTIL IDX > 43
              ADD SH-00             TO HELP-733
           END-IF.
           IF L-STATUT = 2
           OR L-STATEC = 2
              IF L-STATEC = 1
                 ADD 1              TO HELP-702
              ELSE
                 ADD 1              TO HELP-703
              END-IF

              MOVE 0 TO SH-00
              PERFORM FRAIS VARYING IDX FROM 11 BY 1 UNTIL IDX > 110
              ADD SH-00             TO HELP-721
              MOVE 0 TO SH-00
              PERFORM FRAIS VARYING IDX FROM 111 BY 1 UNTIL IDX > 130
              ADD SH-00             TO HELP-722
              MOVE 0 TO SH-00
              PERFORM FRAIS VARYING IDX FROM 161 BY 1 UNTIL IDX > 180
              ADD SH-00             TO HELP-723
           END-IF.

       READ-CALENDRIER.
           MOVE 0 TO LNK-VAL LNK-NUM JOURS-TRAVAIL.
           CALL "6-GCCOL" USING LINK-V CCOL-RECORD.
           MOVE CCOL-CALENDRIER TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.
           PERFORM CALC-JOUR VARYING JOUR FROM 1 BY 1 UNTIL 
                    JOUR > MOIS-JRS(LNK-MOIS).

       CALC-JOUR.
           IF CAL-JOUR(LNK-MOIS, JOUR) = 0
           AND SEM-IDX(LNK-MOIS, JOUR) < 6
              ADD 1 TO JOURS-TRAVAIL.
            
       DIS-FIRME.
           DISPLAY FR-NOM  LINE  4 POSITION  3.
           DISPLAY FR-PAYS LINE  5 POSITION  3.
           MOVE FR-CODE-POST   TO HE-Z6.
           DISPLAY HE-Z6   LINE  5 POSITION  5.
           DISPLAY FR-MAISON LINE  6 POSITION  7.
           DISPLAY FR-RUE   LINE  6 POSITION 12.
           DISPLAY FR-LOCALITE LINE  5 POSITION 12.

           DISPLAY FR-ETAB-A LINE  4 POSITION 65.
           DISPLAY FR-ETAB-N LINE  4 POSITION 70.
           DISPLAY FR-SNOCS  LINE  4 POSITION 75.

       DIS-HE-END.
           EXIT.

       FRAIS.
           ADD L-DEB(IDX) TO SH-00.
       HRS.
           ADD L-UNIT(IDX) TO SH-00.

       DIS-E1-01.
           MOVE  HELP-701           TO HE-Z8.
           DISPLAY HE-Z8  LINE  9 POSITION 26.
           MOVE  HELP-702           TO HE-Z8.
           DISPLAY HE-Z8  LINE  9 POSITION 40.
           MOVE  HELP-703           TO HE-Z8.
           DISPLAY HE-Z8  LINE  9 POSITION 54.
           MOVE  HELP-704           TO HE-Z8.
           DISPLAY HE-Z8  LINE  9 POSITION 68.
           MOVE  HELP-711           TO HE-Z8Z2.
           DISPLAY HE-Z8Z2  LINE 14 POSITION 34.
           MOVE  HELP-712           TO HE-Z8Z2.
           DISPLAY HE-Z8Z2  LINE 14 POSITION 51.
           MOVE  HELP-713           TO HE-Z8Z2.
           DISPLAY HE-Z8Z2  LINE 14 POSITION 68.
           MOVE  HELP-721           TO HE-Z8Z2.
           DISPLAY HE-Z8Z2  LINE 16 POSITION 34.
           MOVE  HELP-722           TO HE-Z8Z2.
           DISPLAY HE-Z8Z2  LINE 16 POSITION 51.
           MOVE  HELP-723           TO HE-Z8Z2.
           DISPLAY HE-Z8Z2  LINE 16 POSITION 68.
           MOVE  HELP-731           TO HE-Z8Z2.
           DISPLAY HE-Z8Z2 LINE 18 POSITION 66.
           MOVE  HELP-732           TO HE-Z8Z2.
           DISPLAY HE-Z8Z2 LINE 19 POSITION 66.
           MOVE  HELP-733           TO HE-Z8Z2.
           DISPLAY HE-Z8Z2 LINE 20 POSITION 66.
           MOVE  HELP-734           TO HE-Z8Z2.
           DISPLAY HE-Z8Z2 LINE 21 POSITION 66.
           DISPLAY JOURS-TRAVAIL LINE 18 POSITION 33.
            
       AFFICHAGE-ECRAN.
           MOVE 2700 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-FIRME THRU DIS-HE-END.
           
       END-PROGRAM.
           CLOSE LIVRE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
