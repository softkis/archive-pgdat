      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-MENUE COPIER TEXTES MENU ANGLAIS          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-MENUE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           SELECT OPTIONAL MENUCOPY ASSIGN TO RANDOM, "X-MENU.E",
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is MNC-KEY,
                  STATUS FS-HELP.
           COPY "MENUTEXT.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴
       FD  MENUCOPY
           LABEL RECORD STANDARD
           DATA RECORD MNC-RECORD.
       01  MNC-RECORD.
           02 MNC-KEY    PIC X(10).
           02 MNC-BODY.
              03  MNC-DESCRIPTION  PIC X(40).

           COPY "MENUTEXT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  NOT-OPEN              PIC 9 VALUE 0.
       01  MENUTEXT-NAME.
           02 FILLER             PIC X(8) VALUE "S-MENU.E".

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "MENU.REC".

       PROCEDURE DIVISION USING LINK-V MN-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MENUCOPY MENUTEXT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-MENU.
           IF NOT-OPEN = 0
              OPEN INPUT MENUCOPY
              OPEN I-O MENUTEXT
              MOVE 1 TO NOT-OPEN
           END-IF.
           MOVE MN-KEY TO MNC-KEY.
           READ MENUCOPY INVALID CONTINUE
             NOT INVALID PERFORM ENREGISTRER
           END-READ.
           IF MN-PROG-NAME NOT = SPACES
              MOVE MN-PROG-NAME TO MNC-KEY
              READ MENUCOPY INVALID CONTINUE
                NOT INVALID PERFORM ENREGISTRER
              END-READ
           END-IF.
           EXIT PROGRAM.

       ENREGISTRER.
           MOVE MNC-RECORD TO MNT-RECORD.
           WRITE MNT-RECORD INVALID REWRITE MNT-RECORD END-WRITE.

