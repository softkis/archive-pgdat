      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-CSR IMPRESSION LISTE CODES SAISIS         �
      *  � PAR PERSONNE DE MOIS DEBUT A MOIS FIN                 �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CSR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "TRIPR.FC".
           COPY "CODPAIE.FC".
           SELECT OPTIONAL CODCUM ASSIGN TO RANDOM, "I-CODCUM",
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is CSH-KEY,
                  STATUS FS-HELP.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".
           COPY "TRIPR.FDE".
           COPY "CODPAIE.FDE".
       FD  CODCUM 
           LABEL RECORD STANDARD
           DATA RECORD CSH-RECORD.
      *  ENREGISTREMENT FICHIER CODE SALAIRE CUMULES 
      
       01  CSH-RECORD.
           02  CSH-KEY.
               03  CSH-CODE               PIC 9(4) .

           02  CSH-REC-DET.
               03  CSH-OUV-EMP            PIC 9.
               03  CSH-COMPTEUR           PIC 9(4).
               03  CSH-UNITE              PIC 9(8)V999.
               03  CSH-TOTAL              PIC 9(7)V99.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  MAX-LIGNES            PIC 9 VALUE 2.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "FIRME.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
SU         COPY "CODTXT.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  COMPTEUR              PIC 9(6) VALUE 0.

       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

           COPY "V-VH00.CPY".
       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".CSR".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR
               CODPAIE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CSR.
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE LNK-SUFFIX TO ANNEE-PAIE.
           OPEN INPUT CODPAIE.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TEST-EXTENSION.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6 THRU 7
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR (14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-SORT
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.            
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       NEXT-CS.
           MOVE 0 TO INPUT-ERROR.
           CALL "6-CSDEF" USING LINK-V CD-RECORD SAVE-KEY.
           IF CD-NUMBER NOT = 0
SU            CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY
              IF INPUT-ERROR = 1
              AND SAVE-KEY NOT = 13
                 GO NEXT-CS
              END-IF
           END-IF.
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.

       APRES-6.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-07.

       APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-3
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-3
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-3
           END-IF.
           OPEN I-O CODCUM.
           MOVE 0 TO COMPTEUR.
           PERFORM GET-CODPAIE.
           IF COMPTEUR > 0
              PERFORM READ-CONTRAT
              PERFORM WRITE-LIST THRU WRITE-LIST-END.
           CLOSE CODCUM.
           DELETE FILE CODCUM.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE CD-NUMBER TO HE-Z4.
           DISPLAY HE-Z4  LINE 13 POSITION 32.
           DISPLAY CTX-NOM LINE 13 POSITION 45.
       DIS-HE-07.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 19 POSITION 35.
           MOVE "MO" TO LNK-AREA.
           MOVE 19401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-08.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 35.
           MOVE "MO" TO LNK-AREA.
           MOVE 20401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1509 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.

       END-PROGRAM.
           IF COUNTER > 0 
              PERFORM TRANSMET.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XSORT.CPY".


       GET-CODPAIE.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY TO CSP-FIRME.
           MOVE MOIS-DEBUT TO CSP-MOIS.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE IMPL-BODY-LINE TO LIN-NUM.
           START CODPAIE KEY > CSP-KEY INVALID KEY
                PERFORM READ-CODPAIE-END
                NOT INVALID 
                PERFORM READ-CODPAIE THRU READ-CODPAIE-END.

       READ-CODPAIE.
           READ CODPAIE NEXT AT END GO READ-CODPAIE-END.
           IF FR-KEY NOT = CSP-FIRME
           OR REG-PERSON NOT = CSP-PERSON
           OR CSP-MOIS > MOIS-FIN
              GO READ-CODPAIE-END
           END-IF.
           IF CSP-SUITE NOT = 0
           OR CSP-CODE = 779
           OR CSP-CODE = 780
              GO READ-CODPAIE.
           PERFORM COMPTER-ENREGISTRER.
           GO READ-CODPAIE.
       READ-CODPAIE-END.
           EXIT.
           
       COMPTER-ENREGISTRER.
           PERFORM DIS-HE-01.
           MOVE CSP-CODE TO CSH-CODE.
           READ CODCUM INVALID INITIALIZE CSH-REC-DET END-READ.
           ADD 1 TO CSH-COMPTEUR COMPTEUR.
           ADD CSP-UNITE TO CSH-UNITE.
           ADD CSP-TOTAL TO CSH-TOTAL.
           WRITE CSH-RECORD INVALID REWRITE CSH-RECORD.


       WRITE-LIST.
           INITIALIZE CSH-RECORD.
           START CODCUM KEY > CSH-KEY INVALID KEY
                GO WRITE-LIST-END.

       READ-CODCUM.
           READ CODCUM NEXT AT END 
                GO WRITE-LIST-END
           END-READ.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= 65
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              PERFORM PAGE-DATE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           PERFORM FILL-TEXTE.
           ADD 1 TO LIN-NUM.
           GO READ-CODCUM.
       WRITE-LIST-END.
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.

       FILL-TEXTE.
           MOVE CSH-CODE TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           ADD 1 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE CTX-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  6 TO COL-NUM.
           MOVE CTX-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 31 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE CSH-COMPTEUR  TO VH-00.
           PERFORM FILL-FORM.


           DIVIDE CSH-TOTAL BY CSH-COMPTEUR GIVING SH-00.
           MOVE 36 TO COL-NUM.
           MOVE 7 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE SH-00  TO VH-00.     
           PERFORM FILL-FORM.
           MOVE 0 TO SH-00.
           IF CSH-UNITE > 0
               DIVIDE CSH-TOTAL BY CSH-UNITE GIVING SH-00.
           MOVE 47 TO COL-NUM.
           MOVE 7 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE SH-00 TO VH-00.     
           PERFORM FILL-FORM.

           MOVE 58 TO COL-NUM.
           MOVE 7 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE CSH-UNITE  TO VH-00.     
           PERFORM FILL-FORM.
           MOVE 70 TO COL-NUM.
           MOVE 7 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE CSH-TOTAL TO VH-00.    
           PERFORM FILL-FORM.

       ENTETE.
           MOVE  5 TO LIN-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  4 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           MOVE 11 TO COL-NUM.
           PERFORM FILL-FORM.

      * DONNEES PERSONNE
           MOVE 07 TO LIN-NUM.
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  4 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           MOVE 11 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 65 TO COL-NUM.
           MOVE REG-ANCIEN-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD  1 TO COL-NUM.
           MOVE REG-ANCIEN-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD  1 TO COL-NUM.
           MOVE REG-ANCIEN-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           ADD  1 TO LIN-NUM.
           MOVE 11 TO COL-NUM.
           MOVE PR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE PR-RUE TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.

           IF CON-FIN-A > 0
               MOVE 65 TO COL-NUM
               MOVE CON-FIN-J TO ALPHA-TEXTE
               PERFORM FILL-FORM
               ADD  1 TO COL-NUM
               MOVE CON-FIN-M TO ALPHA-TEXTE
               PERFORM FILL-FORM
               ADD  1 TO COL-NUM
               MOVE CON-FIN-A TO ALPHA-TEXTE
               PERFORM FILL-FORM
           END-IF.


           ADD 1 TO LIN-NUM.

           MOVE 11 TO COL-NUM.
           MOVE PR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE PR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE PR-LOCALITE TO ALPHA-TEXTE.
           ADD  1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 65 TO COL-NUM.
           MOVE PR-NAISS-A TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD  1 TO COL-NUM.
           MOVE PR-NAISS-M TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD  1 TO COL-NUM.
           MOVE PR-NAISS-J TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD  1 TO COL-NUM.
           MOVE PR-SNOCS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           IF CHOIX NOT = 0 
              PERFORM DETAIL-CHOIX.

       DETAIL-CHOIX.
           MOVE  6 TO LIN-NUM.
           MOVE  2 TO COL-NUM.
           MOVE TEST-NUMBER TO LNK-POSITION.
           MOVE TEST-ALPHA  TO LNK-TEXT.
           MOVE CHOIX       TO LNK-NUM.
           CALL "4-CHOIX" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  4 TO CAR-NUM.
           MOVE  3 TO LIN-NUM.
           MOVE 74 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

           MOVE  2 TO CAR-NUM.
           MOVE  2 TO LIN-NUM.
           MOVE 68 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 71 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 74 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE LNK-ANNEE TO VH-00
           MOVE  3 TO LIN-NUM.
           MOVE  4 TO CAR-NUM
           MOVE 11 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE MOIS-DEBUT TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           ADD 2 TO COL-NUM.
           PERFORM FILL-FORM
           ADD  2 TO COL-NUM.
           MOVE "-" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE MOIS-FIN TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           ADD 2 TO COL-NUM.
           PERFORM FILL-FORM.
