      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-FAMIL MEMBRES DE LA FAMILLE               �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.   1-FAMIL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FAMILLE.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FAMILLE.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  ECRAN-SUITE.
           02 ECR-S1        PIC 999 VALUE 220.
           02 ECR-S2        PIC 999 VALUE 241.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S         PIC 999 OCCURS 2.
       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1  PIC 99 VALUE 23.
           02  CHOIX-MAX-2  PIC 99 VALUE  4.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX    PIC 99 OCCURS 2.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "PAYS.REC".
           COPY "METIER.REC".
           COPY "MESSAGE.REC".
           COPY "V-VAR.CPY".

       01  ACTION-CALL           PIC 9 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z6Z6 PIC Z(6),Z(6) BLANK WHEN ZERO.
           02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
      *袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴�
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FAMILLE.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-FAMIL .

           OPEN I-O FAMILLE.

           CALL "0-TODAY" USING TODAY.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0163640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 2     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 3     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 8     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 9     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 12    MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN CHOIX-MAX(1)
                      MOVE 0000000005 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-10
           WHEN 11 PERFORM AVANT-11
           WHEN 12 PERFORM AVANT-12
           WHEN 13 PERFORM AVANT-13
           WHEN 14 PERFORM AVANT-14
           WHEN 15 PERFORM AVANT-15
           WHEN 16 PERFORM AVANT-16
           WHEN 17 PERFORM AVANT-17
           WHEN 18 PERFORM AVANT-18
           WHEN 19 PERFORM AVANT-19
           WHEN 20 PERFORM AVANT-20
           WHEN 21 PERFORM AVANT-21
           WHEN CHOIX-MAX(1) PERFORM AVANT-DEC.


           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF EXC-KEY > 66 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              IF ECRAN-IDX = 1
                 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
              ELSE
                 MOVE 0 TO INDICE-ZONE
              END-IF
              GO TRAITEMENT-ECRAN
           END-IF.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  8 PERFORM APRES-8 
           WHEN  9 PERFORM APRES-9 
           WHEN 10 PERFORM APRES-10
           WHEN 11 PERFORM APRES-11
           WHEN 12 PERFORM APRES-12
           WHEN 13 PERFORM APRES-13
           WHEN 14 PERFORM APRES-14
           WHEN 16 PERFORM APRES-16
           WHEN CHOIX-MAX(1) PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           INITIALIZE FAM-RECORD.
           MOVE FR-KEY TO REG-FIRME FAM-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           ACCEPT FAM-SUITE
             LINE  5 POSITION 30 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4.            
           ACCEPT FAM-NOM 
             LINE  6 POSITION 30 SIZE 30
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT FAM-NOM CONVERTING
           "abcdefghijklmnopqrstuvwxyz굤닀뀑뙎봺뼏꼪" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZ륟EEAIIO솞UU랢".

       AVANT-5.            
           ACCEPT FAM-PRENOM 
             LINE  7 POSITION 30 SIZE 25
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY CONTINUE.
           INSPECT FAM-PRENOM CONVERTING
           "ABCDEFGHIJKLMNOPQRSTUVWXYZ릻쉸" TO
           "abcdefghijklmnopqrstuvwxyz굱걚".
           MOVE FAM-PRENOM TO LNK-TEXT.
           CALL "0-NOMMAJ" USING LINK-V.
           MOVE LNK-TEXT TO FAM-PRENOM.

       AVANT-6.
           IF FAM-CODE-SEXE = 0
              MOVE 1 TO FAM-CODE-SEXE.
           IF FAM-SUITE > 0
           ACCEPT FAM-CODE-SEXE
             LINE  8 POSITION 30 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE
              EVALUATE PR-CODE-SEXE 
                 WHEN 1 MOVE 2 TO FAM-CODE-SEXE
                 WHEN 2 MOVE 1 TO FAM-CODE-SEXE
              END-EVALUATE
           END-IF.

       AVANT-7.
           ACCEPT FAM-LIEU-NAISSANCE
             LINE 9  POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT FAM-NATIONALITE
             LINE 10 POSITION 30 SIZE 3
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-9.
           ACCEPT FAM-METIER
             LINE 11 POSITION 30 SIZE 5
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-10.
           MOVE 12300000 TO LNK-POSITION.
           CALL "0-GDATE" USING FAM-NAISSANCE
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.

       AVANT-11.           
           ACCEPT FAM-SNOCS
             LINE 12 POSITION 59 SIZE 3 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-12.
           IF FAM-SUITE > 0
           AND FAM-RELATION = 0
              MOVE FAM-CODE-SEXE TO FAM-RELATION.
           IF FAM-SUITE > 0
           ACCEPT FAM-RELATION
             LINE 13 POSITION 30 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE
             MOVE 0 TO FAM-RELATION.

       AVANT-13.
           IF FAM-SUITE = 0
           MOVE 14300000 TO LNK-POSITION
           CALL "0-GDATE" USING FAM-DATE-MARIAGE
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.

       AVANT-14.
           IF FAM-SUITE = 0
           MOVE 15300000 TO LNK-POSITION
           CALL "0-GDATE" USING FAM-DATE-SEPARE
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.

       AVANT-15.
           IF FAM-SUITE = 0
           MOVE 16300000 TO LNK-POSITION
           CALL "0-GDATE" USING FAM-DATE-DEPART
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.

       AVANT-16.
           IF FAM-SUITE = 0
           ACCEPT FAM-EMPLOYEUR
             LINE 17 POSITION 30 SIZE 35
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-17.
           IF FAM-SUITE = 0
           MOVE 18300000 TO LNK-POSITION
           CALL "0-GDATE" USING FAM-DATE-ENGAGEM
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.

       AVANT-18.
           IF FAM-SUITE = 0
           ACCEPT FAM-LOCALITE
             LINE 19 POSITION 30 SIZE 50
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-19.
           IF FAM-SUITE = 0
           ACCEPT FAM-ADRESSE1
             LINE 20 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
       AVANT-20.
           IF FAM-SUITE = 0
           ACCEPT FAM-ADRESSE2
             LINE 21 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-21.
           ACCEPT FAM-COMMENT 
             LINE 22 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  OTHER PERFORM NEXT-REGIS.
           IF INPUT-ERROR = 0
           AND REG-PERSON > 0 
              INITIALIZE FAM-RECORD SAVE-KEY
              PERFORM NEXT-FAMILLE.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-2.
           MOVE "A" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-E1-01.
           IF REG-PERSON = 0 
              MOVE 1 TO INPUT-ERROR
           END-IF.

           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR = 0
           AND EXC-KEY > 64
              INITIALIZE FAM-RECORD SAVE-KEY
              PERFORM NEXT-FAMILLE.
           PERFORM AFFICHAGE-DETAIL.

       APRES-3.
           MOVE REG-PERSON TO FAM-PERSON.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-FAMIL" USING LINK-V FAM-RECORD 
                   PR-RECORD REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN 65 THRU 66 PERFORM NEXT-FAMILLE
           WHEN 13 READ FAMILLE INVALID INITIALIZE FAM-REC-DET END-READ
           END-EVALUATE.
           PERFORM AFFICHAGE-DETAIL.

       APRES-4.
           PERFORM DIS-E1-04.
       APRES-5.
           PERFORM DIS-E1-05.

       APRES-6.
           IF FAM-CODE-SEXE > 2
           OR FAM-CODE-SEXE = 0
              MOVE 1 TO FAM-CODE-SEXE
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-E1-06.
      
       APRES-8.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 MOVE FAM-NATIONALITE TO PAYS-CODE
                   PERFORM NEXT-PAYS
                   MOVE PAYS-CODE TO FAM-NATIONALITE
           WHEN  5 MOVE FAM-NATIONALITE TO LNK-TEXT
                   CALL "1-PAYS" USING LINK-V
                   MOVE LNK-TEXT TO FAM-NATIONALITE PAYS-CODE
                   CANCEL "1-PAYS"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
           WHEN 2 CALL "2-PAYS" USING LINK-V PAYS-RECORD
                  IF PAYS-CODE NOT = SPACES
                     MOVE PAYS-CODE TO FAM-NATIONALITE
                  END-IF
                  CANCEL "2-PAYS"
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL.
           PERFORM DIS-E1-08.

       APRES-9.
           INITIALIZE LNK-TEXT.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 MOVE FAM-METIER TO MET-CODE 
                    PERFORM NEXT-METIER
                    MOVE MET-CODE TO FAM-METIER
           WHEN   5 CALL "1-METIER" USING LINK-V
                    MOVE LNK-TEXT TO FAM-METIER
                    CANCEL "1-METIER"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   2 CALL "2-METIER" USING LINK-V MET-RECORD
                    MOVE MET-CODE TO FAM-METIER
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "2-METIER"
           END-EVALUATE.
           PERFORM DIS-E1-09.

       APRES-10.
           IF FAM-NAISS-A < 1900
              MOVE 1 TO INPUT-ERROR
              MOVE 1900 TO FAM-NAISS-A.
           PERFORM DIS-E1-10.
           PERFORM APRES-DATE.

       APRES-11.
           IF FAM-SNOCS NOT = 0
              CALL "0-MATR" USING FAM-MATRICULE LINK-V END-CALL
              MOVE LNK-NUM TO INPUT-ERROR.
           IF FAM-NAISS-A = 0
              MOVE 1 TO INPUT-ERROR.

       APRES-12.
           MOVE FAM-RELATION TO MS-NUMBER.
           EVALUATE EXC-KEY
           WHEN  2 MOVE "F" TO LNK-AREA
                   CALL "2-MESS" USING LINK-V MS-RECORD
                   IF MS-NUMBER > 0
                      MOVE MS-NUMBER TO FAM-RELATION
                   END-IF
                   CANCEL "2-MESS"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
           WHEN  65 THRU 66 PERFORM NEXT-MESSAGES
                    COMPUTE IDX = MS-NUMBER - FAM-CODE-SEXE
                    DIVIDE IDX BY 2 GIVING IDX-1 REMAINDER IDX-2
                    IF EXC-KEY = 65 
                       SUBTRACT IDX-2 FROM MS-NUMBER
                    ELSE
                       ADD IDX-2 TO MS-NUMBER
                    END-IF
                    MOVE MS-NUMBER TO FAM-RELATION
           END-EVALUATE.
           COMPUTE IDX = FAM-RELATION - FAM-CODE-SEXE.
           IF FAM-RELATION > 0
              DIVIDE  IDX BY 2 GIVING IDX-1 REMAINDER INPUT-ERROR.
           PERFORM DIS-E1-12.

       APRES-13.
           IF FAM-MARIAGE-A NOT = 0
              PERFORM APRES-DATE.

       APRES-14.
           IF FAM-SEPARE-A NOT = 0
              PERFORM APRES-DATE.
           
       APRES-16.
           IF FAM-ENGAGEM-A NOT = 0
              PERFORM APRES-DATE.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 CALL "0-TODAY" USING TODAY
                  MOVE TODAY-TIME TO FAM-TIME
                  MOVE LNK-USER TO FAM-USER
                  IF LNK-SQL = "Y" 
                     CALL "9-FAMIL" USING LINK-V FAM-RECORD WR-KEY 
                  END-IF
                  WRITE FAM-RECORD INVALID REWRITE FAM-RECORD END-WRITE
                  MOVE 1 TO ECRAN-IDX
                  PERFORM AFFICHAGE-ECRAN
                  PERFORM AFFICHAGE-DETAIL
                  MOVE 4 TO DECISION
           WHEN 8 DELETE FAMILLE INVALID CONTINUE END-DELETE
                  IF LNK-SQL = "Y" 
                     CALL "9-FAMIL" USING LINK-V FAM-RECORD DEL-KEY 
                  END-IF
                  MOVE 1 TO INDICE-ZONE ECRAN-IDX
                  PERFORM AFFICHAGE-ECRAN
                  PERFORM AFFICHAGE-DETAIL
                  INITIALIZE FAM-REC-DET
                  MOVE 4 TO DECISION
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       APRES-DATE.
           IF LNK-NUM NOT = 0
              MOVE "AA" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           
       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           
       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           MOVE 0 TO LNK-PERSON LNK-NUM.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       READ-FAMILLE.
           CALL "6-FAMIL" USING LINK-V REG-RECORD FAM-RECORD FAKE-KEY.
           
       NEXT-FAMILLE.
           CALL "6-FAMIL" USING LINK-V REG-RECORD FAM-RECORD EXC-KEY.

       NEXT-PAYS.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD EXC-KEY.
       NEXT-METIER.
           CALL "6-METIER" USING LINK-V MET-RECORD EXC-KEY.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

           
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-E1-01.
           MOVE REG-PERSON  TO HE-Z6.
           IF REG-PERSON NOT = 0
              MOVE REG-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 
             LINE 3 POSITION 15 SIZE 6
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.

       DIS-E1-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.

       DIS-E1-03.
           DISPLAY FAM-SUITE LINE 5 POSITION 30.
       DIS-E1-04.
           DISPLAY FAM-NOM LINE 6 POSITION 30.
       DIS-E1-05.
           DISPLAY FAM-PRENOM LINE 7 POSITION 30.
       DIS-E1-06.
           DISPLAY FAM-CODE-SEXE LINE 8 POSITION 30.
       DIS-E1-07.
           DISPLAY FAM-LIEU-NAISSANCE LINE 9 POSITION 30.
       DIS-E1-08.
           DISPLAY FAM-NATIONALITE  LINE 10 POSITION 30.
           MOVE FAM-NATIONALITE TO PAYS-CODE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.
           DISPLAY PAYS-NATION LINE 10 POSITION 40 HIGH.

       DIS-E1-09.
           DISPLAY FAM-METIER LINE 11 POSITION 30.
           IF FAM-METIER NOT = SPACES
              MOVE FAM-METIER TO MET-CODE
              CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY
           ELSE
              INITIALIZE MET-RECORD
           END-IF.
           DISPLAY MET-NOM(FAM-CODE-SEXE) LINE 11 POSITION 40 SIZE 40.

       DIS-E1-10.
           MOVE FAM-NAISS-A     TO HE-AA.
           MOVE FAM-NAISS-M     TO HE-MM.
           MOVE FAM-NAISS-J     TO HE-JJ.
           DISPLAY HE-DATE LINE 12 POSITION 30.
           COMPUTE IDX-3 = TODAY-ANNEE - FAM-NAISS-A.
           IF FAM-NAISS-M > TODAY-MOIS
              SUBTRACT 1 FROM IDX-3
              COMPUTE IDX-2 = 12 + TODAY-MOIS - FAM-NAISS-M 
           ELSE 
              COMPUTE IDX-2 = TODAY-MOIS - FAM-NAISS-M  
           END-IF.
           MOVE IDX-3 TO HE-Z2.
           DISPLAY HE-Z2 LINE 12 POSITION 73.
           MOVE IDX-2 TO HE-Z2.
           DISPLAY HE-Z2 LINE 12 POSITION 76 LOW.

       DIS-E1-11.
           DISPLAY FAM-SNOCS LINE 12 POSITION 59.
       DIS-E1-12.
           MOVE FAM-RELATION TO LNK-NUM HE-Z2.
           DISPLAY HE-Z2 LINE 13 POSITION 30.
           MOVE "F" TO LNK-AREA.
           COMPUTE LNK-POSITION = 13402500.
           CALL "0-DMESS" USING LINK-V.

       DIS-E1-13.
           MOVE FAM-MARIAGE-A   TO HE-AA.
           MOVE FAM-MARIAGE-M   TO HE-MM.
           MOVE FAM-MARIAGE-J   TO HE-JJ.
           DISPLAY HE-DATE LINE 14 POSITION 30.
       DIS-E1-14.
           MOVE FAM-SEPARE-A    TO HE-AA.
           MOVE FAM-SEPARE-M    TO HE-MM.
           MOVE FAM-SEPARE-J    TO HE-JJ.
           DISPLAY HE-DATE LINE 15 POSITION 30.
       DIS-E1-15.
           MOVE FAM-DEPART-A    TO HE-AA.
           MOVE FAM-DEPART-M    TO HE-MM.
           MOVE FAM-DEPART-J    TO HE-JJ.
           DISPLAY HE-DATE LINE 16 POSITION 30.
       DIS-E1-16.
           DISPLAY FAM-EMPLOYEUR LINE 17 POSITION 30.
       DIS-E1-17.
           MOVE FAM-ENGAGEM-A   TO HE-AA.
           MOVE FAM-ENGAGEM-M   TO HE-MM.
           MOVE FAM-ENGAGEM-J   TO HE-JJ.
           DISPLAY HE-DATE LINE 18 POSITION 30.
       DIS-E1-18.
           DISPLAY FAM-LOCALITE  LINE 19 POSITION 30.
       DIS-E1-19.
           DISPLAY FAM-ADRESSE1  LINE 20 POSITION 30.
       DIS-E1-20.
           DISPLAY FAM-ADRESSE2  LINE 21 POSITION 30.
       DIS-E1-21.
           DISPLAY FAM-COMMENT   LINE 22 POSITION 30.
       DIS-E1-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE ECR-S(ECRAN-IDX) TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           EVALUATE ECRAN-IDX
               WHEN 1 PERFORM DIS-E1-01 THRU DIS-E1-END.

       END-PROGRAM.
           CLOSE FAMILLE.
           CANCEL "2-FAMIL".
           CANCEL "6-FAMIL".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

