      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-FRINIT IMPRESSION DEMANDE DE REMBOURSEMENT�
      *  � DES FRAIS D'INITIATION ET DE SALAIRE                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-FRINIT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "LIVRE.REC".
           COPY "CODPAIE.REC".
           COPY "BANQF.REC".
           COPY "BANQUE.REC".
           COPY "PARMOD.REC".
           COPY "CONTRAT.REC".
           COPY "TXACCID.REC".
           COPY "TAUXSS.REC".
           COPY "MUT.REC".
           COPY "RFI.LNK".
           COPY "CODFIX.REC".

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  COUNTER               PIC 9999 VALUE 0.
       01  TRIMESTER             PIC 9 VALUE 0.
       01  SH-02                 PIC 9(8)V99.
       01  SAVE-LG               PIC X.
       01  LINK-ECRIT.
           02  TOTAL-CHIFFRE     PIC X(100).
           02  REDEF-CHIFFRE REDEFINES TOTAL-CHIFFRE.
               03 CHIFFRE   PIC X OCCURS 100.
           02  TOTAL-VAL         PIC 9(9)V99 VALUE 0.
           02  CHIFFRE-POINT     PIC 999 COMP-4.


           COPY "V-VAR.CPY".
        
       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6Z2 PIC Z(6),ZZ.
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z2Z2 PIC ZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-FRINIT.
       
           MOVE LNK-LANGUAGE TO SAVE-LG.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-PATH TO BQF-CODE.
           MOVE LNK-MOIS TO SAVE-MOIS.
           DIVIDE LNK-MOIS BY 3 GIVING TRIMESTER REMAINDER IDX-2.
           IF IDX-2 > 0
              ADD 1 TO TRIMESTER
           END-IF.
           COMPUTE MOIS-FIN = 3 * TRIMESTER.
           COMPUTE MOIS-DEBUT = MOIS-FIN - 2.
           CALL "0-TODAY" USING TODAY.
           INITIALIZE TXA-RECORD MUT-RECORD.
           CALL "6-ASSACC" USING LINK-V TXA-RECORD.
           IF FR-ACCIDENT = 0
              MOVE 1 TO FR-ACCIDENT 
           END-IF.
           IF LNK-ANNEE > 2008
               CALL "6-MUT" USING LINK-V MUT-RECORD NUL-KEY
               IF MUT-CLASSE = 0
                  MOVE 1 TO MUT-CLASSE
               END-IF
               IF CAR-REGIME > 99
                  MOVE 5 TO MUT-CLASSE
               END-IF

           END-IF.

           PERFORM AFFICHAGE-ECRAN .
           MOVE 66 TO SAVE-KEY.
           IF BQF-CODE = SPACES
              PERFORM NEXT-BANQF.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0100000000 TO EXC-KFR(1)
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5 THRU 6
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-4
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY = 1 
               MOVE 00202014 TO LNK-VAL
               CALL "0-BOOK" USING LINK-V
               PERFORM AFFICHAGE-ECRAN
               PERFORM AFFICHAGE-DETAIL
               GO TRAITEMENT-ECRAN
           END-IF.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-4.
           ACCEPT BQF-CODE
             LINE 15 POSITION 25 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-5.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
                     PERFORM APRES-5 THRU APRES-6
             WHEN 66 ADD      1 TO   MOIS-DEBUT
                     PERFORM APRES-5 THRU APRES-6
             END-EVALUATE.

       AVANT-6.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
                     PERFORM APRES-5 THRU APRES-6
             WHEN 66 ADD      1 TO   MOIS-FIN
                     PERFORM APRES-5 THRU APRES-6
             END-EVALUATE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-4.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-BANQF" USING LINK-V BQF-RECORD
                   IF BQF-CODE NOT = SPACES
                      MOVE BQF-CODE TO BQ-CODE
                   END-IF
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER MOVE EXC-KEY TO SAVE-KEY
                      PERFORM NEXT-BANQF
                      MOVE BQF-CODE TO BQ-CODE
           END-EVALUATE.
           IF BQF-KEY = SPACES MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0
              MOVE BQF-CODE TO PARMOD-PATH
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
           PERFORM DIS-HE-04.

       APRES-5.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-05.
           PERFORM DIS-HE-06.

       APRES-6.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-05.
           PERFORM DIS-HE-06.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-PERSON 
                    PERFORM READ-PERSON THRU READ-EXIT
                    PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM DIS-HE-01.
           INITIALIZE CSF-RECORD.
           MOVE FR-KEY     TO CSF-FIRME.
           MOVE REG-PERSON TO CSF-PERSON LNK-PERSON.
           MOVE 9014       TO CSF-CODE.
           CALL "6-CODFIX" USING LINK-V CSF-RECORD FAKE-KEY.

           IF CSF-POURCENT = 0
           OR CSF-ANNEE-F  = 0
              GO READ-PERSON-2.
           IF CSF-ANNEE-D > LNK-ANNEE
           OR CSF-ANNEE-F < LNK-ANNEE
              GO READ-PERSON-2.
           IF  CSF-ANNEE-D = LNK-ANNEE
           AND CSF-MOIS-D > MOIS-FIN
              GO READ-PERSON-2.
           IF  CSF-ANNEE-F = LNK-ANNEE
           AND CSF-MOIS-F < MOIS-DEBUT
              GO READ-PERSON-2.
           PERFORM CAR-RECENTE.
           INITIALIZE LINK-RECORD SH-00 SH-01.
           PERFORM READ-CONTRAT.
           PERFORM TAUX.
           PERFORM CUMUL-LIVRE.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.
           EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           MOVE 27 TO COL-IDX.
           IF A-N = "A"
              ADD 11 TO COL-IDX
              DISPLAY SPACES LINE 6 POSITION 37 SIZE 1.
           COMPUTE IDX = 80 - COL-IDX.
           MOVE 0 TO SIZ-IDX IDX-1.
           INSPECT PR-NOM TALLYING  SIZ-IDX FOR CHARACTERS BEFORE "  ".
           INSPECT PR-NOM-JF TALLYING IDX-1 FOR CHARACTERS BEFORE "  ".

           IF FR-NOM-JF = SPACES
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX  = COL-IDX + SIZ-IDX + 1
              IF PR-NOM-JF NOT = SPACES
                 COMPUTE IDX = 80 - COL-IDX
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
           ELSE
              IF PR-NOM-JF NOT = SPACES
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX = COL-IDX + SIZ-IDX + 1
           END-IF.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 6 POSITION COL-IDX SIZE IDX LOW.
       DIS-HE-04.
           MOVE BQF-CODE TO BQ-CODE.
           PERFORM READ-BANQUE.
           DISPLAY BQF-CODE   LINE 15 POSITION 25.
           DISPLAY BQ-NOM     LINE 15 POSITION 40.
           DISPLAY BQF-COMPTE LINE 16 POSITION 40.
       DIS-HE-05.
           DISPLAY MOIS-DEBUT LINE 19 POSITION 31.
           MOVE MOIS-DEBUT TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-06.
           DISPLAY MOIS-FIN LINE 20 POSITION 31.
           MOVE MOIS-FIN TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       READ-BANQUE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD EXC-KEY.
       NEXT-BANQF.
           CALL "6-BANQF" USING LINK-V BQF-RECORD SAVE-KEY.

       CUMUL-LIVRE.
           PERFORM GET-CS VARYING LNK-MOIS
           FROM MOIS-DEBUT BY 1 UNTIL LNK-MOIS > MOIS-FIN.
           MOVE SH-00 TO HE-Z6Z2.
           MOVE HE-Z6Z2 TO LINK-BRUT-TOTAL.
           MOVE SH-01 TO HE-Z6Z2.
           MOVE HE-Z6Z2 TO LINK-PARTP-TOTAL.
           ADD SH-01 TO SH-00.
           MOVE SH-00 TO HE-Z6Z2.
           MOVE HE-Z6Z2 TO LINK-TOTAL-AB.
           COMPUTE SH-00 = SH-00 * CSF-POURCENT / 100 + ,005.
           MOVE SH-00 TO HE-Z6Z2 TOTAL-VAL.
           MOVE HE-Z6Z2 TO LINK-TOTAL-STH.
           CALL "0-ECRIT" USING LINK-ECRIT.
           MOVE TOTAL-CHIFFRE TO LINK-TOUT-LETTRE.
           IF SH-00 > 0 
              PERFORM TRANSMET
           END-IF.

       GET-CS.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY     TO CSP-FIRME.
           MOVE REG-PERSON TO CSP-PERSON.
           MOVE 9014       TO CSP-CODE.
           MOVE LNK-MOIS   TO CSP-MOIS LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           DIVIDE LNK-MOIS BY 3 GIVING IDX-3 REMAINDER LIN-IDX.
           IF LIN-IDX = 0
              MOVE 3 TO LIN-IDX 
           END-IF.
           EVALUATE LIN-IDX 
              WHEN 1 MOVE LNK-TEXT TO LINK-NOM-MOIS1
              WHEN 2 MOVE LNK-TEXT TO LINK-NOM-MOIS2
              WHEN 3 MOVE LNK-TEXT TO LINK-NOM-MOIS3
           END-EVALUATE.

           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           IF CSP-POURCENT NOT = 0
              PERFORM NOTER
           END-IF.

       NOTER.
           INITIALIZE L-RECORD LNK-SUITE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           MOVE L-IMPOSABLE-BRUT TO HE-Z6Z2.
           EVALUATE LIN-IDX 
              WHEN 1 MOVE HE-Z6Z2 TO LINK-BRUT-MOIS1
              WHEN 2 MOVE HE-Z6Z2 TO LINK-BRUT-MOIS2
              WHEN 3 MOVE HE-Z6Z2 TO LINK-BRUT-MOIS3
           END-EVALUATE.
           ADD L-IMPOSABLE-BRUT TO SH-00.
           MOVE L-COTIS-MAL-PAT TO SH-02.
           ADD  L-COTIS-PEN-PAT TO SH-02.
           ADD  L-COTIS-SAN-PAT TO SH-02.
           ADD  L-COTIS-ACC-PAT TO SH-02.
           ADD  L-COTIS-ESP-PAT TO SH-02.
           ADD  L-COTIS-MUT-PAT TO SH-02.
           MOVE SH-02 TO HE-Z6Z2.
           EVALUATE LIN-IDX 
              WHEN 1 MOVE HE-Z6Z2 TO LINK-PARTP-MOIS1
              WHEN 2 MOVE HE-Z6Z2 TO LINK-PARTP-MOIS2
              WHEN 3 MOVE HE-Z6Z2 TO LINK-PARTP-MOIS3
           END-EVALUATE.
           ADD SH-02 TO SH-01.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD PV-KEY.

        TAUX.
           INITIALIZE SS-RECORD.
           MOVE CAR-REGIME TO SS-REGIME.
           CALL "6-TSS" USING LINK-V SS-RECORD.
           MOVE TXA-VALUE(FR-ACCIDENT) TO HE-Z2Z2.
           MOVE HE-Z2Z2 TO LINK-TAUX-ACCIDENT.
           MOVE SS-PP-M TO HE-Z2Z2.
           MOVE HE-Z2Z2 TO LINK-TAUX-MALADIE.
           MOVE SS-PP-E TO HE-Z2Z2.
           MOVE HE-Z2Z2 TO LINK-TAUX-ESPECE.
           MOVE SS-PP-P TO HE-Z2Z2.
           MOVE HE-Z2Z2 TO LINK-TAUX-PENSION.
           MOVE SS-PP-S TO HE-Z2Z2.
           MOVE HE-Z2Z2 TO LINK-TAUX-SANTE.
           COMPUTE IDX = MUT-CLASSE + 90
           MOVE TXA-VALUE(IDX) TO HE-Z2Z2.
           MOVE HE-Z2Z2 TO LINK-TAUX-MUTUAL.

       TRANSMET.
           PERFORM LAYOUT.
           MOVE 0 TO LNK-VAL.
           CALL "RFIS" USING LINK-V LINK-RECORD.
           ADD 1 TO COUNTER.


       LAYOUT.
           MOVE CSF-POURCENT TO HE-Z2. 
           MOVE HE-Z2 TO LINK-POURCENT LINK-POURCENT-HAUT. 

           MOVE FR-NOM TO LINK-EMPL-ADRESSE(1).
           MOVE FR-LOCALITE TO LINK-LIEU.
           MOVE 1 TO IDX-4.
           STRING FR-MAISON DELIMITED BY " " INTO 
           LINK-EMPL-ADRESSE(2) WITH POINTER IDX-4.
           ADD 2 TO IDX-4.
           STRING FR-RUE DELIMITED BY "  " INTO LINK-EMPL-ADRESSE(2)
           WITH POINTER IDX-4.
           MOVE 1 TO IDX-4.
           STRING FR-PAYS DELIMITED BY "  " INTO LINK-EMPL-ADRESSE(3)
           WITH POINTER IDX-4.
           MOVE FR-CODE-POST TO HE-Z6.
           ADD 2 TO IDX-4.
           STRING HE-Z6 DELIMITED BY SIZE INTO LINK-EMPL-ADRESSE(3)
           WITH POINTER IDX-4.
           ADD 2 TO IDX-4.
           STRING FR-LOCALITE DELIMITED BY SIZE INTO 
           LINK-EMPL-ADRESSE(3) WITH POINTER IDX-4.
           MOVE BQF-CODE TO LINK-EMPL-BANQUE.
           MOVE 0 TO IDX.
           PERFORM IBAN VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 10.
           MOVE LNK-ANNEE TO LINK-FACT-ANNEE.
           MOVE CSF-TEXTE TO LINK-NO-RECONNAISSANCE.
           MOVE PR-POLITESSE TO LNK-NUM.
           MOVE "F" TO LNK-LANGUAGE.
           IF LNK-NUM = 0
              MOVE PR-CODE-SEXE TO LNK-NUM.
           MOVE "P" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE 1 TO IDX-4.
           STRING LNK-TEXT DELIMITED BY "  " INTO LINK-ADRESSE-1
           WITH POINTER IDX-4.
           ADD 2 TO IDX-4.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           STRING LNK-TEXT DELIMITED BY "   " INTO 
           LINK-ADRESSE-1 WITH POINTER IDX-4.

           CALL "4-PRLOC" USING LINK-V PR-RECORD
           MOVE 1 TO IDX-4.
           STRING LNK-TEXT DELIMITED BY "  " INTO LINK-ADRESSE-2
           WITH POINTER IDX-4.
           CALL "4-PRRUE" USING LINK-V PR-RECORD
           ADD 2 TO IDX-4.
           STRING LNK-TEXT DELIMITED BY "  " INTO LINK-ADRESSE-2
           WITH POINTER IDX-4.
           MOVE 1 TO IDX.
           STRING PR-NAISS-A DELIMITED BY SIZE INTO LINK-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-NAISS-M DELIMITED BY SIZE INTO LINK-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-NAISS-J DELIMITED BY SIZE INTO LINK-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-SNOCS DELIMITED BY SIZE INTO LINK-MATRICULE
           WITH POINTER IDX.

           MOVE CON-DEBUT-J TO HE-JJ.
           MOVE CON-DEBUT-M TO HE-MM.
           MOVE CON-DEBUT-A TO HE-AA.
           MOVE HE-DATE TO LINK-DATE-ENTREE,
           MOVE CON-FIN-J TO HE-JJ.
           MOVE CON-FIN-M TO HE-MM.
           MOVE CON-FIN-A TO HE-AA.
           MOVE HE-DATE TO LINK-DATE-SORTIE.
           MOVE CSF-ANNEE-D TO HE-AA.
           MOVE CSF-MOIS-D  TO HE-MM.
           MOVE HE-DATE TO LINK-DATE-DEBUT.
           MOVE CSF-ANNEE-F TO HE-AA.
           MOVE CSF-MOIS-F  TO HE-MM.
           MOVE HE-DATE TO LINK-DATE-FIN.
           MOVE TODAY-ANNEE TO HE-AA.
           MOVE TODAY-MOIS  TO HE-MM.
           MOVE TODAY-JOUR  TO HE-JJ.
           MOVE HE-DATE TO LINK-DATE.
           MOVE CSF-DONNEE-1 TO HE-MM.
           MOVE HE-MM TO LINK-DUREE-ANS.

       IBAN.       
           IF BQF-PART(IDX-1) > SPACES
              ADD 1 TO IDX
              STRING BQF-PART(IDX-1) DELIMITED BY " " INTO 
              LINK-EMPL-COMPTE
              POINTER IDX ON OVERFLOW CONTINUE END-STRING.

       AFFICHAGE-ECRAN.
           MOVE 1302 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           MOVE 72 TO LNK-NUM.
           MOVE "SL" TO LNK-AREA.
           MOVE 12057000 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
           DISPLAY "TRIMESTER" LINE 10 POSITION 5 LOW.
           DISPLAY TRIMESTER LINE 10 POSITION 17.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
           
       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "RFIS" USING LINK-V LINK-RECORD.
           CANCEL "RFIS".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-MOIS TO LNK-MOIS.
           MOVE SAVE-LG TO LNK-LANGUAGE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

