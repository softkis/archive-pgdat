      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-CTLJRS CORRECTION EVTL JOURS SI DEPART    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-CTLJRS.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.

           COPY "LIVRE.REC".
       01  IDX          PIC 99 VALUE 0.
       01  ACTION       PIC X.
       01  DEL-KEY               PIC 9(4) COMP-1 VALUE 98.
       01  WR-KEY               PIC 9(4) COMP-1 VALUE 99.

       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999 VALUE 0.


       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".

       PROCEDURE DIVISION USING LINK-V REG-RECORD PRESENCES.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-A-NXLP.

           IF ANNEE-JOURS NOT = 0 
              IF LNK-SUFFIX NOT = ANNEE-JOURS
                 CLOSE JOURS
                 MOVE 0 TO ANNEE-JOURS 
              END-IF
           END-IF.

           IF ANNEE-JOURS = 0 
              MOVE LNK-SUFFIX TO ANNEE-JOURS
              OPEN I-O JOURS
           END-IF.

           INITIALIZE JRS-RECORD.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE REG-PERSON TO JRS-PERSON. 
           MOVE LNK-MOIS   TO JRS-MOIS
           START JOURS KEY >= JRS-KEY INVALID EXIT PROGRAM.
           PERFORM READ-JOURS.
           EXIT PROGRAM.

       READ-JOURS.
           READ JOURS NEXT NO LOCK AT END EXIT PROGRAM.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS
              EXIT PROGRAM.
           MOVE 0 TO JRS-HRS(32).
           PERFORM TEST-PRES VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.
           IF JRS-HRS(32) = 0
              IF  JRS-OCCUPATION > 0 AND < 11
              AND JRS-COMPLEMENT = 0
                  PERFORM EFFACE-MALADIE
              END-IF
              DELETE JOURS INVALID CONTINUE
              IF LNK-SQL = "Y" 
                 CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY 
              END-IF
           ELSE
              IF LNK-SQL = "Y" 
                 CALL "9-JOURS" USING LINK-V JRS-RECORD WR-KEY 
              END-IF
              REWRITE JRS-RECORD INVALID CONTINUE
           END-IF.
           START JOURS KEY > JRS-KEY INVALID EXIT PROGRAM.
           GO READ-JOURS.

       TEST-PRES.
           IF PRES-JOUR(LNK-MOIS, IDX) = 0
              MOVE 0 TO JRS-HRS(IDX).
           IF JRS-HRS(IDX) > 0
              ADD JRS-HRS(IDX) TO JRS-HRS(32).

       EFFACE-MALADIE.
           MOVE JRS-OCCUPATION TO LNK-SUITE.
           CALL "4-CSPDEL" USING LINK-V REG-RECORD.
           CANCEL "4-CSPDEL".
           INITIALIZE L-RECORD.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD DEL-KEY.
           MOVE 0 TO LNK-SUITE.
