      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-LPCUM CUMUL LIVRE DE PAIE                 �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      * 1 = cumul ann괻 en cours -> mois pr괹괺ent sans maladies
      * 2 = cumul trois mois
      * 3 = cumul douze mois
      * 4 = cumul ann괻 en cours -> mois pr괹괺ent toutes maladies
      * 5 = cumul ann괻 en cours -> mois pr괹괺ent + maladies avanc괻s

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-LPCUM.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

           COPY "LIVRE.REC".
       01  MOIS         PIC 99.
       01  IDX          PIC 9999 VALUE 0.
       01  IDX-1        PIC 9999 VALUE 0.
       01  ANNEE        PIC 9999 VALUE 0.
       01  RUPTURE      PIC 99 VALUE 0.
       01  action       PIC 9.
       01  SAVE-KEY     PIC 9(4) COMP-1 VALUE 0.
       01  SH-00        PIC S999.
       01  SAVE-MOIS    PIC 99 VALUE 0.
       01  SAVE-SUITE   PIC 99 VALUE 0.
       01  SAVE-SUFFIX  PIC 999 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "LIVRE.CUM".

       PROCEDURE DIVISION USING 
                          LINK-V 
                          REG-RECORD 
                          LCUM-RECORD.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-LPCUM.

           MOVE LNK-MOIS   TO SAVE-MOIS.
           MOVE LNK-SUITE  TO SAVE-SUITE.
           MOVE LNK-SUFFIX TO SAVE-SUFFIX.
           INITIALIZE LCUM-RECORD L-RECORD RUPTURE LNK-SUITE.
           SUBTRACT 1 FROM LNK-SUFFIX.
           COMPUTE SH-00 = LNK-MOIS - 3.
           IF SH-00 <= 0
              ADD 12 TO SH-00
              MOVE SH-00 TO RUPTURE
           END-IF.
           MOVE LNK-MOIS TO L-MOIS.
           MOVE 13 TO SAVE-KEY.
           PERFORM READ-LIVRE THRU READ-LIVRE-END.
           ADD 1 TO LNK-SUFFIX.
           MOVE SAVE-MOIS TO LNK-MOIS.
            IF LNK-MOIS = 1
      *       janvier
               PERFORM EXIT-PROGRAM
            END-IF.
            
           INITIALIZE L-RECORD RUPTURE.
           COMPUTE SH-00 = LNK-MOIS - 3.
           IF SH-00 <= 0
              MOVE 1 TO SH-00
           END-IF.
           MOVE SH-00 TO RUPTURE.
           MOVE 13 TO SAVE-KEY.
           MOVE 1 TO L-MOIS.
           PERFORM READ-LIVRE THRU READ-LIVRE-END.
           PERFORM EXIT-PROGRAM.

       READ-LIVRE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD SAVE-KEY.
           IF L-FIRME = 0
              IF SAVE-KEY = 66
                 GO READ-LIVRE-END
              ELSE
                 MOVE 66 TO SAVE-KEY
                 GO READ-LIVRE
              END-IF
           END-IF.
           MOVE 66 TO SAVE-KEY.
           IF LNK-SUFFIX < SAVE-SUFFIX
              IF L-SUITE = 0
                 MOVE 3 TO IDX-1
                 PERFORM CUMUL
                 IF RUPTURE > 0
                 AND L-MOIS >= RUPTURE
                    MOVE 2 TO IDX-1
                    PERFORM CUMUL
                 END-IF
              END-IF
           END-IF.
           IF LNK-SUFFIX = SAVE-SUFFIX
              IF L-MOIS >= SAVE-MOIS
                 GO READ-LIVRE-END
              END-IF
              MOVE 4 TO IDX-1
              PERFORM CUMUL
              IF L-FLAG-AVANCE = 0
                 MOVE 5 TO IDX-1
                 PERFORM CUMUL
              END-IF
              IF L-SUITE = 0
                 MOVE 1 TO IDX-1
                 PERFORM CUMUL
                 MOVE 3 TO IDX-1
                 PERFORM CUMUL
                 IF RUPTURE > 0
                 AND L-MOIS >= RUPTURE
                    MOVE 2 TO IDX-1
                    PERFORM CUMUL
                 END-IF
              END-IF
           END-IF.
           GO READ-LIVRE.
       READ-LIVRE-END.
           EXIT.

       CUMUL.
           PERFORM CUMUL-DC  VARYING IDX FROM 1 BY 1 UNTIL IDX > 300.
           PERFORM CUMUL-UN  VARYING IDX FROM 1 BY 1 UNTIL IDX > 200.
           PERFORM CUMUL-JRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 70.
           PERFORM CUMUL-COT VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.
           PERFORM CUMUL-COT VARYING IDX FROM 31 BY 1 UNTIL IDX > 70.
           IF L-SUITE = 0
              PERFORM CUMUL-COT VARYING IDX FROM 21 BY 1 UNTIL IDX > 30.
           PERFORM CUMUL-ABA VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           PERFORM CUMUL-IMP VARYING IDX FROM 1 BY 1 UNTIL IDX > 40.
           PERFORM CUMUL-SNO VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           PERFORM CUMUL-MOY VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           PERFORM CUMUL-FLA VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.

       CUMUL-DC.
           ADD L-DC(IDX)        TO LCUM-DC(IDX-1, IDX).
       CUMUL-UN.
           ADD L-UNITE(IDX)     TO LCUM-UNITE(IDX-1, IDX).
       CUMUL-JRS.
           ADD L-JR-OCC(IDX)    TO LCUM-JRS(IDX-1, IDX).
       CUMUL-COT.
           ADD L-COT(IDX)       TO LCUM-COT(IDX-1, IDX).
       CUMUL-ABA.
           ADD L-ABAT(IDX)      TO LCUM-ABAT(IDX-1, IDX).
       CUMUL-IMP.
           ADD L-IMPOS(IDX)     TO LCUM-IMPOS(IDX-1, IDX).
       CUMUL-SNO.
           ADD L-SNOCS(IDX)     TO LCUM-SNOCS(IDX-1, IDX).
       CUMUL-MOY.
           ADD L-MOY(IDX)       TO LCUM-MOY(IDX-1, IDX).
       CUMUL-FLA.
           ADD L-FLAG(IDX)      TO LCUM-FLAG(IDX-1, IDX).

       EXIT-PROGRAM.
           MOVE SAVE-SUITE TO LNK-SUITE.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           EXIT PROGRAM.
      
