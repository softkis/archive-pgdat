      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-DECMAL DECLARATION DES MALADIES           �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-DECMAL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.

      *    Fichier DECMAL

           SELECT OPTIONAL DECMAL ASSIGN TO DISK PARMOD-PATH-REAL
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-HELP.

       DATA DIVISION.


       FILE SECTION.
      *컴컴컴컴컴컴

       FD  DECMAL
           RECORD VARYING DEPENDING IDX-4
           DATA RECORD IS DECMAL-REC.
       01  DECMAL-REC.
           02  DECMAL-X  PIC X OCCURS 1 TO 150 DEPENDING IDX-4.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  PRECISION             PIC 9 VALUE 1.
       01  ADRES.
           02 A-1 PIC X(55)
            VALUE "C:\CETREL\SOFIE\DATA\123456789\TO_CRYPT\DECMAL.DTA".

       01  HE-INPUT-PARAMETER.
      *    LINE COL SIZE HE-SCREEN    LLCCSZECRA
           02 IP-101 PIC 9(10)  VALUE 0405250030.
           02 IP-102 PIC 9(10)  VALUE 0605250035.
           02 IP-103 PIC 9(10)  VALUE 0705250036.
           02 IP-104 PIC 9(10)  VALUE 0905250100.
           02 IP-105 PIC 9(10)  VALUE 1205250208.
           02 IP-106 PIC 9(10)  VALUE 1905250115.
           02 IP-107 PIC 9(10)  VALUE 0450150010.
           02 IP-108 PIC 9(10)  VALUE 0550150013.
           02 IP-DEC PIC 9(10)  VALUE 2305250099.

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
            03 I-PP OCCURS 9.
               04 IP-LINE  PIC 99.
               04 IP-COL   PIC 99.
               04 IP-SIZE  PIC 99.
               04 IP-ECRAN PIC 9999.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "LIVRE.REC".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".
           COPY "JOURS.REC".
           COPY "PARMOD.REC".
           COPY "DECMAL.REC".
           COPY "CARRIERE.REC".

       01  REC-MATR.
           02  REC-MAT PIC X(28).
           02  REC-MATR-R REDEFINES REC-MAT.
               10  MATR-ID1         PIC 9(13).
               10  MATR-CONVENTION  PIC X(6).
               10  MATR-SECULINE    PIC 9(9).

       01  RECO-DATE PIC 9(8) VALUE 0.
       01  RECO-DATE-R REDEFINES RECO-DATE.
           02 RECO-A PIC 9999.
           02 RECO-M PIC 99.
           02 RECO-J PIC 99.
       01  RENEW                 PIC 9  VALUE 0.
       01  TYP                   PIC 9   VALUE 0.
       01  JOUR                  PIC 99  VALUE 0.
       01  JOUR-D                PIC 99  VALUE 0.
       01  JOUR-F                PIC 99  VALUE 0.
       01  JOURS                 PIC 99V99  VALUE 0.

       01  TOTAL-HEURES.
           02 SUITES PIC 99V99 OCCURS 5.
           02 SOUS-TOTAUX.
              03 TOT-H OCCURS 5.
                 04 IDX-HRS   PIC 9.
                 04 TOT-HRS   PIC S999V99 OCCURS 31.

       01  COMPTEUR              PIC 99 COMP-1.
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  SAVE-FIRME            PIC 9(6).
       01  LAST-FIRME            PIC 9(6) VALUE 0.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.
           COPY "V-VH00.CPY".

       01  HEADER.
           02 HD-TYPE        PIC 9 VALUE 0.
           02 HD-DELIM       PIC X VALUE ";".
           02 HD-MATR        PIC 9(13).
           02 HD-DELIM       PIC X VALUE ";".
           02 HD-CONVENTION  PIC 9(6).

       01  DECMAL-BODY.
           02 DECMAL-TYPE           PIC 9 VALUE 1.
           02 DECMAL-DELIM          PIC X VALUE ";".
           02 DECMAL-MATR           PIC 9(13).
           02 DECMAL-DELIM          PIC X VALUE ";".
           02 DECMAL-MATP           PIC X(11).
           02 DECMAL-DELIM          PIC X VALUE ";".
           02 DECMAL-ANNEE          PIC 9(4).
           02 DECMAL-MOIS           PIC 99.
           02 DECMAL-DELIM          PIC X VALUE ";".
           02 DECMAL-TYPE-INC       PIC 9.
           02 DECMAL-DELIM          PIC X VALUE ";".
           02 DECMAL-DEBUT          PIC 9(8).
           02 DECMAL-DEBUT-R REDEFINES DECMAL-DEBUT.
              03 DECMAL-A PIC 9999.
              03 DECMAL-M PIC 99.
              03 DECMAL-J PIC 99.
           02 DECMAL-DELIM          PIC X VALUE ";".
           02 DECMAL-FIN            PIC 9(8).
           02 DECMAL-FIN-R REDEFINES DECMAL-FIN.
              03 DECMAL-AA PIC 9999.
              03 DECMAL-MM PIC 99.
              03 DECMAL-JJ PIC 99.
           02 DECMAL-DELIM          PIC X VALUE ";".
           02 DECMAL-HEURES         PIC 9(3).
           02 DECMAL-DELIM          PIC X VALUE ";".
           02 DECMAL-FIRME          PIC 9(4).
           02 DECMAL-DELIM          PIC X VALUE "_".
           02 DECMAL-PERSON         PIC 9(6).
           02 DECMAL-DELIM          PIC X VALUE "_".
           02 DECMAL-NOM            PIC X(18) VALUE SPACES.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z9Z2 PIC Z(9),ZZ.
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

           02 HE-DECMAL.
              03 HE-A PIC ZZZZ.
              03 FILLER PIC XX VALUE "  ".
              03 HE-M PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HE-J PIC 99.
              03 FILLER PIC XX VALUE "  ".
              03 HE-S PIC ZZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON DECMAL.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-DECMAL.
       
           MOVE LNK-DATE TO SAVE-DATE.
           INITIALIZE PARMOD-RECORD.
           MOVE "SECULINE" TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-MATR TO REC-MATR.

           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           IF MENU-PROG-NUMBER = 1 
              MOVE LNK-USER TO PARMOD-USER.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           IF PARMOD-PATH3 = SPACES
              MOVE PARMOD-SETTINGS TO PARMOD-MATR
              MOVE PARMOD-PATH2 TO PARMOD-PATH1
              MOVE SPACES TO PARMOD-PATH2 
           END-IF.
           MOVE LNK-LANGUAGE TO SAVE-LANGUAGE.
           CALL "0-TODAY" USING TODAY.
           MOVE TODAY-DATE TO RECO-DATE.
           MOVE FR-KEY TO SAVE-FIRME.

           PERFORM AFFICHAGE-ECRAN .
           MOVE 0 TO LNK-NUM.
           IF  LNK-ANNEE > TODAY-ANNEE
           AND LNK-ANNEE > 2008
              MOVE 48 TO LNK-NUM
           END-IF.
           IF LNK-ANNEE   = TODAY-ANNEE
              IF LNK-MOIS = TODAY-MOIS 
              AND TODAY-JOUR > 28
                 CONTINUE
              ELSE
                 COMPUTE IDX = TODAY-MOIS - 1
                 IF LNK-MOIS > IDX
                    MOVE 55 TO LNK-NUM
                 END-IF
              END-IF
           END-IF.
           IF LNK-ANNEE < 2009
              MOVE 95 TO LNK-NUM
           END-IF.
           IF MENU-PROG-NUMBER = 99
              MOVE 0 TO LNK-NUM
           END-IF.
           IF LNK-NUM > 0
              MOVE "SL" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
              ACCEPT ACTION 
              EXIT PROGRAM
           END-IF.
           INITIALIZE FR-RECORD.
           PERFORM AFFICHAGE-DETAIL.
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-PATH
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  5 PERFORM APRES-PATH
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-5. 
           MOVE 12250000 TO LNK-POSITION.
           IF RECO-DATE = TODAY-DATE
              MOVE PARMOD-PROG-NUMBER-1 TO RECO-DATE
           END-IF.
           CALL "0-GDATE" USING RECO-DATE
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
       AVANT-6.
           IF PARMOD-D(1) =  " "
              MOVE "N" TO PARMOD-D(1).
           ACCEPT PARMOD-D(1)
             LINE 19 POSITION 25 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PATH.
           IF PARMOD-PATH-PROTO = SPACES
              MOVE ADRES TO PARMOD-PATH-PROTO
           END-IF.
           INSPECT PARMOD-PATH-PROTO REPLACING ALL "123456789"
           BY MATR-SECULINE.
           MOVE 10057000 TO LNK-POSITION.
           MOVE SPACES TO LNK-LOW.
           CALL "0-GPATH" USING LINK-V PARMOD-RECORD EXC-KEY.
           IF EXC-KEY = 12
              MOVE ADRES TO PARMOD-PATH-PROTO
              GO AVANT-PATH
           END-IF.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-PATH.
           IF LNK-LOW = "!" 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF LNK-NUM > 0 
              MOVE  0 TO LNK-POSITION
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-6.
           IF PARMOD-D(1) = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE "N" TO PARMOD-D(1)
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE "N" TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       TRAITEMENT.
           IF A-N = "A"
              MOVE 9999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           IF FR-KEY = END-NUMBER
              MOVE " " TO FR-SNOCS-YN 
           END-IF.
           IF FR-KEY = 0
              MOVE 66 TO EXC-KEY
           ELSE
              MOVE 13 TO EXC-KEY.
           PERFORM READ-FIRME THRU READ-FIRME-END.
           PERFORM END-PROGRAM.

       READ-FIRME.
           PERFORM NEXT-FIRME.
           MOVE 66 TO EXC-KEY.
           IF FR-KEY = 0
           OR FR-KEY > END-NUMBER
           OR FR-MATCHCODE > END-MATCHCODE
              GO READ-FIRME-END
           END-IF.
           IF FR-COMPETENCE > LNK-COMPETENCE
              GO READ-FIRME
           END-IF.
           IF FR-FIN-A > 0 AND < LNK-ANNEE
              GO READ-FIRME
           END-IF.
           IF FR-SNOCS-YN = "N" 
           OR FR-EXTENS   = 0
              GO READ-FIRME
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM START-PERSON.
           GO READ-FIRME.
       READ-FIRME-END.
           PERFORM END-PROGRAM.

       START-PERSON.
           INITIALIZE REG-RECORD.
           PERFORM READ-PERSON THRU READ-EXIT.

       READ-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY.
           IF REG-PERSON = 0
              GO READ-EXIT.
           IF REG-SNOCS-YN = "N"
              GO READ-PERSON
           END-IF.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF CAR-REGIME < 1 OR > 99
              GO READ-PERSON
           END-IF.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           PERFORM DIS-HE-00.
           display SAVE-MOIS line 1 position 10
           IF PR-NOM   = SPACES
           OR PR-SNOCS = 0
              GO READ-PERSON
           END-IF.
           PERFORM LIVRE THRU LIVRE-END
               VARYING IDX FROM 1 BY 1 UNTIL IDX > SAVE-MOIS.
           GO READ-PERSON.
       READ-EXIT.
           EXIT.

       LIVRE.
           INITIALIZE L-RECORD DM-RECORD RENEW.
           MOVE IDX TO L-MOIS LNK-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           CALL "6-DECMAL" USING LINK-V REG-RECORD DM-RECORD FAKE-KEY.
           
           COMPUTE SH-00 = L-UNIT(1) 
                         + L-UNIT(2) 
                         + L-UNIT(3) 
                         + L-UNIT(4) 
                         + L-UNIT(5) 
                         + L-UNIT(11).

           IF DM-MEMO(1) = 0
              IF SH-00 = 0
                 GO LIVRE-END
              END-IF
           END-IF.
           IF DM-MEMO(1) < DM-MEMO(2)  
              GO LIVRE-END
           END-IF.

           IF  DM-MEMO(2) > 0
           AND DM-MEMO(1) > DM-MEMO(2) 
              MOVE 1 TO RENEW
              PERFORM FILL-LINE
           END-IF.

           MOVE DM-MOIS  TO LNK-MOIS.
           MOVE DM-ANNEE TO LNK-ANNEE.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS = 13
              ADD  1 TO LNK-ANNEE
              MOVE 1 TO LNK-MOIS.
           INITIALIZE TOTAL-HEURES.
           PERFORM HEURES.
           PERFORM SUITES VARYING TYP FROM 1 BY 1 UNTIL TYP > 5.
           INITIALIZE SOUS-TOTAUX.
           MOVE DM-MOIS  TO LNK-MOIS.
           MOVE DM-ANNEE TO LNK-ANNEE.
           PERFORM HEURES.
           PERFORM SUITES VARYING TYP FROM 1 BY 1 UNTIL TYP > 5.
           MOVE 2 TO LNK-STATUS.
           CALL "6-DECMAL" USING LINK-V REG-RECORD DM-RECORD WR-KEY.
       
       LIVRE-END.
           EXIT.

       NEXT-FIRME.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.

       OPEN-FILE.
           OPEN OUTPUT DECMAL
           MOVE MATR-ID1 TO HD-MATR 
           MOVE MATR-CONVENTION TO HD-CONVENTION
           MOVE 22 TO IDX-4
           WRITE DECMAL-REC FROM HEADER
           DISPLAY DECMAL-REC LINE 24 POSITION 25 SIZE 35
           IF FS-HELP = "30"
              MOVE 1 TO INPUT-ERROR
           END-IF.
           MOVE TODAY-DATE TO PARMOD-PROG-NUMBER-1.
           MOVE 1 TO NOT-OPEN.

       HEURES.
           INITIALIZE JRS-RECORD.
           MOVE LNK-MOIS TO JRS-MOIS.
           PERFORM READ-HEURES THRU READ-HEURES-END.

       READ-HEURES.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD NX-KEY.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
           OR JRS-OCCUPATION > 11
              GO READ-HEURES-END
           END-IF.
           IF JRS-COMPLEMENT > 0
           OR JRS-OCCUPATION < 1
              GO READ-HEURES.
           INITIALIZE L-RECORD.
           IF JRS-OCCUPATION < 6
              MOVE JRS-OCCUPATION TO LNK-SUITE
              CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           MOVE 1 TO TYP.
           IF JRS-OCCUPATION = 8
              MOVE 2 TO TYP.
           IF JRS-OCCUPATION = 9
              MOVE 5 TO TYP.
famil      IF L-FLAG(4) NOT = 0
              MOVE 2 TO TYP.
dispe      IF L-FLAG(6) NOT = 0
mater      OR L-SUITE = 5
              MOVE 3 TO TYP.
accue      IF  L-FLAG(4) NOT = 0
mater      AND L-SUITE = 10
              MOVE 4 TO TYP.
accom      IF L-FLAG(7) NOT = 0
              MOVE 5 TO TYP.
           MOVE 1 TO IDX-HRS(TYP).
           PERFORM ADD-HEURES VARYING JOUR FROM 1 BY 1 UNTIL
              JOUR > MOIS-JRS(LNK-MOIS).
           GO READ-HEURES.
       READ-HEURES-END.
           IF LNK-MOIS = DM-MOIS
              PERFORM FILL-FILE VARYING TYP FROM 1 BY 1 UNTIL TYP > 5.

       ADD-HEURES.
           IF JRS-HRS(JOUR) NOT = 0 
              MOVE JRS-HRS(JOUR) TO TOT-HRS(TYP, JOUR)
           END-IF.

       FILL-FILE.
           MOVE 0 TO JOUR-D JOUR-F DECMAL-HEURES.
           IF IDX-HRS(TYP) > 0
              PERFORM TEST-PER VARYING JOUR FROM 1 BY 1 UNTIL
              JOUR > MOIS-JRS(DM-MOIS)
           END-IF.
           IF JOUR-D NOT = 0
              PERFORM FILL-LINE
           END-IF.

       TEST-PER.
           IF  JOUR-D = 0
           AND TOT-HRS(TYP, JOUR) NOT = 0
              MOVE JOUR TO JOUR-D JOUR-F
           END-IF.
           IF TOT-HRS(TYP, JOUR) NOT = 0
              MOVE JOUR TO JOUR-F
           END-IF.
           IF TOT-HRS(TYP, JOUR) > 0
              ADD TOT-HRS(TYP, JOUR) TO DECMAL-HEURES
           END-IF.
           IF TOT-HRS(TYP, JOUR) = 0
           AND JOUR-D NOT = 0
               PERFORM FILL-LINE
               MOVE 0 TO DECMAL-HEURES
           END-IF.

       SUITES.
           MOVE TOT-HRS(TYP, 1) TO SUITES(TYP).
           IF  DM-MEMO(2) > 0
           AND DM-MEMO(1) < DM-MEMO(2) 
              PERFORM FILL-LINE.

       FILL-LINE.
           IF NOT-OPEN = 0 
              PERFORM OPEN-FILE
           END-IF.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO DECMAL-NOM.
           MOVE DM-ANNEE TO DECMAL-ANNEE DECMAL-A DECMAL-AA.
           MOVE DM-MOIS  TO DECMAL-MOIS  DECMAL-M DECMAL-MM.
           MOVE FR-MATRICULE TO DECMAL-MATR.
           MOVE PR-MATRICULE TO DECMAL-MATP.
           MOVE FR-KEY     TO DECMAL-FIRME
           MOVE REG-PERSON TO DECMAL-PERSON.
           IF RENEW = 0
              PERFORM TYPE-1
           ELSE
              PERFORM TYPE-2
           END-IF.
           MOVE 0 TO JOUR-D JOUR-F.

       WRITE-REC.
           MOVE 91 TO IDX-4.
           WRITE DECMAL-REC FROM DECMAL-BODY.

       TYPE-1.
           MOVE 1 TO DECMAL-TYPE
           MOVE JOUR-D TO DECMAL-J.
           MOVE JOUR-F TO DECMAL-JJ.
           MOVE TYP TO DECMAL-TYPE-INC.
           IF  SUITES(TYP) > 0
           AND JOUR-F = MOIS-JRS(DM-MOIS)
              INITIALIZE DECMAL-FIN
           END-IF.
           PERFORM WRITE-REC.
           IF LIN-NUM >= 66
              PERFORM TRANSMET
              INITIALIZE LIN-NUM FORMULAIRE
           END-IF.
           IF LAST-FIRME NOT = FR-KEY
           AND LIN-NUM > 1
              IF PARMOD-D(1) NOT = "N"
              OR LIN-NUM >= 66
                 PERFORM TRANSMET
                 INITIALIZE LIN-NUM FORMULAIRE
              END-IF
           END-IF.

           IF LAST-FIRME NOT = FR-KEY
           AND LIN-NUM > 66
              PERFORM TRANSMET
              INITIALIZE LIN-NUM FORMULAIRE
           END-IF.
           IF LIN-NUM = 0
              MOVE 1 TO LIN-NUM
              MOVE MENU-DESCRIPTION TO ALPHA-TEXTE
              MOVE 5 TO COL-NUM
              PERFORM FILL-FORM
              MOVE 100 TO COL-NUM
              MOVE LNK-ANNEE TO ALPHA-TEXTE
              PERFORM FILL-FORM

              MOVE 60 TO COL-NUM
              MOVE TODAY-JOUR  TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE TODAY-MOIS  TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE TODAY-ANNEE TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 3 TO COL-NUM
              MOVE TODAY-HEURE TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE TODAY-MIN   TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE 0 TO LAST-FIRME
           END-IF.
           PERFORM DIS-HE-00.
           IF LAST-FIRME NOT = FR-KEY
              ADD 2 TO LIN-NUM
              MOVE FR-KEY TO VH-00 LAST-FIRME
              MOVE 3 TO COL-NUM
              MOVE 6 TO CAR-NUM
              PERFORM FILL-FORM
              MOVE 10 TO COL-NUM
              MOVE FR-NOM TO ALPHA-TEXTE
           END-IF.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           MOVE REG-PERSON TO VH-00.
           MOVE 3 TO COL-NUM.
           MOVE 6 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE LNK-MOIS TO VH-00.
           MOVE 85 TO COL-NUM.
           MOVE 2 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE JOUR-D TO VH-00.
           MOVE  90 TO COL-NUM.
           MOVE 2 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE " - " TO ALPHA-TEXTE
           PERFORM FILL-FORM.
           MOVE JOUR-F TO VH-00.
           MOVE 2 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE  DECMAL-HEURES TO VH-00.
           MOVE 100 TO COL-NUM.
           MOVE 3 TO CAR-NUM. 
           MOVE 2 TO DEC-NUM. 
           PERFORM FILL-FORM.

QQ
           MOVE TYP TO VH-00 MS-NUMBER.
           MOVE 110 TO COL-NUM.
           MOVE 1 TO CAR-NUM.
           PERFORM FILL-FORM.
           IF TYP > 1
              MOVE LNK-LANGUAGE TO MS-LANGUAGE
              MOVE "TMAL" TO LNK-AREA
              CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY
              MOVE MS-DESCRIPTION TO ALPHA-TEXTE
              MOVE 112 TO COL-NUM
              PERFORM FILL-FORM.

       TYPE-2.
           MOVE 2 TO DECMAL-TYPE
           INITIALIZE DECMAL-DEBUT 
                      DECMAL-FIN 
                      DECMAL-HEURES 
                      DECMAL-TYPE-INC
                      RENEW.
           PERFORM WRITE-REC.

       DIS-HE-01.
           MOVE FR-KEY    TO HE-Z6.
           DISPLAY HE-Z6  LINE  6 POSITION 17.
           DISPLAY FR-NOM LINE  6 POSITION 25 SIZE 30.
           MOVE END-NUMBER TO HE-Z6.
           DISPLAY HE-Z6  LINE  7 POSITION 17.

       DIS-DATE.
           MOVE RECO-A TO HE-AA.
           MOVE RECO-M TO HE-MM.
           MOVE RECO-J TO HE-JJ.
           DISPLAY HE-DATE LINE 12 POSITION 25.

       DIS-HE-END.
           EXIT.

       DIS-HE-00.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 8 POSITION 17.
           DISPLAY PR-NOM LINE 8 POSITION 25 SIZE 50.

       AFFICHAGE-ECRAN.
           MOVE 32 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           MOVE "EE" TO LNK-AREA.
           MOVE 00042500 TO LNK-POSITION.
           PERFORM LIN VARYING IDX FROM 1 BY 1 UNTIL IDX > 9.
           MOVE 97 TO LNK-NUM
           MOVE "SL" TO LNK-AREA
           MOVE 16100000 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       LIN.
           MOVE IDX TO HE-Z2.
           IF IDX < 7
              DISPLAY HE-Z2 LINE IP-LINE(IDX) POSITION 1 LOW.
           MOVE IP-ECRAN(IDX) TO LNK-NUM.
           MOVE IP-LINE(IDX)  TO LNK-LINE.
           MOVE IP-COL(IDX)   TO LNK-COL.
           MOVE IP-SIZE(IDX)  TO LNK-SIZE.
           MOVE "L" TO LNK-LOW.
           CALL "0-DMESS" USING LINK-V.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
           DISPLAY MATR-ID1        LINE 4 POSITION 65.
           DISPLAY MATR-CONVENTION LINE 5 POSITION 65.
           DISPLAY PARMOD-PATH-PROTO LINE 10 POSITION 5.

       END-PROGRAM.
           IF LIN-NUM > 0
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
           MOVE SAVE-DATE TO LNK-DATE.
           MOVE SAVE-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V A-N FAKE-KEY.
           CANCEL "6-DECMAL".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXF".
           IF  INPUT-ERROR = 0
           AND EXC-KEY = 13
               MOVE CHOIX-MAX TO INDICE-ZONE.
                         
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".
           COPY "XFILL2.CPY".

