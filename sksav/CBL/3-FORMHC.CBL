      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-FORMHC FORMULAIRE DE SAISIE DES HEURES    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-FORMHC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM215.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM215.FDE".
           COPY "TRIPR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "215       ".
       01  CHOIX-MAX             PIC 99 VALUE 8.
       01  MAX-LIGNES            PIC 99 VALUE 10.
       01  PRECISION             PIC 9 VALUE 0.
       01  NEXT-LINE             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "FIRME.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "METIER.REC".
           COPY "MESSAGE.REC".
           COPY "CONTRAT.REC".
           COPY "EQUIPE.REC".
           COPY "POCL.REC".
           COPY "FICHE.REC".
           COPY "DIVISION.REC".
           COPY "REGIME.REC".
           COPY "PAYS.REC".
           COPY "BANQP.REC".
           COPY "BAREME.REC".
           COPY "BARDEF.REC".
           COPY "POINTS.REC".
           COPY "CONTYPE.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(215) OCCURS 45.
       01  SAVE-LANGUE           PIC X.
       01  COMPTEUR              PIC 99 COMP-1.

       01  EQUIPE                PIC 99 VALUE 0.

       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X    VALUE "F".
           02 FORM-POINT         PIC X    VALUE ".".
           02 FORM-EXTENSION     PIC X(3) VALUE "HC".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.
       01  SAL-ACT              PIC 9(7)V9(5).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02 SAL-ACT-A         PIC 9(10).
           02 SAL-ACT-B         PIC 9(2).
       01  SAL-ACT-R1 REDEFINES SAL-ACT.
           02 SAL-ACT-A1        PIC 9(11).
           02 SAL-ACT-B1        PIC 9(1).
       01  SAL-ACT-R2 REDEFINES SAL-ACT.
           02 SAL-ACT-A2        PIC 9(7)V99.
           02 SAL-ACT-B2        PIC 9(3).

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-FORMHC.
       
           MOVE LNK-LANGUAGE TO SAVE-LANGUE.
           IF FR-LANGUE = SPACES
              MOVE "F" TO FR-LANGUE 
           END-IF.
           IF MENU-EXTENSION-1 NOT = SPACES
              MOVE MENU-EXTENSION-1 TO FORM-EXTENSION
           END-IF.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TEST-EXTENSION LIN-NUM LIN-IDX.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 7
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN  8 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-SORT
           WHEN  8 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-3
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-3
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-3
           END-IF.
           IF  EQUIPE > 0 
           AND EQUIPE NOT = CAR-EQUIPE
              GO READ-PERSON-3
           END-IF.
           IF  CHOIX > 0
           AND COUNTER = 0
              IF  TEST-ALPHA  = SPACES
              AND TEST-NUMBER = 0
                 MOVE TRIPR-CHOIX TO TEST-EXTENSION
              END-IF
           END-IF.
           IF CHOIX > 0
           AND CHOIX < 90
           AND COUNTER > 0
              IF TEST-ALPHA  NOT = SPACES
              OR TEST-NUMBER NOT = 0
                 IF TRIPR-CHOIX NOT = TEST-EXTENSION
                 INITIALIZE TEST-EXTENSION
                 PERFORM TRANSMET
                 MOVE 0 TO LIN-NUM
                 PERFORM READ-FORM
              END-IF
           END-IF.
           IF CHOIX > 0
              MOVE TRIPR-CHOIX TO TEST-EXTENSION
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM FILL-PERS.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD CON-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD PV-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE EQ-NUMBER TO HE-Z2.
           DISPLAY HE-Z2  LINE 11 POSITION 31.
           DISPLAY EQ-NOM LINE 11 POSITION 35 SIZE 45.
       DIS-HE-END.
           EXIT.



       ENTETE.
           PERFORM SEMAINE-NOM.
           ADD 1 TO PAGE-NUMBER.
           MOVE 4 TO CAR-NUM.
           MOVE 2 TO LIN-NUM.
           MOVE 120 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

           MOVE  2 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM   TO ALPHA-TEXTE.
           MOVE 18 TO COL-NUM.
           PERFORM FILL-FORM.
           IF STATUT NOT = 0
              MOVE "-" TO ALPHA-TEXTE
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM
              MOVE STAT-NOM TO ALPHA-TEXTE
              ADD  2 TO COL-NUM
              PERFORM FILL-FORM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 18 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           
           MOVE FR-PAYS TO ALPHA-TEXTE.
           ADD  1 TO LIN-NUM.
           MOVE 18 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FR-CODE-POST TO HE-Z5.
           MOVE HE-Z5 TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 4 TO LIN-NUM.
           MOVE 80 TO COL-NUM.
           MOVE LNK-MOIS TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE LNK-ANNEE TO VH-00.
           PERFORM FILL-FORM.
           IF CHOIX NOT = 0 
              PERFORM DETAIL-CHOIX.


       SEMAINE-NOM.
           MOVE  6 TO LIN-NUM.
           MOVE 33 TO COL-NUM.
           PERFORM SEMAINE-INDEX VARYING IDX FROM 1 BY 1 UNTIL 
                    IDX > MOIS-JRS(LNK-MOIS).

       SEMAINE-INDEX.
           COMPUTE LNK-NUM = SEM-IDX(LNK-MOIS, IDX) + 200.
           PERFORM SEM-NOM.
           ADD 2 TO COL-NUM.
           PERFORM FILL-FORM.
           IF SEM-IDX(LNK-MOIS, IDX) = 7
              SUBTRACT 3 FROM COL-NUM
              PERFORM MARQUE-DIM VARYING LIN-NUM FROM 11 BY 4
                                    UNTIL LIN-NUM > 40
              ADD 3 TO COL-NUM
              MOVE 6 TO LIN-NUM
           END-IF.                                    

       MARQUE-DIM.
           MOVE "拙" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           SUBTRACT 2 FROM COL-NUM.

       DETAIL-CHOIX.
           MOVE  2 TO LIN-NUM.
           MOVE 75 TO COL-NUM.
           MOVE TEST-NUMBER TO LNK-POSITION.
           MOVE TEST-ALPHA  TO LNK-TEXT.
           MOVE CHOIX       TO LNK-NUM.
           CALL "4-CHOIX" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

                 MOVE LNK-LANGUAGE TO CT-LANGUE
                 CALL "6-CONTYP" USING LINK-V CT-RECORD FAKE-KEY
                 MOVE CT-NOM TO ALPHA-TEXTE
              END-IF
              PERFORM FILL-FORM
           END-IF.

       FILL-PERS.
           IF COUNTER = 0 
              MOVE FR-LANGUE TO LNK-LANGUAGE FORM-LANGUE
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER 
           END-IF.
           COMPUTE IDX = LIN-NUM + MAX-LIGNES.
           IF LIN-NUM > 35
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              COMPUTE LIN-NUM = 4 
           END-IF.

           ADD   4 TO LIN-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  3 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           IF PR-NOM-JF NOT = SPACES
              ADD 1 TO COL-NUM
              MOVE PR-NOM-JF TO ALPHA-TEXTE
              PERFORM FILL-FORM.
           ADD 3 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE CON-DEBUT-A TO HE-AA.
           MOVE CON-DEBUT-M TO HE-MM.
           MOVE CON-DEBUT-J TO HE-JJ.
           MOVE HE-DATE TO ALPHA-TEXTE.
           MOVE 84 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE CAR-METIER TO MET-CODE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           MOVE 100 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE CAR-POSITION TO ALPHA-TEXTE.
           ADD 3 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE CAR-EQUIPE TO EQ-NUMBER.
           CALL "6-EQUIPE" USING LINK-V EQ-RECORD FAKE-KEY.
           MOVE EQ-NOM TO ALPHA-TEXTE.
           ADD 3 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE PR-NAISS-A TO ALPHA-TEXTE.
           MOVE 180 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE PR-NAISS-M TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE PR-NAISS-J TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE PR-SNOCS TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L215" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 1300 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF LIN-NUM > 0
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "L215" USING LINK-V FORMULAIRE.
           CANCEL "L215".
           CANCEL "4-SORT"
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-LANGUE TO LNK-LANGUAGE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XEQUIPE.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XSORT.CPY".
           COPY "XSEMNOM.CPY".

