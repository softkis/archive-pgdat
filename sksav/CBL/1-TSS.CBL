      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-TSS TAUX COTISATIONS SECURITE SOCIALE     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-TSS.

       ENVIRONMENT DIVISION.
      *袴袴袴袴袴袴袴袴袴袴
       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "TAUXSS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "TAUXSS.FDE".

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  HE-INPUT-PARAMETER.
      *    LINE COL SIZE HE-SCREEN    LLCCSZHELP
           02 IP-101 PIC 9(10)  VALUE 0328040000.
           02 IP-102 PIC 9(10)  VALUE 0428040000.
           02 IP-103 PIC 9(10)  VALUE 0530020000.

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
           02 I-P OCCURS 1.
              03 I-PP OCCURS  3.
                 04 IP-LINE PIC 99.
                 04 IP-COL  PIC 99.
                 04 IP-SIZE PIC 99.
                 04 IP-HELP PIC 9999.

       01  ECRAN-SUITE.
           02 ECR-S1             PIC 999 VALUE 51.
           02 ECR-S2             PIC 999 VALUE 52.
           02 ECR-S2             PIC 999 VALUE 53.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S              PIC 999 OCCURS 3.

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE 44.
           02  CHOIX-MAX-2       PIC 99 VALUE 21.
           02  CHOIX-MAX-3       PIC 99 VALUE 31.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 3.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "REGIME.REC".
           COPY "INDEX.REC".
           COPY "V-VAR.CPY".


       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ.
           02 HE-Z5 PIC ZZZZZ.
           02 HE-ZT PIC ZZ,ZZZZ.
           02 HE-Z2Z2 PIC ZZ,ZZ.
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6Z2 PIC Z(6),ZZ.
           02 HE-Z5Z2 PIC Z(5),ZZ.
           02 HE-Z5Z5 PIC Z(5),Z(5).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TAUXSS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-TSS.
       
           OPEN I-O   TAUXSS.

           INITIALIZE SS-RECORD.
           MOVE 1 TO ECRAN-IDX SS-REGIME.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions
           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
              WHEN 1     MOVE 0029000015 TO EXC-KFR (1)
                         MOVE 0000000065 TO EXC-KFR(13)
                         MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2 THRU 3
                         MOVE 0000080000 TO EXC-KFR (2)
                         MOVE 0000000065 TO EXC-KFR(13)
                         MOVE 6600000000 TO EXC-KFR(14)
              WHEN CHOIX-MAX(1)
                         MOVE 0000000005 TO EXC-KFR (1)
                         MOVE 0000080000 TO EXC-KFR (2)
                         MOVE 0052000000 TO EXC-KFR (11)
                         MOVE 0000680000 TO EXC-KFR (14) 
              WHEN OTHER MOVE 0000000005 TO EXC-KFR (1)
                         MOVE 0012130000 TO EXC-KFR (3)
                         MOVE 0000000065 TO EXC-KFR (13) 
                         MOVE 6600680000 TO EXC-KFR (14) 
           END-IF.

           IF ECRAN-IDX = 2
              MOVE 0067680000 TO EXC-KFR(14) 
              EVALUATE INDICE-ZONE
              WHEN CHOIX-MAX(ECRAN-IDX)
                   MOVE 0100000005 TO EXC-KFR(1)
                   MOVE 0000080000 TO EXC-KFR(2)
                   MOVE 0052000000 TO EXC-KFR(11)
              WHEN OTHER MOVE 0100000005 TO EXC-KFR(1)
                         MOVE 0009130000 TO EXC-KFR(3)
            END-IF.    

           IF ECRAN-IDX = 3
              MOVE 0067000000 TO EXC-KFR(14) 
              EVALUATE INDICE-ZONE
              WHEN  1 THRU 10
                   MOVE 0100000005 TO EXC-KFR(1)
                   MOVE 0000000073 TO EXC-KFR(2)
                   MOVE 0009130000 TO EXC-KFR(3)
              WHEN 11 THRU 30
                   MOVE 0100000005 TO EXC-KFR(1)
                   MOVE 0000000026 TO EXC-KFR(2)
                   MOVE 0009130000 TO EXC-KFR(3)
              WHEN CHOIX-MAX(ECRAN-IDX)
                   MOVE 0100000005 TO EXC-KFR(1)
                   MOVE 0000080000 TO EXC-KFR(2)
                   MOVE 0052000000 TO EXC-KFR(11)
            END-IF.    

           IF IP-HELP(ECRAN-IDX, INDICE-ZONE) > 0
              MOVE 1 TO EXC-KEY-FUN(1) 
           END-IF.
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-1-1 
               WHEN  2 PERFORM AVANT-1-2 
               WHEN  3 PERFORM AVANT-1-3 
               WHEN  4 THRU 43 PERFORM AVANT-ALL THRU AVANT-ALL-END
               WHEN CHOIX-MAX(1) PERFORM AVANT-DEC
           END-IF.

           IF ECRAN-IDX = 2
              EVALUATE INDICE-ZONE
              WHEN  1 THRU 10 PERFORM AVANT-2-ALL
              WHEN 11 THRU 20 PERFORM AVANT-2-YN
              WHEN CHOIX-MAX(ECRAN-IDX) PERFORM AVANT-DEC
           END-IF.

           IF ECRAN-IDX = 3
              EVALUATE INDICE-ZONE
              WHEN  1 THRU 10 PERFORM AVANT-3-PLAF
              WHEN 11 THRU 20 PERFORM AVANT-3-PLAF-100
              WHEN 21 THRU 30 PERFORM AVANT-3-ABAT
              WHEN CHOIX-MAX(ECRAN-IDX) PERFORM AVANT-DEC
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 1 
              MOVE IP-HELP(ECRAN-IDX, INDICE-ZONE) TO LNK-VAL
              PERFORM HELP-SCREEN
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF  EXC-KEY > 66 
           AND EXC-KEY < 69 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              IF ECRAN-IDX = 1
                 MOVE 4 TO INDICE-ZONE
              ELSE
                 MOVE 1 TO INDICE-ZONE
              END-IF
              GO TRAITEMENT-ECRAN
           END-IF.
           
           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
                WHEN 1 PERFORM APRES-1-1 
                WHEN 2 PERFORM APRES-1-2 
                WHEN 3 PERFORM APRES-1-3 
                WHEN 4 THRU 43 PERFORM APRES-1-ALL
                WHEN CHOIX-MAX(1) PERFORM APRES-DEC
              END-EVALUATE
           END-IF.

           IF ECRAN-IDX = 2
              EVALUATE INDICE-ZONE
              WHEN 11 THRU 20 PERFORM APRES-2-YN
              WHEN CHOIX-MAX(ECRAN-IDX) PERFORM APRES-DEC
              WHEN OTHER PERFORM APRES-2-ALL.

           IF ECRAN-IDX = 3
              EVALUATE INDICE-ZONE
              WHEN CHOIX-MAX(ECRAN-IDX) PERFORM APRES-DEC
              WHEN 1 THRU 10 PERFORM APRES-3-PLAF
              WHEN OTHER PERFORM APRES-3-PLAF-100.

           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 52 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 12 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 13 ADD 1 TO INDICE-ZONE
                WHEN 53 ADD 1 TO INDICE-ZONE.

           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.
           ACCEPT SS-REGIME
           LINE     IP-LINE (ECRAN-IDX, INDICE-ZONE) 
           POSITION IP-COL  (ECRAN-IDX, INDICE-ZONE) 
           SIZE     IP-SIZE (ECRAN-IDX, INDICE-ZONE) 
           TAB UPDATE NO BEEP CURSOR  1 REVERSE
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           IF SS-ANNEE = 0
              MOVE LNK-ANNEE TO SS-ANNEE
           END-IF.
           ACCEPT SS-ANNEE
           LINE     IP-LINE (ECRAN-IDX, INDICE-ZONE) 
           POSITION IP-COL  (ECRAN-IDX, INDICE-ZONE) 
           SIZE     IP-SIZE (ECRAN-IDX, INDICE-ZONE) 
           TAB UPDATE NO BEEP CURSOR  1 REVERSE
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.
           ACCEPT SS-MOIS  
           LINE     IP-LINE (ECRAN-IDX, INDICE-ZONE) 
           POSITION IP-COL  (ECRAN-IDX, INDICE-ZONE) 
           SIZE     IP-SIZE (ECRAN-IDX, INDICE-ZONE) 
           TAB UPDATE NO BEEP CURSOR  1 REVERSE
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
           IF INDICE-ZONE < 4
           OR INDICE-ZONE > 43
              GO AVANT-ALL-END.
       AVANT-ALL-1.
           PERFORM AVANT-PARAM.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL-1.
           EVALUATE EXC-KEY
                WHEN  2 ADD 1 TO INDICE-ZONE
                WHEN  5 MOVE 44 TO INDICE-ZONE 
                WHEN 12 MOVE 44 TO INDICE-ZONE
                WHEN 13 ADD 1 TO INDICE-ZONE
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 53 ADD 4 TO INDICE-ZONE
                WHEN 52 SUBTRACT 4 FROM INDICE-ZONE
                WHEN 65 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 66 ADD 1 TO INDICE-ZONE
                WHEN 68 GO AVANT-ALL-END
           END-EVALUATE.
           IF INDICE-ZONE < 44
              GO AVANT-ALL.
       AVANT-ALL-END.
           IF INDICE-ZONE < 4
              MOVE 4 TO INDICE-ZONE.

       AVANT-PARAM.
           PERFORM LIN-COL.
           MOVE TAUX-SS(IDX-1, IDX) TO HE-ZT.
           ACCEPT HE-ZT LINE LIN-IDX POSITION COL-IDX SIZE 7
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-ZT REPLACING ALL "." BY ",".
           MOVE HE-ZT TO TAUX-SS(IDX-1, IDX).

       LIN-COL.
           COMPUTE IDX-2 = INDICE-ZONE - 4.
           DIVIDE IDX-2 BY 4 GIVING IDX REMAINDER IDX-3.
           ADD 1 TO IDX IDX-2.
           COMPUTE IDX-1 = IDX-2 - (4 * (IDX - 1)).
           COMPUTE LIN-IDX = IDX + 8.
           COMPUTE COL-IDX = IDX-1 * 15 + 10.

       AVANT-2-ALL.
           COMPUTE IDX = INDICE-ZONE.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE 25 TO COL-IDX.
           MOVE SS-FACT(IDX) TO HE-Z2Z2.
           ACCEPT HE-Z2Z2 LINE LIN-IDX POSITION COL-IDX SIZE 5
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z2Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z2Z2 TO SS-FACT(IDX).
           
       AVANT-2-YN.
           COMPUTE IDX = INDICE-ZONE - 10.
           COMPUTE LIN-IDX = IDX + 8.
           ACCEPT SS-DEDUCTIBLE(IDX)
             LINE LIN-IDX POSITION 60 SIZE 1
             CONTROL "UPPER"
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3-PLAF.
           COMPUTE IDX = INDICE-ZONE.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE 30 TO COL-IDX.
           MOVE SS-PLAFOND(IDX) TO HE-Z5Z2.
           ACCEPT HE-Z5Z2 LINE LIN-IDX POSITION COL-IDX SIZE 8
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z5Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z5Z2 TO SS-PLAFOND(IDX).

       AVANT-3-PLAF-100.
           COMPUTE IDX = INDICE-ZONE - 10.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE 40 TO COL-IDX.
           MOVE SS-PLAF-I100(IDX) TO HE-Z5Z5.
           ACCEPT HE-Z5Z5 LINE LIN-IDX POSITION COL-IDX SIZE 11
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z5Z5 REPLACING ALL "." BY ",".
           MOVE HE-Z5Z5 TO SS-PLAF-I100(IDX).

       AVANT-3-ABAT.
           COMPUTE IDX = INDICE-ZONE - 20.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE 60 TO COL-IDX.
           MOVE SS-ABAT(IDX) TO HE-Z5Z2.
           ACCEPT HE-Z5Z2 LINE LIN-IDX POSITION COL-IDX SIZE 8
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE HE-Z5Z2 TO SS-ABAT(IDX).

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 MOVE SS-REGIME TO REGIME-NUMBER 
                    PERFORM NEXT-REGIME
                    MOVE REGIME-NUMBER TO SS-REGIME
           WHEN   5 CALL "1-REGIM" USING LINK-V REGIME-RECORD
                    MOVE LNK-VAL TO SS-REGIME REGIME-NUMBER 
                    PERFORM AFFICHAGE-ECRAN 
           WHEN   2 CALL "2-REGIME" USING LINK-V REGIME-RECORD
                    CANCEL "2-REGIME"
                    PERFORM AFFICHAGE-ECRAN 
           WHEN  13 MOVE SS-REGIME TO REGIME-NUMBER 
                    PERFORM NEXT-REGIME
           END-EVALUATE.
           PERFORM TAUXSS-RECENT.
           PERFORM AFFICHAGE-DETAIL.
           IF SS-REGIME = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-1-2.
           EVALUATE EXC-KEY
           WHEN  65 PERFORM TAUXSS-PRECEDENT
           WHEN  66 PERFORM TAUXSS-SUIVANT
           WHEN   8 PERFORM APRES-DEC     
           WHEN OTHER
                    READ TAUXSS INVALID 
                    MOVE "AA" TO LNK-AREA
                    MOVE 13 TO LNK-NUM
                    PERFORM DISPLAY-MESSAGE
                    END-READ.
                    IF SS-ANNEE < 2000
                       MOVE 2000 TO SS-ANNEE
                       MOVE 1 TO INPUT-ERROR
                    END-IF
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-3.
           EVALUATE EXC-KEY
           WHEN  65 PERFORM TAUXSS-PRECEDENT
           WHEN  66 PERFORM TAUXSS-SUIVANT
           WHEN   8 PERFORM APRES-DEC     
           WHEN OTHER
                    IF SS-MOIS < 1
                       MOVE 1 TO SS-MOIS
                       MOVE 1 TO INPUT-ERROR
                    END-IF
                    IF SS-MOIS > 12
                       MOVE 12 TO SS-MOIS
                       MOVE 1 TO INPUT-ERROR
                    END-IF.
           IF INPUT-ERROR = 0
              READ TAUXSS INVALID 
              MOVE "AA" TO LNK-AREA
              MOVE 13 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              END-READ.
           MOVE SS-ANNEE TO INDEX-ANNEE.
           MOVE SS-MOIS  TO INDEX-MOIS.
           CALL "6-INDEX" USING LINK-V INDEX-RECORD NUL-KEY.
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-ALL.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM APRES-DEC
            WHEN OTHER PERFORM DIS-E1-ALL
           END-EVALUATE.

       APRES-2-ALL.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM APRES-DEC
            WHEN OTHER PERFORM DIS-E2-ALL
           END-EVALUATE.

       APRES-2-YN.
           IF  SS-DEDUCTIBLE(IDX) NOT = " "
           AND SS-DEDUCTIBLE(IDX) NOT = "N"
               MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-3-PLAF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM APRES-DEC
            WHEN 10 IF SS-PLAF-I100(IDX) = 0
                       COMPUTE SS-PLAF-I100(IDX) = 
                       SS-PLAFOND(IDX) / INDEX-FACT
                    ELSE
                       MOVE 0 TO SS-PLAF-I100(IDX) 
                    END-IF
                    PERFORM DIS-E3-ALL
            WHEN OTHER PERFORM DIS-E3-ALL
           END-EVALUATE.

       APRES-3-PLAF-100.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM APRES-DEC
            WHEN 10 COMPUTE SS-PLAF-I100(IDX) = 
                    SS-PLAFOND(IDX) / INDEX-FACT
                    PERFORM DIS-E3-ALL
            WHEN OTHER PERFORM DIS-E3-ALL
           END-EVALUATE.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO SS-TIME
                    MOVE LNK-USER TO SS-USER
                    WRITE SS-RECORD INVALID REWRITE SS-RECORD END-WRITE
                    MOVE 1 TO ECRAN-IDX
                    PERFORM AFFICHAGE-ECRAN
                    PERFORM AFFICHAGE-DETAIL
                    MOVE 1 TO DECISION  
            WHEN 8  DELETE TAUXSS INVALID CONTINUE END-DELETE
                    PERFORM TAUXSS-PRECEDENT
                    MOVE 1 TO ECRAN-IDX
                    PERFORM AFFICHAGE-ECRAN
                    PERFORM AFFICHAGE-DETAIL
                    MOVE 1 TO DECISION  
            WHEN 52 COMPUTE DECISION = CHOIX-MAX(ECRAN-IDX) + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.
           
       NEXT-REGIME.
           MOVE 0 TO NOT-FOUND.
           CALL "6-REGIME" USING LINK-V REGIME-RECORD EXC-KEY.

       TAUXSS-RECENT.
           INITIALIZE NOT-FOUND.
           MOVE 9999 TO SS-ANNEE.
           PERFORM TAUXSS-PRECEDENT.

       TAUXSS-PRECEDENT.
           MOVE 0 TO NOT-FOUND .
           START TAUXSS KEY < SS-KEY INVALID KEY
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ TAUXSS PREVIOUS AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           PERFORM TEST-REG.

       TAUXSS-SUIVANT.
           MOVE 0 TO NOT-FOUND .
           START TAUXSS KEY > SS-KEY INVALID KEY
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ TAUXSS PREVIOUS AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           PERFORM TEST-REG.

       TEST-REG.
           IF REGIME-NUMBER NOT = SS-REGIME
              MOVE 1 TO NOT-FOUND.
           IF NOT-FOUND = 1 
              INITIALIZE SS-RECORD
           MOVE REGIME-NUMBER TO SS-REGIME.

       DIS-E1-1.
           DISPLAY SS-REGIME
           LINE     IP-LINE (1, 1) POSITION IP-COL  (1, 1).
           MOVE SS-REGIME TO REGIME-NUMBER.
           CALL "6-REGIME" USING LINK-V REGIME-RECORD FAKE-KEY.
           DISPLAY REGIME-NOM
           LINE IP-LINE (1, 1) POSITION 35 SIZE 40.

       DIS-E1-2.
           DISPLAY SS-ANNEE LINE IP-LINE(1, 2) POSITION IP-COL(1, 2).

       DIS-E1-3.
           MOVE SS-MOIS TO HE-Z2.
           DISPLAY HE-Z2 LINE IP-LINE(1, 3) POSITION IP-COL(1, 3).

       DIS-E1-ALL.
           PERFORM LIN-COL.
           MOVE TAUX-SS(IDX-1, IDX) TO HE-ZT.
           DISPLAY HE-ZT LINE LIN-IDX POSITION COL-IDX.


       DIS-E2-ALL.
           COMPUTE IDX = INDICE-ZONE.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE 25 TO COL-IDX.
           MOVE SS-FACT(IDX) TO HE-Z2Z2.
           DISPLAY HE-Z2Z2 LINE LIN-IDX POSITION COL-IDX.
           COMPUTE SH-00 = INDEX-FACT * SALMIN-100 + ,005.
           COMPUTE SH-00 = SH-00 * SS-FACT(IDX).
           MOVE SH-00 TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE LIN-IDX POSITION 35.
           DISPLAY SS-DEDUCTIBLE(IDX) LINE LIN-IDX POSITION 60.

       DIS-E3-ALL.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE SS-PLAFOND(IDX) TO HE-Z5Z2.
           DISPLAY HE-Z5Z2 LINE LIN-IDX POSITION 30.
           MOVE SS-PLAF-I100(IDX) TO HE-Z5Z5.
           DISPLAY HE-Z5Z5 LINE LIN-IDX POSITION 40.
           MOVE SS-ABAT(IDX) TO HE-Z5Z2.
           DISPLAY HE-Z5Z2 LINE LIN-IDX POSITION 60.


       AFFICHAGE-ECRAN.
           MOVE ECR-S(ECRAN-IDX) TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           IF ECRAN-IDX > 1
              PERFORM DIS-E1-1 THRU DIS-E1-3
           END-IF.

       AFFICHAGE-DETAIL.
           MOVE INDICE-ZONE TO DECISION.
           EVALUATE ECRAN-IDX
               WHEN 1 PERFORM ECRAN-1 VARYING INDICE-ZONE FROM
               1 BY 1 UNTIL INDICE-ZONE = CHOIX-MAX(ECRAN-IDX)
               WHEN 2 PERFORM DIS-E2-ALL VARYING INDICE-ZONE FROM 
               1 BY 1 UNTIL INDICE-ZONE > 11
               WHEN 3 PERFORM DIS-E3-ALL VARYING IDX FROM 
               1 BY 1 UNTIL IDX > 10
           END-EVALUATE.
           MOVE DECISION TO INDICE-ZONE.

       ECRAN-1.
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM DIS-E1-1 
               WHEN  2 PERFORM DIS-E1-2 
               WHEN  3 PERFORM DIS-E1-3 
               WHEN OTHER PERFORM DIS-E1-ALL
           END-EVALUATE.

       END-PROGRAM.
           CLOSE TAUXSS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
