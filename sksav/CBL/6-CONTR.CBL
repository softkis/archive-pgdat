      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-CONTR MODULE GENERAL LECTURE CONTRAT      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-CONTR.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CONTRAT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CONTRAT.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  LNK-TEST PIC 9(6).
       01  ACTION   PIC X.
       01  ESC-KEY               PIC 9(4) COMP-1.

       01  TODAY.
           02 TODAY-TIME.
              03 TODAY-DATE-A    PIC 9(8).
              03 TODAY-TEMPS-A   PIC 9(4).
           02 TODAY-FILLER.
              03 TODAY-FILL   PIC 9999.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "CONTRAT.LNK".
           COPY "REGISTRE.REC".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD REG-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CONTRAT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-REG.
       
           IF NOT-OPEN = 0
              OPEN I-O CONTRAT
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CON-RECORD.
           MOVE FR-KEY TO CON-FIRME CON-FIRME-F.
           MOVE REG-PERSON TO CON-PERSON CON-PERSON-F.
           MOVE 0 TO LNK-VAL.

           IF EXC-KEY = 99 
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-TIME TO CON-TIME
              MOVE LNK-USER TO CON-USER
              MOVE MENU-PROG-NAME TO CON-MODULE
              IF CON-NOTIFE-A = 0
                 MOVE TODAY-DATE-A TO CON-DATE-NOTIFE
              END-IF
              CALL "0-TDATE" USING CON-DATE-DEBUT LNK-TEST
              IF LNK-TEST > 0
                 MOVE 2 TO LNK-NUM 
                 MOVE "AA" TO LNK-AREA
                 PERFORM DISPLAY-MESSAGE
                 PERFORM AA
                 MOVE 9998 TO LNK-NUM 
                 MOVE "AA" TO LNK-AREA
                 PERFORM DISPLAY-MESSAGE
                 DISPLAY "MODULE = " LINE 25 POSITION 50
                 DISPLAY MENU-PROG-NAME LINE 25 POSITION 65
                 PERFORM AA
                 EXIT PROGRAM
              END-IF
              IF LNK-SQL = "Y" 
                 CALL "9-CONTR" USING LINK-V CON-RECORD EXC-KEY 
              END-IF
              WRITE CON-RECORD INVALID 
              REWRITE CON-RECORD END-WRITE   
              MOVE CON-RECORD TO LINK-RECORD
              EXIT PROGRAM
           END-IF.
           
           EVALUATE EXC-KEY 
           WHEN 65 IF CON-DEBUT-A = 0
                      MOVE LNK-ANNEE TO CON-DEBUT-A
                      MOVE LNK-MOIS  TO CON-DEBUT-M
                      MOVE 32        TO CON-DEBUT-J
                   END-IF
               PERFORM START-1
               READ CONTRAT PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ CONTRAT NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 98 DELETE CONTRAT INVALID CONTINUE END-DELETE
                  IF LNK-SQL = "Y" 
                     CALL "9-CONTR" USING LINK-V CON-RECORD EXC-KEY 
                  END-IF
                  EXIT PROGRAM
           WHEN OTHER READ CONTRAT NO LOCK INVALID INITIALIZE 
                CON-REC-DET END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY     NOT = CON-FIRME 
           OR REG-PERSON NOT = CON-PERSON
              GO EXIT-1
           END-IF.
           MOVE CON-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START CONTRAT KEY < CON-KEY INVALID GO EXIT-1.
       START-2.
           START CONTRAT KEY > CON-KEY INVALID GO EXIT-1.


           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
