      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-VD AFFICHAGE PERSONNES AVEC UN VIREMENT   �
      *  � ET UNE VALEUR DONN륟                                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-VD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "VIREMENT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "VIREMENT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  CHOIX                 PIC X(10) VALUE SPACES.
       01  COMPTEUR              PIC 9999 VALUE 0.

       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIR          PIC 999.
       
       01  MINIMAX.
           02 TOT-MINI           PIC 9(8)V99 VALUE 1.
           02 TOT-MAX            PIC 9(8)V99 VALUE 0.
       01  TOTAL-VALEUR.
           02 TOT-TOTAL          PIC 9(8)V99 COMP-3.
           02 TOT-PERS           PIC 9(4) COMP-3.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
           02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.
           02 HE-Z8Z2 PIC Z(8),ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON VIREMENT.


       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-VD .

           MOVE LNK-SUFFIX TO ANNEE-VIR.
           OPEN INPUT VIREMENT.
           INITIALIZE TOTAL-VALEUR.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

      *    EVALUATE INDICE-ZONE
      *    WHEN 1  MOVE 0029000000 TO EXC-KFR(1)
      *            MOVE 0000000065 TO EXC-KFR(13)
      *            MOVE 6600000000 TO EXC-KFR(14).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1
           WHEN  2 PERFORM AVANT-2.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           MOVE TOT-MINI TO HE-Z8Z2.
      *    MOVE 0 TO IDX-3.
      *    INSPECT HE-Z8Z2 TALLYING IDX-3 FOR ALL " ".
           MOVE 5 TO IDX-3.
           ACCEPT HE-Z8Z2
           LINE 3 POSITION 25 SIZE 11
           TAB UPDATE NO BEEP CURSOR  IDX-3 
           ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z8Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z8Z2 TO TOT-MINI.
       AVANT-2.
           MOVE TOT-MAX TO HE-Z8Z2.
      *    MOVE 0 TO IDX-3.
      *    INSPECT HE-Z8Z2 TALLYING IDX-3 FOR ALL " ".
      *    ADD 1 TO IDX-3.
           MOVE 5 TO IDX-3.
           ACCEPT HE-Z8Z2
           LINE 3 POSITION 55 SIZE 11
           TAB UPDATE NO BEEP CURSOR  IDX-3 
           ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z8Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z8Z2 TO TOT-MAX.
           
      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE TOT-MINI TO HE-Z8Z2.
           DISPLAY HE-Z8Z2 LINE  3 POSITION 25.

       APRES-2.
           IF TOT-MINI > TOT-MAX
              MOVE TOT-MINI TO TOT-MAX
              MOVE 1 TO INPUT-ERROR.
           MOVE TOT-MAX TO HE-Z8Z2.
           DISPLAY HE-Z8Z2 LINE  3 POSITION 55.
           IF EXC-KEY NOT = 27
              PERFORM AFFICHE-DEBUT THRU AFFICHE-END.
           IF EXC-KEY = 13 MOVE 52 TO EXC-KEY.
       
       DIS-HE-01.
           MOVE TOT-MINI TO HE-Z8Z2.
           DISPLAY HE-Z8Z2 LINE  3 POSITION 25.
       DIS-HE-02.
           MOVE TOT-MAX TO HE-Z8Z2.
           DISPLAY HE-Z8Z2 LINE  3 POSITION 55.
       DIS-HE-END.
           EXIT.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE NOT-FOUND COMPTEUR CHOIX TOTAL-VALEUR.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO NOT-FOUND.
           INITIALIZE VIR-RECORD REG-RECORD.
           PERFORM READ-VIR THRU READ-VIR-END.
       AFFICHE-END.
           EXIT.

       READ-VIR.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY.
           IF REG-PERSON  = 0
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              GO READ-VIR-END.
           IF LNK-COMPETENCE < REG-COMPETENCE
              GO READ-VIR.
           PERFORM RECHERCHE-V.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              MOVE 4 TO LIN-IDX
           END-IF.
           IF EXC-KEY = 52 GO READ-VIR-END.
           GO READ-VIR.
       READ-VIR-END.
           PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21.
           PERFORM DIS-TOT-LIGNE.


      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           ADD 1 TO COMPTEUR.
           MOVE REG-PERSON TO HE-Z6.
           DISPLAY HE-Z6  LINE LIN-IDX POSITION 01 LOW.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE LIN-IDX POSITION 8 SIZE 30.
           ADD 1 TO TOT-PERS.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000030000 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052000000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           PERFORM DIS-TOT-LIGNE.
           ACCEPT CHOIX 
           LINE 23 POSITION 30 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY = 66
              MOVE 13 TO EXC-KEY.
           IF EXC-KEY = 52 GO INTERRUPT-END.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.

       INTERRUPT-END.
           EXIT.

       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS 
             WHEN 58 PERFORM ADD-MOIS 
           END-EVALUATE.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-DETAIL.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0
              MOVE 1 TO LNK-MOIS
           END-IF.
       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
              MOVE 12 TO LNK-MOIS
           END-IF.


       AFFICHAGE-ECRAN.
           MOVE 2525 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE VIREMENT.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

       DIS-HIS-LIGNE.
           PERFORM DIS-DET-LIGNE.
           DISPLAY VIR-BANQUE-C LINE LIN-IDX POSITION 37 LOW.
           DISPLAY VIR-COMPTE-C LINE LIN-IDX POSITION 45 SIZE 30.
           MOVE VIR-A-PAYER TO HE-Z8Z2.
           INSPECT HE-Z8Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z8Z2   LINE  LIN-IDX POSITION 70.
           ADD VIR-A-PAYER TO TOT-TOTAL.

       DIS-TOT-LIGNE.
           MOVE 23 TO LIN-IDX.
           MOVE TOT-PERS TO HE-Z4.
           DISPLAY HE-Z4   LINE  LIN-IDX POSITION 3.
           MOVE TOT-TOTAL TO HE-Z8Z2.
           INSPECT HE-Z8Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z8Z2 LINE  LIN-IDX POSITION 70.

       RECHERCHE-V.
           INITIALIZE VIR-RECORD.
           MOVE FR-KEY     TO VIR-FIRME-N.
           MOVE REG-PERSON TO VIR-PERSON-N.
           MOVE LNK-MOIS   TO VIR-MOIS-N.
           START VIREMENT KEY >= VIR-KEY-PERSON INVALID CONTINUE
               NOT INVALID PERFORM READ-V THRU READ-V-END.

       READ-V.
           READ VIREMENT NEXT NO LOCK AT END
               GO READ-V-END
           END-READ.
           IF FR-KEY     NOT = VIR-FIRME
           OR REG-PERSON NOT = VIR-PERSON
           OR LNK-MOIS   NOT = VIR-MOIS 
              GO READ-V-END
           END-IF.
           IF VIR-A-PAYER < TOT-MINI
           OR VIR-A-PAYER > TOT-MAX
              GO READ-V
           END-IF.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              MOVE 4 TO LIN-IDX
           END-IF.
           IF EXC-KEY = 52 GO READ-V-END.
           PERFORM DIS-HIS-LIGNE.
           GO READ-V.
       READ-V-END.
           EXIT.


