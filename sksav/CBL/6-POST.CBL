      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-POST MODULE GENERAL LECTURE CODEPOST      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-POST.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODEPOST.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CODEPOST.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN  PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       01  LINK-RECORD.
           02  LINK-KEY.
               03  LINK-PAYS                 PIC XXX.
               03  LINK-CODE                 PIC 9(8) COMP-6.

           02  LINK-REC-DET.
               03  LINK-RUE                  PIC X(40).
               03  LINK-LOCALITE             PIC X(40).
                              
       01  EXC-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CDPOST .
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-CDPOST.
       
           IF NOT-OPEN = 0
              OPEN INPUT CDPOST
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CP-RECORD.
           IF CP-PAYS = SPACES
              MOVE LNK-TEXT TO CP-PAYS 
           END-IF.
           
           EVALUATE EXC-KEY 
              WHEN 65 PERFORM START-1
                  READ CDPOST PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
                  IF CP-PAYS NOT = LNK-TEXT
                     GO EXIT-1
                  END-IF
                  GO EXIT-2
               WHEN 66 PERFORM START-2
                   READ CDPOST NEXT NO LOCK AT END GO EXIT-1 END-READ
                   IF CP-PAYS NOT = LNK-TEXT
                      GO EXIT-1
                   END-IF
               GO EXIT-2
               WHEN OTHER GO EXIT-3
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE CP-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-3.
           READ CDPOST NO LOCK INVALID GO EXIT-1
                NOT  INVALID GO EXIT-2.
       START-1.
           START CDPOST KEY < CP-KEY INVALID GO EXIT-1.
       START-2.
           START CDPOST KEY > CP-KEY INVALID GO EXIT-1.
