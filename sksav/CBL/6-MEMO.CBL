      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-MEMO MODULE GENERAL LECTURE MEMO          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-MEMO.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MEMO.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "MEMO.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  action   PIC X.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "MEMO.LNK".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V REG-RECORD LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MEMO.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-REG.
       
           IF NOT-OPEN = 0
              OPEN I-O MEMO
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO MEM-RECORD.
           MOVE FR-KEY     TO MEM-FIRME.
           MOVE REG-PERSON TO MEM-PERSON.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ MEMO PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
           WHEN 66 PERFORM START-2
               READ MEMO NEXT NO LOCK AT END GO EXIT-1 END-READ
           WHEN 0 PERFORM START-3
               READ MEMO PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
           WHEN OTHER 
                IF MEM-ANNEE = 0
                   MOVE LNK-ANNEE TO MEM-ANNEE
                   MOVE LNK-MOIS  TO MEM-MOIS
                END-IF
           READ MEMO NO LOCK INVALID INITIALIZE MEM-REC-DET MEM-DATE 
           END-READ
           END-EVALUATE.
           GO EXIT-2.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY     NOT = MEM-FIRME 
           OR REG-PERSON NOT = MEM-PERSON
              GO EXIT-1
           END-IF.
           MOVE MEM-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START MEMO KEY < MEM-KEY INVALID GO EXIT-1.
       START-2.
           START MEMO KEY > MEM-KEY INVALID GO EXIT-1.
       START-3.
           START MEMO KEY <= MEM-KEY INVALID GO EXIT-1.


