      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-FIPARC FICHE DE PAIE ARCHIVEE             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *   FIP-POSTE 0 = TEXTE
      *             1 = POSTE af
      *             2 = CS 1+2 = POSTE af 
      *             3 = CUMUL  

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-FIPARC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "TRIPR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 10.
       01  PRECISION             PIC 9 VALUE 0.
       01  LINES-CS              PIC 99 VALUE 19.
       01  MODULE-I              PIC XXXXX VALUE "FIP".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "FICHE.REC".
           COPY "LIVRE.REC".
           COPY "LIVRE.CUM".
           COPY "CONTRAT.REC".
           COPY "CONGE.REC".
           COPY "VIREMENT.REC".
           COPY "TAUXSS.REC".
           COPY "METIER.REC".
           COPY "CODARCH.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
SU         COPY "CODTXT.REC".
           COPY "JOURS.REC".
           COPY "MESSAGE.REC".
           COPY "PARMOD.REC".
           COPY "BAREME.REC".
           COPY "BARDEF.REC".
           COPY "POINTS.REC".
           COPY "BANQP.REC".

       01  MOIS            PIC 99 VALUE 0.
       01  MOIS-ACT         PIC 99 VALUE 0.
       01  LIN-NUM         PIC 99 VALUE 0.
       01  COUNTER         PIC 9999 VALUE 0.
       01  LAST-CS         PIC 9999 VALUE 0.
       01  POSTE           PIC 9(8) VALUE 0.
       01  POSTE-UNITE     PIC 9(8)V99 VALUE 0.
       01  POSTE-TOTAL     PIC 9(8)V99 VALUE 0.
       01  SOLDES.
           03 SOLDE-CG OCCURS 6.
              04 SOLDE     PIC S9999V99 OCCURS 5.

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  ARCHIVE               PIC 99 VALUE 1.
       01  CHOIX                 PIC 99 VALUE 0.
       01  MOIS-DEBUT      PIC 99 VALUE 1.
       01  MOIS-FIN        PIC 99 VALUE 12.

       01  SAL-IDX.
           02  SAL-ACT-IDX       PIC 9(8)V9(4) OCCURS 2.
       01  SAL-ACT               PIC 9(8)V9(4).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02  SAL-ACT-A         PIC 9(8).
           02  SAL-ACT-B         PIC 9(4).

           COPY "FIC.LNK".

       01  ECR-DISPLAY.
           02 HE-Z2   PIC ZZ.
           02 HE-Z4   PIC Z(4).
           02 HE-Z5   PIC Z(5).
           02 HE-Z6   PIC Z(6).
           02 HE-Z8   PIC Z(8).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-S4Z2 PIC -Z(4),ZZ  BLANK WHEN ZERO.
           02 HE-Z4Z4 PIC Z(4),ZZZZ BLANK WHEN ZERO.
           02 HE-Z6Z2 PIC Z(6),ZZ   BLANK WHEN ZERO.
           02 HE-Z7Z2 PIC Z(7),ZZ   BLANK WHEN ZERO.
           02 HE-Z8Z2 PIC Z(8),ZZ   BLANK WHEN ZERO.
           02 HE-DATE .
              03 HE-JJ  PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM  PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA  PIC ZZZZ.
              03 HE-HH  PIC ZZZZZ.
              03 FILLER PIC X VALUE ":".
              03 HE-MI  PIC ZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-FIPARC.
       

           MOVE LNK-MOIS TO SAVE-MOIS MOIS-DEBUT MOIS-FIN.

           CALL "0-TODAY" USING TODAY.
           IF MENU-PROG-NUMBER > 0
              MOVE MENU-PROG-NUMBER TO LINES-CS
           END-IF.
           IF MENU-EXTENSION-1 NOT = SPACES
              MOVE MENU-EXTENSION-1 TO MODULE-I
           END-IF.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7 THRU 8 
                      MOVE 0000000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN  9    MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 10    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-SORT 
           WHEN 10 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  7 PERFORM APRES-7 
           WHEN  8 PERFORM APRES-8 
           WHEN  9 PERFORM APRES-SORT
           WHEN 10 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT ARCHIVE
             LINE 12 POSITION 32 SIZE 2 
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-7.
           ACCEPT MOIS-DEBUT
             LINE 14 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
                     PERFORM APRES-7
             WHEN 66 ADD      1 TO   MOIS-DEBUT
                     PERFORM APRES-7
             END-EVALUATE.

       AVANT-8.
           ACCEPT MOIS-FIN
             LINE 15 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
                     PERFORM APRES-8
             WHEN 66 ADD      1 TO   MOIS-FIN
                     PERFORM APRES-8
             END-EVALUATE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-7.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-07.

       APRES-8.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 99999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           MOVE SAVE-MOIS TO LNK-MOIS.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM I THRU II VARYING MOIS-ACT FROM MOIS-DEBUT BY 1 
           UNTIL MOIS-ACT > MOIS-FIN.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.


       I.
           MOVE MOIS-ACT TO LNK-MOIS.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO II
           END-IF.
           IF STATUT > 0 AND NOT = CAR-STATUT  
              GO II
           END-IF.
           IF COUT > 0 AND NOT = CAR-COUT
              GO II
           END-IF.
           INITIALIZE L-RECORD LCUM-RECORD LNK-SUITE TRANSFERT.
           MOVE ARCHIVE TO LNK-NUM L-ARCHIVE.
           CALL "6-ARCHL" USING LINK-V L-RECORD REG-RECORD FAKE-KEY.
           IF L-PERSON = 0
              GO II
           END-IF.
           ADD 1 TO LNK-MOIS.
           CALL "4-LPCUM" USING LINK-V REG-RECORD LCUM-RECORD.
           SUBTRACT 1 FROM LNK-MOIS.
           MOVE PR-LANGUAGE TO FORM-LANGUE.
           PERFORM ENTETE.
           PERFORM FILL-CODPAIE.
           PERFORM LIVRES.
           PERFORM VIREMENTS.
           MOVE 0 TO LNK-VAL.
           MOVE REG-PERSON TO LNK-PERSON.
           MOVE "          Archiv" TO L-T(2).
           MOVE L-ST-ANNEE TO HE-AA.
           MOVE L-ST-MOIS  TO HE-MM.
           MOVE L-ST-JOUR  TO HE-JJ.
           MOVE L-ST-HEURE TO HE-HH.
           MOVE L-ST-MIN   TO HE-MI.
           MOVE 30 TO IDX.
           STRING HE-DATE DELIMITED BY SIZE INTO L-T(2) WITH POINTER IDX.
           CALL MODULE-I USING LINK-V TRANSFERT SS-RECORD.
           ADD 1 TO COUNTER.
       II.
           EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD FICHE-RECORD COUT-RECORD SS-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD NUL-KEY.
           MOVE CAR-REGIME TO SS-REGIME.
           CALL "6-TSS" USING LINK-V SS-RECORD.
           MOVE CAR-COUT TO COUT-NUMBER.
           PERFORM NEXT-COUT.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       ENTETE. 
           PERFORM ADRESSE.
           PERFORM FICHE.
           PERFORM METIER.
           PERFORM MATR.
           PERFORM SALAIRE.
           PERFORM READ-CONTRAT.
           PERFORM DATES.
           PERFORM JRS.

       METIER.
           MOVE CAR-METIER TO MET-CODE.
           MOVE PR-LANGUAGE TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO METIER.
           MOVE CAR-POSITION TO POSITION-TEXT.
           MOVE CAR-COUT TO CENTRE-NO.
           MOVE COUT-NOM TO CENTRE-TEXT.

       SALAIRE.
           MOVE 0 TO SAL-ACT SAVE-KEY.
           IF CAR-POURCENT = 0
              MOVE 100 TO CAR-POURCENT 
           END-IF.
           IF CAR-BAREME NOT = SPACES
           AND CAR-ECHELON > 0
             INITIALIZE BAR-RECORD
             MOVE CAR-BAREME TO BAR-BAREME BDF-CODE
             MOVE CAR-GRADE  TO BAR-GRADE
             MOVE LNK-ANNEE  TO BAR-ANNEE
             MOVE LNK-MOIS   TO BAR-MOIS
             CALL "6-BAREME" USING LINK-V BAR-RECORD SAVE-KEY
             CALL "6-BARDEF" USING LINK-V BDF-RECORD SAVE-KEY
             MOVE CAR-ECHELON     TO ECHELON-TEXT
             MOVE BAR-DESCRIPTION TO GRADE-TEXT
             IF BAR-POINT > 0
                INITIALIZE PTS-RECORD
                MOVE LNK-ANNEE TO PTS-ANNEE
                MOVE LNK-MOIS  TO PTS-MOIS
                CALL "6-POINT" USING LINK-V PTS-RECORD
                COMPUTE SAL-ACT = PTS-I100(BAR-POINT)
                                * MOIS-IDX(LNK-MOIS) + ,00009
                MOVE SAL-ACT TO POINT-VAL
                MOVE BAR-POINTS(CAR-ECHELON) TO POINTS-NUM
                COMPUTE SAL-ACT = BAR-POINTS(CAR-ECHELON) 
                                * PTS-I100(BAR-POINT)
                                * MOIS-IDX(LNK-MOIS) + ,00009
             ELSE
                COMPUTE SAL-ACT = BAR-ECHELON-I100(CAR-ECHELON) 
                                * MOIS-IDX(LNK-MOIS) + ,00009
             END-IF
           ELSE
              IF CAR-INDEXE NOT = "N"
                 IF CAR-SAL-100 = 0
                    COMPUTE SAL-ACT = MOIS-SALMIN(LNK-MOIS)
                    * MOIS-IDX(LNK-MOIS) + ,00009
                 ELSE
                    COMPUTE SAL-ACT = CAR-SAL-100 * MOIS-IDX(LNK-MOIS)
                    + ,00009
                 END-IF
              ELSE
                 COMPUTE SAL-ACT = CAR-SAL-ACT
              END-IF
           END-IF.
           MOVE SAL-ACT TO BAS-SAL.
           COMPUTE INDEX-COUT = MOIS-IDX(LNK-MOIS) * 100.
           MOVE " INDEX =" TO INDEX-TEXT.


       JRS.
           INITIALIZE JRS-RECORD.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE LNK-MOIS   TO JRS-MOIS.
           MOVE REG-PERSON TO JRS-PERSON.
           MOVE 66 TO SAVE-KEY.
           PERFORM NEXT-JOUR THRU NEXT-JOUR-END.

       NEXT-JOUR.
           MOVE 66 TO SAVE-KEY.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD SAVE-KEY
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
           OR JRS-OCCUPATION > 20
              GO NEXT-JOUR-END
           END-IF.
           IF JRS-COMPLEMENT NOT = 0
           OR JRS-OCCUPATION     = 0
           OR JRS-POSTE      NOT = 0
           OR JRS-EXTENSION  NOT = SPACES
              GO NEXT-JOUR.
           IF  JRS-OCCUPATION > 11
           AND JRS-OCCUPATION < 20
              GO NEXT-JOUR.
           IF JRS-OCCUPATION = 20
              PERFORM FILL-CG VARYING IDX FROM 1 BY 1 UNTIL IDX > 31
           ELSE
              PERFORM FILL-MAL VARYING IDX FROM 1 BY 1 UNTIL IDX > 31
           END-IF.
           GO NEXT-JOUR.
       NEXT-JOUR-END.
           EXIT.

       FILL-MAL.
           IF JRS-HRS(IDX) > 0 
              MOVE IDX TO HE-Z2
              MOVE HE-Z2 TO HRS-MAL(IDX)
           END-IF.  
           IF JRS-HRS(IDX) < 0 
              MOVE "--" TO HRS-MAL(IDX)
           END-IF.

       FILL-CG.
           IF JRS-HRS(IDX) > 0 
              MOVE IDX TO HRS-CG(IDX)
           END-IF.  


       FILL-CODPAIE.
           INITIALIZE CSA-RECORD CODES-SALAIRE.
           MOVE FR-KEY     TO CSA-FIRME.
           MOVE REG-PERSON TO CSA-PERSON.
           MOVE LNK-MOIS   TO CSA-MOIS.
           MOVE ARCHIVE    TO CSA-ARCHIV LNK-NUM.
           MOVE 0 TO LIN-NUM.
           MOVE 299 TO LAST-CS.
           PERFORM READ-CODPAIE THRU READ-CODPAIE-END.

       READ-CODPAIE.
           MOVE ARCHIVE    TO LNK-NUM.
           CALL "6-CODARC" USING LINK-V CSA-RECORD REG-RECORD NX-KEY.
           IF FR-KEY     NOT = CSA-FIRME
           OR REG-PERSON NOT = CSA-PERSON
           OR LNK-MOIS   NOT = CSA-MOIS 
           OR ARCHIVE    NOT = CSA-ARCHIV
           OR CSA-SUITE  > 0
              GO READ-CODPAIE-END 
           END-IF.
           MOVE CSA-CODE TO CD-NUMBER.
           IF CD-NUMBER > 0
              CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY
SU            CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
           IF  CS-IMPRESSION(1) = 0 
           AND CS-IMPRESSION(2) = 0 
           AND CS-IMPRESSION(3) = 0 
           AND CS-IMPRESSION(4) = 0 
           AND CS-IMPRESSION(5) = 0 
           AND CS-IMPRESSION(6) = 0 
              GO READ-CODPAIE
           END-IF.
           IF LIN-NUM > LINES-CS
              MOVE 0 TO LNK-VAL
              CALL MODULE-I USING LINK-V TRANSFERT SS-RECORD
              INITIALIZE TRANSFERT
              PERFORM ENTETE
              MOVE 0 TO LIN-NUM
           END-IF.
           IF MENU-BATCH = 1
           AND LIN-NUM > 0
              IF CSA-CODE > 299 AND < 400 AND > LAST-CS
                 ADD 1 TO LIN-NUM
                 MOVE 399 TO LAST-CS
              END-IF
              IF CSA-CODE > 399 AND < 411 AND > LAST-CS
                 ADD 1 TO LIN-NUM
                 MOVE 410 TO LAST-CS
              END-IF
              IF CSA-CODE > 410 AND < 500 AND > LAST-CS
                 ADD 1 TO LIN-NUM
                 MOVE 499 TO LAST-CS
              END-IF
              IF CSA-CODE > 499 AND < 600 AND > LAST-CS
                 ADD 1 TO LIN-NUM
                 MOVE 599 TO LAST-CS
              END-IF
              IF CSA-CODE > 599 AND < 700 AND > LAST-CS
                 ADD 1 TO LIN-NUM
                 MOVE 699 TO LAST-CS
              END-IF
              IF CSA-CODE > 699 AND < 1000 AND > LAST-CS
                 ADD 1 TO LIN-NUM
                 MOVE 999 TO LAST-CS
              END-IF
              IF CSA-CODE > 999 AND < 2000 AND > LAST-CS
                 ADD 1 TO LIN-NUM
                 MOVE 1999 TO LAST-CS
              END-IF
              IF CSA-CODE > 1999 AND < 3000 AND > LAST-CS
                 ADD 1 TO LIN-NUM
                 MOVE 2999 TO LAST-CS
              END-IF
           END-IF.
           PERFORM EDIT-CS.
           GO READ-CODPAIE.
       READ-CODPAIE-END.
           EXIT.

       EDIT-CS.
           ADD 1 TO LIN-NUM.
           IF POSTE NOT = 0
              IF CSA-POSTE NOT = POSTE
                 MOVE 0 TO POSTE POSTE-UNITE POSTE-TOTAL
              END-IF
           END-IF.
           MOVE CD-NUMBER TO CODES-NUMBER(LIN-NUM) CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           IF CSA-TEXTE NOT = SPACES
              MOVE CSA-TEXTE TO CODES-DESCR(LIN-NUM)
           ELSE
              MOVE CTX-NOM TO CODES-DESCR(LIN-NUM)
           END-IF.
           PERFORM FILL-CS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.

       FILL-CS.
           IF  CS-IMPRESSION(IDX) > 0 
           AND CSA-DONNEE(IDX) > 0
              MOVE CSA-DONNEE(IDX) TO HE-Z6Z2 HE-Z4Z4 HE-Z7Z2
              INSPECT HE-Z6Z2 REPLACING ALL ",00" BY "   "
              INSPECT HE-Z7Z2 REPLACING ALL ",00" BY "   "
              INSPECT HE-Z4Z4 REPLACING ALL ",0000" BY "     "
              MOVE CTX-DESCRIPTION(IDX) TO CODES-TEXTE(LIN-NUM, IDX)
              IF IDX = 5
                 MOVE HE-Z4Z4 TO CODES-VAL(LIN-NUM, IDX)
              ELSE
                 MOVE HE-Z6Z2 TO CODES-VAL(LIN-NUM, IDX)
              END-IF
              IF IDX = 6
                 MOVE HE-Z7Z2 TO CODES-VAL(LIN-NUM, IDX)
              END-IF
           END-IF.

       MATR.
           MOVE 1 TO IDX.
           STRING PR-NAISS-A DELIMITED BY SIZE INTO MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-NAISS-M DELIMITED BY SIZE INTO MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-NAISS-J DELIMITED BY SIZE INTO MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-SNOCS DELIMITED BY SIZE INTO MATRICULE
           WITH POINTER IDX.

       ADRESSE.
           MOVE PR-POLITESSE TO LNK-NUM.
           MOVE PR-LANGUAGE TO LNK-LANGUAGE.
           IF LNK-NUM = 0
              MOVE PR-CODE-SEXE TO LNK-NUM.
           MOVE "P" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO ADRES(1).
           MOVE 1 TO IDX.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           STRING LNK-TEXT DELIMITED BY "   "  INTO ADRES(2) 
           WITH POINTER IDX.
           MOVE PR-POBCO TO ADRES(4). 

           CALL "4-PRLOC" USING LINK-V PR-RECORD
           MOVE LNK-TEXT TO ADRES(5).
           CALL "4-PRRUE" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO ADRES(3).

       TOT-PLUS.
           ADD L-DC(IDX) TO SH-00.

       TOT-MINUS.
           ADD L-DC(IDX) TO SH-00.


       DATES.
           MOVE CON-DEBUT-A TO EN-AA.
           MOVE CON-DEBUT-M TO EN-MM.
           MOVE CON-DEBUT-J TO EN-JJ.
           MOVE CON-FIN-A TO SO-AA.
           MOVE CON-FIN-M TO SO-MM.
           MOVE CON-FIN-J TO SO-JJ.
           MOVE REG-ANCIEN-A TO AN-AA.
           MOVE REG-ANCIEN-M  TO AN-MM.
           MOVE REG-ANCIEN-J  TO AN-JJ.
           MOVE L-JOUR-DEBUT TO DEB-JJ.
           MOVE L-JOUR-FIN   TO FIN-JJ.
           MOVE LNK-MOIS     TO DEB-MM FIN-MM.
           MOVE LNK-ANNEE    TO DEB-AA FIN-AA.


        FICHE.
           MOVE FICHE-NUMERO TO FI-NO.
           MOVE FICHE-CLASSE TO FI-CLASSE.
           IF FICHE-TAUX > 0
              MOVE FICHE-TAUX TO FI-TAUX.
           PERFORM FICHE-ABAT VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 5.

       FICHE-ABAT.   
           IF FICHE-ABAT(IDX-1) > 0
              MOVE FICHE-ABAT(IDX-1) TO FI-ABAT(IDX-1).


       VIREMENTS.
           INITIALIZE VIR-RECORD LNK-SUITE LNK-PERSON IDX-1.
           MOVE LNK-MOIS TO VIR-MOIS-N.
           PERFORM READ-VIREM THRU READ-VIR-END.

       READ-VIREM.
           MOVE 66 TO SAVE-KEY.
           CALL "6-VIREM" USING LINK-V REG-RECORD VIR-RECORD SAVE-KEY.
           IF FR-KEY     NOT = VIR-FIRME
           OR REG-PERSON NOT = VIR-PERSON
           OR VIR-MOIS   NOT = LNK-MOIS 
           OR IDX-1 = 7
              GO READ-VIR-END.
           PERFORM READ-BANQP.
           ADD 1 TO IDX-1.
           MOVE VIR-BANQUE-C TO VIR-BANQUE(IDX-1).
           MOVE VIR-COMPTE-C TO VIR-COMPTE(IDX-1).
           MOVE BQP-BENEFIC-NOM TO VIR-INFO(IDX-1).
           IF BQP-BENEFIC-NOM = SPACES
              MOVE BQP-LIBELLE TO VIR-INFO(IDX-1).
           MOVE VIR-A-PAYER TO VIR-VAL(IDX-1).
           GO READ-VIREM.
       READ-VIR-END.
           EXIT.

       READ-BANQP.
           INITIALIZE BQP-RECORD.
           MOVE FR-KEY TO BQP-FIRME.
           MOVE REG-PERSON TO BQP-PERSON.
           MOVE VIR-TYPE  TO BQP-TYPE.
           MOVE VIR-SUITE TO BQP-SUITE.
           IF BQP-TYPE = 0
              MOVE 1 TO BQP-SUITE.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD FAKE-KEY.

        LIVRES.
           COMPUTE SH-00 = L-IMPOS(1) + L-IMPOS(5) + L-DC(205).  
           MOVE SH-00 TO HE-Z7Z2.
      *    MOVE L-IMPOS(1) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 1).
           MOVE L-IMPOS(24) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(1, 2).
           MOVE L-IMPOS(12) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(1, 3).
           MOVE 0 TO SH-00.
           IF L-REGIME > 99
              COMPUTE SH-00 = L-COT-NET(3) 
                            + L-COT-NET(4) 
                            + L-COT-NET(5) 
                            + L-COT-NET(6) 
                            + L-COT-NET(7) 
                            + L-COT-NET(8) 
                            + L-COT-NET(9) 
                            + L-COT-NET(10) 
           END-IF.
           COMPUTE SH-00 = SH-00 + L-IMPOS(17) + 
                           L-IMPOS(13) + L-IMPOS(14) + L-IMPOS(15) + 
                           L-IMPOS(16) + L-IMPOS(18) + L-IMPOS(19).  
           MOVE SH-00 TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(1, 4).
           MOVE L-IMPOS(22) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(1, 5).
           MOVE L-COT-NET(1) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(1, 6).
           MOVE L-COT-NET(2) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(1, 7).
           MOVE L-COT-NET(5) TO HE-Z7Z2.


           MOVE HE-Z7Z2 TO LIVRE-VAL(1, 10).

           IF L-REGIME > 99
              COMPUTE SH-00 = L-DC(233)
                            + L-DC(234)
                            + L-DC(235)
                            + L-DC(236)
                            + L-DC(237)
                            + L-DC(238)
                            + L-DC(239)
                            + L-DC(240)
              MOVE SH-00 TO HE-Z7Z2
              MOVE HE-Z7Z2 TO LIVRE-VAL(2, 4)
           END-IF.


           MOVE L-DC(231) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 6).
           MOVE L-DC(232) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 7).
           MOVE L-DC(235) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 10).

           COMPUTE SH-00 = L-IMPOS(25) + L-IMPOS(5) + L-DC(205).  
           MOVE SH-00 TO HE-Z7Z2.
      *    MOVE L-IMPOSABLE-NET TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(1, 8).
           COMPUTE SH-00 = L-DC(201) + L-DC(205).  
           MOVE SH-00 TO HE-Z7Z2.
      *    MOVE L-IMPOT TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 8).
           COMPUTE SH-00 = L-IMPOT-FIXE + L-IMPOT-FORFAIT.
           MOVE SH-00 TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 9).

           MOVE L-DECOMPTE-IMPOT TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 11).
      *    MOVE L-SALAIRE-NET TO HE-Z7Z2.
           COMPUTE SH-00 = L-SALAIRE-NET + L-DECOMPTE-IMPOT.
           MOVE SH-00 TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 12).

           MOVE 0 TO SH-00.
           PERFORM TOT-PLUS VARYING IDX FROM 1 BY 1 UNTIL IDX > 9.
           MOVE SH-00 TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 13).

           MOVE 0 TO SH-00.
           PERFORM TOT-PLUS VARYING IDX FROM 181 BY 1 UNTIL IDX > 199.
           MOVE SH-00 TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 14).

           MOVE 0 TO SH-00.
           PERFORM TOT-MINUS VARYING IDX FROM 266 BY 1 UNTIL IDX > 299.
           ADD L-IMPOS(5) TO SH-00.
           MOVE SH-00 TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 15).


           MOVE L-DC(300) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(2, 16).
           MOVE L-DC(200) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(1, 16).


      * NP
           MOVE L-IMPOSABLE-BRUT-NP TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(3, 1).
           MOVE L-DC(241) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(3, 6).
           MOVE L-DC(242) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(3, 7).
           MOVE L-DC(245) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(3, 10).
           MOVE L-DC(202) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(3, 8).
           MOVE L-DC(207) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(3, 9).

           MOVE L-SALAIRE-NET-NP TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(3, 12).

      * CUMUL
           MOVE LCUM-IMPOS(1, 1) TO HE-Z7Z2.
           COMPUTE SH-00 = LCUM-IMPOS(1, 1) + LCUM-IMPOS(1, 5) 
           + LCUM-DC(1, 205).  
           MOVE SH-00 TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(4, 1).
           MOVE LCUM-DC(1, 231) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(4, 6).
           MOVE LCUM-DC(1, 232) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(4, 7).
           MOVE LCUM-DC(1, 235) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(4, 10).
           MOVE LCUM-DC(1, 201) TO HE-Z7Z2.
           COMPUTE SH-00 = LCUM-DC(1, 201) + LCUM-DC(1, 205).
           MOVE SH-00 TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(4, 8).

           MOVE LCUM-IMPOS(1, 31) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(4, 12).

      * CUMUL NP
           MOVE LCUM-IMPOS(1, 2) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(5, 1).
           MOVE LCUM-DC(1, 241) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(5, 6).
           MOVE LCUM-DC(1, 242) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(5, 7).
           MOVE LCUM-DC(1, 245) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(5, 10).
           MOVE LCUM-DC(1, 202) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(5, 8).
           MOVE LCUM-DC(1, 207) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(5, 9).

           MOVE LCUM-IMPOS(1, 32) TO HE-Z7Z2.
           MOVE HE-Z7Z2 TO LIVRE-VAL(5, 12).

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.
           
           COPY "XDIS.CPY".
       DIS-HE-07.
           DISPLAY MOIS-DEBUT LINE 14 POSITION 31.
           MOVE MOIS-DEBUT TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 14351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-08.
           DISPLAY MOIS-FIN LINE 15 POSITION 31.
           MOVE MOIS-FIN TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 15351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1515 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL MODULE-I USING LINK-V TRANSFERT SS-RECORD.
           CANCEL MODULE-I.
           MOVE SAVE-MOIS TO LNK-MOIS.
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XSORT.CPY".

