      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-AUTOM CREATION CODES SALAIRES MISSIONS    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-AUTOM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODPAIE.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CODPAIE.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "V-BASES.REC".
           COPY "CODPAR.REC".
           COPY "CODPFIX.REC".
           COPY "LIVRE.CUM".
           COPY "LIVRE.REC".
           COPY "FICHE.REC".
           COPY "PAREX.REC".
           COPY "CONGE.REC".
           COPY "POCL.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".

       01  COMPTEUR              PIC 999 VALUE 0.
       01  BLOCAGE               PIC 9 VALUE 0.
       01  HRS-MOIS              PIC 999V99.
       01  SAL-ACT               PIC S9(8)V99 COMP-3.
       01  FLAG-NETFIX           PIC 9 VALUE 0.
       01  FLAG-COMPLEMENT.
           02 COMPLEMENT         PIC 9 OCCURS 20.

       01  LAST-FIRME            PIC 9(6) VALUE 0.
       01  LAST-MOIS             PIC 9(2) VALUE 0.
       01  MOIS                  PIC 9(2) VALUE 0.
       01  NOT-OPEN              PIC 9 VALUE 0.

       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-CODPAIE      PIC 999.

       01  HELP-1                PIC 9999.
       01  HELP-2                PIC 9999.
       01  HELP-3                PIC S9(6)V99 COMP-3.
       01  HELP-4                PIC S9(4)V99 COMP-3.
       01  HELP-5                PIC S9(4)V99 COMP-3.
       01  SOLDES.
           03 SOLDE              PIC S99999V99 OCCURS 9.

       01  ECR-DISPLAY.
           02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".
           COPY "PRESENCE.REC".
           COPY "HORSEM.REC".
           COPY "POSE.REC".
           COPY "MISSION.REC".
       01  TOT-OCCUP.
           02 TOT-OCC-IDX OCCURS 200.
              03 OCCUP-UNITE     PIC 9(8)V999 COMP-3.
              03 OCCUP-CODPAIE   PIC 99.


       PROCEDURE DIVISION USING LINK-V REG-RECORD CAR-RECORD CCOL-RECORD
       TOT-OCCUP PRESENCES POSE-RECORD HJS-RECORD MISS-RECORD.


       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON CODPAIE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-AUTOM.
       
           IF NOT-OPEN = 1 
              IF LNK-SUFFIX NOT = ANNEE-CODPAIE
                 CLOSE CODPAIE
                 MOVE 0 TO NOT-OPEN LAST-FIRME LAST-PERSON LAST-MOIS
              END-IF
           END-IF.
           IF NOT-OPEN = 0 
              MOVE LNK-SUFFIX TO ANNEE-CODPAIE
              OPEN I-O CODPAIE
              CALL "6-PARAM" USING LINK-V PAREX-RECORD
              MOVE 1 TO NOT-OPEN 
           END-IF.

           IF  LAST-FIRME  = REG-FIRME
           AND LAST-PERSON = REG-PERSON
           AND LAST-MOIS   = LNK-MOIS
              CONTINUE
           ELSE
              PERFORM HISTORIQUE
           END-IF.
           MOVE REG-FIRME  TO LAST-FIRME.
           MOVE REG-PERSON TO LAST-PERSON. 
           MOVE LNK-MOIS   TO LAST-MOIS.

           IF MISS-INDEXE = "N"
              MOVE MISS-SAL-ACT TO SAL-ACT
           ELSE
              COMPUTE SAL-ACT = MISS-SAL-100 * MOIS-IDX(LNK-MOIS) + ,001
           END-IF.
           MOVE SAL-ACT TO BAS-HORAIRE BAS-PRORATA.


           MOVE MISS-CLIENT TO PC-NUMBER.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY.
           INITIALIZE L-RECORD.
           PERFORM CREATE.
           PERFORM CPIFIX.

           EXIT PROGRAM.

       CSP-KEY.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY      TO CSP-FIRME.
           MOVE REG-PERSON  TO CSP-PERSON.
           MOVE LNK-MOIS    TO CSP-MOIS.
           MOVE LNK-SUITE   TO CSP-SUITE.
           MOVE MISS-CLIENT TO CSP-POSTE.
           MOVE MISS-REFERENCE TO CSP-EXTENSION.
           
       CREATE.
           PERFORM CSP-KEY.
           PERFORM CREATE-CSP VARYING IDX-1 FROM 11 BY 1 
           UNTIL IDX-1 > 180.

       CREATE-CSP.
           IF OCCUP-UNITE(IDX-1) > 0
             PERFORM CRE-CSP THRU CRE-END
             VARYING IDX-2 FROM 1 BY 1 UNTIL IDX-2 > 4.

       CRE-CSP.
           IF COD-PAR(IDX-1, IDX-2) > 0 
              MOVE COD-PAR(IDX-1, IDX-2) TO CSP-CODE CD-NUMBER
              PERFORM READ-CS
              IF CD-NUMBER = 0
                 GO CRE-END
              ELSE
                 PERFORM VAL-PREDEFINIE VARYING IDX-3 FROM 1 BY 1 
                 UNTIL IDX-3 > 8
              END-IF
              MOVE OCCUP-UNITE(IDX-1) TO CSP-UNITE
              PERFORM MINI-MAX VARYING IDX-3 FROM 1 BY 1 UNTIL IDX-3 > 8
              ADD 1 TO COMPTEUR
              PERFORM WRITE-CSP
           END-IF.
       CRE-END.
           EXIT.


       WRITE-CSP.
           IF CS-VALEUR(CAR-STATUT) = 0
              MOVE 0 TO CSP-UNITAIRE CSP-TOTAL
           END-IF.
           IF CS-VALEUR(10) = 1 AND CAR-HOR-MEN = 1
              MOVE 0 TO CSP-UNITAIRE CSP-TOTAL
           END-IF.
           IF CS-VALEUR(10) = 2 AND CAR-HOR-MEN = 0
              MOVE 0 TO CSP-UNITAIRE CSP-TOTAL
           END-IF.
           IF CSP-CODE = 2
              MOVE PC-NOM TO CSP-TEXTE
           ELSE
              INITIALIZE CSP-TEXTE.
           IF CSP-UNITE    > 0
           OR CSP-UNITAIRE > 0
           OR CSP-TOTAL    > 0
              CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY
           END-IF.

       READ-CS.           
           CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY.
SU         CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
           IF CS-PERMIS(CAR-STATUT) = 0
           OR CS-PERIODE(LNK-MOIS)  = 0
              INITIALIZE CS-RECORD
           END-IF.
           IF CAR-HOR-MEN = 1
              IF CS-PERMIS(10) = 1 
                 INITIALIZE CS-RECORD
              END-IF
           ELSE
              IF CS-PERMIS(10) = 2 
                 INITIALIZE CS-RECORD
              END-IF
           END-IF.
           IF CS-MALADIE = 2
              INITIALIZE CS-RECORD
           END-IF.
      
       VAL-PREDEFINIE.
           MOVE CS-VAL-PREDEFINIE(IDX-3) TO CSP-DONNEE(IDX-3).

       CPF-PREDEFINIE.
           IF CPF-DONNEE(IDX-3) > 0
              MOVE CPF-DONNEE(IDX-3) TO CSP-DONNEE(IDX-3).

       MINI-MAX.
           IF CS-PROCEDURE(IDX-3) NOT = 0
              IF CS-PROCEDURE(IDX-3) < 1000
              OR CS-PROCEDURE(IDX-3) > 1200
                 MOVE CS-PROCEDURE(IDX-3) TO LNK-NUM
                 MOVE IDX-3 TO LNK-INDEX
                 CALL "4-PROC" USING LINK-V 
                                     CCOL-RECORD
                                     REG-RECORD
                                     CAR-RECORD
                                     FICHE-RECORD 
                                     PRESENCES 
                                     POSE-RECORD 
                                     HJS-RECORD 
                                     BASES-REMUNERATION
                                     CSP-RECORD
                                     LCUM-RECORD
              ELSE 
                 MOVE CS-PR-3(IDX-3) TO IDX-4
                 MOVE OCCUP-UNITE(IDX-4) TO CSP-DONNEE(IDX-3)
              END-IF
           END-IF.
           IF CS-VAL-MAX(IDX-3) NOT = 0
              IF CSP-DONNEE(IDX-3) > CS-VAL-MAX(IDX-3) 
                 MOVE CS-VAL-MAX(IDX-3) TO CSP-DONNEE(IDX-3)
              END-IF
           END-IF.
           IF CSP-DONNEE(IDX-3) < CS-VAL-MIN(IDX-3) 
              MOVE CS-VAL-MIN(IDX-3) TO CSP-DONNEE(IDX-3)
           END-IF.
           IF CS-NOTPOL(IDX-3) > SPACES
              MOVE IDX-3 TO LNK-NUM
              CALL "4-NOTPOL" USING LINK-V CS-RECORD CSP-RECORD.
           IF CS-FORMULE(IDX-3) NOT = 0
              MOVE IDX-3 TO LNK-NUM
              CALL "4-FORMUL" USING LINK-V CS-RECORD CSP-RECORD.

       HISTORIQUE.
           CALL "6-GCP" USING LINK-V CP-RECORD.
           INITIALIZE FICHE-RECORD SAVE-KEY.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD SAVE-KEY.
           CALL "4-LPCUM" USING LINK-V REG-RECORD LCUM-RECORD.
           CALL "4-SALBAS" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 POSE-RECORD 
                                 HJS-RECORD 
                                 BASES-REMUNERATION.


           CALL "4-SALMOY" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 HJS-RECORD 
                                 BASES-REMUNERATION
                                 LCUM-RECORD.

       CPIFIX.
           PERFORM CPIFIX0.

       CPIFIX0.
           INITIALIZE CPF-RECORD.
           MOVE FR-KEY TO CPF-FIRME.
           PERFORM READ-FIX THRU READ-FIX-END.

       READ-FIX.
           MOVE 66 TO SAVE-KEY.
           CALL "6-CPIFIX" USING LINK-V MISS-RECORD CPF-RECORD SAVE-KEY.
           IF FR-KEY NOT = CPF-FIRME
              GO READ-FIX-END
           END-IF.
           PERFORM READ-CPF THRU READ-CPF-END.
           GO READ-FIX.
       READ-FIX-END.
           EXIT.

       CPIFIX1.
           CALL "6-CPIFIX" USING LINK-V MISS-RECORD CPF-RECORD FAKE-KEY.

       READ-CPF.
           MOVE 0 TO INPUT-ERROR
           PERFORM CSP-KEY.
           MOVE CPF-CODE TO CD-NUMBER CSP-CODE.
           PERFORM READ-CS.
           IF CD-NUMBER = 0
              MOVE 1 TO INPUT-ERROR
              GO READ-CPF-END
           END-IF.
           IF CPF-PERIODE-IDX(LNK-MOIS) = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF CS-PROCEDURE(1) = 999
      *       MOVE 1 TO FLAG-NETFIX 
              MOVE 1 TO INPUT-ERROR
           END-IF.

           IF INPUT-ERROR > 0
              GO READ-CPF-END
           END-IF.
      *    IF CPF-REDEFINE = "Y" OR "J" OR "O" 
      *       PERFORM DEL-CSP
      *    END-IF.

           PERFORM VAL-PREDEFINIE VARYING IDX-3 FROM 1 BY 1 UNTIL 
                                          IDX-3 > 8.
           PERFORM CPF-PREDEFINIE VARYING IDX-3 FROM 1 BY 1 UNTIL 
                                          IDX-3 > 8.
           PERFORM MINI-MAX VARYING IDX-3 FROM 1 BY 1 UNTIL IDX-3 > 8.
           ADD 1 TO COMPTEUR
           PERFORM WRITE-CSP.
       READ-CPF-END.
      *     IF INPUT-ERROR > 0
      *        PERFORM DEL-CSP
      *     END-IF.
           MOVE 0 TO INPUT-ERROR.

        
       SAL-MOIS.
           MOVE L-UNIT(100) TO HRS-MOIS.
           MOVE 0 TO IDX-2.
           PERFORM OCCUP VARYING IDX-1 FROM 11 BY 1 UNTIL IDX-1 > 50.
           MOVE 100 TO IDX-3.
           IF IDX-2 > 0
              MOVE L-DEB(100) TO SH-00
              IF L-UNIT(100) = 0
                 MOVE L-DEB(100) TO L-DEB(IDX-2)
                 MOVE 0 TO L-DEB(100) 
                 MOVE IDX-2 TO IDX-3
                 SUBTRACT 1 FROM IDX-2
              END-IF
              PERFORM OCCUP-1 VARYING IDX-1 FROM 11 BY 1 UNTIL IDX-1 
                 > IDX-2
           END-IF.

       OCCUP.
           IF PAREX-OCC(IDX-1) > 0
           AND L-DEB(IDX-1) = 0 
           AND L-UNIT(IDX-1) > 0 
              ADD L-UNIT(IDX-1) TO HRS-MOIS
              MOVE IDX-1 TO IDX-2
           END-IF.

       OCCUP-1.
           IF PAREX-OCC(IDX-1) > 0
           AND L-DEB(IDX-1) = 0 
              COMPUTE SAL-ACT = SH-00 / HRS-MOIS * L-UNIT(IDX-1) + ,005
              MOVE SAL-ACT TO L-DEB(IDX-1)
              SUBTRACT SAL-ACT FROM L-DEB(IDX-3)
           END-IF.

