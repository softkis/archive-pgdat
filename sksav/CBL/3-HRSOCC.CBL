      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 3-HRSOCC     LIVRE DES OCCUPATIONS �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-HRSOCC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".
           COPY "TRIPR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  PRECISION             PIC 9 VALUE 0.
       01  SYMBOLES.
           02  FILLER PIC X VALUE " ".
           02  FILLER PIC X VALUE "C".
           02  FILLER PIC X VALUE "M".
           02  FILLER PIC X VALUE "F".
           02  FILLER PIC X VALUE "*".
           02  FILLER PIC X VALUE "X".
           02  FILLER PIC X VALUE "R".
           02  FILLER PIC X VALUE "E".
           02  FILLER PIC X VALUE "-".
           02  FILLER PIC X VALUE "A".
           02  FILLER PIC X VALUE "?".
           02  FILLER PIC X VALUE "G".
           02  FILLER PIC X VALUE "#".

       01  SYM REDEFINES SYMBOLES.
           02 SYMBOLE PIC X OCCURS 13.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "METIER.REC".
           COPY "JOURS.REC".
           COPY "CONTRAT.REC".
           COPY "MESSAGE.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(080) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".OCA".

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  OCCUPATIONS.
           02 OC-TEST PIC 9999V99.
           02 OCT OCCURS 14.
              04 OC-TOT PIC 9999V99.
              04 OC-JRS PIC 999.
           02 OC-MOIS OCCURS 12.
              03 OCC OCCURS 14.
                 04 INTER PIC 99V99 OCCURS 31.

      *    1 = TRAVAIL
      *    2 = CONGE  
      *    3 = MALADIE
      *    4 = FERIE
      *    5 = MATERNITE
      *    6 = EXTRA
      *    7 = REPOS/RECUP
      *    8 = FORMATION/ECOLE
      *    9 = CHOMAGE
      *   10 = AUTORISE
      *   11 = VOLONTAIRE
      *   13 = ETRANGER
      *   14 = TOTAL


       01  CHOIX                 PIC 99 VALUE 0.
       01  MOIS                  PIC 99 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-HRSOCC.
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE PRECISION TO LNK-PRESENCE.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR (14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 IF REG-PERSON NOT = END-NUMBER
                      PERFORM AVANT-SORT
                   END-IF
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-SORT
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-3
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-3
           END-IF.
           PERFORM JRS.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.


       FILL-FILES.
           PERFORM READ-CONTRAT.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
           END-IF.
           PERFORM READ-FORM.
           MOVE CAR-METIER TO MET-CODE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           COMPUTE LIN-NUM =  2.
           MOVE 45 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

      * DATE EDITION
           MOVE 68 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 71 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 74 TO COL-NUM.
           MOVE TODAY-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

      * DONNEES FIRME
           COMPUTE LIN-NUM =  4.
           MOVE  3 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.


      * DONNEES PERSONNE
           COMPUTE LIN-NUM = 7.
           MOVE  3 TO COL-NUM.
           MOVE PR-NAISS-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  8 TO COL-NUM.
           MOVE PR-NAISS-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 11 TO COL-NUM.
           MOVE PR-NAISS-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 14 TO COL-NUM.
           MOVE PR-SNOCS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 5.
           MOVE  6 TO CAR-NUM.
           MOVE 20 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           
           PERFORM FILL-FORM.
           MOVE 27 TO COL-NUM.
           MOVE PR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 6.
           MOVE 27 TO COL-NUM.
           MOVE PR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 31 TO COL-NUM.
           MOVE PR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 7.
           MOVE 27 TO COL-NUM.
           MOVE PR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  5 TO CAR-NUM.
           MOVE 29 TO COL-NUM.
           MOVE PR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 35 TO COL-NUM.
           MOVE PR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 8.
           MOVE  3 TO COL-NUM.
           MOVE MET-NOM(PR-CODE-SEXE) TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 9.
           MOVE 13 TO COL-NUM.
           MOVE CON-DEBUT-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 16 TO COL-NUM.
           MOVE CON-DEBUT-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 19 TO COL-NUM.
           MOVE CON-DEBUT-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 38 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE CON-FIN-J TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 41 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE CON-FIN-M TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 45 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE CON-FIN-A TO  VH-00.
           PERFORM FILL-FORM.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE 12        TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.

       JRS.
           INITIALIZE JRS-RECORD OCCUPATIONS.
           PERFORM HRS THRU HRS-END VARYING 
                   MOIS FROM 1 BY 1 UNTIL MOIS > 12.
           IF OC-TEST > 0
              PERFORM FILL-FILES
              PERFORM FILL-TOT VARYING MOIS FROM 1 BY 1 UNTIL MOIS > 12
              PERFORM TRANSMET
           END-IF.

       TRANSMET.
           ADD 1 TO COUNTER.
           MOVE 70 TO LNK-LINE.
           MOVE  0 TO LNK-VAL.
           CALL "P080" USING LINK-V FORMULAIRE.

       HRS.
           IF JRS-MOIS = 0
              MOVE MOIS TO JRS-MOIS
              CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD NUL-KEY
           ELSE
              CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD NX-KEY.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR MOIS       NOT = JRS-MOIS
              GO HRS-END
           END-IF.
           IF JRS-COMPLEMENT NOT = 0
           OR JRS-POSTE      NOT = 0
              GO HRS
           END-IF.
           PERFORM CUM-HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.
           PERFORM CUM-JOURS.
           GO HRS.
       HRS-END.
           INITIALIZE JRS-RECORD.

       CUM-HEURES.
           IF JRS-HRS(IDX) > 0
              ADD JRS-HRS(IDX) TO OC-TEST
              EVALUATE JRS-OCCUPATION
              WHEN  0 
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS,  1, IDX) OC-TOT(1)
              WHEN  1 THRU  9 
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS,  3, IDX) OC-TOT(3)
              WHEN 20
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS,  2, IDX) OC-TOT(2)
              WHEN 10
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS,  5, IDX) OC-TOT(5)
              WHEN 11 THRU 12
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS,  3, IDX) OC-TOT(3)
              WHEN 13 THRU 14
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS,  8, IDX) OC-TOT(8)
              WHEN 15 THRU 16
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS,  4, IDX) OC-TOT(4)
              WHEN 21 THRU 30
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS,  6, IDX) OC-TOT(6)
              WHEN 31 THRU 33
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS,  7, IDX) OC-TOT(7)
              WHEN 35 
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS, 10, IDX) OC-TOT(10)
              WHEN 40 
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS, 11, IDX) OC-TOT(11)
              WHEN 41 THRU 43
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS,  9, IDX) OC-TOT(9)
              WHEN 36 
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS, 13, IDX) OC-TOT(13)
              WHEN 50 
                ADD JRS-HRS(IDX) TO INTER(JRS-MOIS, 12, IDX) OC-TOT(12)
              END-EVALUATE
           END-IF.


       CUM-JOURS.
           EVALUATE JRS-OCCUPATION
              WHEN  0 ADD JRS-JOURS TO OC-JRS(1)
              WHEN  1 THRU  9  ADD JRS-JOURS TO OC-JRS(3)
              WHEN 20 ADD JRS-JOURS TO OC-JRS(2)
              WHEN 10 ADD JRS-JOURS TO OC-JRS(5)
              WHEN 11 THRU 12 ADD JRS-JOURS TO OC-JRS(3)
              WHEN 13 THRU 14 ADD JRS-JOURS TO OC-JRS(8)
              WHEN 15 THRU 16 ADD JRS-JOURS TO OC-JRS(4)
              WHEN 21 THRU 30 ADD JRS-JOURS TO OC-JRS(6)
              WHEN 31 THRU 33 ADD JRS-JOURS TO OC-JRS(7)
              WHEN 35 ADD JRS-JOURS TO OC-JRS(10)
              WHEN 40 ADD JRS-JOURS TO OC-JRS(11)
              WHEN 41 THRU 43 ADD JRS-JOURS TO OC-JRS(9)
              WHEN 36 ADD JRS-JOURS TO OC-JRS(13)
              WHEN 50 ADD JRS-JOURS TO OC-JRS(12)
           END-EVALUATE.

       FILL-TOT.
           PERFORM FILL-JRS VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 13.

       FILL-JRS.
           PERFORM FILL-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.
           IF OC-TOT(IDX-1) > 0
              COMPUTE LIN-NUM = 45 + IDX-1
              MOVE OC-TOT(IDX-1) TO VH-00
              COMPUTE COL-NUM = 45
              MOVE 4 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              PERFORM FILL-FORM
              MOVE OC-JRS(IDX-1) TO VH-00
              COMPUTE COL-NUM = 55
              MOVE 4 TO CAR-NUM
              PERFORM FILL-FORM
           END-IF.

       FILL-HRS.
           IF INTER(MOIS, IDX-1, IDX) > 0
              COMPUTE LIN-NUM = 12 + IDX
              MOVE INTER(MOIS, IDX-1, IDX) TO VH-00
              COMPUTE COL-NUM = 3 + MOIS * 6
              MOVE 2 TO CAR-NUM
              MOVE 1 TO DEC-NUM
              PERFORM FILL-FORM
              COMPUTE COL-NUM = 2 + MOIS * 6
              MOVE SYMBOLE(IDX-1) TO ALPHA-TEXTE
              PERFORM FILL-FORM
           END-IF.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.


       METIER.
           MOVE CAR-METIER TO MET-CODE.
           MOVE "F "       TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           MOVE 20 TO LIN-NUM.
           MOVE 10 TO COL-NUM.
           PERFORM FILL-FORM.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 1202 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XSORT.CPY".
           COPY "XACTION.CPY".
