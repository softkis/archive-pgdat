      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-REVET RELEVE DES ETUDIANTS + ECOLIERS     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-REVET.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "LIVRE.REC".
           COPY "PARMOD.REC".
           COPY "REV.LNK".

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  COUNTER               PIC 9999 VALUE 0.
       01  ANNEE-LIMITE          PIC 9(4).
       01  SAVE-LG               PIC X.


           COPY "V-VAR.CPY".
        
       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6Z2 PIC Z(6),ZZ.
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z4Z4 PIC ZZZZ,ZZZZ.
           02 HE-Z2Z2 PIC ZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HE-PERIODE.
              03 HE-DD PIC ZZ.
              03 FILLER PIC X VALUE "-".
              03 HE-FF PIC ZZ.
              03 FILLER PIC X VALUE " ".
              03 HE-MOIS PIC X(12).


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-REVET.
       
           CALL "0-TODAY" USING TODAY.
           COMPUTE ANNEE-LIMITE = LNK-ANNEE - 25.
           INITIALIZE SH-00 LIN-IDX.
           MOVE LNK-MOIS TO SAVE-MOIS.
           IF MENU-BATCH > 7
              IF LNK-VAL = 99
                 PERFORM END-PROGRAM
              END-IF
              INITIALIZE REG-RECORD
              PERFORM START-PERSON 
              PERFORM READ-PERSON THRU READ-EXIT
              MOVE SAVE-MOIS TO LNK-MOIS
              EXIT PROGRAM
           END-IF.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-PERSON 
                    PERFORM READ-PERSON THRU READ-EXIT
                    PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           INITIALIZE LINK-RECORD.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           IF REG-NAISS-A < ANNEE-LIMITE
              GO READ-PERSON-2.
           PERFORM GET-PERS.
           IF MENU-BATCH > 7
              PERFORM DIS-LOOP
           ELSE
              PERFORM DIS-HE-01.
           PERFORM CUMUL-LIVRE.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.
           IF LIN-IDX > 0 
              PERFORM TRANSMET
           END-IF.


       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       DIS-HE-01.
           DISPLAY A-N LINE 4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           IF A-N = "N"
              DISPLAY LNK-TEXT LINE 6 POSITION 27 SIZE 34
           ELSE
              DISPLAY SPACES   LINE 6 POSITION 37 SIZE 1
              DISPLAY LNK-TEXT LINE 6 POSITION 38 SIZE 23.
       DIS-HE-END.
           EXIT.

       DIS-LOOP.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6  LINE 10 POSITION 20 SIZE 6
           DISPLAY PR-NOM LINE 10 POSITION 27.

       CUMUL-LIVRE.
           PERFORM LP VARYING LNK-MOIS FROM 1 BY 1 UNTIL LNK-MOIS > 12.

       LP.
           INITIALIZE L-RECORD LNK-SUITE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-REGIME = 5
           AND L-IMPOSABLE-BRUT > 0
              PERFORM LIGNES
           END-IF.

       LIGNES.
           ADD 1 TO LIN-IDX.
           MOVE PR-NOM TO LINK-NOM(LIN-IDX).
           CALL "4-PRRUE" USING LINK-V PR-RECORD
           MOVE LNK-TEXT TO LINK-ADRESSE(LIN-IDX).
           MOVE PR-NAISS-J TO HE-JJ.
           MOVE PR-NAISS-M TO HE-MM.
           MOVE PR-NAISS-A TO HE-AA.
           MOVE HE-DATE TO LINK-DATE-NAISS(LIN-IDX).
           MOVE L-IMPOSABLE-BRUT TO HE-Z6Z2.
           MOVE HE-Z6Z2 TO LINK-SALAIRE(LIN-IDX).
           IF L-HOR-MEN = 0
              MOVE L-SAL-HOR  TO HE-Z4Z4
              MOVE HE-Z4Z4 TO LINK-SAL-HEURE(LIN-IDX)
           ELSE
              MOVE L-SAL-MOIS TO HE-Z6Z2
              MOVE HE-Z6Z2 TO LINK-SAL-MOIS(LIN-IDX).
           MOVE L-JOUR-DEBUT TO HE-DD.
           MOVE L-JOUR-FIN   TO HE-FF.
           MOVE L-MOIS TO  LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO HE-MOIS.
           MOVE HE-PERIODE TO LINK-PERIODE(LIN-IDX).

           ADD 1 TO LIN-IDX.
           INSPECT PR-PRENOM CONVERTING
           "ABCDEFGHIJKLMNOPQRSTUVWXYZ릻쉸" TO
           "abcdefghijklmnopqrstuvwxyz굱걚".
           MOVE PR-PRENOM TO LNK-TEXT.
           CALL "0-NOMMAJ" USING LINK-V.
           MOVE LNK-TEXT TO PR-PRENOM.
           MOVE PR-PRENOM TO LINK-NOM(LIN-IDX).

           CALL "4-PRLOC" USING LINK-V PR-RECORD
           MOVE LNK-TEXT TO LINK-ADRESSE(LIN-IDX).

           ADD L-IMPOSABLE-BRUT TO SH-00.
           MOVE SH-00 TO HE-Z6Z2.
           MOVE HE-Z6Z2 TO LINK-TOTSAL.
           IF LIN-IDX > 27
              PERFORM TRANSMET
              MOVE 0 TO LIN-IDX
           END-IF.

       TRANSMET.
           PERFORM ENTETE.
           MOVE 0 TO LNK-VAL.
           CALL "REVET" USING LINK-V LINK-RECORD.
           ADD 1 TO COUNTER.
           INITIALIZE LINK-RECORD.

       ENTETE.
           MOVE FR-NOM TO LINK-PATRON-NOM.
           MOVE 1 TO IDX-4.
           STRING FR-MAISON DELIMITED BY " " INTO 
           LINK-PATRON-ADRESSE WITH POINTER IDX-4.
           ADD 2 TO IDX-4.
           STRING FR-RUE DELIMITED BY "  " INTO LINK-PATRON-ADRESSE
           WITH POINTER IDX-4.
           ADD  1 TO IDX-4.
           STRING FR-PAYS DELIMITED BY "  " INTO LINK-PATRON-ADRESSE
           WITH POINTER IDX-4.
           MOVE FR-CODE-POST TO HE-Z6.
           ADD 2 TO IDX-4.
           STRING HE-Z6 DELIMITED BY SIZE INTO LINK-PATRON-ADRESSE
           WITH POINTER IDX-4.
           ADD 2 TO IDX-4.
           STRING FR-LOCALITE DELIMITED BY SIZE INTO LINK-PATRON-ADRESSE 
           WITH POINTER IDX-4.
           MOVE LNK-ANNEE TO LINK-ANNEE.

           MOVE 1 TO IDX-4.
           STRING FR-ETAB-A DELIMITED BY " " INTO 
           LINK-PATRON-TVA WITH POINTER IDX-4.
           ADD 2 TO IDX-4.
           STRING FR-ETAB-N DELIMITED BY " " INTO 
           LINK-PATRON-TVA WITH POINTER IDX-4.
           ADD 2 TO IDX-4.
           STRING FR-SNOCS DELIMITED BY " " INTO 
           LINK-PATRON-TVA WITH POINTER IDX-4.

       AFFICHAGE-ECRAN.
           MOVE 1301 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
           
       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "REVET" USING LINK-V LINK-RECORD.
           CANCEL "REVET".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

