      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-CPYPER COPIER PERSONNE DANS NOUVELLE FIRME�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-CPYPER.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "REGISTRE.FC".
           COPY "BANQP.FC".
           COPY "CODFIX.FC".
           COPY "CARRIERE.FC".
           COPY "FICHE.FC".
           COPY "CONGE.FC".
           COPY "HORSEM.FC".
           COPY "CONTRAT.FC".
           COPY "JOURS.FC".
           COPY "LIVRE.FC".
           COPY "HEURES.FC".
           COPY "CODPAIE.FC".
           COPY "VIREMENT.FC".
           COPY "FAMILLE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "REGISTRE.FDE".
           COPY "BANQP.FDE".
           COPY "CODFIX.FDE".
           COPY "CARRIERE.FDE".
           COPY "FICHE.FDE".
           COPY "CONGE.FDE".
           COPY "HORSEM.FDE".
           COPY "CONTRAT.FDE".
           COPY "JOURS.FDE".
           COPY "LIVRE.FDE".
           COPY "HEURES.FDE".
           COPY "CODPAIE.FDE".
           COPY "VIREMENT.FDE".
           COPY "FAMILLE.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 5.
       01  NEW-NUMBER            PIC 9(6) VALUE 0.
       01  OLD-NUMBER            PIC 9(6) VALUE 0.
       01  NEW-FIRME             PIC 9(6) VALUE 0.
       01  SAVE-NUMBER           PIC 9(6) VALUE 0.

           COPY "FIRME.REC".
           COPY "PERSON.REC".
           COPY "POCL.REC".
       01  CONGE-NAME.
           02 FILLER             PIC X(9) VALUE "S-CONGE.".
           02 ANNEE-CONGE        PIC 999 VALUE 0.


       01  LIVRE-NAME.
           02 FILLER             PIC X(8) VALUE "S-LIVRE.".
           02 ANNEE-LIVRE        PIC 999.
       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.
       01  VIR-NAME.
           02 FILLER             PIC X(8) VALUE "S-VIREM.".
           02 ANNEE-VIR          PIC 999.
       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.
       01  HRS-NAME.
           02 HRS-ID             PIC X(9) VALUE "S-HEURES.".
           02 ANNEE-HEURES       PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               REGISTRE
               CARRIERE 
               CODFIX
               CONTRAT
               FICHE  
               BANQP 
               CONGE
               HORSEM
               JOURS
               CODPAIE
               LIVRE
               HEURES
               FAMILLE.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-CPYPER.

           MOVE LNK-SUFFIX TO ANNEE-CONGE
                              ANNEE-HEURES
                              ANNEE-JOURS
                              ANNEE-LIVRE
                              ANNEE-PAIE
                              ANNEE-VIR.
           OPEN I-O   REGISTRE.
           MOVE FR-KEY TO STORE.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1 THRU 3 
                  MOVE 0063640000 TO EXC-KFR(1)
                  MOVE 0000000065 TO EXC-KFR(13)
                  MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 MOVE 0000130025 TO EXC-KFR(3)
           WHEN 5 MOVE 0000000025 TO EXC-KFR(1)
                  MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-1.
           ACCEPT FIRME-KEY 
             LINE  4 POSITION 37 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT REG-PERSON 
             LINE  5 POSITION 35 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           ACCEPT REG-MATCHCODE
             LINE  6 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4.
           IF NEW-NUMBER = 0
              MOVE REG-PERSON TO NEW-NUMBER 
           END-IF.
           ACCEPT NEW-NUMBER 
             LINE  8 POSITION 35 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3 
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   ELSE
                      MOVE "N" TO LNK-A-N
                   END-IF
                   CALL "2-FIRME" USING LINK-V
                   PERFORM AFFICHAGE-ECRAN
           WHEN OTHER PERFORM NEXT-FIRME.
           IF FR-COMPETENCE > LNK-COMPETENCE
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-01.

       APRES-2.
           MOVE FR-KEY TO REG-FIRME.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   ELSE
                      MOVE "N" TO LNK-A-N
                   END-IF
                   MOVE 0 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET REG-KEY-A
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-02 THRU DIS-HE-END.
           MOVE REG-PERSON TO OLD-NUMBER.

       APRES-3.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                  PERFORM DIS-HE-03 THRU DIS-HE-END
           END-EVALUATE.
           PERFORM DIS-HE-02.
           
       APRES-4.
           IF NEW-NUMBER = 0
              MOVE 1 TO INPUT-ERROR 
           ELSE
           EVALUATE EXC-KEY 
              WHEN 15 CONTINUE
              WHEN OTHER 
              MOVE REG-PERSON TO SAVE-NUMBER
              MOVE FIRME-KEY TO REG-FIRME
              MOVE NEW-NUMBER TO REG-PERSON
              READ REGISTRE INVALID CONTINUE
              NOT INVALID
              MOVE "SL" TO LNK-AREA
              MOVE 9 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              END-READ
              MOVE SAVE-NUMBER TO REG-PERSON
           END-EVALUATE
           END-IF.
           IF EXC-KEY = 15 
              MOVE 13 TO EXC-KEY 
           END-IF.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN 5 PERFORM CHANGE-NUMERO
      *            MOVE 0 TO NEW-NUMBER
                   MOVE 3 TO DECISION 
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-FIRME.
           CALL "6-FIRMES" USING LINK-V FIRME-RECORD "N" EXC-KEY.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 3
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.


       CHANGE-NUMERO.
           INITIALIZE
           BQP-RECORD
           CAR-RECORD
           CSF-RECORD
           CSP-RECORD
           CON-RECORD
           CONGE-RECORD
           FICHE-RECORD
           VIR-RECORD
           HRS-RECORD
           JRS-RECORD
           HJS-RECORD
           FAM-RECORD
           L-RECORD.
       
           DISPLAY "COMPTES BANQUE" LINE 23 POSITION 40
           PERFORM COPIE-BANQUE.
           DISPLAY "CARRIERE      " LINE 23 POSITION 40
           PERFORM COPIE-CARRIERE.
           DISPLAY "CONTRATS      " LINE 23 POSITION 40
           IF MENU-BATCH = 0 
              PERFORM COPIE-CONTRAT.
           DISPLAY "CODES FIXES   " LINE 23 POSITION 40
           PERFORM COPIE-CSFIX.
           DISPLAY "FICHES IMPOT  " LINE 23 POSITION 40
           PERFORM COPIE-FICHE.
           DISPLAY "HORAIRE SEM   " LINE 23 POSITION 40
           PERFORM COPIE-HORSEM.
           DISPLAY "FAMILLE       " LINE 23 POSITION 40
           PERFORM COPIE-FAMILLE.
           IF MENU-PROG-NUMBER = 1 
              DISPLAY "HEURES        " LINE 23 POSITION 40
              PERFORM COPIE-HEURES
              DISPLAY "JOURS         " LINE 23 POSITION 40
              PERFORM COPIE-JOURS
              DISPLAY "LIVRE DE PAIE " LINE 23 POSITION 40
              PERFORM COPIE-LIVRE
              DISPLAY "VIREMENTS     " LINE 23 POSITION 40
              PERFORM COPIE-VIREMENT
              DISPLAY "CODES SALAIRE " LINE 23 POSITION 40
              PERFORM COPIE-CODPAIE
           END-IF.
           DISPLAY "              " LINE 23 POSITION 40

           MOVE FR-KEY TO REG-FIRME.
           READ REGISTRE INVALID CONTINUE
               NOT INVALID
               MOVE FIRME-KEY TO REG-FIRME REG-FIRME-A
               MOVE NEW-NUMBER TO REG-PERSON REG-PERSON-A
               WRITE REG-RECORD INVALID REWRITE REG-RECORD END-WRITE
               IF LNK-SQL = "Y" 
                  CALL "9-REGIST" USING LINK-V REG-RECORD WR-KEY 
               END-IF
           END-READ.
           MOVE OLD-NUMBER TO REG-PERSON.

       COPIE-BANQUE.
           OPEN I-O   BANQP.
           MOVE FR-KEY  TO BQP-FIRME.
           MOVE REG-PERSON  TO BQP-PERSON.
           READ BANQP INVALID CONTINUE
               NOT INVALID
               MOVE FIRME-KEY  TO BQP-FIRME
               MOVE NEW-NUMBER TO BQP-PERSON
               WRITE BQP-RECORD INVALID REWRITE BQP-RECORD  END-WRITE
               IF LNK-SQL = "Y" 
                  CALL "9-BANQP" USING LINK-V BQP-RECORD WR-KEY 
               END-IF
           END-READ.
           PERFORM READ-BQP THRU READ-BQP-END.
       READ-BQP.
           MOVE FR-KEY  TO BQP-FIRME.
           MOVE REG-PERSON  TO BQP-PERSON.
           START BANQP KEY > BQP-KEY INVALID KEY 
                GO READ-BQP-END.
           READ BANQP NEXT AT END 
                GO READ-BQP-END.
           IF FR-KEY NOT = BQP-FIRME
           OR REG-PERSON  NOT = BQP-PERSON
              GO READ-BQP-END.
           MOVE FIRME-KEY TO BQP-FIRME.
           MOVE NEW-NUMBER TO BQP-PERSON.
           IF LNK-SQL = "Y" 
              CALL "9-BANQP" USING LINK-V BQP-RECORD WR-KEY 
           END-IF.
           WRITE BQP-RECORD INVALID REWRITE BQP-RECORD.
           GO READ-BQP.
       READ-BQP-END.
           CLOSE BANQP.

       COPIE-CARRIERE.
           OPEN I-O   CARRIERE.
           INITIALIZE CAR-RECORD.
           MOVE FR-KEY  TO CAR-FIRME.
           MOVE REG-PERSON  TO CAR-PERSON.
           PERFORM READ-CAR THRU READ-CAR-END.
       READ-CAR.
           MOVE FR-KEY  TO CAR-FIRME.
           MOVE REG-PERSON  TO CAR-PERSON.
           START CARRIERE KEY > CAR-KEY INVALID  
                GO READ-CAR-END.
           READ CARRIERE NEXT AT END 
                GO READ-CAR-END.
           IF FR-KEY NOT = CAR-FIRME
           OR REG-PERSON NOT = CAR-PERSON
              GO READ-CAR-END.
           MOVE FIRME-KEY  TO CAR-FIRME.
           MOVE NEW-NUMBER TO CAR-PERSON.
           IF LNK-SQL = "Y" 
              CALL "9-CARRI" USING LINK-V CAR-RECORD WR-KEY 
           END-IF. 
           WRITE CAR-RECORD INVALID REWRITE CAR-RECORD.
           GO READ-CAR.
       READ-CAR-END.
           CLOSE CARRIERE.

       COPIE-CSFIX.
           OPEN I-O   CODFIX.
           MOVE FR-KEY  TO CSF-FIRME.
           MOVE REG-PERSON  TO CSF-PERSON.
           PERFORM READ-CSF THRU READ-CSF-END.
       READ-CSF.
           MOVE FR-KEY  TO CSF-FIRME.
           MOVE REG-PERSON  TO CSF-PERSON.
           START CODFIX KEY > CSF-KEY INVALID KEY 
                GO READ-CSF-END.
           READ CODFIX NEXT AT END 
                GO READ-CSF-END.
           IF FR-KEY NOT = CSF-FIRME
           OR REG-PERSON  NOT = CSF-PERSON
                GO READ-CSF-END.
           MOVE FIRME-KEY  TO CSF-FIRME.
           MOVE NEW-NUMBER TO CSF-PERSON.
           IF LNK-SQL = "Y" 
              CALL "9-CODFIX" USING LINK-V CSF-RECORD WR-KEY 
           END-IF.
           WRITE CSF-RECORD INVALID REWRITE CSF-RECORD.
           GO READ-CSF.
       READ-CSF-END.
           CLOSE CODFIX.

       COPIE-CONGE.
           OPEN I-O CONGE.
           MOVE FR-KEY TO CONGE-FIRME.
           MOVE REG-PERSON TO CONGE-PERSON.
           READ CONGE INVALID CONTINUE
                NOT INVALID
           MOVE FIRME-KEY  TO CONGE-FIRME
           MOVE NEW-NUMBER TO CONGE-PERSON
           IF LNK-SQL = "Y" 
              CALL "9-CONGE" USING LINK-V CONGE-RECORD WR-KEY 
           END-IF.
           WRITE CONGE-RECORD INVALID REWRITE CONGE-RECORD END-WRITE.
           CLOSE CONGE.

       COPIE-FICHE.
           OPEN I-O FICHE.
           MOVE FR-KEY TO FICHE-FIRME.
           MOVE REG-PERSON TO FICHE-PERSON.
           PERFORM READ-FICHE THRU READ-FICHE-END.
       READ-FICHE.
           MOVE FR-KEY TO FICHE-FIRME.
           MOVE REG-PERSON TO FICHE-PERSON.
           START FICHE KEY > FICHE-KEY INVALID KEY 
                GO READ-FICHE-END.
           READ FICHE NEXT NO LOCK AT END 
                GO READ-FICHE-END.
           IF FR-KEY NOT = FICHE-FIRME
           OR REG-PERSON  NOT = FICHE-PERSON
                GO READ-FICHE-END.
           MOVE FIRME-KEY TO FICHE-FIRME.
           MOVE NEW-NUMBER TO FICHE-PERSON.
           IF LNK-SQL = "Y" 
              CALL "9-FICHE" USING LINK-V FICHE-RECORD WR-KEY 
           END-IF.
           WRITE FICHE-RECORD INVALID REWRITE FICHE-RECORD.
           GO READ-FICHE.
       READ-FICHE-END.
           CLOSE FICHE.

       COPIE-CONTRAT.
           OPEN I-O   CONTRAT.
           MOVE FR-KEY  TO CON-FIRME.
           MOVE REG-PERSON  TO CON-PERSON.
           PERFORM READ-CON THRU READ-CON-END.
       READ-CON.
           MOVE FR-KEY  TO CON-FIRME.
           MOVE REG-PERSON  TO CON-PERSON.
           START CONTRAT KEY > CON-KEY INVALID 
                GO READ-CON-END.
           READ CONTRAT NEXT AT END 
                GO READ-CON-END.
           IF FR-KEY NOT = CON-FIRME
           OR REG-PERSON  NOT = CON-PERSON
                GO READ-CON-END.
           MOVE FIRME-KEY  TO CON-FIRME CON-FIRME-F.
           MOVE NEW-NUMBER TO CON-PERSON CON-PERSON-F.
           MOVE MENU-PROG-NAME TO CON-USER.
           IF LNK-SQL = "Y" 
              CALL "9-CONTR" USING LINK-V CON-RECORD WR-KEY 
           END-IF.
           WRITE CON-RECORD INVALID REWRITE CON-RECORD.
           GO READ-CON.
       READ-CON-END.
           CLOSE CONTRAT.

       COPIE-HORSEM.
           OPEN I-O HORSEM.
           INITIALIZE HJS-RECORD.
           PERFORM READ-HS THRU READ-HS-END.
       READ-HS.
           MOVE FR-KEY  TO HJS-FIRME.
           MOVE REG-PERSON  TO HJS-PERSON.
           START HORSEM KEY > HJS-KEY INVALID GO READ-HS-END.
           READ HORSEM NEXT AT END 
                GO READ-HS-END.
           IF FR-KEY NOT = HJS-FIRME
           OR REG-PERSON NOT = HJS-PERSON
              GO READ-HS-END.
           MOVE FIRME-KEY TO HJS-FIRME.
           MOVE NEW-NUMBER TO HJS-PERSON.
           IF LNK-SQL = "Y" 
              CALL "9-HORSEM" USING LINK-V HJS-RECORD WR-KEY 
           END-IF.
           WRITE HJS-RECORD INVALID REWRITE HJS-RECORD.
           GO READ-HS.
       READ-HS-END.
           CLOSE HORSEM.
       COPIE-HEURES.
           OPEN I-O   HEURES.
           INITIALIZE HRS-RECORD.
           PERFORM READ-HRS THRU READ-HRS-END.
       READ-HRS.
           MOVE FR-KEY     TO HRS-FIRME.
           MOVE REG-PERSON TO HRS-PERSON.
           START HEURES KEY > HRS-KEY INVALID GO READ-HRS-END.
           READ HEURES NEXT AT END 
                GO READ-HRS-END.
           IF FR-KEY     NOT = HRS-FIRME
           OR REG-PERSON NOT = HRS-PERSON
              GO READ-HRS-END.
           MOVE FIRME-KEY TO  HRS-FIRME    HRS-FIRME-A 
                              HRS-FIRME-B  HRS-FIRME-C.
           MOVE NEW-NUMBER TO HRS-PERSON   HRS-PERSON-A 
                              HRS-PERSON-B HRS-PERSON-C.
           IF LNK-SQL = "Y" 
              CALL "9-HEURES" USING LINK-V HRS-RECORD WR-KEY 
           END-IF.
           WRITE HRS-RECORD INVALID REWRITE HRS-RECORD.
           GO READ-HRS.
       READ-HRS-END.
           CLOSE HEURES.


       COPIE-JOURS.
           OPEN I-O   JOURS.
           INITIALIZE JRS-RECORD.
           PERFORM READ-JRS THRU READ-JRS-END.
       READ-JRS.
           MOVE FR-KEY     TO JRS-FIRME-2.
           MOVE REG-PERSON TO JRS-PERSON-2.
           START JOURS KEY > JRS-KEY-2 INVALID GO READ-JRS-END.
           READ JOURS NEXT AT END 
                GO READ-JRS-END.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
              GO READ-JRS-END.
           MOVE FIRME-KEY TO  JRS-FIRME    JRS-FIRME-1
                              JRS-FIRME-2  JRS-FIRME-3.
           MOVE NEW-NUMBER TO JRS-PERSON   JRS-PERSON-1 
                              JRS-PERSON-2 JRS-PERSON-3.
           IF LNK-SQL = "Y" 
              CALL "9-JOURS" USING LINK-V JRS-RECORD WR-KEY 
           END-IF.
           WRITE JRS-RECORD INVALID REWRITE JRS-RECORD.
           IF  JRS-POSTE > 0
           AND JRS-COMPLEMENT = 0
              INITIALIZE PC-RECORD
              MOVE STORE TO FR-KEY
              MOVE JRS-POSTE TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              MOVE FIRME-KEY TO FR-KEY
              CALL "6-POCL" USING LINK-V PC-RECORD "N" WR-KEY
              MOVE STORE TO FR-KEY
           END-IF.
           GO READ-JRS.
       READ-JRS-END.
           CLOSE JOURS.


       COPIE-LIVRE.
           OPEN I-O   LIVRE.
           INITIALIZE L-RECORD.
           PERFORM READ-L THRU READ-L-END.
       READ-L.
           MOVE FR-KEY     TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON.
           START LIVRE KEY > L-KEY INVALID GO READ-L-END.
           READ LIVRE NEXT AT END 
                GO READ-L-END.
           IF FR-KEY     NOT = L-FIRME
           OR REG-PERSON NOT = L-PERSON
              GO READ-L-END.
           MOVE FIRME-KEY TO  L-FIRME  L-FIRME-1  L-FIRME-2.
           MOVE NEW-NUMBER TO L-PERSON L-PERSON-1 L-PERSON-2.
           IF LNK-SQL = "Y" 
              CALL "9-LIVRE" USING LINK-V L-RECORD WR-KEY 
           END-IF.
           WRITE L-RECORD INVALID REWRITE L-RECORD.
           GO READ-L.
       READ-L-END.
           CLOSE LIVRE.

       COPIE-VIREMENT.
           OPEN I-O   VIREMENT.
           INITIALIZE VIR-RECORD.
           PERFORM READ-VIR THRU READ-VIR-END.
       READ-VIR.
           MOVE FR-KEY     TO VIR-FIRME.
           MOVE REG-PERSON TO VIR-PERSON.
           START VIREMENT KEY > VIR-KEY INVALID GO READ-VIR-END.
           READ VIREMENT NEXT AT END 
                GO READ-VIR-END.
           IF FR-KEY     NOT = VIR-FIRME
           OR REG-PERSON NOT = VIR-PERSON
              GO READ-VIR-END.
           MOVE FIRME-KEY TO  VIR-FIRME    VIR-FIRME-S 
                              VIR-FIRME-B  VIR-FIRME-N.
           MOVE NEW-NUMBER TO VIR-PERSON   VIR-PERSON-S 
                              VIR-PERSON-B VIR-PERSON-N.
           IF LNK-SQL = "Y" 
              CALL "9-VIREM" USING LINK-V VIR-RECORD WR-KEY 
           END-IF.
           WRITE VIR-RECORD INVALID REWRITE VIR-RECORD.
           GO READ-VIR.
       READ-VIR-END.
           CLOSE VIREMENT.

       COPIE-FAMILLE.
           OPEN I-O   FAMILLE.
           INITIALIZE FAM-RECORD.
           MOVE FR-KEY     TO FAM-FIRME.
           MOVE REG-PERSON TO FAM-PERSON.
           READ FAMILLE INVALID CONTINUE
           NOT INVALID PERFORM WRITE-FAM.
           PERFORM READ-FAM THRU READ-FAM-END.
       READ-FAM.
           MOVE FR-KEY     TO FAM-FIRME.
           MOVE REG-PERSON TO FAM-PERSON.
           START FAMILLE KEY > FAM-KEY INVALID GO READ-FAM-END.
           READ FAMILLE NEXT AT END 
                GO READ-FAM-END.
           IF FR-KEY     NOT = FAM-FIRME
           OR REG-PERSON NOT = FAM-PERSON
              GO READ-FAM-END.
           MOVE FIRME-KEY TO  FAM-FIRME.
           MOVE NEW-NUMBER TO FAM-PERSON.
           PERFORM WRITE-FAM.
           GO READ-FAM.
       READ-FAM-END.
           CLOSE FAMILLE.

       WRITE-FAM.
           MOVE FIRME-KEY TO  FAM-FIRME.
           MOVE NEW-NUMBER TO FAM-PERSON.
           IF LNK-SQL = "Y" 
              CALL "9-FAMIL" USING LINK-V FAM-RECORD WR-KEY 
           END-IF.
           WRITE FAM-RECORD INVALID REWRITE FAM-RECORD.

       COPIE-CODPAIE.
           OPEN I-O   CODPAIE.
           INITIALIZE CSP-RECORD.
           PERFORM READ-CSP THRU READ-CSP-END.
       READ-CSP.
           MOVE FR-KEY     TO CSP-FIRME.
           MOVE REG-PERSON TO CSP-PERSON.
           START CODPAIE KEY > CSP-KEY INVALID GO READ-CSP-END.
           READ CODPAIE NEXT AT END 
                GO READ-CSP-END.
           IF FR-KEY     NOT = CSP-FIRME
           OR REG-PERSON NOT = CSP-PERSON
              GO READ-CSP-END.
           MOVE FIRME-KEY TO  CSP-FIRME  CSP-FIRME-2  CSP-FIRME-3.
           MOVE NEW-NUMBER TO CSP-PERSON CSP-PERSON-2 CSP-PERSON-3.
           IF LNK-SQL = "Y" 
              CALL "9-CODPAI" USING LINK-V CSP-RECORD WR-KEY 
           END-IF.
           WRITE CSP-RECORD INVALID REWRITE CSP-RECORD.
           GO READ-CSP.
       READ-CSP-END.
           CLOSE CODPAIE.


       DIS-HE-01.
           MOVE FIRME-KEY     TO HE-Z6.
           DISPLAY HE-Z6  LINE  4 POSITION 35.
           DISPLAY FIRME-NOM    LINE  4 POSITION 42 SIZE 30.
       DIS-HE-02.
           MOVE REG-PERSON    TO HE-Z6.
           DISPLAY HE-Z6  LINE  5 POSITION 35.
           DISPLAY PR-NOM    LINE  5 POSITION 42.
           DISPLAY PR-PRENOM LINE  6 POSITION 42.
       DIS-HE-03.
           DISPLAY REG-MATCHCODE LINE  6 POSITION 30.
       DIS-HE-04.
           MOVE NEW-NUMBER TO HE-Z6.
           DISPLAY HE-Z6  LINE  8 POSITION 35.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE  291 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           IF MENU-PROG-NUMBER = 1 
              DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE REGISTRE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

