      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 4-LPADD CUMUL LIVRE DE PAIE SIMPLE           �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-LPADD.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

           COPY "LIVRE.REC".
       01  IDX          PIC 9999 VALUE 0.
       01  EXC-KEY      PIC 9(4) COMP-1 VALUE 13.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "LIVRE.LNK".

       PROCEDURE DIVISION USING 
                          LINK-V 
                          REG-RECORD 
                          LINK-RECORD.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-LPADD.
       
           INITIALIZE L-RECORD.
           MOVE LNK-MOIS TO L-MOIS.
           MOVE LNK-VAL  TO LNK-SUITE.
           MOVE 13 TO EXC-KEY.
           PERFORM READ-LIVRE-ALL THRU READ-LIVRE-END.
           EXIT PROGRAM.


       READ-LIVRE-ALL.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD EXC-KEY.
           IF L-FIRME = 0
           OR L-MOIS NOT = LNK-MOIS
              GO READ-LIVRE-END.
           IF LNK-VAL NOT = L-SUITE
              GO READ-LIVRE-END.
           IF L-FLAG-AVANCE = 0
              PERFORM CUMUL.
           IF L-FLAG-AVANCE > 0
           AND MENU-PROG-NAME = "1-P"
              PERFORM CUMUL.
           MOVE 66 TO EXC-KEY.
           GO READ-LIVRE-ALL.
       READ-LIVRE-END.
           EXIT.

       CUMUL.
           PERFORM CUMUL-DC  VARYING IDX FROM 1 BY 1 UNTIL IDX > 300.
           PERFORM CUMUL-UN  VARYING IDX FROM 1 BY 1 UNTIL IDX > 200.
           PERFORM CUMUL-JRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 70.
           PERFORM CUMUL-COT VARYING IDX FROM 1 BY 1 UNTIL IDX > 70.
           PERFORM CUMUL-ABA VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           PERFORM CUMUL-IMP VARYING IDX FROM 1 BY 1 UNTIL IDX > 40.
           PERFORM CUMUL-SNO VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           PERFORM CUMUL-MOY VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.

       CUMUL-DC.
           ADD L-DC(IDX)        TO LINK-DC(IDX).
       CUMUL-UN.
           ADD L-UNITE(IDX)     TO LINK-UNITE(IDX).
       CUMUL-JRS.
           ADD L-JR-OCC(IDX)    TO LINK-JRS(IDX).
       CUMUL-COT.
           ADD L-COT(IDX)       TO LINK-COT(IDX).
       CUMUL-ABA.
           ADD L-ABAT(IDX)      TO LINK-ABAT(IDX).
       CUMUL-IMP.
           ADD L-IMPOS(IDX)     TO LINK-IMPOS(IDX).
       CUMUL-SNO.
           ADD L-SNOCS(IDX)     TO LINK-SNOCS(IDX).
       CUMUL-MOY.
           ADD L-MOY(IDX)       TO LINK-MOY(IDX).

      
