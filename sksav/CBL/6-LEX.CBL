      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-LEX MODULE GENERAL LECTURE LIVREX         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-LEX.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "LIVREX.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "LIVREX.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "LIVREX.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING  LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON LIVREX.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-LEX.
       
           IF NOT-OPEN = 0
              OPEN INPUT LIVREX
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO LEX-RECORD.
           MOVE LNK-LANGUAGE TO LEX-LANGUAGE.
           MOVE 0 TO LNK-VAL.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ LIVREX PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ LIVREX NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ LIVREX NO LOCK INVALID INITIALIZE LEX-REC-DET 
                      END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF LNK-LANGUAGE NOT = LEX-LANGUAGE
              GO EXIT-1
           END-IF.
           MOVE LEX-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START LIVREX KEY < LEX-KEY INVALID GO EXIT-1.
       START-2.
           START LIVREX KEY > LEX-KEY INVALID GO EXIT-1.


