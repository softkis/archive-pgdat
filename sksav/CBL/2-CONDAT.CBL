      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-CONDAT VUE DATES CONTRAT DU MOIS          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-CONDAT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CONTRAT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CONTRAT.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "STATUT.REC".
           COPY "COUT.REC".
           COPY "CONTYPE.REC".
           COPY "MOTDEP.REC".
           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 1.

       01  CHOIX                 PIC X VALUE SPACES.

       01  ECR-DISPLAY.
           02 HE-Z4 PIC ZZZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-TEMP PIC ZZ.ZZ.ZZZZ BLANK WHEN ZERO.
           02 HE-TEMP-R REDEFINES HE-TEMP.
              03 HE-JJ    PIC ZZ.
              03 FILLER   PIC X.
              03 HE-MM    PIC ZZ.
              03 FILLER   PIC X.
              03 HE-AA    PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.


       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CONTRAT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-2-CONDAT.

           PERFORM AFFICHAGE-ECRAN.
           OPEN I-O CONTRAT.

           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 

       AFFICHE-DEBUT.
           INITIALIZE CON-RECORD CHOIX.
           MOVE FR-KEY TO CON-FIRME.
           MOVE 4 TO LIN-IDX.
           START CONTRAT KEY > CON-KEY INVALID GO AFFICHE-END.
           PERFORM READ-CON THRU READ-CON-END.
           GO AFFICHE-DEBUT.
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-CON.
           READ CONTRAT NEXT NO LOCK AT END GO READ-CON-END END-READ.
           IF FR-KEY NOT = CON-FIRME 
              GO READ-CON-END.
           EVALUATE MENU-PROG-NUMBER 
           WHEN 0 IF LNK-ANNEE = CON-DEBUT-A
                  AND LNK-MOIS = CON-DEBUT-M
                     CONTINUE
                  ELSE
                     GO READ-CON
                  END-IF
           WHEN 1 IF LNK-ANNEE = CON-FIN-A
                  AND LNK-MOIS = CON-FIN-M
                     CONTINUE
                  ELSE
                     GO READ-CON
                  END-IF
           WHEN 2 IF LNK-ANNEE = CON-ESSAI-A
                  AND LNK-MOIS = CON-ESSAI-M
                     CONTINUE
                  ELSE
                     GO READ-CON
                  END-IF
           WHEN 3 IF LNK-ANNEE = CON-PREAVIS-A
                  AND LNK-MOIS = CON-PREAVIS-M
                     CONTINUE
                  ELSE
                     GO READ-CON
                  END-IF
           END-EVALUATE.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
           END-IF.
           GO READ-CON.
       READ-CON-END.
           MOVE 5 TO LNK-NUM.
           MOVE "AA" TO LNK-AREA.
           PERFORM DISPLAY-MESSAGE.
           PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21.
           PERFORM INTERRUPT THRU INTERRUPT-END.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           MOVE CON-PERSON TO REG-PERSON.
           ADD 1 TO LIN-IDX.
           MOVE REG-PERSON TO HE-Z6.
           DISPLAY HE-Z6  LINE LIN-IDX POSITION 1.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE LIN-IDX POSITION 8 SIZE 28.

           MOVE CON-DEBUT-A TO HE-AA.
           MOVE CON-DEBUT-M TO HE-MM.
           MOVE CON-DEBUT-J TO HE-JJ.
           DISPLAY HE-TEMP LINE LIN-IDX POSITION 37 LOW.
           IF MENU-PROG-NUMBER = 0
              DISPLAY HE-TEMP LINE LIN-IDX POSITION 37.
           MOVE CON-FIN-A TO HE-AA.
           MOVE CON-FIN-M TO HE-MM.
           MOVE CON-FIN-J TO HE-JJ.
           DISPLAY HE-TEMP LINE LIN-IDX POSITION 48 LOW.
           IF MENU-PROG-NUMBER = 1
              DISPLAY HE-TEMP LINE LIN-IDX POSITION 48.

           MOVE CON-ESSAI-A TO HE-AA.
           MOVE CON-ESSAI-M TO HE-MM.
           MOVE CON-ESSAI-J TO HE-JJ.
           DISPLAY HE-TEMP LINE LIN-IDX POSITION 59 LOW.
           IF MENU-PROG-NUMBER = 2
              DISPLAY HE-TEMP LINE LIN-IDX POSITION 59.

           MOVE CON-PREAVIS-A TO HE-AA.
           MOVE CON-PREAVIS-M TO HE-MM.
           MOVE CON-PREAVIS-J TO HE-JJ.
           DISPLAY HE-TEMP LINE LIN-IDX POSITION 70 LOW.
           IF MENU-PROG-NUMBER = 3
              DISPLAY HE-TEMP LINE LIN-IDX POSITION 70.

           ADD 1 TO LIN-IDX.
           MOVE CAR-STATUT TO STAT-CODE.
           CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY.
           DISPLAY STAT-NOM LINE LIN-IDX POSITION 1.
           MOVE CAR-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           DISPLAY COUT-NOM LINE LIN-IDX POSITION 10 SIZE 15.
           DISPLAY CAR-METIER LINE LIN-IDX POSITION 25.
           MOVE FR-KEY   TO COUT-FIRME.
           IF MENU-PROG-NUMBER = 0
              PERFORM ENTREE
           ELSE
              PERFORM DEPART.

       ENTREE.
           MOVE CON-TYPE TO CT-CODE.
           CALL "6-CONTYP" USING LINK-V CT-RECORD FAKE-KEY.
           DISPLAY CT-NOM LINE LIN-IDX POSITION 37 SIZE 40.

       DEPART.
           MOVE CON-MOTIF-DEPART TO MD-CODE.
           CALL "6-MOTDEP" USING LINK-V MD-RECORD FAKE-KEY.
           DISPLAY MD-NOM LINE LIN-IDX POSITION 48 SIZE 33.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           ACCEPT CHOIX 
           LINE 23 POSITION 30 SIZE 1
           TAB UPDATE NO BEEP CURSOR 1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82
              PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 4 TO LIN-IDX.

       AFFICHAGE-ECRAN.
           COMPUTE LNK-VAL = 2221 + MENU-PROG-NUMBER.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

