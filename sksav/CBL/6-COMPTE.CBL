      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-COMPTE MODULE GENERAL LECTURE COMPTES GEN �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-COMPTE.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "COMPTE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "COMPTE.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN  PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "COMPTE.LNK".

       01  EXC-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON COMPTE .
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-COMPTE.
       
           IF NOT-OPEN = 0
              OPEN INPUT COMPTE
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO COM-RECORD.
           
           EVALUATE EXC-KEY 
               WHEN 66 PERFORM START-1
               READ COMPTE NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN 65 PERFORM START-2
               READ COMPTE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN OTHER GO EXIT-3
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE COM-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-3.
           READ COMPTE NO LOCK INVALID GO EXIT-4.
           GO EXIT-2.

       EXIT-4.
           MOVE "F" TO COM-LANGUAGE.
           READ COMPTE NO LOCK INVALID INITIALIZE COM-REC-DET.
           GO EXIT-2.

       START-1.
           START COMPTE KEY > COM-KEY INVALID GO EXIT-1.
       START-2.
           START COMPTE KEY < COM-KEY INVALID GO EXIT-1.

