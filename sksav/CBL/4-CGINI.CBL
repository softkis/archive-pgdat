      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-CGINI INITIALISATION DES HEURES DE CONGE  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-CGINI.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".
           COPY "PRESENCE.REC".
           COPY "HORSEM.REC".
           COPY "V-VAR.CPY".

       01  ARRONDI PIC 9999.  

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "CONGE.REC".

       PROCEDURE DIVISION USING LINK-V 
                                REG-RECORD 
                                CONGE-RECORD.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-CGINI.
           MOVE LNK-MOIS TO SAVE-MOIS.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD FAKE-KEY.
           INITIALIZE CONGE-HRS SH-00.
           PERFORM PRESENCE.

           PERFORM ADD-UP VARYING LNK-MOIS FROM 1 BY 1 UNTIL LNK-MOIS 
           > 12.
           COMPUTE ARRONDI = SH-00 + ,5
           SUBTRACT ARRONDI FROM SH-00.
           MOVE ARRONDI TO CONGE-AN.
           PERFORM ROND VARYING IDX-1 FROM 12 BY -1 UNTIL IDX-1 = 0.

           COMPUTE CONGE-TOTAL = CONGE-IDX(1) + CONGE-IDX(11) + CONGE-AN.

           MOVE SAVE-MOIS TO LNK-MOIS.
           MOVE 99 TO SAVE-KEY.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD SAVE-KEY.
           CANCEL "6-CARRI".
           CANCEL "6-CONGE".
           CANCEL "6-GCCOL".
           CANCEL "6-GHJS".
           EXIT PROGRAM.

       ADD-UP.
           IF PRES-TOT(LNK-MOIS) > 15
              PERFORM CAR-RECENTE. 

       CAR-RECENTE.
           MOVE 0 TO SAVE-KEY.
           INITIALIZE CAR-RECORD CCOL-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           CALL "6-GHJS" USING LINK-V HJS-RECORD CAR-RECORD PRESENCES.
           IF CAR-CONGE = 7
           OR CAR-CONGE = 9 
              CONTINUE
           ELSE
              IF CAR-CONGE-INTERIM NOT = 0
              OR CCOL-INTERIM = 1
                 IF CAR-CONGE = 8
                    CONTINUE
                 END-IF
              ELSE
                 PERFORM CALCUL-CONGE
              END-IF
           END-IF.
           IF CAR-CONGE = 7
              INITIALIZE CONGE-HRS. 
       
       CALCUL-CONGE.
           IF CAR-JOURS-CONGE = 0
              MOVE CCOL-CONGE-STANDARD TO CAR-JOURS-CONGE.
           COMPUTE CONGE-MOIS(LNK-MOIS) = 
                   (CAR-JOURS-CONGE / 60 * HJS-HEURES(8)) + ,001.
           ADD CONGE-MOIS(LNK-MOIS) TO SH-00.

       ROND.
           IF CONGE-MOIS(IDX-1) > 0
              SUBTRACT SH-00 FROM CONGE-MOIS(IDX-1)
              MOVE 0 TO SH-00
           END-IF.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
