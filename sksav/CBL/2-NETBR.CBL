      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-NETBR CALCUL SALAIRE   NET - BRUT         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-NETBR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM80.FDE".

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

           COPY "REGIME.REC".
           COPY "LIVRE.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "CCOL.REC".
           COPY "FICHE.REC".
           COPY "LIVRE.CUM".
           COPY "IMPRLOG.REC".

       01  CHOIX-MAX             PIC 99 VALUE 11.
       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  SIZ-IDX               PIC 99 COMP-1.
       01  TAUX-IDX              PIC 9(4) COMP-1.
       01  HELP-VAL              PIC Z BLANK WHEN ZERO.
       01  HEURES                PIC 999 VALUE 173.
       01  SALAIRE-NET           PIC 9(6)V99 VALUE 0.
       01  HELP-1                PIC 9(6)V99.
       01  HELP-2                PIC 9(6)V99.
       01  HELP-3                PIC 9(6).
       01  HE-Z6Z2               PIC Z(6),99 BLANK WHEN ZERO.
       01  HE-Z3Z2               PIC Z(3),99.
       01  HE-Z4Z2               PIC Z(4),99.
       01  HE-Z2                 PIC ZZ.
       01  HE-Z3                 PIC ZZZ.
       01  NB-NET                PIC 9(6)V99.
       01  NB-BRUT               PIC 9(6)V99.
       01  NATURE                PIC 9(6)V99.
       01  HAUT                  pic 9(8)V99.
       01  BAS                   pic 9(8)V99.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X    VALUE "F".
           02  FORM-EXTENSION    PIC X(4) VALUE ".NB".

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-NETBR.
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE 0 TO LNK-VAL LNK-NUM.
           CALL "6-GCCOL" USING LINK-V CCOL-RECORD.
           INITIALIZE FICHE-RECORD L-RECORD CAR-RECORD.
           MOVE 1 TO CAR-STATUT CAR-REGIME FICHE-CLASSE-A.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 11    MOVE 0000000007 TO EXC-KFR (1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-10
           WHEN 11 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4
           WHEN  5 PERFORM APRES-5
           WHEN  8 PERFORM APRES-8
           WHEN  9 PERFORM APRES-9
           WHEN 10 PERFORM APRES-10
           WHEN 11 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT CAR-REGIME
             LINE  4 POSITION 28 SIZE  3
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
             IF FICHE-CLASSE-A = 0
                MOVE 1 TO FICHE-CLASSE.
             ACCEPT FICHE-CLASSE-A
             LINE  5 POSITION 30 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
             IF LNK-ANNEE < 2008
             ACCEPT FICHE-CLASSE-B
             LINE  5 POSITION 48 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4.
             IF FICHE-CLASSE-A = 1 AND FICHE-CLASSE-B = 0
             ACCEPT FICHE-GROUPE
             LINE  5 POSITION 66 SIZE 1
             TAB UPDATE NO BEEP CONTROL "UPPER"
             CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-5.            
           MOVE SALAIRE-NET TO HE-Z6Z2.
           ACCEPT HE-Z6Z2
             LINE 6 POSITION 22 SIZE 9
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z6Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z6Z2 TO SALAIRE-NET.
           MOVE SALAIRE-NET TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE 6 POSITION 22.

       AVANT-6.            
           MOVE NATURE TO HE-Z4Z2.
           ACCEPT HE-Z4Z2
             LINE 6 POSITION 60 SIZE  7
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z4Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z4Z2 TO NATURE.
           MOVE NATURE TO HE-Z4Z2.
           DISPLAY HE-Z4Z2 LINE 6 POSITION 60.

       AVANT-7.
           MOVE FICHE-ABAT-FD TO HE-Z4Z2.
           ACCEPT HE-Z4Z2
             LINE 7 POSITION 24 SIZE  7
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z4Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z4Z2 TO FICHE-ABAT-FD.
           MOVE FICHE-ABAT-FD TO HE-Z4Z2.
           DISPLAY HE-Z4Z2 LINE 7 POSITION 24.

       AVANT-8.
           ACCEPT FICHE-TAUX
             LINE 7 POSITION 48 SIZE  2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-9.
           ACCEPT HEURES    
             LINE 7 POSITION 70 SIZE  3
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-10.
           IF FICHE-TAUX = 0
           ACCEPT FICHE-IMPOSABLE-YN
             LINE 8 POSITION 24 SIZE  1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-1.
           MOVE CAR-REGIME TO REGIME-NUMBER.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-REGIME
                    MOVE REGIME-NUMBER TO CAR-REGIME
           WHEN   5 CALL "1-REGIM" USING LINK-V
                    MOVE LNK-VAL TO CAR-REGIME
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "1-REGIM" 
           WHEN   2 CALL "2-REGIME" USING LINK-V REGIME-RECORD
                    MOVE REGIME-NUMBER TO CAR-REGIME
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "2-REGIM" 
           END-EVALUATE.
           PERFORM DIS-HE-01.
           IF REGIME-NOM = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           
       APRES-2.
            IF FICHE-CLASSE-A < 1 
               MOVE 1 TO FICHE-CLASSE-A INPUT-ERROR.
            IF FICHE-CLASSE-A > 2 
               MOVE 2 TO FICHE-CLASSE-A 
               MOVE 1 TO INPUT-ERROR.
       APRES-3.
           IF FICHE-CLASSE-A = 1 
           IF FICHE-CLASSE-B NOT = 0
              MOVE "A" TO FICHE-GROUPE.
           IF FICHE-CLASSE-A > 1 
              MOVE " " TO FICHE-GROUPE.
           DISPLAY FICHE-GROUPE  LINE  5 POSITION 66.

       APRES-4.
            IF FICHE-CLASSE-A > 1 
               MOVE " " TO FICHE-GROUPE.
            IF FICHE-CLASSE-A = 1 
            IF FICHE-CLASSE-B NOT = 0
               MOVE "A" TO FICHE-GROUPE.
            IF FICHE-GROUPE NOT = "A" 
            AND FICHE-GROUPE NOT = " " 
               MOVE " " TO FICHE-GROUPE
               MOVE 1 TO INPUT-ERROR.
           DISPLAY FICHE-GROUPE  LINE  5 POSITION 66.

       APRES-5.
           IF SALAIRE-NET = 0 
              MOVE 1 TO INPUT-ERROR
           END-IF.
           
       APRES-8.
           IF FICHE-TAUX > 38 
              MOVE 38 TO FICHE-TAUX
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF FICHE-TAUX > 0
              MOVE SPACES TO FICHE-IMPOSABLE-YN
           END-IF.
           DISPLAY FICHE-IMPOSABLE-YN LINE 8 POSITION 24.

       APRES-9.
           IF HEURES > 173
              MOVE 173 TO HEURES
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-10.
           IF FICHE-TAUX > 0
              MOVE SPACES TO FICHE-IMPOSABLE-YN
           END-IF.
           IF FICHE-IMPOSABLE-YN = "N"
           OR FICHE-IMPOSABLE-YN = " "
              CONTINUE
           ELSE
              MOVE SPACES TO FICHE-IMPOSABLE-YN
              MOVE 1 TO INPUT-ERROR
           END-IF.
           MOVE SALAIRE-NET TO NB-NET BAS.
           COMPUTE HAUT = NB-NET * 3 + NATURE.
           MOVE HAUT TO HELP-1.
           INITIALIZE PRESENCES LCUM-RECORD.
           PERFORM FILL-JOUR VARYING IDX FROM 1 BY 1 UNTIL IDX > 
           MOIS-JRS(LNK-MOIS).
           PERFORM APPROCHE.
           PERFORM DIS-HE-04.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN.
           IF EXC-KEY = 5 OR 6
              PERFORM EDITION
              MOVE 1 TO DECISION
           END-IF.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-REGIME.
           CALL "6-REGIME" USING LINK-V REGIME-RECORD EXC-KEY.

       DIS-HE-01.
           MOVE  CAR-REGIME  TO HE-Z3.
           DISPLAY HE-Z3 LINE  4 POSITION 28.
           MOVE CAR-REGIME TO REGIME-NUMBER.
           CALL "6-REGIME" USING LINK-V REGIME-RECORD FAKE-KEY.
           DISPLAY REGIME-NOM LINE  4 POSITION 40 SIZE 20.
       DIS-HE-02.
           DISPLAY FICHE-CLASSE LINE  5 POSITION 30.

       DIS-HE-04.
           SUBTRACT NATURE FROM L-IMPOSABLE-BRUT.
           MOVE  L-IMPOSABLE-BRUT  TO HE-Z6Z2 LNK-VAL.
           DISPLAY HE-Z6Z2  LINE  9 POSITION 32.
           MOVE  L-ABATTEMENT         TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 18 POSITION 32.
           MOVE  L-IMPOSABLE-NET        TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 19 POSITION 32.
           MOVE  L-IMPOT           TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 20 POSITION 32.
           MOVE  L-NET              TO HE-Z6Z2.
           MOVE  L-A-PAYER          TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 21 POSITION 32.
           MOVE  L-COT-S-MAL     TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 11 POSITION 50.
           MOVE  L-COT-S-PEN     TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 12 POSITION 50.
           MOVE  L-COT-S-FAM     TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 13 POSITION 50.
           MOVE  L-COT-S-ACC     TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 14 POSITION 50.
           MOVE  L-COT-S-DEP     TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 15 POSITION 50.
           MOVE  L-COT-S-AAA     TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 16 POSITION 50.
           MOVE  L-COT-S-BBB     TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 17 POSITION 50.

           MOVE  L-CAISSE-MAL-PAT    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 11 POSITION 63.
           MOVE  L-CAISSE-PEN-PAT    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 12 POSITION 63.
           MOVE  L-CAISSE-FAM-PAT    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 13 POSITION 63.
           MOVE  L-CAISSE-ACC-PAT    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 14 POSITION 63.
           MOVE  L-CAISSE-SAN-PAT    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 15 POSITION 63.
           MOVE  L-CAISSE-ESP-PAT    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 16 POSITION 63.
           MOVE  L-CAISSE-MUT-PAT    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 17 POSITION 63.

           MOVE  L-CAISSE-MAL-SAL    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 11 POSITION 32.
           MOVE  L-CAISSE-PEN-SAL    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 12 POSITION 32.
           MOVE  L-CAISSE-FAM-SAL    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 13 POSITION 32.
           MOVE  L-CAISSE-ACC-SAL    TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 14 POSITION 32.
           MOVE  L-CAISSE-DEP-SAL TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 15 POSITION 32.
           MOVE  L-CAISSE-ESP-SAL TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 16 POSITION 32.
           MOVE  L-CAISSE-MUT-SAL TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 17 POSITION 32.

           COMPUTE HELP-1 = L-IMPOSABLE-BRUT 
                          + NATURE
                          + L-COT-PP(1)
                          + L-COT-PP(2)
                          + L-COT-PP(3)
                          + L-COT-PP(4)
                          + L-COT-PP(5)
                          + L-COT-PP(6)
                          + L-COT-PP(7)
                          + L-COT-PP(8)
                          + L-COT-PP(9)
                          + L-COT-PP(10).
           MOVE  HELP-1 TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE 21 POSITION 63.
       DIS-HE-END.
           EXIT.

       APPROCHE.    
           PERFORM CALCUL.
           IF L-A-PAYER > NB-NET
              MOVE HELP-1 TO HAUT
           ELSE
              MOVE HELP-1 TO BAS
           END-IF.
           IF L-A-PAYER NOT = NB-NET
              COMPUTE HELP-1 = (HAUT + BAS) / 2
              GO APPROCHE.
       CALCUL.
           INITIALIZE L-RECORD.
           MOVE LNK-MOIS TO L-MOIS.
           MOVE 1 TO L-JOUR-DEBUT.
           MOVE MOIS-JRS(LNK-MOIS) TO L-JOUR-FIN.
           MOVE NATURE TO L-NATURE.
           MOVE HEURES TO L-SNOCS-HEURES-OUVREES.
           MOVE HELP-1 TO
                L-IMPOSABLE-BRUT
                L-DC(100)
                L-COT-BRUT-MAL
                L-COT-BRUT-PEN
                L-COT-BRUT-FAM
                L-COT-BRUT-ACC
                L-COT-BRUT-SAN
                L-COT-BRUT-DEP
                L-COT-BRUT-AAA
                L-COT-BRUT-BBB
                L-COT-BRUT-CCC
                L-COT-BRUT-DDD
                L-SNOCS-SALAIRE.
           CALL "4-CALCUL" USING LINK-V
                                REG-RECORD
                                CAR-RECORD
                                FICHE-RECORD
                                CCOL-RECORD
                                PRESENCES
                                L-RECORD
                                LCUM-RECORD.

       FILL-JOUR.
           ADD 1 TO PRES-JOUR(LNK-MOIS, IDX) PRES-TOT(LNK-MOIS)
            PRES-ANNEE.
           IF SEM-IDX(LNK-MOIS, IDX) = 7 
               ADD 1 TO PRES-DIM(LNK-MOIS).

       AFFICHAGE-ECRAN.
           MOVE 2900 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".


       EDITION.
           CALL "0-TODAY" USING TODAY.
           PERFORM READ-IMPRIMANTE.
           PERFORM READ-FORM.
           PERFORM FILL-FILES.
           PERFORM TRANSMET.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.

       FILL-FILES.

           MOVE REGIME-NOM TO ALPHA-TEXTE.
           MOVE  8 TO LIN-NUM.
           MOVE 30 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FICHE-CLASSE-A TO VH-00.
           MOVE  1 TO CAR-NUM.
           MOVE 10 TO LIN-NUM.
           MOVE 30 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD  1 TO COL-NUM.
           MOVE FICHE-GROUPE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE FICHE-CLASSE-B TO VH-00.
           ADD   1 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           PERFORM FILL-FORM.

           MOVE FICHE-TAUX TO VH-00. 
           MOVE  2 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 14 TO LIN-NUM.
           MOVE 30 TO COL-NUM.
           PERFORM FILL-FORM.
           IF FICHE-TAUX = 0
           AND FICHE-IMPOSABLE-YN = "N"
              MOVE "0" TO ALPHA-TEXTE
              PERFORM FILL-FORM.

           MOVE  L-ABATTEMENT TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 12 TO LIN-NUM.
           MOVE 28 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 36 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE L-IMPOSABLE-BRUT TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 18 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE NATURE TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 18 TO LIN-NUM.
           MOVE 63 TO COL-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = L-COT-SAL(6)
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 32 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.
           COMPUTE VH-00 = L-COT-SAL(7)
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 34 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.


           COMPUTE VH-00 = L-COT-PP(6)
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 32 TO LIN-NUM.
           MOVE 63 TO COL-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = L-COT-PP(7)
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 34 TO LIN-NUM.
           MOVE 63 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE  L-IMPOSABLE-NET TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 38 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-IMPOT TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 40 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-A-PAYER TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 43 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE  L-COT-S-MAL     TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 22 TO LIN-NUM.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-COT-S-PEN     TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 24 TO LIN-NUM.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-COT-S-FAM     TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 26 TO LIN-NUM.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-COT-S-ACC     TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 28 TO LIN-NUM.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-COT-S-DEP     TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 30 TO LIN-NUM.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-COT-S-AAA     TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 32 TO LIN-NUM.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-COT-S-BBB     TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 34 TO LIN-NUM.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE  L-CAISSE-MAL-PAT    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 22 TO LIN-NUM.
           MOVE 63 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-CAISSE-PEN-PAT    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 24 TO LIN-NUM.
           MOVE 63 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-CAISSE-FAM-PAT    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 26 TO LIN-NUM.
           MOVE 63 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-CAISSE-ACC-PAT    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 28 TO LIN-NUM.
           MOVE 63 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-CAISSE-SAN-PAT    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 30 TO LIN-NUM.
           MOVE 63 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-CAISSE-ESP-PAT    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 32 TO LIN-NUM.
           MOVE 63 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-CAISSE-MUT-PAT    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 34 TO LIN-NUM.
           MOVE 63 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE  L-CAISSE-MAL-SAL    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 22 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-CAISSE-PEN-SAL    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 24 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-CAISSE-DEP-SAL    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 30 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-CAISSE-ESP-SAL    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 32 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE  L-CAISSE-MUT-SAL    TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 34 TO LIN-NUM.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.


           MOVE  HELP-1  TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 43 TO LIN-NUM.
           MOVE 63 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 3  TO LIN-NUM.
           MOVE 65 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 68 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 71 TO COL-NUM.
           MOVE TODAY-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE  5 TO LIN-NUM.
           MOVE 30 TO COL-NUM.
           PERFORM MOIS-NOM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE LNK-ANNEE TO VH-00.
           MOVE  4 TO CAR-NUM.
           PERFORM FILL-FORM.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           MOVE 99 TO LNK-VAL
           CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           MOVE " " TO LNK-YN.
