      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 7-CSFMOD MODIFICATION CODE SALAIRE FIXE     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    7-CSFMOD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODFIX.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CODFIX.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 14.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "FIRME.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "CONTRAT.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
SU         COPY "CODTXT.REC".
           COPY "CODPAIE.REC".
           COPY "STATUT.REC".
           COPY "COUT.REC".


           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  MODIFICATION          PIC 9 VALUE 0.
       01  VAL-INIT              PIC 9(5)V99  VALUE 0.
       01  VAL-MODIF             PIC S9(5)V99 VALUE 0.
       01  VAL-PCT               PIC S9(4)V999. 
       01  SAL-ACT               PIC S9(8)V99.
       01  TEST-KEY              PIC 9(4) COMP-1.

       01  DATES.
           02 ANNEE-D PIC 9999 VALUE 0.
           02 ANNEE-F PIC 9999 VALUE 0.
           02 MOIS-D PIC 99 VALUE 0.
           02 MOIS-F PIC 99 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z2Z2 PIC ZZ,ZZ. 
           02 HE-Z5Z2 PIC -Z(5),ZZ. 
           02 HE-Z4Z3 PIC -Z(3),ZZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CODFIX.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-7-CSFMOD-.
       
           PERFORM AFFICHAGE-ECRAN .
           OPEN I-O CODFIX.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 14    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052080000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-10
           WHEN 11 PERFORM AVANT-11
           WHEN 12 PERFORM AVANT-12
           WHEN 13 PERFORM AVANT-13
           WHEN 14 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN 10 PERFORM APRES-10
           WHEN 11 PERFORM APRES-11
           WHEN 12 PERFORM APRES-12
           WHEN 13 PERFORM APRES-13
           WHEN 14 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-6.
           ACCEPT CD-NUMBER
             LINE 13 POSITION 29 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           MOVE VAL-INIT  TO HE-Z5Z2.
           ACCEPT HE-Z5Z2
             LINE 15 POSITION 30 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z5Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z5Z2 TO VAL-INIT.
           PERFORM DIS-HE-07.

       AVANT-8.
           MOVE VAL-MODIF TO HE-Z5Z2.
           ACCEPT HE-Z5Z2
             LINE 16 POSITION 30 SIZE 9
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z5Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z5Z2 TO VAL-MODIF.
           PERFORM DIS-HE-08.

       AVANT-9.
           MOVE VAL-PCT TO HE-Z4Z3.
           ACCEPT HE-Z4Z3
             LINE 17 POSITION 30 SIZE 9
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z4Z3 REPLACING ALL "." BY ",".
           MOVE HE-Z4Z3 TO VAL-PCT.

           PERFORM DIS-HE-09.


       AVANT-10.           
           ACCEPT ANNEE-D
             LINE 19 POSITION 14 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-11.           
           IF ANNEE-D > 0
           ACCEPT MOIS-D 
             LINE 20 POSITION 16 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-12.           
           ACCEPT ANNEE-F
             LINE 19 POSITION 44 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-13.
           IF ANNEE-F > 0
           ACCEPT MOIS-F
             LINE 20 POSITION 46 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE
             MOVE 0 TO MOIS-F
           END-IF.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           EVALUATE EXC-KEY
           WHEN 2 CALL "2-CSDEF" USING LINK-V CD-RECORD
                  IF CD-NUMBER > 0
                     PERFORM CS-DATA 
                  END-IF
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DISPLAY-F-KEYS
           WHEN OTHER PERFORM NEXT-CS
           END-EVALUATE.
           IF CTX-NOM = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-10.
           COMPUTE IDX-4 = LNK-ANNEE + 20.
           IF ANNEE-D  NOT = 0
              IF ANNEE-D  > IDX-4
                 MOVE 1 TO INPUT-ERROR 
                 MOVE IDX-4 TO ANNEE-D
              END-IF
              IF ANNEE-D  < LNK-ANNEE
                 MOVE 1 TO INPUT-ERROR 
                 MOVE LNK-ANNEE TO ANNEE-D
              END-IF
           END-IF.

       APRES-11.
           IF ANNEE-D = 0 
              MOVE 0 TO MOIS-D.
           IF ANNEE-D NOT = 0
           IF MOIS-D > 12
              MOVE  1 TO INPUT-ERROR 
              MOVE 12 TO MOIS-D. 
           IF ANNEE-D  NOT = 0
           IF MOIS-D  < 1
              MOVE 1 TO INPUT-ERROR 
              MOVE 1 TO MOIS-D.
              PERFORM DIS-HE-11.

       APRES-12.
           IF ANNEE-F NOT = 0
              IF  ANNEE-D NOT = 0
              AND ANNEE-F < ANNEE-D
                 MOVE 1 TO INPUT-ERROR 
                 MOVE ANNEE-D TO ANNEE-F 
              END-IF
              IF ANNEE-F < LNK-ANNEE
                 MOVE 1 TO INPUT-ERROR 
                 MOVE LNK-ANNEE TO ANNEE-F 
              END-IF
           END-IF.

       APRES-13.
           IF ANNEE-F = 0 
              MOVE 0 TO MOIS-F.
           IF ANNEE-F NOT = 0
           IF MOIS-F > 12
              MOVE  1 TO INPUT-ERROR 
              MOVE 12 TO MOIS-F. 
           IF ANNEE-F NOT = 0
           IF MOIS-F < 1
              MOVE 1 TO INPUT-ERROR 
              MOVE LNK-MOIS TO MOIS-F.
           IF ANNEE-F = ANNEE-D
           IF MOIS-F  < MOIS-D
              MOVE 1 TO INPUT-ERROR 
              MOVE MOIS-D TO MOIS-F.
           PERFORM DIS-HE-13.

       NEXT-CS.
           CALL "6-CSDEF" USING LINK-V CD-RECORD EXC-KEY.
           PERFORM CS-DATA.

       CS-DATA.
           INITIALIZE CS-RECORD.
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
SU         CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.

       APRES-DEC.
           MOVE EXC-KEY TO TEST-KEY.
           EVALUATE EXC-KEY 
           WHEN 5 THRU 8 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-1
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           INITIALIZE CSF-RECORD MODIFICATION.
           MOVE FR-KEY     TO CSF-FIRME.
           MOVE REG-PERSON TO CSF-PERSON.
           MOVE CD-NUMBER  TO CSF-CODE.
           READ CODFIX NO LOCK INVALID 
              GO READ-PERSON-1
           END-READ.
           IF TEST-KEY = 8
              DELETE CODFIX INVALID CONTINUE
              NOT INVALID MOVE 1 TO MODIFICATION END-DELETE
              IF LNK-SQL = "Y" 
                 CALL "9-CODFIX" USING LINK-V CSF-RECORD DEL-KEY 
              END-IF
              GO READ-PERSON-1
           END-IF.
           PERFORM MODIF-SIMPLE.

       READ-PERSON-1.
           IF MODIFICATION = 1
              PERFORM DIS-HE-01
              MOVE 0 TO LNK-SUITE
              CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES
           END-IF.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.


       MODIF-SIMPLE.
           IF VAL-INIT = 0
           OR VAL-INIT = CSF-TOTAL
              IF VAL-PCT NOT = 0
                 COMPUTE SAL-ACT = CSF-TOTAL * 
                 (100 + VAL-PCT) / 100 + ,005
                 IF SAL-ACT < 0 MOVE 0 TO SAL-ACT END-IF
                 MOVE SAL-ACT TO CSF-TOTAL
              END-IF
              IF VAL-MODIF NOT = 0
                 COMPUTE SAL-ACT = VAL-MODIF + CSF-TOTAL
                 IF SAL-ACT < 0 MOVE 0 TO SAL-ACT END-IF
                 MOVE SAL-ACT TO CSF-TOTAL
              END-IF
           END-IF.
           IF CSF-TOTAL > 0
              IF CSF-UNITE > 0
                 COMPUTE CSF-UNITAIRE = CSF-TOTAL / CSF-UNITE
              END-IF
              IF ANNEE-D > 0
                 MOVE ANNEE-D TO CSF-ANNEE-D
                 MOVE MOIS-D  TO CSF-MOIS-D
              END-IF
              IF ANNEE-F > 0
                 MOVE ANNEE-F TO CSF-ANNEE-F
                 MOVE MOIS-F  TO CSF-MOIS-F
              END-IF
              IF LNK-SQL = "Y" 
                 CALL "9-CODFIX" USING LINK-V CSF-RECORD WR-KEY 
              END-IF
              REWRITE CSF-RECORD INVALID CONTINUE END-REWRITE 
           ELSE
              IF LNK-SQL = "Y" 
                 CALL "9-CODFIX" USING LINK-V CSF-RECORD DEL-KEY 
              END-IF
              DELETE CODFIX INVALID CONTINUE
           END-IF.
           MOVE 1 TO MODIFICATION.

 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE CD-NUMBER TO HE-Z4.
           DISPLAY HE-Z4  LINE 13 POSITION 29.
           DISPLAY CTX-NOM LINE 13 POSITION 35.
       DIS-HE-07.
           MOVE VAL-INIT  TO HE-Z5Z2.
           DISPLAY HE-Z5Z2  LINE 15 POSITION 30.
       DIS-HE-08.
           MOVE VAL-MODIF TO HE-Z5Z2.
           DISPLAY HE-Z5Z2 LINE 16 POSITION 30.
       DIS-HE-09.
           MOVE VAL-PCT TO HE-Z4Z3.
           DISPLAY HE-Z4Z3 LINE 17 POSITION 30.
       DIS-HE-10.
           MOVE ANNEE-D    TO HE-Z4.
           DISPLAY HE-Z4  LINE 19 POSITION 14.
       DIS-HE-11.
           MOVE MOIS-D    TO HE-Z4.
           DISPLAY HE-Z4  LINE 20 POSITION 14.
       DIS-HE-12.
           MOVE ANNEE-F    TO HE-Z4.
           DISPLAY HE-Z4  LINE 19 POSITION 44.
       DIS-HE-13.
           MOVE MOIS-F    TO HE-Z4.
           DISPLAY HE-Z4  LINE 20 POSITION 44.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 212 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A SAVE-ANNEE.
           MOVE LNK-MOIS  TO CON-DEBUT-M SAVE-MOIS.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE CODFIX.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
