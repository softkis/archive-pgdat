      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-MEMO MEMOS                                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-MEMO.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MEMO.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "MEMO.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
             
       01  ECRAN-SUITE.
           02 ECR-S1             PIC 999 VALUE 221.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S              PIC 999 OCCURS 1.

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE 11.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CONTRAT.REC".
           COPY "V-VAR.CPY".
       01  ACTION-CALL           PIC 9 VALUE 0.

       77  HELP-1                PIC 9(6).

       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-Z2 PIC ZZ BLANK WHEN ZERO.
           02 HE-Z2Z2 PIC ZZ,ZZ. 
           02 HE-Z3Z2 PIC ZZZ,ZZ. 
           02 HE-Z4Z2 PIC ZZZZ,ZZ. 
           02 HE-Z6 PIC Z(6). 
           02 HE-Z6Z2 PIC Z(6),ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
      *袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴�

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MEMO. 
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-MEMO.

           OPEN I-O   MEMO .

           CANCEL "1-GMEMO".
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

      *    IF ECRAN-IDX = 1
      *       MOVE 0000680000 TO EXC-KFR (14) 
      *    ELSE
      *       MOVE 0067000000 TO EXC-KFR (14) 
      *    END-IF.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 4
                      MOVE 0017000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 11    MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
           END-EVALUATE 
           END-IF.


       TRAITEMENT-ECRAN-01.

           PERFORM DISPLAY-F-KEYS.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1-1 
           WHEN  2 PERFORM AVANT-1-2 
           WHEN  3 PERFORM AVANT-1-3 
           WHEN  4 PERFORM AVANT-1-4 
           WHEN  5 THRU 9 PERFORM AVANT-1-ALL
           WHEN 10 PERFORM AVANT-1-10 
           WHEN CHOIX-MAX(ECRAN-IDX) PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF EXC-KEY > 66 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              IF ECRAN-IDX = 1
                 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
              ELSE
                 MOVE 1 TO INDICE-ZONE
              END-IF
              GO TRAITEMENT-ECRAN
           END-IF.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-1 
           WHEN  2 PERFORM APRES-1-2 
           WHEN  3 PERFORM APRES-1-3 
           WHEN  4 PERFORM APRES-1-4 
           WHEN 10 PERFORM APRES-YN 
           WHEN CHOIX-MAX(ECRAN-IDX) PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-1-1.
           IF REG-PERSON = 0
              INITIALIZE MEM-RECORD.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.            
           IF MEM-ANNEE = 0 
              IF CON-DEBUT-A < LNK-ANNEE
                 MOVE LNK-ANNEE TO MEM-ANNEE
                 MOVE 1 TO MEM-MOIS
              ELSE
                 MOVE CON-DEBUT-A TO MEM-ANNEE
                 MOVE CON-DEBUT-M TO MEM-MOIS
              END-IF
           END-IF.
           ACCEPT MEM-ANNEE
             LINE  4 POSITION 32 SIZE  4
             TAB UPDATE NO BEEP CURSOR 1 
             ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-1-4. 
           ACCEPT MEM-MOIS  
             LINE  5 POSITION 34 SIZE 2 
             TAB UPDATE NO BEEP CURSOR 1 
             ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-1-ALL.         
           COMPUTE IDX-1 = INDICE-ZONE - 4.
           COMPUTE LIN-IDX = IDX-1 + 6.
           ACCEPT MEM-TEXTE(IDX-1)
             LINE LIN-IDX POSITION 30 SIZE 50
             TAB UPDATE NO BEEP CURSOR 1 
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-10.
           ACCEPT MEM-FICHE
             LINE 13 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  OTHER PERFORM NEXT-REGIS.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM CONTRAT-RECENT.
           PERFORM MEMO-RECENTE.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-1-2.
           MOVE "A" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN 65 THRU 66 PERFORM NEXT-REGIS.
           PERFORM DIS-E1-01.
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF REG-PERSON = 0 
              MOVE 1 TO INPUT-ERROR
           ELSE
              PERFORM CONTRAT-RECENT
              PERFORM AFFICHAGE-DETAIL
           END-IF.
           
       APRES-1-3.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM MEMO-NEXT
                   PERFORM AFFICHAGE-DETAIL
                   MOVE 1 TO INPUT-ERROR
           WHEN  2 CALL "2-MEMO" USING LINK-V MEM-RECORD
                   PR-RECORD REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           END-EVALUATE.
           IF CON-DEBUT-A > MEM-ANNEE
              MOVE 1 TO INPUT-ERROR
              MOVE CON-DEBUT-A TO MEM-ANNEE.

       APRES-1-4.
           EVALUATE EXC-KEY
           WHEN 65 PERFORM MEMO-NEXT
                   MOVE 1 TO INPUT-ERROR
           WHEN 66 PERFORM MEMO-NEXT
                   MOVE 1 TO INPUT-ERROR
           WHEN  2 CALL "2-MEMO" USING LINK-V MEM-RECORD
                   PR-RECORD REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           END-EVALUATE.
           IF CON-DEBUT-A = MEM-ANNEE
           IF CON-DEBUT-M > MEM-MOIS 
              MOVE 1 TO INPUT-ERROR
              MOVE CON-DEBUT-M TO MEM-MOIS.
           IF MEM-MOIS = 0
              MOVE LNK-MOIS TO MEM-MOIS
              MOVE 1 TO INPUT-ERROR.

           IF MEM-MOIS > 12
              MOVE 12 TO MEM-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF MEM-MOIS < 1
              MOVE 1 TO MEM-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0
              READ MEMO INVALID INITIALIZE MEM-REC-DET END-READ
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-YN.
           IF MEM-FICHE = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE SPACES TO MEM-FICHE 
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
            MOVE FR-KEY TO MEM-FIRME.
            MOVE REG-PERSON TO MEM-PERSON.
            EVALUATE EXC-KEY 
            WHEN  5 CALL "0-TODAY" USING TODAY
                   MOVE TODAY-TIME TO MEM-TIME
                   MOVE LNK-USER TO MEM-USER
                   WRITE MEM-RECORD INVALID REWRITE MEM-RECORD
                   END-WRITE   
                   MOVE 1 TO DECISION 
            WHEN  8 DELETE MEMO INVALID CONTINUE END-DELETE
                    INITIALIZE MEM-RECORD
                    MOVE 1 TO DECISION 
            WHEN 52 COMPUTE DECISION = CHOIX-MAX(ECRAN-IDX) + 1.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       CONTRAT-RECENT.
           INITIALIZE CON-RECORD.
           CALL "1-GCONTR" USING LINK-V REG-RECORD CON-RECORD.

       MEMO-RECENTE.
           MOVE 999999 TO MEM-DATE.
           MOVE EXC-KEY TO SAVE-KEY.
           MOVE 65 TO EXC-KEY.
           PERFORM MEMO-NEXT.
           MOVE SAVE-KEY TO EXC-KEY.

       MEMO-NEXT.
           CALL "6-MEMO" USING LINK-V REG-RECORD MEM-RECORD EXC-KEY.
           MOVE FR-KEY TO MEM-FIRME.
           MOVE REG-PERSON TO MEM-PERSON.
           IF MEM-DATE = 999999 
              MOVE 0 TO MEM-DATE.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
                
       ENTREE-SORTIE.
           MOVE "AA" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           PERFORM DISPLAY-MESSAGE.
           MOVE 0 TO LNK-NUM.
           
       DIS-E1-01.
           MOVE REG-PERSON  TO HE-Z6.
           IF REG-PERSON NOT = 0
              MOVE REG-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 
             LINE 3 POSITION 15 SIZE 6
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.

       DIS-E1-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.

       DIS-E1-03.
           DISPLAY MEM-ANNEE  LINE  4 POSITION 32.
       DIS-E1-04.
           MOVE MEM-MOIS TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2  LINE  5 POSITION 34.
           MOVE "MO" TO LNK-AREA.
           MOVE 05401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       DIS-E1-10.
           DISPLAY MEM-FICHE LINE 13 POSITION 32.

       DIS-E1-END.
           PERFORM DIS-E1-TEXTE VARYING IDX-1 FROM 1 BY 1 UNTIL 
           IDX-1 > 5.

       DIS-E1-TEXTE.
           COMPUTE LIN-IDX = 6 + IDX-1.
           DISPLAY MEM-TEXTE(IDX-1) LINE LIN-IDX POSITION 30.

       AFFICHAGE-ECRAN.
           MOVE ECR-S(ECRAN-IDX) TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           EVALUATE ECRAN-IDX
               WHEN 1 PERFORM DIS-E1-01 THRU DIS-E1-END
           END-EVALUATE.
       END-PROGRAM.
           CLOSE MEMO.
           CANCEL "2-MEMO".
           CANCEL "6-MEMO".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
