      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME TR-PRIME SAISIE DES PRIMES TRANSPORT        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    TR-PRIME.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "TP-PRIME.FC".
           COPY "TP-PAR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "TP-PRIME.FDE".
           COPY "TP-PAR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 18. 

       01  ECRAN-SUITE.
           02 ECR-S1    PIC 999 VALUE 502.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S     PIC 999 OCCURS 1.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".
           COPY "TP-CCOL.REC".
           COPY "POSE.REC".
           COPY "PRESENCE.REC".
           COPY "HORSEM.REC".
           COPY "CODPAIE.REC".

       01  PRIME-NAME.
           02 FILLER             PIC X(9) VALUE "TP-PRIME.".
           02 ANNEE-PRIME        PIC 999.
       01  PAIE-NAME.
           02 FILLER             PIC X(8) VALUE "S-CSPAI.".
           02 ANNEE-PAIE         PIC 999.

       01  ECR-DISPLAY.
           02 HES3Z2 PIC -ZZ,ZZ BLANK WHEN ZERO.
           02 HEZ5Z2 PIC -Z(5),ZZ BLANK WHEN ZERO.
           02 HE-Z6Z2 PIC Z(6),ZZ BLANK WHEN ZERO.
           02 HEZ2Z2 PIC ZZ,ZZ BLANK WHEN ZERO.
           02 HES2Z2 PIC -Z,ZZ BLANK WHEN ZERO.
           02 HEZ3   PIC ZZZ.
           02 HE-Z5  PIC Z(5) BLANK WHEN ZERO.
           02 HE-Z6  PIC Z(6).

       77  WHELP-1    PIC 9(8)V99.
       77  ARRONDIS   PIC S9V999.

       77  HELP-4     PIC 9(6)V9999 COMP-3.
       77  HELP-5     PIC S9(6)V9999 COMP-3.

           COPY "V-BASES.REC".
           
       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
          COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                PRIME
                TP-PAR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-TR-PRIME.
       
           MOVE LNK-SUFFIX TO ANNEE-PRIME.
           OPEN I-O PRIME.
           OPEN INPUT TP-PAR.

           MOVE FR-KEY TO TP-PAR-FIRME.
           READ TP-PAR INVALID CONTINUE.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF  ECRAN-IDX = 1 
           AND INDICE-ZONE < 1
              MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 3 THRU 15
                      MOVE 0000000028 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
              WHEN 18 MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
           END-EVALUATE.

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-1 
               WHEN  2 PERFORM AVANT-2 
               WHEN  3 THRU 17 PERFORM AVANT-DONNEE
               WHEN 18 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM APRES-1 
               WHEN  2 PERFORM APRES-2 
               WHEN 18 PERFORM APRES-DEC.

           IF EXC-KEY = 27 OR EXC-KEY = 52 MOVE 0 TO INPUT-ERROR.
           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 52 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
                WHEN 53 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.
           IF INDICE-ZONE > CHOIX-MAX
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE  3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-DONNEE.
           COMPUTE IDX-2 = INDICE-ZONE - 2.
           IF TP-CCOL-FACTEUR(IDX-2) > 0
              PERFORM AVANT-DONNEE-1.

       AVANT-DONNEE-1.
           IF INDICE-ZONE < 10
              MOVE 0000000000 TO EXC-KFR (1)
           ELSE
              MOVE 0000000028 TO EXC-KFR (1)
           END-IF.
           PERFORM DISPLAY-F-KEYS.
           COMPUTE LIN-IDX = 4 + IDX-2.
           COMPUTE COL-IDX = 45.
           MOVE PRIME-DONNEE(IDX-2) TO HEZ5Z2
           ACCEPT HEZ5Z2 LINE LIN-IDX POSITION COL-IDX SIZE 9
             TAB UPDATE NO BEEP
             ON EXCEPTION EXC-KEY CONTINUE.
           INSPECT HEZ5Z2 REPLACING ALL "." BY ",".
           MOVE HEZ5Z2 TO PRIME-DONNEE(IDX-2).
           IF EXC-KEY = 8
              MOVE 0 TO PRIME-DONNEE(IDX-2)
              GO AVANT-DONNEE-1.
           
           IF EXC-KEY = 5
              PERFORM CONVERSION
              PERFORM DISPLAY-VAL.
           MOVE ,005 TO ARRONDIS.
           IF TP-CCOL-FACTEUR(IDX-2) < 0
               MOVE -,005 TO ARRONDIS.
           EVALUATE IDX-2
              WHEN 1 THRU 9
           
              COMPUTE HELP-5 = PRIME-DONNEE(IDX-2) *
                               TP-CCOL-FACTEUR(IDX-2) + ARRONDIS
              IF TP-PAR-CS(IDX-2) = 0
                 MOVE HELP-5 TO PRIME-VALEUR(IDX-2, 1)
              ELSE
                 MOVE HELP-5 TO PRIME-VALEUR(IDX-2, 2)
              END-IF
              WHEN 10 THRU 14
              IF  TP-CCOL-FACTEUR(IDX-2) NOT = 0
              AND TP-CCOL-FACTEUR(IDX-2) NOT = 1
                 COMPUTE HELP-5 = PRIME-DONNEE(IDX-2) * BAS-HORAIRE 
                                * TP-CCOL-FACTEUR(IDX-2) / 100 + ,005
              ELSE
                 COMPUTE HELP-5 = PRIME-DONNEE(IDX-2)
              END-IF
              IF TP-PAR-CS(IDX-2) = 0
                 MOVE HELP-5 TO PRIME-VALEUR(IDX-2, 1)
              ELSE
                 MOVE HELP-5 TO PRIME-VALEUR(IDX-2, 2)
              END-IF
              WHEN 15 PERFORM HRS-SUP
           END-EVALUATE.
           MOVE IDX-2 TO IDX-1.
           PERFORM DISPLAY-VAL VARYING IDX-2 FROM 16 BY 1 UNTIL 
                    IDX-2 > 18.
           MOVE IDX-1 TO IDX-2.
           PERFORM DISPLAY-VAL.
           EVALUATE EXC-KEY
               WHEN 54 MOVE CHOIX-MAX TO IDX-2 INDICE-ZONE
               WHEN 82 PERFORM END-PROGRAM
               WHEN OTHER COMPUTE INDICE-ZONE = 2 + IDX-2
           END-EVALUATE.
           

       CONVERSION.
           MOVE PRIME-DONNEE(IDX-2) TO HELP-4.
           IF IDX-2 > 9
              MOVE BAS-HORAIRE TO HELP-5
              IF TP-CCOL-FACTEUR(IDX-2) NOT = 0 
                 COMPUTE HELP-5 = BAS-HORAIRE * 
                 TP-CCOL-FACTEUR(IDX-2) / 100
              END-IF
           ELSE
              COMPUTE HELP-5 = TP-CCOL-FACTEUR(IDX-2) 
           END-IF.
           COMPUTE PRIME-DONNEE(IDX-2) = HELP-4 / HELP-5.
           COMPUTE WHELP-1 = HELP-4 * HELP-5 + ,005.
           MOVE WHELP-1 TO PRIME-VALEUR(IDX-1, 2).
           MOVE 13 TO EXC-KEY.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 0 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  5 IF REG-PERSON NOT = 0
                      CALL "4-A" USING LINK-V PR-RECORD REG-RECORD
                      CALL "1-P" USING LINK-V
                      CANCEL "1-P"
                      PERFORM AFFICHAGE-ECRAN 
                      PERFORM DIS-E1-01 THRU DIS-E1-VAL
                      PERFORM DISPLAY-F-KEYS
                      MOVE 1 TO INPUT-ERROR
                   END-IF
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.
           PERFORM CAR-RECENTE.
           PERFORM READ-PRIME.
           PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 
              PERFORM NEXT-REGIS
              PERFORM CAR-RECENTE
              PERFORM READ-PRIME
              PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.                     
           
       APRES-DEC.
           MOVE FR-KEY     TO PRIME-FIRME.
           MOVE REG-PERSON TO PRIME-PERSON.
           MOVE LNK-MOIS   TO PRIME-MOIS.  
           EVALUATE EXC-KEY
               WHEN 5 MOVE 0 TO DECISION WHELP-1
                      PERFORM TEST-SI-VIDE VARYING IDX FROM 1
                      BY 1 UNTIL IDX > 15
                      IF WHELP-1 = 0 
                         MOVE 8 TO EXC-KEY
                         GO APRES-DEC
                      END-IF
                      PERFORM MAKE-CSP
                      WRITE PRIME-RECORD INVALID 
                         REWRITE PRIME-RECORD
                      END-WRITE   
                      INITIALIZE PRIME-RECORD
                      PERFORM AFFICHAGE-DETAIL
                      MOVE 0 TO DECISION INDICE-ZONE
               WHEN 8 DELETE PRIME
                         INVALID CONTINUE
                      END-DELETE
                      INITIALIZE PRIME-RECORD
                      PERFORM MAKE-CSP
                      PERFORM AFFICHAGE-DETAIL
                      MOVE 0 TO DECISION INDICE-ZONE
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           
       CAR-RECENTE.
           INITIALIZE CAR-RECORD POSE-RECORD HJS-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           MOVE CAR-CCOL TO LNK-VAL.
           CALL "6-TPCC" USING LINK-V TP-CCOL-RECORD NUL-KEY.
           MOVE 100                   TO TP-CCOL-FACTEUR(16).
           MOVE CCOL-TAUX-HRS-SUPP(1) TO TP-CCOL-FACTEUR(17).
           CALL "6-GHJS" USING LINK-V HJS-RECORD CAR-RECORD PRESENCES.
           CALL "4-SALBAS" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 POSE-RECORD 
                                 HJS-RECORD 
                                 BASES-REMUNERATION.

           PERFORM AFFICHAGE-CCOL.

       HRS-SUP.
           INITIALIZE PRIME-VALEUR(16, 1)
                      PRIME-VALEUR(16, 2)
                      PRIME-VALEUR(17, 2)
                      PRIME-VALEUR(18, 1)
                      PRIME-VALEUR(18, 2).
           IF PRIME-DONNEE(15) > 0
              COMPUTE WHELP-1 = BAS-HORAIRE + (BAS-HORAIRE 
              * CCOL-TAUX-HRS-SUPP(1) / 100)
              COMPUTE PRIME-DONNEE(16) = PRIME-DONNEE(15) / WHELP-1
              COMPUTE PRIME-DONNEE(17) = PRIME-DONNEE(16)
           END-IF.

       MAKE-CSP.
           INITIALIZE CSP-RECORD.
           PERFORM CSP-AUTO THRU CSP-AUTO-END
               VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 18.

       CSP-AUTO.
           MOVE TP-PAR-CS(IDX-1) TO CSP-CODE.
           INITIALIZE CSP-REC-DET.  
           IF IDX-1 < 10 
              MOVE PRIME-DONNEE(IDX-1) TO CSP-TOTAL
              IF TP-CCOL-FACTEUR(IDX-1) NOT = 1 AND NOT = 0
                 MOVE TP-CCOL-FACTEUR(IDX-1) TO CSP-UNITAIRE
                 MOVE PRIME-DONNEE(IDX-1) TO CSP-UNITE
                 COMPUTE WHELP-1 = CSP-UNITAIRE * CSP-UNITE + ,005
                 MOVE WHELP-1 TO PRIME-VALEUR(IDX-1, 2) CSP-TOTAL
              END-IF
           END-IF.
           IF IDX-1 > 9 AND IDX-1 < 19
              IF TP-CCOL-FACTEUR(IDX-1) NOT = 0
                 MOVE BAS-HORAIRE TO CSP-DONNEE-2
                 MOVE TP-CCOL-FACTEUR(IDX-1) TO CSP-POURCENT
                 MOVE PRIME-DONNEE(IDX-1) TO CSP-UNITE
                 COMPUTE CSP-UNITAIRE = BAS-HORAIRE *
                         TP-CCOL-FACTEUR(IDX-1) / 100 
                 COMPUTE WHELP-1 = CSP-UNITAIRE * CSP-UNITE + ,005
                 MOVE WHELP-1 TO PRIME-VALEUR(IDX-1, 2) CSP-TOTAL
                 IF CSP-TOTAL = 0
                    INITIALIZE CSP-REC-DET
                 END-IF
              ELSE
                 MOVE PRIME-DONNEE(IDX-1) TO CSP-TOTAL
              END-IF
           END-IF.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.
       CSP-AUTO-END.
           EXIT.

       READ-PRIME.
           MOVE FR-KEY     TO PRIME-FIRME.
           MOVE LNK-MOIS   TO PRIME-MOIS.
           MOVE REG-PERSON TO PRIME-PERSON.
           READ PRIME INVALID INITIALIZE PRIME-REC-DET END-READ.
           PERFORM DIS-E1-01 THRU DIS-E1-VAL.

       DIS-E1-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6  LINE 3 POSITION 15.
           DISPLAY PR-NOM LINE 3 POSITION 47 SIZE 33.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 48 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 3 POSITION COL-IDX SIZE IDX LOW.

       DIS-E1-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-E1-END.
           EXIT.

       DIS-E1-VAL.
           PERFORM DISPLAY-VAL VARYING IDX-2 FROM 1 BY 1 UNTIL 
                    IDX-2 > 18.
           MOVE BAS-HORAIRE TO HEZ5Z2.
           DISPLAY HEZ5Z2 LINE 4 POSITION 70.

       DISPLAY-VAL.
           COMPUTE LIN-IDX = 4 + IDX-2.
           MOVE 45 TO COL-IDX.
           MOVE PRIME-DONNEE(IDX-2) TO HEZ5Z2.
           DISPLAY HEZ5Z2 LINE LIN-IDX POSITION COL-IDX.
           PERFORM DISPLAY-VALEUR VARYING IDX FROM 1 BY 1 UNTIL 
                   IDX > 2.

       DISPLAY-VALEUR.
           ADD 9 TO COL-IDX.
           MOVE PRIME-VALEUR(IDX-2, IDX) TO HE-Z6Z2.
           DISPLAY HE-Z6Z2 LINE LIN-IDX POSITION COL-IDX HIGH.
       
       TEST-SI-VIDE.
           ADD PRIME-RES(IDX) TO WHELP-1.

           
       AFFICHAGE-ECRAN.
           MOVE ECR-S(ECRAN-IDX) TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           PERFORM AFFICHAGE-PARAM.

       AFFICHAGE-DETAIL.
           PERFORM DIS-E1-01 THRU DIS-E1-VAL.

       AFFICHAGE-PARAM.
           PERFORM DIS-HE-TEXTE VARYING IDX FROM 1 BY 1 UNTIL IDX > 18.
           
       DIS-HE-TEXTE.
           COMPUTE LIN-IDX = IDX + 4.
           DISPLAY TP-PAR-TEXTE(IDX) LINE LIN-IDX POSITION 5 LOW.

       AFFICHAGE-CCOL.
           PERFORM DIS-CCOL VARYING IDX FROM 1 BY 1 UNTIL IDX > 15.
           
       DIS-CCOL.
           COMPUTE LIN-IDX = IDX + 4.
           MOVE TP-CCOL-FACTEUR(IDX) TO HE-Z6Z2.
           DISPLAY HE-Z6Z2  LINE LIN-IDX POSITION 30 LOW.

       END-PROGRAM.
           CLOSE PRIME.
           CLOSE TP-PAR.
           
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".
 
