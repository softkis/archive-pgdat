      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME CONJ INSTALLATIONS MODULES CHOM CONJONCTUREL�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    CONJ.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MENU.FC".
           COPY "MENUCOPY.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "MENU.FDE".
           COPY "MENUCOPY.FDE".

       WORKING-STORAGE SECTION.
             
       01  MENUS.
           02 MEN-01 PIC 9(10) VALUE 0401040500.
           02 MEN-02 PIC 9(10) VALUE 0401040600.
       01  COD-M REDEFINES MENUS.
           02  MEN PIC 9(10) OCCURS 2.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON MENU MENUCOPY.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  program.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-CONJ.

           OPEN I-O MENU.
           OPEN INPUT MENUCOPY.

           MOVE 0 TO LNK-VAL.
           DISPLAY "ADAPTATION MENU            " LINE 24 POSITION 10.
           PERFORM MENU VARYING IDX FROM 1 BY 1 UNTIL IDX > 2.
           EXIT PROGRAM.

       MENU.
           MOVE MEN(IDX) TO MNC-KEY.
           READ MENUCOPY INVALID CONTINUE
           NOT INVALID
              PERFORM TRANSLATE.

       TRANSLATE.
           MOVE MNC-RECORD TO MN-RECORD.
           WRITE MN-RECORD INVALID REWRITE MN-RECORD END-WRITE.
           CALL "4-MENUD" USING LINK-V MN-RECORD.
           CALL "4-MENUE" USING LINK-V MN-RECORD.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".

.
