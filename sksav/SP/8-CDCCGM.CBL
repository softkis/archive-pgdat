      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-CDCCGM TRANSFER ASCII CONGE CDC MOIS      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-CDCCGM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".
           SELECT OPTIONAL TF-JOURS ASSIGN TO RANDOM, PARMOD-PATH1,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.


       DATA DIVISION.

       FILE SECTION.

           COPY "JOURS.FDE".

       FD  TF-JOURS
           LABEL RECORD STANDARD
           DATA RECORD T1-RECORD.

       01  T1-RECORD.
           02 T1-A.
              03 T1-NUMBER PIC 9(6).
              03 T1-DELIM1 PIC X.
              03 T1-NOM    PIC X(25).
              03 T1-DELIM2 PIC X.
              03 T1-H OCCURS 32.
                 05 T1-HRS PIC ZZZ.
                 05 T1-DELIM PIC X.

              03 T1-REPORT PIC ZZZZ.
              03 T1-DELIM3 PIC X.
              03 T1-ANNEE  PIC ZZZZ.
              03 T1-DELIM4 PIC X.
              03 T1-M OCCURS 12.
                 05 T1-MOIS PIC ZZZ.
                 05 T1-DEL  PIC X.
              03 T1-COMP   PIC ZZZZ.
              03 T1-DELIM5 PIC X.
              03 T1-SOLDE  PIC ZZZZZ.
              03 T1-DELIM6 PIC X.
              03 T1-JOURS  PIC ZZ,ZZ.
              03 T1-DELIM7 PIC X.

           02 T1R-A REDEFINES T1-A.
              03 T1R-NUMBER PIC Z(6).
              03 T1R-DELIM1 PIC X.
              03 T1R-NOM    PIC X(25).
              03 T1R-DELIM2 PIC X.
              03 T1R-H OCCURS 32.
                 05 T1R-HRS PIC XXX.
                 05 T1R-DELIM PIC X.
              03 T1R-REPORT PIC XXXX.
              03 T1R-DELIM3 PIC X.
              03 T1R-ANNEE  PIC XXXX.
              03 T1R-DELIM4 PIC X.
              03 T1R-M OCCURS 12.
                 05 T1R-MOIS PIC XXX.
                 05 T1R-DEL  PIC X.
              03 T1R-COMP    PIC XXXX.
              03 T1R-DELIM5  PIC X.
              03 T1R-SOLDE   PIC XXXXX.
              03 T1R-DELIM6  PIC X.
              03 T1R-JOURS   PIC XXXXX.
              03 T1R-DELIM7  PIC X.



       WORKING-STORAGE SECTION.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "STATUT.REC".
           COPY "CONGE.REC".
           COPY "LIVRE.REC".
           COPY "PARMOD.REC".
           COPY "CALEN.REC".

           COPY "V-VAR.CPY".

       01  END-NUMBER        PIC 9(6) VALUE 999999.
       01  END-MATCHCODE     PIC X(10) VALUE "ZZZZZZZZZZ".
       01  TOT-COMP          PIC 9999V99.

       01  CHOIX-MAX         PIC 99 VALUE 6.
       01  PRECISION         PIC 9 VALUE 1.
       01  NOT-OPEN          PIC 9 VALUE 0.

       01  JOURS-NAME.
           02 FILLER         PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS    PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).


       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                JOURS 
                TF-JOURS.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-CDCCGM.
       
           MOVE "A" TO A-N.
           INITIALIZE PARMOD-RECORD.
           MOVE "8-CDCCGM" TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN INPUT JOURS.
           MOVE 1 TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2 MOVE 0063640400 TO EXC-KFR(1)
                  MOVE 0000000065 TO EXC-KFR(13)
                  MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6 MOVE 0000000025 TO EXC-KFR (1)
                  MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT 
           WHEN  5 PERFORM AVANT-PATH
           WHEN  6 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  6 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-PATH.
           COMPUTE IDX-4 = INDICE-ZONE - 4.
           COMPUTE LIN-IDX = IDX-4 + 16.
           IF PARMOD-PATHS(IDX-4) = SPACES
              MOVE "C:\FILENAME" TO PARMOD-PATHS(IDX-4) 
           END-IF.
           ACCEPT PARMOD-PATHS(IDX-4)
             LINE LIN-IDX  POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TH 
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TH.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-PERSON-END.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-PERSON.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON    > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-PERSON-END
           END-IF.
           INITIALIZE CAR-RECORD CONGE-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON
           END-IF.
           PERFORM DIS-HE-01.
           CALL "6-CONGE" USING LINK-V REG-RECORD CONGE-RECORD FAKE-KEY.
           PERFORM TRANS-O THRU TRANS-O-END.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
               GO READ-PERSON.
       READ-PERSON-END.
           EXIT.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           MOVE "AA" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.
           MOVE 1 TO INPUT-ERROR.
           
       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           MOVE 27 TO COL-IDX.
           IF A-N = "A"
              ADD 11 TO COL-IDX
              DISPLAY SPACES LINE 6 POSITION 37 SIZE 1.
           COMPUTE IDX = 80 - COL-IDX.
           MOVE 0 TO SIZ-IDX IDX-1.
           INSPECT PR-NOM TALLYING  SIZ-IDX FOR CHARACTERS BEFORE "  ".
           INSPECT PR-NOM-JF TALLYING IDX-1 FOR CHARACTERS BEFORE "  ".

           IF FR-NOM-JF = SPACES
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX  = COL-IDX + SIZ-IDX + 1
              IF PR-NOM-JF NOT = SPACES
                 COMPUTE IDX = 80 - COL-IDX
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
           ELSE
              IF PR-NOM-JF NOT = SPACES
                 DISPLAY PR-NOM-JF LINE 6 POSITION COL-IDX SIZE IDX
                 REVERSE
                 COMPUTE COL-IDX = COL-IDX + IDX-1 + 1
              END-IF
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-NOM LINE 6 POSITION COL-IDX SIZE IDX
              COMPUTE COL-IDX = COL-IDX + SIZ-IDX + 1
           END-IF.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 6 POSITION COL-IDX SIZE IDX LOW.
       DIS-HE-STAT.
           DISPLAY STAT-NOM  LINE 9 POSITION 35.
       DIS-HE-END.
           EXIT.


       TRANS-O.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-JOURS
              INITIALIZE T1-RECORD T1R-NUMBER
              MOVE ";" TO 
              T1-DELIM1 T1-DELIM2 T1-DELIM3 T1-DELIM4
              T1-DELIM5 T1-DELIM6 T1-DELIM7
              PERFORM DAT VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > 12
              PERFORM J-N
              PERFORM LIMS VARYING IDX FROM 1 BY 1 UNTIL IDX > 32
              WRITE T1-RECORD
              PERFORM J-S 
              PERFORM MOIS VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > 12
              MOVE "REP " TO T1R-REPORT
              MOVE "DU  " TO T1R-ANNEE
              MOVE "COMP" TO T1R-COMP
              MOVE "SOLDE" TO T1R-SOLDE
              MOVE "JOURS" TO T1R-JOURS
              INITIALIZE T1R-NUMBER T1R-NOM
              PERFORM LIMS VARYING IDX FROM 1 BY 1 UNTIL IDX > 32
              WRITE T1-RECORD
              MOVE 1 TO NOT-OPEN 
           END-IF.
           INITIALIZE JRS-RECORD T1-RECORD.
           MOVE ";" TO 
              T1-DELIM1 T1-DELIM2 T1-DELIM3 T1-DELIM4
              T1-DELIM5 T1-DELIM6 T1-DELIM7.
           MOVE CONGE-SOLDE TO T1-REPORT SH-00.
           ADD  CONGE-AJUSTE TO CONGE-AN.
           ADD  CONGE-AN TO SH-00.
           MOVE CONGE-AN TO T1-ANNEE.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO T1-NOM.
           MOVE FR-KEY TO JRS-FIRME-2.
           MOVE REG-PERSON TO JRS-PERSON-2 T1-NUMBER.
           START JOURS KEY > JRS-KEY-2 INVALID 
                GO TRANS-O-END.
       TRANS-O-1.
           READ JOURS NEXT AT END
                GO TRANS-O-END
           END-READ.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
              GO TRANS-O-END.
           IF 20 NOT = JRS-OCCUPATION 
              GO TRANS-O-1
           END-IF.
           IF JRS-MOIS = LNK-MOIS
              PERFORM TRANS-1 VARYING IDX FROM 1 BY 1 UNTIL IDX > 32.
           MOVE JRS-HRS(32) TO T1-MOIS(JRS-MOIS).
           SUBTRACT JRS-HRS(32) FROM SH-00.
           GO TRANS-O-1.
       TRANS-O-END.
           MOVE 0 TO TOT-COMP.
           PERFORM COMPENSATION VARYING IDX FROM 1 BY 1 UNTIL IDX > 12.
           MOVE TOT-COMP TO T1-COMP.
           SUBTRACT TOT-COMP FROM SH-00.
           MOVE SH-00 TO T1-SOLDE.
           COMPUTE SH-00 = SH-00 / CAR-HRS-JOUR.
           MOVE SH-00 TO T1-JOURS.
           PERFORM LIMS VARYING IDX FROM 1 BY 1 UNTIL IDX > 32.
           PERFORM MOIS-LIM VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > 12.
           WRITE T1-RECORD.

       TRANS-1.
           MOVE JRS-HRS(IDX) TO T1-HRS(IDX).

       LIMS.
           MOVE ";" TO T1-DELIM(IDX).

       DAT.
           PERFORM MOIS-NOM.
           MOVE ";" TO T1-DEL(IDX-4).

       MOIS.
           MOVE IDX-4 TO T1-MOIS(IDX-4).
           PERFORM MOIS-LIM.

       MOIS-NOM.
           MOVE IDX-4 TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           ADD 200 TO LNK-NUM.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO T1R-MOIS(IDX-4).

       JOUR-SEM.
           MOVE SEM-IDX(LNK-MOIS, IDX) TO LNK-NUM.
           ADD 200 TO LNK-NUM.
           MOVE "SW" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO T1R-HRS(IDX)
           IF  CAL-JOUR(LNK-MOIS, IDX) > 0
           AND CAL-JOUR(LNK-MOIS, IDX) < 7
               MOVE ":-)" TO T1R-HRS(IDX)
           END-IF.

       MOIS-LIM.
           MOVE ";" TO T1-DEL(IDX-4).

       J-S.
           PERFORM JOUR-SEM VARYING IDX FROM 1 BY 1 UNTIL IDX > 
           MOIS-JRS(LNK-MOIS).
       J-N.
           PERFORM JOUR-NUM VARYING IDX FROM 1 BY 1 UNTIL IDX > 
           MOIS-JRS(LNK-MOIS).

       JOUR-NUM.
           MOVE IDX TO T1-HRS(IDX).

       COMPENSATION.
           INITIALIZE L-RECORD.
           MOVE FR-KEY     TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON.
           MOVE IDX        TO L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           ADD L-UNI-CONGE-PLUS       TO   TOT-COMP.
           SUBTRACT L-UNI-CONGE-MIN FROM TOT-COMP.
       
       AFFICHAGE-ECRAN.
           MOVE 608 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".
