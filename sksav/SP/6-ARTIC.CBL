      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-ARTIC MODULE GENERAL LECTURE ARTICLE      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-ARTIC.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "ARTICLE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "ARTICLE.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "ARTICLE.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON ARTICLE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-ARTIC.
       
           IF NOT-OPEN = 0
              OPEN I-O ARTICLE
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO ART-RECORD.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 99
      *       IF LNK-SQL = "Y" 
      *          CALL "9-ARTIC" USING LINK-V ART-RECORD EXC-KEY
      *       END-IF
              WRITE ART-RECORD INVALID REWRITE ART-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ ARTICLE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ ARTICLE NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ ARTICLE NO LOCK INVALID INITIALIZE ART-RECORD
                      END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE ART-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START ARTICLE KEY < ART-KEY INVALID GO EXIT-1.
       START-2.
           START ARTICLE KEY > ART-KEY INVALID GO EXIT-1.


               