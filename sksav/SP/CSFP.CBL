      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME CSFP IMPORTATION                            �
      *  � CODES SALAIRES FICHE DE PAIE 2009                     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    CSFP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
             
       01  COD-V.
           02  COD-1    PIC 9999 VALUE 9500.
           02  COD-2    PIC 9999 VALUE 9501.
           02  COD-3    PIC 9999 VALUE 9502.
           02  COD-4    PIC 9999 VALUE 9505.
           02  COD-5    PIC 9999 VALUE 9506.
           02  COD-6    PIC 9999 VALUE 9507.
           02  COD-7    PIC 9999 VALUE 9511.
           02  COD-8    PIC 9999 VALUE 9512.
           02  COD-9    PIC 9999 VALUE 9513.
           02  COD-10   PIC 9999 VALUE 9514.
           02  COD-11   PIC 9999 VALUE 9515.
           02  COD-12   PIC 9999 VALUE 9521.
           02  COD-13   PIC 9999 VALUE 9521.
           02  COD-14   PIC 9999 VALUE 9522.
           02  COD-15   PIC 9999 VALUE 9523.
           02  COD-16   PIC 9999 VALUE 9524.
           02  COD-17   PIC 9999 VALUE 9525.
           02  COD-18   PIC 9999 VALUE 9526.
           02  COD-19   PIC 9999 VALUE 9527.
           02  COD-20   PIC 9999 VALUE 9528.
           02  COD-21   PIC 9999 VALUE 9529.
           02  COD-22   PIC 9999 VALUE 9550.
           02  COD-23   PIC 9999 VALUE 9551.
           02  COD-24   PIC 9999 VALUE 9552.
           02  COD-25   PIC 9999 VALUE 9600.
           02  COD-26   PIC 9999 VALUE 9601.
           02  COD-27   PIC 9999 VALUE 9602.
           02  COD-28   PIC 9999 VALUE 9605.
           02  COD-29   PIC 9999 VALUE 9630.
           02  COD-30   PIC 9999 VALUE 9631.
           02  COD-31   PIC 9999 VALUE 9632.
           02  COD-32   PIC 9999 VALUE 9650.
           02  COD-33   PIC 9999 VALUE 9651.
           02  COD-34   PIC 9999 VALUE 9652.
           02  COD-35   PIC 9999 VALUE 9660.
           02  COD-36   PIC 9999 VALUE 9665.
       01  COD-R REDEFINES COD-V.
           02  COD PIC 9999 OCCURS 36.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".



       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-CSFP.
           MOVE 0 TO LNK-VAL.
           MOVE LNK-ANNEE TO IDX-1.
           MOVE 2009 TO LNK-ANNEE.
           DISPLAY "ADAPTATION CODES SALAIRE ! " LINE 24 POSITION 10.
           PERFORM COPIES VARYING IDX FROM 1 BY 1 UNTIL IDX > 36.

           MOVE IDX-1 TO LNK-ANNEE.
           EXIT PROGRAM.

       COPIES.
           MOVE COD(IDX) TO LNK-NUM.
           CALL "4-CSIMP" USING LINK-V.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".


