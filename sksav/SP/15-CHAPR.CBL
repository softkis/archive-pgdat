      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 15-CHAPR TRANSFERT CHAPRO                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    15-CHAPR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.

           COPY "FILENAME.FC".

           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, FILE-NAME
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

           SELECT OPTIONAL F-BATCH ASSIGN TO DISK, BATCH-NAME
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-HELP.


       DATA DIVISION.
      *袴袴袴袴袴袴�

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FILENAME.FDE".

       FD  F-BATCH
           LABEL RECORD STANDARD
           DATA RECORD BAT-RECORD.

       01  BAT-RECORD PIC X(80).

       FD  TF-TRANS
           LABEL RECORD STANDARD
           DATA RECORD CHAPRO-BODY.

      *  ENREGISTREMENT FICHIER DE TRANSFER

       01  CHAPRO-BODY.
           02 CHAPRO-ANNEE          PIC 9(4).
           02 CHAPRO-FILLER         PIC X.
           02 CHAPRO-EMPLOYEUR      PIC 9(13).
           02 CHAPRO-FILLER         PIC X.
           02 CHAPRO-MATRICULE      PIC 9(11).
           02 CHAPRO-FILLER         PIC X.
           02 CHAPRO-NOM            PIC X(30).
           02 CHAPRO-FILLER         PIC X.
           02 CHAPRO-CODE           PIC X.
           02 CHAPRO-FILLER         PIC X.
           02 CHAPRO-APPRENTI       PIC X.
           02 CHAPRO-FILLER         PIC X.
           02 CHAPRO-MATERNITE      PIC X.
           02 CHAPRO-FILLER         PIC X.
           02 CHAPRO-VALEUR         PIC 999V99.

       WORKING-STORAGE SECTION.

       01  JOB-STANDARD          PIC X(10) VALUE "160       ".
       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  BATCH-NAME            PIC X(50) VALUE "CHAPRO.BAT".
       01  B-S    PIC X VALUE "\".
       01  C-D    PIC XX VALUE "CD".
       01  DONE                  PIC X VALUE " ".
       01  ADRES.
           02 A-1 PIC X(52) VALUE 
           "C:\CETREL\SOFIE\DATA\123456789\DECRYPTED\PRODUCTION\".
       01  REP-FILE PIC X(40) VALUE "C:\SM3\FILE\CHAPLIST".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PARMOD.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "FIRME.REC".
           COPY "FIRMAT.REC".

       01  FILE-NAME            PIC X(150).

       01  REC-MATR.
           02  REC-MAT PIC X(28).
           02  REC-MATR-R REDEFINES REC-MAT.
               10  MATR-ID1         PIC 9(13).
               10  MATR-CONVENTION  PIC X(6).
               10  MATR-SECULINE    PIC 9(9).

           COPY "V-VH00.CPY".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(160) OCCURS 45.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z4 PIC Z(4).
           02 HE-ZX REDEFINES HE-Z4.
               03 HE-M PIC Z.
               03 HE-F PIC Z.
               03 HE-A PIC ZZ.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               TF-TRANS
               FILENAME 
               F-BATCH.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-15-CHAPR.

           INITIALIZE PARMOD-RECORD.
           MOVE "SECULINE" TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-MATR TO REC-MATR.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE PARMOD-RECORD LIN-NUM LIN-IDX SH-00 FN-RECORD
           FORMULAIRE.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           IF MENU-PROG-NUMBER = 1 
              MOVE LNK-USER TO PARMOD-USER.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           PERFORM AFFICHAGE-ECRAN.

           MOVE FR-KEY TO STORE.
           MOVE LNK-MOIS  TO SAVE-MOIS.
           MOVE LNK-ANNEE TO SAVE-ANNEE.
           CALL "4-FIRMAT" USING LINK-V.
           CANCEL "4-FIRMAT".
           MOVE "CHAP" TO LNK-AREA.
           IF PARMOD-TEXT1 NOT = SPACES
              MOVE 3 TO INDICE-ZONE
           END-IF.
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 3     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 4     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2
           WHEN  3 PERFORM APRES-3
           WHEN  4 PERFORM APRES-DEC.
 
           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT PARMOD-TEXT1
             LINE 4 POSITION 15 SIZE 40
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "UPPER"
             ON EXCEPTION EXC-KEY CONTINUE.
           IF PARMOD-TEXT1 = SPACES
              MOVE REP-FILE TO PARMOD-TEXT1
              MOVE 1 TO INPUT-ERROR
           END-IF.

       AVANT-2.
           ACCEPT PARMOD-TEXT3
             LINE 6 POSITION 15 SIZE 65
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "UPPER"
             ON EXCEPTION EXC-KEY CONTINUE.
           IF PARMOD-TEXT3 = SPACES
           OR EXC-KEY      = 12
              MOVE ADRES TO PARMOD-TEXT3
              INSPECT PARMOD-TEXT3 REPLACING ALL "123456789"
              BY MATR-SECULINE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           MOVE 0 TO IDX-1 IDX-2.
           INSPECT PARMOD-TEXT3 TALLYING IDX-1 FOR CHARACTERS 
           BEFORE "\ ".
           INSPECT PARMOD-TEXT3 TALLYING IDX-2 FOR CHARACTERS 
           BEFORE "  ".
           COMPUTE IDX-3 = IDX-2 + 1
           IF IDX-1 > IDX-2
              STRING B-S DELIMITED BY SIZE INTO PARMOD-TEXT3 POINTER
              IDX-3.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       AVANT-3.
           IF DONE = SPACES
              PERFORM GET-NAMES.
           ACCEPT FN-NOM
             LINE 8 POSITION 15 SIZE 60
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "UPPER"
             ON EXCEPTION EXC-KEY CONTINUE.

       GET-NAMES.
           OPEN OUTPUT F-BATCH.
           MOVE 1 TO IDX-3.
           UNSTRING PARMOD-TEXT3 DELIMITED BY "\" INTO BAT-RECORD
           WITH POINTER IDX-3.
           WRITE BAT-RECORD.
           MOVE PARMOD-TEXT3 TO BAT-RECORD
           MOVE 1 TO IDX-3.
           STRING C-D DELIMITED BY SIZE INTO BAT-RECORD WITH POINTER 
           IDX-3.
           WRITE BAT-RECORD.
           MOVE "DIR chap*.* > " TO BAT-RECORD.
           MOVE 16 TO IDX-3.
           STRING PARMOD-TEXT1 DELIMITED BY " " INTO BAT-RECORD
           WITH POINTER IDX-3.
           WRITE BAT-RECORD.
           MOVE "EXIT" TO BAT-RECORD.
           WRITE BAT-RECORD.
           CLOSE F-BATCH.
           
           CALL "SYSTEM" USING BATCH-NAME.
           MOVE "CHAPLIST" TO LNK-TEXT-1.
           MOVE "CHAPRO." TO LNK-TEXT-2.
           MOVE "CHAP" TO LNK-AREA.
           CALL "15-FNAME" USING LINK-V.
           MOVE 9999 TO FN-NUMBER.
           CALL "6-FNAME" USING LINK-V FN-RECORD PV-KEY.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           IF PARMOD-TEXT1 = SPACES
              MOVE REP-FILE     TO PARMOD-TEXT1
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-2.
           IF PARMOD-TEXT3 = SPACES
              MOVE ADRES TO PARMOD-TEXT3
              MOVE 1 TO INPUT-ERROR
           END-IF.


       APRES-3.
           MOVE "CHAP" TO LNK-AREA.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-FILENAME
           WHEN   2 CALL "2-FNAME" USING LINK-V FN-RECORD
                    PERFORM AFFICHAGE-ECRAN 
           END-EVALUATE.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY
               WHEN 5 MOVE 1 TO IDX-1
                      STRING PARMOD-TEXT3 DELIMITED BY "  " 
                      INTO FILE-NAME POINTER IDX-1
                      STRING FN-NOM DELIMITED BY "  " 
                      INTO FILE-NAME POINTER IDX-1
                      OPEN INPUT TF-TRANS
                      PERFORM LECT-TRANS THRU END-TRANS
                      PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-FILENAME.
           CALL "6-FNAME" USING LINK-V FN-RECORD EXC-KEY.

       LECT-TRANS.
           READ TF-TRANS AT END GO END-TRANS.
           ADD 1 TO SH-00
           IF SH-00 = 1
              GO LECT-TRANS.
           DISPLAY CHAPRO-BODY LINE 21 POSITION 1 SIZE 80.
           MOVE CHAPRO-MATRICULE TO LNK-MATRICULE.
           MOVE CHAPRO-EMPLOYEUR TO LNK-MATR FIRMAT-KEY.
           CALL "6-FIRMAT" USING LINK-V FIRMAT-RECORD.
           CALL "4-REG" USING LINK-V.
           PERFORM TEST-LIVRE.
           GO LECT-TRANS.
       END-TRANS.
           CLOSE TF-TRANS.
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET.

       TEST-LIVRE.
           MOVE LNK-KEY TO FR-KEY.
           CALL "6-FIRME" USING LINK-V "N" FAKE-KEY.
           MOVE LNK-PERSON TO REG-PERSON
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           MOVE CHAPRO-ANNEE TO LNK-ANNEE.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

           IF COUNTER = 0 
              MOVE 0 TO LIN-NUM DEC-NUM
              MOVE 1 TO COUNTER
           END-IF.
           COMPUTE IDX = LIN-NUM + 1.
           IF IDX >= 45
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              INITIALIZE FORMULAIRE
              MOVE 2 TO LIN-NUM COL-NUM
              MOVE FN-NOM TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE 50 TO COL-NUM
              MOVE MENU-DESCRIPTION TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE 112 TO COL-NUM
              MOVE TODAY-JOUR  TO VH-00
              MOVE  2 TO CAR-NUM
              PERFORM FILL-FORM
              MOVE  2 TO CAR-NUM
              MOVE 115 TO COL-NUM
              MOVE TODAY-MOIS  TO VH-00
              PERFORM FILL-FORM
              MOVE 4 TO CAR-NUM
              MOVE 118 TO COL-NUM
              MOVE TODAY-ANNEE TO VH-00
              PERFORM FILL-FORM
              ADD 1 TO LIN-NUM
           END-IF.
           ADD  1 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           MOVE CHAPRO-ANNEE TO VH-00.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.

           ADD 1 TO COL-NUM.
           MOVE CHAPRO-EMPLOYEUR TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 28 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           IF FR-KEY > 0
              MOVE FR-KEY TO VH-00
           ELSE
              MOVE FIRMAT-NUMBER TO VH-00
              MOVE FIRMAT-NOM TO FR-NOM.
           PERFORM FILL-FORM

           MOVE 33 TO COL-NUM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           ADD 2 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 60 TO COL-NUM.
           MOVE CHAPRO-VALEUR TO VH-00
           MOVE 3 TO CAR-NUM
           MOVE 2 TO DEC-NUM
           PERFORM FILL-FORM.

           MOVE 70 TO COL-NUM.
           IF REG-PERSON > 0
              MOVE REG-PERSON TO VH-00
              MOVE 6 TO CAR-NUM
              PERFORM FILL-FORM
           ELSE
              MOVE "?" TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE CHAPRO-NOM TO PR-NOM.

           MOVE 80 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 135 TO COL-NUM.
           MOVE CHAPRO-MATRICULE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L160O" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.
           INITIALIZE FORMULAIRE.

       AFFICHAGE-ECRAN.
           COMPUTE LNK-VAL = 2557.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           DISPLAY PARMOD-TEXT1 LINE 4 POSITION 15.
           DISPLAY PARMOD-TEXT3 LINE 6 POSITION 15 SIZE 60.
           DISPLAY SPACES       LINE 10 POSITION 1 SIZE 60.

       END-PROGRAM.
           MOVE STORE TO FR-KEY.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           MOVE SAVE-ANNEE TO LNK-ANNEE.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "L160O" USING LINK-V FORMULAIRE.
           CANCEL "L160O".
           CANCEL "15-FNAME".
           CANCEL "6-FNAME".
           CANCEL "2-FNAME".
           DELETE FILE FILENAME.
           CALL "6-FIRME" USING LINK-V "N" FAKE-KEY.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFILL2.CPY".
           COPY "XACTION.CPY".

           INITIALIZE FORMULAIRE.
