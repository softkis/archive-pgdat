      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-FACFDU IMPRESSION FACTURES CLIENTS        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-FACFDU.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FACTINT.FC".
           COPY "FACDINT.FC".
           COPY "BANQF.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FACTINT.FDE".
           COPY "FACDINT.FDE".
           COPY "BANQF.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 12.
       01  PRECISION             PIC 99 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "MESSAGE.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
SU         COPY "CODTXT.REC".
           COPY "FAC.LNK".
           COPY "ECHEANCE.REC".
           COPY "PARMOD.REC".
           COPY "BANQUE.REC".
           COPY "FIRME.REC".
           COPY "TVA.REC".

       01  PAGES                  PIC 99 VALUE 0.
       01  NO-FACTURE             PIC 9(8) VALUE 0.
       01  SAVE-FIRME             PIC 9(8) VALUE 0.

           COPY "V-VH00.CPY".

       01  FACTURE-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FI".
           02 FIRME-FACTURE      PIC 9999.
       01  FID-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FD".
           02 FIRME-FID          PIC 9999.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  H-TODAY.
           02 H-DATE        PIC 9(8).
           02 H-TIME        PIC 9(8).

       01  REEDITION             PIC X VALUE "N".

       01  TXT-RECORD.
           02 TXT-KEY.
              03 TXT-MODULE  PIC X(10).
              03 TXT-FIRME   PIC 999999.
           02 TXT-REC-DET.
              03 TXT-TXT     PIC X(60) OCCURS 5.
              03 TXT-FILLER  PIC XXXX.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z2Z2 PIC ZZ,ZZ.
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z5Z2 PIC Z(5),ZZ BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FACTURE FID.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-FACFDU.
       
           MOVE LNK-LANGUAGE TO SAVE-LANGUAGE.
           CANCEL "4-RAP".
           CANCEL "4-FACPR".
           INITIALIZE LINK-RECORD PARMOD-RECORD TXT-RECORD.
           MOVE FR-KEY   TO SAVE-FIRME.
           MOVE LNK-MOIS TO SAVE-MOIS.
           CALL "0-TODAY" USING TODAY.
           CALL "0-TODAY" USING H-TODAY.
           MOVE LNK-SUFFIX TO FIRME-FACTURE FIRME-FID.
           OPEN I-O FACTURE.
           OPEN I-O FID.
           MOVE "FACT-TXT"     TO TXT-MODULE.
           CALL "6-PARMOD" USING LINK-V TXT-RECORD "R".
           MOVE MENU-PROG-NUMBER TO FIRME-KEY.
           CALL "6-FIRMES" USING LINK-V FIRME-RECORD "N" FAKE-KEY.
           OPEN INPUT BANQF.

           PERFORM AFFICHAGE-ECRAN .
           INITIALIZE FR-RECORD.
           PERFORM DIS-LIGNES.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)

           WHEN 12    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0041410000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 THRU 11 PERFORM AVANT-TEXTE
           WHEN 12 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  6 PERFORM APRES-6 
           WHEN 12 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-4.
           ACCEPT NO-FACTURE 
             LINE 9 POSITION 27 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-5.
           MOVE 11230000 TO LNK-POSITION.
           CALL "0-GDATE" USING TODAY-DATE
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.

       AVANT-6.
           ACCEPT REEDITION
             LINE 13 POSITION 32 SIZE 1 
             TAB UPDATE CONTROL "UPPER" NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.
           MOVE REEDITION TO ACTION.

       AVANT-TEXTE.        
           COMPUTE IDX = INDICE-ZONE - 6.
           COMPUTE LIN-IDX = 14 + IDX.
           IF IDX = 1
             ACCEPT TXT-TXT(IDX)
             LINE LIN-IDX POSITION 20 SIZE 60
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE
           ELSE
             IF TXT-TXT(1) NOT = SPACES
             ACCEPT TXT-TXT(IDX)
             LINE LIN-IDX POSITION 20 SIZE 60
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE
             ELSE
                INITIALIZE TXT-REC-DET
                PERFORM DIS-LIGNES
             END-IF
           END-IF.
           CALL "6-PARMOD" USING LINK-V TXT-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-4.
           IF NO-FACTURE NOT = 0
              MOVE NO-FACTURE TO FI-NUMBER
              READ FACTURE NO LOCK INVALID MOVE 0 TO NO-FACTURE END-READ
              IF NO-FACTURE NOT = 0 MOVE FI-CLIENT TO FR-KEY 
                 END-NUMBER
                 PERFORM NEXT-FIRME
                 PERFORM AFFICHAGE-DETAIL
              END-IF
           END-IF.

       APRES-6.
           IF ACTION = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE "N" TO REEDITION
              MOVE 1 TO INPUT-ERROR.
       
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 IF NO-FACTURE = 0
                     PERFORM TRAITEMENT
                  ELSE
                     PERFORM MISE-EN-PAGE
                  END-IF
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT.
           MOVE 0 TO INPUT-ERROR.
           PERFORM START-FIRME.
           IF INPUT-ERROR = 0 
               PERFORM READ-FIRME THRU READ-FIRME-END.
           PERFORM END-PROGRAM.

       START-FIRME.
           IF FR-KEY > 0 
              SUBTRACT 1 FROM FR-KEY.
           IF A-N = "A"
              MOVE 9999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-FIRME.
           PERFORM NEXT-FIRME.
           IF FR-KEY = 0
           OR FR-KEY > END-NUMBER
           OR FR-MATCHCODE > END-MATCHCODE
              GO READ-FIRME-END
           END-IF.
           IF FR-FIN-A > 0 AND < LNK-ANNEE
              GO READ-FIRME
           END-IF.
           PERFORM TOTAL-FACTURE THRU TOTAL-FACTURE-END.
           PERFORM DIS-HE-01.
           GO READ-FIRME.
       READ-FIRME-END.
           PERFORM END-PROGRAM.
           
       NEXT-FIRME.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       TOTAL-FACTURE.
           INITIALIZE FI-RECORD.
           MOVE FR-KEY TO FI-CLIENT.
           MOVE 1 TO PAGES.
           START FACTURE KEY > FI-KEY-2 INVALID
              PERFORM READ-END
              GO TOTAL-FACTURE-END.
           PERFORM READ-FACTURE THRU READ-END.
       TOTAL-FACTURE-END.
           EXIT.

       READ-FACTURE.
           READ FACTURE NEXT NO LOCK AT END 
              GO READ-END.
           IF FR-KEY NOT = FI-CLIENT
              GO READ-END
           END-IF.
           IF LNK-ANNEE NOT = FI-DEBUT-A
              GO READ-FACTURE
           END-IF.
           IF REEDITION = "N"
              IF FI-EDIT NOT = 0
                 GO READ-FACTURE
              END-IF
      *    ELSE
      *       IF FI-EDIT NOT = TODAY-DATE
      *          GO READ-FACTURE
      *       END-IF
           END-IF.

           IF REEDITION = "N"
           AND FI-EDIT  = 0
              IF TODAY-DATE = 0
                 MOVE H-DATE TO FI-EDIT
              ELSE
                 MOVE TODAY-DATE TO FI-EDIT
              END-IF
           END-IF.
           PERFORM MISE-EN-PAGE.
           GO READ-FACTURE.
       READ-END.
           EXIT.

       MISE-EN-PAGE.
           MOVE 1 TO PAGES.
           PERFORM LAYOUT.
           PERFORM TEXTES.
           PERFORM PAGE-1.
           PERFORM DET-FACTURE.
           PERFORM TOTAUX.
           PERFORM PAGE-FIN.
           PERFORM TRANSMET.
           REWRITE FI-RECORD INVALID CONTINUE.

       DET-FACTURE.
           INITIALIZE FID-RECORD CS-RECORD.
           MOVE 0 TO LIN-NUM.
           MOVE FI-NUMBER TO FID-NUMBER.
           START FID KEY > FID-KEY-3 INVALID CONTINUE
              NOT INVALID
           PERFORM READ-FID THRU READ-FID-END.
           INITIALIZE FID-RECORD.
           MOVE FI-NUMBER TO FID-NUMBER.
           START FID KEY > FID-KEY-3 INVALID CONTINUE
              NOT INVALID
           PERFORM SET-FID THRU SET-FID-END.

       READ-FID.
           READ FID NEXT NO LOCK AT END 
              GO READ-FID-END.
           IF FI-NUMBER NOT = FID-NUMBER 
              GO READ-FID-END.
           MOVE FID-CODE TO CD-NUMBER.
           CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY.
           INITIALIZE CS-RECORD.
SU         CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
           IF FR-LANGUE NOT = SPACES
              MOVE FR-LANGUE TO LNK-LANGUAGE
           ELSE
              MOVE "F" TO LNK-LANGUAGE
           END-IF
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           IF  CS-IMPRESSION(1) = 0 
           AND CS-IMPRESSION(2) = 0 
           AND CS-IMPRESSION(3) = 0 
           AND CS-IMPRESSION(4) = 0 
           AND CS-IMPRESSION(5) = 0 
           AND CS-IMPRESSION(6) = 0 
              GO READ-FID
           END-IF.
           ADD 1 TO LIN-NUM.
           IF LIN-NUM >= 24
              PERFORM TRANSMET
              PERFORM LAYOUT
              PERFORM TEXTES
           END-IF.
           PERFORM EDIT-CS.
           GO READ-FID.
       READ-FID-END.

       SET-FID.
           READ FID NEXT NO LOCK AT END 
              GO SET-FID-END.
           IF FI-NUMBER NOT = FID-NUMBER 
              GO SET-FID-END.
           DELETE FID INVALID CONTINUE.
           MOVE TODAY-ANNEE TO FID-ANNEE.
           MOVE TODAY-MOIS  TO FID-MOIS.
           WRITE FID-RECORD INVALID REWRITE FID-RECORD.
           GO SET-FID.
       SET-FID-END.

       EDIT-CS.
           MOVE FID-DONNEE(4) TO HE-Z4
           MOVE HE-Z4 TO LINK-ART-A(LIN-NUM).
           MOVE CTX-NOM TO LINK-ART-B(LIN-NUM).
      *    IF FID-TEXTE NOT = SPACES
      *       MOVE FID-TEXTE TO LINK-ART-B(LIN-NUM)
      *    END-IF.
           PERFORM FILL-CS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.

       FILL-CS.
           IF  CS-IMPRESSION(IDX) > 0 
           AND FID-DONNEE(IDX)    > 0 
              MOVE FID-DONNEE(IDX) TO HE-Z5Z2
              EVALUATE IDX 
              WHEN 1 MOVE  23 TO IDX-1
                     MOVE  33 TO IDX-2
              WHEN 2 MOVE  41 TO IDX-1
                     MOVE  51 TO IDX-2
              WHEN 3 MOVE  59 TO IDX-1
                     MOVE  69 TO IDX-2
              WHEN 4 MOVE   0 TO IDX-1
                     MOVE   0 TO IDX-2
              WHEN 5 MOVE   0 TO IDX-1
                     MOVE  98 TO IDX-2
              WHEN 6 MOVE   0 TO IDX-1
                     MOVE 108 TO IDX-2
              END-EVALUATE
              IF IDX-1 > 0
                 STRING CTX-DESCRIPTION(IDX) DELIMITED BY "  " INTO 
                 LINK-ART-B(LIN-NUM) WITH POINTER IDX-1
              END-IF
              IF IDX-2 > 0
                 STRING HE-Z5Z2 DELIMITED BY SIZE INTO 
                 LINK-ART-B(LIN-NUM) WITH POINTER IDX-2
              END-IF
           END-IF.

       TOTAUX.
           MOVE FI-TOTAL TO HE-Z5Z2
           MOVE HE-Z5Z2 TO LINK-BASE(1).
           MOVE FI-TVA TO HE-Z5Z2
           MOVE HE-Z5Z2 TO LINK-TVA(1). 
           COMPUTE SH-00 = FI-TOTAL + FI-TVA.
           MOVE SH-00  TO HE-Z5Z2
           MOVE HE-Z5Z2 TO LINK-TTC(1).
           MOVE FI-A-PAYER  TO HE-Z5Z2
           MOVE HE-Z5Z2 TO LINK-A-PAYER.


       LAYOUT.
           IF MENU-EXTENSION-2 = SPACES
              PERFORM ENTETE
           END-IF.

           MOVE FIRME-PHONE  TO LINK-TEXTE(1).
           MOVE FIRME-FAX    TO LINK-TEXTE(2).
           MOVE FIRME-E-MAIL TO LINK-TEXTE(3).

           MOVE 1 TO IDX-1.
           STRING FIRME-ETAB-A DELIMITED BY SIZE INTO LINK-TEXTE(4)
           WITH POINTER IDX-1.
           ADD 1 TO IDX-1.
           STRING FIRME-ETAB-N DELIMITED BY SIZE INTO LINK-TEXTE(4)
           WITH POINTER IDX-1.
           ADD 1 TO IDX-1.
           STRING FIRME-SNOCS DELIMITED BY SIZE INTO LINK-TEXTE(4)
           WITH POINTER IDX-1.

           MOVE FIRME-TVA   TO LINK-TEXTE(5).
           MOVE FIRME-COMMERCE TO LINK-TEXTE(6).


           MOVE FI-NUMBER TO HE-Z8.
           MOVE HE-Z8  TO LINK-NUM.
           MOVE PAGES  TO LINK-PAGE .
           MOVE FR-KEY TO HE-Z8.
           MOVE HE-Z8  TO LINK-NUM-CLIENT.
           IF FR-COMPTA > SPACES
              MOVE FR-COMPTA TO LINK-NUM-CLIENT.
           
           MOVE FI-EDIT-A TO HE-AA.
           MOVE FI-EDIT-M TO HE-MM.
           MOVE FI-EDIT-J TO HE-JJ.
           MOVE HE-DATE TO LINK-DATE.

           MOVE FI-DEBUT-M TO LINK-REFERENCE.
           
           MOVE FR-NOM TO LINK-ADR-01(1).
           MOVE 1 TO IDX-1.
           STRING FR-MAISON DELIMITED BY "  " INTO LINK-ADR-01(2)
           WITH POINTER IDX-1.
           ADD 1 TO IDX-1.
           STRING FR-RUE DELIMITED BY "  " INTO LINK-ADR-01(2)
           WITH POINTER IDX-1.
           MOVE 1 TO IDX-1.
           STRING FR-PAYS DELIMITED BY "  " INTO LINK-ADR-01(3)
           WITH POINTER IDX-1.
           MOVE FR-CODE-POST TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE INTO LINK-ADR-01(3)
           WITH POINTER IDX-1.
           ADD 1 TO IDX-1.
           STRING FR-LOCALITE DELIMITED BY "  " INTO LINK-ADR-01(3)
           WITH POINTER IDX-1.
           IF FR-COURRIER-1 NOT = SPACES
              MOVE FR-COURRIER-1 TO LINK-ADR-01(3)
              MOVE FR-COURRIER-2 TO LINK-ADR-01(4)
              MOVE FR-COURRIER-3 TO LINK-ADR-01(5).

       ENTETE.
           MOVE FIRME-NOM TO LINK-FIRM-NOM.
           MOVE 1 TO IDX-1.
           STRING FIRME-MAISON DELIMITED BY "  " 
           INTO LINK-FIRM-ADR-01(2) WITH POINTER IDX-1.
           ADD 1 TO IDX-1.
           STRING FIRME-RUE DELIMITED BY "  " 
           INTO LINK-FIRM-ADR-01(2) WITH POINTER IDX-1.
           MOVE 1 TO IDX-1.
           STRING FIRME-PAYS DELIMITED BY "  " 
           INTO LINK-FIRM-ADR-01(3) WITH POINTER IDX-1.
           MOVE FIRME-CODE-POST TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE INTO LINK-FIRM-ADR-01(3)
           WITH POINTER IDX-1.
           ADD 1 TO IDX-1.
           STRING FIRME-LOCALITE DELIMITED BY "  " INTO 
           LINK-FIRM-ADR-01(3) WITH POINTER IDX-1.
           
       TEXTES.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           IF FR-LANGUE = "D"
              MOVE FR-LANGUE TO PARMOD-USER
           END-IF.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-TX4(1) TO LINK-ENTTX-01.
           MOVE PARMOD-TX4(2) TO LINK-DATE-TX.
           MOVE PARMOD-TX4(3) TO LINK-NUM-TX.
           MOVE PARMOD-TX4(4) TO LINK-PAGE-TX.
           MOVE PARMOD-TX4(5) TO LINK-NUM-CLI-TX.
           MOVE PARMOD-TX4(6) TO LINK-REFER-TX.
           MOVE PARMOD-TX4(7) TO LINK-ECHEANCE-TX.
           MOVE PARMOD-TX4(8) TO LINK-PC-TVA-TX
           MOVE PARMOD-TX4(9) TO LINK-BASE-TX
           MOVE PARMOD-TX4(10) TO LINK-MT-TVA-TX
           MOVE PARMOD-TX4(11) TO LINK-TTC-TX
           MOVE PARMOD-TX4(12) TO LINK-APAYER-TX
           MOVE PARMOD-TX4(13) TO LINK-DESC(1).
           MOVE PARMOD-TX4(14) TO LINK-DESC(2).
           MOVE PARMOD-TX4(15) TO LINK-DESC(3).
           MOVE PARMOD-TX4(16) TO LINK-DESC(4).
           MOVE PARMOD-TX4(17) TO LINK-DESC(5).
           MOVE PARMOD-TX4(18) TO LINK-DESC(6).
           MOVE PARMOD-TX4(19) TO LINK-DESC(7).
           MOVE PARMOD-TX4(20) TO LINK-DESC(8).
           MOVE 114 TO LNK-NUM.
           EVALUATE FR-FREQ-FACT 
              WHEN 2 MOVE 118 TO LNK-NUM
              WHEN 3 MOVE 115 TO LNK-NUM
              WHEN 6 MOVE 116 TO LNK-NUM
           END-EVALUATE.
           MOVE "AA" TO LNK-AREA.
           MOVE FR-LANGUE TO LNK-LANGUAGE.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO LINK-REFER-TX.
           MOVE SAVE-LANGUAGE TO LNK-LANGUAGE.

       B-C.
           MOVE FIRME-KEY TO BQF-FIRME.
           START BANQF KEY > BQF-KEY INVALID GO B-C-END.

       B-C-1.
           READ BANQF NEXT NO LOCK AT END GO B-C-END.
           IF FIRME-KEY NOT = BQF-FIRME 
              GO B-C-END
           END-IF.
           MOVE BQF-CODE TO BQ-CODE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD FAKE-KEY.
           IF BQ-FICT NOT = 0
              GO B-C-1.
           ADD 1 TO IDX.
           IF IDX = 6
              GO B-C-END.
           MOVE 5 TO IDX-1.
           STRING BQF-CODE DELIMITED BY "    " INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           MOVE 20 TO IDX-1.
           STRING BQ-SWIFT DELIMITED BY "    " INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           MOVE 40 TO IDX-1.
           STRING BQF-PART(1) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(2) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(3) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(4) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(5) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(6) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(7) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           GO B-C-1.
       B-C-END.
           EXIT.


       PAGE-1.
           PERFORM LIGNES VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 5.

       PAGE-FIN.
           MOVE FI-ECHE-CODE TO ECH-CODE.
           IF ECH-CODE < 1
              MOVE FR-ECHEANCE TO ECH-CODE
           END-IF.
           IF ECH-CODE < 1
              MOVE 0 TO ECH-CODE
           END-IF.
           CALL "6-ECHEA" USING LINK-V ECH-RECORD FAKE-KEY.
           MOVE ECH-CODE TO FI-ECHE-CODE.
           PERFORM FILL-EC VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           IF FI-EDIT-M = 0
              MOVE TODAY TO FI-EDIT
           END-IF.
           MOVE FI-EDIT TO FI-ECHEANCE
           IF ECH-FIN-MOIS = 1
              MOVE MOIS-JRS(FI-EDIT-M) TO FI-ECHEA-J
           END-IF.
           ADD ECH-JOURS TO FI-ECHEA-J.
           IF FI-ECHEA-J > MOIS-JRS(FI-EDIT-M)
              SUBTRACT MOIS-JRS(FI-EDIT-M) FROM FI-ECHEA-J
              ADD 1 TO FI-ECHEA-M
           END-IF
           ADD ECH-MOIS TO FI-ECHEA-M
           IF FI-ECHEA-M > 12
              SUBTRACT 12 FROM FI-ECHEA-M
              ADD 1 TO FI-ECHEA-A
           END-IF.
           MOVE FI-ECHEA-A TO HE-AA.
           MOVE FI-ECHEA-M TO HE-MM.
           MOVE FI-ECHEA-J TO HE-JJ.
           MOVE HE-DATE TO LINK-ECHEANCE.
           IF ECH-FACTORING NOT = "N"
              INITIALIZE BQF-RECORD IDX
              PERFORM B-C THRU B-C-END.
           INITIALIZE TVA-RECORD.
           MOVE FI-EDIT-A TO TVA-ANNEE.
           MOVE FI-EDIT-M TO TVA-MOIS.
           CALL "6-TVA" USING LINK-V TVA-RECORD PV-KEY.
           MOVE TVA-TAUX(1) TO HE-Z2Z2.
           MOVE HE-Z2Z2 TO LINK-PC-TVA(1).

       LIGNES.
           MOVE TXT-TXT(IDX-1) TO LINK-TXT(IDX-1).

       FILL-EC.
           MOVE ECH-TEXTE(IDX) TO LINK-COMMENTAIRE(IDX).
       
       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE FR-KEY    TO HE-Z6.
           DISPLAY HE-Z6  LINE  6 POSITION 17.
           IF A-N = "A"
              DISPLAY FR-NOM LINE 6 POSITION 37 SIZE 43
           ELSE
              DISPLAY FR-NOM LINE 6 POSITION 25
           END-IF.
       DIS-HE-02.
           MOVE END-NUMBER TO HE-Z6.
           DISPLAY HE-Z6 LINE 7 POSITION 17.
       DIS-HE-05.
           MOVE TODAY-ANNEE TO HE-AA.
           MOVE TODAY-MOIS  TO HE-MM.
           MOVE TODAY-JOUR  TO HE-JJ.
           DISPLAY HE-DATE LINE 11 POSITION 23.
       DIS-HE-06.
           DISPLAY REEDITION LINE 13 POSITION 32.

       DIS-LIGNES.
           PERFORM D-L VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 5.
       DIS-HE-END.
           EXIT.

       D-L.
           COMPUTE LIN-IDX = 14 + IDX-1.
           DISPLAY TXT-TXT(IDX-1) LINE LIN-IDX POSITION 20 SIZE 60.

       AFFICHAGE-ECRAN.
           MOVE 1601 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY "DATE " LINE 11 POSITION 5 LOW.
           DISPLAY "R릱DITION " LINE 13 POSITION 5 LOW.
           DISPLAY "TEXTE" LINE 15 POSITION 5 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           CALL "FACT" USING LINK-V LINK-RECORD.
           ADD 1 TO COUNTER PAGES.
           MOVE 1 TO LIN-NUM.
           INITIALIZE LINK-RECORD. 

       END-PROGRAM.
           IF COUNTER > 0 
              MOVE 99 TO LNK-VAL
              CALL "FACT" USING LINK-V LINK-RECORD
           END-IF.
           CANCEL "FACT".
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V "N" FAKE-KEY.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXF".
           COPY "XDEC.CPY".
           IF EXC-KEY = 68
           OR EXC-KEY = 67
              IF EXC-KEY = 67
                 MOVE "D" TO LNK-YN
              ELSE
                 MOVE " " TO LNK-YN
              END-IF
              CALL "1-PARTXT" USING LINK-V
              CANCEL "1-PARTXT"
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              PERFORM DISPLAY-F-KEYS
              GO AVANT-DEC
           END-IF.
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".

