      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-PTEQJ AFFICHAGE PLANS PERSONNES PAR EQUIPE�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-PTEQJ.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.


       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "EQUIPE.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "V-VAR.CPY".
           COPY "PLANS.REC".
           COPY "CALEN.REC".
           COPY "PLAGE.REC".

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  CHOIX                 PIC X(10) VALUE SPACES.
       01  COMPTEUR              PIC 99.
       01  ACTION-CALL           PIC 9 VALUE 0.
       01  JOUR-IDX              PIC 99 VALUE 1.
       
       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
       01  HELP-2     PIC Z(4).
       01  HELP-2-R REDEFINES HELP-2.
           02 HELP-2A    PIC ZZ BLANK WHEN ZERO.
           02 HELP-2B    PIC XX.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-PTEQJ .

           PERFORM AFFICHAGE-ECRAN .
           MOVE 1 TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.
           PERFORM JSF.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1
           WHEN  2 PERFORM AVANT-2.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 09 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT EQ-NUMBER
             LINE  3 POSITION 15 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
          
       AVANT-2.
           IF JOUR-IDX < 1
              MOVE 1 TO JOUR-IDX
           END-IF.
           ACCEPT JOUR-IDX
             LINE  3 POSITION 52 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           
      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-EQUIPE" USING LINK-V EQ-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-EQUIPE
           END-EVALUATE.
           IF EQ-NUMBER NOT = 0
              PERFORM AFFICHAGE-DETAIL
              PERFORM AFFICHE-DEBUT THRU AFFICHE-END
           ELSE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-2.
           MOVE 0 TO LNK-NUM.
           IF EXC-KEY = 65
              SUBTRACT 1 FROM JOUR-IDX.
           IF EXC-KEY = 66
              ADD 1 TO JOUR-IDX.
           IF JOUR-IDX < 1
              MOVE 1 TO JOUR-IDX
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF JOUR-IDX > MOIS-JRS(LNK-MOIS) 
              MOVE MOIS-JRS(LNK-MOIS) TO JOUR-IDX
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF JOUR-IDX < 1
              MOVE 1 TO JOUR-IDX
              MOVE 2 TO LNK-NUM
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF JOUR-IDX >  MOIS-JRS(LNK-MOIS) 
              MOVE MOIS-JRS(LNK-MOIS) TO JOUR-IDX
              MOVE 2 TO LNK-NUM
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF JOUR-IDX > 0 
              PERFORM JSF
           END-IF.
           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

       JSF.
           MOVE JOUR-IDX TO HE-Z2.
           DISPLAY HE-Z2  LINE 3 POSITION 52.
           MOVE SEM-IDX(LNK-MOIS, JOUR-IDX) TO LNK-NUM
           ADD 100 TO LNK-NUM
           MOVE "SW" TO LNK-AREA
           MOVE 03551510 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V
           IF  CAL-JOUR(LNK-MOIS, JOUR-IDX) > 0
           AND CAL-JOUR(LNK-MOIS, JOUR-IDX) < 3
               MOVE "SL" TO LNK-AREA
               MOVE 24 TO LNK-NUM
               MOVE 03651510 TO LNK-POSITION
               CALL "0-DMESS" USING LINK-V
           ELSE
               DISPLAY SPACES LINE 3 POSITION 70 SIZE 10
           END-IF.

       NEXT-EQUIPE.
           CALL "6-EQUIPE" USING LINK-V EQ-RECORD EXC-KEY.

       DIS-HE-01.
           DISPLAY EQ-NUMBER LINE  3 POSITION 15.
           DISPLAY EQ-NOM    LINE  3 POSITION 20 SIZE 30.
       DIS-HE-END.
           EXIT.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE NOT-FOUND COMPTEUR CHOIX.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO NOT-FOUND SAVE-KEY.
           INITIALIZE REG-RECORD.
           PERFORM READ-EQ THRU READ-EQ-END.
       AFFICHE-END.
           EXIT.

       READ-EQ.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
           ELSE
      *       IF LIN-IDX > 4
      *          PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
      *          PERFORM INTERRUPT THRU INTERRUPT-END
      *       END-IF
              GO READ-EQ-END.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           IF CAR-IN-ACTIF = 1
              GO READ-EQ.
      *    IF CAR-EQUIPE NOT = EQ-NUMBER
      *       GO READ-EQ.
           INITIALIZE PT-RECORD.
           CALL "6-PLANS" USING LINK-V REG-RECORD PT-RECORD FAKE-KEY.
           IF PT-FIRME = 0
              GO READ-EQ.
           MOVE PT-PLAGE(JOUR-IDX) TO PLG-KEY.
           CALL "6-PLAGE" USING LINK-V PLG-RECORD FAKE-KEY.
           IF PLG-EQUIPE = EQ-NUMBER
              PERFORM DIS-DET-LIGNE
           ELSE
              GO READ-EQ.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              MOVE 5 TO LIN-IDX
           END-IF.
           IF EXC-KEY = 52 GO READ-EQ-END.
           GO READ-EQ.
       READ-EQ-END.
           PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX.
           ADD 1 TO COMPTEUR.
           MOVE REG-PERSON TO HE-Z4.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 1 LOW.
           DISPLAY PR-NOM LINE LIN-IDX POSITION 6.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "  ".
           ADD 7 TO COL-IDX.
           DISPLAY PR-PRENOM LINE LIN-IDX POSITION COL-IDX LOW.
           DISPLAY PT-PLAGE(JOUR-IDX) LINE LIN-IDX POSITION 30.
           DISPLAY PLG-NOM LINE LIN-IDX POSITION 41.
           MOVE PT-DEBUT(JOUR-IDX, 1) TO HELP-2.
           DISPLAY HELP-2A LINE LIN-IDX POSITION 55.
           INSPECT HELP-2B REPLACING ALL "00" BY "  ".
           DISPLAY HELP-2B LINE LIN-IDX POSITION 58.
           MOVE PT-FIN(JOUR-IDX, 1) TO HELP-2.
           DISPLAY HELP-2A LINE LIN-IDX POSITION 62.
           INSPECT HELP-2B REPLACING ALL "00" BY "  ".
           DISPLAY HELP-2B LINE LIN-IDX POSITION 65.
           MOVE PT-DEBUT(JOUR-IDX, 2) TO HELP-2.
           DISPLAY HELP-2A LINE LIN-IDX POSITION 69.
           INSPECT HELP-2B REPLACING ALL "00" BY "  ".
           DISPLAY HELP-2B LINE LIN-IDX POSITION 72.
           MOVE PT-FIN(JOUR-IDX, 2) TO HELP-2.
           DISPLAY HELP-2A LINE LIN-IDX POSITION 76.
           INSPECT HELP-2B REPLACING ALL "00" BY "  ".
           DISPLAY HELP-2B LINE LIN-IDX POSITION 79.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052000000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.

           ACCEPT CHOIX 
           LINE 23 POSITION 30 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.

       INTERRUPT-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 2302 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CANCEL "2-EQUIPE".
           CANCEL "6-EQUIPE".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

