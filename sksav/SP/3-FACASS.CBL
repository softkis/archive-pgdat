      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-FACASS IMPRESSION FACTURES CLIENTS        �
      *  � ASSISTANCE ->      IMPRESSION CHEZ LES CLIENTS        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-FACASS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  PRECISION             PIC 99 VALUE 1.
       01  BQF.
           02 B1 PIC X(50)
           VALUE "BCEE      BCEELULL       LU63 0019 2200 0715 8000".
           02 B2 PIC X(50) 
           VALUE "BGL       BGLLLULL       LU19 0030 5746 8894 0000".
           02 B3 PIC X(50)
           VALUE "BIL       BILLLULL       LU20 0028 1214 2700 0000".
           02 B4 PIC X(50)
           VALUE "CCR       CCRALULL       LU83 0090 0000 0691 7033".
       01  BQF-R REDEFINES BQF.
           02 BF OCCURS 4.
              03 BQ-NOM PIC X(10).
              03 BQ-BIC PIC X(15).
              03 BQ-COMPTE PIC X(25).

       01  PRIX.
           02 PX-P01 PIC 9(6) VALUE  250.
           02 PX-P02 PIC 9(6) VALUE  350.
           02 PX-P03 PIC 9(6) VALUE  500.
           02 PX-P04 PIC 9(6) VALUE  600.
           02 PX-P05 PIC 9(6) VALUE  800.
           02 PX-P06 PIC 9(6) VALUE 1000.
           02 PX-P07 PIC 9(6) VALUE 1250.
           02 PX-P08 PIC 9(6) VALUE 1500.
           02 PX-P09 PIC 9(6) VALUE 1750.
           02 PX-P10 PIC 9(6) VALUE 2000.
           02 PX-P11 PIC 9(6) VALUE 2500.
           02 PX-P12 PIC 9(6) VALUE 2750.
           02 PX-P13 PIC 9(6) VALUE 3000.
           02 PX-P14 PIC 9(6) VALUE 3500.
           02 PX-P15 PIC 9(6) VALUE 4000.
           02 PX-P16 PIC 9(6) VALUE 5000.
           02 PX-P17 PIC 9(6) VALUE 6000.
           02 PX-P18 PIC 9(6) VALUE 7000.
           02 PX-P19 PIC 9(6) VALUE 8000.
           02 PX-P20 PIC 9(6) VALUE 10000.

           02 PX-V01 PIC 9(4) VALUE 10.
           02 PX-V02 PIC 9(4) VALUE 15.
           02 PX-V03 PIC 9(4) VALUE 25.
           02 PX-V04 PIC 9(4) VALUE 40.
           02 PX-V05 PIC 9(4) VALUE 60.
           02 PX-V06 PIC 9(4) VALUE 80.
           02 PX-V07 PIC 9(4) VALUE 100.
           02 PX-V08 PIC 9(4) VALUE 200.
           02 PX-V09 PIC 9(4) VALUE 300.
           02 PX-V10 PIC 9(4) VALUE 400.
           02 PX-V11 PIC 9(4) VALUE 600.
           02 PX-V12 PIC 9(4) VALUE 750.
           02 PX-V13 PIC 9(4) VALUE 1000.
           02 PX-V14 PIC 9(4) VALUE 2000.
           02 PX-V15 PIC 9(4) VALUE 3000.
           02 PX-V16 PIC 9(4) VALUE 4000.
           02 PX-V17 PIC 9(4) VALUE 5000.
           02 PX-V18 PIC 9(4) VALUE 6000.
           02 PX-V19 PIC 9(4) VALUE 7000.
           02 PX-V20 PIC 9(4) VALUE 8000.

       01  PRIX-R REDEFINES PRIX.
           02 PX-REC-DET.
              03 PX-PRIX.
                 04 PX-V    PIC 9(6)  OCCURS 20.
              03 PX-CRENAUX.
                 04 PX-CR   PIC 9(4)  OCCURS 20.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "MESSAGE.REC".
           COPY "ARTICLE.REC".
           COPY "FACTURE.REC".
           COPY "FAC.LNK".

       01  PAGES                  PIC 99 VALUE 0.
       01  PDF                    PIC 99 VALUE 0.
       01  NO-FACTURE             PIC 9(8) VALUE 0.

           COPY "V-VH00.CPY".
           COPY "V-VAR.CPY".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z2Z2 PIC ZZ,ZZ.
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z5Z2 PIC Z(5),ZZ BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
        
       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "POCL.REC".

       PROCEDURE DIVISION USING LINK-V PC-RECORD.
             
       START-PROGRAMME-3-FACASS.
       
           CALL "0-TODAY" USING TODAY.
           CALL "4-STATFF" USING LINK-V.
           INITIALIZE FAC-RECORD.
           MOVE PC-NUMBER TO FAC-CLIENT HE-Z4.
           MOVE HE-Z4 TO LINK-REFERENCE.
           MOVE PC-FLAG-09 TO FAC-NUMBER HE-Z4.
           MOVE  6 TO IDX-1.
           STRING HE-Z4 DELIMITED BY SIZE INTO LINK-REFERENCE
              WITH POINTER IDX-1.
           MOVE LNK-VAL TO HE-Z4.
           MOVE 12 TO IDX-1.
           STRING HE-Z4 DELIMITED BY SIZE INTO LINK-REFERENCE
              WITH POINTER IDX-1.
           MOVE 1 TO PAGES IDX.
           PERFORM PRIX THRU PRIX-END.
           PERFORM LAYOUT.
           PERFORM TEXTES.
           PERFORM B-C VARYING IDX FROM 1 BY 1 UNTIL IDX > 4.
           PERFORM EDIT-CS.
           PERFORM TOTAUX.
           PERFORM PAGE-FIN.
           MOVE 9000 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           PERFORM AA.
           PERFORM TRANSMET.

       EDIT-CS.
           MOVE "2009" TO LINK-ART-A(1).
           MOVE "Assistance SM3" TO LINK-ART-B(1).

       TOTAUX.
           MOVE FAC-TOTAL TO HE-Z5Z2
           MOVE HE-Z5Z2 TO LINK-BASE(1).
           MOVE 110 TO IDX-1.
           STRING HE-Z5Z2 DELIMITED BY SIZE INTO 
              LINK-ART-B(1) WITH POINTER IDX-1.
           IF PC-TVA-PAYS = "LU"
           OR PC-TVA-PAYS = "  "
              COMPUTE FAC-TOTAL-TVA = FAC-TOTAL * ,15.
           MOVE FAC-TOTAL-TVA TO HE-Z5Z2
           MOVE HE-Z5Z2 TO LINK-TVA(1). 
           COMPUTE SH-00 = FAC-TOTAL + FAC-TOTAL-TVA.
           MOVE SH-00  TO HE-Z5Z2
           MOVE HE-Z5Z2 TO LINK-TTC(1) LINK-A-PAYER.


       LAYOUT.
           MOVE "SOFT-KIS s.� r.l" TO LINK-FIRM-NOM.
           MOVE 1 TO IDX-1.
           MOVE "68 RUE DE LUXEMBOURG" TO LINK-FIRM-ADR-01(2)
           MOVE "L-7540 ROLLINGEN" TO LINK-FIRM-ADR-01(3)

           MOVE "32 83 80" TO LINK-TEXTE(1).
           MOVE "32 79 84" TO LINK-TEXTE(2).
           MOVE "info@softkis.lu" TO LINK-TEXTE(3).
           MOVE "1990 2408 854"   TO LINK-TEXTE(4)

           MOVE "LU 14870373" TO LINK-TEXTE(5).
           MOVE "64 833" TO LINK-TEXTE(6).

           MOVE FAC-NUMBER TO HE-Z8.
           MOVE HE-Z8 TO LINK-NUM.
           MOVE 1 TO LINK-PAGE .
           MOVE PC-NUMBER TO HE-Z8.
           MOVE HE-Z8 TO LINK-NUM-CLIENT.
           MOVE 2009 TO HE-AA.
           MOVE 1 TO HE-MM.
           MOVE 2 TO HE-JJ.
           MOVE HE-DATE TO LINK-DATE.
           MOVE 7 TO HE-JJ.
           MOVE HE-DATE TO LINK-ECHEANCE.
           
           MOVE PC-NOM TO LINK-ADR-01(1).
           MOVE 1 TO IDX-1.
           STRING PC-MAISON DELIMITED BY "  " INTO LINK-ADR-01(2)
           WITH POINTER IDX-1.
           ADD 1 TO IDX-1.
           STRING PC-RUE DELIMITED BY "  " INTO LINK-ADR-01(2)
           WITH POINTER IDX-1.
           MOVE 1 TO IDX-1.
           STRING PC-PAYS DELIMITED BY "  " INTO LINK-ADR-01(3)
           WITH POINTER IDX-1.
           MOVE PC-CODE-POST TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE INTO LINK-ADR-01(3)
           WITH POINTER IDX-1.
           ADD 1 TO IDX-1.
           STRING PC-LOCALITE DELIMITED BY "  " INTO LINK-ADR-01(3)
           WITH POINTER IDX-1.
           IF PC-FACTURE-1 NOT = SPACES
              MOVE PC-FACTURE-1 TO LINK-ADR-01(3).
           MOVE PC-FACTURE-2 TO LINK-ADR-01(4).
           MOVE PC-FACTURE-3 TO LINK-ADR-01(5).
           MOVE PC-TVA       TO LINK-NO-TVA.
           
           
       TEXTES.
           MOVE "FACTURE"     TO LINK-ENTTX-01.
           MOVE "DATE"        TO LINK-DATE-TX.
           MOVE "NUMERO"      TO LINK-NUM-TX.
           MOVE "PAGE"        TO LINK-PAGE-TX.
           MOVE "CLIENT"      TO LINK-NUM-CLI-TX.
           MOVE "R괽굍ence � indiquer" TO LINK-REFER-TX.
           MOVE "Ech괶nce"    TO LINK-ECHEANCE-TX.
           MOVE "% TVA"       TO LINK-PC-TVA-TX
           MOVE "BASE"        TO LINK-BASE-TX
           MOVE "TVA"         TO LINK-MT-TVA-TX
           MOVE "TTC"         TO LINK-TTC-TX
           MOVE "A payer"     TO LINK-APAYER-TX
           MOVE "T굃굋hone"   TO LINK-DESC(1).
           MOVE "Fax"         TO LINK-DESC(2).
           MOVE "E-mail"      TO LINK-DESC(3).
           MOVE "Matricule"   TO LINK-DESC(4).
           MOVE "No TVA"      TO LINK-DESC(5).
           MOVE "R괾 de commerce" TO LINK-DESC(6).

           MOVE "Nos factures sont payables des r괹eption" TO 
           LINK-COMMENTAIRE(1).

           MOVE "Unsere Rechnungen sind f꼕lig bei Erhalt" TO 
           LINK-COMMENTAIRE(3).

       B-C.
           MOVE 1 TO IDX-1.
           STRING BQ-NOM(IDX) DELIMITED BY SIZE INTO 
           LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           MOVE 20 TO IDX-1.
           STRING BQ-BIC(IDX) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           MOVE 40 TO IDX-1.
           STRING BQ-COMPTE(IDX) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.

       PRIX.
           MOVE PX-V(IDX) TO FAC-TOTAL.
           IF LNK-VAL <= PX-CR(IDX) 
              GO PRIX-END
           END-IF.
           ADD 1 TO IDX.
           IF IDX < 19
              GO PRIX.
       PRIX-END.
           EXIT.

       PAGE-FIN.
           MOVE HE-DATE TO LINK-ECHEANCE.
           MOVE 15 TO HE-Z2Z2.
           MOVE HE-Z2Z2 TO LINK-PC-TVA(1).

       TRANSMET.
           MOVE "Y" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           CALL "FACT" USING LINK-V LINK-RECORD.
           MOVE 99 TO LNK-VAL.
           CALL "FACT" USING LINK-V LINK-RECORD.
           CANCEL "FACT".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

