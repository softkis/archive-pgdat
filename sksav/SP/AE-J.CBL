      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME AE-J EXPLOITATION COMPTA    ADEM            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    AE-J.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURNAL.FC".
           COPY "INTERCOM.FC".
           COPY "FORM215.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "JOURNAL.FDE".
           COPY "FORM215.FDE".

       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire JOURNAL COMPTABLE

       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-PERS   PIC 9(8).
              03 INTER-ANNEE  PIC 9999.
              03 INTER-MOIS   PIC 99.
              03 INTER-SUITE  PIC 99.

           02 INTER-REC-DET.
              03 INTER-VALUE PIC S9(7)V99 OCCURS 20.
              03 INTER-COMPTEUR PIC 9999.
              03 INTER-FLAG   PIC X.
              03 INTER-NUMBER   PIC 9999.


       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  PRECISION             PIC 9 VALUE 2.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "CARRIERE.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "BANQP.REC".
           COPY "COUT.REC".
           COPY "IMPRLOG.REC".
           COPY "PLAN.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(215) OCCURS 45.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(12) VALUE "REEMPLOI.RAP".

       01  PROC-IDX        PIC 9 VALUE 0.
       01  DC-IDX          PIC 9.
       01  NOT-OPEN        PIC 9 VALUE 0.
       01  NO-JOURNAL            PIC 9(8) VALUE 0.
       01  DATE-MAJ.
           02 D-MAJ-A         PIC 9999.
           02 D-MAJ-M         PIC 99.
           02 D-MAJ-J         PIC 99.

       01  JOURNAL-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-JO".
           02 FIRME-JOURNAL      PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 ANNEE-JOURNAL      PIC 999.

       01  INTER-NAME.
           02 FILLER             PIC XXXX VALUE "IRUB".
           02 FIRME-INTER        PIC 9999.
           02 FILLER             PIC XXXX VALUE ".ADE".

       01  CUMUL.
           02 CUMUL-D            PIC S9(8)V99.
           02 CUMUL-C            PIC S9(8)V99.
       01  CUMUL-R REDEFINES CUMUL.
           02 CUMUL-DC           PIC S9(8)V99 OCCURS 2.

           COPY "V-VAR.CPY".
        
       01  COMPTEUR              PIC 99 COMP-1.
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  HELP-COMPTE           PIC X(10).
       01  HELP-COMPTE-R REDEFINES HELP-COMPTE.
           02 INDEX-COMPTE       PIC 99.
           02 RESTE-COMPTE       PIC X(8).
       01  MISE-A-JOUR           PIC X VALUE "N".
       01  REEDITION             PIC X VALUE "N".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.
       01  MOIS-SAVE             PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               JOURNAL 
               FORM 
               INTER.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-AE-J.
       
           INITIALIZE PLAN-RECORD INTER-RECORD.
           MOVE 5   TO PLAN-NUMERO.
           CALL "6-PLAN" USING LINK-V PLAN-RECORD FAKE-KEY.
           MOVE 45 TO IMPL-MAX-LINE.
           MOVE LNK-SUFFIX TO ANNEE-JOURNAL.
           MOVE FR-KEY TO FIRME-JOURNAL FIRME-INTER.
           OPEN I-O   JOURNAL.
           DELETE FILE INTER.
           OPEN I-O INTER WITH LOCK.
           MOVE 1 TO NOT-OPEN.

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-MOIS TO MOIS-SAVE MOIS-FIN.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-NUMBER
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4
           WHEN  5 PERFORM APRES-5
           WHEN  6 PERFORM APRES-DATE
           WHEN  7 PERFORM APRES-7
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-4.
           IF REG-PERSON NOT = END-NUMBER
           ACCEPT COUT
             LINE 10 POSITION 29 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE 
             MOVE 0 TO COUT
           END-IF.

       AVANT-5.
           ACCEPT MISE-A-JOUR 
             LINE 15 POSITION 35 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-6.
           MOVE 17260000 TO LNK-POSITION.
           IF MISE-A-JOUR = "N"
           CALL "0-GDATE" USING DATE-MAJ
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY
           ELSE
              INITIALIZE DATE-MAJ
           END-IF.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO D-MAJ-A
              MOVE TODAY-MOIS  TO D-MAJ-M
              MOVE TODAY-JOUR  TO D-MAJ-J
              MOVE 0 TO LNK-NUM.

       AVANT-7.
           IF MISE-A-JOUR = "N"
           AND D-MAJ-A = 0
           ACCEPT REEDITION 
             LINE 19 POSITION 35 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE
              MOVE "N" TO REEDITION
           END-IF.

       AVANT-NUMBER.
           ACCEPT NO-JOURNAL
             LINE 20 POSITION 32 SIZE 8
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-4.
           MOVE COUT TO COUT-NUMBER.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-COUT" USING LINK-V
                    IF LNK-VAL > 0
                       MOVE LNK-VAL TO COUT
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   2 CALL "2-COUT" USING LINK-V COUT-RECORD
                    IF COUT-NUMBER > 0
                       MOVE COUT-NUMBER TO COUT
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-COUT
                    MOVE COUT-NUMBER TO COUT
           END-EVALUATE.
           PERFORM DIS-HE-04.

       NEXT-COUT.
           CALL "6-COUT" USING LINK-V COUT-RECORD SAVE-KEY.

       APRES-5.
           IF MISE-A-JOUR = "J"
           OR MISE-A-JOUR = "O"
           OR MISE-A-JOUR = "Y"
           OR MISE-A-JOUR = "N" 
              CONTINUE
           ELSE
              MOVE "N" TO MISE-A-JOUR
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-04.

       APRES-7.
           IF REEDITION = "J"
           OR REEDITION = "O"
           OR REEDITION = "Y"
           OR REEDITION = "N" 
              CONTINUE
           ELSE
              MOVE "N" TO REEDITION
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-07.

       APRES-DATE.
           IF LNK-NUM NOT = 0
              MOVE "AA" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-06.

       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TRAITEMENT.
           MOVE MOIS-COURANT TO LNK-MOIS.
           INITIALIZE CUMUL.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           PERFORM DIS-HE-01.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           MOVE MOIS-SAVE TO LNK-MOIS.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           IF COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           PERFORM CUMUL-JOURNAL.
       READ-PERSON-1.
           IF  REG-PERSON   < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.
           PERFORM FULL-PROCESS.

       CUMUL-VALEURS.
           MOVE 1 TO DC-IDX.
           PERFORM PROC VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 200.
           MOVE 2 TO DC-IDX.
           PERFORM PROC VARYING IDX-1 FROM 201 BY 1 UNTIL IDX-1 > 300.

       PROC.
           IF JRL-V(IDX-1) NOT =  0
              MOVE JRL-V(IDX-1) TO VH-00
              MOVE PLAN-COM(IDX-1) TO HELP-COMPTE
              MOVE INDEX-COMPTE TO IDX-4
              IF IDX-4 > 0
                 PERFORM WRITE-INTER
              END-IF
           END-IF.

       WRITE-INTER.
           INITIALIZE INTER-RECORD.
           MOVE JRL-PERS   TO INTER-PERS.
           MOVE LNK-ANNEE  TO INTER-ANNEE.
           MOVE JRL-MOIS   TO INTER-MOIS.
           MOVE JRL-SUITE  TO INTER-SUITE.
           MOVE JRL-NUMERO TO INTER-NUMBER.
           IF  JRL-ANNEE-C > 0 
           AND REEDITION NOT = "N"
              MOVE "*" TO INTER-FLAG.

           PERFORM WRITE-REC-INTER.
        
       WRITE-REC-INTER.
           READ INTER INVALID CONTINUE.
           ADD VH-00 TO INTER-VALUE(IDX-4).
           WRITE INTER-RECORD INVALID REWRITE INTER-RECORD END-WRITE.
           INITIALIZE INTER-RECORD.
           MOVE JRL-PERS  TO INTER-PERS.
           MOVE 9999      TO INTER-ANNEE.
           READ INTER INVALID CONTINUE.
           ADD VH-00 TO INTER-VALUE(IDX-4),
           ADD 1 TO INTER-COMPTEUR.
           WRITE INTER-RECORD INVALID REWRITE INTER-RECORD END-WRITE.

           INITIALIZE INTER-RECORD.
           MOVE 999999 TO INTER-PERS.
           MOVE 9999   TO INTER-ANNEE.
           READ INTER INVALID CONTINUE.
           ADD VH-00 TO INTER-VALUE(IDX-4),
           ADD 1 TO INTER-COMPTEUR.
           WRITE INTER-RECORD INVALID REWRITE INTER-RECORD END-WRITE.

       CUMUL-JOURNAL.
           INITIALIZE JRL-RECORD.
           MOVE REG-PERSON TO JRL-PERS.
           START JOURNAL KEY > JRL-KEY INVALID CONTINUE
              NOT INVALID
              PERFORM LECTURE-JOURNAL THRU LECTURE-JOURNAL-END.

       LECTURE-JOURNAL.
           READ JOURNAL NEXT AT END GO LECTURE-JOURNAL-END.
           IF  NO-JOURNAL NOT = 0
           AND JRL-NUMERO NOT = NO-JOURNAL 
               GO LECTURE-JOURNAL.
           IF  JRL-ANNEE-C > 0 
           AND MISE-A-JOUR NOT = "N"
              GO LECTURE-JOURNAL
           END-IF.
           IF  JRL-ANNEE-C > 0 
           AND MISE-A-JOUR = "N"
           AND D-MAJ-A = 0
           AND REEDITION = "N"
              GO LECTURE-JOURNAL
           END-IF.
           IF REG-PERSON NOT = JRL-PERS
              GO LECTURE-JOURNAL-END.
           IF JRL-MOIS > MOIS-FIN
              GO LECTURE-JOURNAL-END.
           IF D-MAJ-A > 0
           AND DATE-MAJ NOT = JRL-DATE-COMPTA
              GO LECTURE-JOURNAL
           END-IF.
           MOVE JRL-MOIS TO LNK-MOIS.
           PERFORM CUMUL-VALEURS.
           PERFORM DIS-HE-01.
           IF MISE-A-JOUR NOT = "N"
              MOVE TODAY-ANNEE TO JRL-ANNEE-C
              MOVE TODAY-MOIS  TO JRL-MOIS-C
              MOVE TODAY-JOUR  TO JRL-JOUR-C
              WRITE JRL-RECORD INVALID REWRITE JRL-RECORD END-WRITE.
           GO LECTURE-JOURNAL.
       LECTURE-JOURNAL-END.
           EXIT.

       FULL-PROCESS.
           INITIALIZE INTER-RECORD REG-RECORD.
           START INTER KEY > INTER-KEY INVALID CONTINUE
            NOT INVALID
           PERFORM READ-INTER THRU READ-INTER-END.
           
           IF COUNTER > 0 
           AND LIN-NUM > 0
              PERFORM TRANSMET
           END-IF.

       READ-INTER.
           READ INTER NEXT AT END GO READ-INTER-END.
           IF COUNTER = 0 
              ADD 1 TO COUNTER
              PERFORM READ-FORM
              PERFORM ENTETE
              COMPUTE LIN-NUM = LIN-IDX 
           END-IF.
           IF LIN-NUM >= 45
              PERFORM TRANSMET
              PERFORM READ-FORM
              PERFORM ENTETE
              COMPUTE LIN-NUM = LIN-IDX 
           END-IF.
           PERFORM PRT-PERS.
           GO READ-INTER.
       READ-INTER-END.
           EXIT.


       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       ENTETE.
           COMPUTE LIN-NUM = 4.
           MOVE 10 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 2.
           MOVE  6 TO CAR-NUM.
           MOVE 120 TO COL-NUM.
           ADD 1 TO PAGE-NUMBER.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 45 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 50 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 3.
           MOVE 45 TO COL-NUM.
           MOVE FR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 50 TO COL-NUM.
           MOVE FR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 4.
           MOVE 45 TO COL-NUM.
           MOVE FR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  5 TO CAR-NUM.
           MOVE 48 TO COL-NUM.
           MOVE FR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           MOVE 54 TO COL-NUM.
           MOVE FR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
      *    DATE EDITION
           MOVE 110 TO COL-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE TODAY-JOUR TO VH-00.
           PERFORM FILL-FORM.
           MOVE 113 TO COL-NUM.
           MOVE TODAY-MOIS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 116 TO COL-NUM.
           MOVE TODAY-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.


       PRT-PERS.
           ADD 1 TO LIN-NUM.
           IF  INTER-PERS NOT = REG-PERSON
           AND INTER-PERS NOT = 999999
           AND INTER-ANNEE NOT = 9999
              IF LIN-NUM >= 45
                 PERFORM TRANSMET
                 PERFORM READ-FORM
                 PERFORM ENTETE
                 COMPUTE LIN-NUM = LIN-IDX 
              END-IF
              PERFORM PRT-NOM
              ADD 1 TO LIN-NUM
           END-IF.

           IF INTER-PERS = 999999
              MOVE 8 TO COL-NUM
              MOVE "Total" TO ALPHA-TEXTE
              PERFORM FILL-FORM.

           IF INTER-ANNEE < 9999
              MOVE INTER-FLAG TO ALPHA-TEXTE
              MOVE 1 TO COL-NUM
              PERFORM FILL-FORM

              MOVE INTER-ANNEE TO VH-00
              MOVE 4 TO CAR-NUM
              MOVE 6 TO COL-NUM
              PERFORM FILL-FORM
              MOVE INTER-MOIS TO VH-00
              MOVE 2 TO CAR-NUM
              MOVE 3 TO COL-NUM
              PERFORM FILL-FORM
              COMPUTE VH-00 = INTER-SUITE
              MOVE  1 TO CAR-NUM
              MOVE 11 TO COL-NUM
              PERFORM FILL-FORM
              COMPUTE VH-00 = INTER-NUMBER
              MOVE  4 TO CAR-NUM
              MOVE 170 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.
           COMPUTE INTER-VALUE(15) = INTER-VALUE(4) 
                                   + INTER-VALUE(10)
                                   + INTER-VALUE(11)
                                   + INTER-VALUE(12)
                                   + INTER-VALUE(13)
                                   + INTER-VALUE(14).
           MOVE 19 TO COL-NUM.
           IF INTER-ANNEE < 9999
           OR INTER-PERS = 999999
              PERFORM D-C VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 17
           ELSE
              IF INTER-COMPTEUR > 17
              AND INTER-PERS < 999999
                 PERFORM D-C VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 17
                 ADD 1 TO LIN-NUM
              END-IF
           END-IF.

       PRT-NOM.
           MOVE INTER-PERS TO REG-PERSON VH-00.
           MOVE 6 TO CAR-NUM.
           MOVE 1 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           PERFORM GET-PERS.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           ADD 5 TO COL-NUM.
           MOVE PR-NAISS-A TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-NAISS-M TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-NAISS-J TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-SNOCS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           INITIALIZE BQP-RECORD.
           CALL "6-BANQP" USING LINK-V REG-RECORD BQP-RECORD FAKE-KEY.
           MOVE BQP-BANQUE TO ALPHA-TEXTE.
           ADD 5 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE BQP-COMPTE TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

        D-C.
           MOVE INTER-VALUE(IDX-1) TO VH-00.
           MOVE  5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           COMPUTE COL-NUM = IDX-1 * 9 + 4.
           IF VH-00 > 0
              PERFORM FILL-FORM
           END-IF.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 27 SIZE 6
           DISPLAY PR-NOM LINE 6 POSITION 47 SIZE 33.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 48 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 6 POSITION COL-IDX SIZE IDX LOW.
           IF A-N = "N"
              DISPLAY SPACES LINE 6 POSITION 35 SIZE 10.
       DIS-HE-04.
           MOVE COUT-NUMBER TO HE-Z4.
           DISPLAY HE-Z4  LINE 10 POSITION 29.
           DISPLAY COUT-NOM LINE 10 POSITION 35 SIZE 45.
       DIS-HE-05.
           DISPLAY MISE-A-JOUR LINE 15 POSITION 35.
       DIS-HE-06.
           MOVE D-MAJ-A TO HE-AA.
           IF HE-AA = SPACES
              INITIALIZE DATE-MAJ.
           MOVE D-MAJ-M TO HE-MM.
           MOVE D-MAJ-J TO HE-JJ.
           DISPLAY HE-DATE LINE 17 POSITION 26.
       DIS-HE-07.
           DISPLAY REEDITION LINE 19 POSITION 35.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1403 TO LNK-VAL.
           MOVE "ADEM" TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L215" USING LINK-V FORMULAIRE.
           MOVE 0 TO LIN-NUM
           ADD 1 TO COUNTER.

       END-PROGRAM.
           IF COUNTER > 0 
              MOVE 99 TO LNK-VAL
              CALL "L215" USING LINK-V FORMULAIRE
           END-IF.
           CANCEL "L215".
           CLOSE JOURNAL.
           MOVE MOIS-SAVE TO LNK-MOIS.
           IF NOT-OPEN = 1
              CLOSE INTER
              DELETE FILE INTER.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XMOISNOM.CPY".


