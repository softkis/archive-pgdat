      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-HRLL FICHIER HEURES LUXLAIT               �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-HRLL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "LIVRE.FC".
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.


           COPY "LIVRE.FDE".

       FD  TF-TRANS
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.

       01  TF-RECORD.
           02 TF-ANNEE      PIC ZZZZZ.
           02 TF-FILLER     PIC X.
           02 TF-MOIS       PIC ZZZZ.
           02 TF-FILLER     PIC X.
           02 TF-PERSON     PIC Z(8).
           02 TF-FILLER     PIC X.
           02 TF-NOM        PIC X(35).
           02 TF-FILLER     PIC X.
           02 TF-CODE       PIC Z(8).
           02 TF-FILLER     PIC X.
           02 TF-PC-NOM     PIC X(25).
           02 TF-FILLER     PIC X.
           02 TF-STAT       PIC XXX.
           02 TF-FILLER     PIC X.
           02 TF-REG        PIC Z(8),ZZ.
           02 TF-FILLER     PIC X.
           02 TF-FER        PIC Z(8),ZZ.
           02 TF-FILLER     PIC X.
           02 TF-CGE        PIC Z(8),ZZ.
           02 TF-FILLER     PIC X.
           02 TF-MAL        PIC Z(8),ZZ.
           02 TF-FILLER     PIC X.
           02 TF-ACC        PIC Z(8),ZZ.
           02 TF-FILLER     PIC X.
           02 TF-SUP        PIC Z(8),ZZ.
           02 TF-FILLER     PIC X.
           02 TF-MOB        PIC Z(8),ZZ.
           02 TF-FILLER     PIC X.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01  NOT-OPEN   PIC 9 VALUE 0.
       01  NOT-OPEN-1 PIC 9 VALUE 0.
       01  TXT-RECORD.
           02 TXT-ANNEE           PIC X(5) VALUE "ANNEE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-MOIS            PIC X(4) VALUE "MOIS".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-PERS            PIC X(8) VALUE "PERSONNE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-NOM             PIC X(35) VALUE "NOM".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-CODE            PIC X(8) VALUE "CODE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-PC-NOM          PIC X(25) VALUE "POSTE DE FRAIS".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-OE              PIC X(3) VALUE "E/O".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-REG             PIC X(11) VALUE "REGULIER".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-FER             PIC X(11) VALUE "FERIE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-CGE             PIC X(11) VALUE "CONGE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-MAL             PIC X(11) VALUE "MALADIE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-ACC             PIC X(11) VALUE "ACCIDENT".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-SUP             PIC X(11) VALUE "HRS SUPPL".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-MOB             PIC X(11) VALUE "MOBILE ".
           02 TXT-FILLER          PIC X VALUE ";".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "REGISTRE.REC".
           COPY "PERSON.REC".
           COPY "POCL.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "LIVRE.LNK".
           COPY "JOURS.REC".
       
       01  LIVRE-NAME.
           02 FILLER             PIC X(8) VALUE "S-LIVRE.".
           02 ANNEE-LIVRE        PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z8 PIC -Z(8),ZZ.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TF-TRANS LIVRE.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-HRLL.
       

           IF NOT-OPEN = 0
              MOVE LNK-SUFFIX TO ANNEE-LIVRE
              OPEN INPUT LIVRE
              INITIALIZE REG-RECORD
              MOVE 1 TO NOT-OPEN
           END-IF.


           IF LNK-VAL = 99
              PERFORM END-PROGRAM
           ELSE  
              PERFORM RECHERCHE
              EXIT PROGRAM
           END-IF.



       END-PROGRAM.
           IF NOT-OPEN-1 = 1
              CLOSE TF-TRANS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

       RECHERCHE.
           INITIALIZE L-RECORD.
           MOVE FR-KEY     TO L-FIRME-2.
           MOVE PARMOD-MOIS-DEB  TO L-MOIS-2.
           START LIVRE KEY >= L-KEY-2 INVALID CONTINUE
               NOT INVALID PERFORM READ-L THRU READ-L-END.

       READ-L.
           READ LIVRE NEXT NO LOCK AT END
               GO READ-L-END
           END-READ.
           IF FR-KEY NOT = L-FIRME
           OR L-MOIS > PARMOD-MOIS-FIN
              GO READ-L-END
           END-IF.
           IF L-SUITE NOT = 0
              GO READ-L
           END-IF.
           PERFORM FILL-TEXTE.
           ADD 1 TO STORE.
           GO READ-L.
       READ-L-END.
           EXIT.

       DONNEES-PERSONNE.
           MOVE L-PERSON TO REG-PERSON
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO TF-NOM.

           
       FILL-TEXTE.
           IF NOT-OPEN-1 = 0
              OPEN OUTPUT TF-TRANS
              WRITE TF-RECORD FROM TXT-RECORD
              MOVE 1 TO NOT-OPEN-1
           END-IF.
           INITIALIZE TF-RECORD SH-01.
           INSPECT TF-RECORD  REPLACING ALL " " BY ";".
           IF L-STATUT = 1 
              MOVE "O" TO TF-STAT
           ELSE
              MOVE "E" TO TF-STAT
           END-IF.
           PERFORM DONNEES-PERSONNE.
           MOVE L-RECORD TO LINK-RECORD.
           PERFORM TEST-ACC THRU TEST-END.
           MOVE LNK-ANNEE     TO TF-ANNEE.
           MOVE L-MOIS        TO TF-MOIS. 
           MOVE L-PERSON      TO TF-PERSON.
           MOVE L-UNI-SALAIRE TO TF-REG.
           COMPUTE SH-00 = L-UNI-MOBIL-PLUS + L-UNI-HRS-SUP.
           MOVE SH-00 TO TF-SUP.
           COMPUTE SH-00 = L-UNI-FERIE + L-UNI-FERIE-R
           MOVE SH-00 TO TF-FER.
           COMPUTE SH-00 = L-UNI-CONGE 
21                       + L-UNI-EXTRA   
22                       + L-UNI-NOCES   
23                       + L-UNI-NAISS   
24                       + L-UNI-MARIAGE 
25                       + L-UNI-DEMENAG 
26                       + L-UNI-DECES   
27                       + L-UNI-BENEVOLE
28                       + L-UNI-CONGE-SUPPL.
           MOVE SH-00 TO TF-CGE.
           COMPUTE SH-00 = L-UNIT(1)
                         + L-UNIT(2)   
                         + L-UNIT(3)   
                         + L-UNIT(4)   
                         + L-UNIT(5)   
                         + L-UNIT(6)   
                         + L-UNIT(7)   
                         + L-UNIT(8)   
                         + L-UNIT(9)   
                         + L-UNIT(11)
ACC                      - SH-01.
           MOVE SH-00 TO TF-MAL.
           MOVE SH-01 TO TF-ACC.
           MOVE L-UNI-MOBIL-MIN TO TF-MOB.
           MOVE L-MOIS TO LNK-MOIS.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           MOVE CAR-POSTE-FRAIS TO PC-NUMBER TF-CODE.
           MOVE FR-KEY TO PC-FIRME.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY.
           MOVE PC-NOM TO TF-PC-NOM.
           WRITE TF-RECORD.

           COPY "XACTION.CPY".

       TEST-ACC.
           CALL "4-NXLP" USING LINK-V REG-RECORD LINK-RECORD NX-KEY.
           IF LNK-MOIS   NOT = LINK-MOIS
           OR REG-PERSON NOT = LINK-PERSON 
              GO TEST-END.
           IF LINK-FLAG(2) > 0
              ADD LINK-UNITE(100) TO SH-01.
           GO TEST-ACC.
       TEST-END.
           INITIALIZE JRS-RECORD.
           MOVE 6 TO JRS-OCCUPATION.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD FAKE-KEY.
           IF LNK-ANNEE > 2008
              ADD JRS-HRS(32) TO SH-01.
