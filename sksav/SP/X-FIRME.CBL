      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME X-FIRME  FIRME                                      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    X-FIRME.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL .
           COPY "FIRME.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FIRME.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "OCCOM.REC".
           COPY "COUT.REC".
        
       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z2Z2 PIC ZZ,ZZ. 
           02 HE-Z6Z2 PIC Z(6),ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FIRME.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-X-FIRME.
       
           PERFORM AFFICHAGE-ECRAN .

           OPEN I-O   FIRME.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0000000025 TO EXC-KFR(1).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.

           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT.
           INITIALIZE FIRME-RECORD.
           START FIRME KEY >= FIRME-KEY INVALID EXIT PROGRAM
              NOT INVALID
              PERFORM C-C THRU C-C-END.
       C-C.
           READ FIRME NEXT NO LOCK AT END 
              GO C-C-END.
      *    PERFORM CREATE-MODIF.
           INITIALIZE FIRME-REMARQUE.
           REWRITE FIRME-RECORD INVALID CONTINUE.
           GO C-C.
       C-C-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 0 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

       CREATE-MODIF.
           MOVE FR-KEY TO IDX-1.
           MOVE FIRME-KEY TO FR-KEY.
           CALL "4-MCCOL" USING LINK-V.
           CANCEL "4-MCCOL".
           MOVE IDX-1 TO FR-KEY.
           INITIALIZE OCO-RECORD.
           MOVE FIRME-KEY TO OCO-FIRME.
           MOVE 11 TO OCO-NUMBER.
           MOVE "F굍i�" TO OCO-NOM.
           PERFORM WRITE-OCCOM.
           MOVE 20 TO OCO-NUMBER.
           MOVE "Dimanche" TO OCO-NOM.
           PERFORM WRITE-OCCOM.
           MOVE 1 TO OCO-NUMBER OCO-NUIT.
           MOVE "Nuit" TO OCO-NOM.
           PERFORM WRITE-OCCOM.
           INITIALIZE COUT-RECORD.
           MOVE FIRME-KEY TO COUT-FIRME.
           MOVE 1 TO COUT-NUMBER.
           MOVE "Ouvriers" TO COUT-NOM.
           PERFORM WRITE-COUT.
           MOVE 2 TO COUT-NUMBER.
           MOVE "Employ굎" TO COUT-NOM.
           PERFORM WRITE-COUT.
           MOVE 3 TO COUT-NUMBER.
           MOVE "Exploitant" TO COUT-NOM.
           PERFORM WRITE-COUT.

       WRITE-COUT.
           CALL "6-COUT" USING LINK-V COUT-RECORD WR-KEY.
      
       WRITE-OCCOM.
           CALL "6-OCCOM" USING LINK-V OCO-RECORD WR-KEY.
