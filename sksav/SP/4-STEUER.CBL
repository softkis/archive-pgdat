      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-STEUER AJUSTEMENT IMPOSABLE               �
      *  � CHAUFFEURS ALLEMADS                                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-STEUER.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 3.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "LIVRE.REC".
           COPY "LIVRE.LNK".
           COPY "CODPAIE.REC".
           COPY "TAUXSS.REC".
           COPY "CARRIERE.REC".

       01  HEURES.
           02 HRS-ETR   PIC 9(4)V99.
           02 HRS-TRAV  PIC 9(4)V99.
           02 HRS-LIB   PIC 9(4)V99 OCCURS 13.
        
       01  HELP-1      PIC 9(6)V99.
  
       01   ECR-DISPLAY.
            02 HE-Z2 PIC ZZ  BLANK WHEN ZERO.
            02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4).
            02 HE-Z6 PIC Z(6).
            02 HE-Z8 PIC Z(8)-.
            02 HE-Z2Z2 PIC ZZ,ZZ. 
            02 HE-Z4Z2 PIC -ZZZZ,ZZ BLANK WHEN ZERO. 
            02 HE-Z6Z2 PIC Z(6),ZZ. 
            02 HE-Z6Z4 PIC Z(6),Z(4). 
            02 HE-Z8Z3 PIC Z(8),ZZZ BLANK WHEN ZERO.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".

       PROCEDURE DIVISION USING LINK-V REG-RECORD PRESENCES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-STEUER .

       
           IF LNK-ANNEE < 2006
              EXIT PROGRAM.
           PERFORM CORRECTION.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

       CORRECTION.
           INITIALIZE L-RECORD HEURES LNK-SUITE.
           MOVE LNK-MOIS TO SAVE-MOIS.
           PERFORM ARCHIVES VARYING LNK-MOIS 
              FROM 1 BY 1 UNTIL  LNK-MOIS > SAVE-MOIS.
           IF HRS-ETR = 0
              MOVE SAVE-MOIS TO LNK-MOIS
              EXIT PROGRAM.
           COMPUTE SH-01 = 1 / HRS-TRAV * HRS-ETR.
           PERFORM TEST-MOIS VARYING LNK-MOIS 
                   FROM 1 BY 1 UNTIL  LNK-MOIS > SAVE-MOIS.
           MOVE SAVE-MOIS TO LNK-MOIS.

       ARCHIVES.
           INITIALIZE L-RECORD.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-MOIS NOT = 0
              PERFORM ARCHIVES-ADD
           END-IF. 

       ARCHIVES-ADD.
           ADD L-UNI-ETRANGER     TO HRS-ETR.
           ADD L-UNI-MALADIES     TO HRS-TRAV.
           ADD L-UNI-SALAIRE      TO HRS-TRAV.

           ADD L-UNI-FERIE        TO HRS-LIB(L-MOIS) HRS-LIB(13).
           ADD L-UNI-FERIE-R      TO HRS-LIB(L-MOIS) HRS-LIB(13).
           ADD L-UNI-CONGE        TO HRS-LIB(L-MOIS) HRS-LIB(13).
           ADD L-UNI-EXTRA        TO HRS-LIB(L-MOIS) HRS-LIB(13).
           ADD L-UNI-NOCES        TO HRS-LIB(L-MOIS) HRS-LIB(13).
           ADD L-UNI-NAISS        TO HRS-LIB(L-MOIS) HRS-LIB(13).
           ADD L-UNI-MARIAGE      TO HRS-LIB(L-MOIS) HRS-LIB(13).
           ADD L-UNI-DEMENAG      TO HRS-LIB(L-MOIS) HRS-LIB(13).
           ADD L-UNI-DECES        TO HRS-LIB(L-MOIS) HRS-LIB(13).
           ADD L-UNI-BENEVOLE     TO HRS-LIB(L-MOIS) HRS-LIB(13).
           ADD L-UNI-CONGE-SUPPL  TO HRS-LIB(L-MOIS) HRS-LIB(13).
           
       TEST-MOIS.
           IF HRS-LIB(LNK-MOIS) > 0
              PERFORM RECAL-MOIS
           END-IF.

       RECAL-MOIS.
           INITIALIZE L-RECORD.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           MOVE L-RECORD TO LINK-RECORD.
           INITIALIZE CSP-RECORD CAR-RECORD SS-RECORD.
           MOVE REG-PERSON TO CSP-PERSON.
           COMPUTE CSP-CODE = 487.
           MOVE LNK-MOIS TO CSP-MOIS.
           COMPUTE CSP-DONNEE(7) =
                   L-MNT-FERIE       +
                   L-MNT-FERIE-R     +
                   L-MNT-CONGE       +
                   L-MNT-EXTRA       +
                   L-MNT-NOCES       +
                   L-MNT-NAISS       +
                   L-MNT-MARIAGE     +
                   L-MNT-DEMENAG     +
                   L-MNT-DECES       +
                   L-MNT-BENEVOLE    +
                   L-MNT-CONGE-SUPPL +
                   L-MNT-CONGE-MOYENNE.
           MOVE SH-01 TO CSP-DONNEE(8).
           COMPUTE CSP-DONNEE(1) = CSP-DONNEE(7) * CSP-DONNEE(8).
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           MOVE CAR-REGIME TO SS-REGIME.
           CALL "6-TSS" USING LINK-V SS-RECORD.
           MOVE SS-TX(1) TO CSP-DONNEE(2).
           MOVE SS-TX(2) TO CSP-DONNEE(3).
           COMPUTE HELP-1 = CSP-DONNEE(1) * (100 - SS-TX(1) - SS-TX(2)) 
                         / 100 + ,005.
           MOVE HELP-1 TO CSP-DONNEE(4).
           COMPUTE CSP-DONNEE(6) = CSP-DONNEE(1).
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.
           
           MOVE 0 TO LNK-SUITE.
           CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES.
           INITIALIZE L-RECORD.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           COMPUTE SH-00 = LINK-DC(200) - L-DEB(200)
                         - LINK-DC(300) + L-CRE(100).

           IF SH-00 NOT = 0
              MOVE SH-00 TO HELP-1 HE-Z4Z2
              IF LNK-MOIS NOT = SAVE-MOIS
                 PERFORM AJUSTE
              END-IF
              CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES
           END-IF.

       AJUSTE.
           INITIALIZE CSP-RECORD.
           MOVE REG-PERSON TO CSP-PERSON.
           IF SH-00 > 0
              COMPUTE  CSP-CODE = 1000 + LNK-MOIS
              MOVE SAVE-MOIS TO CSP-MOIS
              PERFORM WRITE-CSP
              COMPUTE  CSP-CODE = 2000 + SAVE-MOIS
              MOVE LNK-MOIS TO CSP-MOIS
              PERFORM WRITE-CSP
           ELSE
              COMPUTE  CSP-CODE = 2000 + LNK-MOIS
              MOVE SAVE-MOIS TO CSP-MOIS
              PERFORM WRITE-CSP
              COMPUTE  CSP-CODE = 1000 + SAVE-MOIS
              MOVE LNK-MOIS TO CSP-MOIS
              PERFORM WRITE-CSP
           END-IF.

       WRITE-CSP.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           ADD HELP-1 TO CSP-TOTAL.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.


           COPY "XACTION.CPY".
