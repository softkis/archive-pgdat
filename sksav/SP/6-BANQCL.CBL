      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-BANQCL MODULE GENERAL LECTURE BANQUE CLIENT�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-BANQCL.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "BANQCL.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "BANQCL.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  IDX-1    PIC 9 VALUE 0.
       01  IDX-2    PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "BANQCL.LNK".
           COPY "POCL.REC".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V PC-RECORD LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON BANQCL.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-REG.
       
           IF NOT-OPEN = 0
              OPEN I-O BANQCL
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO BCL-RECORD.
           MOVE FR-KEY TO BCL-FIRME.
           MOVE PC-NUMBER TO BCL-CLIENT.
           IF EXC-KEY = 99
      *       IF LNK-SQL = "Y" 
      *           CALL "9-BANQCL" USING LINK-V BCL-RECORD EXC-KEY 
      *       END-IF
              WRITE BCL-RECORD INVALID REWRITE BCL-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.
           MOVE 0 TO LNK-VAL.
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ BANQCL PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ BANQCL NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 0 PERFORM START-3
               READ BANQCL NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ BANQCL NO LOCK INVALID GO EXIT-1 END-READ
              GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY    NOT = BCL-FIRME 
           OR PC-NUMBER NOT = BCL-CLIENT
              GO EXIT-1
           END-IF.
           MOVE BCL-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START BANQCL KEY < BCL-KEY INVALID GO EXIT-1.
       START-2.
           START BANQCL KEY > BCL-KEY INVALID GO EXIT-1.
       START-3.
           START BANQCL KEY >= BCL-KEY INVALID GO EXIT-1.

