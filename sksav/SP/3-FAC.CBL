      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-FAC IMPRESSION FACTURES FIDUCIAIRES       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-FAC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "FACTINT.FC".
           COPY "FACDINT.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".
           COPY "FACTINT.FDE".
           COPY "FACDINT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80       ".
       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  PRECISION             PIC 99 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
SU         COPY "CODTXT.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.
       01  TEXTES                PIC X(19200).

       01  PAGES                  PIC 99 VALUE 0.
       01  NOT-OPEN               PIC 99 VALUE 0.
       01  LIN-CS                 PIC 999 VALUE 0.
       01  COL-CS                 PIC 999 VALUE 0.
       01  UNI-CS                 PIC 999 VALUE 0.
       01  TOT-CS                 PIC 999 VALUE 0.
       01  DES-CS                 PIC 999 VALUE 0.
       01  POS-PR                 PIC 9(5) VALUE 0.
       01  POS-IDX                PIC 9(5) VALUE 0.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(8) VALUE "FORMFAC.".
           02  FORM-LANGUE       PIC X VALUE "F".

       01  FACTURE-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FI".
           02 FIRME-FACTURE      PIC 9999.
       01  FID-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FD".
           02 FIRME-FID          PIC 9999.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-FIRME            PIC 9(6).

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4Z2 PIC ZZZZ,ZZ.
           02 HE-Z5Z2 PIC Z(5),99.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM FACTURE FID.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-FAC.
       
           MOVE FR-KEY   TO SAVE-FIRME.
           MOVE LNK-MOIS TO SAVE-MOIS.
           CALL "0-TODAY" USING TODAY.
           MOVE LNK-SUFFIX TO FIRME-FACTURE FIRME-FID.
           OPEN I-O FACTURE.
           OPEN INPUT FID.
           MOVE 70 TO IMPL-MAX-LINE.
           INITIALIZE FORMULAIRE.
           PERFORM READ-FORM.
           PERFORM DEBUT.

           PERFORM AFFICHAGE-ECRAN .
           INITIALIZE FR-RECORD.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           


       TRAITEMENT.
           MOVE 0 TO INPUT-ERROR.
           PERFORM START-FIRME.
           IF INPUT-ERROR = 0 
               PERFORM READ-FIRME THRU READ-FIRME-END.
           PERFORM END-PROGRAM.

       START-FIRME.
           IF FR-KEY > 0 
              SUBTRACT 1 FROM FR-KEY.
           IF A-N = "A"
              MOVE 9999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-FIRME.
           PERFORM NEXT-FIRME.
           IF FR-KEY = 0
           OR FR-KEY > END-NUMBER
           OR FR-MATCHCODE > END-MATCHCODE
              GO READ-FIRME-END
           END-IF.
           ACCEPT ACTION TIME 1 LINE 23 POSITION 27 NO BEEP
           ON EXCEPTION ESC-KEY CONTINUE END-ACCEPT.
           IF ESC-KEY = 27 
              EXIT PROGRAM.
           IF FR-FIN-A > 0 AND < LNK-ANNEE
              GO READ-FIRME
           END-IF.
           PERFORM TOTAL-FACTURE THRU TOTAL-FACTURE-END.
           PERFORM DIS-HE-01.
           GO READ-FIRME.
       READ-FIRME-END.
           PERFORM END-PROGRAM.

       NEXT-FIRME.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.


       TOTAL-FACTURE.
           INITIALIZE FI-RECORD.
           MOVE LNK-ANNEE TO FI-DEBUT-A.
           MOVE LNK-MOIS  TO FI-DEBUT-M.
           MOVE FR-KEY TO FI-CLIENT.
           MOVE 1 TO PAGES.
           START FACTURE KEY > FI-KEY-3 INVALID
              PERFORM READ-END
              GO TOTAL-FACTURE-END.
           PERFORM READ-FACTURE THRU READ-END.
       TOTAL-FACTURE-END.
           EXIT.

       READ-FACTURE.
           READ FACTURE NEXT NO LOCK AT END 
              GO READ-END.
           IF FR-KEY    NOT = FI-CLIENT
           OR LNK-ANNEE NOT = FI-DEBUT-A
           OR LNK-MOIS  NOT = FI-DEBUT-M 
              GO READ-END.
           IF FI-EDIT = 0
              MOVE TODAY-DATE TO FI-EDIT
              REWRITE FI-RECORD INVALID CONTINUE
           ELSE
              GO READ-FACTURE
           END-IF.
           MOVE 1 TO PAGES.
           PERFORM LAYOUT.
           PERFORM DET-FACTURE.
           COMPUTE IDX-1 = IMPL-MAX-LINE - 6
           IF LIN-NUM > IDX-1
              PERFORM TRANSMET
              ADD 1 TO PAGES
              PERFORM LAYOUT
           END-IF.
           PERFORM TOTAUX.
           PERFORM TRANSMET.
           GO READ-FACTURE.
       READ-END.
           EXIT.

       DET-FACTURE.
           INITIALIZE FID-RECORD.
           COMPUTE LIN-NUM = LIN-CS + 1
           MOVE FI-NUMBER TO FID-NUMBER.
           START FID KEY > FID-KEY-3 INVALID CONTINUE
              NOT INVALID
           PERFORM READ-FID THRU READ-FID-END.

       READ-FID.
           READ FID NEXT NO LOCK AT END 
              GO READ-FID-END.
           IF FI-NUMBER NOT = FID-NUMBER 
              GO READ-FID-END.
           IF LIN-NUM >= IMPL-MAX-LINE
              PERFORM TRANSMET
              ADD 1 TO PAGES
              PERFORM LAYOUT
           END-IF.
           MOVE FID-CODE TO CD-NUMBER.
           CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY.
SU         CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           PERFORM EDIT-CS.
           GO READ-FID.
       READ-FID-END.

       LAYOUT.
           MOVE 70 TO IMPL-MAX-LINE.
           INITIALIZE FORMULAIRE.
           PERFORM READ-FORM.
           ADD 1 TO LIN-NUM.
           MOVE FORMULAIRE TO TEXTES.
           IF FI-FREQ-FACT < 2
              PERFORM MOIS-N
           ELSE
              PERFORM PER-N
           END-IF.
           CALL "4-DOCFR" USING LINK-V TEXTES.
           MOVE PAGES TO LNK-VAL.
           CALL "4-DOCFAC" USING LINK-V FI-RECORD TEXTES.
           MOVE TEXTES TO FORMULAIRE.

       EDIT-CS.
           MOVE DES-CS TO COL-NUM.
           ADD 1 TO COL-NUM.
           IF FID-TEXTE NOT = SPACES
              MOVE FID-TEXTE TO ALPHA-TEXTE
           ELSE
              MOVE CTX-NOM TO ALPHA-TEXTE
           END-IF.
           PERFORM FILL-FORM.

           MOVE FID-DONNEE(4) TO VH-00.
           MOVE 4 TO CAR-NUM.
           MOVE COL-CS TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 0 TO VH-00 CAR-NUM.
           
           MOVE FID-DONNEE(5) TO HE-Z5Z2.
           MOVE HE-Z5Z2 TO ALPHA-TEXTE
           MOVE UNI-CS TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FID-DONNEE(6) TO HE-Z5Z2.
           MOVE HE-Z5Z2 TO ALPHA-TEXTE
           MOVE TOT-CS TO COL-NUM.
           PERFORM FILL-FORM.

           ADD 1 TO LIN-NUM.

       TOTAUX.
           MOVE FI-TOTAL TO HE-Z5Z2.
           INSPECT FORMULAIRE REPLACING ALL "TOTALXX " BY HE-Z5Z2.
           MOVE FI-TVA   TO HE-Z5Z2.
           INSPECT FORMULAIRE REPLACING ALL "TVAXXXX " BY HE-Z5Z2.
           MOVE FI-A-PAYER TO HE-Z5Z2.
           INSPECT FORMULAIRE REPLACING ALL "A-PAYER " BY HE-Z5Z2.

       DEBUT.
           MOVE 1 TO NOT-OPEN.
           MOVE 0 TO IDX-1.
           INSPECT FORMULAIRE TALLYING IDX-1 FOR CHARACTERS 
           BEFORE ">DEB".
           DIVIDE IDX-1 BY 80 GIVING LIN-CS REMAINDER COL-CS.

           MOVE 0 TO IDX-1.
           INSPECT FORMULAIRE TALLYING IDX-1 FOR CHARACTERS 
           BEFORE ">DES".
           DIVIDE IDX-1 BY 80 GIVING LIN-CS REMAINDER DES-CS.

           MOVE 0 TO IDX-1.
           INSPECT FORMULAIRE TALLYING IDX-1 FOR CHARACTERS 
           BEFORE ">UNI".
           DIVIDE IDX-1 BY 80 GIVING LIN-CS REMAINDER UNI-CS.

           MOVE 0 TO IDX-1.
           INSPECT FORMULAIRE TALLYING IDX-1 FOR CHARACTERS 
           BEFORE ">TOT".
           DIVIDE IDX-1 BY 80 GIVING LIN-CS REMAINDER TOT-CS.

           MOVE FORMULAIRE TO TEXTES.
           MOVE 0 TO POS-PR.
           INSPECT TEXTES TALLYING POS-PR FOR CHARACTERS 
           BEFORE ">PER".
           ADD 1 TO POS-PR.
           IF POS-PR > 19000
              MOVE 0 TO POS-PR.

       DIS-HE-01.
           MOVE FR-KEY    TO HE-Z6.
           DISPLAY HE-Z6  LINE  6 POSITION 17.
           DISPLAY FR-NOM LINE  6 POSITION 25 SIZE 30.
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 435 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.


       END-PROGRAM.
           IF COUNTER > 0 
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE
           END-IF.
           CANCEL "P080".
           CANCEL "4-DOCFR".
           CANCEL "4-DOCFAC".
           MOVE 0 TO LNK-SUITE.
           MOVE SAVE-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V A-N FAKE-KEY.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXF".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".

       MOIS-N.
           MOVE FI-DEBUT-M TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE POS-PR TO POS-IDX.
           STRING LNK-TEXT DELIMITED BY " " INTO TEXTES 
           POINTER POS-IDX ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO POS-IDX.
           STRING LNK-ANNEE DELIMITED BY SIZE INTO TEXTES 
           POINTER POS-IDX ON OVERFLOW CONTINUE END-STRING.


       PER-N.
           EVALUATE FI-FREQ-FACT 
              WHEN 2 MOVE 118 TO LNK-NUM
              WHEN 3 MOVE 115 TO LNK-NUM
              WHEN 6 MOVE 116 TO LNK-NUM
           END-EVALUATE.
           MOVE "AA" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE POS-PR TO POS-IDX.
           STRING LNK-TEXT DELIMITED BY " " INTO TEXTES 
           POINTER POS-IDX ON OVERFLOW CONTINUE END-STRING.
           DIVIDE LNK-MOIS BY FI-FREQ-FACT GIVING IDX-1 REMAINDER IDX-2.
           MOVE IDX-1 TO HE-Z2.
           ADD 2 TO POS-IDX.
           STRING HE-Z2 DELIMITED BY SIZE INTO TEXTES 
           POINTER POS-IDX ON OVERFLOW CONTINUE END-STRING.
           
           ADD 2 TO POS-IDX.
           STRING LNK-ANNEE DELIMITED BY SIZE INTO TEXTES 
           POINTER POS-IDX ON OVERFLOW CONTINUE END-STRING.

