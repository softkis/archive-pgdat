      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME MF-HO SAISIE DES HEURES PAR HORAIRE         �
      *  � MULLER & FILS                                         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    MF-HO.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".
           COPY "HEURES.FC".

      *       1 =  Heures de nuit 
      *       2 =  Heures r괾ie de nuit 
      *       3 =  Heures r괾ie normales
      *       5 =  Heures suppl굆entaires
      *       6 =  Heures r괾ie suppl굆entaires
      *       9 =  Heures r괾ie d굋lacement
      *      10 =  Heures d굋lacement

      *      11 =  Heures f굍i괻s travaill괻s
      *      12 =  Heures f굍i괻s travaill괻s > 8
      *      13 =  Heures r괾ie f굍i괻s travaill괻s
      *      14 =  Heures r괾ie f굍i괻s travaill괻s > 8
      *      19 =  Heures r괾ie dimanche
      *      20 =  Heures dimanche (d괽inies automatiquement)

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURS.FDE".
       FD  HEURES
           LABEL RECORD STANDARD
           DATA RECORD HRS-RECORD.
      *  ENREGISTREMENT FICHIER HEURES 
       01  HRS-RECORD.
           02 HRS-KEY.
              03 HRS-FIRME          PIC 9(8).
1             03 HRS-PERSON         PIC 9(8).
2             03 HRS-DATE.
                 04 HRS-MOIS        PIC 99.
                 04 HRS-JOUR        PIC 99.
              03 HRS-INTERVENT      PIC 9(4).
3             03 HRS-COUT.
                 04 HRS-OCCUP       PIC 99.
                 04 HRS-POSTE       PIC 9(8).
                 04 HRS-RUBRIQUE    PIC 9999.
                 04 HRS-OBJET       PIC 99.
                 04 HRS-S-POSTE     PIC X(44).

           02 HRS-KEY-A.
              03 HRS-FIRME-A        PIC 9(8).
              03 HRS-COUT-A.
                 04 HRS-OCCUP-A     PIC 99.
                 04 HRS-POSTE-A     PIC 9(8).
                 04 HRS-RUBRIQUE-A  PIC 9999.
                 04 HRS-OBJET-A     PIC 99.
                 04 HRS-S-POSTE-A   PIC X(44).
              03 HRS-PERSON-A       PIC 9(8).
              03 HRS-DATE-A.
                 04 HRS-MOIS-A      PIC 99.
                 04 HRS-JOUR-A      PIC 99.
              03 HRS-INTERVENT-A    PIC 9999.
                
           02 HRS-KEY-B.
              03 HRS-FIRME-B        PIC 9(8).
              03 HRS-COUT-B.
                 04 HRS-OCCUP-B     PIC 99.
                 04 HRS-POSTE-B     PIC 9(8).
                 04 HRS-RUBRIQUE-B  PIC 9999.
                 04 HRS-OBJET-B     PIC 99.
                 04 HRS-S-POSTE-B   PIC X(44).
              03 HRS-DATE-B.
                 04 HRS-MOIS-B      PIC 99.
                 04 HRS-JOUR-B      PIC 99.
              03 HRS-INTERVENT-B    PIC 9999.
              03 HRS-PERSON-B       PIC 9(8).

           02 HRS-KEY-C.
              03 HRS-FIRME-C        PIC 9(8).
              03 HRS-PERSON-C       PIC 9(8).
              03 HRS-COUT-C.
                 04 HRS-OCCUP-C     PIC 99.
                 04 HRS-POSTE-C     PIC 9(8).
                 04 HRS-RUBRIQUE-C  PIC 9999.
                 04 HRS-OBJET-C     PIC 99.
                 04 HRS-S-POSTE-C   PIC X(44).
              03 HRS-DATE-C.
                 04 HRS-MOIS-C      PIC 99.
                 04 HRS-JOUR-C      PIC 99.
              03 HRS-INTERVENT-C    PIC 9999.

           02 HRS-REC-DET.
              03 HRS-TEMPS.
                 04 HRS-DEBUT       PIC 9999.
                 04 HRS-DEBUTR REDEFINES HRS-DEBUT.
                    05 HRS-HR-D     PIC 99.
                    05 HRS-MIN-D    PIC 99.
                 04 HRS-FIN         PIC 9999.
                 04 HRS-FINR REDEFINES HRS-FIN.
                    05 HRS-HR-F     PIC 99.
                    05 HRS-MIN-F    PIC 99.
             03 HRS-REPOS           PIC 9999.
             03 HRS-TOTAL-BRUT      PIC 99V99.
             03 HRS-TOTAL-NET       PIC 99V99.
             03 HRS-TARIF.
                04 HRS-TARIF-HRS    PIC 99V99  OCCURS 10.
                04 HRS-TARIF-JRS    PIC 99V99  OCCURS 10.
             03 HRS-TARIF-R REDEFINES HRS-TARIF.
                04 HRS-TARIF-IDX    PIC 99V99  OCCURS 20.
             03 HRS-UNITES.
                04 HRS-UNITE        PIC 9(6)V99 OCCURS 10.
             03 HRS-UNITES-R REDEFINES HRS-UNITES.
                04 HRS-UNIT         PIC 9(8) OCCURS 10.
             03 HRS-JOUR-LOGIC      PIC 99.
             03 HRS-LOGIC           PIC 9(4).
             03 HRS-L-R REDEFINES HRS-LOGIC.
                04 HRS-HR-L         PIC 99.
                04 HRS-MIN-L        PIC 99.
             03 HRS-INFO            PIC X(20).
             03 HRS-STAMP.
                04 HRS-TIME.
                   05 HRS-ST-ANNEE PIC 9999.
                   05 HRS-ST-MOIS  PIC 99.
                   05 HRS-ST-JOUR  PIC 99.
                   05 HRS-ST-HEURE PIC 99.
                   05 HRS-ST-MIN   PIC 99.
                04 HRS-USER        PIC X(10).

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX-V.
          02  CHOIX-MAX-1       PIC 99 VALUE 21.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 1. 
       01  ECRAN-SUITE.
           02 ECR-S1  PIC 9999 VALUE 311.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S   PIC 9999 OCCURS 1.
       01  CONVERSION.
           02 CONV-1  PIC 99 VALUE  5.
           02 CONV-2  PIC 99 VALUE  1.
           02 CONV-3  PIC 99 VALUE 11.
           02 CONV-4  PIC 99 VALUE 12.
           02 CONV-5  PIC 99 VALUE 10.
           02 CONV-6  PIC 99 VALUE  3.
           02 CONV-7  PIC 99 VALUE  6.
           02 CONV-8  PIC 99 VALUE  2.
           02 CONV-9  PIC 99 VALUE 13.
           02 CONV-10 PIC 99 VALUE 14.
           02 CONV-11 PIC 99 VALUE  9.
       01  CONVERSION-R REDEFINES CONVERSION.
           02 CON     PIC 99 OCCURS 11.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".
           COPY "POCL.REC".
           COPY "CALEN.REC".
           COPY "OCCOM.REC".
           COPY "RUB.REC".
           COPY "V-VAR.CPY".

       01  JOUR-IDX              PIC 99 VALUE 0.
       01  TARIF                 PIC 99 VALUE 0.
       01  MODIFICATION          PIC 9 VALUE 0.
       01  OB-NUMBER             PIC 9(6).

       01  JOUR                  PIC 99.

       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.
       01  HRS-NAME.
           02 HRS-ID             PIC X(9) VALUE "S-HEURES.".
           02 ANNEE-HEURES       PIC 999.

       01  ECR-DISPLAY.
           02 HEZ2Z2 PIC ZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z2 PIC ZZ.
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).

       77  HELP-1     PIC 9(4).
       01  HELP-2     PIC 9(4).
       01  HELP-2-R REDEFINES HELP-2.
           02 HELP-2A    PIC 99.
           02 HELP-2B    PIC 99.
       77  HELP-3     PIC 99V99 COMP-3.
       77  HELP-4     PIC S99V99 COMP-3.

       01  ABSENCES.
           02 ABS OCCURS 3.
              03 INTER PIC 99V99 OCCURS 31.

       01  MOIS-EN-COURS.
           02 JOUR-DEB     PIC 99.
           02 JOUR-FIN     PIC 99.

       LINKAGE SECTION.
      
          COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS HEURES.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-MF-HO.
       
           MOVE LNK-SUFFIX TO ANNEE-HEURES ANNEE-JOURS.

           OPEN I-O   HEURES.
           OPEN INPUT JOURS.
           PERFORM AFFICHAGE-ECRAN.
           MOVE 1 TO JOUR-IDX.
           INITIALIZE PC-RECORD HRS-RECORD.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           IF ECRAN-IDX = 1
               EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 3  MOVE 0000002422 TO EXC-KFR(1)
                      MOVE 3000080000 TO EXC-KFR(2)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 4 THRU 5
                      MOVE 0064002400 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 6  MOVE 0000002400 TO EXC-KFR(1)
                      MOVE 3000080000 TO EXC-KFR(2)
              WHEN CHOIX-MAX(1)
                      MOVE 0000000005 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11)
              WHEN OTHER
                      MOVE 0000000005 TO EXC-KFR (1)
               END-EVALUATE
           END-IF.        

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1 
               EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-1-1 
               WHEN  2 PERFORM AVANT-1-2 
               WHEN  3 PERFORM AVANT-1-3 
               WHEN  4 PERFORM AVANT-1-4 
               WHEN  5 PERFORM AVANT-1-5 
      *        WHEN  6 PERFORM AVANT-1-6 
               WHEN  6 CONTINUE
               WHEN  7 PERFORM AVANT-1-7 
               WHEN  19 THRU 20 PERFORM AVANT-1-KM
               WHEN  CHOIX-MAX(1) PERFORM AVANT-DEC
               WHEN  OTHER PERFORM AVANT-1-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN.
           
           IF ECRAN-IDX = 1
               EVALUATE INDICE-ZONE
               WHEN  1 PERFORM APRES-1-1 
               WHEN  2 PERFORM APRES-1-2 
               WHEN  3 PERFORM APRES-1-3 
               WHEN  4 PERFORM APRES-1-4 
               WHEN  5 PERFORM APRES-1-5 
      *        WHEN  6 PERFORM APRES-1-6 
               WHEN  6 CONTINUE
               WHEN  7 PERFORM APRES-1-7 
               WHEN  12 PERFORM APRES-1-12 
               WHEN  14 THRU 18 PERFORM APRES-1-REG
               WHEN  19 THRU 20 PERFORM APRES-KM
               WHEN  CHOIX-MAX(1) PERFORM APRES-DEC
               WHEN  OTHER PERFORM APRES-1-ALL.

           IF EXC-KEY = 27 MOVE 0 TO INPUT-ERROR.
           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           PERFORM CUMUL-OCC-JRS.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.
           IF JOUR-IDX < JOUR-DEB
              MOVE JOUR-DEB TO JOUR-IDX
           END-IF.
           ACCEPT JOUR-IDX
             LINE  4 POSITION 19 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
            
       AVANT-1-4.
           ACCEPT PC-NUMBER
             LINE  5 POSITION 13 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-5.
           ACCEPT RUB-NUMBER
             LINE  6 POSITION 17 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-6.
           ACCEPT OB-NUMBER
             LINE  6 POSITION 72 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-7.
           MOVE HRS-TOTAL-BRUT TO HEZ2Z2.
           ACCEPT HEZ2Z2 
             LINE  8 POSITION 30 SIZE 5
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HEZ2Z2 REPLACING ALL "." BY ",".
           MOVE HEZ2Z2 TO HRS-TOTAL-BRUT.

       AVANT-1-ALL.
           COMPUTE IDX = INDICE-ZONE - 7.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE CON(IDX) TO IDX-4.
           MOVE HRS-TARIF-IDX(IDX-4) TO HEZ2Z2.
           ACCEPT HEZ2Z2
             LINE LIN-IDX POSITION 30 SIZE 5
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HEZ2Z2 REPLACING ALL "." BY ",".
           MOVE HEZ2Z2 TO HRS-TARIF-IDX(IDX-4).

       AVANT-1-KM.
           COMPUTE IDX = INDICE-ZONE - 18.
           COMPUTE LIN-IDX = IDX + 20.
           MOVE HRS-UNIT(IDX) TO HE-Z4.
           ACCEPT HRS-UNITE(IDX)
             LINE LIN-IDX POSITION 31 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM DIS-E1-KMS.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE
              IF PRES-TOT(LNK-MOIS) = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           PERFORM AFFICHAGE-DETAIL.
           IF REG-PERSON NOT = 0
              PERFORM CAR-RECENTE
              IF CAR-IN-ACTIF = 1 
                 MOVE "SL" TO LNK-AREA
                 MOVE 6 TO LNK-NUM
                 PERFORM DISPLAY-MESSAGE
                 MOVE 1 TO INPUT-ERROR
              END-IF 
           END-IF .
           PERFORM DIS-E1-01.
      *    IF MODIFICATION > 0
      *       PERFORM CALCULE 
      *    END-IF.

       APRES-1-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF REG-PERSON NOT = 0
              PERFORM CAR-RECENTE
              IF CAR-IN-ACTIF = 1 
                 MOVE "SL" TO LNK-AREA
                 MOVE 6 TO LNK-NUM
                 PERFORM DISPLAY-MESSAGE
                 MOVE 1 TO INPUT-ERROR
              END-IF 
           ELSE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-E1-01.
           PERFORM PREPARATION.
      *    IF MODIFICATION > 0
      *       PERFORM CALCULE 
      *    END-IF.

           IF INPUT-ERROR = 0
              PERFORM PREPARATION
              PERFORM TOTAL-ABSENCES
           END-IF.
           PERFORM DIS-E1-01.
           
       APRES-1-3.
           MOVE 0 TO LNK-NUM.
           IF EXC-KEY = 65
              SUBTRACT 1 FROM JOUR-IDX.
           IF EXC-KEY = 66
              ADD 1 TO JOUR-IDX.
           IF JOUR-IDX < JOUR-DEB
              MOVE JOUR-DEB TO JOUR-IDX
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF JOUR-IDX >  JOUR-FIN 
              MOVE JOUR-FIN TO JOUR-IDX
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF JOUR-IDX < JOUR-DEB
              MOVE JOUR-DEB TO JOUR-IDX
              MOVE 2 TO LNK-NUM
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF JOUR-IDX >  JOUR-FIN 
              MOVE JOUR-FIN TO JOUR-IDX
              MOVE 2 TO LNK-NUM
              MOVE 1 TO INPUT-ERROR
           END-IF.
           MOVE JOUR-IDX TO HRS-JOUR.
           EVALUATE EXC-KEY
           WHEN  4 PERFORM NEXT-HEURES
           WHEN  5 PERFORM COPIE THRU COPIE-FIN 
           WHEN  6 MOVE REG-PERSON TO LNK-PERSON
                   CALL "2-HO" USING LINK-V
                   CANCEL "2-HO"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
           WHEN  8 PERFORM APRES-DEC
           END-EVALUATE.
           PERFORM DIS-E1-03.
           IF JOUR-IDX > 0
              IF INTER(1, JOUR-IDX) > 0
                 MOVE 21 TO LNK-NUM
              END-IF
              IF INTER(2, JOUR-IDX) > 0
                 MOVE 22 TO LNK-NUM
              END-IF
              IF INTER(3, JOUR-IDX) > 0
                 MOVE 23 TO LNK-NUM
              END-IF
              IF PRES-JOUR(LNK-MOIS, JOUR-IDX) = 0
                 MOVE 2 TO LNK-NUM
                 MOVE 1 TO INPUT-ERROR
              END-IF
           END-IF .
           IF LNK-NUM NOT = 0 
              MOVE "SL" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
           END-IF.

       APRES-1-4.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   ELSE
                      MOVE "N" TO LNK-A-N
                   END-IF
                   CALL "2-POCL" USING LINK-V PC-RECORD 
                   CANCEL "2-POCL"
                   MOVE 0 TO LNK-PERSON
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-E1-01 THRU DIS-E1-END
                   PERFORM DISPLAY-F-KEYS
           WHEN  4 PERFORM NEXT-HEURES
           WHEN  8 PERFORM APRES-DEC
           WHEN  6 MOVE REG-PERSON TO LNK-PERSON
                   CALL "2-HO" USING LINK-V
                   CANCEL "2-HO"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-E1-01 THRU DIS-E1-END
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER PERFORM NEXT-POCL 
           END-EVALUATE.
           IF PC-NUMBER = 0
           OR PC-NOM    = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-E1-04.

       APRES-1-5.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN  4 PERFORM NEXT-HEURES
                   CALL "6-RUB" USING LINK-V RUB-RECORD FAKE-KEY
           WHEN  2 CALL "2-RUB" USING LINK-V RUB-RECORD 
                   CANCEL "2-RUB"
                   MOVE 0 TO LNK-PERSON
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-E1-01 THRU DIS-E1-END
                   PERFORM DISPLAY-F-KEYS
           WHEN  8 PERFORM APRES-DEC
           WHEN 10 MOVE REG-PERSON TO LNK-PERSON
                   CALL "2-HRS" USING LINK-V
                   CANCEL "2-HRS"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-E1-01 THRU DIS-E1-END
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER PERFORM NEXT-RUB 
           END-EVALUATE.
      *    IF RUB-NUMBER = 0
      *    OR RUB-NOM    = SPACES
      *       MOVE 1 TO INPUT-ERROR
      *    END-IF.
      *    PERFORM DIS-E1-05.

      *APRES-1-6.
           MOVE 0 TO OB-NUMBER.
           PERFORM MAKE-KEY.
           READ HEURES INVALID INITIALIZE HRS-REC-DET END-READ.
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-7.
           IF HRS-TOTAL-BRUT > 24
              MOVE 1 TO INPUT-ERROR
              MOVE 24 TO HRS-TOTAL-BRUT
           END-IF.
           IF HRS-TOTAL-BRUT = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.
           MOVE HRS-TOTAL-BRUT TO HRS-TOTAL-NET.
           INITIALIZE HRS-TARIF-IDX(20).
           IF SEM-IDX (LNK-MOIS, JOUR-IDX) = 7
              MOVE HRS-TOTAL-NET TO HRS-TARIF-IDX(20)
           END-IF.
           PERFORM APRES-KEY.

       APRES-1-12.
           COMPUTE IDX-1 = INDICE-ZONE - 7.
           COMPUTE IDX = IDX-1 + 6.
           MOVE CON(IDX-1) TO IDX-3.
           MOVE CON(IDX)   TO IDX-4.
           IF HRS-TARIF-IDX(IDX-3) < HRS-TARIF-IDX(IDX-4)
              MOVE HRS-TARIF-IDX(IDX-3) TO HRS-TARIF-IDX(IDX-4)
           END-IF.
           PERFORM DIS-E1-ALL.
           PERFORM APRES-KEY.

       APRES-1-ALL.
           IF HRS-TARIF-IDX(IDX-4) > HRS-TOTAL-NET 
              MOVE HRS-TOTAL-NET TO HRS-TARIF-IDX(IDX-4)
              MOVE  1 TO INPUT-ERROR
           END-IF.
           COMPUTE IDX-1 = INDICE-ZONE - 7.
           COMPUTE IDX = IDX-1 + 6.
           MOVE CON(IDX-1) TO IDX-3.
           MOVE CON(IDX)   TO IDX-4.
           IF HRS-TARIF-IDX(IDX-3) < HRS-TARIF-IDX(IDX-4)
              MOVE HRS-TARIF-IDX(IDX-3) TO HRS-TARIF-IDX(IDX-4)
           END-IF.
           PERFORM DIS-E1-ALL.
           PERFORM APRES-KEY.

       APRES-1-REG.
           COMPUTE IDX-1 = INDICE-ZONE - 13.
           COMPUTE IDX = INDICE-ZONE - 7.
           MOVE CON(IDX) TO IDX-4.
           MOVE CON(IDX-1) TO IDX-3.
           IF HRS-TARIF-IDX(IDX-3) < HRS-TARIF-IDX(IDX-4)
              MOVE HRS-TARIF-IDX(IDX-3) TO HRS-TARIF-IDX(IDX-4)
           END-IF.
           PERFORM DIS-E1-ALL.
           PERFORM APRES-KEY.

       APRES-KM.
           IF EXC-KEY = 13
           AND INDICE-ZONE = 20
              MOVE 5 TO EXC-KEY.
           PERFORM APRES-KEY.

       APRES-KEY.
           EVALUATE EXC-KEY
               WHEN  5 PERFORM APRES-DEC
               WHEN  8 PERFORM APRES-DEC
           END-EVALUATE.

       APRES-DEC.
           PERFORM MAKE-KEY.
           EVALUATE EXC-KEY
               WHEN 5 MOVE 4 TO DECISION 
                      CALL "0-TODAY" USING TODAY
                      MOVE TODAY-TIME TO HRS-TIME
                      MOVE LNK-USER TO HRS-USER
                      IF LNK-SQL = "Y" 
                         CALL "9-HEURES" USING LINK-V HRS-RECORD WR-KEY 
                      END-IF
                      WRITE HRS-RECORD INVALID 
                         REWRITE HRS-RECORD
                      END-WRITE   
                      MOVE 1 TO MODIFICATION
                      PERFORM AFFICHAGE-DETAIL
               WHEN 8 DELETE HEURES
                         INVALID CONTINUE
                      END-DELETE
                      IF LNK-SQL = "Y" 
                         CALL "9-HEURES" USING LINK-V HRS-RECORD DEL-KEY 
                      END-IF
                      MOVE 1 TO MODIFICATION
                      PERFORM NEXT-HEURES
                      PERFORM AFFICHAGE-DETAIL
                      MOVE 4 TO DECISION 
               WHEN 52 COMPUTE DECISION = CHOIX-MAX(1) + 1
           END-EVALUATE.
           COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(1)
               MOVE CHOIX-MAX(1) TO INDICE-ZONE.

       MAKE-KEY.
           MOVE FR-KEY TO HRS-FIRME HRS-FIRME-A HRS-FIRME-B HRS-FIRME-C.
           MOVE REG-PERSON TO HRS-PERSON HRS-PERSON-A HRS-PERSON-B 
           HRS-PERSON-C.
           MOVE PC-NUMBER TO HRS-POSTE HRS-POSTE-A HRS-POSTE-B 
           HRS-POSTE-C.
           MOVE RUB-NUMBER TO HRS-RUBRIQUE HRS-RUBRIQUE-A HRS-RUBRIQUE-B
           HRS-RUBRIQUE-C.
           MOVE OB-NUMBER TO HRS-OBJET HRS-OBJET-A HRS-OBJET-B 
           HRS-OBJET-C.
           MOVE LNK-MOIS TO HRS-MOIS HRS-MOIS-A HRS-MOIS-B HRS-MOIS-C.
           MOVE JOUR-IDX TO HRS-JOUR HRS-JOUR-A HRS-JOUR-B HRS-JOUR-C.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           MOVE 0 TO JOUR-DEB JOUR-FIN.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           PERFORM TEST-FIN VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.

       TEST-FIN.
           IF PRES-JOUR(LNK-MOIS, IDX) = 1
              IF JOUR-DEB = 0
                 MOVE IDX TO JOUR-DEB 
              END-IF
              MOVE IDX TO JOUR-FIN 
           END-IF.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           PERFORM DISPLAY-MESSAGE.
           
       NEXT-POCL.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" SAVE-KEY.

       NEXT-RUB.
           CALL "6-RUB" USING LINK-V RUB-RECORD SAVE-KEY.

       NEXT-HEURES.
           MOVE FR-KEY     TO HRS-FIRME.
           MOVE REG-PERSON TO HRS-PERSON.
           MOVE PC-NUMBER  TO HRS-POSTE.
           MOVE RUB-NUMBER TO HRS-RUBRIQUE.
           MOVE OB-NUMBER  TO HRS-OBJET.
           MOVE LNK-MOIS   TO HRS-MOIS.
           IF JOUR-IDX < HRS-JOUR
               MOVE 0 TO HRS-INTERVENT.
           MOVE JOUR-IDX   TO HRS-JOUR.
           START HEURES KEY > HRS-KEY INVALID KEY
                INITIALIZE HRS-RECORD
                NOT INVALID
               READ HEURES NEXT AT END
                   INITIALIZE HRS-RECORD
               END-READ.
           IF FR-KEY NOT = HRS-FIRME
           OR REG-PERSON NOT = HRS-PERSON
           OR LNK-MOIS  NOT = HRS-MOIS 
              INITIALIZE HRS-REC-DET
           END-IF.
           MOVE HRS-POSTE TO PC-NUMBER.
           MOVE HRS-RUBRIQUE TO RUB-NUMBER.
           MOVE HRS-OBJET TO OB-NUMBER.
           IF HRS-JOUR > 0
              MOVE HRS-JOUR TO JOUR-IDX.
           PERFORM AFFICHAGE-DETAIL.

       COPIE.
           IF HRS-JOUR = 0
           OR HRS-POSTE = 0
           OR HRS-TOTAL-BRUT = 0
           OR HRS-JOUR = MOIS-JRS(LNK-MOIS)
              GO COPIE-FIN
           END-IF.
           COMPUTE IDX-1 = HRS-JOUR + 1.
       COPIE-1.
           IF IDX-1 > MOIS-JRS(LNK-MOIS)
              GO COPIE-FIN
           END-IF.
           IF SEM-IDX(LNK-MOIS, IDX-1) > 5
           OR CAL-JOUR(LNK-MOIS, IDX-1) > 0
           OR PRES-JOUR(LNK-MOIS, IDX-1) = 0
           OR INTER(1, IDX-1) > 0
           OR INTER(2, IDX-1) > 0
           OR INTER(3, IDX-1) > 0
              ADD 1 TO IDX-1
              GO COPIE-1
           END-IF.
           MOVE IDX-1 TO JOUR-IDX.
           INITIALIZE HRS-TARIF-IDX(20) HRS-TARIF-IDX(11).
           MOVE HRS-TOTAL-BRUT TO HRS-TOTAL-NET.
           IF  SEM-IDX (LNK-MOIS, JOUR-IDX) = 7
           AND CAL-JOUR(LNK-MOIS, JOUR-IDX) = 0
               MOVE HRS-TOTAL-NET TO HRS-TARIF-IDX(20)
           END-IF.
           IF  CAL-JOUR(LNK-MOIS, JOUR-IDX) > 0
           AND CAL-JOUR(LNK-MOIS, JOUR-IDX) < 3
               MOVE HRS-TOTAL-NET TO HRS-TARIF-IDX(11)
           END-IF.
           PERFORM APRES-DEC.
       COPIE-FIN.
           EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       PREPARATION.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           MOVE CCOL-CALENDRIER TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.

       CUMUL-OCC-JRS.
           IF MODIFICATION = 1
              IF MENU-EXTENSION-2 NOT = SPACES
                 CALL MENU-EXTENSION-2 USING LINK-V REG-RECORD
              ELSE
                 CALL "4-HRCRE" USING LINK-V REG-RECORD
              END-IF
              MOVE 0 TO MODIFICATION.


       TOTAL-ABSENCES.
           INITIALIZE JRS-RECORD ABSENCES NOT-FOUND. 
           MOVE FR-KEY TO JRS-FIRME.
           MOVE LNK-MOIS  TO JRS-MOIS.
           MOVE REG-PERSON TO JRS-PERSON.
           START JOURS KEY > JRS-KEY INVALID KEY
                MOVE 1 TO NOT-FOUND .
           IF NOT-FOUND = 0 
              PERFORM READ-ABSENCES TEST BEFORE UNTIL NOT-FOUND = 1.
            
       READ-ABSENCES.
           READ JOURS NEXT AT END
               MOVE 1 TO NOT-FOUND
           END-READ.
           IF FR-KEY NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS  NOT = JRS-MOIS 
              MOVE 1 TO NOT-FOUND
           END-IF.
           IF NOT-FOUND   = 0 
              PERFORM CUM-HEURES VARYING IDX-2 FROM 1 BY 1 UNTIL
              IDX-2 > 31
           END-IF.                   

       CUM-HEURES.
           EVALUATE JRS-OCCUPATION
              WHEN  0 CONTINUE
              WHEN  1 THRU 11 ADD JRS-HRS(IDX-2) TO INTER(1, IDX-2)
              WHEN 20 THRU 30 ADD JRS-HRS(IDX-2) TO INTER(2, IDX-2)
              WHEN OTHER      ADD JRS-HRS(IDX-2) TO INTER(3, IDX-2)
           END-EVALUATE.


       DIS-E1-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           DISPLAY PR-NOM LINE 3 POSITION 47 SIZE 33.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 48 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 3 POSITION COL-IDX SIZE IDX LOW.

       DIS-E1-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-E1-03.
           MOVE JOUR-IDX TO HE-Z2.
           DISPLAY HE-Z2 LINE 4 POSITION 19.
           IF JOUR-IDX > 0 
              PERFORM DIS-JOUR-SEM
           END-IF.

       DIS-E1-04.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY.
           MOVE PC-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 5 POSITION 13.
           DISPLAY PC-NOM LINE 5 POSITION 33 SIZE 40.

       DIS-E1-05.
           MOVE RUB-NUMBER TO HE-Z4.
           DISPLAY HE-Z4   LINE 6 POSITION 17.
           DISPLAY RUB-NOM LINE 6 POSITION 33.

       DIS-E1-06.
           MOVE OB-NUMBER TO HE-Z2.
           DISPLAY HE-Z2  LINE 6 POSITION 72.

       DIS-E1-07.
           MOVE HRS-TOTAL-BRUT TO HEZ2Z2.
           INSPECT HEZ2Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ2Z2 LINE  8 POSITION 30.

       DIS-E1-ALL.
           PERFORM DIS-E1-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 11.

       DIS-E1-KM.
           PERFORM DIS-E1-KMS VARYING IDX FROM 1 BY 1 UNTIL IDX > 2.

       DIS-E1-END.
           EXIT.

       DIS-E1-HRS.
           COMPUTE LIN-IDX = IDX + 8.
           MOVE CON(IDX) TO IDX-4.
           MOVE HRS-TARIF-IDX(IDX-4) TO HEZ2Z2.
           INSPECT HEZ2Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HEZ2Z2 LINE LIN-IDX POSITION 30.

       DIS-E1-KMS.
           COMPUTE LIN-IDX = IDX + 20.
           MOVE HRS-UNITE(IDX) TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 31.

       DIS-JOUR-SEM.
           MOVE SEM-IDX(LNK-MOIS, JOUR-IDX) TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "SW" TO LNK-AREA.
           MOVE 04331200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
           IF  CAL-JOUR(LNK-MOIS, JOUR-IDX) > 0
           AND CAL-JOUR(LNK-MOIS, JOUR-IDX) < 7
              MOVE "SL" TO LNK-AREA
              MOVE 24 TO LNK-NUM
              MOVE "R" TO LNK-LOW 
              MOVE 04471200 TO LNK-POSITION
              CALL "0-DMESS" USING LINK-V
           ELSE
              DISPLAY SPACES LINE 4 POSITION 47 SIZE 15
           END-IF.
       
       AFFICHAGE-ECRAN.
           MOVE ECR-S(ECRAN-IDX) TO LNK-VAL.
           MOVE "MUFI" TO LNK-AREA.
           PERFORM WORK-ECRAN.
           INITIALIZE OCO-RECORD.
           PERFORM OCCOM VARYING IDX FROM 1 BY 1 UNTIL IDX > 11.

       OCCOM.
           MOVE CON(IDX) TO OCO-NUMBER.
           CALL "6-OCCOM" USING LINK-V OCO-RECORD FAKE-KEY.
           COMPUTE LIN-IDX = IDX + 8.
           COMPUTE IDX-1 = IDX + 7.
           MOVE IDX-1 TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 2 LOW.
           DISPLAY OCO-NOM LINE LIN-IDX POSITION 5 LOW.


       AFFICHAGE-DETAIL.
           EVALUATE ECRAN-IDX
               WHEN 1 PERFORM DIS-E1-01 THRU DIS-E1-END 
           END-EVALUATE.

       END-PROGRAM.
           PERFORM CUMUL-OCC-JRS.
           CLOSE JOURS.
           CLOSE HEURES.
           
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
