      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-MISFAC CONTROLE + MODIFICATION FACTURE INT�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  2-MISFAC .

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FACTINT.FC".
           COPY "FACDINT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FACTINT.FDE".
           COPY "FACDINT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  PRECISION             PIC 9 VALUE 0.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "POCL.REC".
           COPY "V-VAR.CPY".

       01  MAL-IDX               PIC 99 COMP-1.
       01  ARROW                 PIC X VALUE ">".

       01  FACTURE-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FI".
           02 FIRME-FACTURE      PIC 9999.
       01  FID-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FD".
           02 FIRME-FID          PIC 9999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z3Z2 PIC ZZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z4Z2 PIC ZZZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z5Z2 PIC Z(5),ZZ BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZ.

       01 HE-FACT.
          02 H-V OCCURS 15.
             03 HE-FACTURE  PIC 9(8).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FACTURE FID.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-MISFAC.
       
           CALL "0-TODAY" USING TODAY.
           MOVE FR-KEY  TO FIRME-FACTURE FIRME-FID.
           INITIALIZE LNK-PRESENCE.
           OPEN I-O   FACTURE.
           OPEN I-O   FID.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.


      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0129000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN OTHER MOVE 0000002800 TO EXC-KFR (1)
                      MOVE 2800080010 TO EXC-KFR (2).
           IF INDICE-ZONE = CHOIX-MAX
                      MOVE 0052000000 TO EXC-KFR (11).


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  OTHER PERFORM AVANT-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  OTHER PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           MOVE FR-KEY TO PC-FIRME.
           ACCEPT PC-NUMBER 
             LINE 3 POSITION 15 SIZE 8
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 
                PERFORM CHANGE-MOIS
             MOVE 27 TO EXC-KEY
           END-EVALUATE.

       AVANT-2.
           ACCEPT PC-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
           COMPUTE MAL-IDX = INDICE-ZONE - 2.
           COMPUTE LIN-IDX = MAL-IDX + 6.
           MOVE HE-FACTURE(MAL-IDX) TO HE-Z8.
           MOVE ">" TO ARROW.
           ACCEPT ARROW
             LINE  LIN-IDX POSITION 10 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "REVERSE"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           DISPLAY SPACES LINE LIN-IDX POSITION 10 SIZE 1.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3 
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   ELSE
                      MOVE "N" TO LNK-A-N
                   END-IF
                   CALL "2-POCL" USING LINK-V PC-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  OTHER PERFORM NEXT-POCL.
           PERFORM DIS-HE-01.
           PERFORM AFFICHAGE-DETAIL.
           IF INPUT-ERROR = 0 
              AND EXC-KEY NOT = 53
              PERFORM TOTAL-FACTURE THRU TOTAL-FACTURE-END.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              IF  MAL-IDX = 0 
              AND PC-NUMBER NOT = 0
                 GO APRES-1 
              END-IF.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-POCL
           END-EVALUATE.                     
           PERFORM DIS-HE-01.
           PERFORM TOTAL-FACTURE THRU TOTAL-FACTURE-END.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              IF  MAL-IDX = 0 
              AND PC-NUMBER NOT = 0
                 GO APRES-2
              END-IF.
           
       APRES-DEC.
           INITIALIZE FI-RECORD.
           MOVE PC-NUMBER TO FI-CLIENT.
           MOVE LNK-ANNEE TO FI-DEBUT-A.
           MOVE LNK-MOIS  TO FI-DEBUT-M.
           MOVE HE-FACTURE(MAL-IDX) TO FI-NUMBER.
           EVALUATE EXC-KEY 
              WHEN  4 THRU 6
              READ FACTURE INVALID CONTINUE END-READ
              IF EXC-KEY = 4
                 INITIALIZE FI-EDIT 
              ELSE
                 INITIALIZE FI-ECHEANCE
              END-IF
              REWRITE FI-RECORD INVALID CONTINUE END-REWRITE
              PERFORM TOTAL-FACTURE THRU TOTAL-FACTURE-END
              WHEN  8 PERFORM DEL-FACTURE
              SUBTRACT 1 FROM INDICE-ZONE
           WHEN 10 PERFORM REMBOURSEMENT
              PERFORM TOTAL-FACTURE THRU TOTAL-FACTURE-END
           END-EVALUATE.

       NEXT-POCL.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-POCL" USING LINK-V PC-RECORD A-N EXC-KEY.

       REMBOURSEMENT.
           READ FACTURE INVALID CONTINUE END-READ
           IF FI-PAYE > 0
              MOVE 0 TO FI-PAYE FI-DATE-PAYE
           ELSE
              MOVE FI-A-PAYER TO FI-PAYE
              MOVE TODAY-DATE TO FI-DATE-PAYE
           END-IF.
           REWRITE FI-RECORD INVALID CONTINUE END-REWRITE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           PERFORM DISPLAY-MESSAGE.

      *    HISTORIQUE DES FACTURES
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       TOTAL-FACTURE.
           MOVE 0 TO MAL-IDX.
           MOVE 6 TO LIN-IDX.
           INITIALIZE FI-RECORD HE-FACT.
           MOVE LNK-ANNEE TO FI-DEBUT-A.
           MOVE LNK-MOIS  TO FI-DEBUT-M.
           MOVE PC-NUMBER TO FI-CLIENT.
           START FACTURE KEY > FI-KEY-3 INVALID
              PERFORM READ-END
              GO TOTAL-FACTURE-END.
           PERFORM READ-FACTURE THRU READ-END.
           COMPUTE CHOIX-MAX = 2 + MAL-IDX.
       TOTAL-FACTURE-END.
           EXIT.

       READ-FACTURE.
           READ FACTURE NEXT NO LOCK AT END 
              GO READ-END.
           IF PC-NUMBER NOT = FI-CLIENT
           OR LNK-ANNEE NOT = FI-DEBUT-A
           OR LNK-MOIS  NOT = FI-DEBUT-M 
              GO READ-END.
           ADD 1 TO MAL-IDX.
           MOVE FI-NUMBER TO HE-FACTURE(MAL-IDX).
           PERFORM DIS-HIS-LIGNE.
           IF LIN-IDX > 22
              GO READ-END.
           GO READ-FACTURE.
       READ-END.
           ADD 1 TO LIN-IDX .
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 24.


       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.


       DEL-FACTURE.
           INITIALIZE FID-RECORD.
           MOVE HE-FACTURE(MAL-IDX) TO FI-NUMBER FID-NUMBER.
           START FID KEY > FID-KEY-3 INVALID CONTINUE
              NOT INVALID
           PERFORM READ-FID THRU READ-FID-END.

       READ-FID.
           READ FID NEXT NO LOCK AT END 
              GO READ-FID-END.
           IF FI-NUMBER NOT = FID-NUMBER 
              GO READ-FID-END.
           DELETE FID INVALID CONTINUE.
           GO READ-FID.
       READ-FID-END.
           DELETE FACTURE INVALID CONTINUE.
           PERFORM TOTAL-FACTURE THRU TOTAL-FACTURE-END.

       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS 
             WHEN 58 PERFORM ADD-MOIS
           END-EVALUATE.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-DETAIL.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0 
              MOVE 1 TO LNK-MOIS
           END-IF.
       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
              MOVE 12 TO LNK-MOIS
           END-IF.


       DIS-HE-01.
           MOVE PC-NUMBER  TO HE-Z8.
           DISPLAY HE-Z8 LINE 3 POSITION 15.
           DISPLAY PC-NOM LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY PC-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-END.
           EXIT.

       DIS-HIS-LIGNE.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           MOVE FI-NUMBER TO HE-Z8.
           DISPLAY HE-Z8 LINE LIN-IDX POSITION 2 LOW.
           MOVE FI-EDIT-J TO HE-JJ.
           MOVE FI-EDIT-M TO HE-MM.
           MOVE FI-EDIT-A TO HE-AA.
           DISPLAY HE-DATE LINE  LIN-IDX POSITION 12.
           MOVE FI-ECHEA-J TO HE-JJ.
           MOVE FI-ECHEA-M TO HE-MM.
           MOVE FI-ECHEA-A TO HE-AA.
           DISPLAY HE-DATE LINE  LIN-IDX POSITION 25.

           MOVE FI-ECHE-CODE TO HE-Z4.
           DISPLAY HE-Z4 LINE  LIN-IDX POSITION 34.


           MOVE FI-TOTAL  TO HE-Z5Z2.
           DISPLAY HE-Z5Z2 LINE LIN-IDX POSITION 40.
           MOVE FI-TVA TO HE-Z3Z2.
           DISPLAY HE-Z3Z2 LINE LIN-IDX POSITION 50.
           MOVE FI-A-PAYER TO HE-Z5Z2.
           DISPLAY HE-Z5Z2 LINE LIN-IDX POSITION 60.
           MOVE FI-PAYE TO HE-Z5Z2.
           DISPLAY HE-Z5Z2 LINE LIN-IDX POSITION 70.

       AFFICHAGE-ECRAN.
           MOVE 2461 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
