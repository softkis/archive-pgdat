      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-SISTO TRANSFERT COMPTABILITE              �
      *  � SISTO ARMATUREN                                       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-SISTO.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "INTERCOM.FC".
           SELECT OPTIONAL TF-COMPTA ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.


       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire JOURNAL COMPTABLE

       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-MOIS   PIC 99.
              03 INTER-SUITE  PIC 99.
              03 INTER-STAT   PIC 99.
              03 INTER-COUT   PIC 9(8).
              03 INTER-PERS   PIC 9(8).
              03 INTER-DC     PIC 9.
              03 INTER-COMPTE PIC X(30).
              03 INTER-POSTE  PIC 9(10).
              03 INTER-EXT    PIC X(50).
              03 INTER-EXTR REDEFINES INTER-EXT.
                 04 INTER-EXT-R PIC 99.
                 04 INTER-EXT-O PIC 9(6).
                 04 INTER-FILL  PIC X(42).
              03 INTER-LIB    PIC 9999.
              03 INTER-PERS-2 PIC 9(8).

           02 INTER-REC-DET.
              03 INTER-VALUE PIC S9(8)V99.
              03 INTER-UNITE PIC S9(8)V99.
              03 INTER-PERIODE PIC 99.

       FD  TF-COMPTA
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.

       01  TF-RECORD.
 1         02 TF-OPERATION  PIC 999.
 2         02 TF-SITUATION  PIC 9.
 3         02 TF-CORRECTION PIC 9.
 4         02 TF-FILLER     PIC XX.
 5         02 TF-FIRME      PIC 99.
 6         02 TF-POSTE      PIC 9(6).
 7         02 TF-COUT       PIC XX.

 8         02 TF-TYPE       PIC 9.
 9         02 TF-COMPTE     PIC X(6).
11         02 TF-LIBELLE-1  PIC X(4).
12         02 TF-MOIS-C     PIC 99.
13         02 TF-ANNEE-C    PIC 99.
14         02 TF-DATE.
              03 TF-DATE-J  PIC 99.
              03 TF-DATE-M  PIC 99.
              03 TF-DATE-A  PIC 99.
16         02 TF-FILLER     PIC X(4).
17         02 TF-DEUX       PIC 9.
18         02 TF-MOIS       PIC 99.
19         02 TF-NULL       PIC 9.
20         02 TF-FILLER     PIC X(9).
21         02 TF-MONTANT    PIC 9(11)V99.
22         02 TF-DEBIT-CREDIT PIC X.

23+        02 TF-FILLER     PIC X(48).

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01  NOT-OPEN PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "COUT.REC".
           COPY "COMPTE.REC".
           COPY "LIBELLE.REC".
           COPY "POCL.REC".

       01  HELP-XX.
           02 HELP-X                PIC S9(9)V99 OCCURS 2.
       
       01  INTER-NAME.
           02 FILLER             PIC XXXX VALUE "INTC".
           02 FIRME-INTER        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-INTER         PIC XXX.

       01  ECR-DISPLAY.
           02 HE-Z8 PIC -Z(8),ZZ.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               TF-COMPTA
               INTER.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-SISTO.
       

           CALL "0-TODAY" USING TODAY.
           MOVE FR-KEY TO FIRME-INTER.
           MOVE LNK-USER  TO USER-INTER.
           INITIALIZE COUT-RECORD.
           OPEN INPUT INTER.
           INITIALIZE HELP-XX.
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END GO READ-INTER-END.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-COMPTA
              MOVE 1 TO NOT-OPEN
           END-IF.
           IF FR-KEY  NOT = COUT-FIRME
           OR INTER-COUT NOT = COUT-NUMBER
              PERFORM READ-COUT.
           PERFORM FILL-TEXTE.
           GO READ-INTER.
       READ-INTER-END.
           IF HELP-X(1) NOT = HELP-X(2)
              ACCEPT ACTION NO BEEP.
           PERFORM END-PROGRAM.

       FILL-TEXTE.
           INITIALIZE TF-RECORD.
           MOVE INTER-COMPTE TO TF-COMPTE
           MOVE "+" TO TF-DEBIT-CREDIT.
           IF INTER-VALUE > 0
              IF INTER-DC = 2
                 MOVE "-" TO TF-DEBIT-CREDIT 
              END-IF
           ELSE 
              IF INTER-DC = 1
                 MOVE "-" TO TF-DEBIT-CREDIT 
              END-IF
           END-IF.
           INSPECT TF-COMPTE REPLACING ALL "++" BY INTER-EXT-R.

           MOVE INTER-VALUE TO TF-MONTANT.
           ADD  LNK-ANNEE   TO TF-ANNEE-C TF-DATE-A.
           MOVE INTER-MOIS  TO TF-MOIS TF-MOIS-C TF-DATE-M.
           MOVE MOIS-JRS(TF-DATE-M) TO TF-DATE-J.
           MOVE "LOHN" TO TF-LIBELLE-1.
           IF INTER-POSTE NOT = 0
              MOVE INTER-POSTE TO TF-POSTE 
           END-IF.
           MOVE  531 TO TF-OPERATION.
           MOVE 1 TO TF-TYPE.
           MOVE 2 TO TF-DEUX.
           WRITE TF-RECORD.

       READ-COUT.
           MOVE FR-KEY TO COUT-FIRME.
           MOVE INTER-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.

       END-PROGRAM.
           CLOSE INTER.
           IF NOT-OPEN = 1
              CLOSE TF-COMPTA.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------
