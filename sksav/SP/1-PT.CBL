      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-PT SAISIE PLANS DE TRAVAIL                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-PT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PLANS.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "PLANS.FDE".

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE  5. 
           02  CHOIX-MAX-2       PIC 99 VALUE 31.
           02  CHOIX-MAX-3       PIC 99 VALUE  6.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 3.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "PLAGE.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "HORSEM.REC".
           COPY "OCCUP.REC".
           COPY "OCCUP.LNK".
           COPY "OCCTXT.REC".
           COPY "CALEN.REC".
           COPY "CCOL.REC".
           COPY "POSE.REC".
           COPY "V-VAR.CPY".

       01  JOUR-IDX              PIC 99.
       01  POSE-IDX              PIC  9.
       01  JOUR                  PIC 99.
       01  DEBUT-JR              PIC 99.
       01  FIN-JR                PIC 99.
       01  FLAG-BEGIN            PIC 99 VALUE 0.
       01  FLAG-END              PIC 99 VALUE 0.
       01  FLIP-FLOP             PIC 9 VALUE 0.
       01  MODIFICATION          PIC 9 VALUE 0.
       01  LAST-PLAGE            PIC X(10) VALUE SPACES.
       01  PREV-PLAGE            PIC X(10) VALUE SPACES.
       01  FLAG-PLAGE            PIC X(10) VALUE SPACES.
       01  A-NUM    PIC X.

       01  PLANS-NAME.
           02 PLANS-ID           PIC X(8) VALUE "S-PLANS.".
           02 ANNEE-PLANS        PIC 999.

       01  ABSENCE.
           02 DET OCCURS 31.
              03 INTER PIC 99V99.
              03 ABS   PIC 99.

       01  ECR-DISPLAY.
           02 HE-Z3Z2 PIC ZZZ,ZZ BLANK WHEN ZERO.
           02 HEZ2Z2 PIC ZZ,ZZ BLANK WHEN ZERO.
           02 HES2Z2 PIC -ZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z2 PIC ZZ.
           02 HEZ3 PIC ZZZ.
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HEZ4Z2 PIC ZZZZ,ZZ BLANK WHEN ZERO. 

       01  HELP-2     PIC 9(4).
       01  HELP-2-R REDEFINES HELP-2.
           02 HELP-2A    PIC 99.
           02 HELP-2B    PIC 99.

       77  HELP-1     PIC 9(4).
       77  HELP-ZZ    PIC ZZ.ZZ.
       77  HELP-3     PIC 99V99 COMP-3.
       77  HELP-4     PIC S999V99 COMP-3.
       77  HELP-5     PIC 999V99 COMP-3.
       77  HELP-6     PIC 9(6) COMP-6.
       01  JOUR-CAL   PIC 99 VALUE 0.

       01  MOIS-EN-COURS.
           02 JOUR-DEB     PIC 99.
           02 JOUR-FIN     PIC 99.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
          COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PLANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-1-PT.
       

           MOVE LNK-SUFFIX TO ANNEE-PLANS.

           OPEN I-O PLANS.

           PERFORM AFFICHAGE-ECRAN.
           INITIALIZE LNK-POSITION.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF ECRAN-IDX = 1 AND INDICE-ZONE < 1
              MOVE 1 TO INDICE-ZONE.
        
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0163640000 TO EXC-KFR(1)
                      MOVE 0000130000 TO EXC-KFR(3)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
            WHEN 2    MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
                      MOVE 0000130000 TO EXC-KFR(3)
           END-IF.        

           IF ECRAN-IDX = 2
              MOVE 2263000905 TO EXC-KFR(1)
              MOVE 0000084161 TO EXC-KFR(2)
              MOVE 0000130000 TO EXC-KFR(3)
              MOVE 5600000000 TO EXC-KFR(12) 
              MOVE 0000000065 TO EXC-KFR(13) 
              MOVE 6600000000 TO EXC-KFR(14) 
           END-IF.    
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1 
              EVALUATE INDICE-ZONE
              WHEN  1 PERFORM AVANT-1-1 
              WHEN  2 PERFORM AVANT-1-2 
              END-EVALUATE
           END-IF.

           IF ECRAN-IDX = 2
              PERFORM AVANT-ALL
              IF ECRAN-IDX = 3
                 MOVE 1 TO INDICE-ZONE
              END-IF
           END-IF.

           IF ECRAN-IDX = 3 
               EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-DEBUT
               WHEN  2 PERFORM AVANT-FIN
               WHEN  3 PERFORM AVANT-PAUSE
               WHEN  4 PERFORM AVANT-DEBUT
               WHEN  5 PERFORM AVANT-FIN
               WHEN  6 PERFORM AVANT-PAUSE
              END-EVALUATE
              PERFORM DIS-PLAGE
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.

           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
              WHEN  1 PERFORM APRES-1-1 
              WHEN  2 PERFORM APRES-1-2
              END-EVALUATE
           END-IF.

           IF ECRAN-IDX = 3
              EVALUATE INDICE-ZONE
                 WHEN  1 PERFORM APRES-DEBUT
                 WHEN  2 PERFORM APRES-FIN
                 WHEN  3 PERFORM APRES-PAUSE
                 WHEN  4 PERFORM APRES-DEBUT
                 WHEN  5 PERFORM APRES-FIN
                 WHEN  6 PERFORM APRES-PAUSE
              END-EVALUATE
              IF ECRAN-IDX = 2
                 MOVE IDX-2 TO INDICE-ZONE
              END-IF
           END-IF.

           IF EXC-KEY = 27 
              MOVE 0 TO INPUT-ERROR.
           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 ADD 1 TO INDICE-ZONE.

           EVALUATE ECRAN-IDX 
              WHEN 1 IF INDICE-ZONE > CHOIX-MAX(1)
                        ADD 1 TO ECRAN-IDX
                        MOVE 1 TO INDICE-ZONE 
                        END-IF
              WHEN 2 IF INDICE-ZONE = 0
                        SUBTRACT 1 FROM ECRAN-IDX
                        MOVE 2 TO INDICE-ZONE
                      END-IF
              WHEN 3 IF INDICE-ZONE > CHOIX-MAX(3)
                     OR INDICE-ZONE < 1
                        SUBTRACT 1 FROM ECRAN-IDX
                     END-IF
           END-EVALUATE.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 PERFORM CHANGE-MOIS
                             MOVE 99 TO EXC-KEY
           END-EVALUATE.

       AVANT-1-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
           MOVE INDICE-ZONE TO JOUR-IDX.
           PERFORM AVANT-ALL-2 THRU AVANT-ALL-END.
           IF JOUR-IDX > 0
              MOVE JOUR-IDX TO INDICE-ZONE.
           
       AVANT-ALL-2.
           IF PRES-JOUR(LNK-MOIS, JOUR-IDX) = 0
              GO AVANT-ALL-END
           END-IF.
     *     INITIALIZE PLG-RECORD.
     *     IF  PT-PLAGE(JOUR-IDX) NOT = SPACES
     *        PERFORM READ-PLAGE.
           PERFORM LIN-COL.
           PERFORM ACCEPT-PARAM.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL-2.
           EVALUATE EXC-KEY
              WHEN 1 MOVE LAST-PLAGE TO PT-PLAGE(JOUR-IDX) 
                     INITIALIZE PREV-PLAGE
                     PERFORM READ-PLAGE
                     DISPLAY PT-PLAGE(JOUR-IDX) LINE LIN-IDX 
                     POSITION COL-IDX
              WHEN 2 CALL "2-PLAGE" USING LINK-V PLG-RECORD
                     IF PLG-KEY NOT = SPACES
                        MOVE PLG-KEY TO PT-PLAGE(JOUR-IDX)
                     END-IF
                     MOVE JOUR-IDX TO IDX 
                     PERFORM AFFICHAGE-ECRAN 
                     PERFORM AFFICHAGE-DETAIL
                     PERFORM DISPLAY-F-KEYS
                     MOVE IDX TO JOUR-IDX 
                     INITIALIZE PT-VAL(JOUR-IDX) 
                     PERFORM FILL-PLAGE
              WHEN 4 IF FLAG-BEGIN = 0
                        MOVE JOUR-IDX TO FLAG-BEGIN
                     ELSE
                        MOVE JOUR-IDX TO FLAG-END
                        PERFORM FILL-FLAG
                     END-IF
              WHEN 8 INITIALIZE PT-HEURES(JOUR-IDX) 
                     DISPLAY PT-PLAGE(JOUR-IDX) LINE LIN-IDX 
                     POSITION COL-IDX
                     PERFORM CUMUL-PT 
                     MOVE 1 TO MODIFICATION
              WHEN 9 PERFORM AVANT-TEXTE
              WHEN 65 THRU 66 
                     PERFORM READ-PLAGE
                     MOVE PLG-KEY TO PT-PLAGE(JOUR-IDX)
                     INITIALIZE PT-VAL(JOUR-IDX) 
                     PERFORM FILL-PLAGE
                     DISPLAY PT-PLAGE(JOUR-IDX)
                     LINE LIN-IDX POSITION COL-IDX  
              WHEN 13 IF PT-PLAGE(JOUR-IDX) NOT = SPACES
                         IF PT-PLAGE(JOUR-IDX) NOT = PREV-PLAGE
                            INITIALIZE PT-VAL(JOUR-IDX) 
                         END-IF
                         PERFORM READ-PLAGE
                         IF PT-VAL(JOUR-IDX) = 0
                            PERFORM FILL-PLAGE
                         END-IF
                      END-IF
           END-EVALUATE.
      *    PERFORM DIS-PLAGE.
           IF  PT-VAL(JOUR-IDX) = 0
           AND PT-PLAGE(JOUR-IDX) NOT = SPACES
              GO AVANT-ALL-2
           END-IF.
           IF PT-PLAGE(JOUR-IDX) = SPACES
              INITIALIZE PT-HORAIRE(JOUR-IDX) PT-VAL(JOUR-IDX).
           PERFORM CUMUL-PT.
           PERFORM DISPLAY-CAL.
       AVANT-ALL-2-2.
           EVALUATE EXC-KEY
                WHEN  5 PERFORM KEY-PT
                        PERFORM WRITE-PLANS
                        MOVE 2 TO MODIFICATION
                        MOVE 0 TO INDICE-ZONE JOUR-IDX
                        GO AVANT-ALL-END
                WHEN 54 PERFORM DISPLAY-INIT
                        MOVE 0 TO JOUR-IDX
                        MOVE 4 TO INDICE-ZONE 
                        MOVE 1 TO ECRAN-IDX
                        GO AVANT-ALL-END
                WHEN  8 ADD 1 TO JOUR-IDX
                WHEN 10 MOVE 1 TO INDICE-ZONE 
                        MOVE 3 TO ECRAN-IDX
                        MOVE JOUR-IDX TO IDX-2
                        GO AVANT-ALL-END
                WHEN 13 ADD 1 TO JOUR-IDX
                WHEN 27 SUBTRACT 1 FROM JOUR-IDX
                WHEN 53 ADD 7 TO JOUR-IDX
                WHEN 52 IF JOUR-IDX > 7 
                           SUBTRACT 7 FROM JOUR-IDX
                        ELSE
                           PERFORM DISPLAY-INIT
                           PERFORM CUMUL-PT 
///                        MOVE 5 TO INDICE-ZONE 
                           MOVE 0 TO JOUR-IDX
                           MOVE 1 TO ECRAN-IDX
                           GO AVANT-ALL-END
                        END-IF
                WHEN 67 GO AVANT-ALL-END
           END-EVALUATE.
       AVANT-ALL-3.
           IF JOUR-IDX > JOUR-FIN
              MOVE JOUR-FIN TO JOUR-IDX
           END-IF.
           IF  JOUR-IDX > 0
           AND JOUR-IDX < 32
              GO AVANT-ALL-2
           END-IF.
       AVANT-ALL-END.
           EXIT.

       ACCEPT-PARAM.
           IF  CAL-JOUR(LNK-MOIS, JOUR-IDX) > 0
           AND CAL-JOUR(LNK-MOIS, JOUR-IDX) < 7
              MOVE "SL" TO LNK-AREA
              MOVE 0 TO LNK-POSITION
              MOVE 24 TO LNK-NUM
              CALL "0-DMESS" USING LINK-V
           ELSE
              DISPLAY SPACES LINE 24 POSITION 1 SIZE 80
           END-IF.
           MOVE PT-PLAGE(JOUR-IDX) TO PREV-PLAGE.
           DISPLAY PT-COMMENT(JOUR-IDX) LINE 23 POSITION 1
QQQ           PERFORM DIS-PLAGE
           ACCEPT PT-PLAGE(JOUR-IDX)
                 LINE LIN-IDX POSITION COL-IDX SIZE 10 
                 TAB UPDATE NO BEEP CURSOR 1
                 CONTROL "UPPER"
                 ON EXCEPTION EXC-KEY CONTINUE
               END-ACCEPT.
           IF PT-PLAGE(JOUR-IDX) NOT = SPACES
              MOVE PT-PLAGE(JOUR-IDX) TO LAST-PLAGE
           END-IF.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE
              IF PRES-TOT(LNK-MOIS) = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           PERFORM AFFICHAGE-DETAIL.
           IF REG-PERSON NOT = 0
              PERFORM CAR-RECENTE
              IF CAR-IN-ACTIF = 1 
                 MOVE "SL" TO LNK-AREA
                 MOVE 6 TO LNK-NUM
                 PERFORM DISPLAY-MESSAGE
                 MOVE 1 TO INPUT-ERROR
              END-IF 
           END-IF .
           PERFORM DIS-NP.
           PERFORM DIS-E1-01.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM DISPLAY-INIT.

       APRES-1-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN  1 MOVE REG-PERSON TO LNK-PERSON
                   CALL "2-CGREP" USING LINK-V
                   CANCEL "2-CGREP"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-E1-01 THRU DIS-E1-END
                   PERFORM DISPLAY-F-KEYS
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
           WHEN  OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
           AND EXC-KEY NOT = 13
              PERFORM PRESENCE
              IF PRES-TOT(LNK-MOIS) = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           IF REG-PERSON NOT = 0
              PERFORM CAR-RECENTE
              IF CAR-IN-ACTIF = 1 
                 MOVE "SL" TO LNK-AREA
                 MOVE 6 TO LNK-NUM
                 PERFORM DISPLAY-MESSAGE
                 MOVE 1 TO INPUT-ERROR
              END-IF 
           ELSE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-E1-01.
           PERFORM PREPARATION.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM DISPLAY-INIT.

       LATEST.
           IF PT-PLAGE(IDX) > SPACES
              MOVE IDX TO DECISION JOUR-IDX
           END-IF.


       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           PERFORM DISPLAY-MESSAGE.


       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           MOVE 0 TO JOUR-DEB JOUR-FIN JOUR-CAL.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           PERFORM TEST-FIN VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.

       TEST-FIN.
           IF PRES-JOUR(LNK-MOIS, IDX) = 1
              IF JOUR-DEB = 0
                 MOVE IDX TO JOUR-DEB 
              END-IF
              MOVE IDX TO JOUR-FIN 
           END-IF.

       PREPARATION.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           MOVE CCOL-CALENDRIER TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.
           PERFORM PT-FERIE VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.

       
       PT-FERIE.
           IF CAL-JOUR(LNK-MOIS, IDX) > 0
              ADD 1 TO JOUR-CAL 
              PERFORM FETE
           END-IF.

       FETE.
           COMPUTE LIN-IDX = (SEM-LINE(LNK-MOIS, IDX) - 1) * 3 + 5.
           COMPUTE COL-IDX = 11 * SEM-IDX(LNK-MOIS, IDX) - 9.
           MOVE IDX TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION COL-IDX.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD HJS-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           CALL "6-GHJS" USING LINK-V HJS-RECORD  CAR-RECORD PRESENCES.
           MOVE CAR-EQUIPE TO POSE-CODE.
           CALL "6-POSE" USING LINK-V POSE-RECORD FAKE-KEY.
           INITIALIZE SH-01.
           IF POSE-JRS(LNK-MOIS) = 0
              PERFORM HJS VARYING IDX FROM 1 BY 1 UNTIL 
                                  IDX > MOIS-JRS(LNK-MOIS)
           ELSE
              PERFORM POS VARYING IDX FROM 1 BY 1 UNTIL 
                                  IDX > MOIS-JRS(LNK-MOIS)
           END-IF.

        POS.
           IF PRES-JOUR(LNK-MOIS, IDX) = 1 
              IF POSE-JOUR(LNK-MOIS, IDX) > 0
                 ADD CAR-HRS-JOUR TO SH-01
              END-IF
           END-IF.

       HJS.
           COMPUTE IDX-1 = SEM-IDX(LNK-MOIS, IDX).
           IF HJS-HEURES(IDX-1) > 0
           AND PRES-JOUR(LNK-MOIS, IDX) = 1 
              ADD  HJS-HEURES(IDX-1) TO SH-01
           END-IF.

       WRITE-PLANS.
           IF PT-TOTAL > 0
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-TIME TO PT-TIME
              MOVE LNK-USER   TO PT-USER
              WRITE PT-RECORD INVALID REWRITE PT-RECORD END-WRITE
           ELSE
              PERFORM DEL-PLANS
           END-IF.

       DEL-PLANS.
           DELETE PLANS INVALID CONTINUE.

       FILL-FLAG.
           IF FLAG-BEGIN > FLAG-END
              MOVE FLAG-BEGIN TO IDX
              MOVE FLAG-END TO FLAG-BEGIN
              MOVE IDX TO FLAG-END
           END-IF.
           IF LAST-PLAGE = SPACES
              MOVE PREV-PLAGE TO FLAG-PLAGE
           ELSE
              MOVE LAST-PLAGE TO FLAG-PLAGE
           END-IF.
           PERFORM FILL-HEURE VARYING JOUR-IDX FROM FLAG-BEGIN BY 1
           UNTIL JOUR-IDX > FLAG-END.
           PERFORM DIS-E2-01.
           MOVE FLAG-END TO JOUR-IDX.
           INITIALIZE FLAG-BEGIN FLAG-END.

       FILL-HEURE.
           IF  PRES-JOUR(LNK-MOIS, JOUR-IDX) = 1 
           AND PT-PLAGE(JOUR-IDX) = SPACES
              PERFORM F-H.

       F-H.
           MOVE SEM-IDX(LNK-MOIS, JOUR-IDX) TO IDX-3.
           MOVE 0 TO HELP-5.
           IF  POSE-JRS(LNK-MOIS) > 0
           AND POSE-JOUR(LNK-MOIS, JOUR-IDX) > 0
               MOVE 1 TO HELP-5
           ELSE
              MOVE HJS-HEURES(IDX-3) TO HELP-5
           END-IF.
           IF HELP-5 > 0
              MOVE FLAG-PLAGE TO PT-PLAGE(JOUR-IDX)
              PERFORM READ-PLAGE
              PERFORM FILL-PLAGE.


       DISPLAY-INIT.
           CALL "4-ABS" USING LINK-V REG-RECORD ABSENCE.
           PERFORM INIT-PT.
           READ PLANS NO LOCK INVALID CONTINUE.
           PERFORM DIS-E2-01.
           PERFORM CUMUL-PT.

       INIT-PT.
           INITIALIZE PT-RECORD.
           PERFORM KEY-PT.

       KEY-PT.
           MOVE FR-KEY     TO PT-FIRME  PT-FIRME-1.
           MOVE REG-PERSON TO PT-PERSON PT-PERSON-1.
           MOVE LNK-MOIS   TO PT-MOIS   PT-MOIS-1.

       CUMUL-PT.
           MOVE 0 TO PT-TOTAL.
           PERFORM ADD-GLOBAL VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.
           MOVE SH-01      TO HE-Z3Z2.
           INSPECT HE-Z3Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z3Z2 LINE 20 POSITION 69.

           MOVE PT-TOTAL TO HE-Z3Z2.
           INSPECT HE-Z3Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HE-Z3Z2 LINE 21 POSITION 69.
           COMPUTE SH-00 = SH-01 - PT-TOTAL.
           MOVE SH-00      TO HE-Z3Z2 HES2Z2.
           INSPECT HE-Z3Z2 REPLACING ALL ",00" BY "   "
           INSPECT HES2Z2 REPLACING ALL ",00" BY "   "
           IF SH-00 > 0
              DISPLAY HE-Z3Z2 LINE 22 POSITION 69
           ELSE
              DISPLAY HES2Z2 LINE 22 POSITION 69 REVERSE.
       
       ADD-GLOBAL.
           ADD PT-VAL(IDX) TO PT-TOTAL.

       DEL-PT.
           PERFORM KEY-PT.
           DELETE PLANS INVALID CONTINUE.

       LIN-COL.
           COMPUTE LIN-IDX = 3 + 3 * SEM-LINE(LNK-MOIS, JOUR-IDX).
           COMPUTE COL-IDX = 11 * SEM-IDX(LNK-MOIS, JOUR-IDX) - 9.

       LIN-COL-X.
           COMPUTE LIN-IDX = 2 +  3 * SEM-LINE(LNK-MOIS, IDX).
           COMPUTE COL-IDX = 11 * SEM-IDX(LNK-MOIS, IDX) - 5.

       DIS-NP.
           PERFORM LIN-NP VARYING JOUR-IDX FROM 1 BY 1
                            UNTIL JOUR-IDX > MOIS-JRS(LNK-MOIS).

       LIN-NP.
           PERFORM LIN-COL.
           IF PRES-JOUR(LNK-MOIS, JOUR-IDX) = 0
              DISPLAY " []" LINE LIN-IDX POSITION COL-IDX
           ELSE
              DISPLAY "     " LINE LIN-IDX POSITION COL-IDX
           END-IF.

       LIN-COL-VAL.
           PERFORM LIN-COL.
           COMPUTE COL-IDX = COL-IDX - 1.
           IF PRES-JOUR(LNK-MOIS, JOUR-IDX) = 1
              DISPLAY HE-Z6 LINE LIN-IDX POSITION COL-IDX.

       NEXT-OCCUP.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD SAVE-KEY.
           MOVE OCC-KEY TO OT-NUMBER
           CALL "6-OCCTXT" USING LINK-V OT-RECORD FAKE-KEY.

       AFFICHAGE-ECRAN.
           MOVE 301 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE  5 TO LNK-LINE.
           MOVE  2 TO LNK-COL.
           MOVE  3 TO LNK-SPACE.
           MOVE 11 TO LNK-SIZE.
           CALL "0-DSCAL" USING LINK-V.

       AFFICHAGE-DETAIL.
           PERFORM DIS-E1-01 THRU DIS-E1-END.
           PERFORM DIS-E2-01 THRU DIS-E2-END.

       DIS-E1-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-E1-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-E1-END.
           EXIT.

       DIS-E2-01.
           PERFORM DISPLAY-CAL VARYING JOUR-IDX FROM 1 BY 1
              UNTIL JOUR-IDX > MOIS-JRS(LNK-MOIS).
       DIS-E2-END.
           EXIT.

       DISPLAY-CAL.
           PERFORM LIN-COL.
           DISPLAY PT-PLAGE(JOUR-IDX) LINE LIN-IDX POSITION COL-IDX
           SIZE 10.
           IF PRES-JOUR(LNK-MOIS, JOUR-IDX) = 0 
              DISPLAY "[]        " LINE LIN-IDX POSITION COL-IDX
           END-IF.
           ADD 1 TO LIN-IDX.
           MOVE PT-VAL(JOUR-IDX) TO HEZ2Z2.
           INSPECT HEZ2Z2 REPLACING ALL ",00" BY "   "
           DISPLAY HEZ2Z2 LINE LIN-IDX POSITION COL-IDX LOW.
           SUBTRACT 2 FROM LIN-IDX.
           ADD 2 TO COL-IDX.
           IF INTER(JOUR-IDX) > 0 
              MOVE INTER(JOUR-IDX) TO LINK-KEY
              CALL "6-OCCUP" USING LINK-V LINK-RECORD FAKE-KEY
              DISPLAY LINK-CODE LINE LIN-IDX POSITION COL-IDX 
              SIZE 9 LOW
           ELSE
              DISPLAY SPACES LINE LIN-IDX POSITION COL-IDX SIZE 9
           END-IF.

       END-PROGRAM.
           CLOSE PLANS.
           CANCEL "0-DSCAL".
           CANCEL "6-OCCUP".
           CANCEL "2-PLAGE".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS THRU SUBTRACT-END
             WHEN 58 PERFORM ADD-MOIS THRU ADD-END
           END-EVALUATE.
           MOVE 99 TO EXC-KEY.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM DISPLAY-INIT.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0 
              GO SUBTRACT-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO SUBTRACT-END.
           GO SUBTRACT-MOIS.
       SUBTRACT-END.
           IF LNK-MOIS = 0
              MOVE SAVE-MOIS TO LNK-MOIS
           END-IF.
       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
              GO ADD-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO ADD-END.
           GO ADD-MOIS.
       ADD-END.
           IF LNK-MOIS > 12
              MOVE SAVE-MOIS TO LNK-MOIS
           END-IF.

       READ-PLAGE.
           MOVE PT-PLAGE(JOUR-IDX) TO PLG-KEY.
           CALL "6-PLAGE" USING LINK-V PLG-RECORD EXC-KEY.

       FILL-PLAGE.
           MOVE PLG-HORAIRE TO PT-HORAIRE(JOUR-IDX).
           IF PT-PLAGE(JOUR-IDX) NOT = SPACES
              MOVE PT-PLAGE(JOUR-IDX) TO LAST-PLAGE
           END-IF.
           COMPUTE PT-VAL(JOUR-IDX) = PT-NET(JOUR-IDX, 1) 
                                    + PT-NET(JOUR-IDX, 2).
           
       DIS-PLAGE.
           DISPLAY SPACES  LINE 22 POSITION 1 SIZE 50.
           DISPLAY PLG-NOM LINE 21 POSITION 1 SIZE 50.
           MOVE PLG-OCC TO OCC-KEY.
           IF  PLG-NOM NOT = SPACES
           AND OCC-KEY NOT = 0
              CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY
              MOVE OCC-KEY TO OT-NUMBER
              CALL "6-OCCTXT" USING LINK-V OT-RECORD FAKE-KEY
              DISPLAY OT-NOM LINE 21 POSITION 1 SIZE 50 LOW.
           MOVE PT-DEBUT(JOUR-IDX, 1) TO HELP-ZZ.
           INSPECT HELP-ZZ REPLACING ALL ".00" BY "   "
           DISPLAY HELP-ZZ LINE 21 POSITION 30
           MOVE PT-FIN(JOUR-IDX, 1) TO HELP-ZZ.
           INSPECT HELP-ZZ REPLACING ALL ".00" BY "   "
           DISPLAY ">" LINE 22 POSITION 35.
           DISPLAY HELP-ZZ LINE 21 POSITION 36 LOW.
           MOVE PT-REPOS(JOUR-IDX, 1) TO HELP-ZZ.
           INSPECT HELP-ZZ REPLACING ALL ".00" BY "   "
           DISPLAY HELP-ZZ LINE 21 POSITION 42 LOW.
           DISPLAY ">" LINE 21 POSITION 35.

           MOVE PT-DEBUT(JOUR-IDX, 2) TO HELP-ZZ.
           INSPECT HELP-ZZ REPLACING ALL ".00" BY "   "
           DISPLAY HELP-ZZ LINE 22 POSITION 30 LOW.
           MOVE PT-FIN(JOUR-IDX, 2) TO HELP-ZZ.
           INSPECT HELP-ZZ REPLACING ALL ".00" BY "   "
           DISPLAY HELP-ZZ LINE 22 POSITION 36 LOW.
           MOVE PT-REPOS(JOUR-IDX, 2) TO HELP-ZZ.
           INSPECT HELP-ZZ REPLACING ALL ".00" BY "   "
           DISPLAY HELP-ZZ LINE 22 POSITION 42 LOW.
           IF PT-DEBUT(JOUR-IDX, 2) NOT = 0
              DISPLAY ">" LINE 22 POSITION 35.

       AVANT-TEXTE.
           ACCEPT PT-COMMENT(JOUR-IDX)
             LINE 23 POSITION 1 SIZE 80
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-DEBUT.
           PERFORM POSE-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 30 SIZE 5.
           ACCEPT PT-DEBUT(IDX-2, POSE-IDX)
             LINE LIN-IDX POSITION 30 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-FIN.
           PERFORM POSE-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 36 SIZE 5.
           IF  POSE-IDX = 2
           AND PT-DEBUT(IDX-2, POSE-IDX) = 0
              CONTINUE
           ELSE
              ACCEPT PT-FIN(IDX-2, POSE-IDX)
              LINE LIN-IDX POSITION 36 SIZE 4
              TAB UPDATE NO BEEP CURSOR  1
              ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-PAUSE.
           PERFORM POSE-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 42 SIZE 5.
           IF  POSE-IDX = 2
           AND PT-DEBUT(IDX-2, POSE-IDX) = 0
              CONTINUE
           ELSE
              ACCEPT PT-REPOS(IDX-2, POSE-IDX)
              LINE LIN-IDX POSITION 42 SIZE 4
              TAB UPDATE NO BEEP CURSOR 1
              ON EXCEPTION EXC-KEY CONTINUE.

       APRES-DEBUT.
           IF POSE-IDX = 2
              IF PT-DEBUT(IDX-2, POSE-IDX) > 0 
              AND PT-DEBUT(IDX-2, POSE-IDX) < PT-FIN(IDX-2, 1)
               MOVE PT-FIN(IDX-2, 1) TO PT-DEBUT(IDX-2, POSE-IDX)  
               MOVE 1 TO INPUT-ERROR
              END-IF
           END-IF.
           IF PT-HR-D(IDX-2, POSE-IDX) > 23
              MOVE 23 TO PT-HR-D(IDX-2, POSE-IDX)
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF PT-MIN-D(IDX-2, POSE-IDX) > 59
              MOVE 59 TO PT-MIN-D(IDX-2, POSE-IDX)
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF PT-DEBUT(IDX-2, POSE-IDX) > 0 
           AND PT-DEBUT(IDX-2, POSE-IDX) = PT-FIN(IDX-2, POSE-IDX)
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-FIN.
           IF PT-HR-F(IDX-2, POSE-IDX) > 23
              MOVE 23 TO PT-HR-F(IDX-2, POSE-IDX)        
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF PT-MIN-F(IDX-2, POSE-IDX) > 59
              MOVE 59 TO PT-MIN-F(IDX-2, POSE-IDX)        
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF  PT-DEBUT(IDX-2, POSE-IDX) > 0
           AND PT-DEBUT(IDX-2, POSE-IDX) = PT-FIN(IDX-2, POSE-IDX)
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF POSE-IDX = 2
              IF  PT-DEBUT(IDX-2, POSE-IDX) = 0
              AND PT-FIN(IDX-2, POSE-IDX) = 0
                 MOVE 0 TO INPUT-ERROR
              END-IF
           END-IF.

       APRES-PAUSE.
           IF HELP-2B > 59
              MOVE 59 TO HELP-2B
              MOVE HELP-2 TO PT-REPOS(IDX-2, POSE-IDX)
              MOVE 1 TO INPUT-ERROR
           END-IF.
           MOVE PT-FIN(IDX-2, POSE-IDX) TO HELP-2.
           IF PT-FIN(IDX-2, POSE-IDX) < PT-DEBUT(IDX-2, POSE-IDX)
              ADD 2400 TO HELP-2
           END-IF.
           SUBTRACT PT-DEBUT(IDX-2, POSE-IDX) FROM HELP-2.
           IF HELP-2B > 59
              SUBTRACT 40 FROM HELP-2B
           END-IF.
           IF HELP-2 < PT-REPOS(IDX-2, POSE-IDX)
              MOVE HELP-2 TO PT-REPOS(IDX-2, POSE-IDX)
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR = 0
              CALL "4-MINUTE" USING PT-TEMPS(IDX-2, POSE-IDX).
           MOVE IDX-2 TO JOUR-IDX.
      *    PERFORM DIS-PLAGE.
           IF POSE-IDX = 2
              COMPUTE PT-VAL(JOUR-IDX) = PT-NET(JOUR-IDX, 1) 
                                       + PT-NET(JOUR-IDX, 2)
              PERFORM CUMUL-PT 
              PERFORM DIS-E2-01
              MOVE 2 TO ECRAN-IDX.
           
       POSE-IDX.
           EVALUATE INDICE-ZONE
               WHEN 1 THRU 3 
                    MOVE 1 TO POSE-IDX
                    MOVE 21 TO LIN-IDX
               WHEN 4 THRU 6
                    MOVE 2 TO POSE-IDX
                    MOVE 22 TO LIN-IDX.
