      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-FAC FACTURE + ARTICLES                    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-FAC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FACTINT.FC".
           COPY "FACDINT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FACTINT.FDE".
           COPY "FACDINT.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 8.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�


       01  FX-RECORD.
           02 FX-KEY-3.
              03 FX-DEBUT              PIC 9(6).
              03 FX-DEBUT-R REDEFINES FX-DEBUT.
                 04 FX-DEBUT-A         PIC 9999.
                 04 FX-DEBUT-M         PIC 99.
              03 FX-KEY-2.
                 04 FX-CLIENT          PIC 9(8).
                 04 FX-NUMBER          PIC 9(8).

           02 FX-REC-DET.
      * Dates drapeau
              03 FX-TOTAL            PIC S9(8)V99.
              03 FX-TVA              PIC S9(8)V99.
              03 FX-A-PAYER          PIC S9(8)V99.
              03 FX-TAUX-TVA         PIC 99V99.
              03 FX-REFERENCE        PIC X(20).
              03 FX-TEXTE            PIC X(50).
              03 FX-EDIT             PIC 9(8).
              03 FX-EDIT-R REDEFINES FX-EDIT.
                 04 FX-EDIT-A        PIC 9999.
                 04 FX-EDIT-M        PIC 99.
                 04 FX-EDIT-J        PIC 99.
              03 FX-SOLDE            PIC 9(8).
              03 FX-SOLDE-R REDEFINES FX-SOLDE.
                 04 FX-SOLDE-A       PIC 9999.
                 04 FX-SOLDE-M       PIC 99.
                 04 FX-SOLDE-J       PIC 99.
              03 FX-PAYE             PIC 9(8)V99.
              03 FX-DATE-PAYE        PIC 9(8).
              03 FX-PAYE-R REDEFINES FX-DATE-PAYE.
                 04 FX-PAYE-A        PIC 9999.
                 04 FX-PAYE-M        PIC 99.
                 04 FX-PAYE-J        PIC 99.
              03 FX-FREQ-FACT        PIC 9.
              03 FX-FREQUENCE        PIC 99.
      * 1ER, 2E, ETC
              03 FX-FILLER           PIC X(179).
              03 FX-STAMP.
                 04 FX-TIME.
                    05 FX-ST-ANNEE PIC 9999.
                    05 FX-ST-MOIS  PIC 99.
                    05 FX-ST-JOUR  PIC 99.
                    05 FX-ST-HEURE PIC 99.
                    05 FX-ST-MIN   PIC 99.
                 04 FX-USER        PIC X(10).

       01  FXD-RECORD.
           02 FXD-KEY-3.
              03 FXD-NUMBER        PIC 9(8).
              03 FXD-KEY-2.
                 04 FXD-CLIENT     PIC 9(8).
                 04 FXD-KEY.
                    06 FXD-PERSON  PIC 9(8).
                    06 FXD-ANNEE   PIC 9999.
                    06 FXD-MOIS    PIC 99.
                    06 FXD-MISSION PIC 9(8).
                    06 FXD-CODE    PIC 9(4).
                    06 FXD-NUM-1   PIC 9(8).

           02 FXD-REC-DET.
              03 FXD-VALEURS.
                 04 FXD-DONNEE     PIC 9(8)V9(5) OCCURS 6.
              03 FXD-VAL-DET REDEFINES FXD-VALEURS.
                 04 FXD-DONNEE-1   PIC 9(8)V9(5).
                 04 FXD-DONNEE-2   PIC 9(8)V9(5).
                 04 FXD-POURCENT   PIC 9(8)V9(5).
                 04 FXD-UNITE      PIC 9(8)V9(5).
                 04 FXD-UNITAIRE   PIC 9(8)V9(5).
                 04 FXD-TOTAL      PIC 9(8)V9(5).
              03 FXD-TEXTE         PIC X(30).
              03 FXD-FILLER        PIC X(100).
              03 FXD-STAMP.
                 04 FXD-TIME.
                    05 FXD-ST-ANNEE PIC 9999.
                    05 FXD-ST-MOIS  PIC 99.
                    05 FXD-ST-JOUR  PIC 99.
                    05 FXD-ST-HEURE PIC 99.
                    05 FXD-ST-MIN   PIC 99.
                 04 FXD-USER        PIC X(10).


           COPY "V-VAR.CPY".
           COPY "FIRME.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
SU         COPY "CODTXT.REC".
           COPY "CODPAIE.REC".
           COPY "PARMOD.REC".
           COPY "TVA.REC".

       01  COMPTEUR              PIC 99 COMP-1.
       01  MODIFICATION          PIC 9 VALUE 0.
       01  LAST-FIRME            PIC 9(6) VALUE 0.
       01  LAST-MOIS             PIC 9(2) VALUE 0.

       01  SAL-ARR               PIC 9(8)V99.
       01  SAL-TOT               PIC 9(8)V9(5).
       01  SAL-ACT               PIC S9(8)V9(5).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02 SAL-ACT-A          PIC S9(8).
           02 SAL-ACT-B          PIC 9(5).
       01  IND-TEXTE             PIC X(25).

       01  VAL-HLP-08.
           02 VH-08          PIC 9(8)V99999 OCCURS 6.

       01  H-Z1.
           03 FILLER         PIC Z(7).
           03 ZZ-01          PIC Z,Z(5).
       01  H-Z2 REDEFINES H-Z1.
           03 FILLER         PIC Z(6).
           03 ZZ-02          PIC ZZ,Z(5).
       01  H-Z3 REDEFINES H-Z1.
           03 FILLER         PIC Z(5).
           03 ZZ-03          PIC ZZZ,Z(5).
       01  H-Z4 REDEFINES H-Z1.
           03 FILLER         PIC Z(4).
           03 ZZ-04          PIC Z(4),Z(5).
       01  H-Z5 REDEFINES H-Z1.
           03 FILLER         PIC Z(3).
           03 ZZ-05          PIC Z(5),Z(5).
       01  H-Z6 REDEFINES H-Z1.
           03 FILLER         PIC ZZ.
           03 ZZ-06          PIC Z(6),Z(5).
       01  H-Z7 REDEFINES H-Z1.
           03 FILLER         PIC Z.
           03 ZZ-07          PIC Z(7),Z(5).
       01  H-Z8 REDEFINES H-Z1.
           03 ZZ-08          PIC Z(8),Z(5).

       01  HELP-VAL              PIC Z BLANK WHEN ZERO.
       01  HELP-1                PIC 9999.
       01  HELP-2                PIC 9.
       01  HELP-3                PIC S9(6)V99 COMP-3.

       01  FACTURE-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FI".
           02 ANNEE-FACTURE      PIC 9999.
       01  FID-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FD".
           02 ANNEE-FID          PIC 9999.

       01   ECR-DISPLAY.
            02 HE-Z2 PIC ZZ  BLANK WHEN ZERO.
            02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4).
            02 HE-Z6 PIC Z(6).
            02 HE-Z8 PIC Z(8)-.
            02 HE-Z2Z2 PIC ZZ,ZZ. 
            02 HE-Z4Z2 PIC -ZZZZ,ZZ BLANK WHEN ZERO. 
            02 HE-Z6Z2 PIC Z(6),ZZ. 
            02 HE-Z6Z4 PIC Z(6),Z(4). 
            02 HE-Z8Z5 PIC Z(8),Z(5) BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FACTURE FID.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-FAC .
       
           INITIALIZE PARMOD-RECORD.
           MOVE "FACTURE" TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           INITIALIZE FID-RECORD FI-RECORD.
           MOVE FR-KEY TO FIRME-KEY.
           MOVE LNK-SUFFIX TO ANNEE-FACTURE ANNEE-FID.
           OPEN I-O  FACTURE.
           OPEN I-O  FID.

           PERFORM AFFICHAGE-ECRAN .
           MOVE 0 TO MODIFICATION.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0163640015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 3  MOVE 0163640015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 4  MOVE 0164002400 TO EXC-KFR(1)
                      MOVE 4100081774 TO EXC-KFR(2)
                      MOVE 0000130000 TO EXC-KFR(3)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600680000 TO EXC-KFR(14)
           WHEN 8  MOVE 0000000005 TO EXC-KFR(1)
                   MOVE 0000080000 TO EXC-KFR(2)
                   MOVE 0052000000 TO EXC-KFR(11)
           END-EVALUATE.

           IF INDICE-ZONE < 5 OR INDICE-ZONE > 10
              PERFORM DISPLAY-F-KEYS
           ELSE
              COMPUTE IDX-1 = INDICE-ZONE - 4
              IF CS-SAISIE(IDX-1) > 0 
                 PERFORM DISPLAY-F-KEYS
              END-IF
           END-IF.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  8 PERFORM AVANT-DEC
           WHEN OTHER PERFORM AVANT-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           IF EXC-KEY NOT = 27
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  8 PERFORM APRES-DEC
           WHEN OTHER PERFORM APRES-ALL.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT FIRME-KEY 
              LINE 3 POSITION 15 SIZE 6
              TAB UPDATE NO BEEP CURSOR 1
              ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT FIRME-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           ACCEPT FI-NUMBER
             LINE  5 POSITION 20 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE.


       AVANT-4.            
           IF CD-NUMBER < 8001
              MOVE 8001 TO CD-NUMBER
           END-IF.
           ACCEPT CD-NUMBER
             LINE  6 POSITION 24 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF EXC-KEY = 27 
           OR EXC-KEY = 52 
              IF MODIFICATION = 1
                  PERFORM CALCUL
              END-IF
           END-IF.

       AVANT-ALL.
           PERFORM RECHARGE VARYING IDX FROM 4 BY 1 UNTIL IDX > 6.
           COMPUTE IDX = INDICE-ZONE - 1.
           COMPUTE LIN-IDX = IDX + 4.
           COMPUTE COL-IDX = 24 - CS-COLONNES(IDX).
           COMPUTE SIZ-IDX = CS-DECIMALES(IDX) + CS-COLONNES(IDX).
           IF CS-DECIMALES(IDX) > 0 ADD 1 TO SIZ-IDX.
           IF CS-SAISIE(IDX) > 0 
              PERFORM AVANT-ALL-1
           END-IF.
           IF IDX = 6
              IF EXC-KEY = 5 
                 MOVE VH-08(6) TO FID-TOTAL
                 COMPUTE FID-UNITE = FID-TOTAL / FID-UNITAIRE
                 MOVE FID-UNITE TO VH-08(4)
                 PERFORM DIS-HE-ALL
              END-IF
              IF EXC-KEY = 6 
                 MOVE VH-08(6) TO FID-TOTAL
                 COMPUTE FID-UNITAIRE = FID-TOTAL / FID-UNITE
                 MOVE FID-UNITAIRE TO VH-08(5)
                 PERFORM DIS-HE-ALL
              END-IF
           END-IF.
             
       RECHARGE.
           MOVE FID-DONNEE(IDX) TO VH-08(IDX).

       AVANT-ALL-1.
           MOVE VH-08(IDX) TO ZZ-08.
           EVALUATE CS-COLONNES(IDX)
           WHEN 1 ACCEPT ZZ-01
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 2 ACCEPT ZZ-02
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           WHEN 3 ACCEPT ZZ-03
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 4 ACCEPT ZZ-04
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           WHEN 5 ACCEPT ZZ-05
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 6 ACCEPT ZZ-06
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           WHEN 7 ACCEPT ZZ-07
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 8 ACCEPT ZZ-08
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           END-EVALUATE.
           INSPECT ZZ-08 REPLACING ALL "." BY ",".
           MOVE ZZ-08 TO VH-08(IDX).

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   CALL "2-FIRME" USING LINK-V FIRME-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  OTHER PERFORM NEXT-FIRME.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-FIRME
           END-EVALUATE.                     
           PERFORM FACTURE-RECENTE.
           PERFORM AFFICHAGE-DETAIL.

       APRES-3.
           READ FACTURE INVALID INITIALIZE FI-REC-DET END-READ.
           IF FIRME-KEY NOT = FI-CLIENT 
              INITIALIZE FI-RECORD
              MOVE FIRME-KEY TO FI-CLIENT 
           END-IF.
           IF FI-NUMBER > 0 
              PERFORM PREMIER-CS
           ELSE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

--     APRES-4.
           MOVE 0 TO INPUT-ERROR.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 1 MOVE 01001001 TO LNK-VAL
                  PERFORM HELP-SCREEN
                  MOVE 1 TO INPUT-ERROR
           WHEN 4 PERFORM RECAP-FID
                  MOVE 13 TO SAVE-KEY
                  PERFORM NEXT-CS 
                  MOVE 4 TO SAVE-KEY
           WHEN 6 PERFORM READ-FID
                  PERFORM CS-TEXT THRU CS-TEXT-END 
                  MOVE 1 TO INPUT-ERROR
           WHEN  8 PERFORM DEL-FID
           WHEN  9 IF MODIFICATION = 1
                     PERFORM CALCUL
                     INITIALIZE CD-RECORD FID-RECORD
                  END-IF
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM DIS-HE-01 THRU DIS-HE-03
                  MOVE 1 TO INPUT-ERROR
                  MOVE 98 TO EXC-KEY
           WHEN OTHER PERFORM NEXT-CS 
           END-EVALUATE.
           IF CD-NUMBER < 8001
              MOVE 8001 TO CD-NUMBER
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF CD-NUMBER > 8100
              MOVE 8100 TO CD-NUMBER
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR = 0
               PERFORM LOAD-FID.
           PERFORM DIS-HE-04.


      
       LOAD-FID.
           PERFORM READ-FID.
           IF IND-TEXTE NOT = SPACES
              MOVE IND-TEXTE TO FID-TEXTE
              INITIALIZE IND-TEXTE.
      *    PERFORM RECHARGE VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.
           PERFORM DIS-HE-04.
           PERFORM DIS-HE-ALL.


       READ-FID.
           IF INPUT-ERROR = 0
              PERFORM FID-KEY
              READ FID NO LOCK INVALID
                   INITIALIZE FID-REC-DET
      *            PERFORM SET-UP VARYING IDX FROM 1 BY 1 UNTIL IDX > 6
              END-READ
           END-IF.    

       TEST-CS.
           MOVE 0 TO LNK-NUM INPUT-ERROR.
           IF CD-NUMBER = 0 
              MOVE 1 TO LNK-NUM.
           IF LNK-NUM NOT = 0 
              IF LNK-NUM = 1 
                 MOVE "AA" TO LNK-AREA
              ELSE
                 MOVE "SL" TO LNK-AREA
              END-IF
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-NUM.


       APRES-ALL.
           COMPUTE IDX = INDICE-ZONE - 1.
           MOVE VH-08(IDX) TO FID-DONNEE(IDX).
           IF IDX = 5
              IF  FID-DONNEE(4) > 0
              AND FID-DONNEE(5) > 0
                  COMPUTE FID-DONNEE(6) = FID-DONNEE(4) * FID-DONNEE(5) 
              END-IF
           END-IF.
           PERFORM DIS-HE-ALL.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM FID-KEY
                  CALL "0-TODAY" USING TODAY
                  MOVE TODAY-TIME TO FID-TIME
                  MOVE LNK-USER TO FID-USER
                  MOVE CS-NEGATIF TO FID-REMISE
                  PERFORM WRITE-FID
                  INITIALIZE VAL-HLP-08 
      *           INITIALIZE FID-RECORD CD-RECORD VAL-HLP-08 
                  MOVE 1 TO MODIFICATION
                  MOVE 4  TO DECISION
                  PERFORM DIS-HE-03 THRU DIS-HE-END
           WHEN 8 PERFORM DEL-FID
                  MOVE 4 TO DECISION
                  PERFORM DIS-HE-03 THRU DIS-HE-END
           IF DECISION < 3 AND MODIFICATION = 1
              PERFORM CALCUL
              INITIALIZE CD-RECORD FID-RECORD
           END-IF.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       CALCUL.
           INITIALIZE FID-RECORD FI-TOTAL FI-TVA FI-EDIT.
           MOVE FI-NUMBER TO FID-NUMBER.
           START FID KEY > FID-KEY-3 INVALID CONTINUE
              NOT INVALID
           PERFORM READ-FI-DET THRU READ-FID-END.
           INITIALIZE TVA-RECORD.
           MOVE LNK-ANNEE TO TVA-ANNEE.
           MOVE LNK-MOIS  TO TVA-MOIS.
           CALL "6-TVA" USING LINK-V TVA-RECORD PV-KEY.
           IF FIRME-PAYS = "L"
              COMPUTE FI-TVA = (FI-TOTAL * TVA-TAUX(1)) / 100 + ,005.
           COMPUTE FI-A-PAYER = FI-TVA + FI-TOTAL.
           MOVE FIRME-KEY TO FI-CLIENT.
           WRITE FI-RECORD INVALID REWRITE FI-RECORD.
           IF FI-TOTAL = 0
              DELETE FACTURE INVALID CONTINUE.
              
       READ-FI-DET.
           READ FID NEXT NO LOCK AT END 
              GO READ-FID-END.
           IF FI-NUMBER NOT = FID-NUMBER 
              GO READ-FID-END.
           IF FID-REMISE = SPACES
              ADD FID-TOTAL TO FI-TOTAL
           ELSE
              SUBTRACT FID-TOTAL FROM FI-TOTAL.
           GO READ-FI-DET.
       READ-FID-END.
           EXIT.

       NEXT-FIRME.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           MOVE FIRME-RECORD TO FR-RECORD.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.
           MOVE FR-RECORD TO FIRME-RECORD.
           
       NEXT-CS.
           MOVE 0 TO INPUT-ERROR.
           CALL "6-CSDEF" USING LINK-V CD-RECORD SAVE-KEY.
           IF CD-NUMBER NOT = 0
SU            CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY
              PERFORM TEST-CS
              IF INPUT-ERROR = 1
              AND SAVE-KEY NOT = 13
                 GO NEXT-CS
              END-IF
           END-IF.
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.

       RECAP-FID.
           PERFORM FID-KEY.
           START FID KEY > FID-KEY
                INVALID INITIALIZE FID-RECORD CD-RECORD
                NOT INVALID READ FID NEXT AT END
                INITIALIZE FID-RECORD CD-RECORD END-READ.
           IF FIRME-KEY NOT = FID-CLIENT
      *    OR LNK-MOIS   NOT = FID-MOIS 
      *    OR LNK-ANNEE  NOT = FID-ANNEE
              INITIALIZE FID-RECORD CD-RECORD
           ELSE
              ADD 1 TO COMPTEUR
           END-IF.
           MOVE FID-CODE   TO CD-NUMBER.
           MOVE FID-CLIENT TO FIRME-KEY.
           MOVE FID-NUMBER TO FI-NUMBER.
           PERFORM DIS-HE-04.

       NEXT-FID.
           PERFORM FID-KEY.
           START FID KEY > FID-KEY INVALID GO NEXT-FID-END.
       NEXT-FID-1.
           READ FID NEXT AT END
              GO NEXT-FID-END
           END-READ.
           IF LNK-ANNEE  NOT = FID-ANNEE
           OR LNK-MOIS   NOT = FID-MOIS 
           OR FIRME-KEY  NOT = FID-CLIENT
           OR FI-NUMBER  NOT = FID-NUMBER
              GO NEXT-FID-END
           END-IF.
           ADD 1 TO COMPTEUR.
           MOVE FID-CODE TO CD-NUMBER.
           MOVE 13 TO SAVE-KEY.
           PERFORM NEXT-CS.
           GO NEXT-FID-1.
       NEXT-FID-END.
           EXIT.

       DIS-HE-01.
           MOVE FIRME-KEY  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           DISPLAY FIRME-NOM LINE 3 POSITION 47 SIZE 33.

       DIS-HE-02.
           DISPLAY FIRME-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-03.
           MOVE FI-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 5 POSITION 20.
           MOVE FI-DEBUT-M TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 05301200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
           MOVE FI-DEBUT-A TO HE-Z8.
           DISPLAY HE-Z8  LINE 5 POSITION 40.

       DIS-HE-04.
           MOVE CD-NUMBER    TO HE-Z4.
           DISPLAY HE-Z4  LINE  6 POSITION 24.
           IF FID-TEXTE = SPACES
              DISPLAY CTX-NOM LINE 6 POSITION 33
           ELSE
              DISPLAY FID-TEXTE LINE 6 POSITION 33
           END-IF.
           PERFORM DISPLAY-TEXTE VARYING IDX-2 FROM 4 BY 1
           UNTIL IDX-2 > 6.

       DIS-HE-ALL.
           PERFORM DIS-VAL VARYING IDX-1 FROM 4 BY 1 UNTIL IDX-1 > 6.
       DIS-HE-END.
           EXIT.

       DIS-VAL.
           COMPUTE LIN-IDX = IDX-1 + 4.
           DISPLAY SPACES LINE LIN-IDX POSITION 16 SIZE 20.
           MOVE 24 TO COL-IDX.
           MOVE 2 TO CS-DECIMALES(IDX-1).
           IF CS-DECIMALES(IDX-1) > 0
              COMPUTE COL-IDX = 25 + CS-DECIMALES(IDX-1).
           MOVE FID-DONNEE(IDX-1) TO HE-Z8Z5.
           DISPLAY HE-Z8Z5 LINE LIN-IDX POSITION 16.
           DISPLAY "      " LINE LIN-IDX POSITION COL-IDX.

       DISPLAY-TEXTE.
           COMPUTE LIN-IDX = IDX-2 + 4.
      *    DISPLAY CTX-DESCRIPTION(IDX-2) LINE LIN-IDX POSITION 5 LOW.

       CS-TEXT.
           MOVE FID-TEXTE TO IND-TEXTE.
           MOVE 0000000000 TO EXC-KFR(1).
           MOVE 0000080000 TO EXC-KFR(2).
           PERFORM DISPLAY-F-KEYS.
           ACCEPT IND-TEXTE LINE 6 POSITION 33 SIZE 25
           TAB UPDATE NO BEEP CURSOR 1
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO CS-TEXT.
           IF EXC-KEY = 98 GO CS-TEXT.
           MOVE IND-TEXTE TO FID-TEXTE.
           REWRITE FID-RECORD INVALID CONTINUE 
           NOT INVALID INITIALIZE IND-TEXTE
           END-REWRITE.
           PERFORM DIS-HE-03.
       CS-TEXT-END.
           EXIT.

       WRITE-FID.
           IF FID-UNITE NOT = 0 
           OR FID-UNITAIRE NOT = 0 
           OR FID-TOTAL NOT = 0
              IF FID-CODE NOT = 0
                 MOVE 1 TO MODIFICATION
                 WRITE FID-RECORD INVALID REWRITE FID-RECORD
              END-IF
           END-IF.

       DEL-FID.
           PERFORM FID-KEY.
           DELETE  FID INVALID CONTINUE.
           MOVE 1 TO MODIFICATION.
           INITIALIZE FID-RECORD CD-RECORD.

       FID-KEY.
           IF FID-ANNEE = 0
              MOVE LNK-ANNEE  TO FID-ANNEE.
           IF FID-MOIS = 0
              MOVE LNK-MOIS   TO FID-MOIS.
           MOVE FI-NUMBER  TO FID-NUMBER FID-NUM-1.
           MOVE CD-NUMBER  TO FID-CODE.
           MOVE FIRME-KEY  TO FID-CLIENT.


       AFFICHAGE-ECRAN.
           MOVE 256 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY SPACES LINE 11 POSITION 1 SIZE 80.
           DISPLAY SPACES LINE 12 POSITION 1 SIZE 80.
           DISPLAY SPACES LINE 13 POSITION 1 SIZE 80.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF MODIFICATION = 1
              PERFORM CALCUL
           END-IF.
           CLOSE FID.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

       PREMIER-CS.
           INITIALIZE FID-RECORD.
           MOVE FI-NUMBER TO FID-NUMBER.
           START FID KEY > FID-KEY-3 INVALID CONTINUE
                NOT INVALID READ FID NEXT AT END
                INITIALIZE FID-RECORD CD-RECORD END-READ.
           IF FIRME-KEY NOT = FID-CLIENT
              INITIALIZE FID-RECORD CD-RECORD
           END-IF.
           MOVE FID-CODE TO CD-NUMBER.

       NUMERO-RECENT.
           MOVE 99999999 TO FI-NUMBER.
           START FACTURE KEY < FI-NUMBER INVALID INITIALIZE FI-RECORD
                NOT INVALID 
                   READ FACTURE PREVIOUS AT END 
                 INITIALIZE FI-RECORD
           END-READ.
           IF FI-NUMBER > PARMOD-PROG-NUMBER-1
              MOVE FI-NUMBER TO PARMOD-PROG-NUMBER-1
           END-IF.
           ADD 1 TO PARMOD-PROG-NUMBER-1.

       FACTURE-RECENTE.
           MOVE FIRME-KEY TO FI-CLIENT.
           MOVE 99999999 TO FI-NUMBER.
           START FACTURE KEY < FI-KEY-2 INVALID INITIALIZE FI-RECORD
                NOT INVALID 
                   READ FACTURE PREVIOUS AT END INITIALIZE FI-RECORD
           END-READ.
           IF FIRME-KEY NOT = FI-CLIENT 
              INITIALIZE FI-RECORD
           END-IF.



       X.  
           IF FI-NUMBER > PARMOD-PROG-NUMBER-1
              MOVE FI-NUMBER TO PARMOD-PROG-NUMBER-1
           END-IF.
           ADD 1 TO PARMOD-PROG-NUMBER-1.

