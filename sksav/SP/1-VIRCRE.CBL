      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-VIRCRE SAISIE MANUELLE VIREMENT CREDITEURS�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-VIRCRE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "VIRCRED.FC".
           COPY "FACTURE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FACTURE.FDE".
           COPY "VIRCRED.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1  PIC 99 VALUE  14.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX    PIC 99 OCCURS 1.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "CREDIT.REC".
           COPY "BANQUE.REC".
           COPY "BANQC.REC".
           COPY "BANQF.REC".
           COPY "MESSAGE.REC".
           COPY "V-VAR.CPY".

       01  ANNEE          PIC 9999.
       01  MOIS           PIC 99.
       01  JOUR           PIC 99.
       01  SUITE          PIC 99 VALUE 0.
       01  PIECE-V        PIC 9(8).
       01  MOIS-ACTUEL    PIC 99 VALUE 0.

       01  VIR-NAME.
           02 FILLER      PIC X(8) VALUE "A-VIRCRE".

       01  FACTURE-NAME.
           02 IDENTITE           PIC X(4) VALUE "A-AC".
           02 FIRME-FACTURE      PIC 9999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z7Z2 PIC Z(7),ZZ BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON VIRCRE FACTURE.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-VIRCRE .

           MOVE FR-KEY TO FIRME-FACTURE.
      
           OPEN INPUT FACTURE.
           OPEN I-O   VIRCRE.
           MOVE LNK-MOIS TO MOIS-ACTUEL.

      *    INDEX LANGUE 1 = DEFAUT

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0090172400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0029172415 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0029172415 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 11 THRU 13
                      MOVE 0000000034 TO EXC-KFR(1)
           WHEN CHOIX-MAX(1) 
                      MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1-1 
           WHEN  2 PERFORM AVANT-1-2 
           WHEN  3 PERFORM AVANT-1-3 
           WHEN  4 PERFORM AVANT-1-4 
           WHEN  5 PERFORM AVANT-1-5 
           WHEN  6 PERFORM AVANT-1-6 
           WHEN  7 PERFORM AVANT-1-7 
           WHEN  8 PERFORM AVANT-1-8 
           WHEN  9 PERFORM AVANT-1-9 
           WHEN 10 PERFORM AVANT-1-10 
           WHEN 11 PERFORM AVANT-1-11 
           WHEN 12 PERFORM AVANT-1-12 
           WHEN 13 PERFORM AVANT-1-13 
           WHEN 14 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-1 
           WHEN  2 PERFORM APRES-1-2 
           WHEN  3 PERFORM APRES-1-3 
           WHEN  4 PERFORM APRES-1-4 
           WHEN  5 PERFORM APRES-1-5 
           WHEN  6 PERFORM APRES-1-6 
           WHEN  7 PERFORM APRES-1-7 
           WHEN 14 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.            
           MOVE FR-KEY TO CR-FIRME.
           ACCEPT CR-NUMBER 
           LINE 3 POSITION 15 SIZE 6
           TAB UPDATE NO BEEP CURSOR 1
            ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.            
           ACCEPT CR-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.
           ACCEPT MOIS-ACTUEL
             LINE  5 POSITION 30 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

        AVANT-1-4.
           ACCEPT PIECE-V
             LINE  7 POSITION 24 SIZE 8 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-5.
           ACCEPT SUITE
             LINE  9 POSITION 30 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-6.
           ACCEPT VIR-BANQUE-D
             LINE 11 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-7.
           ACCEPT VIR-BANQUE-C
             LINE 13 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-8.
           ACCEPT VIR-COMPTE-C
             LINE 15 POSITION 40 SIZE 30
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-9.
           IF VIR-A-PAYER = 0
              MOVE FAC-A-PAYER TO VIR-A-PAYER.
           MOVE VIR-A-PAYER TO HE-Z7Z2 SH-00.
           MOVE 0 TO IDX-3.
           INSPECT HE-Z7Z2 TALLYING IDX-3 FOR ALL " ".
           ADD 1 TO IDX-3.
           IF IDX-3 > 5
              MOVE 3 TO IDX-3
           END-IF.
           ACCEPT HE-Z7Z2
             LINE 17 POSITION 30 SIZE 10 
             TAB UPDATE NO BEEP CURSOR IDX-3
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z7Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z7Z2 TO VIR-A-PAYER.

        AVANT-1-10.
           ACCEPT VIR-EXTRAIT
             LINE 18 POSITION 30 SIZE 20
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-11.
           MOVE 19300000 TO LNK-POSITION.
           IF MOIS-ACTUEL > 0 
           CALL "0-GDATE" USING VIR-DATE-EDITION
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-DATE TO VIR-DATE-EDITION
              MOVE 0 TO LNK-NUM
              MOVE 13 TO EXC-KEY
              PERFORM DIS-E1-10.

       AVANT-1-12.
           MOVE 20300000 TO LNK-POSITION.
           IF MOIS-ACTUEL > 0 
           CALL "0-GDATE" USING VIR-DATE-VIREMENT
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-DATE TO VIR-DATE-VIREMENT
              MOVE 0 TO LNK-NUM
              MOVE 13 TO EXC-KEY
              PERFORM DIS-E1-11.

       AVANT-1-13.
           MOVE 21300000 TO LNK-POSITION.
           IF MOIS-ACTUEL > 0 
           CALL "0-GDATE" USING VIR-DATE-SUSPENSION
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-DATE TO VIR-DATE-SUSPENSION
              MOVE 0 TO LNK-NUM
              MOVE 13 TO EXC-KEY
              PERFORM DIS-E1-12.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   CALL "2-CREDIT" USING LINK-V CR-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN 65 THRU 66 PERFORM NEXT-CREDIT
           WHEN 5 CALL "1-CREDIT" USING LINK-V
                  IF LNK-VAL > 0
                     MOVE LNK-VAL TO CR-NUMBER
                     PERFORM NEXT-CREDIT
                  END-IF
                  CANCEL "1-CREDIT"  
                  PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-CREDIT
           END-EVALUATE.                     
           PERFORM AFFICHAGE-DETAIL.
           INITIALIZE VIR-RECORD BQC-RECORD.
           PERFORM READ-VIREM.
           PERFORM DIS-E1-01 THRU DIS-E1-END.
           
       APRES-1-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-CREDIT
           END-EVALUATE.                     
           INITIALIZE VIR-RECORD.
           PERFORM READ-VIREM.
           PERFORM DIS-E1-01 THRU DIS-E1-END.

       APRES-1-3.
           IF MOIS-ACTUEL > 12
              MOVE 12 TO MOIS-ACTUEL
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-E1-03.
           

       APRES-1-4.
           EVALUATE EXC-KEY
           WHEN  13 IF PIECE-V NOT = 0
                       PERFORM READ-FACT
                    ELSE
                       PERFORM PREV-FAC
                    END-IF
                    PERFORM AFFICHAGE-DETAIL
           WHEN  65 PERFORM PREV-FAC
                    PERFORM AFFICHAGE-DETAIL
           WHEN  66 PERFORM NEXT-FAC
                    PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.
           PERFORM READ-VIREM.
           PERFORM DIS-E1-01 THRU DIS-E1-END.
           IF PIECE-V = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-1-5.
           EVALUATE EXC-KEY
           WHEN 65 PERFORM PREV-VIREM
                   MOVE VIR-PIECE TO PIECE-V
                   MOVE VIR-SUITE TO SUITE
           WHEN 66 PERFORM NEXT-VIREM
                   MOVE VIR-PIECE  TO PIECE-V
                   MOVE VIR-SUITE TO SUITE
           WHEN  2 CALL "2-VIRCRE" USING LINK-V CR-RECORD
                   CANCEL "2-VIRCRE"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN  3 CALL "2-BANQC" USING LINK-V BQC-RECORD CR-RECORD
                   CANCEL "2-BANQC"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           END-EVALUATE.
           IF VIR-BANQUE-C = SPACES
              MOVE BQC-BANQUE TO VIR-BANQUE-C
              MOVE BQC-COMPTE TO VIR-COMPTE-C
           END-IF.
           PERFORM READ-VIREM.
           PERFORM DIS-E1-01 THRU DIS-E1-END.

       APRES-1-6.
           MOVE EXC-KEY TO SAVE-KEY
           EVALUATE EXC-KEY
           WHEN  5 MOVE BQC-BANQUE TO LNK-TEXT
                   CALL "1-BANQUE" USING LINK-V
                   MOVE LNK-TEXT TO BQC-BANQUE BQ-CODE
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-E1-01 THRU DIS-E1-END
           WHEN  2 CALL "2-BANQF" USING LINK-V BQF-RECORD
                   MOVE BQF-CODE TO VIR-BANQUE-D 
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DIS-E1-01 THRU DIS-E1-END
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER MOVE VIR-BANQUE-D TO BQF-CODE
                   PERFORM READ-BANQF 
                   MOVE BQF-CODE TO VIR-BANQUE-D 
           END-EVALUATE.
      *  BQC-BANQUE-PREF
           PERFORM DIS-E1-06.
           IF BQ-CODE = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           MOVE BQ-CODE TO VIR-BANQUE-D.

       APRES-1-7.
           MOVE EXC-KEY TO SAVE-KEY
           EVALUATE EXC-KEY
           WHEN  5 CALL "1-BANQC" USING LINK-V
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
           WHEN  2 CALL "2-BANQC" USING LINK-V BQC-RECORD CR-RECORD
                   MOVE BQC-BANQUE TO VIR-BANQUE-C BQ-CODE
                   CANCEL "2-BANQC"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER MOVE VIR-BANQUE-C TO BQC-BANQUE BQ-CODE
                   PERFORM NEXT-BANQC 
                   MOVE VIR-BANQUE-C TO BQC-BANQUE BQ-CODE
           END-EVALUATE.
           IF BQ-CODE = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-E1-07.
           MOVE BQ-CODE TO VIR-BANQUE-C.

       APRES-DEC.
            EVALUATE EXC-KEY 
            WHEN  5 IF VIR-A-PAYER = 0
                       MOVE 8 TO EXC-KEY
                       GO APRES-DEC
                    END-IF
           MOVE FR-KEY      TO VIR-FIRME    VIR-FIRME-B 
                               VIR-FIRME-S  VIR-FIRME-N
           MOVE CR-NUMBER   TO VIR-CREDIT   VIR-CREDIT-B
                               VIR-CREDIT-S VIR-CREDIT-N
           MOVE MOIS-ACTUEL TO VIR-MOIS     VIR-MOIS-B  
                               VIR-MOIS-S   VIR-MOIS-N
           MOVE PIECE-V     TO VIR-PIECE    VIR-PIECE-B 
                               VIR-PIECE-S  VIR-PIECE-N HE-Z6
           MOVE SUITE       TO VIR-SUITE    VIR-SUITE-B 
                               VIR-SUITE-S  VIR-SUITE-N
               MOVE BQF-CODE   TO VIR-BANQUE-D
               MOVE BQF-COMPTE TO VIR-COMPTE-D
               MOVE BQC-BANQUE TO VIR-BANQUE-C
               MOVE BQC-COMPTE TO VIR-COMPTE-C
               INITIALIZE VIR-LIBELLE
               MOVE 1 TO IDX
               STRING CR-COMPTA DELIMITED BY "  " INTO VIR-LIBELLE 
               WITH POINTER IDX
               ADD 1 TO IDX
               STRING FAC-REFERENCE DELIMITED BY "  " INTO VIR-LIBELLE 
               WITH POINTER IDX
               MOVE 1 TO IDX
               STRING FAC-TEXTE DELIMITED BY "  " INTO VIR-LIBELLE-2
               WITH POINTER IDX
               STRING HE-Z6 DELIMITED BY SIZE INTO VIR-LIBELLE-2
               WITH POINTER IDX
                    WRITE VIR-RECORD INVALID 
                    REWRITE VIR-RECORD END-WRITE
                    MOVE 1 TO INDICE-ZONE ECRAN-IDX
                    PERFORM AFFICHAGE-ECRAN
                    PERFORM AFFICHAGE-DETAIL
                    MOVE 4 TO DECISION
            WHEN  8 DELETE VIRCRE INVALID CONTINUE
                    END-DELETE
                    MOVE 1 TO INDICE-ZONE ECRAN-IDX
                    PERFORM AFFICHAGE-ECRAN
                    PERFORM AFFICHAGE-DETAIL
                    INITIALIZE VIR-REC-DET  
                    MOVE 4 TO DECISION
           END-EVALUATE.
           IF DECISION NOT = 0
               COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
               MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       NEXT-CREDIT.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-CREDIT" USING LINK-V CR-RECORD A-N EXC-KEY.

       READ-BANQF.
           CALL "6-BANQF" USING LINK-V BQF-RECORD SAVE-KEY.

       READ-BANQC.
           CALL "6-BANQC" USING LINK-V CR-RECORD BQC-RECORD SAVE-KEY.

      *    HISTORIQUE DES VIRCRES
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       NEXT-BANQC.
           CALL "6-BANQC" USING LINK-V CR-RECORD BQC-RECORD SAVE-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD SAVE-KEY.

       NEXT-VIREM.
           MOVE FR-KEY TO VIR-FIRME.
           MOVE CR-NUMBER TO VIR-CREDIT.
           MOVE LNK-ANNEE  TO VIR-ANNEE,
           MOVE MOIS-ACTUEL  TO VIR-MOIS.
           START VIRCRE KEY > VIR-KEY INVALID CONTINUE
                NOT INVALID
           READ VIRCRE NEXT NO LOCK AT END 
               INITIALIZE VIR-RECORD END-READ.
           PERFORM TEST-VIREM.

       PREV-VIREM.
           MOVE FR-KEY TO VIR-FIRME.
           MOVE CR-NUMBER TO VIR-CREDIT.
           MOVE LNK-ANNEE  TO VIR-ANNEE,
           MOVE MOIS-ACTUEL  TO VIR-MOIS.
           START VIRCRE KEY < VIR-KEY INVALID CONTINUE
                NOT INVALID
           READ VIRCRE PREVIOUS NO LOCK AT END 
               INITIALIZE VIR-RECORD END-READ.
           PERFORM TEST-VIREM.

       READ-VIREM.
           INITIALIZE VIR-RECORD.
           MOVE FR-KEY TO VIR-FIRME    VIR-FIRME-B 
                          VIR-FIRME-S  VIR-FIRME-N.
           MOVE CR-NUMBER TO VIR-CREDIT   VIR-CREDIT-B
                             VIR-CREDIT-S VIR-CREDIT-N.
           MOVE LNK-ANNEE TO VIR-ANNEE   VIR-ANNEE-B  
                             VIR-ANNEE-S VIR-ANNEE-N.
           MOVE MOIS-ACTUEL TO VIR-MOIS   VIR-MOIS-B  
                               VIR-MOIS-S VIR-MOIS-N.
           MOVE PIECE-V TO VIR-PIECE    VIR-PIECE-B 
                           VIR-PIECE-S  VIR-PIECE-N.
           MOVE SUITE  TO VIR-SUITE   VIR-SUITE-B 
                          VIR-SUITE-S VIR-SUITE-N.
           READ VIRCRE NO LOCK INVALID CONTINUE. 
      *    CALL "6-VIREM" USING LINK-V CR-RECORD VIR-RECORD FAKE-KEY.
           CALL "6-BANQC" USING LINK-V CR-RECORD BQC-RECORD FAKE-KEY.

       TEST-VIREM.
           IF FR-KEY    NOT = VIR-FIRME
           OR CR-NUMBER NOT = VIR-CREDIT
               INITIALIZE VIR-RECORD.

       READ-FACT.
           INITIALIZE FAC-RECORD.
           MOVE CR-NUMBER TO FAC-CLIENT.
           MOVE PIECE-V TO FAC-NUMBER.
           READ FACTURE INVALID INITIALIZE FAC-RECORD.

       PREV-FAC.
           INITIALIZE FAC-RECORD.
           MOVE CR-NUMBER TO FAC-CLIENT.
           IF PIECE-V > 0
              MOVE PIECE-V TO FAC-NUMBER
           ELSE
              MOVE 99999999 TO FAC-NUMBER.
           START FACTURE KEY < FAC-KEY-2 INVALID CONTINUE
           NOT INVALID PERFORM READ-FACP THRU READ-FACP-END.

       READ-FACP.
           READ FACTURE PREVIOUS NO LOCK AT END 
              INITIALIZE FAC-RECORD
              GO READ-FACP-END.
           IF CR-NUMBER  NOT = FAC-CLIENT
              INITIALIZE FAC-RECORD
              GO READ-FACP-END.
           MOVE FAC-NUMBER TO PIECE-V.
           MOVE FAC-A-PAYER TO VIR-A-PAYER.
       READ-FACP-END.

       NEXT-FAC.
           INITIALIZE FAC-RECORD.
           MOVE CR-NUMBER TO FAC-CLIENT.
           MOVE PIECE-V TO FAC-NUMBER.
           START FACTURE KEY > FAC-KEY-2 INVALID 
           MOVE 0 TO PIECE-V 
           NOT INVALID PERFORM READ-FAC THRU READ-FAC-END.

       READ-FAC.
           READ FACTURE NEXT NO LOCK AT END 
              INITIALIZE FAC-RECORD
              GO READ-FAC-END.
           IF CR-NUMBER  NOT = FAC-CLIENT
              INITIALIZE FAC-RECORD
              GO READ-FAC-END.
           MOVE FAC-NUMBER TO PIECE-V.
           MOVE FAC-A-PAYER TO VIR-A-PAYER.
       READ-FAC-END.

       DIS-E1-01.
           MOVE CR-NUMBER  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6.
           DISPLAY CR-NOM       LINE 3 POSITION 22.
       DIS-E1-02.
           DISPLAY CR-MATCHCODE LINE 3 POSITION 33.
       DIS-E1-03.
           DISPLAY MOIS-ACTUEL LINE 5 POSITION 30.
           MOVE MOIS-ACTUEL TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 05351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-E1-04.
           MOVE PIECE-V  TO HE-Z8.
           DISPLAY HE-Z8 LINE 7 POSITION 24.

       DIS-E1-05.
           DISPLAY SUITE LINE 9 POSITION 30.
       DIS-E1-06.
           DISPLAY VIR-BANQUE-D LINE 11 POSITION 30.
           MOVE VIR-BANQUE-D TO BQ-CODE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD FAKE-KEY.
           PERFORM READ-BANQF.
           DISPLAY BQ-NOM     LINE 11 POSITION 40 SIZE 40.
           DISPLAY BQF-COMPTE LINE 12 POSITION 40 SIZE 40 LOW.
       DIS-E1-07.
           DISPLAY VIR-BANQUE-C LINE 13 POSITION 30.
           MOVE 13 TO SAVE-KEY.
           PERFORM READ-BANQC.
           IF  VIR-BANQUE-C = SPACES
           AND BQC-BANQUE NOT = SPACES
               MOVE BQC-BANQUE TO VIR-BANQUE-C.
           DISPLAY BQC-COMPTE LINE 14 POSITION 40 SIZE 40 LOW.
           DISPLAY BQC-BENEFIC-NOM LINE 15 POSITION 40 SIZE 40 LOW.
           MOVE VIR-BANQUE-C TO BQ-CODE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD FAKE-KEY.
           DISPLAY BQ-NOM      LINE 13 POSITION 40 SIZE 40.
       DIS-E1-08.
           DISPLAY VIR-COMPTE-C  LINE 14 POSITION 40 SIZE 40.
       DIS-E1-09.
           MOVE FAC-A-PAYER TO HE-Z7Z2.
           DISPLAY HE-Z7Z2  LINE  17 POSITION 30.
       DIS-E1-10.
           DISPLAY VIR-EXTRAIT   LINE 18 POSITION 30 SIZE 20.

       DIS-E1-11.
           MOVE VIR-JOUR-E  TO HE-JJ.
           MOVE VIR-MOIS-E  TO HE-MM.
           MOVE VIR-ANNEE-E TO HE-AA.
           DISPLAY HE-DATE  LINE  19 POSITION 30.

       DIS-E1-12.
           MOVE VIR-JOUR-V  TO HE-JJ.
           MOVE VIR-MOIS-V  TO HE-MM.
           MOVE VIR-ANNEE-V TO HE-AA.
           DISPLAY HE-DATE  LINE  20 POSITION 30.
       DIS-E1-13.
           MOVE VIR-JOUR-SS  TO HE-JJ.
           MOVE VIR-MOIS-SS  TO HE-MM.
           MOVE VIR-ANNEE-SS TO HE-AA.
           DISPLAY HE-DATE  LINE  21 POSITION 30.
       DIS-E1-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 532 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-E1-01 THRU DIS-E1-END.

       END-PROGRAM.
           CLOSE VIRCRE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".
           
