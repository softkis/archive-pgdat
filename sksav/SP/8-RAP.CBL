      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-RAP IMPRESSION RAPPORTS                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  8-RAP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "RAPPORT.FC".
           SELECT OPTIONAL TF-RAP ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "RAPPORT.FDE".
       FD  TF-RAP 
           LABEL RECORD STANDARD
           DATA RECORD TR-RECORD.

      *  ENREGISTREMENT FICHIER DE TRANSFER ASCII PERSONNE

      
       01  TR-RECORD.
           02 TR-FIRME           PIC 9(6).
           02 TR-FILLER-1        PIC X.
           02 TR-NOM             PIC X(30).
           02 TR-FILLER-2        PIC X.
           02 TR-ADR1            PIC X(40).
           02 TR-FILLER-2        PIC X.
           02 TR-ADR2            PIC X(40).
           02 TR-FILLER-2        PIC X.
           02 TR-ANNEE           PIC Z(5).
           02 TR-FILLER-1        PIC X.
           02 TR-MOIS            PIC Z(5).
           02 TR-FILLER-1        PIC X.
           02 TR-COMPTA          PIC X(10).
           02 TR-FILLER-2        PIC X.
           02 TR-FREQUENCE       PIC X(10).
           02 TR-FILLER-2        PIC X.

           02 TR-VALEURS         OCCURS 20.
              03 TR-VAL          PIC 9(10).
              03 TR-FILLER-1     PIC X.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "MESSAGE.REC".

       01  HEADER.
           02 HD-FIRME           PIC X(6) VALUE "FIRME".
           02 HD-FILLER-1        PIC X VALUE ";".
           02 HD-NOM             PIC X(30) VALUE "NOM".
           02 HD-FILLER-2        PIC X VALUE ";".
           02 HD-ADR1            PIC X(40) VALUE "RUE".
           02 HD-FILLER-2        PIC X VALUE ";".
           02 HD-ADR2            PIC X(40) VALUE "LOCALITE".
           02 HD-FILLER-2        PIC X VALUE ";".
           02 HD-ANNEE           PIC X(5) VALUE "ANNEE".
           02 HD-FILLER-1        PIC X VALUE ";".
           02 HD-MOIS            PIC X(5) VALUE "MOIS".
           02 HD-FILLER-1        PIC X VALUE ";".
           02 HD-COMPTA          PIC X(10) VALUE "COMPTA".
           02 HD-FILLER-2        PIC X VALUE ";".
           02 HD-FREQUENCE       PIC X(10) VALUE "FREQUENCE".
           02 HD-FILLER-2        PIC X VALUE ";".
           02 HD-VALEURS         OCCURS 20.
              03 HD-VAL          PIC X(10).
              03 HD-FILLER       PIC X.

       01  LOOP-OPEN             PIC 9 VALUE 0.
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  HE-Z5                 PIC Z(5).

       01  RAP-NAME.
           02 FILLER             PIC X(7) VALUE "S-RAPP.".
           02 ANNEE-RAP          PIC 999.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                RAPPORT
                TF-RAP.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-RAP .

       
           CALL "0-TODAY" USING TODAY.

           IF LOOP-OPEN = 0
              MOVE LNK-SUFFIX TO ANNEE-RAP
              OPEN I-O RAPPORT
              MOVE 1 TO LOOP-OPEN
           END-IF.

           IF LNK-VAL = 99
              PERFORM END-PROGRAM.
           CALL "0-TODAY" USING TODAY.
           INITIALIZE RAP-RECORD.
           MOVE FR-KEY TO RAP-FIRME.
           MOVE PARMOD-MOIS-DEB  TO RAP-MOIS.
           START RAPPORT KEY >= RAP-KEY INVALID CONTINUE
           NOT INVALID PERFORM READ-RAP THRU READ-RAP-END.
           EXIT PROGRAM.

       READ-RAP.
           READ RAPPORT NEXT AT END 
              GO READ-RAP-END.
           IF FR-KEY  NOT = RAP-FIRME
           OR PARMOD-MOIS-FIN < RAP-MOIS 
              GO READ-RAP-END.
           IF RAP-EDIT-A NOT = 0
              GO READ-RAP.
           PERFORM FULL-PROCESS.
           MOVE TODAY-ANNEE TO RAP-EDIT-A.
           MOVE TODAY-MOIS  TO RAP-EDIT-M.
           MOVE TODAY-JOUR  TO RAP-EDIT-J.
           REWRITE RAP-RECORD INVALID CONTINUE.
           GO READ-RAP.
       READ-RAP-END.
           EXIT.

       FULL-PROCESS.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-RAP
              PERFORM DEFINIT VARYING IDX FROM 1 BY 1 UNTIL IDX > 20
              WRITE TR-RECORD FROM HEADER
              MOVE 1 TO NOT-OPEN
           END-IF.
           PERFORM DONNEES-FIRME.

       END-PROGRAM.
           CLOSE RAPPORT.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".

       DONNEES-FIRME.
           INITIALIZE TR-RECORD.
           INSPECT TR-RECORD REPLACING ALL " " BY ";".
           MOVE FR-KEY TO TR-FIRME.
           MOVE FR-NOM TO TR-NOM

           INITIALIZE TR-ADR1 TR-ADR2.

           MOVE 1 TO IDX-1.
           STRING FR-MAISON DELIMITED BY "  " INTO TR-ADR1 
           POINTER IDX-1 ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-1.
           STRING FR-RUE DELIMITED BY "  " INTO TR-ADR1 POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.

           MOVE 1 TO IDX-1.
           STRING FR-PAYS DELIMITED BY " " INTO TR-ADR2 POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-1.
           MOVE FR-CODE-POST TO HE-Z5.
           STRING HE-Z5 DELIMITED BY SIZE INTO TR-ADR2 POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-1.
           STRING FR-LOCALITE DELIMITED BY "   " INTO TR-ADR2 
           POINTER IDX-1 ON OVERFLOW CONTINUE END-STRING.

           MOVE FR-COMPTA TO TR-COMPTA
           EVALUATE FR-FREQ-FACT  
                WHEN 0 THRU 1 MOVE 104 TO LNK-NUM
                WHEN 2 MOVE 108 TO LNK-NUM
                WHEN 3 MOVE 105 TO LNK-NUM
                WHEN 6 MOVE 106 TO LNK-NUM
                WHEN 9 MOVE 99  TO LNK-NUM
           END-EVALUATE.
           MOVE "AA" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO TR-FREQUENCE.

           MOVE RAP-MOIS TO TR-MOIS.
           MOVE LNK-ANNEE TO TR-ANNEE.
           PERFORM FILL-X VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.
           WRITE TR-RECORD.

       FILL-X.
           MOVE RAP-X(IDX) TO TR-VAL(IDX).


       DEFINIT.
           MOVE IDX   TO MS-NUMBER.
           MOVE "F"   TO LNK-LANGUAGE.
           MOVE "RAP" TO LNK-AREA.
           CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY.
           MOVE MS-DESCRIPTION TO HD-VAL(IDX).
           MOVE ";" TO HD-FILLER(IDX).
