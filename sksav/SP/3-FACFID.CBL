      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-FACFID IMPRESSION FACTURES FIDUCIAIRES    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-FACFID.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FACTINT.FC".
           COPY "FACDINT.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FACTINT.FDE".
           COPY "FACDINT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  PRECISION             PIC 99 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
SU         COPY "CODTXT.REC".
           COPY "FAC.LNK".
           COPY "BANQF.REC".
           COPY "BANQUE.REC".
           COPY "TVA.REC".
           COPY "PARMOD.REC".
       01  TXT-RECORD.
           02 TXT-KEY.
              03 TXT-MODULE  PIC X(10).
              03 TXT-FIRME   PIC 999999.
           02 TXT-REC-DET.
              03 TXT-TXT     PIC X(60) OCCURS 5.
              03 TXT-FILLER  PIC XXXX.

       01  PAGES                  PIC 99 VALUE 0.
       01  NOT-OPEN               PIC 99 VALUE 0.
       01  LIN-CS                 PIC 999 VALUE 0.
       01  COL-CS                 PIC 999 VALUE 0.
       01  UNI-CS                 PIC 999 VALUE 0.
       01  TOT-CS                 PIC 999 VALUE 0.
       01  DES-CS                 PIC 999 VALUE 0.
       01  POS-PR                 PIC 9(5) VALUE 0.
       01  POS-IDX                PIC 9(5) VALUE 0.

           COPY "V-VH00.CPY".

       01  FACTURE-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FI".
           02 FIRME-FACTURE      PIC 9999.
       01  FID-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FD".
           02 FIRME-FID          PIC 9999.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-FIRME            PIC 9(6).

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4Z2 PIC ZZZZ,ZZ.
           02 HE-Z5Z2 PIC Z(5),99.
           02 HE-Z6Z2 PIC Z(6),99.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FACTURE FID.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-FACFID.
       
           INITIALIZE PARMOD-RECORD TXT-RECORD.
           MOVE FR-KEY   TO SAVE-FIRME.
           MOVE LNK-MOIS TO SAVE-MOIS.
           CALL "0-TODAY" USING TODAY.
           MOVE LNK-SUFFIX TO FIRME-FACTURE FIRME-FID.
           MOVE 1 TO TXT-FIRME.
           OPEN I-O FACTURE.
           OPEN INPUT FID.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE TXT-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           CALL "6-PARMOD" USING LINK-V TXT-RECORD "R".

           PERFORM AFFICHAGE-ECRAN .
           INITIALIZE FR-RECORD.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           


       TRAITEMENT.
           MOVE 0 TO INPUT-ERROR.
           PERFORM START-FIRME.
           IF INPUT-ERROR = 0 
               PERFORM READ-FIRME THRU READ-FIRME-END.
           PERFORM END-PROGRAM.

       START-FIRME.
           IF FR-KEY > 0 
              SUBTRACT 1 FROM FR-KEY.
           IF A-N = "A"
              MOVE 9999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-FIRME.
           PERFORM NEXT-FIRME.
           IF FR-KEY = 0
           OR FR-KEY > END-NUMBER
           OR FR-MATCHCODE > END-MATCHCODE
              GO READ-FIRME-END
           END-IF.
           ACCEPT ACTION TIME 1 LINE 23 POSITION 27 NO BEEP
           ON EXCEPTION ESC-KEY CONTINUE END-ACCEPT.
           IF ESC-KEY = 27 
              EXIT PROGRAM.
           IF FR-FIN-A > 0 AND < LNK-ANNEE
              GO READ-FIRME
           END-IF.
           PERFORM TOTAL-FACTURE THRU TOTAL-FACTURE-END.
           PERFORM DIS-HE-01.
           GO READ-FIRME.
       READ-FIRME-END.
           PERFORM END-PROGRAM.

       NEXT-FIRME.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.


       TOTAL-FACTURE.
           INITIALIZE FI-RECORD.
           MOVE LNK-ANNEE TO FI-DEBUT-A.
           MOVE LNK-MOIS  TO FI-DEBUT-M.
           MOVE FR-KEY TO FI-CLIENT.
           MOVE 1 TO PAGES.
           START FACTURE KEY > FI-KEY-3 INVALID
              PERFORM READ-END
              GO TOTAL-FACTURE-END.
           PERFORM READ-FACTURE THRU READ-END.
       TOTAL-FACTURE-END.
           EXIT.

       READ-FACTURE.
           READ FACTURE NEXT NO LOCK AT END 
              GO READ-END.
           IF FR-KEY    NOT = FI-CLIENT
           OR LNK-ANNEE NOT = FI-DEBUT-A
           OR LNK-MOIS  NOT = FI-DEBUT-M 
              GO READ-END.
           IF FI-EDIT = 0
              MOVE TODAY-DATE TO FI-EDIT
              REWRITE FI-RECORD INVALID CONTINUE
           ELSE
              GO READ-FACTURE
           END-IF.
           MOVE 1 TO PAGES.
           PERFORM LAYOUT.
           PERFORM DET-FACTURE.
           COMPUTE IDX-1 = IMPL-MAX-LINE - 6
           IF LIN-NUM > IDX-1
              PERFORM TRANSMET
              ADD 1 TO PAGES
              PERFORM LAYOUT
           END-IF.
           PERFORM TOTAUX.
           PERFORM TRANSMET.
           GO READ-FACTURE.
       READ-END.
           EXIT.

       DET-FACTURE.
           INITIALIZE FID-RECORD.
           MOVE 0 TO LIN-NUM.
           MOVE FI-NUMBER TO FID-NUMBER.
           START FID KEY > FID-KEY-3 INVALID CONTINUE
              NOT INVALID
           PERFORM READ-FID THRU READ-FID-END.

       READ-FID.
           READ FID NEXT NO LOCK AT END 
              GO READ-FID-END.
           IF FI-NUMBER NOT = FID-NUMBER 
              GO READ-FID-END.
              
           MOVE FID-CODE TO CD-NUMBER.
           CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY.
SU         CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           IF  CS-IMPRESSION(1) = 0 
           AND CS-IMPRESSION(2) = 0 
           AND CS-IMPRESSION(3) = 0 
           AND CS-IMPRESSION(4) = 0 
           AND CS-IMPRESSION(5) = 0 
           AND CS-IMPRESSION(6) = 0 
              GO READ-FID
           END-IF.
           ADD 1 TO LIN-NUM.
           IF LIN-NUM >= 24
              PERFORM TRANSMET
              PERFORM LAYOUT
           END-IF.
           PERFORM EDIT-CS.
           GO READ-FID.
       READ-FID-END.

       EDIT-CS.
           MOVE CD-NUMBER TO HE-Z4.
           MOVE FID-MISSION TO HE-Z4.
           MOVE HE-Z4 TO LINK-ART-A(LIN-NUM).
           MOVE CTX-NOM TO LINK-ART-B(LIN-NUM).
           IF FID-TEXTE NOT = SPACES
              MOVE FID-TEXTE TO LINK-ART-B(LIN-NUM)
           END-IF.
           PERFORM FILL-CS VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.

       FILL-CS.
           IF  CS-IMPRESSION(IDX) > 0 
           AND FID-DONNEE(IDX)    > 0 
              MOVE FID-DONNEE(IDX) TO HE-Z6Z2
              EVALUATE IDX 
              WHEN 1 MOVE 13 TO IDX-1
                     MOVE 22 TO IDX-2
              WHEN 2 MOVE 31 TO IDX-1
                     MOVE 40 TO IDX-2
              WHEN 3 MOVE 49 TO IDX-1
                     MOVE 56 TO IDX-2
              WHEN 4 MOVE 64 TO IDX-1
                     MOVE 73 TO IDX-2
              WHEN 5 MOVE 82 TO IDX-1
                     MOVE 91 TO IDX-2
              WHEN 6 MOVE 100 TO IDX-1
                     MOVE 109 TO IDX-2
              END-EVALUATE
              STRING CTX-DESCRIPTION(IDX) DELIMITED BY "  " INTO 
              LINK-ART-B(LIN-NUM) WITH POINTER IDX-1
              STRING HE-Z6Z2 DELIMITED BY SIZE INTO 
              LINK-ART-B(LIN-NUM) WITH POINTER IDX-2
           END-IF.

       TOTAUX.
           MOVE FI-TOTAL TO HE-Z6Z2
           MOVE HE-Z6Z2 TO LINK-BASE(1).
           MOVE FI-TVA  TO HE-Z6Z2
           MOVE HE-Z6Z2 TO LINK-TVA(1). 
           COMPUTE SH-00 = FI-TOTAL + FI-TVA.
           MOVE SH-00  TO HE-Z6Z2
           MOVE HE-Z6Z2 TO LINK-TTC(1).
           MOVE FI-A-PAYER  TO HE-Z6Z2
           MOVE HE-Z6Z2 TO LINK-A-PAYER.



       LAYOUT.
           MOVE FI-NUMBER TO HE-Z8.
           MOVE HE-Z8 TO LINK-NUM.
           MOVE PAGES TO LINK-PAGE .
           MOVE FR-KEY TO HE-Z8.
           MOVE HE-Z8 TO LINK-NUM-CLIENT.
           MOVE FI-EDIT-A TO HE-AA.
           MOVE FI-EDIT-M TO HE-MM.
           MOVE FI-EDIT-J TO HE-JJ.
           MOVE HE-DATE TO LINK-DATE.

           
           MOVE FR-NOM TO LINK-ADR-01(1).
           MOVE 1 TO IDX-1.
           STRING FR-MAISON DELIMITED BY "  " INTO LINK-ADR-01(2)
           WITH POINTER IDX-1.
           ADD 1 TO IDX-1.
           STRING FR-RUE DELIMITED BY "  " INTO LINK-ADR-01(2)
           WITH POINTER IDX-1.
           MOVE 1 TO IDX-1.
           STRING FR-PAYS DELIMITED BY "  " INTO LINK-ADR-01(3)
           WITH POINTER IDX-1.
           MOVE FR-CODE-POST TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE INTO LINK-ADR-01(3)
           WITH POINTER IDX-1.
           ADD 1 TO IDX-1.
           STRING FR-LOCALITE DELIMITED BY "  " INTO LINK-ADR-01(3)
           WITH POINTER IDX-1.

           IF FI-FREQ-FACT < 2
              PERFORM MOIS-N
           ELSE
              PERFORM PER-N
           END-IF.
           PERFORM TEXTES.

       TEXTES.
           MOVE PARMOD-TX4(1) TO LINK-ENTTX-01.
           MOVE PARMOD-TX4(2) TO LINK-DATE-TX.
           MOVE PARMOD-TX4(3) TO LINK-NUM-TX.
           MOVE PARMOD-TX4(4) TO LINK-PAGE-TX.
           MOVE PARMOD-TX4(5) TO LINK-NUM-CLI-TX.
           MOVE PARMOD-TX4(6) TO LINK-REFER-TX.
           MOVE PARMOD-TX4(7) TO LINK-ECHEANCE-TX.
           MOVE PARMOD-TX4(8) TO LINK-PC-TVA-TX
           MOVE PARMOD-TX4(9) TO LINK-BASE-TX
           MOVE PARMOD-TX4(10) TO LINK-MT-TVA-TX
           MOVE PARMOD-TX4(11) TO LINK-TTC-TX
           MOVE PARMOD-TX4(12) TO LINK-APAYER-TX
           MOVE PARMOD-TX4(13) TO LINK-DESC(1).
           MOVE PARMOD-TX4(14) TO LINK-DESC(2).
           MOVE PARMOD-TX4(15) TO LINK-DESC(3).
           MOVE PARMOD-TX4(16) TO LINK-DESC(4).
           MOVE PARMOD-TX4(17) TO LINK-DESC(5).
           MOVE PARMOD-TX4(18) TO LINK-DESC(6).
           MOVE PARMOD-TX4(19) TO LINK-DESC(7).
           MOVE PARMOD-TX4(20) TO LINK-DESC(8).

       B-C.
           CALL "6-BANQF" USING LINK-V BQF-RECORD NX-KEY.
           IF BQF-FIRME = 0
              GO B-C-END.
           MOVE BQF-CODE TO BQ-CODE.
           CALL "6-BANQUE" USING LINK-V BQ-RECORD FAKE-KEY.
           IF BQ-FICT NOT = 0
              GO B-C.
           ADD 1 TO IDX.
           IF IDX = 6
              GO B-C-END.
           MOVE 5 TO IDX-1.
           STRING BQF-CODE DELIMITED BY "    " INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           MOVE 20 TO IDX-1.
           STRING BQ-SWIFT DELIMITED BY "    " INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           MOVE 40 TO IDX-1.
           STRING BQF-PART(1) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(2) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(3) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(4) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(5) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(6) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           ADD  2 TO IDX-1.
           STRING BQF-PART(7) DELIMITED BY SIZE INTO 
              LINK-REMARQUE(IDX) WITH POINTER IDX-1.
           GO B-C.
       B-C-END.
           EXIT.


       DIS-HE-01.
           MOVE FR-KEY    TO HE-Z6.
           DISPLAY HE-Z6  LINE  6 POSITION 17.
           DISPLAY FR-NOM LINE  6 POSITION 25 SIZE 30.
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 435 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           CALL "FACT" USING LINK-V LINK-RECORD.
           ADD 1 TO COUNTER PAGES.
           MOVE 1 TO LIN-NUM.
           INITIALIZE LINK-RECORD. 


       END-PROGRAM.
           IF COUNTER > 0 
              MOVE 99 TO LNK-VAL
              CALL "FACT" USING LINK-V LINK-RECORD
           END-IF.
           CANCEL "FACT".
           MOVE 0 TO LNK-SUITE.
           MOVE SAVE-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V A-N FAKE-KEY.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXF".
           COPY "XDEC.CPY".
           IF EXC-KEY = 58
              CALL "1-PARTXT" USING LINK-V
              CANCEL "1-PARTXT"
              INITIALIZE PARMOD-RECORD
              MOVE MENU-PROG-NAME TO PARMOD-MODULE
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R"
              PERFORM TEXTES
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              GO AVANT-DEC
           END-IF.
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".

       MOIS-N.
           MOVE FI-DEBUT-M TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE POS-PR TO POS-IDX.
      *    STRING LNK-TEXT DELIMITED BY " " INTO TEXTES 
      *    POINTER POS-IDX ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO POS-IDX.
      *    STRING LNK-ANNEE DELIMITED BY SIZE INTO TEXTES 
      *    POINTER POS-IDX ON OVERFLOW CONTINUE END-STRING.


       PER-N.
           EVALUATE FI-FREQ-FACT 
              WHEN 2 MOVE 118 TO LNK-NUM
              WHEN 3 MOVE 115 TO LNK-NUM
              WHEN 6 MOVE 116 TO LNK-NUM
           END-EVALUATE.
           MOVE "AA" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE POS-PR TO POS-IDX.
      *    STRING LNK-TEXT DELIMITED BY " " INTO TEXTES 
      *    POINTER POS-IDX ON OVERFLOW CONTINUE END-STRING.
           DIVIDE LNK-MOIS BY FI-FREQ-FACT GIVING IDX-1 REMAINDER IDX-2.
           MOVE IDX-1 TO HE-Z2.
           ADD 2 TO POS-IDX.
      *    STRING HE-Z2 DELIMITED BY SIZE INTO TEXTES 
      *    POINTER POS-IDX ON OVERFLOW CONTINUE END-STRING.
           
           ADD 2 TO POS-IDX.
      *    STRING LNK-ANNEE DELIMITED BY SIZE INTO TEXTES 
      *    POINTER POS-IDX ON OVERFLOW CONTINUE END-STRING.

      