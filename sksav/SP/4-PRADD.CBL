      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-PRADD CUMUL  DES PRIMES PAR HORAIRE       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-PRADD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".
           COPY "HEURES.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURS.FDE".
           COPY "HEURES.FDE".

       WORKING-STORAGE SECTION.

           COPY "V-VAR.CPY".

       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.
       01  HRS-NAME.
           02 FILLER             PIC X(9) VALUE "S-HEURES.".
           02 ANNEE-HEURES        PIC 999.

       77  NOT-OPEN     PIC 9 VALUE 0.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".

       PROCEDURE DIVISION USING LINK-V REG-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON HEURES JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-4-PRADD.
       
           IF  NOT-OPEN = 1
           AND LNK-SUFFIX NOT = ANNEE-HEURES 
               CLOSE HEURES
               CLOSE JOURS
               MOVE 0 TO NOT-OPEN.

           IF NOT-OPEN = 0
              MOVE LNK-SUFFIX TO  ANNEE-HEURES ANNEE-JOURS
              OPEN INPUT HEURES
              OPEN I-O   JOURS
              MOVE 1 TO NOT-OPEN 
           END-IF.
           PERFORM CLEAN-OLD-TOTAL.

           PERFORM CUMUL-JRS THRU CUMUL-END.
           EXIT PROGRAM.

*****************************************************************

       CLEAN-OLD-TOTAL.
           PERFORM JRS-KEYS.
           START JOURS KEY >= JRS-KEY INVALID CONTINUE
           NOT INVALID
                PERFORM DELETE-JOURS THRU DELETE-JRS-END.

       DELETE-JOURS.
           READ JOURS NEXT AT END
               GO DELETE-JRS-END
           END-READ.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
              GO DELETE-JRS-END
           END-IF.
           IF JRS-COMPLEMENT < 21
              GO DELETE-JOURS.
           IF LNK-SQL = "Y" 
              CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY 
           END-IF
           DELETE JOURS INVALID CONTINUE.
           GO DELETE-JOURS.
       DELETE-JRS-END.
           PERFORM JRS-KEYS.

       JRS-KEYS.
           INITIALIZE JRS-RECORD.
           MOVE FR-KEY TO JRS-FIRME  JRS-FIRME-1 JRS-FIRME-2 JRS-FIRME-3.
           MOVE REG-PERSON TO JRS-PERSON JRS-PERSON-1 JRS-PERSON-2 
                              JRS-PERSON-3.
           MOVE LNK-MOIS TO JRS-MOIS JRS-MOIS-1 JRS-MOIS-2 JRS-MOIS-3.

*****************************************************************

       CUMUL-JRS.
           INITIALIZE HRS-RECORD.
           MOVE FR-KEY     TO HRS-FIRME.
           MOVE REG-PERSON TO HRS-PERSON.
           MOVE LNK-MOIS   TO HRS-MOIS.
           START HEURES KEY > HRS-KEY INVALID GO CUMUL-END.
       CUMUL-1.
           READ HEURES NEXT AT END
                GO CUMUL-END
           END-READ.
           IF FR-KEY NOT = HRS-FIRME
           OR REG-PERSON NOT = HRS-PERSON
           OR LNK-MOIS  NOT = HRS-MOIS 
              GO CUMUL-END
           END-IF.
           IF HRS-JOUR = 0
              GO CUMUL-1.
           PERFORM WRITE-JRS-TARIF THRU WRITE-JRS-TARIF-END
           VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 10.
           GO CUMUL-1.
       CUMUL-END.
           EXIT.

       A-H.
           IF JRS-HRS(IDX) > 0
              ADD 1 TO JRS-JOURS.

       WRITE-JRS-TARIF.
           IF HRS-UNITE(IDX-1) = 0
              GO WRITE-JRS-TARIF-END.
           COMPUTE IDX-4 = IDX-1 + 20.
           MOVE IDX-4 TO JRS-COMPLEMENT   JRS-COMPLEMENT-1 
                         JRS-COMPLEMENT-2 JRS-COMPLEMENT-3.
           READ JOURS INVALID INITIALIZE JRS-REC-DET END-READ.
           ADD HRS-UNITE(IDX-1) TO JRS-HRS(HRS-JOUR) JRS-HRS(32).
           CALL "0-TODAY" USING TODAY.
           MOVE TODAY-TIME TO JRS-TIME.
           MOVE LNK-USER TO JRS-USER.
           IF LNK-SQL = "Y" 
              CALL "9-JOURS" USING LINK-V JRS-RECORD WR-KEY 
           END-IF.
           WRITE JRS-RECORD INVALID REWRITE JRS-RECORD.
       WRITE-JRS-TARIF-END.
           EXIT.

           