      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-CTF RECHERCHE CONTACTS FIRME              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-CTF.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CONTACT.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.

       01  CHOIX                 PIC 9(8).
       01  CHOIX-A               PIC X(50).
       01  CHOIX-A1 REDEFINES CHOIX-A.
           02 CHOIX-1            PIC 9.
           02 CHOIX-F1           PIC X(9).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A2 REDEFINES CHOIX-A.
           02 CHOIX-2            PIC 9(2).
           02 CHOIX-F2           PIC X(8).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A3 REDEFINES CHOIX-A.
           02 CHOIX-3            PIC 9(3).
           02 CHOIX-F3           PIC X(7).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A4 REDEFINES CHOIX-A.
           02 CHOIX-4            PIC 9(4).
           02 CHOIX-F4           PIC X(6).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A5 REDEFINES CHOIX-A.
           02 CHOIX-5            PIC 9(5).
           02 CHOIX-F5           PIC X(5).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A6 REDEFINES CHOIX-A.
           02 CHOIX-6            PIC 9(6).
           02 CHOIX-F6           PIC X(4).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A7 REDEFINES CHOIX-A.
           02 CHOIX-7            PIC 9(7).
           02 CHOIX-F7           PIC X(3).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A8 REDEFINES CHOIX-A.
           02 CHOIX-8            PIC 9(8).
           02 CHOIX-F8           PIC X(2).
           02 CHOIX-FX           PIC X(40).
       01  COMPTEUR              PIC 99.

       01 HE-REG.
          02 H-R PIC 9(8) OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z8 PIC Z(8).
           02 HE-TEMP.
              03 HE-JJ    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-MM    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-AA    PIC ZZZZ.

      * crit둹es 

       01  CRIT-FLAG             PIC 9 VALUE 0.
       01  CRITERES-1            PIC X(50).
       01  HLP-CRIT REDEFINES CRITERES-1.
           03 CRIT               PIC X OCCURS 50.

       01  TST                  PIC X(4).

       01  ITEMS.
           02 ITEM-A OCCURS 10.
              03 ITEM-IDX        PIC 99.
              03 ITEM            PIC X(10).
              03 ITEM-A REDEFINES ITEM.
                 04 ITEM-2       PIC X(2).
                 04 ITEM-F2      PIC X(8).
              03 ITEM-B REDEFINES ITEM.
                 04 ITEM-3       PIC X(3).
                 04 ITEM-F3      PIC X(7).
              03 ITEM-C REDEFINES ITEM.
                 04 ITEM-4       PIC X(4).
                 04 ITEM-F4      PIC X(6).
              03 ITEM-D REDEFINES ITEM.
                 04 ITEM-5       PIC X(5).
                 04 ITEM-F5      PIC X(5).
              03 ITEM-E REDEFINES ITEM.
                 04 ITEM-6       PIC X(6).
                 04 ITEM-F6      PIC X(4).
              03 ITEM-F REDEFINES ITEM.
                 04 ITEM-7       PIC X(7).
                 04 ITEM-F7      PIC X(3).
              03 ITEM-G REDEFINES ITEM.
                 04 ITEM-8       PIC X(8).
                 04 ITEM-F8      PIC X(2).
              03 ITEM-H REDEFINES ITEM.
                 04 ITEM-9       PIC X(9).
                 04 ITEM-F9      PIC X(1).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "CONTACT.LNK".
           COPY "POCL.REC".

       PROCEDURE DIVISION USING LINK-V  LINK-RECORD.

       START-DISPLAY SECTION.
             
       START-2-CTF.

           MOVE LNK-A-N TO A-N.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE CTC-RECORD IDX-1 EXC-KEY COMPTEUR CHOIX.
           MOVE 4 TO LIN-IDX.
           PERFORM READ-CL THRU READ-CL-END.
           IF EXC-KEY = 66 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-CL.
           MOVE 66 TO EXC-KEY.
           CALL "6-CTF" USING LINK-V  CTC-RECORD A-N EXC-KEY.
           IF CTC-PERSON = 0
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-REG IDX-1
                 GO READ-CL
              END-IF
              IF CHOIX NOT = 0
                 GO READ-CL-END
              END-IF
              GO READ-CL
           END-IF.
           IF CTC-ACTIF = "N"
              GO READ-CL
           END-IF.
           IF CRIT-FLAG = 1
              PERFORM RECH-CRIT
              IF INPUT-ERROR = 1
                 GO READ-CL
              END-IF
           END-IF.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 INITIALIZE HE-REG IDX-1
                 GO READ-CL
              END-IF
              IF CHOIX NOT = 0
                 GO READ-CL-END
              END-IF
           END-IF.
           GO READ-CL.
       READ-CL-END.
           IF CHOIX = 0
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82
              AND NOT = 68
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX COMPTEUR IDX-1.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           MOVE CTC-PERSON TO HE-Z8 H-R(IDX-1).
           DISPLAY HE-Z8  LINE LIN-IDX POSITION 1.
           DISPLAY CTC-NOM LINE LIN-IDX POSITION 10.
           DISPLAY CTC-PRENOM LINE LIN-IDX POSITION 30 SIZE 15.
           DISPLAY CTC-PHONE LINE LIN-IDX POSITION 50.
           DISPLAY CTC-ACTIF LINE LIN-IDX POSITION 75.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 4400000000 TO EXC-KFR(1).
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0000530000 TO EXC-KFR(11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE CHOIX CHOIX-A.
           IF A-N = "N"
              ACCEPT CHOIX 
              LINE 3 POSITION 15 SIZE 8
              TAB UPDATE NO BEEP CURSOR  1
              ON EXCEPTION EXC-KEY CONTINUE
           ELSE
              ACCEPT CHOIX-A
              LINE 3 POSITION 10 SIZE 40
              TAB UPDATE NO BEEP CURSOR  1
              CONTROL "UPPER"
              ON EXCEPTION EXC-KEY CONTINUE
              END-ACCEPT
              IF CHOIX-A NOT = SPACES
                 MOVE FR-KEY TO CTC-FIRME CTC-FIRME-A
                 MOVE CHOIX-A TO CTC-MATCHCODE
              END-IF
              MOVE 0 TO CHOIX IDX-1
           END-IF.
           MOVE 0 TO IDX CRIT-FLAG.
           INSPECT CHOIX-A TALLYING IDX FOR CHARACTERS BEFORE "?".
           IF IDX < 50 
           OR EXC-KEY = 1
              MOVE 1 TO CRIT-FLAG
              PERFORM HOMOGENIZE
              MOVE 4 TO LIN-IDX
           ELSE
              MOVE 0 TO CHOIX IDX-1 CRIT-FLAG
           END-IF.
           IF CRIT-FLAG = 0
           IF CHOIX-1 NUMERIC
              INSPECT CHOIX-A TALLYING IDX-1 FOR CHARACTERS BEFORE " "
              EVALUATE IDX-1
                 WHEN 1 MOVE CHOIX-1 TO CHOIX
                 WHEN 2 MOVE CHOIX-2 TO CHOIX
                 WHEN 3 MOVE CHOIX-3 TO CHOIX
                 WHEN 4 MOVE CHOIX-4 TO CHOIX
                 WHEN 5 MOVE CHOIX-5 TO CHOIX
                 WHEN 6 MOVE CHOIX-6 TO CHOIX
                 WHEN 7 MOVE CHOIX-7 TO CHOIX
                 WHEN 8 MOVE CHOIX-8 TO CHOIX
              END-EVALUATE
              IF CHOIX NOT = 0
                 PERFORM END-PROGRAM
              END-IF
           END-IF.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF A-N = "N"
              IF CHOIX NOT = 0
                 PERFORM END-PROGRAM
              END-IF
           ELSE
              IF CHOIX-A > SPACES
                 INITIALIZE CTC-RECORD
                 IF CRIT-FLAG = 0
                    MOVE CHOIX-A TO CTC-MATCHCODE
                 END-IF
                 MOVE 66 TO EXC-KEY
              END-IF
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IDX-1.

       AFFICHAGE-ECRAN.
           MOVE 2141 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       END-PROGRAM.
           MOVE CHOIX TO CTC-PERSON.
           CALL "6-CTF" USING LINK-V  CTC-RECORD "N" FAKE-KEY.
           IF CTC-PERSON NOT = 0
              MOVE CTC-RECORD TO LINK-RECORD.
              
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IDX-1 + 4.
           MOVE H-R(IDX-1) TO HE-Z8.
           ACCEPT HE-Z8
             LINE  LIN-IDX POSITION 1 SIZE 8
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           MOVE H-R(IDX-1) TO HE-Z8.
           DISPLAY HE-Z8 LINE LIN-IDX POSITION 1.
           IF EXC-KEY = 54
              GO AVANT-ALL-END.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN OTHER
                     MOVE H-R(IDX-1) TO CHOIX
                     PERFORM END-PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-R(IDX-1) = 0
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.

       RECH-CRIT.
           INSPECT CTC-RECORD CONVERTING
           "abcdefghijklmnopqrstuvwxyz릠뒋뎲땶뱮걭뾼솞�;:" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZEEEEEAIIOOUUUAOUA  ".
           MOVE 0 TO INPUT-ERROR IDX-3.
           PERFORM TEST-CRIT THRU TEST-CRIT-END.

        TEST-CRIT.
           ADD 1 TO IDX-3.
           IF ITEM-IDX(IDX-3) < 2
              GO TEST-CRIT-END
           END-IF.
           MOVE 0 TO IDX.
           EVALUATE ITEM-IDX(IDX-3)
              WHEN 2 INSPECT CTC-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-2(IDX-3)
              WHEN 3 INSPECT CTC-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-3(IDX-3)
              WHEN 4 INSPECT CTC-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-4(IDX-3)
              WHEN 5 INSPECT CTC-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-5(IDX-3)
              WHEN 6 INSPECT CTC-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-6(IDX-3)
              WHEN 7 INSPECT CTC-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-7(IDX-3)
              WHEN 8 INSPECT CTC-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-8(IDX-3)
              WHEN 9 INSPECT CTC-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-9(IDX-3)
              WHEN 10 INSPECT CTC-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM(IDX-3)
           END-EVALUATE.
           IF IDX > 2580
              MOVE 1 TO INPUT-ERROR
              GO TEST-CRIT-END
           END-IF.
           IF IDX-3 < 10
              GO TEST-CRIT.
        TEST-CRIT-END.
           EXIT.
                        
       HOMOGENIZE.
           MOVE CHOIX-A TO CRITERES-1.
           INSPECT CRITERES-1 CONVERTING
           "abcdefghijklmnopqrstuvwxyz릠뒋뎲땶뱮걭뾼솞�;:?" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZEEEEEAIIOOUUUAOUA   ".
           MOVE 1 TO IDX-1 IDX-2 IDX.
           PERFORM PROPER-E THRU PROPER-END.

        PROPER-E.
           IF CRIT(IDX) > SPACES 
           AND IDX-1 < 11
              STRING CRIT(IDX) DELIMITED BY SIZE INTO ITEM(IDX-2) WITH 
              POINTER IDX-1 ON OVERFLOW CONTINUE
           ELSE
              IF IDX-1 > 2
                 ADD 1 TO IDX-2
               END-IF
               MOVE 1 TO IDX-1
           END-IF.
           COMPUTE ITEM-IDX(IDX-2) = IDX-1 - 1.
           ADD 1 TO IDX.
           IF IDX < 51
           AND IDX-2 < 11
              GO PROPER-E.
        PROPER-END.
           EXIT.
