      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-HPJ MODULE GENERAL LECTURE PLAN DE TRAVAIL�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-HPJ.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "HOPJOUR.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "HOPJOUR.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "HOPJOUR.LNK".
           COPY "REGISTRE.REC".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD REG-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON HOPJOUR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-REG.
       
           IF NOT-OPEN = 0
              OPEN I-O HOPJOUR
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO HPJ-RECORD.
           MOVE FR-KEY TO HPJ-FIRME
              HPJ-FIRME-A HPJ-FIRME-B HPJ-FIRME-C HPJ-FIRME-D.
           MOVE REG-PERSON  TO HPJ-PERSON
              HPJ-PERSON-A HPJ-PERSON-B HPJ-PERSON-C HPJ-PERSON-D.  
           MOVE HPJ-POSTE TO 
              HPJ-POSTE-A HPJ-POSTE-B HPJ-POSTE-C HPJ-POSTE-D.  
           MOVE HPJ-DATE TO 
              HPJ-DATE-A HPJ-DATE-B HPJ-DATE-C HPJ-DATE-D.  
           MOVE HPJ-DEBUT(1) TO HPJ-H-A HPJ-H-B HPJ-H-C HPJ-H-D.  

           IF EXC-KEY = 98
              DELETE HOPJOUR INVALID CONTINUE
              EXIT PROGRAM
           END-IF.                   
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ HOPJOUR PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ HOPJOUR NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 0 PERFORM START-3
               READ HOPJOUR PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ HOPJOUR NO LOCK INVALID INITIALIZE 
               HPJ-REC-DET END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY     NOT = HPJ-FIRME 
           OR REG-PERSON NOT = HPJ-PERSON
              GO EXIT-1
           END-IF.
           MOVE HPJ-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START HOPJOUR KEY < HPJ-KEY-D INVALID GO EXIT-1.
       START-2.
           START HOPJOUR KEY > HPJ-KEY-D INVALID GO EXIT-1.
       START-3.
           START HOPJOUR KEY <= HPJ-KEY-D INVALID GO EXIT-1.


