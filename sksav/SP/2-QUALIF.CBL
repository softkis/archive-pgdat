      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-QUALIF RECHERCHE QUALIFS                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-QUALIF.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "QUALIF.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  ARROW                 PIC X VALUE ">".

       01  CHOIX                 PIC 9(4).
       01  COMPTEUR              PIC 99.
       01  SAVE-AREA             PIC XXXX.

       01 HE-MS.
          02 H-R PIC X(30) OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-10 PIC Z(10).
           02 HE-TEMP.
              03 HE-JJ    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-MM    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-AA    PIC ZZZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "QUALIF.LNK".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       START-DISPLAY SECTION.
             
       START-2-QUALIF.

           PERFORM AFFICHAGE-ECRAN .
           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE Q-RECORD IDX-1 EXC-KEY COMPTEUR CHOIX HE-MS.
           MOVE  4 TO LIN-IDX.
           PERFORM READ-MS THRU READ-MS-END.
           IF EXC-KEY = 9 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-MS.
           CALL "6-QUALIF" USING LINK-V Q-RECORD NX-KEY.
           IF Q-NOM = SPACES
           AND EXC-KEY = 13
              MOVE 66 TO EXC-KEY
              GO READ-MS
           END-IF.
           MOVE 66 TO EXC-KEY.
           IF Q-NOM = SPACES
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 INITIALIZE HE-MS IDX-1
                 GO READ-MS
              END-IF
              GO READ-MS-END.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 INITIALIZE HE-MS IDX-1
                 GO READ-MS
              END-IF
              IF CHOIX NOT = 0
                 GO READ-MS-END
              END-IF
              INITIALIZE HE-MS IDX-1
           END-IF.
           GO READ-MS.
       READ-MS-END.
           IF CHOIX = 0
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82 AND NOT = 68
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX COMPTEUR IDX-1.
           MOVE Q-KEY TO H-R(IDX-1).
           MOVE Q-CODE TO HE-10.
           IF Q-CODE > 0
              DISPLAY HE-10 LINE LIN-IDX POSITION 3
           ELSE
              DISPLAY Q-ALPHA LINE LIN-IDX POSITION 3.
           DISPLAY Q-NOM LINE LIN-IDX POSITION 20.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0000530000 TO EXC-KFR (11).
           MOVE 6667000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE CHOIX.
           ACCEPT CHOIX 
           LINE 3 POSITION 30 SIZE 4
           TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF CHOIX NOT = 0
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IDX-1.

       AFFICHAGE-ECRAN.
           MOVE 2004 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       END-PROGRAM.
           IF CHOIX > 0
              MOVE H-R(CHOIX) TO Q-KEY
              CALL "6-QUALIF" USING LINK-V Q-RECORD FAKE-KEY.
           IF Q-NOM NOT = SPACES
              MOVE Q-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IDX-1 + 4.
           MOVE ">" TO ARROW.
           ACCEPT ARROW
             LINE  LIN-IDX POSITION 1 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "REVERSE"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 1.
           IF EXC-KEY = 82 
              MOVE IDX-1 TO CHOIX
              PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN OTHER
                     MOVE IDX-1 TO CHOIX
                     PERFORM END-PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-R(IDX-1) = SPACES
              SUBTRACT 1 FROM IDX-1
           END-IF.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.
