      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-FAL LECTURE ARTICLES FACTURATION          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-FAL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FACTLINE.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FACTLINE.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  INPUT-ERROR PIC 9 VALUE 0.
       01  IDX-1       PIC 9 VALUE 0.
       01  IDX                   PIC 9(4)  VALUE 0.
       01  SH-00  PIC S9(10)V999999 COMP-3.
       01  action      PIC 9.
       01  TODAY.
           02 TODAY-TIME     PIC 9(12).
           02 TODAY-FILLER   PIC 9999.

       01  FAL-NAME.
           02 IDENTITE           PIC X(4) VALUE "A-FD".
           02 FIRME-FAL          PIC 9999 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".
           COPY "FACTLINE.LNK".
           COPY "POCL.REC".
       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD PC-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FAL.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-FAL.


           IF  FIRME-FAL > 0
           AND FIRME-FAL NOT = FR-KEY
              CLOSE FAL
              MOVE 0 TO FIRME-FAL
           END-IF.

           IF FIRME-FAL = 0
              MOVE FR-KEY TO FIRME-FAL
              OPEN I-O FAL
           END-IF.

           MOVE LINK-RECORD TO FAL-RECORD.
           MOVE PC-NUMBER   TO FAL-CLIENT.
           MOVE LNK-ANNEE   TO FAL-ANNEE.
           IF FAL-MOIS = 0
              MOVE LNK-MOIS TO FAL-MOIS.
           PERFORM RECHERCHE.

       RECHERCHE.
           EVALUATE EXC-KEY 
           WHEN 65 START FAL KEY < FAL-KEY INVALID GO EXIT-1
               NOT INVALID
               READ FAL PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               END-START
           WHEN 66 START FAL KEY > FAL-KEY INVALID GO EXIT-1
               NOT INVALID
               READ FAL NEXT NO LOCK AT END GO EXIT-1 END-READ
               END-START
           WHEN 0 START FAL KEY <= FAL-KEY INVALID GO EXIT-1
               NOT INVALID
               READ FAL PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               END-START
           WHEN 98 DELETE FAL INVALID CONTINUE END-DELETE
           WHEN 99 MOVE 0 TO SH-00
                   PERFORM TEST-0 VARYING IDX FROM 1 BY 1 UNTIL IDX > 6
                      IF SH-00 > 0
                      CALL "0-TODAY" USING TODAY
                      MOVE TODAY-TIME TO FAL-TIME
                      MOVE LNK-USER TO FAL-USER
                      WRITE FAL-RECORD INVALID REWRITE FAL-RECORD 
                      END-WRITE
                   ELSE
                      DELETE FAL INVALID CONTINUE END-DELETE
                   END-IF
           WHEN OTHER READ FAL NO LOCK INVALID GO EXIT-2 END-READ
           END-EVALUATE.
           IF PC-NUMBER NOT = FAL-CLIENT
           OR LNK-ANNEE  NOT = FAL-ANNEE
           OR LNK-MOIS   NOT = FAL-MOIS
              GO EXIT-1
           END-IF.
           MOVE FAL-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           INITIALIZE LINK-REC-DET.
           EXIT PROGRAM.

       TEST-0.
           ADD FAL-DONNEE(IDX) TO SH-00.