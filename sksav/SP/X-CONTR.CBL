      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME X-CONTR DATE FIN CONTRAT GENERAL            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    X-CONTR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL .
           COPY "CONTRAT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CONTRAT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 3.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "MOTDEP.REC".
           COPY "REGISTRE.REC".
           COPY "CONGE.REC".

       01  DATE-FIN              PIC 9(8) VALUE 0.
       01  DATE-FIN-R REDEFINES DATE-FIN.
            04 FIN-A       PIC 9999.
            04 FIN-M       PIC 99.
            04 FIN-J       PIC 99.
       01  MOTIF-INTERNE         PIC 99 VALUE 0.
       01  COMPTEUR              PIC 9999 VALUE 0.
       01  NOT-OPEN              PIC 9999 VALUE 0.
       01  DEL-FLAG              PIC 9 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z2Z2 PIC ZZ,ZZ. 
           02 HE-Z6Z2 PIC Z(6),ZZ. 
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CONTRAT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           perform aa.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-X-CONTR.
       
           PERFORM AFFICHAGE-ECRAN .

           OPEN I-O CONTRAT.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0029000015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600680000 TO EXC-KFR(14)
           WHEN 3     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0000000025 TO EXC-KFR(3).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1
           WHEN  2 PERFORM AVANT-2
           WHEN  3 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 15 MOVE 5  TO EXC-KEY DEL-FLAG
           END-EVALUATE.

           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2
           WHEN  3 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           MOVE 05320000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-FIN
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.

       AVANT-2.
           ACCEPT MOTIF-INTERNE
             LINE 10 POSITION 32 SIZE 2 
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           IF DATE-FIN = 0
              MOVE 1 TO INPUT-ERROR
              MOVE 10 TO LNK-NUM
              MOVE "AA" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
           END-IF.

       APRES-2.
           MOVE MOTIF-INTERNE TO MD-CODE.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-MOTDEP" USING LINK-V MD-RECORD
                    MOVE MD-CODE TO CON-MOTIF-INTERNE
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN  65 THRU 66 PERFORM NEXT-MOTIF
           END-EVALUATE.
           MOVE MD-CODE TO MOTIF-INTERNE.
           PERFORM DIS-02.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT.
           INITIALIZE CON-RECORD.
           MOVE FR-KEY TO CON-FIRME CON-FIRME-F.
           START CONTRAT KEY >= CON-KEY INVALID EXIT PROGRAM
              NOT INVALID
              PERFORM C-C THRU C-C-END.
       C-C.
           READ CONTRAT NEXT NO LOCK AT END 
             GO C-C-END.
           IF FR-KEY NOT = CON-FIRME 
             GO C-C-END.
           IF  CON-DATE-FIN NOT = 0
           AND CON-DATE-FIN < DATE-FIN
              GO C-C.
           IF CON-DATE-DEBUT > DATE-FIN
              GO C-C.
           MOVE DATE-FIN TO CON-DATE-FIN.
           MOVE 1 TO CON-MOTIF-DEPART.
           MOVE MOTIF-INTERNE TO CON-MOTIF-INTERNE.
           WRITE CON-RECORD INVALID REWRITE CON-RECORD END-WRITE.
           MOVE CON-PERSON TO REG-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           CALL "4-CGINI" USING LINK-V REG-RECORD CONGE-RECORD.
           DISPLAY CON-KEY LINE 20 POSITION 11.
      *     PERFORM AA.

           GO C-C.
       C-C-END.
           EXIT PROGRAM.
       NEXT-MOTIF.
           CALL "6-MOTDEP" USING LINK-V MD-RECORD EXC-KEY.

       AFFICHAGE-ECRAN.
           MOVE 32 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY "Date fin global" LINE 1 POSITION 3 LOW.
           DISPLAY "DATE FIN" line 5 position 5 low.
           DISPLAY "MOTIF-INTERNE" line 10 position 5 low.


       AFFICHAGE-DETAIL.
           PERFORM DIS-01 THRU DIS-02.

       DIS-01.
           MOVE FIN-A TO HE-AA.
           MOVE FIN-M TO HE-MM.
           MOVE FIN-J TO HE-JJ.
           DISPLAY HE-DATE LINE 5 POSITION 32.

       DIS-02. 
           MOVE MOTIF-INTERNE TO HE-Z2 MD-CODE.
           DISPLAY HE-Z2 LINE 10 POSITION 32.
           INITIALIZE MD-REC-DET.
           CALL "6-MOTDEP" USING LINK-V MD-RECORD FAKE-KEY.
           DISPLAY MD-NOM LINE 10 POSITION 35 SIZE 40.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


