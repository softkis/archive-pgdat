      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-PTE  SAISIE    PLANS PERSONNES PAR EQUIPE �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-PTE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PLANS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "PLANS.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "EQUIPE.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "PLAGE.REC".

       01  CALENDRIER.
          02 CAL-RECORD OCCURS 21.
          03 CAL-KEY.
             04 CAL-FIRME          PIC 9(6).
             04 CAL-MOIS           PIC 99.
             04 CAL-PERSON         PIC 9(8).

          03 CAL-KEY-1.
             04 CAL-FIRME-1        PIC 9(6).
             04 CAL-PERSON-1       PIC 9(8).
             04 CAL-MOIS-1         PIC 99.
              
          03 CAL-REC-DET.
             04 CAL-HEURES OCCURS 31.
                05 CAL-PLAGE     PIC X(12).
                05 CAL-HORAIRE.
                   06 CAL-TEMPS OCCURS 2.
                      07 CAL-DEBUT       PIC 9999.
                      07 CAL-DEB  REDEFINES CAL-DEBUT.
                         08 CAL-HR-D     PIC 99.
                         08 CAL-MIN-D    PIC 99.
                      07 CAL-FIN         PIC 9999.
                      07 CAL-FINR REDEFINES CAL-FIN.
                         08 CAL-HR-F     PIC 99.
                         08 CAL-MIN-F    PIC 99.
                      07 CAL-REPOS       PIC 9999.
                      07 CAL-REPR REDEFINES CAL-REPOS.
                         08 CAL-HR-R     PIC 99.
                         08 CAL-MIN-R    PIC 99.
                      07 CAL-BRUT        PIC 99V99.
                      07 CAL-NET         PIC 99V99.
                05 CAL-VAL               PIC 99V99.
                05 CAL-COMMENT           PIC X(80).
             04 CAL-TOTAL  PIC 999V99.
             04 CAL-JOURS  PIC 99.
             04 CAL-FILLER PIC X(100).
             04 CAL-STAMP.
                05 CAL-TIME.
                   06 CAL-ST-ANNEE PIC 9999.
                   06 CAL-ST-MOIS  PIC 99.
                   06 CAL-ST-JOUR  PIC 99.
                   06 CAL-ST-HEURE PIC 99.
                   06 CAL-ST-MIN   PIC 99.
                05 CAL-USER        PIC X(10).

       01  TAB-PRESENCES.
           02 T-P OCCURS 21.
              03 TP-ANNEE     PIC 999.
              03 TP-DATE-FIN  PIC 9(8).
              03 TP-MOIS OCCURS 12.
                 04 TP-TOT PIC 99.
                 04 TP-DIM PIC 9.
                 04 TP-JOUR PIC 9 OCCURS 31.

       01  CHOIX-MAX             PIC 99 VALUE 13. 
       01  JOUR-IDX              PIC 9.
       01  HELP-VAL              PIC XX.
       01  COPIE                 PIC XX VALUE SPACES.
       01  HELP-1                PIC 9999.
       01  HELP-2                PIC 99 VALUE 0.
       
       01   ECR-DISPLAY.
            02 HE-01 PIC zz.
            02 HE-Z4 PIC ZZZZ.

       01  JOUR             PIC 99.
       01  LIGNE             PIC 99.

       01  PLANS-NAME.
           02 FILLER             PIC X(8) VALUE "S-PLANS.".
           02 ANNEE-PLANS        PIC 999.
 
       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PLANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-1-PTE.
       
           MOVE LNK-SUFFIX TO ANNEE-PLANS.
           OPEN I-O PLANS.

           PERFORM AFFICHAGE-ECRAN .
           MOVE 0 TO INDICE-ZONE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 0 MOVE 0 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0012130000 TO EXC-KFR(3).
           MOVE 0027000000 TO EXC-KFR(6).
           MOVE 0052535400 TO EXC-KFR(11).
           MOVE 0000000065 TO EXC-KFR(13).
           MOVE 6600000000 TO EXC-KFR(14).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 0     MOVE 0064000000 TO EXC-KFR(1)
           WHEN OTHER MOVE 0022630005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  0 PERFORM AVANT-0
           WHEN OTHER PERFORM AVANT-CAL.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  0 PERFORM APRES-0.
           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-0.
           ACCEPT EQ-NUMBER
             LINE  1 POSITION 10 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-CAL.
           MOVE INDICE-ZONE TO LIGNE.
           IF JOUR < 1
              MOVE 1 TO JOUR
           END-IF.
           IF JOUR > MOIS-JRS(LNK-MOIS)
              MOVE MOIS-JRS(LNK-MOIS) TO JOUR
           END-IF.
           COMPUTE LIN-IDX = 3 + LIGNE.
           COMPUTE COL-IDX = 16 + (JOUR * 2).
           MOVE CAL-PLAGE(LIGNE, JOUR) TO HELP-VAL.
           ACCEPT HELP-VAL LINE LIN-IDX POSITION COL-IDX SIZE 2
             TAB UPDATE NO BEEP
             CONTROL "UPPER"
             ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 2
              IF COPIE NOT = SPACES
                 MOVE COPIE TO HELP-VAL 
              END-IF
              MOVE 13 TO EXC-KEY 
           END-IF.
           IF EXC-KEY = 8
              MOVE SPACES TO HELP-VAL 
              MOVE 13 TO EXC-KEY 
           END-IF.
           MOVE 0 TO SAVE-KEY.
           IF HELP-VAL NOT = SPACES
              PERFORM READ-PLAGE
           ELSE 
              INITIALIZE CAL-PLAGE(LIGNE, JOUR) CAL-VAL(LIGNE, JOUR) 
           END-IF.

           DISPLAY HELP-VAL LINE LIN-IDX POSITION COL-IDX.
           IF TP-JOUR(LIGNE, LNK-MOIS, JOUR) = 0
              DISPLAY "--" LINE LIN-IDX POSITION COL-IDX LOW
              MOVE SPACES TO HELP-VAL
           END-IF.
           MOVE HELP-VAL TO CAL-PLAGE(LIGNE, JOUR) .
           PERFORM DIS-AP.
           EVALUATE EXC-KEY
              WHEN 3 CALL "2-PLAGE" USING LINK-V PLG-RECORD
                     MOVE PLG-KEY TO CAL-PLAGE(LIGNE, JOUR)
                     MOVE JOUR  TO IDX-1
                     MOVE LIGNE TO IDX-2
                     PERFORM AFFICHAGE-ECRAN 
                     PERFORM AFFICHAGE-DETAIL
                     MOVE CHOIX-MAX TO HELP-2
                     PERFORM AFFICHE-DEBUT 
                     MOVE HELP-2 TO CHOIX-MAX 
                     PERFORM AFFICHE-PLAN
                     PERFORM DISPLAY-F-KEYS
                     MOVE IDX-1 TO JOUR 
                     MOVE IDX-2 TO LIGNE
                     MOVE  3 TO EXC-KEY
               WHEN 52 SUBTRACT 1 FROM INDICE-ZONE
               WHEN 53 ADD 1 TO INDICE-ZONE
               WHEN 27 SUBTRACT 1 FROM JOUR
                       IF JOUR = 0
                          SUBTRACT 1 FROM INDICE-ZONE
                          MOVE MOIS-JRS(LNK-MOIS) TO JOUR
                       END-IF
               WHEN 5  PERFORM TESTS
                       MOVE 0 TO INDICE-ZONE
               WHEN 54 MOVE 0 TO INDICE-ZONE
               WHEN 82 PERFORM END-PROGRAM
               WHEN 13 ADD 1 TO JOUR
                       IF JOUR > MOIS-JRS(LNK-MOIS)
                          ADD 1 TO INDICE-ZONE
                          MOVE 1 TO JOUR
                       END-IF
               WHEN 65 THRU 66
                       MOVE EXC-KEY TO SAVE-KEY
                       PERFORM READ-PLAGE
                       MOVE HELP-VAL TO CAL-PLAGE(LIGNE, JOUR)
               WHEN OTHER GO AVANT-CAL
           END-EVALUATE.
           IF HELP-VAL NOT = SPACES
              MOVE HELP-VAL TO COPIE
           END-IF.
           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.
           IF INDICE-ZONE > 0 
              GO AVANT-CAL.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-0.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-EQUIPE" USING LINK-V EQ-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-EQUIPE
           END-EVALUATE.
           IF EQ-NUMBER NOT = 0
               INITIALIZE CALENDRIER
               PERFORM AFFICHAGE-DETAIL
               MOVE 0 TO HELP-2
               PERFORM AFFICHE-DEBUT.
           MOVE 1 TO JOUR.

       AFFICHE-DEBUT.
           MOVE 3 TO LIN-IDX.
           INITIALIZE REG-RECORD CHOIX-MAX PT-RECORD.
           PERFORM READ-EQ THRU READ-EQ-END.

       READ-EQ.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY.
           IF REG-PERSON > 0
              CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES
           ELSE
              GO READ-EQ-END.
           IF PRES-TOT(LNK-MOIS) = 0
              GO READ-EQ.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF CAR-IN-ACTIF = 1
              GO READ-EQ.
      *    IF CAR-EQUIPE NOT = EQ-NUMBER
      *       GO READ-EQ.
           IF HELP-2 = 0
              PERFORM PLAN.
           IF CHOIX-MAX > 20
              GO READ-EQ-END.
           GO READ-EQ.
       READ-EQ-END.
           PERFORM AFFICHE-PLAN.
           PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 24.

       PLAN.
           INITIALIZE PT-RECORD IDX-4.
           MOVE LNK-MOIS TO PT-MOIS PT-MOIS-1.
           CALL "6-PLANS" USING LINK-V REG-RECORD PT-RECORD FAKE-KEY.
           PERFORM TEST-EQ VARYING JOUR FROM 1 BY 1 UNTIL JOUR > 
           MOIS-JRS(LNK-MOIS).
           IF IDX-4 = 1
              MOVE PRESENCES TO T-P(CHOIX-MAX)
              PERFORM DIS-DET-LIGNE.


       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX.
           MOVE REG-PERSON TO HE-Z4.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 1 LOW.
           DISPLAY PR-NOM LINE LIN-IDX POSITION 6.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "  ".
           ADD 7 TO COL-IDX.
           DISPLAY PR-PRENOM LINE LIN-IDX POSITION COL-IDX LOW.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.

       NEXT-EQUIPE.
           CALL "6-EQUIPE" USING LINK-V EQ-RECORD EXC-KEY.

       DIS-HE-01.
           DISPLAY EQ-NUMBER LINE  1 POSITION 10.
           DISPLAY EQ-NOM    LINE  1 POSITION 15 SIZE 30.
       DIS-HE-END.

       AFFICHAGE-ECRAN.
           MOVE 2301 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE  2 TO LNK-LINE.
           MOVE 19 TO LNK-COL.
           MOVE  0 TO LNK-SPACE.
           MOVE LNK-MOIS TO LNK-SIZE.
           CALL "0-DSCAL" USING LINK-V.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
           PERFORM DISPLAY-F-KEYS.

       AFFICHE-PLAN.
           PERFORM A-P VARYING LIGNE FROM 1 BY 1 UNTIL LIGNE > 
           CHOIX-MAX.
       A-P.
           PERFORM DIS-AP VARYING JOUR FROM 1 BY 1 UNTIL JOUR > 
           MOIS-JRS(LNK-MOIS).

       DIS-AP.
           COMPUTE LIN-IDX = LIGNE + 3.
           COMPUTE COL-IDX = JOUR * 2 + 16.
           DIVIDE JOUR BY 2 GIVING IDX-3 REMAINDER IDX-4.
           IF IDX-4 = 0
              DISPLAY CAL-PLAGE(LIGNE, JOUR) LINE LIN-IDX 
              POSITION COL-IDX SIZE 2
           ELSE
              DISPLAY CAL-PLAGE(LIGNE, JOUR) LINE LIN-IDX 
              POSITION COL-IDX SIZE 2 LOW
           END-IF.
           IF TP-JOUR(LIGNE, LNK-MOIS, JOUR) = 0
              DISPLAY "--" LINE LIN-IDX POSITION COL-IDX LOW
           END-IF.

       END-PROGRAM.
           CLOSE PLANS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

       TESTS.
           CANCEL "6-PLANS".
           PERFORM TEST-MOIS VARYING LIGNE FROM 1 BY 1 UNTIL LIGNE > 
           CHOIX-MAX.

       TEST-MOIS.
           MOVE 0 TO CAL-TOTAL(LIGNE).
           PERFORM TEST-JOUR VARYING JOUR FROM 1 BY 1 UNTIL JOUR > 
           MOIS-JRS(LNK-MOIS).
           MOVE CAL-RECORD(LIGNE) TO PT-RECORD.
           IF PT-TOTAL > 0
              WRITE PT-RECORD INVALID REWRITE PT-RECORD END-WRITE
           ELSE
              DELETE PLANS INVALID CONTINUE.

       TEST-JOUR.
           ADD CAL-VAL(LIGNE, JOUR) TO CAL-TOTAL(LIGNE).

       TEST-EQ.
           MOVE PT-PLAGE(JOUR) TO PLG-KEY.
           CALL "6-PLAGE" USING LINK-V PLG-RECORD FAKE-KEY.
           IF PLG-EQUIPE = EQ-NUMBER
              IF IDX-4 = 0
                 ADD 1 TO CHOIX-MAX
              END-IF
              MOVE PT-PLAGE(JOUR) TO CAL-PLAGE(CHOIX-MAX, JOUR)
              MOVE 1 TO IDX-4
           END-IF.

       READ-PLAGE.
           MOVE HELP-VAL TO PLG-KEY.
           CALL "6-PLAGE" USING LINK-V PLG-RECORD SAVE-KEY.
           MOVE PLG-KEY TO HELP-VAL.

