      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-CSDEF MODULE GENERAL LECTURE CSDEF        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-CSDEF.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CSDEF.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CSDEF.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN  PIC 9 VALUE 0.
       01  ACTION    PIC 9 VALUE 0.
       01  ESC-KEY   PIC 9(4) COMP-1.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "CSDEF.LNK".

       01  EXC-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CSDEF .
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-CSDEF.

           IF NOT-OPEN = 0
              OPEN I-O CSDEF
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CD-RECORD.

           IF EXC-KEY = 99
      *       IF LNK-SQL = "Y" 
      *          CALL "9-CSDEF" USING LINK-V CD-RECORD EXC-KEY 
      *       END-IF
              WRITE CD-RECORD INVALID REWRITE CD-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.
           
           EVALUATE EXC-KEY 
               WHEN 0 PERFORM START-3
               READ CSDEF NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN 1 PERFORM START-4
               READ CSDEF PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN 66 PERFORM START-1
               READ CSDEF NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN 65 PERFORM START-2
               READ CSDEF PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN OTHER GO EXIT-3
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE CD-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-3.
           READ CSDEF NO LOCK INVALID INITIALIZE CD-REC-DET.
           GO EXIT-2.

       START-1.
           START CSDEF KEY > CD-KEY INVALID GO EXIT-1.
       START-2.
           START CSDEF KEY < CD-KEY INVALID GO EXIT-1.
       START-3.
           START CSDEF KEY >= CD-KEY INVALID GO EXIT-1.
       START-4.
           START CSDEF KEY <= CD-KEY INVALID GO EXIT-1.

           COPY "XACTION.CPY".
