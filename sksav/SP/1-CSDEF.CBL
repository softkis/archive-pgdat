      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-CSDEF DEFINITION CODES SALAIRES           �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-CSDEF.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CSDEF.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CSDEF.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 5. 

       01  HE-INPUT-PARAMETER.
      *    LINE COL SIZE HE-SCREEN    LLCCSZECRA
           02 IP-101 PIC 9(10)  VALUE 0425080011.
           02 IP-102 PIC 9(10)  VALUE 0525010001.
           02 IP-103 PIC 9(10)  VALUE 0625083024.
           02 IP-104 PIC 9(10)  VALUE 0725083020.
           02 IP-DEC PIC 9(10)  VALUE 2335020099.

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
            03 I-PP OCCURS 5.
               04 IP-LINE  PIC 99.
               04 IP-COL   PIC 99.
               04 IP-SIZE  PIC 99.
               04 IP-ECRAN PIC 9999.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CODTXT.REC".

       01   ECR-DISPLAY.
            02 HE-Z2 PIC ZZ.
            02 HE-Z4 PIC Z(4).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CSDEF.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-1-CSDEF.
       
           OPEN I-O CSDEF.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0029232216 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-DESCR
           WHEN  3 PERFORM AVANT-3
           WHEN  4 PERFORM AVANT-4
           WHEN  5 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT CD-NUMBER
             LINE  4 POSITION 20 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-DESCR.        
           ACCEPT CD-DEFINITION
             LINE  5 POSITION 20 SIZE 60
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           ACCEPT CD-FIN-A 
             LINE   6 POSITION 20 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
       AVANT-4.
           IF CD-FIN-A NOT = 0
             ACCEPT CD-FIN-M
             LINE 7 POSITION 22 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE
           ELSE
             MOVE 0 TO CD-FIN-M
           END-IF.
           IF CD-FIN-A NOT = 0
              EVALUATE EXC-KEY
                WHEN 65 SUBTRACT 1 FROM CD-FIN-M
                        PERFORM APRES-4
                WHEN 66 ADD      1 TO   CD-FIN-M
                        PERFORM APRES-4
              END-EVALUATE
           END-IF.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-CSDEF" USING LINK-V CD-RECORD
                    CANCEL "2-CSDEF"
                    PERFORM AFFICHAGE-ECRAN 
           WHEN  3 CALL "2-CSD" USING LINK-V CTX-RECORD
                   IF CTX-NUMBER > 0
                      MOVE CTX-NUMBER TO CD-NUMBER
                      CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY
                   END-IF
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  4 THRU 5 
                   IF CD-NUMBER NOT = 0
                      PERFORM COPY-CODSAL
                   END-IF
                   MOVE 1 TO INPUT-ERROR
           WHEN OTHER PERFORM NEXT-CS
           END-EVALUATE.
           IF CD-NUMBER = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-3.
           IF  CD-FIN-A > 0
           AND CD-FIN-A < 2002
              MOVE 2002 TO CD-FIN-A
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-4.
           IF CD-FIN-A > 0
              IF CD-FIN-M < 1
              OR CD-FIN-M > 12
                 MOVE 12 TO CD-FIN-M
                 MOVE 1 TO INPUT-ERROR
               END-IF
           END-IF.
           PERFORM DIS-HE-04.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO CD-TIME
                    MOVE LNK-USER TO CD-USER
                    WRITE CD-RECORD INVALID 
                        REWRITE CD-RECORD
                    END-WRITE   
                    MOVE 1 TO DECISION
                    PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN  8 CALL "4-CSDEL" USING LINK-V CD-RECORD
                    CANCEL "4-CSDEL"
                    DELETE CSDEF INVALID CONTINUE END-DELETE
                    INITIALIZE CD-RECORD
                    MOVE 1 TO DECISION
                    PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
               MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-CS.
           CALL "6-CSDEF" USING LINK-V CD-RECORD SAVE-KEY.

       DIS-HE-01.
           MOVE CD-NUMBER TO HE-Z4.
           DISPLAY HE-Z4 LINE  4 POSITION 20.
       DIS-HE-02.
           DISPLAY CD-DEFINITION LINE 5 POSITION 20.
       DIS-HE-03.
           MOVE CD-FIN-A TO HE-Z4.
           DISPLAY HE-Z4 LINE 6 POSITION 20.
       DIS-HE-04.
           MOVE CD-FIN-M TO LNK-NUM HE-Z2.
           DISPLAY HE-Z2 LINE 7 POSITION 22.
           MOVE "MO" TO LNK-AREA.
           MOVE 07301500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 32 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           MOVE "EE" TO LNK-AREA.
           MOVE 00042500 TO LNK-POSITION.
           PERFORM LIGNE VARYING IDX FROM 1 BY 1 UNTIL IDX > CHOIX-MAX.

       LIGNE.
           MOVE IDX TO HE-Z2.
           IF IDX < CHOIX-MAX
              DISPLAY HE-Z2 LINE IP-LINE(IDX) POSITION 1 LOW.
           MOVE IP-ECRAN(IDX) TO LNK-NUM.
           MOVE IP-LINE(IDX)  TO LNK-LINE.
           MOVE 4             TO LNK-COL.
           MOVE "L" TO LNK-LOW.
           CALL "0-DMESS" USING LINK-V.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE CSDEF.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

       COPY-CODSAL.   
           MOVE CD-NUMBER TO LNK-A LNK-B.
           MOVE "EE" TO LNK-AREA.
           MOVE 04402500 TO LNK-POSITION.
           MOVE "L" TO LNK-LOW.
           IF EXC-KEY = 4 
              MOVE " " TO LNK-YN 
              MOVE 500 TO LNK-NUM
           ELSE
              MOVE "X" TO LNK-YN 
              MOVE 501 TO LNK-NUM
           END-IF.
           CALL "0-DMESS" USING LINK-V.
           ACCEPT LNK-B
             LINE  4 POSITION 60 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.
           IF LNK-YN = " "
           AND LNK-A = LNK-B
              CONTINUE
           ELSE
              CALL "4-CSCOPY" USING LINK-V
              CANCEL "4-CSCOPY".
           MOVE " " TO LNK-YN.
           DISPLAY SPACES LINE 4 POSITION 40 SIZE 40.

