      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-FACLST IMPRESSION FACTURES                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  3-FACLST.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FACTINT.FC".
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FACTINT.FDE".
           COPY "FORM80.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  JOB-STANDARD          PIC X(10) VALUE "80        ".
       01  TIRET-TEXTE           PIC X(7) VALUE "_______".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "IMPRLOG.REC".
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  LAST-FIRME            PIC 9(6) VALUE 0.

       01  TOTAUX.
           02 TOTAL              PIC 9(8)V99.
           02 TVA                PIC 9(8)V99.
           02 A-PAYER            PIC 9(8)V99.

       01  TOTAUX-G.
           02 TOTAL-G            PIC 9(8)V99.
           02 TVA-G              PIC 9(8)V99.
           02 A-PAYER-G          PIC 9(8)V99.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FACTURE-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FI".
           02 ANNEE-FACTURE      PIC 9999.

       01  FORM-NAME             PIC X(9) VALUE "FACLIST.F".

       01  ECR-DISPLAY.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                FACTURE
                FORM.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-FACLST .

       
           IF NOT-OPEN = 0
              MOVE LNK-SUFFIX TO ANNEE-FACTURE
              OPEN INPUT FACTURE
              INITIALIZE TOTAUX-G
              MOVE 1 TO NOT-OPEN.

           IF LNK-VAL = 99
              PERFORM END-PROGRAM.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE FI-RECORD TOTAUX.
           MOVE FR-KEY TO FI-CLIENT.
           MOVE PARMOD-MOIS-DEB  TO FI-DEBUT-M.
           START FACTURE KEY >= FI-KEY-2 INVALID CONTINUE
           NOT INVALID PERFORM READ-FI THRU READ-FI-END.
           EXIT PROGRAM.

       READ-FI.
           READ FACTURE NEXT AT END 
              GO READ-FI-END.
           IF FR-KEY NOT = FI-CLIENT
              GO READ-FI-END.
           IF PARMOD-MOIS-DEB > FI-DEBUT-M 
              GO READ-FI.
           IF PARMOD-MOIS-FIN < FI-DEBUT-M 
              GO READ-FI.
           PERFORM FULL-PROCESS.
           GO READ-FI.
       READ-FI-END.
           IF FR-KEY = LAST-FIRME
              ADD  1 TO LIN-NUM
              MOVE 3 TO COL-NUM
              PERFORM TIRET UNTIL COL-NUM > 75
              ADD 1 TO LIN-NUM.

       FULL-PROCESS.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= 64
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM LAST-FIRME
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM PAGE-DATE 
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           IF FR-KEY NOT = LAST-FIRME
              PERFORM DONNEES-FIRME
              MOVE FR-KEY TO LAST-FIRME.
           PERFORM DETAILS.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF COUNTER > 0
              IF LIN-IDX < LIN-NUM
                 PERFORM TRANSMET
              END-IF
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE
              CANCEL "P080"
              CLOSE FACTURE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".

       DONNEES-FIRME.
           MOVE  0 TO DEC-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  5 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 50 TO COL-NUM.
           MOVE FR-COMPTA TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 70 TO COL-NUM.
           EVALUATE FR-FREQ-FACT  
                WHEN 0 MOVE 104 TO LNK-NUM
                WHEN 1 MOVE 104 TO LNK-NUM
                WHEN 2 MOVE 108 TO LNK-NUM
                WHEN 3 MOVE 105 TO LNK-NUM
                WHEN 6 MOVE 106 TO LNK-NUM
                WHEN 9 MOVE 99  TO LNK-NUM
           END-EVALUATE.
           MOVE "AA" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 2 TO LIN-NUM.

       PAGE-DATE.
           CALL "0-TODAY" USING TODAY.
           ADD 1 TO PAGE-NUMBER.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO LIN-NUM.
           MOVE  75 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 45 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 48 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 51 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       DETAILS.
           MOVE FI-NUMBER TO VH-00.
           MOVE  8 TO CAR-NUM.
           MOVE  5 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FI-EDIT-A TO HE-AA.
           MOVE FI-EDIT-M TO HE-MM.
           MOVE FI-EDIT-J TO HE-JJ.
           MOVE HE-DATE TO ALPHA-TEXTE.
           MOVE 25 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FI-DEBUT-M TO VH-00.
           MOVE  2 TO CAR-NUM.
           MOVE  17 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FI-TOTAL TO VH-00.
           ADD  FI-TOTAL TO TOTAL TOTAL-G.
           MOVE  8 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 40 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FI-TVA   TO VH-00.
           ADD  FI-TVA   TO TVA TVA-G.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 52 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FI-A-PAYER TO VH-00.
           ADD  FI-A-PAYER TO A-PAYER A-PAYER-G.
           MOVE  8 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 64 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           IF FI-TOTAL NOT = TOTAL
              PERFORM TOTAUX.
           PERFORM TOTAL-G.

       TOTAUX.
           MOVE TOTAL TO VH-00.
           MOVE  8 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 40 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE TVA   TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 52 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE A-PAYER TO VH-00.
           MOVE  8 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 64 TO COL-NUM.
           PERFORM FILL-FORM.

       TOTAL-G.
           MOVE LIN-NUM TO IDX-1.
           MOVE 66 TO LIN-NUM.

           MOVE TOTAL-G TO VH-00.
           MOVE  8 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 40 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE TVA-G TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 52 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE A-PAYER-G TO VH-00.
           MOVE  8 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 64 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE IDX-1 TO LIN-NUM.
           