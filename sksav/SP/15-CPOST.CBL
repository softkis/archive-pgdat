      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 15-CPOST TRANSFERT CODES POSTAUX       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    15-CPOST.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.

           COPY "C-P.FC".

           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.
      *袴袴袴袴袴袴�

       FILE SECTION.
      *컴컴컴컴컴컴
           COPY "C-P.FDE".

       FD  TF-TRANS
           LABEL RECORD STANDARD
           DATA RECORD TFP-RECORD.

       01  TFP-RECORD.
           02 TFP-REC-1    PIC X(120).
           02 TFP-REC-2 REDEFINES TFP-REC-1.
              03 TFP-ID     PIC X.
              03 TFP-FILLER PIC X(119).

       WORKING-STORAGE SECTION.
       01  ESPACE   PIC X VALUE SPACES.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PARMOD.REC".
       01  POINTERS.
           02 PN            PIC 999 OCCURS 20.

       01  BIDULE.
           02 BD            PIC X OCCURS 120.

       01  CHOIX-MAX   PIC 99 VALUE 2.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z4 PIC Z(4).
           02 HE-ZX REDEFINES HE-Z4.
               03 HE-M PIC Z.
               03 HE-F PIC Z.
               03 HE-A PIC ZZ.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                     CDPOST
                     TF-TRANS.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-15-RMG.

           INITIALIZE PARMOD-RECORD.
           MOVE 0 TO LNK-SUITE.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           OPEN I-O   CDPOST.

           PERFORM AFFICHAGE-ECRAN.
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.


       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0000000025 TO EXC-KFR (1)
                      MOVE 0052000000 TO EXC-KFR (11).


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  2 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           IF PARMOD-PATH = SPACES
              MOVE "C:\FILE" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 14 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       
       APRES-DEC.
           EVALUATE EXC-KEY
               WHEN 5 OPEN INPUT TF-TRANS
                      PERFORM LECT-TRANS THRU END-TRANS
                      PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       LECT-TRANS.
           READ TF-TRANS AT END GO END-TRANS.
      *    IF TFP-ID NOT = "P"
      *       GO LECT-TRANS.
           IF TFP-ID = "P"
              GO LECT-TRANS.
           ADD 1 TO IDX-4.
           DISPLAY  IDX-4 LINE 16 POSITION 10.
           INITIALIZE CP-RECORD IDX-1 POINTERS IDX-2.
           MOVE TFP-RECORD TO BIDULE.
           PERFORM GET-POINT VARYING IDX FROM 1 BY 1 UNTIL IDX > 120.
           COMPUTE IDX-2 = PN(6) - PN(5) - 1.
           COMPUTE IDX-3 = PN(6) - 1.
           STRING ESPACE DELIMITED BY SIZE INTO TFP-RECORD 
           WITH POINTER IDX-3.
           IF IDX-2 = 0
              MOVE PN(6) TO PN(5).
           UNSTRING TFP-RECORD DELIMITED BY ";" INTO CP-RUE WITH POINTER 
           PN(5).
           UNSTRING TFP-RECORD DELIMITED BY ";" INTO CP-MATCHCODE-2 
           WITH POINTER PN(7).
           UNSTRING TFP-RECORD DELIMITED BY ";" INTO CP-CODE 
           WITH POINTER PN(9).
           UNSTRING TFP-RECORD DELIMITED BY ";" INTO CP-LOCALITE
           WITH POINTER PN(10).
           MOVE CP-LOCALITE TO CP-MATCHCODE CP-MATCHCODE-3. 
           display cp-KEY line 20 position 10.
           IF CP-CODE > 0
              WRITE CP-RECORD INVALID REWRITE CP-RECORD END-REWRITE.
           display cp-KEY line 19 position 10.
           INITIALIZE TFP-RECORD.
           GO LECT-TRANS.
       END-TRANS.
           CLOSE TF-TRANS.


       AFFICHAGE-ECRAN.
           COMPUTE LNK-VAL = 2556.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       END-PROGRAM.
           EXIT PROGRAM.


      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

       GET-POINT.
           IF BD(IDX) = ";"
              ADD 1 TO IDX-1
              COMPUTE PN(IDX-1) = IDX + 1.
