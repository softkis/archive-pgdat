      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-QUADEF MODULE LECTURE DEFINITION QUALIFICATION�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-QUADEF.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "QUADEF.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "QUADEF.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "QUADEF.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON QUADEF.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-QUADEF.
       
           IF NOT-OPEN = 0
              OPEN INPUT QUADEF
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO QD-RECORD.
           MOVE 0 TO LNK-VAL.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ QUADEF PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ QUADEF NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ QUADEF NO LOCK INVALID INITIALIZE 
               QD-REC-DET END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE QD-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START QUADEF KEY < QD-KEY INVALID GO EXIT-1.
       START-2.
           START QUADEF KEY > QD-KEY INVALID GO EXIT-1.


               