      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME BAT ADAPTATON CC BATIMENT                   �
      *  � CODES SALAIRES, MENU, BAREME, CP                      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    BAT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODPAR.FC".
           COPY "BAREME.FC".
           SELECT OPTIONAL TF-FILE ASSIGN TO DISK "BATIMENT.BAR",
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

           COPY "MENU.FC".
           COPY "MENUCOPY.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CODPAR.FDE".
           COPY "MENU.FDE".
           COPY "MENUCOPY.FDE".
           COPY "BAREME.FDE".

       FD  TF-FILE 
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TF-KEY.
              03 TF-BAREME       PIC X(10).
              03 TF-GRADE        PIC 999.
              03 TF-DATE.
                 04 TF-ANNEE     PIC 9999.
                 04 TF-MOIS      PIC 99.

           02 TF-KEY-1.  
              03 TF-BAREME-1     PIC X(10).
              03 TF-DATE-1.
                 04 TF-ANNEE-1   PIC 9999.
                 04 TF-MOIS-1    PIC 99.
              03 TF-GRADE-1      PIC 999.

           02 TF-REC-DET.
              03 TF-DESCRIPTION  PIC X(10).
              03 TF-POINT        PIC 9.
              03 TF-ECHELONS.
                 04 TF-ECHELON PIC 9(10) OCCURS 80.
              03 TF-STAMP.
                 04 TF-TIME.
                    05 TF-ST-ANNEE PIC 9999.
                    05 TF-ST-MOIS  PIC 99.
                    05 TF-ST-JOUR  PIC 99.
                    05 TF-ST-HEURE PIC 99.
                    05 TF-ST-MIN   PIC 99.
                 04 TF-USER        PIC X(10).


       WORKING-STORAGE SECTION.
             
       01  COD-V.
           02  COD-1    PIC 9999 VALUE 351.
           02  COD-2    PIC 9999 VALUE 352.
           02  COD-3    PIC 9999 VALUE 356.
           02  COD-4    PIC 9999 VALUE 357.
           02  COD-5    PIC 9999 VALUE 370.
           02  COD-6    PIC 9999 VALUE 371.
           02  COD-7    PIC 9999 VALUE 372.
           02  COD-8    PIC 9999 VALUE 700.
       01  COD-R REDEFINES COD-V.
           02  COD PIC 9999 OCCURS 7.

       01  MENUS.
           02  MEN-1    PIC 9(10) VALUE 0401040100.
           02  MEN-2    PIC 9(10) VALUE 0401040200.
           02  MEN-3    PIC 9(10) VALUE 0401040300.
           02  MEN-4    PIC 9(10) VALUE 0401040400.
           02  MEN-5    PIC 9(10) VALUE 0401040500.
           02  MEN-6    PIC 9(10) VALUE 0401040600.
           02  MEN-7    PIC 9(10) VALUE 0101060000.
           02  MEN-8    PIC 9(10) VALUE 0101060100.
           02  MEN-9    PIC 9(10) VALUE 0101060200.
           02  MEN-10   PIC 9(10) VALUE 0101060300.
           02  MEN-11   PIC 9(10) VALUE 0101060400.
           02  MEN-12   PIC 9(10) VALUE 0401100300.
           02  MEN-13   PIC 9(10) VALUE 0204000000.
           02  MEN-14   PIC 9(10) VALUE 0204010000.
           02  MEN-15   PIC 9(10) VALUE 0204030000.
           02  MEN-16   PIC 9(10) VALUE 0204040000.
           02  MEN-17   PIC 9(10) VALUE 0204050000.
           02  MEN-18   PIC 9(10) VALUE 0204060000.

           02  MEN-19   PIC 9(10) VALUE 0101030400.
           02  MEN-20   PIC 9(10) VALUE 0101031000.
           02  MEN-21   PIC 9(10) VALUE 0610000000.
       01  COD-M REDEFINES MENUS.
           02  MEN PIC 9(10) OCCURS 21.


      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "BARDEF.REC".

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON 
           CODPAR MENU MENUCOPY BAREME TF-FILE.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  program.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-BAT.

           OPEN I-O CODPAR
           INITIALIZE CP-RECORD 
           READ CODPAR INVALID CONTINUE
               NOT INVALID 
               PERFORM WRITE-CODPAR.

           OPEN I-O MENU.
           OPEN INPUT MENUCOPY.
           OPEN I-O BAREME.

           MOVE 0 TO LNK-VAL.
           DISPLAY "ADAPTATION CODES SALAIRE ! " LINE 24 POSITION 10.
           PERFORM COPIES VARYING IDX FROM 1 BY 1 UNTIL IDX > 8.
           DISPLAY "ADAPTATION MENU            " LINE 24 POSITION 10.
           PERFORM MENU   VARYING IDX FROM  1 BY 1 UNTIL IDX > 18.
           PERFORM EFFACE VARYING IDX FROM 19 BY 1 UNTIL IDX > 21.
           DISPLAY "                           " LINE 24 POSITION 10.
BAR        PERFORM READ-FILES THRU READ-FILES-END.
           EXIT PROGRAM.

       COPIES.
           MOVE COD(IDX) TO LNK-NUM.
           CALL "4-CSIMP" USING LINK-V.

       MENU.
           MOVE MEN(IDX) TO MNC-KEY.
           READ MENUCOPY INVALID CONTINUE
           NOT INVALID
              PERFORM TRANSLATE.

       TRANSLATE.
           MOVE MNC-RECORD TO MN-RECORD.
           WRITE MN-RECORD INVALID REWRITE MN-RECORD END-WRITE.
           CALL "4-MENUD" USING LINK-V MN-RECORD.
           CALL "4-MENUE" USING LINK-V MN-RECORD.

       EFFACE.
           MOVE MEN(IDX) TO MN-KEY.
           DELETE MENU INVALID CONTINUE.

       READ-FILES.
           IF NOT-OPEN = 0
              OPEN INPUT TF-FILE
              MOVE 1 TO NOT-OPEN
           END-IF.
           READ TF-FILE AT END 
              GO READ-FILES-END
           END-READ.
           WRITE BAR-RECORD FROM TF-RECORD INVALID CONTINUE.
           GO READ-FILES.
       READ-FILES-END.
           INITIALIZE BDF-RECORD.
           MOVE "B" TO BDF-CODE.
           MOVE 3 TO BDF-DIGIT.
           MOVE 5 TO BDF-DECIM.
           MOVE "BATIMENT" TO BDF-NOM.
           CALL "6-BARDEF" USING LINK-V BDF-RECORD WR-KEY.

       WRITE-CODPAR.
           MOVE 370 TO COD-PAR(197, 2).
           MOVE 371 TO COD-PAR(197, 3).
           REWRITE CP-RECORD INVALID CONTINUE.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".


