      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 15-IMPC TRANSFERT HEURES IMPRIMERIE CENTRALE�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    15-IMPC.

       ENVIRONMENT DIVISION.
 
       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.

           SELECT PICKUP ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-PICKUP.

           SELECT OPTIONAL CONVERSION ASSIGN TO DISK "TABHEURE.CON"
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-HELP.

           COPY "JOURS.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.

       FD  PICKUP
           LABEL RECORD STANDARD
           DATA RECORD IS PICK-RECORD.

       01  PICK-RECORD.
           02 PICK-PERS            PIC 9(6).
           02 PICK-ANNEE           PIC 99.
           02 PICK-MOIS            PIC 99.
           02 PICK-JOUR            PIC 99.

  COM      02 PICK-HEURES.
              03 PICK-HEURE        PIC 9999 OCCURS 40.

           COPY "JOURS.FDE".
           COPY "TRIPR.FDE".

       FD  CONVERSION
           RECORD CONTAINS 800 CHARACTERS
           LABEL RECORD STANDARD
           DATA RECORD IS TRPA-RECORD.
       01  TRPA-RECORD.
           02 PARAM-OCC  PIC 9999 OCCURS 100.
           02 PARAM-OCO  PIC 9999 OCCURS 100.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX PIC 99 VALUE 2.
           COPY "V-MINUTE.CPY".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "REGISTRE.REC".
           COPY "PERSON.REC".
           COPY "CARRIERE.REC".
           COPY "PARMOD.REC".

           COPY "PRESENCE.REC".
           COPY "CCOL.REC".
           COPY "CALEN.REC".
           COPY "LIVRE.REC".

       01  JOUR-IDX             PIC 99 COMP-3.
       01  JOUR                 PIC 99.
       01  ANNEE                PIC 9999.
       01  HELP-1               PIC 9999.
       01  HELP-2 REDEFINES HELP-1 PIC 99V99.

       01  HELP-HH              PIC 9999.
       01  HELP-HR REDEFINES HELP-HH.
           02 HELP-H            PIC 99.
           02 HELP-M            PIC 99.
           
       01  O-PARAMETER.
           02 OCC-PARAMETER.
              03 OCC  PIC 9999 OCCURS 100.
           02 OCO-PARAMETER.
              03 OCO  PIC 9999 OCCURS 100.

       01  JOURS-NAME.
           02 FILLER            PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS       PIC 999.
       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "I".
           02 FIRME-TRIPR        PIC 999999.
           02 FILLER             PIC XXXX VALUE ".IMP".
           02 USER-TRIPR         PIC XXXXXXXX.

       01  HE-Z6 PIC Z(6).
       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-ZX REDEFINES HE-Z4.
               03 HE-M PIC Z.
               03 HE-F PIC Z.
               03 HE-A PIC ZZ.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
          COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                PICKUP 
                CONVERSION
                TRIPR
                JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-15-IMPC.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           MOVE 0 TO FIRME-TRIPR.
           ADD  FR-KEY TO FIRME-TRIPR.
           MOVE LNK-USER  TO USER-TRIPR.
           DELETE FILE TRIPR.
           OPEN I-O TRIPR.
           INITIALIZE TRIPR-RECORD.

           OPEN INPUT CONVERSION.
           READ CONVERSION INTO O-PARAMETER AT END CONTINUE END-READ.
           CLOSE CONVERSION.
           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN I-O JOURS.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0000000025 TO EXC-KFR (1)
                      MOVE 0052000000 TO EXC-KFR (11).


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-PATH
           WHEN  2 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
               PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  2 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\IMP\FILE" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 14 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-DEC.
           EVALUATE EXC-KEY
               WHEN 5 OPEN INPUT PICKUP
                      PERFORM LECT-PICKUP THRU END-PICKUP
                      PERFORM START-TRI
                      PERFORM END-PROGRAM
           END-EVALUATE.

       LECT-PICKUP.
           INITIALIZE PICK-RECORD.
           READ PICKUP AT END GO END-PICKUP.
           DISPLAY PICK-RECORD LINE 20 POSITION 10 SIZE 20.
TEST!      IF PICK-ANNEE = 97
              MOVE 02 TO PICK-ANNEE.
           COMPUTE ANNEE = PICK-ANNEE + 2000.
           IF ANNEE     NOT = LNK-ANNEE
           OR PICK-MOIS NOT = LNK-MOIS
              PERFORM DIS-ERR.
           IF PICK-PERS NOT = TRIPR-PERSON
              PERFORM WRITE-TRI.
           PERFORM TEST-NULL VARYING IDX-2 FROM 1 BY 1 UNTIL IDX-2 > 40.
           GO LECT-PICKUP.
       END-PICKUP.
           CLOSE PICKUP.

       WRITE-TRI.
           MOVE PICK-PERS TO TRIPR-PERSON REG-PERSON.
           WRITE TRIPR-RECORD INVALID CONTINUE
                NOT INVALID PERFORM CLEAN-OLD-TOTAL.

       CLEAN-OLD-TOTAL.
           INITIALIZE JRS-RECORD.
           MOVE FR-KEY TO JRS-FIRME.
           MOVE PICK-PERS TO JRS-PERSON.
           MOVE PICK-MOIS TO JRS-MOIS.
           PERFORM DELETE-JOURS 
              VARYING IDX-2 FROM 1 BY 1 UNTIL IDX-2 > 60.

       DELETE-JOURS.
           IF OCC(IDX-2) NOT = 99
              MOVE OCC(IDX-2) TO JRS-OCCUPATION
              MOVE OCO(IDX-2) TO JRS-COMPLEMENT
              IF LNK-SQL = "Y" 
                 CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY 
              END-IF
              DELETE JOURS INVALID CONTINUE END-DELETE
           END-IF.
           IF  OCC(IDX-2) = 99
           AND OCO(IDX-2) = 99
              MOVE OCC(IDX-2) TO JRS-OCCUPATION
              MOVE 0 TO JRS-COMPLEMENT
              IF LNK-SQL = "Y" 
                 CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY 
              END-IF
              DELETE JOURS INVALID CONTINUE END-DELETE
           END-IF.

       START-TRI.
           INITIALIZE TRIPR-RECORD.
           START TRIPR KEY > TRIPR-KEY INVALID CONTINUE
           NOT INVALID PERFORM READ-TRI THRU READ-TRI-END.
       READ-TRI.
           READ TRIPR NEXT AT END 
               GO READ-TRI-END
           END-READ.
           MOVE TRIPR-PERSON TO REG-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           IF REG-PERSON = 0
              GO READ-TRI.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           PERFORM DIS-HE-01.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF PRES-TOT(LNK-MOIS) NOT = 0
              CALL "4-A" USING LINK-V PR-RECORD REG-RECORD
              PERFORM MALADIE THRU MALADIE-END.
           DELETE TRIPR INVALID CONTINUE.
           GO READ-TRI.
       READ-TRI-END.
           EXIT.

       TEST-NULL.
           MOVE PICK-HEURE(IDX-2) TO HELP-1.
           IF HELP-1 = 0
           OR OCC(IDX-2) = 99
              CONTINUE
           ELSE
              PERFORM WRITE-OCC.

       WRITE-OCC.
           INITIALIZE JRS-RECORD.
           MOVE FR-KEY TO 
           JRS-FIRME  JRS-FIRME-1 JRS-FIRME-2 JRS-FIRME-3.
           MOVE PICK-PERS TO
           JRS-PERSON JRS-PERSON-1 JRS-PERSON-2 JRS-PERSON-3.
           MOVE PICK-MOIS TO LNK-MOIS
           JRS-MOIS JRS-MOIS-1 JRS-MOIS-2 JRS-MOIS-3.
           MOVE OCC(IDX-2) TO
                JRS-OCCUPATION
                JRS-OCCUPATION-1
                JRS-OCCUPATION-2
                JRS-OCCUPATION-3.
PLAN !     IF OCC(IDX-2) NOT = 99
           MOVE OCO(IDX-2) TO
                JRS-COMPLEMENT
                JRS-COMPLEMENT-1
                JRS-COMPLEMENT-2
                JRS-COMPLEMENT-3.
           READ JOURS INVALID INITIALIZE JRS-REC-DET.
           MOVE PICK-HEURE(IDX-2) TO HELP-HH.
           COMPUTE SH-00 = HELP-H.
           IF HELP-M > 0
              COMPUTE SH-00 = SH-00 + MINUTE(HELP-M) / 100.
           ADD SH-00 TO JRS-HRS(PICK-JOUR).
           MOVE 0 TO JRS-HRS(32).
           PERFORM ADD-UP VARYING IDX-1 FROM 1 BY 1 UNTIL 
                                  IDX-1 > 31.
           IF JRS-HRS(32) > 0 
              IF LNK-SQL = "Y" 
                 CALL "9-JOURS" USING LINK-V JRS-RECORD WR-KEY 
              END-IF
              WRITE JRS-RECORD INVALID 
                    REWRITE JRS-RECORD
              END-WRITE   
           END-IF.

       ADD-UP.
           ADD JRS-HRS(IDX-1) TO JRS-HRS(32).

       DIS-HE-01.
           MOVE REG-PERSON    TO HE-Z6.
           DISPLAY HE-Z6     LINE  7 POSITION 35.
           DISPLAY PR-NOM    LINE  7 POSITION 45.
           DISPLAY PR-PRENOM LINE  8 POSITION 45.

       DIS-ERR.
           MOVE PICK-MOIS TO HE-A LNK-NUM.
           DISPLAY HE-A LINE 19 POSITION 50.
           MOVE "MO" TO LNK-AREA.
           MOVE 19551200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
           DISPLAY ANNEE LINE 19 POSITION 65.
           MOVE "AA" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
           ACCEPT ACTION.
           PERFORM END-PROGRAM.

       AFFICHAGE-ECRAN.
           MOVE 2555 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           EXIT.

       END-PROGRAM.
           CLOSE JOURS.
           CLOSE TRIPR.
           DELETE FILE TRIPR.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

       MALADIE.
           INITIALIZE L-RECORD.
           MOVE FR-KEY     TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON. 
           MOVE LNK-MOIS   TO L-MOIS.
       MALADIE-1.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD NX-KEY.
           IF REG-PERSON NOT = L-PERSON
           OR LNK-MOIS   NOT = L-MOIS
              GO MALADIE-END.
           IF L-SUITE > 0
              MOVE L-SUITE TO LNK-SUITE 
              PERFORM DJT.
           GO MALADIE-1.
       MALADIE-END.
           MOVE 0 TO LNK-SUITE.

       DJT.
           CALL "4-DJT" USING LINK-V REG-RECORD.
           MOVE LNK-VAL   TO L-DATE-REPRISE.
           MOVE LNK-VAL-2 TO L-DATE-DERNIER.
           MOVE LNK-POSITION TO L-DATE-3M.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD WR-KEY.
