      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME CSCHAM IMPORTATION                          �
      *  � CODES SALAIRES CHAMBRE SALARIES 2009                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    CSCHAM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
             
       01  COD-V.
           02  COD-1    PIC 9999 VALUE 0650.
           02  COD-2    PIC 9999 VALUE 0651.
           02  COD-3    PIC 9999 VALUE 0201.
           02  COD-4    PIC 9999 VALUE 0299.
           02  COD-5    PIC 9999 VALUE 9011.
           02  COD-6    PIC 9999 VALUE 9014.
           02  COD-7    PIC 9999 VALUE 9015.
           02  COD-8    PIC 9999 VALUE 9017.
       01  COD-R REDEFINES COD-V.
           02  COD PIC 9999 OCCURS 8.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".



       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-CSCHAM.
           MOVE 0 TO LNK-VAL.
           MOVE LNK-ANNEE TO IDX-1.
           MOVE 2009 TO LNK-ANNEE.
           DISPLAY "ADAPTATION CODES SALAIRE ! " LINE 24 POSITION 10.
           PERFORM COPIES VARYING IDX FROM 1 BY 1 UNTIL IDX > 8.
           MOVE IDX-1 TO LNK-ANNEE.
           EXIT PROGRAM.

       COPIES.
           MOVE COD(IDX) TO LNK-NUM.
           CALL "4-CSIMP" USING LINK-V.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".


