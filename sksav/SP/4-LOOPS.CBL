      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-LOOPS LANCEMENT AUTOMATIQUE DE MODULES DE �
      *  � FIRME DEBUT A FIRME FIN SANS CONTROLE ACTIF/INACTIF   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-LOOPS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.


       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 9.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PARMOD.REC".
           COPY "STATUT.REC".
           COPY "V-VAR.CPY".
           COPY "MOTDEP.REC".
        
       01  COMPTEUR              PIC 99.

       01 HE-MOT.
          02 H-M PIC X       OCCURS 20.

       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-FIRME            PIC 9(6).
       01  SAVE-LANGUE           PIC X.
       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  HE-ORDRE              PIC 9999 VALUE 0.
       01  HE-SEL REDEFINES HE-ORDRE.
           02 H-S PIC 9    OCCURS 4.
       01  HE-ALP REDEFINES HE-ORDRE.
           02 H-A PIC XXXX.

       01  DATES.
           03 ELECTION    PIC 9(8).
           03 ELECTION-R REDEFINES ELECTION.
              05 ELECTION-A    PIC 9999.
              05 ELECTION-M    PIC 99.
              05 ELECTION-J    PIC 99.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-PROGRAMME-4-LOOPS.
       
           MOVE LNK-LANGUAGE TO SAVE-LANGUE.
           INITIALIZE PARMOD-RECORD STATUT.
           MOVE MENU-PROG-NAME   TO PARMOD-MODULE.
           IF MENU-EXTENSION-1 NOT = SPACES
              MOVE MENU-EXTENSION-1 TO PARMOD-MODULE.
           MOVE MENU-PROG-NUMBER TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           IF PARMOD-SETTINGS = SPACES 
              MOVE "NNNN NNNNN" TO PARMOD-SETTINGS
           END-IF.
           MOVE FR-KEY TO SAVE-FIRME.
           MOVE 0 TO FR-KEY.
           MOVE LNK-MOIS  TO SAVE-MOIS.
           MOVE 0 TO COL-IDX.
           INSPECT MENU-EXTENSION-2 TALLYING COL-IDX FOR 
           CHARACTERS BEFORE "-".
           IF COL-IDX > 2
              MOVE MENU-EXTENSION-2 TO H-A.
           IF H-S(3) = 2
              MOVE LNK-MOIS TO MOIS-DEBUT.
           IF H-S(4) = 2
              MOVE LNK-MOIS TO MOIS-FIN.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0000000000 TO EXC-KFR(1)
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6 THRU 7
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11)
                      IF MENU-BATCH > 7
                         MOVE 0000000007 TO EXC-KFR(1)
                         MOVE 1700000000 TO EXC-KFR(2)
                      END-IF
                      IF MENU-PROG-NUMBER = 703
                         MOVE 0000680000 TO EXC-KFR (14)
                      END-IF.
           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 IF H-S(1) > 0
                   OR MENU-PROG-NUMBER = 703
                      PERFORM AVANT-STAT
                   END-IF
           WHEN  5 IF H-S(2) > 0
                      PERFORM AVANT-PATH
                   END-IF
                   IF MENU-PROG-NUMBER = 703
                      PERFORM AVANT-PATH
                   END-IF
           WHEN  6 IF H-S(3) > 0
                      PERFORM AVANT-6
                   END-IF
           WHEN  7 IF H-S(4) > 0
                      PERFORM AVANT-7
                   END-IF
           WHEN  8 IF MENU-PROG-NUMBER = 703
                      PERFORM AVANT-8
                   END-IF
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 IF H-S(1) > 0
                   OR MENU-PROG-NUMBER = 703
                      PERFORM APRES-STAT
                   END-IF
           WHEN  6 IF H-S(3) > 0
                      PERFORM APRES-6
                   END-IF
           WHEN  7 IF H-S(4) > 0
                      PERFORM APRES-7
                   END-IF
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-STAT.
           ACCEPT STATUT 
             LINE 9 POSITION 22 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       APRES-STAT.
           MOVE STATUT TO STAT-CODE.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-STATUT" USING LINK-V STAT-RECORD
                    MOVE STAT-CODE TO STATUT
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-STATUT
                    MOVE STAT-CODE TO STATUT
           END-EVALUATE.
           IF STAT-CODE > 4
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-STAT.

       NEXT-STATUT.
           CALL "6-STATUT" USING LINK-V STAT-RECORD EXC-KEY.

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 12 POSITION 20 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           DISPLAY PARMOD-PATH LINE 12 POSITION 20.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       AVANT-6.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 21 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 21 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           IF PARMOD-PROG-NUMBER-1 = 0
              MOVE TODAY-DATE TO PARMOD-PROG-NUMBER-1
           END-IF.
           MOVE 15200000 TO LNK-POSITION.
           CALL "0-GDATE" USING PARMOD-PROG-NUMBER-1 
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-DATE TO PARMOD-PROG-NUMBER-1 
              MOVE 0 TO LNK-NUM
              MOVE 13 TO EXC-KEY.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
           PERFORM DIS-HE-08.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-06.
           PERFORM DIS-HE-07.

       APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-07.


       APRES-DEC.
           MOVE "N" TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM TRAITEMENT
            WHEN 68 CALL "5-SELMOT" USING LINK-V PARMOD-RECORD
                    MOVE PARMOD-SETTINGS TO HE-MOT
                    MOVE "N" TO H-M(1)
                                H-M(2)
                                H-M(3)
                                H-M(4)
                                H-M(6)
                                H-M(7)
                                H-M(8)
                                H-M(9)
                                H-M(10)
                    MOVE HE-MOT TO PARMOD-SETTINGS
                    CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    PERFORM DISPLAY-F-KEYS
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TRAITEMENT.
           MOVE 0 TO INPUT-ERROR.
           PERFORM START-FIRME.
           IF INPUT-ERROR = 0 
               PERFORM READ-FIRME THRU READ-FIRME-END.
           PERFORM END-PROGRAM.

       START-FIRME.
           IF FR-KEY > 0 
              SUBTRACT 1 FROM FR-KEY.
           IF A-N = "A"
              MOVE 9999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-FIRME.
           PERFORM NEXT-FIRME.
           IF FR-KEY = 0
           OR FR-KEY > END-NUMBER
           OR FR-MATCHCODE > END-MATCHCODE
              GO READ-FIRME-END
           END-IF.
           ACCEPT ACTION TIME 1 LINE 23 POSITION 27 NO BEEP
           ON EXCEPTION ESC-KEY CONTINUE END-ACCEPT.
           IF ESC-KEY = 27 
              EXIT PROGRAM.
DIFF  *    IF FR-FIN-A > 0 AND < LNK-ANNEE
      *       GO READ-FIRME
      *    END-IF.
           PERFORM DIS-HE-01.
           MOVE STATUT     TO PARMOD-STATUT.
           MOVE MOIS-DEBUT TO PARMOD-MOIS-DEB.
           MOVE MOIS-FIN   TO PARMOD-MOIS-FIN.
           CALL MENU-EXTENSION-1 USING LINK-V PARMOD-RECORD.
           GO READ-FIRME.
       READ-FIRME-END.
           PERFORM END-PROGRAM.

       NEXT-FIRME.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.

       DIS-HE-01.
           MOVE FR-KEY    TO HE-Z6.
           DISPLAY HE-Z6  LINE  6 POSITION 17.
           DISPLAY FR-NOM LINE  6 POSITION 25 SIZE 30.
       DIS-HE-STAT.
           IF H-S(1) > 0
           OR MENU-PROG-NUMBER = 703
              DISPLAY STAT-NOM  LINE 9 POSITION 25 SIZE 20.
       DIS-HE-PATH.
           IF H-S(2) > 0
              DISPLAY PARMOD-PATH  LINE 12 POSITION 20.
       DIS-HE-06.
           IF H-S(3) > 0
              MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM
              DISPLAY HE-Z2 LINE 19 POSITION 21
              MOVE "MO" TO LNK-AREA
              MOVE 19251200 TO LNK-POSITION
              CALL "0-DMESS" USING LINK-V.
       DIS-HE-07.
           IF H-S(3) > 0
              MOVE MOIS-FIN TO HE-Z2 LNK-NUM
              DISPLAY HE-Z2 LINE 20 POSITION 21
              MOVE "MO" TO LNK-AREA
              MOVE 20251200 TO LNK-POSITION
              CALL "0-DMESS" USING LINK-V.
       DIS-HE-08.
           IF MENU-PROG-NUMBER = 703
              MOVE PARMOD-PROG-NUMBER-1 TO ELECTION LNK-KEY
              MOVE ELECTION-A TO HE-AA
              MOVE ELECTION-M TO HE-MM
              MOVE ELECTION-J TO HE-JJ
              DISPLAY HE-DATE LINE 15 POSITION 20
           END-IF.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE MENU-PROG-NUMBER TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

           IF MENU-PROG-NUMBER = 703
              PERFORM AFFICHE-PARAM.
           
       AFFICHE-PARAM.
           MOVE 2 TO LIN-IDX.
           MOVE PARMOD-SETTINGS TO HE-MOT.
           INITIALIZE MD-RECORD COMPTEUR.
           PERFORM AFFICHE-D THRU AFFICHE-END.

       AFFICHE-D.
           MOVE COMPTEUR TO LNK-NUM.
           ADD 1 TO LIN-IDX LNK-NUM.
           CALL "6-MOTDEP" USING LINK-V MD-RECORD NX-KEY.
           IF MD-NOM = SPACES
              GO AFFICHE-END.
           ADD 1 TO COMPTEUR.
           MOVE COMPTEUR TO HE-Z2.
           DISPLAY MD-NOM LINE LIN-IDX POSITION 65 SIZE 16 LOW.
           DISPLAY H-M(COMPTEUR) LINE LIN-IDX POSITION 64.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 62 LOW.
           GO AFFICHE-D.
       AFFICHE-END.
           EXIT.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF MENU-BATCH > 7
              MOVE 99 TO LNK-VAL
              CALL MENU-EXTENSION-1 USING LINK-V PARMOD-RECORD.
           IF MENU-PROG-NUMBER = 703
              CANCEL MENU-EXTENSION-1
              CALL MENU-EXTENSION-2 USING LINK-V PARMOD-RECORD
              CANCEL MENU-EXTENSION-2
           END-IF.
           MOVE SAVE-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V A-N FAKE-KEY.
           MOVE SAVE-MOIS TO LNK-MOIS.
           MOVE SAVE-LANGUE TO LNK-LANGUAGE.
           CANCEL MENU-EXTENSION-1.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXF".
           COPY "XKEY.CPY".
           COPY "XDEC.CPY".
           MOVE EXC-KEY TO LNK-EXC-KEY.
           IF EXC-KEY = 15 OR 16
              SUBTRACT 10 FROM EXC-KEY
           END-IF.
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

