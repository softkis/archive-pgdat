      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 5-RPDRAP ENLEVEMENT DRAPEAUX RAPPORTS       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    5-RPDRAP.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "RAPPORT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "RAPPORT.FDE".

       WORKING-STORAGE SECTION.

       01  CHOIX-MAX             PIC 99 VALUE 3.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  COMPTEUR              PIC 99.
       01  HELP-1                PIC 9(4) COMP-1.
       01  RAP-NAME.
           02 PRECISION          PIC X(6) VALUE "S-RAPP".
           02 FILLER             PIC X VALUE ".".
           02 ANNEE-RAPPORT      PIC 999.

       01  DATE-COMPTA.
           04 D-COMPTA-A      PIC 9999.
           04 D-COMPTA-M      PIC 99.
           04 D-COMPTA-J      PIC 99.

       01  DATE-TRANS.
           04 D-TRANS-A      PIC 9999.
           04 D-TRANS-M      PIC 99.
           04 D-TRANS-J      PIC 99.

       01  DATE-MAJ.
           04 D-MAJ-A      PIC 9999.
           04 D-MAJ-M      PIC 99.
           04 D-MAJ-J      PIC 99.

       01  ECR-DISPLAY.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON RAPPORT.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-5-RPDRAP.

           MOVE LNK-SUFFIX TO ANNEE-RAPPORT.

           OPEN I-O RAPPORT.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1 THRU 2 MOVE 0000000059 TO EXC-KFR (1)
           WHEN 3     MOVE 0000000025 TO EXC-KFR (1)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-DATE
           WHEN  2 PERFORM APRES-DATE
           WHEN  3 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           MOVE 10320000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-TRANS
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO D-TRANS-A
              MOVE TODAY-MOIS  TO D-TRANS-M
              MOVE TODAY-JOUR  TO D-TRANS-J
              MOVE 0 TO LNK-NUM.

       AVANT-2.
           MOVE 12320000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-MAJ
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO D-MAJ-A
              MOVE TODAY-MOIS  TO D-MAJ-M
              MOVE TODAY-JOUR  TO D-MAJ-J
              MOVE 0 TO LNK-NUM.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DATE.
           IF LNK-NUM NOT = 0
              MOVE "AA" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM RAPPORT THRU RAPPORT-END
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.


       RAPPORT.
           IF  D-TRANS-A  = 0
           AND D-MAJ-A    = 0
               PERFORM END-PROGRAM.
           INITIALIZE RAP-RECORD.
           START RAPPORT KEY >= RAP-KEY INVALID 
                GO RAPPORT-END.
       RAPPORT-1.
           READ RAPPORT NEXT AT END 
                GO RAPPORT-END.

           IF  RAP-EDIT-A = D-TRANS-A
           AND RAP-EDIT-M = D-TRANS-M
           AND RAP-EDIT-J = D-TRANS-J
               INITIALIZE RAP-DATE-EDITION
               PERFORM WRITE-RAPPORT.
           IF  RAP-ST-ANNEE = D-MAJ-A
           AND RAP-ST-MOIS  = D-MAJ-M
           AND RAP-ST-JOUR  = D-MAJ-J
           AND D-MAJ-A NOT = 0
               DELETE RAPPORT INVALID CONTINUE.
           GO RAPPORT-1.
       RAPPORT-END.
           CLOSE RAPPORT.
           PERFORM END-PROGRAM.

       WRITE-RAPPORT.
           REWRITE RAP-RECORD INVALID CONTINUE.

       DIS-HE-01.
           MOVE D-TRANS-A TO HE-AA.
           MOVE D-TRANS-M TO HE-MM.
           MOVE D-TRANS-J TO HE-JJ.
           DISPLAY HE-DATE LINE 10 POSITION 32.

       DIS-HE-02.
           MOVE D-MAJ-A TO HE-AA.
           MOVE D-MAJ-M TO HE-MM.
           MOVE D-MAJ-J TO HE-JJ.
           DISPLAY HE-DATE LINE 12 POSITION 32.

       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE  437 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 4.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *-----------------------------
      *    Routines standard: clause copies
      *-----------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

