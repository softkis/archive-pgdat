      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-CS MODULE GENERAL LECTURE CSDET           �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-CS.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CSDET.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CSDET.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  ACTION   PIC X.
       01  DEL-KEY  PIC 9(4) COMP-1 VALUE 98.
       01  WR-KEY   PIC 9(4) COMP-1 VALUE 99.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "CSDET.LNK".
           COPY "CSDEF.REC".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD CD-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CSDET.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-CS.
       
           IF NOT-OPEN = 0
              OPEN I-O CSDET
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CS-RECORD.
           IF CD-NUMBER > 0
              MOVE CD-NUMBER TO CS-NUMBER
           ELSE
              MOVE CS-NUMBER TO CD-NUMBER.

           IF CS-ANNEE = 0
              MOVE LNK-ANNEE TO CS-ANNEE
              MOVE LNK-MOIS  TO CS-MOIS
           END-IF.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 99
              PERFORM WRITE-CS
      *       IF LNK-SQL = "Y" 
      *          CALL "9-CSDET" USING LINK-V CS-RECORD WR-KEY 
      *       END-IF 
              EXIT PROGRAM
           END-IF.

           IF EXC-KEY = 98
      *       IF LNK-SQL = "Y" 
      *          CALL "9-CSDET" USING LINK-V CS-RECORD DEL-KEY 
      *       END-IF 
              DELETE CSDET INVALID CONTINUE END-DELETE
              EXIT PROGRAM
           END-IF.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ CSDET PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ CSDET NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 0 PERFORM START-3
               READ CSDET PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ CSDET NO LOCK INVALID GO EXIT-1 END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF CD-NUMBER NOT = CS-NUMBER
              INITIALIZE LINK-RECORD
           ELSE
              MOVE CS-RECORD TO LINK-RECORD
           END-IF.
           EXIT PROGRAM.

       START-1.
           START CSDET KEY < CS-KEY INVALID GO EXIT-1.
       START-2.
           START CSDET KEY > CS-KEY INVALID GO EXIT-1.
       START-3.
           START CSDET KEY <= CS-KEY INVALID GO EXIT-1.

       WRITE-CS.
      *    IF LNK-SQL = "Y" 
      *       CALL "9-CSDET" USING LINK-V CS-RECORD EXC-KEY 
      *    END-IF.
           WRITE CS-RECORD INVALID REWRITE CS-RECORD END-WRITE.

           COPY "XMESSAGE.CPY".
