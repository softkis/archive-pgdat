      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME U-MUT MODIF PLAN CIS          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    U-MUT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PLAN.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴
           COPY "PLAN.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

           COPY "LIBELLE.REC".
           COPY "V-VAR.CPY".

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PLAN.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.

       END DECLARATIVES.

       START-DISPLAY SECTION.

       START-PROGRAMME-U-MUT.
       
           OPEN I-O PLAN.
           INITIALIZE PLAN-RECORD LIB-RECORD.
           START PLAN KEY >= PLAN-KEY INVALID GO PLAN-END.
           PERFORM PLAN THRU PLAN-END.

           MOVE 112 TO LIB-NUMBER.
           MOVE "F" TO LIB-LANGUAGE.
           MOVE "Esp둩es " TO LIB-DESCRIPTION.
           PERFORM WRITE-LIB.
           MOVE "D" TO LIB-LANGUAGE.
           MOVE "Krankengeld" TO LIB-DESCRIPTION.
           PERFORM WRITE-LIB.
           MOVE "E" TO LIB-LANGUAGE.
           MOVE "Sickpay" TO LIB-DESCRIPTION.
           PERFORM WRITE-LIB.
           MOVE 113 TO LIB-NUMBER.
           MOVE "F" TO LIB-LANGUAGE.
           MOVE "Mutualit�" TO LIB-DESCRIPTION.
           PERFORM WRITE-LIB.
           MOVE 114 TO LIB-NUMBER.
           MOVE "F" TO LIB-LANGUAGE.
           MOVE "Surprime" TO LIB-DESCRIPTION.
           PERFORM WRITE-LIB.

           EXIT PROGRAM.

        WRITE-LIB.
           CALL "6-LIB" USING LINK-V LIB-RECORD WR-KEY.

       PLAN.
           READ PLAN NEXT NO LOCK AT END GO PLAN-END.
           PERFORM WRITE-PLAN.
           GO PLAN.
       PLAN-END.
           EXIT.

       WRITE-PLAN.
           MOVE 112 TO PLAN-LIB(166) 
                       PLAN-LIB(176) 
                       PLAN-LIB(216) 
                       PLAN-LIB(226) 
                       PLAN-LIB(236) 
                       PLAN-LIB(246) 
           MOVE 113 TO PLAN-LIB(167) 
                       PLAN-LIB(177) 
                       PLAN-LIB(217) 
                       PLAN-LIB(227) 
           MOVE 114 TO PLAN-LIB(237) 
                       PLAN-LIB(247) 
           WRITE PLAN-RECORD INVALID REWRITE PLAN-RECORD.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XACTION.CPY".

