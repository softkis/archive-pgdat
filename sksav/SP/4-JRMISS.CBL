      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � 4-JRMISS DEFINITION JOURS DE PRESENCE MISSION            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  4-JRMISS.

       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.

       01  ACTION   PIC 99.
       01  IDX      PIC 99.
       01  MOIS     PIC 99.
       01  IDX-1    PIC 99.
       01  IDX-2    PIC 99.

       01  MOIS-DEB PIC 99.
       01  MOIS-FIN PIC 99.
       01  JOUR-DEB PIC 99.
       01  JOUR-FIN PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "MISSION.REC".
           COPY "PRESMISS.REC".


       PROCEDURE DIVISION USING LINK-V MISS-RECORD PRESMISS.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-JRMISS .
       
           INITIALIZE  PRESMISS.
           IF  MISS-FIN-A > 0
           AND MISS-FIN-A < LNK-ANNEE
              EXIT PROGRAM
           END-IF.
           IF MISS-DEBUT-A > LNK-ANNEE
              EXIT PROGRAM
           END-IF.
           MOVE MISS-DEBUT-M TO MOIS-DEB.
           MOVE MISS-FIN-M   TO MOIS-FIN.
           MOVE MISS-DEBUT-J TO JOUR-DEB.
           MOVE MISS-FIN-J   TO JOUR-FIN.

           IF MISS-FIN-A = 0
           OR MISS-FIN-A > LNK-ANNEE
              MOVE 12 TO MOIS-FIN 
              MOVE 31 TO JOUR-FIN
           END-IF.
              
           IF MISS-DEBUT-A < LNK-ANNEE
              MOVE 1 TO MOIS-DEB JOUR-DEB
           END-IF.
           PERFORM FILL-MOIS VARYING MOIS FROM MOIS-DEB BY 1
              UNTIL MOIS > MOIS-FIN.
           EXIT PROGRAM.

       FILL-MOIS.
           MOVE 1 TO IDX-1
           MOVE MOIS-JRS(MOIS) TO IDX-2
           IF MOIS = MOIS-DEB 
              MOVE JOUR-DEB TO IDX-1
           END-IF.  
           IF MOIS = MOIS-FIN 
              MOVE JOUR-FIN TO IDX-2 
           END-IF.  
           MOVE IDX-1 TO PRM-DEBUT(MOIS).
           MOVE IDX-2 TO PRM-FIN(MOIS).
           PERFORM JOURS VARYING IDX FROM IDX-1 BY 1 UNTIL IDX > IDX-2.

       JOURS.
           ADD 1 TO PRM-JOUR(MOIS, IDX) PRM-TOT(MOIS) PRM-ANNEE.



