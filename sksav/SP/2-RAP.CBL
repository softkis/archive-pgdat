      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-RAP CONTROLE + MODIFICATION RAPPORTS      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  2-RAP .

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "RAPPORT.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "RAPPORT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  PRECISION             PIC 9 VALUE 0.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "FIRME.REC".
           COPY "V-VAR.CPY".

       01  RAP-IDX               PIC 99 COMP-1.
       01  HELP-1                PIC 9(6).
       01  SAVE-FIRME            PIC 9(6).

       01  RAP-NAME.
           02 FILLER             PIC X(7) VALUE "S-RAPP.".
           02 ANNEE-RAP          PIC 999.

       01  SUITE-IDX             PIC 99 VALUE 2.
       01  INTERMEDIATE          PIC 999V99.
       01  ARROW                 PIC X VALUE ">".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z3 PIC Z(3) BLANK WHEN ZERO.
           02 HE-Z3Z2 PIC ZZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z4N PIC -Z(3) BLANK WHEN ZERO.
           02 HE-Z4Z2 PIC ZZZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZ.

       01 HE-RAP.
          02 H-V OCCURS 17.
             03 HE-RAP-SUITE              PIC 99.
             03 HE-RAP-MOIS               PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON RAPPORT.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-RAP.
       
           CALL "0-TODAY" USING TODAY.
           CANCEL "4-RAP".
           INITIALIZE FIRME-RECORD.
       
           MOVE FR-KEY  TO SAVE-FIRME.
           MOVE LNK-SUFFIX TO ANNEE-RAP.
           OPEN I-O   RAPPORT.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN OTHER MOVE 0000002000 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2).
           IF INDICE-ZONE = CHOIX-MAX
                      MOVE 0052000000 TO EXC-KFR (11).


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  OTHER PERFORM AVANT-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  OTHER PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           IF FIRME-KEY = 0
              MOVE FR-KEY TO FIRME-KEY.
           ACCEPT FIRME-KEY 
             LINE 3 POSITION 15 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 
                PERFORM CHANGE-MOIS
             MOVE 27 TO EXC-KEY
           END-EVALUATE.

       AVANT-2.
           ACCEPT FIRME-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
           COMPUTE RAP-IDX = INDICE-ZONE - 2.
           COMPUTE LIN-IDX = RAP-IDX + 6.
           MOVE HE-RAP-SUITE(RAP-IDX) TO SUITE-IDX.
           MOVE ">" TO ARROW.
           ACCEPT ARROW
             LINE  LIN-IDX POSITION 3 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           DISPLAY SPACES LINE LIN-IDX POSITION 3 SIZE 1.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   CALL "2-FIRME" USING LINK-V FIRME-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  OTHER PERFORM NEXT-FIRME.
           PERFORM AFFICHAGE-DETAIL.
           IF INPUT-ERROR = 0 
              AND EXC-KEY NOT = 53
              PERFORM TOTAL-RAP THRU TOTAL-RAP-END.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              IF  RAP-IDX = 0 
              AND FIRME-KEY NOT = 0
                 GO APRES-1 
              END-IF.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-FIRME
           END-EVALUATE.                     
           PERFORM DIS-HE-01.
           PERFORM TOTAL-RAP THRU TOTAL-RAP-END.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              IF  RAP-IDX = 0 
              AND FIRME-KEY NOT = 0
                 GO APRES-2
              END-IF.
           
       APRES-DEC.
           MOVE FR-KEY   TO RAP-FIRME.
           MOVE HE-RAP-SUITE(RAP-IDX) TO RAP-SUITE.
           MOVE HE-RAP-MOIS(RAP-IDX)  TO RAP-MOIS.
           EVALUATE EXC-KEY 
            WHEN  4 READ RAPPORT INVALID CONTINUE
                    NOT INVALID
                        INITIALIZE RAP-DATE-EDITION
                        WRITE RAP-RECORD INVALID REWRITE RAP-RECORD
                        END-WRITE   
                    END-READ
                    MOVE 2 TO DECISION
                    PERFORM TOTAL-RAP THRU TOTAL-RAP-END
            WHEN  8 DELETE RAPPORT INVALID CONTINUE END-DELETE
                    MOVE 2 TO DECISION
                    PERFORM TOTAL-RAP THRU TOTAL-RAP-END
             END-EVALUATE.


       NEXT-FIRME.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 3
              MOVE "A" TO A-N.
           MOVE FIRME-RECORD TO FR-RECORD.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.
           MOVE FR-RECORD TO FIRME-RECORD.


      *    HISTORIQUE RAPPORTS
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       TOTAL-RAP.
           MOVE 0 TO RAP-IDX.
           MOVE 6 TO LIN-IDX.
           INITIALIZE RAP-RECORD RAP-IDX.
           MOVE FR-KEY    TO RAP-FIRME.
           MOVE LNK-MOIS  TO RAP-MOIS.
           IF MENU-PROG-NUMBER > 0
              MOVE 1 TO RAP-MOIS.
           START RAPPORT KEY >= RAP-KEY INVALID CONTINUE
           NOT INVALID PERFORM READ-RAP THRU READ-RAP-END.
           COMPUTE CHOIX-MAX = 2 + RAP-IDX.
       TOTAL-RAP-END.

       READ-RAP.
           READ RAPPORT NEXT AT END 
              GO READ-RAP-END.
           IF FR-KEY     NOT = RAP-FIRME
           OR RAP-MOIS > LNK-MOIS
              GO READ-RAP-END.
           ADD 1 TO RAP-IDX.
           MOVE RAP-SUITE TO HE-RAP-SUITE(RAP-IDX).
           MOVE RAP-MOIS  TO HE-RAP-MOIS(RAP-IDX).
           PERFORM DIS-HIS-LIGNE.
           IF LIN-IDX > 22
              GO READ-RAP-END.
           GO READ-RAP.
       READ-RAP-END.
           ADD 1 TO LIN-IDX .
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 24.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       DIS-HIS-LIGNE.
           ADD 1 TO LIN-IDX.
           MOVE RAP-SUITE TO HE-Z2.
           DISPLAY HE-Z2 LINE  LIN-IDX POSITION 3 LOW.
           MOVE RAP-MOIS TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 1.
           PERFORM RAPS VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.
           MOVE RAP-EDIT-A TO HE-AA.
           MOVE RAP-EDIT-M TO HE-MM.
           MOVE RAP-EDIT-J TO HE-JJ.
           DISPLAY HE-DATE LINE LIN-IDX POSITION 70 LOW.
           MOVE RAP-ST-ANNEE TO HE-AA.
           MOVE RAP-ST-MOIS  TO HE-MM.
           MOVE RAP-ST-JOUR  TO HE-JJ.
           DISPLAY HE-DATE LINE LIN-IDX POSITION 76.
       
       RAPS.
           COMPUTE COL-IDX = IDX * 3 + 4.
           MOVE RAP-X(IDX) TO HE-Z3 HE-Z4N.
           DISPLAY HE-Z3 LINE LIN-IDX POSITION COL-IDX.
           IF RAP-X(IDX) < 0
              DISPLAY HE-Z3 LINE LIN-IDX POSITION COL-IDX REVERSE.

       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS 
             WHEN 58 PERFORM ADD-MOIS 
           END-EVALUATE.
           PERFORM AFFICHAGE-ECRAN.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-DETAIL.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0 
              MOVE 1 TO LNK-MOIS.

       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
              MOVE 12 TO LNK-MOIS.


       DIS-HE-01.
           MOVE FIRME-KEY  TO HE-Z4.
           DISPLAY HE-Z4 LINE 3 POSITION 15.
           DISPLAY FIRME-NOM LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY FIRME-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-END.
           EXIT.



       AFFICHAGE-ECRAN.
           MOVE 2435 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE RAPPORT.
           MOVE SAVE-FIRME TO FR-KEY.
           CALL "6-FIRME" USING LINK-V "N" FAKE-KEY.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

