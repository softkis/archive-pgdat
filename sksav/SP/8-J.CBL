      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-J EXPLOITATION COMPTA -> RUBRIQUES        �
      *  � SPECIAL TEXTILCORD INCLURE RUBRIQUES VIDES MENU = 1   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-J.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURNAL.FC".
           COPY "INTERCOM.FC".
      *    Fichier interm괺iaire COMPTES
           SELECT OPTIONAL ASSORT ASSIGN TO RANDOM, ASSORT-NAME,
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is ASSORT-KEY,
                  STATUS FS-HELP.

           SELECT OPTIONAL TF-COMPTA ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "JOURNAL.FDE".

       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire JOURNAL COMPTABLE

       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-TOT1   PIC 9.
              03 INTER-MOIS   PIC 99.
              03 INTER-COUT   PIC 9(8).
              03 INTER-TOT2   PIC 9.
              03 INTER-STAT   PIC 99.
              03 INTER-SUITE  PIC 99.
              03 INTER-PERS   PIC 9(8).
              03 INTER-POSTE  PIC 9(10).
              03 INTER-EXT    PIC X(50).
              03 INTER-DC     PIC 9.
              03 INTER-COMPTE PIC X(30).

           02 INTER-REC-DET.
              03 INTER-VALUE PIC S9(8)V99.
              03 INTER-UNITE PIC S9(8)V99.
              03 INTER-PERIODE PIC 99.

       FD  ASSORT 
           LABEL RECORD STANDARD
           DATA RECORD ASSORT-RECORD.
      *    Fichier interm괺iaire JOURNAL COMPTABLE

       01  ASSORT-RECORD.
           02 ASSORT-KEY.
              03 ASSORT-DC     PIC 9.
              03 ASSORT-COMPTE PIC X(30).

           02 ASSORT-REC-DET.
              03 ASSORT-VALUE PIC 999.

       FD  TF-COMPTA
           RECORD VARYING DEPENDING ON IDX-4
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 2356 DEPENDING IDX-4.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 12.
       01  PRECISION             PIC 9 VALUE 1.
       01  TIRET-TEXTE           PIC X VALUE ".".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "MESSAGE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "PARMOD.REC".
           COPY "POCL.REC".
           COPY "PLAN.REC".
           COPY "COMPTE.REC".
           COPY "PAREX.REC".


       01  PROC-IDX        PIC 9 VALUE 0.
       01  DC-IDX          PIC 9.
       01  TESTP           PIC 99999 VALUE 0.
       01  LIGNES          PIC 9999 VALUE 0.
       01  FINALE          PIC 9 VALUE 0.
       01  POST            PIC 9(8) VALUE 0.
       01  VH-00           PIC S9(8)V99.
       01  NOT-OPEN        PIC 9 VALUE 0.
       01  DATE-MAJ.
           02 D-MAJ-A         PIC 9999.
           02 D-MAJ-M         PIC 99.
           02 D-MAJ-J         PIC 99.

       01 HE-SEL.
          02 H-S PIC X       OCCURS 20.
       01 HE-PLAN.
          02 H-P PIC 9       OCCURS 10000.
           
       01  JOURNAL-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-JO".
           02 FIRME-JOURNAL      PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 ANNEE-JOURNAL      PIC 999.

       01  INTER-NAME.
           02 FILLER             PIC XXXX VALUE "IRUB".
           02 FIRME-INTER        PIC 9999.

       01  RESULT-RECORD.
           03 RESULT-FIRME  PIC Z(10).
           03 RESULT-FILL-1 PIC X.
           03 RESULT-MOIS   PIC Z(5).
           03 RESULT-MOIS-T REDEFINES RESULT-MOIS   PIC X(5).
17         03 RESULT-FILL-2 PIC X.
           03 RESULT-SUITE  PIC Z(5).
           03 RESULT-FILL-3 PIC X.
29         03 RESULT-STAT   PIC Z(6).
           03 RESULT-STAT-T REDEFINES RESULT-STAT PIC X(6).
           03 RESULT-FILL-4 PIC X.
46         03 RESULT-COUT   PIC Z(16).
           03 RESULT-COUT-T REDEFINES RESULT-COUT PIC X(16).
           03 RESULT-FILL-5 PIC X.
           03 RESULT-NOM-C  PIC X(20).
           03 RESULT-FILL-6 PIC X.
78         03 RESULT-PERS   PIC Z(10).
           03 RESULT-PERS-T REDEFINES RESULT-PERS PIC X(10).
           03 RESULT-FILL-7 PIC X.
           03 RESULT-NOM    PIC X(25).
105        03 RESULT-FILL-8 PIC X.
121        03 RESULT-POSTE  PIC X(22).
           03 RESULT-POSTE-T REDEFINES RESULT-POSTE.
              04 RESULT-POSTE-N   PIC Z(8).
              04 RESULT-POSTE-F   PIC X(2).
              04 RESULT-POSTE-NOM PIC X(12).
122        03 RESULT-FILL-9 PIC X.
           03 RESULT-VALUES OCCURS 100.
              04 RESULT-LINE      PIC X(16).
              04 RESULT-VALS   REDEFINES RESULT-LINE.
                 05 RESULT-VAL PIC Z(12),ZZ-.
              04 RESULT-HEADER REDEFINES RESULT-LINE.
                 05 RESULT-DC     PIC XX.
                 05 RESULT-COMPTE PIC X(14).
              04 RESULT-FILLER PIC X.


       01  ASSORT-NAME.
           02 FILLER             PIC XXXX VALUE "IAS2".
           02 FIRME-ASSORT        PIC 9999.

       01  CUMUL.
           02 CUMUL-D            PIC S9(8)V99.
           02 CUMUL-C            PIC S9(8)V99.
       01  CUMUL-R REDEFINES CUMUL.
           02 CUMUL-DC           PIC S9(8)V99 OCCURS 2.

           COPY "V-VAR.CPY".
        
       01  COMPTEUR              PIC 99 COMP-1.
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  MISE-A-JOUR           PIC X VALUE "N".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.
       01  MOIS-SAVE             PIC 99.
       01  EXIST-POSTE           PIC 9(10) VALUE 0.

       01  TESTS.
           02 TEST-MOIS       PIC 99 VALUE 0.
           02 TEST-STAT       PIC 99 VALUE 0.
           02 TEST-SUITE      PIC 99 VALUE 0.
           02 TEST-COUT       PIC 9(8) VALUE 0.
           02 TEST-POSTE      PIC 9(10) VALUE 0.
           02 TEST-EXT        PIC X(50) VALUE SPACES.
           02 TEST-PERS       PIC 9(8) VALUE 0.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               JOURNAL 
               TF-COMPTA
               ASSORT
               INTER.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-J.
       
           INITIALIZE PC-RECORD.
           MOVE 66 TO SAVE-KEY.
           PERFORM NEXT-POSTE.
           MOVE PC-NUMBER TO EXIST-POSTE.
           MOVE 0 TO SAVE-KEY.
           INITIALIZE PARMOD-RECORD PLAN-RECORD COUT-RECORD HE-PLAN.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           CALL "6-PAREX" USING LINK-V PAREX-RECORD.
           MOVE PARMOD-SETTINGS TO HE-SEL.
           MOVE LNK-SUFFIX TO ANNEE-JOURNAL.
           MOVE FR-KEY  TO FIRME-JOURNAL 
                           FIRME-INTER 
                           FIRME-ASSORT 
                           PARMOD-FIRME.
           IF MENU-BATCH > 0
              MOVE "S-JM" TO IDENTITE.
      *    1 = CCM
      *    2 = CCM + POSTES DE LA CARRIERE

           OPEN I-O   JOURNAL.
           DELETE FILE INTER.
           DELETE FILE ASSORT.
           OPEN I-O INTER WITH LOCK.
           OPEN I-O ASSORT WITH LOCK.
           MOVE 1 TO NOT-OPEN.
           IF MENU-PROG-NUMBER = 1
              PERFORM SORTER THRU SORTER-END.

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-MOIS TO MOIS-SAVE MOIS-FIN.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8     MOVE 0000000034 TO EXC-KFR(1)
           WHEN 12    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR (14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-10
           WHEN 11 PERFORM AVANT-PATH
           WHEN 12 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-DATE
           WHEN  9 PERFORM APRES-9
           WHEN 10 PERFORM APRES-10
           WHEN 12 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-6.
           IF REG-PERSON NOT = END-NUMBER
           AND EXIST-POSTE > 0
           ACCEPT POST
           LINE 13 POSITION 28 SIZE 8
           TAB UPDATE NO BEEP CURSOR  1 
           ON EXCEPTION EXC-KEY CONTINUE
           ELSE 
             MOVE 0 TO POST
           END-IF.

       AVANT-7.
           IF POST = 0
           ACCEPT MISE-A-JOUR 
              LINE 15 POSITION 35 SIZE 1
              TAB UPDATE NO BEEP CURSOR 1
              CONTROL "UPPER"
              ON EXCEPTION EXC-KEY CONTINUE
           ELSE 
              MOVE "N" TO MISE-A-JOUR 
           END-IF.

       AVANT-8.
           MOVE 17260000 TO LNK-POSITION.
           IF MISE-A-JOUR = "N"
           CALL "0-GDATE" USING DATE-MAJ
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY
           ELSE
              INITIALIZE DATE-MAJ
           END-IF.
           IF EXC-KEY = 5
              MOVE TODAY-ANNEE TO D-MAJ-A
              MOVE TODAY-MOIS  TO D-MAJ-M
              MOVE TODAY-JOUR  TO D-MAJ-J
              MOVE 0 TO LNK-NUM.

       AVANT-9.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-10.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 22 POSITION 40 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-6.
           MOVE POST TO PC-NUMBER.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 5 CALL "1-POCL" USING LINK-V
                  MOVE LNK-VAL TO PC-NUMBER
                  CANCEL "1-POCL"
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
           WHEN  2 THRU 3 CALL "2-POCL" USING LINK-V PC-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-POSTE
           END-EVALUATE.
           IF PC-NUMBER > 0
              MOVE PC-NUMBER TO POST
           END-IF.
           PERFORM DIS-HE-06.

       APRES-7.
           IF MISE-A-JOUR = "J"
           OR MISE-A-JOUR = "O"
           OR MISE-A-JOUR = "Y"
           OR MISE-A-JOUR = "N" 
              CONTINUE
           ELSE
              MOVE "N" TO MISE-A-JOUR
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-05.
           IF  MISE-A-JOUR NOT = "N"
           AND MENU-EXTENSION-2 NOT = SPACES
              PERFORM AVANT-PATH
           END-IF.

       APRES-DATE.
           IF LNK-NUM NOT = 0
              MOVE "AA" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-06.

       APRES-9.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.

       APRES-10.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
      *    IF MOIS-FIN > LNK-MOIS
      *       MOVE LNK-MOIS TO MOIS-FIN
      *       MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           WHEN 68 MOVE "AY" TO LNK-AREA
                  CALL "5-SEL" USING LINK-V PARMOD-RECORD
                  MOVE PARMOD-SETTINGS TO HE-SEL
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM DISPLAY-F-KEYS
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TRAITEMENT.
           MOVE MOIS-COURANT TO LNK-MOIS.
           INITIALIZE CUMUL.
           IF REG-PERSON = END-NUMBER
      *       apr둺 effacement !
              CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY
              MOVE END-NUMBER TO REG-PERSON
              PERFORM CUMUL-JOURNAL
              PERFORM READ-EXIT
           ELSE
              PERFORM START-PERSON
              PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-1
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.

           PERFORM CUMUL-JOURNAL.

       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.
           PERFORM FULL-PROCESS.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.

       CUMUL-VALEURS.
           MOVE 1 TO DC-IDX.
           PERFORM PROC VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 200.
           MOVE 2 TO DC-IDX.
           PERFORM PROC VARYING IDX-1 FROM 201 BY 1 UNTIL IDX-1 > 300.

       PROC.
           IF JRL-V(IDX-1) NOT =  0
              MOVE JRL-V(IDX-1) TO VH-00
              PERFORM WRITE-INTER
           END-IF.

       WRITE-INTER.
           INITIALIZE INTER-RECORD ASSORT-RECORD.
           MOVE DC-IDX TO INTER-DC ASSORT-DC.
           MOVE PLAN-COM(IDX-1) TO INTER-COMPTE ASSORT-COMPTE.
           IF H-S(1) NOT = "N"
              MOVE JRL-COUT  TO INTER-COUT.
           IF H-S(2) NOT = "N"
              MOVE JRL-STAT  TO INTER-STAT.
           IF H-S(3) NOT = "N"
           AND PAREX-PRINT(IDX-1) > 0
              MOVE JRL-POSTE TO INTER-POSTE.
           IF H-S(3) NOT = "N"
           AND MENU-BATCH = 2
              MOVE CAR-POSTE-FRAIS TO INTER-POSTE.
           IF H-S(4) NOT = "N"
              MOVE JRL-EXTENSION TO INTER-EXT.
           IF H-S(5) NOT = "N"
              MOVE JRL-PERS TO INTER-PERS.
           IF H-S(6) NOT = "N"
              MOVE JRL-MOIS TO INTER-MOIS.
           IF H-S(7) NOT = "N"
              MOVE JRL-SUITE TO INTER-SUITE.
           MOVE 0 TO SH-00.
           IF IDX-1 < 101
              MOVE JRL-U(IDX-1) TO SH-00
           END-IF.
           PERFORM WRITE-REC-INTER.
           MOVE 0 TO SH-00.
        
       WRITE-REC-INTER.
           READ INTER INVALID MOVE 0 TO INTER-VALUE INTER-UNITE.
           ADD VH-00 TO INTER-VALUE.
           ADD SH-00 TO INTER-UNITE.
           MOVE MOIS-SAVE TO INTER-PERIODE.
           IF INTER-COMPTE NOT = SPACES
              WRITE INTER-RECORD INVALID REWRITE INTER-RECORD END-WRITE.
           IF ASSORT-COMPTE NOT = SPACES
              WRITE ASSORT-RECORD INVALID CONTINUE.

           MOVE 0 TO INTER-POSTE INTER-PERS INTER-EXT INTER-STAT.
           MOVE 1 TO INTER-TOT2.
           READ INTER INVALID MOVE 0 TO INTER-VALUE.
           ADD VH-00 TO INTER-VALUE.
           ADD SH-00 TO INTER-UNITE.
           WRITE INTER-RECORD  INVALID REWRITE INTER-RECORD END-WRITE.

           MOVE 0 TO INTER-MOIS INTER-COUT INTER-TOT2.
           MOVE 1 TO INTER-TOT1.
           READ INTER INVALID MOVE 0 TO INTER-VALUE.
           ADD VH-00 TO INTER-VALUE.
           ADD SH-00 TO INTER-UNITE.
           WRITE INTER-RECORD  INVALID REWRITE INTER-RECORD END-WRITE.
           INITIALIZE INTER-RECORD.

       CUMUL-JOURNAL.
           INITIALIZE JRL-RECORD.
           MOVE REG-PERSON TO JRL-PERS.
           MOVE MOIS-DEBUT TO JRL-MOIS.
           MOVE 0 TO NOT-FOUND.
           START JOURNAL KEY > JRL-KEY INVALID CONTINUE
              NOT INVALID
              PERFORM LECTURE-JOURNAL THRU LECTURE-JOURNAL-END.

       LECTURE-JOURNAL.
           READ JOURNAL NEXT AT END GO LECTURE-JOURNAL-END.
           IF  JRL-ANNEE-C > 0 
           AND MISE-A-JOUR NOT = "N"
              GO LECTURE-JOURNAL
           END-IF.
           IF REG-PERSON NOT = JRL-PERS
              GO LECTURE-JOURNAL-END.
           IF JRL-MOIS > MOIS-FIN
              GO LECTURE-JOURNAL-END.
           IF STATUT > 0 
           AND STATUT NOT = JRL-STAT  
              GO LECTURE-JOURNAL
           END-IF.
           IF COUT > 0 
           AND COUT NOT = JRL-COUT  
              GO LECTURE-JOURNAL
           END-IF.
           IF POST > 0 
           AND POST NOT = JRL-POSTE 
              GO LECTURE-JOURNAL
           END-IF.
           IF D-MAJ-A > 0
           AND DATE-MAJ NOT = JRL-DATE-COMPTA
              GO LECTURE-JOURNAL
           END-IF.

           MOVE JRL-MOIS TO LNK-MOIS.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           PERFORM NEXT-COUT.
           PERFORM READ-PLAN.
           PERFORM CUMUL-VALEURS.
           MOVE JRL-POSTE TO PC-NUMBER.
           PERFORM NEXT-POSTE.
           PERFORM DIS-HE-01.
           IF MISE-A-JOUR NOT = "N"
              MOVE TODAY-ANNEE TO JRL-ANNEE-C
              MOVE TODAY-MOIS  TO JRL-MOIS-C
              MOVE TODAY-JOUR  TO JRL-JOUR-C
              WRITE JRL-RECORD INVALID REWRITE JRL-RECORD END-WRITE.
           GO LECTURE-JOURNAL.
       LECTURE-JOURNAL-END.
           EXIT.

       FULL-PROCESS.
           PERFORM RENUMBER.
           INITIALIZE INTER-RECORD COUT TESTS.
           START INTER KEY > INTER-KEY INVALID CONTINUE
            NOT INVALID
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END GO READ-INTER-END.
           IF LIGNES = 0
              PERFORM HEADER
           END-IF.
           ADD 1 TO LIGNES.
           IF INTER-TOT1 = 1
              IF FINALE = 0
                 PERFORM RUPTURE
                 MOVE 1 TO FINALE LIGNES
                 MOVE "TOTAL" TO RESULT-COUT-T
              END-IF
           ELSE
              IF INTER-TOT2 = 1
              AND TEST-PERS NOT = 0
                 PERFORM LOAD-TESTS
                 PERFORM RUPTURE
              ELSE
                 PERFORM TEST-RUPTURE
                 PERFORM LOAD-TESTS
              END-IF
           END-IF.
           MOVE INTER-DC TO ASSORT-DC.
           MOVE INTER-COMPTE TO ASSORT-COMPTE.
           READ ASSORT INVALID CONTINUE.
           MOVE INTER-VALUE TO RESULT-VAL(ASSORT-VALUE).
           GO READ-INTER.
       READ-INTER-END.
           IF FINALE > 0
              PERFORM RUPTURE.

       RENUMBER.
           INITIALIZE ASSORT-RECORD IDX-1 RESULT-RECORD.
           START ASSORT KEY > ASSORT-KEY INVALID CONTINUE
            NOT INVALID
           PERFORM READ-ASSORT THRU READ-ASSORT-END.

       READ-ASSORT.
           READ ASSORT NEXT AT END GO READ-ASSORT-END.
           ADD 1 TO IDX-1.
           MOVE IDX-1 TO ASSORT-VALUE.
           REWRITE ASSORT-RECORD INVALID CONTINUE.
           MOVE ASSORT-COMPTE TO RESULT-COMPTE(IDX-1).
           IF ASSORT-DC = 1
              MOVE "D" TO RESULT-DC(IDX-1)
           ELSE
              MOVE "C" TO RESULT-DC(IDX-1)
           END-IF.
           MOVE ";" TO RESULT-FILLER(IDX-1)
           GO READ-ASSORT.
       READ-ASSORT-END.
           IF IDX-1 > 0
              COMPUTE IDX-4 = 128 + IDX-1 * 17
              OPEN OUTPUT TF-COMPTA
              MOVE ";" TO   RESULT-FILL-1 RESULT-FILL-2 
              RESULT-FILL-3 RESULT-FILL-4 RESULT-FILL-5
              RESULT-FILL-6 RESULT-FILL-7 RESULT-FILL-8 RESULT-FILL-9
              PERFORM VIDE
              WRITE TF-RECORD FROM RESULT-RECORD
              PERFORM COMPTES VARYING IDX FROM 1 BY 1 UNTIL IDX > IDX-1
              MOVE 104 TO MS-NUMBER
              MOVE "AA" TO LNK-AREA
              PERFORM GET-MESS
              MOVE MS-DESCRIPTION TO RESULT-MOIS-T
              MOVE 101 TO MS-NUMBER
              MOVE "SL" TO LNK-AREA
              PERFORM GET-MESS
              MOVE MS-DESCRIPTION TO RESULT-STAT-T
              MOVE 102 TO MS-NUMBER
              MOVE "SL" TO LNK-AREA
              PERFORM GET-MESS
              MOVE MS-DESCRIPTION TO RESULT-COUT-T
              MOVE 103 TO MS-NUMBER
              MOVE "SL" TO LNK-AREA
              PERFORM GET-MESS
              MOVE MS-DESCRIPTION TO RESULT-POSTE
              MOVE 106 TO MS-NUMBER
              MOVE "SL" TO LNK-AREA
              PERFORM GET-MESS
              MOVE MS-DESCRIPTION TO RESULT-PERS-T
              PERFORM VIDE
              WRITE TF-RECORD FROM RESULT-RECORD
              INITIALIZE CUMUL TESTS RESULT-RECORD
           END-IF.

       VIDE.
           IF H-S(1) = "N"
              INITIALIZE RESULT-COUT-T RESULT-FILL-5
              RESULT-NOM-C RESULT-FILL-6.
           IF H-S(2) = "N"
              INITIALIZE RESULT-STAT-T RESULT-FILL-4.
           IF H-S(3) = "N"
              INITIALIZE RESULT-POSTE-T RESULT-FILL-9.
              MOVE POST TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
           IF H-S(5) = "N"
              INITIALIZE RESULT-PERS-T RESULT-FILL-7
              RESULT-NOM RESULT-FILL-8.
           IF H-S(6) = "N"
              INITIALIZE RESULT-MOIS-T RESULT-FILL-2.
           IF H-S(7) = "N"
              INITIALIZE RESULT-SUITE RESULT-FILL-3.

       GET-MESS.
           CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY.

       COMPTES.
           MOVE LNK-LANGUAGE TO COM-LANGUAGE.
           MOVE RESULT-COMPTE(IDX) TO COM-NUMBER.
           CALL "6-COMPTE" USING LINK-V COM-RECORD FAKE-KEY.
           MOVE COM-NOM TO RESULT-LINE(IDX).

       TEST-RUPTURE.
           IF H-S(1) NOT = "N"
              IF INTER-COUT NOT = TEST-COUT 
              AND TEST-COUT > 0
                 PERFORM RUPTURE
              END-IF
           END-IF.
           IF H-S(2) NOT = "N"
              IF INTER-STAT NOT = TEST-STAT
              AND TEST-STAT > 0
                 PERFORM RUPTURE
              END-IF
           END-IF.
           IF H-S(5) NOT = "N"
              IF INTER-PERS NOT = TEST-PERS
              AND TEST-PERS > 0
                 PERFORM RUPTURE
              END-IF
           END-IF.
           IF H-S(3) NOT = "N"
              IF INTER-POSTE NOT = TEST-POSTE
              AND TEST-POSTE > 0
                 PERFORM RUPTURE
              END-IF
           END-IF.
           IF H-S(4) NOT = "N"
              IF INTER-EXT NOT = TEST-EXT
              AND TEST-EXT > 0
                 PERFORM RUPTURE
              END-IF
           END-IF.
           IF H-S(6) NOT = "N"
              IF INTER-MOIS NOT = TEST-MOIS
              AND TEST-MOIS NOT = 0
                 PERFORM RUPTURE
              END-IF
           END-IF.
           IF H-S(7) NOT = "N"
              IF INTER-SUITE NOT = TEST-SUITE
              AND TEST-SUITE NOT = 0
                 PERFORM RUPTURE
              END-IF
           END-IF.
           PERFORM LOAD-TESTS.

       RUPTURE.
           MOVE ";" TO   RESULT-FILL-1 RESULT-FILL-2 
           RESULT-FILL-3 RESULT-FILL-4 RESULT-FILL-5
           RESULT-FILL-6 RESULT-FILL-7 RESULT-FILL-8 RESULT-FILL-9.
           PERFORM LIMITE VARYING IDX FROM 1 BY 1 UNTIL IDX > IDX-1.
           PERFORM VIDE.
           WRITE TF-RECORD FROM RESULT-RECORD.
           INITIALIZE CUMUL TESTS RESULT-RECORD LIGNES.

       LIMITE.
           MOVE ";" TO RESULT-FILLER(IDX).

       HEADER.
           IF INTER-PERS NOT = 0
              MOVE INTER-PERS TO REG-PERSON RESULT-PERS
              MOVE 13 TO EXC-KEY
              MOVE "N" TO A-N
              PERFORM NEXT-REGIS
              CALL "4-PRNOM" USING LINK-V PR-RECORD
              MOVE LNK-TEXT TO RESULT-NOM
           END-IF.
           IF INTER-COUT NOT = 0
              MOVE INTER-COUT TO RESULT-COUT COUT-NUMBER
              CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY
              MOVE COUT-NOM TO RESULT-NOM-C
           ELSE
              MOVE "TOTAL"  TO RESULT-NOM-C.
           MOVE FR-KEY      TO RESULT-FIRME.
           MOVE INTER-MOIS  TO RESULT-MOIS.
           MOVE INTER-STAT  TO RESULT-STAT.
           MOVE INTER-POSTE TO RESULT-POSTE-N.
           IF H-S(3) NOT = "N"
           AND INTER-POSTE NOT = 0
              IF INTER-POSTE NOT = PC-NUMBER
                 MOVE INTER-POSTE TO PC-NUMBER
                 CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              END-IF
              MOVE PC-NOM TO RESULT-POSTE-NOM
           END-IF.

       LOAD-TESTS.
           IF H-S(1) NOT = "N"
              MOVE INTER-COUT  TO TEST-COUT.
           IF H-S(2) NOT = "N"
              MOVE INTER-STAT  TO TEST-STAT.
           IF H-S(3) NOT = "N"
              MOVE INTER-POSTE TO TEST-POSTE.
           IF H-S(4) NOT = "N"
              MOVE INTER-EXT  TO TEST-EXT.
           IF H-S(5) NOT = "N"
              MOVE INTER-PERS TO TEST-PERS.
           IF H-S(6) NOT = "N"
              MOVE INTER-MOIS TO TEST-MOIS.
           IF H-S(7) NOT = "N"
              MOVE INTER-SUITE TO TEST-SUITE.


       NEXT-POSTE.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" SAVE-KEY.

       READ-PLAN.
           MOVE JRL-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           INSPECT COUT-PLANS REPLACING ALL " " BY "0".
           MOVE COUT-PLAN(JRL-STAT) TO PLAN-NUMERO.
           CALL "6-PLAN" USING LINK-V PLAN-RECORD FAKE-KEY.


      *PREPARATION RUBRIQUES 
       SORTER.
           CALL "6-COUT" USING LINK-V COUT-RECORD NX-KEY.
           IF FR-KEY NOT = COUT-FIRME
              GO SORTER-END.
           PERFORM ASS-PLAN VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.
           GO SORTER.
       SORTER-END.
           EXIT.

       ASS-PLAN.
           MOVE COUT-PLAN(IDX) TO IDX-3 TESTP.
           IF TESTP = 0
              MOVE 10000 TO TESTP.
           IF H-P(TESTP) = 0
              PERFORM ASSORTS.
           MOVE 1 TO H-P(TESTP).

        ASSORTS.
           INITIALIZE PLAN-RECORD.
           MOVE IDX-3 TO PLAN-NUMERO.
           CALL "6-PLAN" USING LINK-V PLAN-RECORD FAKE-KEY.
           MOVE 1 TO DC-IDX.
           PERFORM IN-ASS VARYING IDX-1 FROM   1 BY 1 UNTIL IDX-1 > 200.
           MOVE 2 TO DC-IDX.
           PERFORM IN-ASS VARYING IDX-1 FROM 201 BY 1 UNTIL IDX-1 > 300.

        IN-ASS.
           INITIALIZE ASSORT-RECORD.
           MOVE DC-IDX TO ASSORT-DC.
           MOVE PLAN-COM(IDX-1) TO ASSORT-COMPTE.
           IF ASSORT-COMPTE NOT = SPACES
              WRITE ASSORT-RECORD INVALID CONTINUE.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE POST TO HE-Z8.
           DISPLAY HE-Z8 LINE 13 POSITION 28.
           IF POST NOT = 0
              MOVE POST TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
           ELSE
              INITIALIZE PC-RECORD
           END-IF.
           DISPLAY PC-NOM  LINE 13 POSITION 45.

       DIS-HE-07.
           DISPLAY MISE-A-JOUR LINE 15 POSITION 35.
       DIS-HE-08.
           MOVE D-MAJ-A TO HE-AA.
           IF HE-AA = SPACES
              INITIALIZE DATE-MAJ.
           MOVE D-MAJ-M TO HE-MM.
           MOVE D-MAJ-J TO HE-JJ.
           DISPLAY HE-DATE LINE 17 POSITION 26.
       DIS-HE-09.
           DISPLAY MOIS-DEBUT LINE 19 POSITION 35.
       DIS-HE-10.
           DISPLAY MOIS-FIN LINE 20 POSITION 35.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1401 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE 13 TO LIN-IDX.
           MOVE 0 TO COMPTEUR.
           PERFORM AFFICHE-D THRU AFFICHE-END.

       AFFICHE-D.
           MOVE COMPTEUR TO LNK-NUM.
           ADD 1 TO LIN-IDX LNK-NUM.
           MOVE "AY" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           IF LNK-TEXT = SPACES
              GO AFFICHE-END.
           ADD 1 TO COMPTEUR.
           IF H-S(COMPTEUR) NOT = "N"
              DISPLAY LNK-TEXT LINE LIN-IDX POSITION 53 SIZE 15 REVERSE
           ELSE
              DISPLAY LNK-TEXT LINE LIN-IDX POSITION 55 SIZE 15 LOW.
           GO AFFICHE-D.
       AFFICHE-END.
           EXIT.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE JOURNAL.
           MOVE MOIS-SAVE TO LNK-MOIS.
           IF NOT-OPEN = 1
              CLOSE ASSORT
              CLOSE INTER
              DELETE FILE ASSORT
              DELETE FILE INTER.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

