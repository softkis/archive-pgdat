      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-MISFIN IMPRESSION MISSION TERMINEES       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-MISFIN.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MISSION.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "MISSION.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80       ".
       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  PRECISION             PIC 99 VALUE 1.


      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "MESSAGE.REC".
           COPY "POCL.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 60.

       01  PAGES                  PIC 99 VALUE 0.

           COPY "V-VH00.CPY".

       01  MISSION-NAME.
           02 FILLER             PIC X(4) VALUE "S-MI".
           02 MISS-FIRME         PIC 9999.

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  LAST-NUMBER           PIC 9(8) VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MISSION.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-MISFIN.
       
           INITIALIZE REG-RECORD LIN-NUM FORMULAIRE.
           CALL "0-TODAY" USING TODAY.
           MOVE FR-KEY TO MISS-FIRME.
           OPEN INPUT MISSION.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM START-CLIENT
                  PERFORM READ-CLIENT THRU READ-EXIT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-CLIENT.
           MOVE FR-KEY TO PC-FIRME PC-FIRME-A.
           IF PC-NUMBER > 0 
              SUBTRACT 1 FROM PC-NUMBER.
           MOVE PC-NUMBER TO PC-NUMBER-A.
           IF A-N = "A"
              MOVE 99999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-CLIENT.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-POCL.
           PERFORM DIS-HE-01.
           IF PC-NUMBER = 0
           OR PC-NUMBER > END-NUMBER
           OR PC-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM TOTAL-MISSION THRU TOTAL-MISSION-END.
           IF  PC-NUMBER    < END-NUMBER
           AND PC-MATCHCODE < END-MATCHCODE
              GO READ-CLIENT
           END-IF.
       READ-EXIT.

       NEXT-POCL.
           CALL "6-POCL" USING LINK-V PC-RECORD A-N EXC-KEY.
           IF PC-NUMBER > 0
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PC-DATE-FIN = 0
                    CONTINUE
                 ELSE
                    IF PC-FIN-A < TODAY-ANNEE
                       GO NEXT-POCL
                    END-IF
                 END-IF
              END-IF
           END-IF.
 
       GET-PERS.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           MOVE 20 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
       TOTAL-MISSION.
           INITIALIZE MISS-RECORD.
           MOVE LNK-ANNEE TO MISS-DEBUT-A.
           SUBTRACT 1 FROM   MISS-DEBUT-A.
           MOVE LNK-MOIS  TO MISS-DEBUT-M.
           MOVE PC-NUMBER TO MISS-CLIENT.
           START MISSION KEY > MISS-KEY-3 INVALID
              PERFORM READ-END
              GO TOTAL-MISSION-END.
           PERFORM READ-MISSION THRU READ-END.
       TOTAL-MISSION-END.
           EXIT.

       READ-MISSION.
           READ MISSION NEXT NO LOCK AT END 
              GO READ-END.
           IF PC-NUMBER NOT = MISS-CLIENT
              GO READ-END.
           IF  LNK-ANNEE = MISS-FIN-A
           AND LNK-MOIS  = MISS-FIN-M 
              PERFORM DET-MISSION
           ELSE
              GO READ-MISSION.
           GO READ-MISSION.
       READ-END.
           EXIT.


       DET-MISSION.
           IF LIN-NUM > 59
              PERFORM TRANSMET
              ADD 1 TO PAGES
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              MOVE 1 TO LIN-NUM
              INITIALIZE FORMULAIRE
              MOVE 20 TO COL-NUM
              MOVE MENU-DESCRIPTION TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              PERFORM MOIS-NOM
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE LNK-ANNEE TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO LIN-NUM
           END-IF.
           IF PC-NUMBER NOT = LAST-NUMBER
              ADD 1 TO LIN-NUM
              MOVE 10 TO COL-NUM
              MOVE 8 TO CAR-NUM
              MOVE PC-NUMBER TO VH-00 LAST-NUMBER
              PERFORM FILL-FORM
              MOVE 20 TO COL-NUM
              MOVE PC-NOM TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD  1 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           MOVE 8 TO CAR-NUM.
           MOVE MISS-CONTRAT TO VH-00.
           PERFORM FILL-FORM.
      * DATE-MISSION.
           MOVE 55 TO COL-NUM.
           MOVE MISS-DEBUT-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 58 TO COL-NUM.
           MOVE MISS-DEBUT-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 61 TO COL-NUM.
           MOVE MISS-DEBUT-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE " -" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 68 TO COL-NUM.
           MOVE MISS-FIN-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 71 TO COL-NUM.
           MOVE MISS-FIN-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 74 TO COL-NUM.
           MOVE MISS-FIN-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE 8 TO CAR-NUM.
           MOVE MISS-PERSON TO VH-00 REG-PERSON
           PERFORM FILL-FORM.
           PERFORM GET-PERS.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE PC-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 6 POSITION 25.
           DISPLAY PC-NOM LINE 6 POSITION 46 SIZE 33.
           IF A-N = "N"
              DISPLAY SPACES LINE 6 POSITION 35 SIZE 10.
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 1602 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 60 TO LNK-LINE.
           CALL "PDOC" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           INITIALIZE REG-RECORD LIN-NUM LAST-NUMBER.


       END-PROGRAM.
           IF LIN-NUM > 1
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0 
              MOVE 99 TO LNK-VAL
              CALL "PDOC" USING LINK-V FORMULAIRE
           END-IF.
           CANCEL "PDOC".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXP".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XECRAN.CPY".
           COPY "XFILL2.CPY".

