      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 7-JRGAR COMPTEUR HEURES MALADIE POUR        �
      *  � PRIME GARAGISTES                                      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    7-JRGAR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PRESENCE.REC".
           COPY "V-VAR.CPY".
        
       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".

       PROCEDURE DIVISION USING LINK-V REG-RECORD.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-FA-PRIME.

           INITIALIZE SH-00.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF LNK-MOIS = 12
              MOVE 12 TO IDX
              PERFORM C-H
              MOVE SH-00 TO LNK-VAL
              EXIT PROGRAM
           ELSE
              PERFORM C-H VARYING IDX FROM 1 BY 1 UNTIL IDX > LNK-MOIS.
           SUBTRACT 1 FROM LNK-ANNEE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           PERFORM C-H.
           ADD 1 TO LNK-ANNEE.
           MOVE SH-00 TO LNK-VAL.
           EXIT PROGRAM.

       C-H.
           IF PRES-TOT(IDX) > 15
              ADD 1 TO SH-00
           END-IF.
