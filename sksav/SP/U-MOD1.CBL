      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME U-MOD1 MODIF 1 2009                         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    U-MOD1.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MENU.FC".
           COPY "MENUCOPY.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "MENU.FDE".
           COPY "MENUCOPY.FDE".

       WORKING-STORAGE SECTION.
             
       01  MENUS.
           02 MEN-01 PIC 9(10) VALUE 0801020400.
       01  COD-M REDEFINES MENUS.
           02  MEN PIC 9(10) OCCURS 1.

       01  COD-V.
           02  COD-1    PIC 9999 VALUE 0001.
           02  COD-2    PIC 9999 VALUE 0323.
           02  COD-2    PIC 9999 VALUE 0356.
           02  COD-2    PIC 9999 VALUE 0357.
       01  COD-R REDEFINES COD-V.
           02  COD PIC 9999 OCCURS 4.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  NOT-OPEN PIC 9 VALUE 0.


       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON MENU MENUCOPY.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  program.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-U-MOD1.

           OPEN I-O MENU.
           OPEN INPUT MENUCOPY.
           MOVE 0 TO LNK-VAL.
           MOVE LNK-ANNEE TO IDX-1.
           MOVE 2009 TO LNK-ANNEE.
           PERFORM MENU VARYING IDX FROM 1 BY 1 UNTIL IDX > 1.
           PERFORM COPIES VARYING IDX FROM 1 BY 1 UNTIL IDX > 4.
           MOVE IDX-1 TO LNK-ANNEE.
           EXIT PROGRAM.

       COPIES.
           MOVE COD(IDX) TO LNK-NUM.
           CALL "4-CSIMP" USING LINK-V.

       MENU.
           MOVE MEN(IDX) TO MNC-KEY.
           READ MENUCOPY INVALID CONTINUE
           NOT INVALID
              PERFORM TRANSLATE.

       TRANSLATE.
           MOVE MNC-RECORD TO MN-RECORD.
           WRITE MN-RECORD INVALID REWRITE MN-RECORD END-WRITE.
           CALL "4-MENUD" USING LINK-V MN-RECORD.
           CALL "4-MENUE" USING LINK-V MN-RECORD.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".


