      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-SISHD TRANSFERT HEURES SAISIES HD         �
      *  � RECAPITULATION MENSUELLE  SPECIAL SISTO               �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-SISHD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "HEURES.FC".
           SELECT OPTIONAL TF-SISTO ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

       FD  HEURES
           LABEL RECORD STANDARD
           DATA RECORD HRS-RECORD.
      *  ENREGISTREMENT FICHIER HEURES 
       01  HRS-RECORD.
           02 HRS-KEY.
              03 HRS-FIRME          PIC 9(8).
1             03 HRS-PERSON         PIC 9(8).
2             03 HRS-DATE.
                 04 HRS-MOIS        PIC 99.
                 04 HRS-JOUR        PIC 99.
              03 HRS-INTERVENT      PIC 9(4).
3             03 HRS-COUT.
                 04 HRS-OCCUP       PIC 99.
                 04 HRS-POSTE       PIC 9(8).
                 04 HRS-RUBRIQUE    PIC 99.
                 04 HRS-OBJET       PIC 9(6).
                 04 HRS-S-POSTE     PIC X(42).

           02 HRS-KEY-A.
              03 HRS-FIRME-A        PIC 9(8).
              03 HRS-COUT-A.
                 04 HRS-OCCUP-A     PIC 99.
                 04 HRS-POSTE-A     PIC 9(8).
                 04 HRS-RUBRIQUE-A  PIC 99.
                 04 HRS-OBJET-A     PIC 9(6).
                 04 HRS-S-POSTE-A   PIC X(42).
              03 HRS-PERSON-A       PIC 9(8).
              03 HRS-DATE-A.
                 04 HRS-MOIS-A      PIC 99.
                 04 HRS-JOUR-A      PIC 99.
              03 HRS-INTERVENT-A    PIC 9999.
                
           02 HRS-KEY-B.
              03 HRS-FIRME-B        PIC 9(8).
              03 HRS-COUT-B.
                 04 HRS-OCCUP-B     PIC 99.
                 04 HRS-POSTE-B     PIC 9(8).
                 04 HRS-RUBRIQUE-B  PIC 99.
                 04 HRS-OBJET-B     PIC 9(6).
                 04 HRS-S-POSTE-B   PIC X(42).
              03 HRS-DATE-B.
                 04 HRS-MOIS-B      PIC 99.
                 04 HRS-JOUR-B      PIC 99.
              03 HRS-INTERVENT-B    PIC 9999.
              03 HRS-PERSON-B       PIC 9(8).

           02 HRS-KEY-C.
              03 HRS-FIRME-C        PIC 9(8).
              03 HRS-PERSON-C       PIC 9(8).
              03 HRS-COUT-C.
                 04 HRS-OCCUP-C     PIC 99.
                 04 HRS-POSTE-C     PIC 9(8).
                 04 HRS-RUBRIQUE-C  PIC 99.
                 04 HRS-OBJET-C     PIC 9(6).
                 04 HRS-S-POSTE-C   PIC X(42).
              03 HRS-DATE-C.
                 04 HRS-MOIS-C      PIC 99.
                 04 HRS-JOUR-C      PIC 99.
              03 HRS-INTERVENT-C    PIC 9999.

           02 HRS-REC-DET.
              03 HRS-TEMPS.
                 04 HRS-DEBUT       PIC 9999.
                 04 HRS-DEBUTR REDEFINES HRS-DEBUT.
                    05 HRS-HR-D     PIC 99.
                    05 HRS-MIN-D    PIC 99.
                 04 HRS-FIN         PIC 9999.
                 04 HRS-FINR REDEFINES HRS-FIN.
                    05 HRS-HR-F     PIC 99.
                    05 HRS-MIN-F    PIC 99.
             03 HRS-REPOS           PIC 9999.
             03 HRS-TOTAL-BRUT      PIC 99V99.
             03 HRS-TOTAL-NET       PIC 99V99.
             03 HRS-TARIF.
                04 HRS-TARIF-HRS    PIC 99V99  OCCURS 10.
                04 HRS-TARIF-JRS    PIC 99V99  OCCURS 10.
             03 HRS-TARIF-R REDEFINES HRS-TARIF.
                04 HRS-TARIF-IDX    PIC 99V99  OCCURS 20.
             03 HRS-UNITES.
                04 HRS-UNITE        PIC 9(6)V99 OCCURS 10.
             03 HRS-UNITES-R REDEFINES HRS-UNITES.
                04 HRS-UNIT         PIC 9(8) OCCURS 10.
             03 HRS-JOUR-LOGIC      PIC 99.
             03 HRS-LOGIC           PIC 9(4).
             03 HRS-L-R REDEFINES HRS-LOGIC.
                04 HRS-HR-L         PIC 99.
                04 HRS-MIN-L        PIC 99.
             03 HRS-INFO            PIC X(20).
             03 HRS-STAMP.
                04 HRS-TIME.
                   05 HRS-ST-ANNEE PIC 9999.
                   05 HRS-ST-MOIS  PIC 99.
                   05 HRS-ST-JOUR  PIC 99.
                   05 HRS-ST-HEURE PIC 99.
                   05 HRS-ST-MIN   PIC 99.
                04 HRS-USER        PIC X(10).

       FD  TF-SISTO
           RECORD CONTAINS 161 CHARACTERS
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TF-ANNEE      PIC Z(5).
           02 TF-ANNEE-T REDEFINES TF-ANNEE PIC X(5).
           02 TF-FILLER-1   PIC X.
           02 TF-MOIS       PIC ZZZZZ.
           02 TF-MOIS-T REDEFINES TF-MOIS PIC X(5).
           02 TF-FILLER-2   PIC X.
           02 TF-JOUR       PIC ZZZZZ.
           02 TF-JOUR-T REDEFINES TF-JOUR PIC X(5).
           02 TF-FILLER-3   PIC X.
           02 TF-PERSON     PIC Z(8).
           02 TF-PERS-T REDEFINES TF-PERSON PIC X(8).
           02 TF-FILLER-4   PIC X.
           02 TF-NOM        PIC X(20).
           02 TF-FILLER-5   PIC X.
           02 TF-POSTE      PIC Z(8).
           02 TF-POSTE-T REDEFINES TF-POSTE PIC X(8).
           02 TF-FILLER-6   PIC X.
           02 TF-RUBRIQUE   PIC Z(7).
           02 TF-RUBRIQUE-T REDEFINES TF-RUBRIQUE PIC X(7).
           02 TF-FILLER-7   PIC X.
           02 TF-OBJET      PIC Z(7).
           02 TF-OBJET-T REDEFINES TF-OBJET PIC X(7).
           02 TF-FILLER-8   PIC X.
           02 TF-HEURES     PIC Z(4),ZZ.
           02 TF-HEURES-T REDEFINES TF-HEURES PIC X(7).
           02 TF-FILLER-9   PIC X.
           02 TF-NUIT       PIC Z(4),ZZ.
           02 TF-NUIT-T REDEFINES TF-NUIT PIC X(7).
           02 TF-FILLER-10  PIC X.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "PARMOD.REC".

       01  MOIS-DEBUT      PIC 99 VALUE 1.
       01  MOIS-FIN        PIC 99 VALUE 12.

       01  HRS-NAME.
           02 HRS-ID           PIC X(9) VALUE "S-HEURES.".
           02 ANNEE-HEURES        PIC 999.
           COPY "V-VAR.CPY".
        
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  VALEUR                PIC X.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON HEURES.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-SISHD.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE LNK-MOIS TO SAVE-MOIS MOIS-DEBUT MOIS-FIN.

           MOVE LNK-SUFFIX TO ANNEE-HEURES.
           OPEN INPUT HEURES.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6 THRU 7 
                      MOVE 0000000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-PATH
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7 
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-6.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
                     PERFORM APRES-6
             WHEN 66 ADD      1 TO   MOIS-DEBUT
                     PERFORM APRES-6
             END-EVALUATE.

       AVANT-7.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
                     PERFORM APRES-7
             WHEN 66 ADD      1 TO   MOIS-FIN
                     PERFORM APRES-7
             END-EVALUATE.

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 22 POSITION 20 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-06.
           PERFORM DIS-HE-07.

       APRES-7.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-07.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM START-PERSON
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           PERFORM READ-PERSON THRU READ-EXIT.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           PERFORM RECHERCHE-HRS.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.


       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.


       RECHERCHE-HRS.
           PERFORM DIS-HE-01.
           INITIALIZE HRS-RECORD.
           MOVE FR-KEY       TO HRS-FIRME.
           MOVE REG-PERSON   TO HRS-PERSON.
           MOVE MOIS-DEBUT   TO HRS-MOIS.
           START HEURES KEY >= HRS-KEY INVALID CONTINUE
               NOT INVALID PERFORM READ-HRS THRU READ-HRS-END.

       READ-HRS.
           READ HEURES NEXT AT END
               GO READ-HRS-END
           END-READ.
           IF FR-KEY       NOT = HRS-FIRME
           OR REG-PERSON   NOT = HRS-PERSON
           OR MOIS-FIN     <     HRS-MOIS 
              GO READ-HRS-END
           END-IF.
           IF NOT-OPEN = 0 
              OPEN OUTPUT TF-SISTO
              INITIALIZE TF-RECORD
              MOVE "JAHR "     TO TF-ANNEE-T
              MOVE "MONAT"     TO TF-MOIS-T
              MOVE "TAG"       TO TF-JOUR-T
              MOVE "PERSON "   TO TF-PERS-T
              MOVE "NAME"      TO TF-NOM
              MOVE "KOSTEN"    TO TF-POSTE-T
              MOVE "LOHNART"   TO TF-RUBRIQUE-T
              MOVE "PROJEKT"   TO TF-OBJET-T
              MOVE "STUNDEN"   TO TF-HEURES-T
              MOVE "NACHT  "   TO TF-NUIT-T
              MOVE ";" TO TF-FILLER-1 
                          TF-FILLER-2
                          TF-FILLER-3
                          TF-FILLER-4
                          TF-FILLER-5
                          TF-FILLER-6
                          TF-FILLER-7
                          TF-FILLER-8
                          TF-FILLER-9
                          TF-FILLER-10
              WRITE TF-RECORD
              ADD 1 TO NOT-OPEN
           END-IF.
           MOVE LNK-ANNEE  TO TF-ANNEE.
           MOVE HRS-MOIS   TO TF-MOIS.
           MOVE REG-PERSON TO TF-PERSON.
           MOVE PR-NOM     TO TF-NOM.
           MOVE HRS-JOUR   TO TF-JOUR.
           MOVE HRS-POSTE  TO TF-POSTE.
           MOVE HRS-RUBRIQUE TO TF-RUBRIQUE.
           MOVE HRS-OBJET  TO TF-OBJET.
           MOVE HRS-TOTAL-NET TO TF-HEURES.
           MOVE HRS-TARIF-HRS(1) TO TF-NUIT.

           MOVE ";" TO TF-FILLER-1 
                       TF-FILLER-2
                       TF-FILLER-3
                       TF-FILLER-4
                       TF-FILLER-5
                       TF-FILLER-6
                       TF-FILLER-7
                       TF-FILLER-8
                       TF-FILLER-9
                       TF-FILLER-10.
           WRITE TF-RECORD.
           GO READ-HRS.
       READ-HRS-END.
           EXIT.

           COPY "XDIS.CPY".
       DIS-HE-06.
           DISPLAY MOIS-DEBUT LINE 19 POSITION 31.
           MOVE MOIS-DEBUT TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-07.
           DISPLAY MOIS-FIN LINE 20 POSITION 31.
           MOVE MOIS-FIN TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 605 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".


