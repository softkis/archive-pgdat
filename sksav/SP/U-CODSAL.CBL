      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME U-CODSAL CONVERSION CODES SALAIRES          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    U-CODSAL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODSAL.FC".
           COPY "CSDEF.FC".
           COPY "CODTXT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CODSAL.FDE".
           COPY "CSDEF.FDE".
           COPY "CODTXT.FDE".
               
       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CSDET.LNK".

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON CODSAL CODTEXT CSDEF.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  program.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-U-CODSAL.

           OPEN INPUT CODSAL.
           OPEN I-O CSDEF.
           OPEN I-O CODTEXT.

           INITIALIZE CS-RECORD.
           START CODSAL KEY >= CS-KEY INVALID EXIT PROGRAM
              NOT INVALID
              PERFORM CS-C THRU CS-C-END.
       CS-C.
           READ CODSAL NEXT NO LOCK AT END 
              GO CS-C-END.
           INITIALIZE LINK-RECORD CTX-RECORD CD-RECORD.
           MOVE CS-NUMBER TO LINK-NUMBER CTX-NUMBER CD-NUMBER.
           MOVE 2001 TO LINK-ANNEE.
           MOVE    1 TO LINK-MOIS.
           MOVE CS-PERMISSION  TO LINK-PERMISSION. 
           MOVE CS-VALEURS     TO LINK-VALEURS.
           MOVE CS-MALADIE     TO LINK-MALADIE.
           MOVE CS-PERIODES    TO LINK-PERIODES. 
           MOVE CS-GROUPE      TO LINK-GROUPE. 
           MOVE CS-FRAIS       TO LINK-FRAIS.
           MOVE CS-NEGATIF     TO LINK-NEGATIF.

           PERFORM CONV VARYING IDX FROM 1 BY 1 UNTIL IDX > 8.
           MOVE CS-STAMP  TO LINK-STAMP.
           CALL "6-CS" USING LINK-V LINK-RECORD CD-RECORD WR-KEY.
           MOVE CS-NOM TO CD-DEFINITION.
           WRITE CD-RECORD INVALID REWRITE CD-RECORD END-WRITE.
           MOVE "F" TO LNK-LANGUAGE.
           PERFORM TEXTE.
           CALL "6-CODTXT" USING LINK-V CS-RECORD.
           MOVE "D" TO LNK-LANGUAGE.
           CALL "6-CODTXT" USING LINK-V CS-RECORD.
           PERFORM TEXTE.
           MOVE "E" TO LNK-LANGUAGE.
           CALL "6-CODTXT" USING LINK-V CS-RECORD.
           PERFORM TEXTE.
           MOVE "F" TO LNK-LANGUAGE.
           GO CS-C.
       CS-C-END.
           EXIT PROGRAM.

       CONV.
           MOVE CS-SAISIE(IDX)         TO LINK-SAISIE(IDX).
           MOVE CS-IMPRESSION(IDX)     TO LINK-IMPRESSION(IDX).
           MOVE CS-PROCEDURE(IDX)      TO LINK-PROCEDURE(IDX).
           MOVE CS-FORMULE(IDX)        TO LINK-FORMULE(IDX).
           MOVE CS-VAL-PREDEFINIE(IDX) TO LINK-VAL-PREDEFINIE(IDX).
           MOVE CS-VAL-MAX(IDX)        TO LINK-VAL-MAX(IDX).
           MOVE CS-VAL-MIN(IDX)        TO LINK-VAL-MIN(IDX).
           MOVE CS-NULL(IDX)           TO LINK-NULL(IDX).
           MOVE CS-INPUT(IDX)          TO LINK-INPUT(IDX).
           MOVE CS-COLONNES(IDX)       TO LINK-COLONNES(IDX).
           MOVE CS-DECIMALES(IDX)      TO LINK-DECIMALES(IDX).
           MOVE CS-VENT(IDX)           TO LINK-VENT(IDX).
           MOVE CS-NOTPOL(IDX)         TO LINK-NOTPOL(IDX).

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".

       TEXTE.
           MOVE LNK-LANGUAGE   TO CTX-LANGUE.
           MOVE CS-NOM         TO CTX-NOM.
           MOVE CS-COMMENTAIRE TO CTX-COMMENTAIRE.
           PERFORM DESC VARYING IDX FROM 1 BY 1 UNTIL IDX > 8.
           CALL "6-CSTXT" USING LINK-V CTX-RECORD WR-KEY.

       DESC.
           MOVE CS-DESCRIPTION(IDX) TO CTX-DESCRIPTION(IDX).
      