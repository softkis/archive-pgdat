      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-BARDEF MODULE GENERAL LECTURE BARDEF      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-BARDEF.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "BARDEF.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "BARDEF.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN  PIC 9 VALUE 0.
       01  ACTION    PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "BARDEF.LNK".

       01  EXC-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON BARDEF .
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-BARDEF.

           IF NOT-OPEN = 0
              OPEN I-O BARDEF
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO BDF-RECORD.

           IF EXC-KEY = 99
              IF LNK-SQL = "Y" 
                 CALL "9-BARDEF" USING LINK-V BDF-RECORD EXC-KEY 
              END-IF
              WRITE BDF-RECORD INVALID REWRITE BDF-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.
           
           EVALUATE EXC-KEY 
               WHEN 66 PERFORM START-1
               READ BARDEF NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN 65 PERFORM START-2
               READ BARDEF PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN OTHER GO EXIT-3
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE BDF-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-3.
           READ BARDEF NO LOCK INVALID INITIALIZE BDF-REC-DET.
           GO EXIT-2.

       START-1.
           START BARDEF KEY > BDF-KEY INVALID GO EXIT-1.
       START-2.
           START BARDEF KEY < BDF-KEY INVALID GO EXIT-1.

