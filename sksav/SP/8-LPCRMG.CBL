      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-LPCRMG TRANSFER ASCII LIVRE CUMULE RMG    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-LPCRMG.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

       FD  TF-TRANS
           LABEL RECORD STANDARD
           DATA RECORD TLP-RECORD.
      *  ENREGISTREMENT FICHIER DE TRANSFERT ASCII LIVRE DE PAIE
      
       01  TLP-RECORD.
           02 TLP-MATRICULE       PIC X(11).
           02 TLP-FILLER          PIC X.
           02 TLP-NOM             PIC X(30).
           02 TLP-FILLER          PIC X.
           02 TLP-PRENOM          PIC X(20).
           02 TLP-FILLER          PIC X.
           02 TLP-PERS            PIC Z(6).
           02 TLP-FILLER          PIC X.
           02 TLP-ANNEE           PIC 99999.
           02 TLP-FILLER          PIC X.
           02 TLP-MOIS            PIC 9999.
           02 TLP-FILLER          PIC X.
           02 TLP-MOIS-FIN        PIC Z(4). 
           02 TLP-FILLER          PIC X.
           02 TLP-JRS-IMP-TOT     PIC 9(3).
           02 TLP-FILLER          PIC X.

           02 TLP-HEURES.
              03 TLP-H OCCURS 2.
                 04 TLP-TEXTE        PIC X.
                 04 TLP-HR           PIC Z(6).
                 04 TLP-FILLER       PIC X.
           02 TLP-HRS-R REDEFINES TLP-HEURES.
              03 TLP-H1 OCCURS 2.
                 04 TLP-HRS          PIC Z(4),ZZ.
                 04 TLP-FILLER       PIC X.
           02 TLP-HEURES-R REDEFINES TLP-HEURES.
              03 TLP-HRS-PROD-TOT    PIC Z(4),ZZ.
              03 TLP-FILLER          PIC X.
              03 TLP-HRS-PROD-SUPPL  PIC Z(4),ZZ.
              03 TLP-FILLER          PIC X.

           02 TLP-SAL-DET.
              03 TLP-S OCCURS 24.
                 04 TLP-VAL          PIC -Z(7),ZZ.
                 04 TLP-FILLER       PIC X.
           02 TLP-SAL-N REDEFINES TLP-SAL-DET.
              03 TLP-N OCCURS 24.
                 04 TLP-NUM          PIC -Z(10).
                 04 TLP-FILLER       PIC X.
           02 TLP-SAL-R REDEFINES TLP-SAL-DET.
1             03 TLP-BASE            PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
2             03 TLP-HS              PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
3             03 TLP-DIMANCHE        PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
4             03 TLP-FERIE           PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
5             03 TLP-AJUSTEMENT      PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
6             03 TLP-BRUT            PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
7             03 TLP-COTIS-MAL-SAL   PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
8             03 TLP-COTIS-PEN-SAL   PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
9             03 TLP-SHS-FRANCHISE   PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
10            03 TLP-NDF-FRANCHISE   PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
11            03 TLP-ABAT-TOT        PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
12            03 TLP-IMPOSABLE       PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
13            03 TLP-IMPOTS          PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
14            03 TLP-COTIS-DEP-SAL   PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
15            03 TLP-NET             PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
16            03 TLP-TOTAL-PLUS      PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
17            03 TLP-TOTAL-MINUS     PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
18            03 TLP-TOTAL-SAISIE    PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
19            03 TLP-A-PAYER         PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.

20            03 TLP-COTIS-MAL-PAT   PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
21            03 TLP-COTIS-PEN-PAT   PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
22            03 TLP-COTIS-ACC-PAT   PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
23            03 TLP-COTIS-FAM-PAT   PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.
24            03 TLP-COUT-TOTAL      PIC -Z(7),ZZ.
              03 TLP-FILLER          PIC X.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 5.
       01  PRECISION             PIC 9 VALUE 1.

       01  TXT-RECORD.
           02 TXT-FIRME           PIC X(11) VALUE "MATRICULE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-NOM             PIC X(30) VALUE "NOM".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-PRENOM          PIC X(20) VALUE "PRENOM".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-PERS            PIC X(6) VALUE "NUMERO".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-ANNEE           PIC XXXXX VALUE "ANNEE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-MOIS            PIC XXXX VALUE "MOIS".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-MOIS-FIN        PIC X(4) VALUE "FIN". 
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-JRS-IMP-TOT     PIC X(3) VALUE "JIM".
           02 TXT-FILLER          PIC X VALUE ";".

           02 TXT-HEURES.
             03 TXT-HRS-PROD-TOT    PIC X(7) VALUE "HR PROD".
             03 TXT-FILLER          PIC X VALUE ";".
             03 TXT-HRS-PROD-SUPPL  PIC X(7) VALUE "HR SUPP".
             03 TXT-FILLER          PIC X VALUE ";".

           02 TXT-SALAIRE.
1            03 TXT-BASE            PIC X(11) VALUE "BASE".
             03 TXT-FILLER          PIC X VALUE ";".
2            03 TXT-HS              PIC X(11) VALUE "HRS SUPP".
             03 TXT-FILLER          PIC X VALUE ";".
3            03 TXT-DIM             PIC X(11) VALUE "DIMANCHE".
             03 TXT-FILLER          PIC X VALUE ";".
4            03 TXT-FERIE           PIC X(11) VALUE "FERIE ".
             03 TXT-FILLER          PIC X VALUE ";".
5            03 TXT-REPRISE         PIC X(11) VALUE "REPRISE".
             03 TXT-FILLER          PIC X VALUE ";".
6            03 TXT-BRUT            PIC X(11) VALUE "BRUT".
             03 TXT-FILLER          PIC X VALUE ";".
7            03 TXT-COTIS-MAL-SAL   PIC X(11) VALUE "MALADIE".
             03 TXT-FILLER          PIC X VALUE ";".
8            03 TXT-COTIS-PEN-SAL   PIC X(11) VALUE "PENSION".
             03 TXT-FILLER          PIC X VALUE ";".
9            03 TXT-SHS-FRANCHISE   PIC X(11) VALUE "FRANCH SHS".
             03 TXT-FILLER          PIC X VALUE ";".
10           03 TXT-NDF-FRANCHISE   PIC X(11) VALUE "FRANCH NDF".
             03 TXT-FILLER          PIC X VALUE ";".
11           03 TXT-ABAT-TOT        PIC X(11) VALUE "ABATTEMENT".
             03 TXT-FILLER          PIC X VALUE ";".
12           03 TXT-IMPOSABLE       PIC X(11) VALUE "IMPOSABLE".
             03 TXT-FILLER          PIC X VALUE ";".
13           03 TXT-IMPOTS          PIC X(11) VALUE "IMPOT".
             03 TXT-FILLER          PIC X VALUE ";".
14           03 TXT-COTIS-DEP-SAL   PIC X(11) VALUE "DEPENDANCE".
             03 TXT-FILLER          PIC X VALUE ";".
15           03 TXT-NET             PIC X(11) VALUE "NET".
             03 TXT-FILLER          PIC X VALUE ";".

16           03 TXT-TOTAL-PLUS      PIC X(11) VALUE "NET PLUS".
             03 TXT-FILLER          PIC X VALUE ";".
17           03 TXT-TOTAL-MINUS     PIC X(11) VALUE "NET MINUS".
             03 TXT-FILLER          PIC X VALUE ";".
18           03 TXT-TOTAL-SAISIE    PIC X(11) VALUE "SAISIES  ".
             03 TXT-FILLER          PIC X VALUE ";".
19           03 TXT-A-PAYER         PIC X(11) VALUE "A PAYER".
             03 TXT-FILLER          PIC X VALUE ";".

20           03 TXT-COTIS-MAL-PAT   PIC X(11) VALUE "MALADIE EMP".
             03 TXT-FILLER          PIC X VALUE ";".
21           03 TXT-COTIS-PEN-PAT   PIC X(11) VALUE "PENSION EMP".
             03 TXT-FILLER          PIC X VALUE ";".
22           03 TXT-COTIS-ACC-PAT   PIC X(11) VALUE "ACCIDENT EM".
             03 TXT-FILLER          PIC X VALUE ";".
23           03 TXT-COTIS-FAM-PAT   PIC X(11) VALUE "ALL FAM EMP".
             03 TXT-FILLER          PIC X VALUE ";".
24           03 TXT-COUT-TOTAL      PIC X(11) VALUE "COUT TOTAL".
             03 TXT-FILLER          PIC X VALUE ";".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "PARMOD.REC".
           COPY "LIVRE.REC".
           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       01  NOT-OPEN              PIC 99 VALUE 0.

       01 HELP-CUMUL.
          02 HELP-HEURES.
             03 HELP-HRS             PIC 9(8)V99 OCCURS 2.

          02 HELP-MONTANT.
             03 HELP-VAL             PIC S9(8)V99 OCCURS 24.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TF-TRANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-LPCRMG.
       
           MOVE "A" TO A-N.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-MOIS TO SAVE-MOIS.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-PATH
           WHEN  5 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  5 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================



       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 14 POSITION 40 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       


       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           INITIALIZE L-RECORD HELP-CUMUL TLP-RECORD.
           INSPECT TLP-RECORD REPLACING ALL " " BY ";"
           PERFORM DIS-HE-01.
           PERFORM READ-LIVRE THRU READ-LIVRE-END.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.

       READ-LIVRE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD NX-KEY.
           IF L-PERSON NOT = REG-PERSON
              GO READ-LIVRE-END.
           IF L-JOUR-DEBUT > 0
              PERFORM FILL-FILES.
           GO READ-LIVRE.
       READ-LIVRE-END.
           COMPUTE HELP-VAL(6) = HELP-VAL(1) + HELP-VAL(2) 
           + HELP-VAL(3) + HELP-VAL(4).
           COMPUTE HELP-VAL(24) = HELP-VAL(6) 
           + HELP-VAL(20) + HELP-VAL(21) + HELP-VAL(22) + HELP-VAL(23).
           PERFORM LOAD-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 2.
           PERFORM LOAD-VAL VARYING IDX FROM 1 BY 1 UNTIL IDX > 24.
           IF HELP-VAL(1) > 0
              WRITE TLP-RECORD.

       FILL-FILES.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-TRANS
              MOVE 1 TO NOT-OPEN
              WRITE TLP-RECORD FROM TXT-RECORD
              INITIALIZE TLP-RECORD
              INSPECT TLP-RECORD REPLACING ALL " " BY ";"
           END-IF.
           MOVE PR-MATRICULE TO TLP-MATRICULE.
           MOVE REG-PERSON TO TLP-PERS.
           MOVE LNK-ANNEE  TO TLP-ANNEE.
           MOVE PR-NOM          TO TLP-NOM.
           MOVE PR-PRENOM       TO TLP-PRENOM.
           IF TLP-MOIS = 0
              MOVE L-MOIS       TO TLP-MOIS.
           MOVE L-MOIS          TO TLP-MOIS-FIN.
           PERFORM CUMUL-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 2.
           PERFORM CUMUL VARYING IDX FROM 1 BY 1 UNTIL IDX > 23.
           ADD L-SHS-AJUSTE    TO HELP-VAL(9).
           ADD L-NDF-FRANCHISE TO HELP-VAL(10).
           ADD L-ABATTEMENT    TO HELP-VAL(11).
           ADD L-IMPOSABLE-NET TO HELP-VAL(12).
           ADD L-SALAIRE-NET   TO HELP-VAL(15).
           ADD L-JRS-IMP-TRAV  TO TLP-JRS-IMP-TOT.
           SUBTRACT L-DC(200) FROM HELP-VAL(19).


       R-H.
           MOVE IDX TO TLP-HR(IDX).
           MOVE "H" TO TLP-TEXTE(IDX).
       R-S.
           MOVE IDX TO TLP-NUM(IDX).


       CUMUL.
           MOVE 0 TO IDX-2 IDX-3.
           EVALUATE IDX 
                WHEN 1 MOVE 100 TO IDX-1
                       PERFORM DC 
                       MOVE  11 TO IDX-2
                       MOVE  50 TO IDX-3
                WHEN 2 MOVE  95 TO IDX-2
                       MOVE  98 TO IDX-3
                WHEN 3 MOVE  80 TO IDX-2 IDX-3
                WHEN 4 MOVE  71 TO IDX-2
                       MOVE  72 TO IDX-3
                WHEN 5 MOVE 160 TO IDX-2 IDX-3
                WHEN 7 MOVE 231 TO IDX-2 IDX-3
                WHEN 8 MOVE 232 TO IDX-2 IDX-3
                WHEN 13 MOVE 201 TO IDX-2 IDX-3
                WHEN 14 MOVE 235 TO IDX-2 IDX-3
                WHEN 16 MOVE 190 TO IDX-1
                        PERFORM DC 
                        MOVE 133 TO IDX-2 IDX-3
                WHEN 17 MOVE 290 TO IDX-2 IDX-3
                WHEN 18 MOVE 292 TO IDX-2 
                        MOVE 297 TO IDX-3
                WHEN 19 MOVE 300 TO IDX-2 IDX-3
                WHEN 20 MOVE 161 TO IDX-2 IDX-3
                WHEN 21 MOVE 162 TO IDX-2 IDX-3
                WHEN 22 MOVE 170 TO IDX-2 IDX-3
                WHEN 23 MOVE 163 TO IDX-2 IDX-3
           END-EVALUATE.
           IF IDX-2 > 0
              PERFORM DC 
              VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3.

       DC.
           ADD L-DC(IDX-1) TO HELP-VAL(IDX).

       CUMUL-HRS.
           EVALUATE IDX 
                WHEN 1 MOVE 100 TO IDX-2 IDX-3
           PERFORM HR VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE  88 TO IDX-2
                       MOVE  90 TO IDX-3
           PERFORM HR VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3
                       MOVE  12 TO IDX-2
                       MOVE  50 TO IDX-3
                WHEN 2 MOVE  95 TO IDX-2 IDX-3
           END-EVALUATE.
           PERFORM HR VARYING IDX-1 FROM IDX-2 BY 1 UNTIL IDX-1 > IDX-3.

       HR.
           ADD L-UNITE(IDX-1) TO HELP-HRS(IDX).

       LOAD-HRS.
           MOVE HELP-HRS(IDX) TO TLP-HRS(IDX).
       LOAD-VAL.
           MOVE HELP-VAL(IDX) TO TLP-VAL(IDX).



       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.
       DIS-HE-IDX.

       AFFICHAGE-ECRAN.
           MOVE  606 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".

