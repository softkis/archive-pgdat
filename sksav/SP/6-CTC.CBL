      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-CTC MODULE GENERAL LECTURE POSTES CLIENTS �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-CTC.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CONTACT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CONTACT.FDE".

       WORKING-STORAGE SECTION.

       01  ACTION         PIC X.
       01  NOT-OPEN       PIC 9 VALUE 0.
       01  LAST-CTC       PIC 9(8) VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "POCL.REC".
           COPY "CONTACT.LNK".
                 
       01  A-NUM    PIC X.
       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V PC-RECORD
                                LINK-RECORD A-NUM EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CONTACT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-CTC.
       
           IF NOT-OPEN = 0
              OPEN I-O CONTACT
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CTC-RECORD.
           MOVE FR-KEY    TO CTC-FIRME  CTC-FIRME-A.
           MOVE PC-NUMBER TO CTC-CLIENT CTC-CLIENT-A.
           IF EXC-KEY = 99
              IF LNK-SQL = "Y" 
                 CALL "9-CTC" USING LINK-V CTC-RECORD EXC-KEY 
              END-IF
              WRITE CTC-RECORD INVALID REWRITE CTC-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.
           IF EXC-KEY = 98
              IF LNK-SQL = "Y" 
                 CALL "9-CTC" USING LINK-V CTC-RECORD EXC-KEY 
              END-IF
              DELETE CONTACT INVALID CONTINUE END-DELETE
              EXIT PROGRAM
           END-IF.

           
           EVALUATE EXC-KEY 
           WHEN 65
               IF A-NUM = "N"
                  START CONTACT KEY < CTC-KEY INVALID GO EXIT-1
               ELSE
                  START CONTACT KEY < CTC-KEY-A INVALID GO EXIT-1
               END-IF
               READ CONTACT PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66
               IF A-NUM = "N"
                  START CONTACT KEY > CTC-KEY INVALID GO EXIT-1
               ELSE
                  START CONTACT KEY > CTC-KEY-A INVALID GO EXIT-1
               END-IF
               READ CONTACT NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER
               IF EXC-KEY = 4 
               OR EXC-KEY = 13
                  IF CTC-PERSON = 0
                     MOVE LAST-CTC TO CTC-PERSON 
                  END-IF
               END-IF
               READ CONTACT NO LOCK INVALID INITIALIZE CTC-REC-DET
               END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE CTC-RECORD TO LINK-RECORD.
           IF FR-KEY NOT = CTC-FIRME 
           OR PC-NUMBER NOT = CTC-CLIENT 
              INITIALIZE LINK-RECORD
           END-IF.
           IF CTC-PERSON < 99999999
              MOVE CTC-PERSON TO LAST-CTC.
      *    display last-CTC line 3 position 70.
      *    accept action no beep.
           MOVE 0 TO LNK-STATUS.
           IF CTC-ACTIF = "N"
              MOVE 1 TO LNK-STATUS
           END-IF.
           EXIT PROGRAM.



