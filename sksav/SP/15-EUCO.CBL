      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 15-EUCO TRANSFERT HEURES EUROCOMPOSITES     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    15-EUCO.

       ENVIRONMENT DIVISION.
 
       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
      *    Fichier interface EUROCOMPOSITES

           SELECT PICKUP ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-PICKUP.

           SELECT PROBLEME ASSIGN TO DISK, "HR-PROB"
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

           SELECT OPTIONAL CONVERSION ASSIGN TO DISK "TABHEURE.CON"
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-HELP.

           COPY "JOURS.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.

       FD  PICKUP
           LABEL RECORD STANDARD
           DATA RECORD IS PICK-RECORD.

       01  PICK-RECORD.
           02  PICK-PERS            PIC 9(6).
           02  PICK-COUT            PIC 9999.
           02  PICK-FIRME           PIC 9999.
           02  PICK-ANNEE           PIC 9999.
           02  PICK-MOIS            PIC 99.
           02  PICK-JOUR            PIC 99.
           02  PICK-CODE            PIC 9999.
           02  PICK-FILLER          PIC XX.
           02  PICK-TIME            PIC -ZZ,ZZ.

           COPY "JOURS.FDE".
           COPY "TRIPR.FDE".

       FD  CONVERSION
           RECORD CONTAINS 800 CHARACTERS
           LABEL RECORD STANDARD
           DATA RECORD IS TRPA-RECORD.
       01  TRPA-RECORD.
           02 PARAM-OCC  PIC 9999 OCCURS 100.
           02 PARAM-OCO  PIC 9999 OCCURS 100.

       FD  PROBLEME
           LABEL RECORD STANDARD
           DATA RECORD IS PROB-RECORD.

       01  PROB-RECORD.
           02 PROB-FIRME   PIC 9999.
           02 PROB-TEST-F  PIC X.
           02 PROB-FILLER  PIC X.
           02 PROB-PERS    PIC 9(6).
           02 PROB-TEST-P  PIC X.
           02 PROB-FILLER  PIC X.
           02 PROB-ANNEE   PIC 9999.
           02 PROB-TEST-A  PIC X.
           02 PROB-FILLER  PIC X.
           02 PROB-MOIS    PIC 99.
           02 PROB-TEST-M  PIC X.
           02 PROB-FILLER  PIC X.
           02 PROB-OCCUP   PIC 9999.
           02 PROB-TEST-O  PIC X.
           02 PROB-FILLER  PIC X.
           02 PROB-COMPL   PIC 9999.
           02 PROB-TEST-C  PIC X.
           02 PROB-FILLER  PIC X.
           02 PROB-LINE    PIC 9(5).



       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX PIC 99 VALUE 2.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "REGISTRE.REC".
           COPY "PERSON.REC".
           COPY "CARRIERE.REC".
           COPY "PARMOD.REC".
           COPY "LIVRE.REC".

           COPY "PRESENCE.REC".
           COPY "CCOL.REC".
           COPY "CALEN.REC".
           COPY "OCCUP.REC".
           COPY "OCCOM.REC".

       01  TARIF                PIC 99.
       01  JOUR-IDX             PIC 99.
       01  JOUR                 PIC 99.
       01  ANNEE                PIC 99.
       01  HELP-1               PIC 99.
       01  HELP-2               PIC 99.
       01  HELP-4               PIC S999V99.
       01  HELP-HH              PIC 99V99.
       01  HELP-HR REDEFINES HELP-HH.
           02 HELP-H            PIC 99.
           02 HELP-M            PIC 99.
       
       01  TEST-ROUND           PIC 999V99999.
       01  COMPTEUR             PIC 9(5).
       01  COMPTEUR-FAUTES      PIC 9(5) VALUE 0.

       01  O-PARAMETER.
           02 OCC-PARAMETER.
              03 OCC  PIC 9999 OCCURS 100.
           02 OCO-PARAMETER.
              03 OCO  PIC 9999 OCCURS 100.

       01  JOURS-NAME.
           02 FILLER            PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS       PIC 999.
       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "I".
           02 FIRME-TRIPR        PIC 999999.
           02 FILLER             PIC XXXX VALUE ".EUC".
           02 USER-TRIPR         PIC XXXXXXXX.

       01  HE-Z6 PIC Z(6).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
          COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               PICKUP 
               PROBLEME
               CONVERSION
               TRIPR
               JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-15-EUCO.

           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN I-O JOURS.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           MOVE 0 TO FIRME-TRIPR.
           ADD  FR-KEY TO FIRME-TRIPR.
           MOVE LNK-USER  TO USER-TRIPR.
           DELETE FILE TRIPR.
           OPEN I-O TRIPR.
           INITIALIZE TRIPR-RECORD.

           OPEN INPUT CONVERSION.
           READ CONVERSION INTO O-PARAMETER AT END CONTINUE END-READ.
           CLOSE CONVERSION.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0000000025 TO EXC-KFR (1)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-PATH
           WHEN  2 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
               PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  2 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\TIM\FILE" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 14 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           INITIALIZE COMPTEUR PROB-RECORD IDX-4.
           EVALUATE EXC-KEY
               WHEN 5 OPEN INPUT PICKUP
                      OPEN OUTPUT PROBLEME
                      PERFORM LECT-PICKUP THRU END-PICKUP
                      PERFORM START-TRI
                      PERFORM END-PROGRAM
           END-EVALUATE.

       LECT-PICKUP.
           INITIALIZE PICK-RECORD.
           READ PICKUP AT END GO END-PICKUP.
           ADD 1 TO COMPTEUR.
           MOVE 0 TO INPUT-ERROR
           DISPLAY PICK-RECORD LINE 4 POSITION 10 SIZE 51.
           IF PICK-FIRME < 1 GO LECT-PICKUP. 
           MOVE PICK-TIME TO HELP-4.
           IF HELP-4 < 0 GO LECT-PICKUP. 
           IF PICK-PERS NOT = TRIPR-PERSON
              PERFORM WRITE-TRI.
           IF PICK-ANNEE NOT = LNK-ANNEE
              MOVE "?" TO PROB-TEST-A 
              MOVE 1 TO INPUT-ERROR.
           IF PICK-MOIS  NOT = LNK-MOIS
              MOVE "?" TO PROB-TEST-M 
              MOVE 1 TO INPUT-ERROR.

           IF PICK-FIRME NOT = FR-KEY
              MOVE "?" TO PROB-TEST-F 
              MOVE 1 TO INPUT-ERROR.

           MOVE 0 TO OCC-KEY OCO-NUMBER.
           IF PICK-CODE = 1
              MOVE 0 TO PICK-CODE
           END-IF.
           IF PICK-CODE > 0
              IF OCO(PICK-CODE) > 0
                 MOVE OCO(PICK-CODE) TO OCO-NUMBER
                 CALL "6-OCCOM" USING LINK-V OCO-RECORD FAKE-KEY
              ELSE
                 IF OCC(PICK-CODE) > 0
                    MOVE OCC(PICK-CODE) TO OCC-KEY
                    CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY
                 END-IF
              END-IF
              IF  OCC-KEY < 1
              AND OCO-NUMBER < 1
                 MOVE "?" TO PROB-TEST-O 
                 MOVE 1 TO INPUT-ERROR
              END-IF
           ELSE
              CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY
           END-IF.

           IF INPUT-ERROR = 1
              PERFORM WRITE-PROB
              GO LECT-PICKUP
           END-IF.
           PERFORM WRITE-OCC.
           GO LECT-PICKUP.
       END-PICKUP.
           CLOSE PICKUP.

       WRITE-OCC.
           INITIALIZE JRS-RECORD.
           MOVE PICK-FIRME TO JRS-FIRME
                              JRS-FIRME-1
                              JRS-FIRME-2
                              JRS-FIRME-3.
           MOVE PICK-PERS  TO JRS-PERSON
                              JRS-PERSON-1
                              JRS-PERSON-2
                              JRS-PERSON-3.
           MOVE PICK-MOIS  TO JRS-MOIS
                              JRS-MOIS-1
                              JRS-MOIS-2
                              JRS-MOIS-3.

           MOVE OCO-NUMBER TO
                JRS-COMPLEMENT 
                JRS-COMPLEMENT-1
                JRS-COMPLEMENT-2
                JRS-COMPLEMENT-3

           MOVE OCC-KEY TO 
                JRS-OCCUPATION 
                JRS-OCCUPATION-1
                JRS-OCCUPATION-2
                JRS-OCCUPATION-3.
           READ JOURS INVALID INITIALIZE JRS-REC-DET.
           INSPECT PICK-TIME REPLACING ALL "." BY ",".
           INSPECT PICK-TIME REPLACING ALL "-" BY " ".
           MOVE PICK-TIME TO HELP-4.
           ADD HELP-4 TO JRS-HRS(PICK-JOUR).

           MOVE 0 TO JRS-HRS(32).
           PERFORM ADD-UP VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 31.
           IF JRS-HRS(32) > 0 
              IF LNK-SQL = "Y" 
                 CALL "9-JOURS" USING LINK-V JRS-RECORD WR-KEY 
              END-IF
              WRITE JRS-RECORD INVALID REWRITE JRS-RECORD END-WRITE   
           END-IF.

       ADD-UP.
           ADD JRS-HRS(IDX-1) TO JRS-HRS(32).

       WRITE-TRI.
           MOVE PICK-PERS TO TRIPR-PERSON REG-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF PRES-TOT(LNK-MOIS) = 0
              MOVE 0 TO REG-PERSON 
           END-IF.
           IF REG-PERSON = 0
              MOVE "?" TO PROB-TEST-P 
              MOVE 1 TO INPUT-ERROR 
              MOVE 0 TO TRIPR-PERSON 
           ELSE
              WRITE TRIPR-RECORD INVALID CONTINUE 
              NOT INVALID PERFORM CLEAN-OLD-TOTAL
              END-WRITE
           END-IF.

       CLEAN-OLD-TOTAL. 
           INITIALIZE JRS-RECORD.
           MOVE PICK-FIRME TO JRS-FIRME
           MOVE PICK-PERS  TO JRS-PERSON
           MOVE PICK-MOIS  TO JRS-MOIS
           PERFORM DELETE-JOURS.
           PERFORM DEL-JOURS VARYING IDX FROM 1 BY 1 UNTIL IDX > 100.

       DEL-JOURS.
           IF  OCC(IDX) NOT = 99
           AND OCC(IDX) NOT = 0
              MOVE OCC(IDX) TO JRS-OCCUPATION
              MOVE 0 TO JRS-COMPLEMENT
              PERFORM DELETE-JOURS
           END-IF.
           IF  OCC(IDX) = 0
           AND OCO(IDX) NOT = 0
              MOVE OCC(IDX) TO JRS-OCCUPATION
              MOVE OCO(IDX) TO JRS-COMPLEMENT
              PERFORM DELETE-JOURS
           END-IF.

       DELETE-JOURS.
           IF LNK-SQL = "Y" 
              CALL "9-JOURS" USING LINK-V JRS-RECORD DEL-KEY 
           END-IF.
           DELETE JOURS INVALID CONTINUE END-DELETE.

       WRITE-PROB.
           MOVE PICK-FIRME TO PROB-FIRME.
           MOVE PICK-PERS  TO PROB-PERS.
           MOVE PICK-ANNEE TO PROB-ANNEE.
           MOVE PICK-MOIS  TO PROB-MOIS.
           MOVE PICK-CODE  TO PROB-OCCUP.
           MOVE COMPTEUR TO PROB-LINE.
           WRITE PROB-RECORD.
           ADD 1 TO COMPTEUR-FAUTES.
           MOVE COMPTEUR-FAUTES TO HE-Z6.
           DISPLAY HE-Z6 LINE 20 POSITION 20.
           DISPLAY PICK-RECORD LINE 20 POSITION 30 SIZE 24.
           INITIALIZE PROB-RECORD.


       START-TRI.
           INITIALIZE TRIPR-RECORD.
           START TRIPR KEY > TRIPR-KEY INVALID CONTINUE
           NOT INVALID PERFORM READ-TRI THRU READ-TRI-END.
       READ-TRI.
           READ TRIPR NEXT AT END 
               GO READ-TRI-END
           END-READ.
           MOVE TRIPR-PERSON TO REG-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           IF REG-PERSON = 0
              GO READ-TRI.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           PERFORM DIS-HE-01.
           CALL "4-A" USING LINK-V PR-RECORD REG-RECORD.
           PERFORM MALADIE THRU MALADIE-END.
           DELETE TRIPR INVALID CONTINUE.
           GO READ-TRI.
       READ-TRI-END.
           EXIT.

       DIS-HE-01.
           MOVE REG-PERSON    TO HE-Z6.
           DISPLAY HE-Z6     LINE  7 POSITION 35.
           DISPLAY PR-NOM    LINE  7 POSITION 45.
           DISPLAY PR-PRENOM LINE  8 POSITION 45.

       AFFICHAGE-ECRAN.
           MOVE 2555 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           EXIT.

       END-PROGRAM.
           CLOSE JOURS.
           CLOSE TRIPR.
           DELETE FILE TRIPR.
           IF COMPTEUR-FAUTES > 0
              MOVE "SL" TO LNK-AREA
              MOVE 0 TO LNK-POSITION
              MOVE 46 TO LNK-NUM
              CALL "0-DMESS" USING LINK-V
              ACCEPT ACTION
           END-IF.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       MALADIE.
           INITIALIZE L-RECORD.
           MOVE FR-KEY     TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON. 
           MOVE LNK-MOIS   TO L-MOIS.
       MALADIE-1.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD NX-KEY.
           IF REG-PERSON NOT = L-PERSON
           OR LNK-MOIS   NOT = L-MOIS
              GO MALADIE-END.
           IF L-SUITE > 0
              MOVE L-SUITE TO LNK-SUITE 
              PERFORM DJT.
           GO MALADIE-1.
       MALADIE-END.
           MOVE 0 TO LNK-SUITE.

       DJT.
           CALL "4-DJT" USING LINK-V REG-RECORD.
           MOVE LNK-VAL   TO L-DATE-REPRISE.
           MOVE LNK-VAL-2 TO L-DATE-DERNIER.
           MOVE LNK-POSITION TO L-DATE-3M.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD WR-KEY.
