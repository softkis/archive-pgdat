      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 6-LIC MODULE GENERAL LECTURE LICENSES CLIENTS�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-LIC.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           SELECT OPTIONAL POCL ASSIGN TO RANDOM, "S-LIC",
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is PC-KEY,
                  ALTERNATE RECORD KEY is PC-KEY-ALPHA,
                  ALTERNATE RECORD KEY is PC-KEY-CODE DUPLICATES,
                  STATUS FS-POCL.

       DATA DIVISION.

       FILE SECTION.

           COPY "POCL.FDE".

       WORKING-STORAGE SECTION.

       01  ACTION         PIC X.
       01  NOT-OPEN       PIC 9 VALUE 0.
       01  LAST-POCL      PIC 9(8) VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "POCL.LNK".
                 
       01  A-NUM    PIC X.
       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD A-NUM EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON POCL.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-LIC.
       
           IF NOT-OPEN = 0
              OPEN I-O POCL
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO PC-RECORD.
           IF EXC-KEY = 99
              WRITE PC-RECORD INVALID REWRITE PC-RECORD END-WRITE
              EXIT PROGRAM
           END-IF.
           IF EXC-KEY = 98
              DELETE POCL INVALID CONTINUE END-DELETE
              EXIT PROGRAM
           END-IF.

           
           EVALUATE EXC-KEY 
           WHEN 65
               IF A-NUM = "N"
                  START POCL KEY < PC-KEY INVALID GO EXIT-1
               ELSE
                  START POCL KEY < PC-KEY-ALPHA INVALID GO EXIT-1
               END-IF
               READ POCL PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66
               IF A-NUM = "N"
                  START POCL KEY > PC-KEY INVALID GO EXIT-1
               ELSE
                  START POCL KEY > PC-KEY-ALPHA INVALID GO EXIT-1
               END-IF
               READ POCL NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER
               READ POCL NO LOCK INVALID INITIALIZE PC-RECORD
               END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE PC-RECORD TO LINK-RECORD.
           EXIT PROGRAM.



