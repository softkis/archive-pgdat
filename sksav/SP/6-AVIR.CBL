      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-AVIR MODULE GESTION ARCHIVE VIREMENT      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-AVIR.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "VIREMENT.FCA".

       DATA DIVISION.

       FILE SECTION.

           COPY "VIREMENT.FDE".

       WORKING-STORAGE SECTION.

       01  VIR-NAME.
           02 FILLER            PIC X(8) VALUE "Z-VIREM.".
           02 ANNEE-VIR         PIC 999.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  ACTION   PIC X.
       01  TODAY.
           02 TODAY-DATE.
              03 TODAY-ANNEE  PIC 9999.
              03 TODAY-MOIS   PIC 99.
              03 TODAY-JOUR   PIC 99.
           02 TODAY-TEMPS.
              03 TODAY-HEURE  PIC 99.
              03 TODAY-MIN    PIC 99.
              03 TODAY-SECS   PIC 9999.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "VIREMENT.LNK".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON VIREMENT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-AVIR.
       
           IF NOT-OPEN = 1
           AND LNK-SUFFIX NOT = ANNEE-VIR
              CLOSE VIREMENT
              MOVE 0 TO NOT-OPEN.

           IF NOT-OPEN = 0
              MOVE LNK-SUFFIX TO ANNEE-VIR
              OPEN I-O VIREMENT
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO VIR-RECORD.
           IF EXC-KEY = 98
           OR EXC-KEY = 99
              PERFORM WRITE-CAR
              EXIT PROGRAM
           END-IF.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ VIREMENT PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ VIREMENT NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 0 PERFORM START-3
               READ VIREMENT PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE VIR-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START VIREMENT KEY < VIR-A-KEY INVALID GO EXIT-1.
       START-2.
           START VIREMENT KEY > VIR-A-KEY INVALID GO EXIT-1.
       START-3.
           START VIREMENT KEY <= VIR-A-KEY INVALID GO EXIT-1.

       WRITE-CAR.
           MOVE VIR-KEY  TO VIR-A-KEY.
           MOVE LNK-USER TO VIR-AR-USER.
           CALL "0-TODAY" USING TODAY.
           MOVE TODAY    TO VIR-AR-TIME.
           IF EXC-KEY = 99
              MOVE "W" TO VIR-ACTION
           ELSE
              MOVE "D" TO VIR-ACTION
           END-IF.
           WRITE VIR-RECORD INVALID REWRITE VIR-RECORD END-WRITE.

           COPY "XMESSAGE.CPY".

