      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-COMPTA TRANSFERT COMPTABILITE             �
      *  � STANDARD PARAMETRABLE                                 �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-COMPTA.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "INTERCOM.FC".
           SELECT OPTIONAL TF-COMPTA ASSIGN TO DISK PARMOD-PATH4,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.


       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire JOURNAL COMPTABLE

       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-MOIS   PIC 99.
              03 INTER-SUITE  PIC 99.
              03 INTER-STAT   PIC 99.
              03 INTER-COUT   PIC 9(8).
              03 INTER-PERS   PIC 9(8).
              03 INTER-DC     PIC 9.
              03 INTER-COMPTE PIC X(30).
              03 INTER-POSTE  PIC 9(10).
              03 INTER-RUB    PIC 9(8).
              03 INTER-OBJET  PIC 9(8).
              03 INTER-DETAIL PIC 9(8).
              03 INTER-EXT    PIC X(26).
              03 INTER-LIB    PIC 9999.
              03 INTER-PERS-2 PIC 9(8).

           02 INTER-REC-DET.
              03 INTER-VALUE PIC S9(8)V99.
              03 INTER-UNITE PIC S9(8)V99.
              03 INTER-PERIODE PIC 99.

       FD  TF-COMPTA
           RECORD VARYING DEPENDING ON IDX-4
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 1500 DEPENDING IDX-4.


       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01  NOT-OPEN PIC 9 VALUE 0.
       01  DELIM    PIC X VALUE ";".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "COUT.REC".
           COPY "COMPTE.REC".
           COPY "LIBELLE.REC".
           COPY "LIVREX.REC".
           COPY "POCL.REC".
           COPY "RUB.REC".
           COPY "OBJET.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "STATUT.REC".
           COPY "MESSAGE.REC".

       01  INT-RECORD.
              02 TF-NO-FIRME  PIC 9(6).  
              02 TF-NOM-FIRME PIC X(30).  
              02 TF-AN-FIRME  PIC X(20).  
              02 TF-PERIODE   PIC 99.  
              02 TF-MOIS      PIC 99.  
              02 TF-ANNEE     PIC 9999.  
              02 TF-MONTANT   PIC Z(10),ZZ.
              02 TF-DEBIT     PIC Z(10),ZZ.
              02 TF-CREDIT    PIC Z(10),ZZ.
              02 TF-NO-LIB    PIC 9(4).  
              02 TF-LIBELLE   PIC X(30).  
              02 TF-COUT      PIC 9(10).  
              02 TF-COUT-NOM  PIC X(30).  
              02 TF-COUT-ANA  PIC X(30).  
              02 TF-COMPTE    PIC X(20).  
              02 TF-COMPTENOM PIC X(30).  
              02 TF-CONTRE    PIC X(20).  
              02 TF-DC        PIC X.  
              02 TF-SH        PIC X.  
              02 TF-PLUSMINUS PIC X.  
              02 TF-POSTE     PIC 9(10).  
              02 TF-POSTE-NOM PIC X(30).  
              02 TF-POSTE-AN  PIC X(20).  
              02 TF-RUBRIQUE  PIC 9(8).  
              02 TF-RUB-NOM   PIC X(30).  
              02 TF-RUB-COMPL PIC X(30).  
              02 TF-OBJET     PIC 9(8).  
              02 TF-OBJET-NOM PIC X(30).  
              02 TF-OBJET-COM PIC X(30).  
              02 TF-PERSONNE  PIC 9(8).  
              02 TF-PERS-NOM  PIC X(40).  
              02 TF-SUITE     PIC 99.  
              02 TF-LIGNE     PIC 9(6).  
              02 TF-UNITES    PIC 9999,99.
              02 TF-STATUT    PIC 9.  
              02 TF-STAT-NOM  PIC X(20).  
           02 TF-DATE-AMJ.
              03 TF-DATE-AA PIC 9999.
              03 TF-DATE-MM PIC 99.
              03 TF-DATE-JJ PIC 99.
           02 TF-DATE-JMA.
              03 TF-DATE-J  PIC 99.
              03 TF-DATE-M  PIC 99.
              03 TF-DATE-A  PIC 9999.
           02 TF-DATE-J2M2A2.
              03 TF-DATE-J2 PIC 99.
              03 FILLER     PIC X VALUE ".".
              03 TF-DATE-M2 PIC 99.
              03 FILLER     PIC X VALUE ".".
              03 TF-DATE-A2 PIC 99.

       01  HELP-RECORD PIC X(2000).


       01  HELP-XX.
           02 HELP-X                PIC 9(10)V99 OCCURS 2.
       
       01  INTER-NAME.
           02 INTER-NOM          PIC XXXX VALUE "INTC".
           02 FIRME-INTER        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-INTER         PIC XXX.

       01  ECR-DISPLAY.
           02 HE-Z8 PIC -Z(8),ZZ.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               TF-COMPTA
               INTER.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-COMPTA.
       
           IF MENU-PROG-NAME = "3-JM"
              MOVE "ICCM" TO INTER-NOM
           END-IF.
           CALL "0-TODAY" USING TODAY.
           MOVE FR-KEY TO FIRME-INTER.
           MOVE LNK-USER  TO USER-INTER.
           INITIALIZE COUT-RECORD.
           OPEN INPUT INTER.
           INITIALIZE HELP-XX.
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END GO READ-INTER-END.
           IF INTER-LIB = 0
              GO READ-INTER.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-COMPTA
              IF LNK-YN = "Y"
                 MOVE 1 TO IDX-4
                 INITIALIZE HELP-RECORD
                 MOVE "CO" TO LNK-AREA
                 PERFORM ENTETE VARYING IDX FROM 1 BY 1 UNTIL IDX > 20
                 WRITE TF-RECORD FROM HELP-RECORD
                 INITIALIZE HELP-RECORD
              END-IF
              MOVE 1 TO NOT-OPEN
           END-IF.
           PERFORM FILL-TEXTE.
           GO READ-INTER.
       READ-INTER-END.
           IF HELP-X(1) NOT = HELP-X(2)
              ACCEPT ACTION NO BEEP.
           PERFORM END-PROGRAM.

       FILL-TEXTE.
           INITIALIZE INT-RECORD.
           MOVE PARMOD-PATH3  TO TF-CONTRE
           MOVE FR-COMPTA     TO TF-CONTRE
           MOVE FR-KEY        TO TF-NO-FIRME  
           MOVE FR-NOM        TO TF-NOM-FIRME 
           MOVE FR-COMPTA     TO TF-AN-FIRME  
           MOVE INTER-PERIODE TO TF-PERIODE.
           MOVE LNK-ANNEE     TO TF-ANNEE.
           MOVE INTER-MOIS    TO TF-MOIS.
           IF INTER-MOIS = 0
              MOVE LNK-MOIS   TO TF-MOIS.
           MOVE INTER-COMPTE  TO TF-COMPTE COM-NUMBER.
           CALL "6-COMPTE" USING LINK-V COM-RECORD FAKE-KEY.
           MOVE COM-NOM TO TF-COMPTENOM.

           MOVE INTER-VALUE   TO TF-MONTANT.
           INSPECT TF-MONTANT REPLACING ALL " " BY "0".
           MOVE INTER-LIB     TO LIB-NUMBER TF-NO-LIB.
           CALL "6-LIB" USING LINK-V LIB-RECORD FAKE-KEY.
           MOVE LIB-DESCRIPTION TO TF-LIBELLE.
           IF MENU-HELP-TEXT = 99 
              MOVE INTER-LIB     TO LEX-NUMBER 
              CALL "6-LEX" USING LINK-V LEX-RECORD FAKE-KEY
              MOVE LEX-DESCRIPTION TO TF-LIBELLE.


           MOVE FR-KEY TO COUT-FIRME.
           MOVE INTER-COUT    TO COUT-NUMBER TF-COUT.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           MOVE COUT-NOM      TO TF-COUT-NOM  
           MOVE COUT-ANALYTIC TO TF-COUT-ANA  
           MOVE INTER-DC TO IDX.
           IF INTER-VALUE < 0
              ADD 2 TO IDX.
           EVALUATE IDX
              WHEN 2 THRU 3 
                 MOVE "C" TO TF-DC 
                 MOVE "H" TO TF-SH 
                 MOVE "-" TO TF-PLUSMINUS
                 MOVE 2 TO INTER-DC
                 MOVE INTER-VALUE TO TF-CREDIT
              WHEN OTHER
                 MOVE "D" TO TF-DC 
                 MOVE "S" TO TF-SH 
                 MOVE "+" TO TF-PLUSMINUS
                 MOVE 1 TO INTER-DC
                 MOVE INTER-VALUE TO TF-DEBIT
           END-EVALUATE.

           IF INTER-POSTE > 0
              MOVE INTER-POSTE TO TF-POSTE PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              MOVE PC-NOM        TO TF-POSTE-NOM
              MOVE PC-ANALYTIQUE TO TF-POSTE-AN
           END-IF.
           IF INTER-RUB > 0
              MOVE INTER-RUB TO TF-RUBRIQUE RUB-NUMBER
              CALL "6-RUB" USING LINK-V RUB-RECORD FAKE-KEY
              MOVE RUB-NOM        TO TF-RUB-NOM
              MOVE RUB-COMPLEMENT TO TF-RUB-COMPL
           END-IF.
           IF INTER-OBJET > 0
              MOVE INTER-OBJET TO TF-OBJET OB-NUMBER
              CALL "6-OBJET" USING LINK-V OB-RECORD FAKE-KEY
              MOVE OB-NOM        TO TF-OBJET-NOM
              MOVE OB-COMPLEMENT TO TF-OBJET-COM
           END-IF.
           IF INTER-PERS > 0
              MOVE INTER-PERS TO REG-PERSON TF-PERSONNE
              CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY
              MOVE REG-MATRICULE TO PR-MATRICULE
              CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY
              CALL "4-PRNOM"  USING LINK-V PR-RECORD
              MOVE LNK-TEXT TO TF-PERS-NOM
           END-IF.
           MOVE INTER-SUITE  TO TF-SUITE     
           ADD 1 TO PARMOD-PROG-NUMBER-1.
           MOVE PARMOD-PROG-NUMBER-1 TO TF-LIGNE. 
           MOVE INTER-UNITE TO TF-UNITES    
           IF INTER-STAT > 0
              MOVE INTER-STAT TO TF-STATUT STAT-CODE
              CALL "6-STATUT" USING LINK-V STAT-RECORD FAKE-KEY
              MOVE STAT-NOM TO TF-STAT-NOM  
           END-IF.
           MOVE TODAY-ANNEE TO TF-DATE-A TF-DATE-AA TF-DATE-A2.
           MOVE TODAY-MOIS  TO TF-DATE-M TF-DATE-MM TF-DATE-M2
           MOVE TODAY-JOUR  TO TF-DATE-J TF-DATE-JJ TF-DATE-J2.
           MOVE 1 TO IDX-4.
           PERFORM FILL-RECORD VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.

           INSPECT HELP-RECORD CONVERTING
           "굤닀뀑뙏뱚뼏꼦솞�" TO "eeeeaiioouuuaaOUA".
           ADD INTER-VALUE TO HELP-X(INTER-DC).
           WRITE TF-RECORD FROM HELP-RECORD.
           INITIALIZE HELP-RECORD.
           MOVE HELP-X(1) TO HE-Z8.
           DISPLAY HE-Z8 LINE 24 POSITION 40.
           MOVE HELP-X(2) TO HE-Z8.
           DISPLAY HE-Z8 LINE 24 POSITION 55.

       READ-COUT.

       END-PROGRAM.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
           CLOSE INTER.
           IF NOT-OPEN = 1
              CLOSE TF-COMPTA.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XACTION.CPY".


       FILL-RECORD.
           EVALUATE PARMOD-SELECT(IDX)
             WHEN  1 STRING TF-NO-FIRME  
             DELIMITED BY "*"  INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN  2 STRING TF-NOM-FIRME 
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN  3 STRING TF-AN-FIRME  
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN  4 STRING TF-PERIODE   
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN  5 STRING TF-MOIS      
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN  6 STRING TF-ANNEE     
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN  7 STRING TF-MONTANT   
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN  8 STRING TF-DEBIT
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN  9 STRING TF-CREDIT    
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 10 STRING TF-COMPTE    
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 11 STRING TF-COMPTENOM 
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 12 STRING TF-LIBELLE   
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 13 STRING TF-DC        
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 14 STRING TF-SH        
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 15 STRING TF-PLUSMINUS 
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 16 STRING TF-COUT      
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 17 STRING TF-COUT-NOM  
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 18 STRING TF-COUT-ANA  
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 22 STRING TF-POSTE     
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 23 STRING TF-POSTE-NOM 
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 24 STRING TF-POSTE-AN  
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 25 STRING TF-PERSONNE  
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 26 STRING TF-PERS-NOM  
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 27 STRING TF-SUITE     
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 28 STRING TF-LIGNE     
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 29 STRING TF-UNITES    
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 30 STRING TF-STATUT    
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 31 STRING TF-STAT-NOM  
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 32 STRING TF-DATE-JMA  
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 33 STRING TF-DATE-AMJ  
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 34 STRING TF-RUBRIQUE  
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 35 STRING TF-RUB-NOM   
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 36 STRING TF-RUB-COMPL 
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 37 STRING TF-OBJET     
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 38 STRING TF-OBJET-NOM 
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 39 STRING TF-OBJET-COM 
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 40 STRING TF-CONTRE   
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 41 STRING TF-NO-LIB   
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
             WHEN 42 STRING TF-DATE-J2M2A2
             DELIMITED BY SIZE INTO  HELP-RECORD  WITH POINTER IDX-4
           END-EVALUATE. 
           IF PARMOD-SELECT(IDX) > 0
              STRING DELIM 
              DELIMITED BY SIZE INTO HELP-RECORD WITH POINTER IDX-4
           END-IF.
             
       ENTETE.
           IF PARMOD-SELECT(IDX) > 0
              MOVE PARMOD-SELECT(IDX) TO MS-NUMBER
              CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY
              STRING MS-DESCRIPTION DELIMITED BY "   " 
              INTO HELP-RECORD WITH POINTER IDX-4
              STRING DELIM 
              DELIMITED BY SIZE INTO HELP-RECORD WITH POINTER IDX-4
           END-IF.
             