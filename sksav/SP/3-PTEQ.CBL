      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-PTEQ IMPRESSION PLAN DE TRAVAIL EQUIPE    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-PTEQ.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PLANS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "PLANS.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "215       ".
       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  PRECISION             PIC 9 VALUE 0.
       01  TIRET-TEXTE           PIC X VALUE "�".
       01  COIN-1                PIC X VALUE "�".
       01  COIN-2                PIC X VALUE "�".
       01  COIN-3                PIC X VALUE "�".
       01  COIN-4                PIC X VALUE "�".
       01  BARRE                 PIC X VALUE "�".
       01  CASE                  PIC X(11) VALUE "          �".
       01  HAUT                  PIC X(11) VALUE "컴컴컴컴컴�".
       01  BAS                   PIC X(11) VALUE "컴컴컴컴컴�".
       01  GAUCHE                PIC X VALUE "�".
       01  DROITE                PIC X VALUE "�".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".
           COPY "PRESENCE.REC".
           COPY "EQUIPE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "PLAGE.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(380) OCCURS 45.
       01  FORMULAIRE-R REDEFINES FORMULAIRE.
           02 FORM-LINES         PIC X(190) OCCURS 90.
       01  FORMULAIRES.
           02 FEUILLE   OCCURS 2.
              03 F-L            PIC X(215) OCCURS 45.


       01  PLANS-NAME.
           02 PLANS-ID           PIC X(8) VALUE "S-PLANS.".
           02 ANNEE-PLANS        PIC 999.

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".PEQ".

           COPY "V-VH00.CPY".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 999999.
       01  SAVE-PERSON           PIC 9(8) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  EQUIPE                PIC 99 VALUE 0.
       01  CUMUL                 PIC 99999V99.
       01  CUMUL-T               PIC 99999V99.
       01  MOIS-NUM              PIC 99.
       01  PR-NUM                PIC 9(8).
       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.
       01  JOUR-DEBUT            PIC 99 VALUE 1.
       01  JOUR-FIN              PIC 99 VALUE 31.
       01  JOUR-IDX              PIC 99.
       01  TEST-LINE             PIC 99.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HE-TEMP PIC ZZ.ZZ BLANK WHEN ZERO.
           02 HE-FIN.
              03 HE-X PIC X VALUE ":".
              03 HE-F PIC ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PLANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-PTEQ.
       
           MOVE LNK-SUFFIX TO ANNEE-PLANS.
           MOVE MOIS-JRS(LNK-MOIS) TO JOUR-FIN.
           OPEN INPUT PLANS.

           INITIALIZE LIN-NUM LIN-IDX.
           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-MOIS TO SAVE-MOIS MOIS-DEBUT MOIS-FIN.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7 THRU 8
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7 
           WHEN  8 PERFORM APRES-8 
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-7.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-07.

       APRES-8.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.


       APRES-7-A.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM JOUR-DEBUT
             WHEN 66 ADD 1 TO JOUR-DEBUT
           END-EVALUATE.
           IF JOUR-DEBUT < 1 
              MOVE 1 TO JOUR-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF JOUR-DEBUT > MOIS-JRS(LNK-MOIS)
              MOVE MOIS-JRS(LNK-MOIS) TO JOUR-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF   JOUR-DEBUT >  JOUR-FIN
           MOVE JOUR-DEBUT TO JOUR-FIN.
           PERFORM DIS-HE-07.

       APRES-8-A.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM JOUR-FIN
             WHEN 66 ADD 1 TO JOUR-FIN
           END-EVALUATE.
           IF JOUR-FIN < 1 
              MOVE 1 TO JOUR-FIN
              MOVE 1 TO INPUT-ERROR.
           IF JOUR-FIN > MOIS-JRS(LNK-MOIS)
              MOVE MOIS-JRS(LNK-MOIS) TO JOUR-FIN
              MOVE 1 TO INPUT-ERROR.
           IF JOUR-DEBUT > JOUR-FIN
              MOVE JOUR-DEBUT TO JOUR-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.
       
       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM FULL-PROCESS
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       FULL-PROCESS.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           PERFORM READ-PERSON THRU READ-EXIT.
       START-PR-END.
           EXIT.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-1
           END-IF.
           IF STATUT > 0 AND NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF COUT > 0 AND NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           PERFORM FILL-FILES VARYING MOIS-COURANT FROM MOIS-DEBUT 
                           BY 1 UNTIL MOIS-COURANT > MOIS-FIN.
           PERFORM DIS-HE-01.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE EQ-NUMBER TO HE-Z2.
           DISPLAY HE-Z2  LINE 11 POSITION 31.
           DISPLAY EQ-NOM LINE 11 POSITION 35.
       DIS-HE-07.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 19 POSITION 35.
           MOVE "MO" TO LNK-AREA.
           MOVE 19401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-08.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 35.
           MOVE "MO" TO LNK-AREA.
           MOVE 20401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1329 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           PERFORM SEPARE VARYING IDX FROM 1 BY 1 UNTIL IDX > 90.
           MOVE 191 TO COL-NUM.
           STRING COIN-2 DELIMITED BY SIZE
           INTO F-L(1, 3) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.
           MOVE 191 TO COL-NUM.
           STRING COIN-4 DELIMITED BY SIZE
           INTO F-L(1, 6) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.
           MOVE 191 TO COL-NUM.
           STRING BARRE DELIMITED BY SIZE
           INTO F-L(1, 4) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.
           MOVE 191 TO COL-NUM.
           STRING BARRE DELIMITED BY SIZE
           INTO F-L(1, 5) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.


           MOVE 1 TO COL-NUM.
           STRING COIN-1 DELIMITED BY SIZE
           INTO F-L(2, 3) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.
           MOVE 1 TO COL-NUM.
           STRING COIN-3 DELIMITED BY SIZE
           INTO F-L(2, 6) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.
           MOVE  0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L215" USING LINK-V FEUILLE(1).
           CALL "L215" USING LINK-V FEUILLE(2).
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       SEPARE.
           COMPUTE IDX-1 = IDX + 1.
           DIVIDE IDX-1 BY 2 GIVING IDX-3 REMAINDER IDX-4.
           ADD 1 TO IDX-4.
           MOVE FORM-LINES(IDX) TO F-L(IDX-4, IDX-3).

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "L215" USING LINK-V FORMULAIRE.
           CANCEL "L215".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XEQUIPE.CPY".
           IF EQUIPE = 0
              MOVE 1 TO INPUT-ERROR.
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
      *    COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XSEMNOM.CPY".


       FILL-FILES.
           INITIALIZE PT-RECORD.
           MOVE FR-KEY TO PT-FIRME.
           MOVE REG-PERSON TO PT-PERSON.
           MOVE MOIS-COURANT TO PT-MOIS.
           READ PLANS INVALID CONTINUE
                NOT INVALID 
                PERFORM DONNEES-PERSONNE.

       DONNEES-PERSONNE.
           PERFORM TEST-LIGNE.
           MOVE  0 TO DEC-NUM IDX-4 TEST-LINE.
           MOVE  6 TO CAR-NUM.
           MOVE  6 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           PERFORM FILL-LIGNE VARYING JOUR-IDX 
                FROM JOUR-DEBUT BY 1 UNTIL JOUR-IDX > JOUR-FIN.
           IF IDX-4 = 0
              INITIALIZE FORM-LINE(LIN-NUM)
           ELSE
               ADD 1 TO LIN-NUM
               MOVE  3 TO COL-NUM
               MOVE PR-NOM TO ALPHA-TEXTE
               PERFORM FILL-FORM
               ADD 1 TO COL-NUM
               MOVE PR-PRENOM TO ALPHA-TEXTE
               PERFORM FILL-FORM
               COMPUTE LIN-NUM = LIN-NUM + 1 + TEST-LINE
               MOVE 1 TO COL-NUM
               IF LIN-NUM < 42
                  PERFORM TIRET UNTIL COL-NUM > 378
               END-IF
               ADD 1 TO LIN-NUM
           END-IF.

       TEST-LIGNE.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= 42
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM 
           END-IF.
           IF LIN-NUM = 0
              MOVE LNK-LANGUAGE TO FORM-LANGUE
              PERFORM CREATE-FORM
              PERFORM ENTETE
              MOVE 7 TO LIN-NUM
           END-IF.


       ENTETE.
           MOVE  2 TO LIN-NUM.
           MOVE MENU-DESCRIPTION TO ALPHA-TEXTE
           MOVE 5 TO COL-NUM
           PERFORM FILL-FORM
           ADD 1 TO PAGE-NUMBER.
           MOVE  3 TO CAR-NUM.
           MOVE 200 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  3 TO CAR-NUM.
           MOVE 400 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE 300 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

      *    DATE EDITION
           MOVE  2 TO CAR-NUM.
           MOVE 111 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 114 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 117 TO COL-NUM.
           MOVE TODAY-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 50 TO COL-NUM.
           PERFORM MOIS-NOM.
           PERFORM FILL-FORM.
           MOVE 60 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 4 TO LIN-NUM.
           MOVE 5 TO COL-NUM.
           MOVE EQ-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           PERFORM JOUR-MOIS VARYING IDX FROM 1 BY 1 UNTIL 
                   IDX > MOIS-JRS(LNK-MOIS).
           MOVE 5 TO LIN-NUM.
           PERFORM SEMAINE-INDEX VARYING IDX FROM 1 BY 1 UNTIL 
                   IDX > MOIS-JRS(LNK-MOIS).

       SEMAINE-INDEX.
           COMPUTE COL-NUM = 28 + IDX * 11
           COMPUTE LNK-NUM = SEM-IDX(LNK-MOIS, IDX).
           PERFORM SEM-NOM.
           PERFORM FILL-FORM.

       JOUR-MOIS.
           COMPUTE COL-NUM = 31 + IDX * 11
           MOVE IDX TO VH-00.
           MOVE 2 TO CAR-NUM.
           PERFORM FILL-FORM.


       CREATE-FORM.
           INITIALIZE FORMULAIRE.
           MOVE 1 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           STRING COIN-1 DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

           PERFORM TIRET UNTIL COL-NUM > 377.
           STRING COIN-2 DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

           MOVE 2 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           STRING BARRE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.
           MOVE 378 TO COL-NUM.
           STRING BARRE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

           MOVE 3 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           STRING GAUCHE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.
           PERFORM TIRET UNTIL COL-NUM > 26.
           PERFORM TOPS VARYING IDX FROM 1 BY 1 UNTIL IDX > 32.
           SUBTRACT 1 FROM COL-NUM.
           STRING DROITE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

           MOVE 4 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           STRING BARRE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.
           MOVE 27 TO COL-NUM.
           PERFORM CASES VARYING IDX FROM 1 BY 1 UNTIL IDX > 32.
           MOVE 5 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           STRING BARRE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.
           MOVE 27 TO COL-NUM.
           PERFORM CASES VARYING IDX FROM 1 BY 1 UNTIL IDX > 32.

           MOVE 6 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           STRING COIN-3 DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.
           PERFORM TIRET UNTIL COL-NUM > 26.
           PERFORM BOTTOMS VARYING IDX FROM 1 BY 1 UNTIL IDX > 32.
           SUBTRACT 1 FROM COL-NUM.
           STRING COIN-4 DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       TOPS.
           STRING HAUT DELIMITED BY SIZE INTO FORM-LINE(LIN-NUM) POINTER 
           COL-NUM ON OVERFLOW CONTINUE END-STRING.

       BOTTOMS.
           STRING BAS DELIMITED BY SIZE INTO FORM-LINE(LIN-NUM) POINTER 
           COL-NUM ON OVERFLOW CONTINUE END-STRING.

       CASES.
           STRING CASE DELIMITED BY SIZE INTO FORM-LINE(LIN-NUM) POINTER 
           COL-NUM ON OVERFLOW CONTINUE END-STRING.

       FILL-LIGNE.
           IF PT-PLAGE(JOUR-IDX) > SPACES
              PERFORM READ-PLAGE.

       READ-PLAGE.
           COMPUTE COL-NUM = 28 + JOUR-IDX * 11
           MOVE PT-PLAGE(JOUR-IDX) TO PLG-KEY.
           CALL "6-PLAGE" USING LINK-V PLG-RECORD FAKE-KEY.
           IF EQUIPE = PLG-EQUIPE
              ADD 1 TO IDX-4
              MOVE PT-PLAGE(JOUR-IDX) TO ALPHA-TEXTE
              PERFORM FILL-FORM
              ADD 1 TO LIN-NUM
              COMPUTE COL-NUM = 27 + JOUR-IDX * 11
              MOVE PT-HR-D(JOUR-IDX, 1) TO VH-00 
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE PT-MIN-D(JOUR-IDX, 1) TO VH-00 
              PERFORM FILL-FORM
              MOVE 2 TO CAR-NUM
              ADD 2 TO COL-NUM
              MOVE PT-HR-F(JOUR-IDX, 1) TO VH-00 
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
              ADD 1 TO COL-NUM
              MOVE PT-MIN-F(JOUR-IDX, 1) TO VH-00 
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
              IF PT-HR-D(JOUR-IDX, 2) > 0
                 PERFORM 2-LIGNE
              END-IF
              SUBTRACT 1 FROM LIN-NUM
           END-IF.

       2-LIGNE.
           ADD 1 TO LIN-NUM
           COMPUTE COL-NUM = 27 + JOUR-IDX * 11
           MOVE PT-HR-D(JOUR-IDX, 2) TO VH-00 
           MOVE 2 TO CAR-NUM
           PERFORM FILL-FORM
           ADD 1 TO COL-NUM
           MOVE PT-MIN-D(JOUR-IDX, 2) TO VH-00 
           PERFORM FILL-FORM
           MOVE 2 TO CAR-NUM
           ADD 2 TO COL-NUM
           MOVE PT-HR-F(JOUR-IDX, 2) TO VH-00 
           MOVE 2 TO CAR-NUM
           PERFORM FILL-FORM
           ADD 1 TO COL-NUM
           MOVE PT-MIN-F(JOUR-IDX, 2) TO VH-00 
           MOVE 2 TO CAR-NUM
           PERFORM FILL-FORM.
           SUBTRACT 1 FROM LIN-NUM.
           MOVE 1 TO TEST-LINE.
       
