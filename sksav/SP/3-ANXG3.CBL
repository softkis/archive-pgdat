      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-ANXG3 IMPRESSION ANNEXE G3 REPAS          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-ANXG3.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM160.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM160.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  CHOIX-MAX             PIC 99 VALUE 6.
       01  PRECISION             PIC 9 VALUE 1.
       01  JOB-STANDARD          PIC X(10) VALUE "160       ".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CONTRAT.REC".
           COPY "CONGE.REC".
           COPY "PRESENCE.REC".
           COPY "STATUT.REC".
           COPY "COUT.REC".
           COPY "LIVRE.REC".
           COPY "V-VAR.CPY".
           COPY "IMPRLOG.REC".

       01  END-NUMBER            PIC  9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  DRAP-FORM             PIC     9 VALUE 0.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(160) OCCURS 45.

           COPY "V-VH00.CPY".

       01  FORM-NAME             PIC X(9) VALUE "ANNEXE.G3".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).

       01  TOTAUX.
           02 TOT                PIC 9(6)V99 OCCURS 4.


       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-ANXG3.

           MOVE LNK-MOIS TO SAVE-MOIS.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0000000025 TO EXC-KFR (1)
                      MOVE 1700000000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5
           WHEN  6 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN  5 PERFORM START-PERSON
                   PERFORM READ-PERSON 
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-PERSON.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              PERFORM END-PROGRAM.
           IF LNK-COMPETENCE < REG-COMPETENCE
              GO READ-PERSON
           END-IF.
           INITIALIZE CAR-RECORD CONGE-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF COUT > 0 AND NOT = CAR-COUT
              GO READ-PERSON
           END-IF.
           MOVE CAR-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           PERFORM LIVRE-ANNEE.
           PERFORM FULL-PROCESS.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
               GO READ-PERSON
           ELSE
               PERFORM END-PROGRAM.
                
       FULL-PROCESS.
           IF TOT(4) NOT = 0
              PERFORM ENTETE
              PERFORM TRANSMET.
           PERFORM DIS-HE-01.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.

       ENTETE.
           COMPUTE LIN-NUM =  3.
           MOVE 6 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
      * DONNEES FIRME
           COMPUTE LIN-NUM =  1.
           MOVE 20 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

      * DONNEES PERSONNE

           COMPUTE LIN-NUM = 3.
           MOVE  6 TO CAR-NUM.
           MOVE 54 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 2.
           PERFORM FILL-FORM.
           MOVE 20 TO COL-NUM.
           MOVE COUT-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.


      *    DATE EDITION
           COMPUTE LIN-NUM = 1.
           MOVE 116 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 119 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 122 TO COL-NUM.
           MOVE TODAY-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.



       LIVRE-ANNEE.
           MOVE 1 TO IDX-3.
           IF REG-ANCIEN-A = LNK-ANNEE
              MOVE REG-ANCIEN-M TO IDX-3
           END-IF.
           INITIALIZE DRAP-FORM TOTAUX.
           PERFORM ANNEE VARYING LNK-MOIS FROM IDX-3 BY 1 
                           UNTIL LNK-MOIS > 12.
           PERFORM FILL-TOTAL.

       ANNEE.
           INITIALIZE L-RECORD.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-FIRME NOT = 0
              IF COUNTER = 0 
                 PERFORM READ-IMPRIMANTE 
                 MOVE 1 TO COUNTER
              END-IF
              IF DRAP-FORM = 0
                 PERFORM READ-FORM
                 MOVE 1 TO DRAP-FORM
              END-IF
              IF L-REPAS NOT = 0
                 PERFORM FILL-NORMAL
              END-IF
           END-IF.

       FILL-NORMAL.
           COMPUTE LIN-NUM = 8 + LNK-MOIS * 2.

           COMPUTE VH-00 = L-NATURE.
           MOVE 49 TO COL-NUM.
           MOVE  5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.
           ADD VH-00 TO TOT(1).

           COMPUTE VH-00 = L-REPAS.
           MOVE 71 TO COL-NUM.
           MOVE  5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.
           ADD VH-00 TO TOT(2).

           COMPUTE VH-00 = L-IMPOT-SPECIAL-PP + L-IMPOT-FORFAIT-PP.
           MOVE 82 TO COL-NUM.
           MOVE  5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.
           ADD VH-00 TO TOT(3).

           COMPUTE VH-00 = L-IMPOT-SPECIAL-PP + L-REPAS 
                         + L-IMPOT-FORFAIT-PP - L-NATURE.
           MOVE 93 TO COL-NUM.
           MOVE  5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.
           ADD VH-00 TO TOT(4).

           
       FILL-TOTAL.
           COMPUTE LIN-NUM = 34.

           MOVE TOT(1) TO VH-00.
           MOVE  49 TO COL-NUM.
           MOVE   6 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.

           MOVE TOT(2) TO VH-00.
           MOVE  71 TO COL-NUM.
           MOVE   6 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.
           
           MOVE TOT(3) TO VH-00.
           MOVE  82 TO COL-NUM.
           MOVE   6 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.

           MOVE TOT(4) TO VH-00.
           MOVE  93 TO COL-NUM.
           MOVE   6 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L160O" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.


       AFFICHAGE-ECRAN.
           MOVE 1203 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "L160O" USING LINK-V FORMULAIRE.
           CANCEL "L160O".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XACTION.CPY".


