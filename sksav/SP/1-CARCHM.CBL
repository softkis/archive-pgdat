      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-CARCHM SAISIE RAPIDE CODE CHAMBRE METIERS �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-CARCHM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CARRIERE.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CARRIERE.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

           COPY "PERSON.REC".
           COPY "CCOL.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "MESSAGE.REC".
           COPY "METIER.REC".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 5.
       01  QUALIFICATION         PIC 9 VALUE 1.
       01  HELP-VAL              PIC Z BLANK WHEN ZERO.
       01  HELP-1                PIC 9999.
       01  HELP-2                PIC 9.
       01  YN                    PIC X.
       01  MOIS                  PIC 99.


       01  ECR-DISPLAY.
           02 HE-Z4   PIC Z(4).
           02 HE-Z5   PIC Z(5).
           02 HE-Z6   PIC Z(6).
           02 HE-Z2   PIC ZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CARRIERE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-CARCHM.
       
           OPEN I-O CARRIERE.
           INITIALIZE MET-RECORD.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0163640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6500000000 TO EXC-KFR(14)
                      MOVE 0000000005 TO EXC-KFR(1)
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6500000000 TO EXC-KFR(14)
           WHEN 3     MOVE 0029000000 TO EXC-KFR(1)
           WHEN 4     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN CHOIX-MAX
                      MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11)
           END-EVALUATE.


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.


           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 1 SIZE 80.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-DEC.

           IF EXC-KEY = 27
              MOVE 0 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                        MOVE 0 TO INPUT-ERROR
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION EXC-KEY CONTINUE.


       AVANT-3.
           ACCEPT MET-CODE-CH
             LINE 5 POSITION 20 SIZE 5
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-4.
           ACCEPT QUALIFICATION
             LINE  9 POSITION 24 SIZE 1
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  5 IF  MET-CODE-CH   > 0
                   AND QUALIFICATION > 0
                   AND REG-PERSON    > 0
                      PERFORM TRAITEMENT
                   END-IF
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE
              IF PRES-ANNEE = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           IF INPUT-ERROR = 0 
              PERFORM CAR-RECENTE
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN  5 CONTINUE
           WHEN  OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF REG-PERSON NOT = 0
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 PERFORM CAR-RECENTE
                 PERFORM AFFICHAGE-DETAIL
              END-IF
           ELSE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-3.
           MOVE "CM" TO MET-LANGUE LNK-TEXT.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-METIER" USING LINK-V MET-RECORD
                    IF MET-CODE NOT = SPACES
                       MOVE MET-CODE-CH TO CAR-METIER-CM
                    END-IF
                    MOVE SPACES TO LNK-TEXT
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER
                 CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY
           END-EVALUATE.
           IF MET-CODE-CH = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-03.

       APRES-4.
           MOVE QUALIFICATION TO MS-NUMBER.
           MOVE "OQ" TO LNK-AREA.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-MESS" USING LINK-V MS-RECORD
                    MOVE MS-NUMBER TO CAR-QUALIFICATION-CM QUALIFICATION
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "2-MESS"
           WHEN  OTHER PERFORM NEXT-MESSAGES
                 MOVE MS-NUMBER TO CAR-QUALIFICATION-CM QUALIFICATION
           END-EVALUATE.
           PERFORM DIS-HE-04.
           IF QUALIFICATION = 0
              MOVE 1 TO INPUT-ERROR QUALIFICATION
           END-IF.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM TRAITEMENT
                    MOVE 1 TO DECISION
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1.
           IF DECISION > 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           MOVE "A" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           PERFORM DISPLAY-MESSAGE.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF CAR-METIER-CM > 0
              MOVE CAR-METIER-CM TO MET-CODE-CH
           END-IF.
           IF CAR-QUALIFICATION-CM > 0
              MOVE CAR-QUALIFICATION-CM TO QUALIFICATION
           END-IF.


       TRAITEMENT.
           INITIALIZE CAR-RECORD.
           MOVE FR-KEY     TO CAR-FIRME.
           MOVE REG-PERSON TO CAR-PERSON.
           START CARRIERE KEY >= CAR-KEY INVALID CONTINUE
              NOT INVALID
              PERFORM C-C THRU C-C-END.
       C-C.
           READ CARRIERE NEXT NO LOCK AT END 
             GO C-C-END.
           IF FR-KEY     NOT = CAR-FIRME 
           OR REG-PERSON NOT = CAR-PERSON
             GO C-C-END
           END-IF.
           IF  CAR-METIER-CM > 0
           AND CAR-QUALIFICATION-CM > 0
              GO C-C.
           MOVE MET-CODE-CH   TO CAR-METIER-CM.
           MOVE QUALIFICATION TO CAR-QUALIFICATION-CM
           IF LNK-SQL = "Y" 
              CALL "9-CARRI" USING LINK-V CAR-RECORD WR-KEY 
           END-IF. 
           IF LNK-LOG = "Y" 
              CALL "6-ACARRI" USING LINK-V CAR-RECORD WR-KEY 
           END-IF. 
           REWRITE CAR-RECORD INVALID CONTINUE.
           GO C-C.
       C-C-END.
           EXIT.

       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
           DISPLAY CAR-QUALIFICATION-CM LINE 11 POSITION 24.

       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-03.
           MOVE "CM" TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           DISPLAY CAR-METIER-CM LINE 7 POSITION 20.
           MOVE MET-CODE-CH TO HE-Z5.
           DISPLAY HE-Z5 LINE 5 POSITION 20.
           DISPLAY MET-CHM LINE 5 POSITION 30.
       DIS-HE-04.
           MOVE QUALIFICATION TO MS-NUMBER.
           MOVE "OQ" TO LNK-AREA.
           CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY.
           DISPLAY QUALIFICATION  LINE 9 POSITION 24.
           DISPLAY MS-DESCRIPTION LINE 9 POSITION 30.

       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 222 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE CARRIERE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

