      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME X-CSFIX  REMISE CS FIXE                     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    X-CSFIX.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODFIX.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CODFIX.FDE".

       WORKING-STORAGE SECTION.
             

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON CODFIX.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  program.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-X-CSFIX.

           OPEN INPUT CODFIX.
           INITIALIZE CSF-RECORD.
           START CODFIX KEY >= CSF-KEY INVALID EXIT PROGRAM
              NOT INVALID
              PERFORM C-C THRU C-C-END.
       C-C.
           READ CODFIX NEXT NO LOCK AT END GO C-C-END.
           IF CSF-CODE = 650
           OR CSF-CODE = 651
              DISPLAY CSF-KEY LINE 1 POSITION 1
              PERFORM AA.
           GO C-C.
       C-C-END.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".

