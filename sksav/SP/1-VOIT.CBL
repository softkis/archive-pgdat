      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-VOIT GESTION DES VOITURES                 �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-VOIT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "VOITURE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "VOITURE.FDE".

       WORKING-STORAGE SECTION.

       01  CHOIX-MAX             PIC 99 VALUE 18.

       01  HE-INPUT-PARAMETER.
      *    LINE COL SIZE HE-SCREEN    LLCCSZECRA
           02 IP-101 PIC 9(10)  VALUE 0425081001.
           02 IP-102 PIC 9(10)  VALUE 0525081011.
           02 IP-103 PIC 9(10)  VALUE 0625010010.
           02 IP-104 PIC 9(10)  VALUE 0735020092.
           02 IP-105 PIC 9(10)  VALUE 0835020091.
           02 IP-106 PIC 9(10)  VALUE 0935020019.
           02 IP-107 PIC 9(10)  VALUE 1025400017.
           02 IP-108 PIC 9(10)  VALUE 1135020018.
           02 IP-109 PIC 9(10)  VALUE 1235020018.
           02 IP-110 PIC 9(10)  VALUE 1335020012.
           02 IP-111 PIC 9(10)  VALUE 1435020090.
           02 IP-112 PIC 9(10)  VALUE 1535020089.
           02 IP-113 PIC 9(10)  VALUE 1635020088.
           02 IP-114 PIC 9(10)  VALUE 1735020086.
           02 IP-115 PIC 9(10)  VALUE 1835020085.
           02 IP-116 PIC 9(10)  VALUE 1935020087.
           02 IP-117 PIC 9(10)  VALUE 2035020084.
           02 IP-DEC PIC 9(10)  VALUE 2335020099.

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
            03 I-PP OCCURS 18.
               04 IP-LINE  PIC 99.
               04 IP-COL   PIC 99.
               04 IP-SIZE  PIC 99.
               04 IP-ECRAN PIC 9999.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PAYS.REC".
           COPY "VOITURE.LNK".
           COPY "ORTKZ.REC".
           COPY "SECTEUR.REC".
           COPY "MESSAGE.REC".
           COPY "PARMOD.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CREDIT.REC".
           COPY "VM.REC".


       01  HE-SEL.
           02 H-S PIC X       OCCURS 20.

       01   ECR-DISPLAY.
            02 HE-Z2 PIC Z(2).
            02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4).
            02 HE-Z5 PIC Z(5).
            02 HE-Z6 PIC Z(6).
            02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON VOITURE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-1-VOIT.

           OPEN I-O VOITURE.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0029230015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0029000015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8 THRU 11
                      MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 16    MOVE 0063640015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN CHOIX-MAX
                      MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-10
           WHEN 11 PERFORM AVANT-11
           WHEN 12 PERFORM AVANT-12
           WHEN 13 PERFORM AVANT-13
           WHEN 14 PERFORM AVANT-14
           WHEN 15 PERFORM AVANT-15
           WHEN 16 PERFORM AVANT-16
           WHEN 17 PERFORM AVANT-17
           WHEN CHOIX-MAX PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 IF PAYS-CODE = "D"
                      PERFORM APRES-2 
                   END-IF
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  8 PERFORM APRES-8 
           WHEN  9 PERFORM APRES-9 
           WHEN 10 PERFORM APRES-10
           WHEN 11 PERFORM APRES-11
           WHEN 12 PERFORM APRES-12
           WHEN 13 PERFORM APRES-13
           WHEN 14 PERFORM APRES-14
           WHEN 15 PERFORM APRES-15
           WHEN 16 PERFORM APRES-16
           WHEN 17 PERFORM APRES-17
           WHEN  CHOIX-MAX
                   PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           IF VT-PAYS = " "
              MOVE PAYS-CODE TO VT-PAYS
           END-IF.
           ACCEPT VT-PAYS
             LINE  4 POSITION 25 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-2.
           IF VT-PAYS   = "D"
           OR PAYS-CODE = "D"
              IF VT-REGION = SPACES
                 MOVE OK-KEY TO VT-REGION
              END-IF
              ACCEPT VT-REGION
              LINE  5 POSITION 25 SIZE 5
              TAB UPDATE NO BEEP CURSOR 1
              CONTROL "UPPER"
              ON EXCEPTION EXC-KEY CONTINUE
              END-ACCEPT
           ELSE
              INITIALIZE VT-REGION
           END-IF.
            

       AVANT-3.
           ACCEPT VT-MATRICULE
             LINE 6 POSITION 25 SIZE 10
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT VT-PERSON 
             LINE  7 POSITION 21 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-5.
           ACCEPT VT-PROP
             LINE  8 POSITION 25 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-6.
           ACCEPT VT-MARQUE
             LINE 9 POSITION 25 SIZE 10
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
       AVANT-7.
           ACCEPT VT-MODELE
             LINE 10 POSITION 25 SIZE 20
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT VT-COULEUR-1
             LINE 11 POSITION 25 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY CONTINUE.

       AVANT-9.
           ACCEPT VT-COULEUR-2
             LINE 12 POSITION 25 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-10.
           ACCEPT VT-TYPE
             LINE 13 POSITION 25 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-11.
           ACCEPT VT-CARB
             LINE 14 POSITION 25 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-12.
           ACCEPT VT-PUISSANCE
             LINE 15 POSITION 24 SIZE 3
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-13.
           ACCEPT VT-PAX
             LINE 16 POSITION 25 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.


       AVANT-14.
           ACCEPT VT-CHASSIS
             LINE 17 POSITION 25 SIZE 25
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-15.
           ACCEPT VT-MOTEUR
             LINE 18 POSITION 25 SIZE 25
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-16.
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT VT-ASSUREUR
             LINE 19 POSITION 21 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-17.
           ACCEPT VT-ASSURANCE
             LINE 20 POSITION 25 SIZE 25
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *    02 VT-KEY-1.
      *      *03 VT-FIRME      *  PIC 9(6).
      *      *03 VT-NUMBER      * PIC 9(8).
      *      *03 VT-KEY.
      *      *   04 VT-PAYS      *PIC XXXX.
      *      *   04 VT-REGION     PIC XXXX.
      *      *   04 VT-MATRICULE  PIC X(20).

      *    02 VT-REC-DET.
      *      *03 VT-TYPE          PIC 9999.
      *      *03 VT-REMARQUE      PIC X(70).
      *      *03 VT-ASSUREUR      PIC 9(8).
      *      *03 VT-PAX           PIC 99.
      *      *03 VT-CARB          PIC 99.
      *      *03 VT-PUISSANCE     PIC 9999.
      *      *03 VT-MARQUE-NOM    PIC X(30).
      *      *03 VT-COLOR1-NOM    PIC X(20).
      *      *03 VT-COLOR2-NOM    PIC X(20).

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           IF VT-PAYS > SPACES
              MOVE VT-PAYS TO PAYS-CODE.
           PERFORM DIS-HE-01.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-PAYS" USING LINK-V PAYS-RECORD
                    CANCEL "2-PAYS"
                    IF PAYS-CODE > SPACES
                       MOVE PAYS-CODE TO VT-PAYS
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-PAYS
                MOVE PAYS-CODE TO VT-PAYS
           END-EVALUATE.
           IF VT-PAYS = SPACES MOVE 1 TO INPUT-ERROR.
           READ VOITURE NO LOCK INVALID INITIALIZE VT-RECORD 
           REG-RECORD END-READ.
           MOVE PAYS-CODE TO VT-PAYS.
           PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           IF VT-REGION > SPACES
              MOVE VT-REGION TO OK-KEY.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-ORTKZ" USING LINK-V OK-RECORD
                    IF OK-KEY > SPACES
                       MOVE OK-KEY TO VT-REGION
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "2-ORTKZ"
           WHEN   3 MOVE PAYS-CODE TO VT-PAYS 
                    CALL "2-VOIT" USING LINK-V VT-RECORD
                    MOVE VT-MARQUE TO VM-KEY
                    MOVE VT-REGION TO OK-KEY
                    MOVE VT-PERSON TO REG-PERSON
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "2-VOIT"
           WHEN   5 CALL "1-ORTKZ" USING LINK-V
                    IF LNK-TEXT > SPACES
                       MOVE LNK-TEXT TO VT-REGION OK-KEY
                    END-IF
                    CANCEL "1-ORTKZ"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   OTHER  PERFORM NEXT-ORTKZ
                  MOVE OK-KEY TO VT-REGION
           END-EVALUATE.
           MOVE OK-KEY TO VT-REGION.
           IF VT-REGION = SPACES MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-02.

       APRES-3.
           EVALUATE EXC-KEY
           WHEN   2 MOVE PAYS-CODE TO VT-PAYS 
                    CALL "2-VOIT" USING LINK-V VT-RECORD
                    MOVE VT-MARQUE TO VM-KEY
                    MOVE VT-REGION TO OK-KEY
                    MOVE VT-PERSON TO REG-PERSON
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "2-VOIT"
           WHEN 13  READ VOITURE NO LOCK INVALID 
           INITIALIZE VT-REC-DET END-READ
           WHEN OTHER PERFORM NEXT-VOIT.
           IF VT-PAYS > SPACES
              MOVE VT-PAYS TO PAYS-CODE.
           MOVE VT-MARQUE TO VM-KEY.
           IF VT-MATRICULE = SPACES MOVE 1 TO INPUT-ERROR.
           PERFORM AFFICHAGE-DETAIL.

       APRES-4.
           MOVE FR-KEY TO REG-FIRME.
           MOVE VT-PERSON TO REG-PERSON.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   IF REG-PERSON > 0
                      MOVE REG-PERSON TO VT-PERSON 
                   END-IF
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
           WHEN 13 IF REG-PERSON > 0
                      PERFORM NEXT-REGIS
                   END-IF
           WHEN OTHER PERFORM NEXT-REGIS
                   MOVE REG-PERSON TO VT-PERSON
           END-EVALUATE.
           PERFORM DIS-HE-04.

       APRES-5.
           MOVE "PO" TO LNK-AREA.
           MOVE VT-PROP TO MS-NUMBER.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-MESS" USING LINK-V MS-RECORD
                    CANCEL "2-MESS"
                    IF MS-NUMBER > 0
                       MOVE MS-NUMBER TO VT-PROP
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN  65 THRU 66 
                    MOVE "PO" TO LNK-AREA
                    PERFORM NEXT-MESSAGES
                    MOVE MS-NUMBER TO VT-PROP
           END-EVALUATE.
           PERFORM DIS-HE-05.

       APRES-6.
           MOVE VT-MARQUE TO VM-KEY.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-VM" USING LINK-V
                    IF LNK-TEXT > SPACES
                       MOVE LNK-TEXT TO VT-MARQUE
                    END-IF
                    CANCEL "1-VM"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN   2 CALL "2-VM" USING LINK-V VM-RECORD
                    CANCEL "2-VM"
                    IF VM-KEY NOT = SPACES
                       MOVE VM-KEY TO VT-MARQUE
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-VM
                      MOVE VM-KEY TO VT-MARQUE.
           IF VT-MARQUE = SPACES MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-06.

       APRES-8.
           MOVE VT-COULEUR-1 TO MS-NUMBER.
           EVALUATE EXC-KEY
           WHEN   2 MOVE "CL" TO LNK-AREA
                    CALL "2-MESS" USING LINK-V MS-RECORD
                    CANCEL "2-MESS"
                    IF MS-NUMBER > 0
                       MOVE MS-NUMBER TO VT-COULEUR-1
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN  65 THRU 66 
                    MOVE "CL" TO LNK-AREA
                    PERFORM NEXT-MESSAGES
                    MOVE MS-NUMBER TO VT-COULEUR-1
           END-EVALUATE.
           PERFORM DIS-HE-08.

       APRES-9.
           MOVE "CL" TO LNK-AREA.
           MOVE VT-COULEUR-2 TO MS-NUMBER.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-MESS" USING LINK-V MS-RECORD
                    CANCEL "2-MESS"
                    IF MS-NUMBER > 0
                       MOVE MS-NUMBER TO VT-COULEUR-2
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN  65 THRU 66 
                    MOVE "CL" TO LNK-AREA
                    PERFORM NEXT-MESSAGES
                    MOVE MS-NUMBER TO VT-COULEUR-2
           END-EVALUATE.
           PERFORM DIS-HE-09.
 
       APRES-10.
           MOVE "AU" TO LNK-AREA.
           MOVE VT-TYPE TO MS-NUMBER.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-MESS" USING LINK-V MS-RECORD
                    CANCEL "2-MESS"
                    IF MS-NUMBER > 0
                       MOVE MS-NUMBER TO VT-TYPE
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN  65 THRU 66 
                    MOVE "AU" TO LNK-AREA
                    PERFORM NEXT-MESSAGES
                    MOVE MS-NUMBER TO VT-TYPE
           END-EVALUATE.
           PERFORM DIS-HE-10.

       APRES-11.
           MOVE "CB" TO LNK-AREA.
           MOVE VT-CARB TO MS-NUMBER.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-MESS" USING LINK-V MS-RECORD
                    CANCEL "2-MESS"
                    IF MS-NUMBER > 0
                       MOVE MS-NUMBER TO VT-CARB
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN  65 THRU 66 
                    MOVE "CB" TO LNK-AREA
                    PERFORM NEXT-MESSAGES
                    MOVE MS-NUMBER TO VT-CARB
           END-EVALUATE.
           PERFORM DIS-HE-11.

       APRES-12.
           PERFORM DIS-HE-12.
       APRES-13.
           PERFORM DIS-HE-13.
       APRES-14.
           PERFORM DIS-HE-14.
       APRES-15.
           PERFORM DIS-HE-15.
       APRES-16.
           MOVE VT-ASSUREUR TO CR-NUMBER.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-CREDIT" USING LINK-V
                    IF LNK-VAL > 0
                       MOVE LNK-VAL TO CR-NUMBER VT-ASSUREUR 
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "1-CREDIT"
           WHEN   2 THRU 3 
                    IF EXC-KEY = 2 
                       MOVE "A" TO LNK-A-N
                    ELSE
                       MOVE "N" TO LNK-A-N
                    END-IF
                    CALL "2-CREDIT" USING LINK-V CR-RECORD
                    IF CR-NUMBER > 0
                       MOVE CR-NUMBER TO VT-ASSUREUR 
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "2-CREDIT"
           WHEN OTHER PERFORM NEXT-CREDIT
                      MOVE CR-NUMBER TO VT-ASSUREUR 
           END-EVALUATE.
           PERFORM DIS-HE-16.

       APRES-17.
           PERFORM DIS-HE-17.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 MOVE PAYS-CODE TO VT-PAYS
                    MOVE FR-KEY TO VT-FIRME
                    CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO VT-TIME
                    MOVE LNK-USER   TO VT-USER
                    IF LNK-SQL = "Y" 
                       CALL "9-VOIT" USING LINK-V VT-RECORD WR-KEY
                    END-IF
                    WRITE VT-RECORD INVALID 
                        REWRITE VT-RECORD
                    END-WRITE   
                    IF LNK-SQL = "Y"
                       CALL "9-VOIT" USING LINK-V VT-RECORD WR-KEY
                    END-IF
                    MOVE 1 TO DECISION
            WHEN  8 IF LNK-SQL = "Y"
                       CALL "9-VOIT" USING LINK-V VT-RECORD DEL-KEY
                    END-IF
                    DELETE VOITURE INVALID CONTINUE END-DELETE
                    INITIALIZE VT-RECORD
                    MOVE 1 TO DECISION
                    PERFORM AFFICHAGE-DETAIL
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION > 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-CREDIT.
           MOVE FR-KEY TO CR-FIRME CR-FIRME-A.
           CALL "6-CREDIT" USING LINK-V CR-RECORD "N" EXC-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-VOIT.
           MOVE PAYS-CODE TO VT-PAYS.
           CALL "6-VOIT" USING LINK-V VT-RECORD EXC-KEY.

       NEXT-ORTKZ.
           CALL "6-ORTKZ" USING LINK-V OK-RECORD EXC-KEY.

       NEXT-PAYS.
           MOVE LNK-LANGUAGE TO PAYS-LANGUE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD EXC-KEY.

       NEXT-VM.
           CALL "6-VM" USING LINK-V VM-RECORD EXC-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
 
       DIS-HE-01.
           IF VT-PAYS > SPACES
              MOVE VT-PAYS TO PAYS-CODE.
           DISPLAY PAYS-CODE LINE 4 POSITION 25.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.
           DISPLAY PAYS-NOM LINE 4 POSITION 32 SIZE 44 HIGH.
       DIS-HE-02.
           DISPLAY VT-REGION LINE  5 POSITION 25.
           IF VT-REGION > SPACES
              MOVE VT-REGION TO OK-KEY
              CALL "6-ORTKZ" USING LINK-V OK-RECORD FAKE-KEY.
           IF PAYS-CODE NOT = "D"
              INITIALIZE OK-RECORD.
           DISPLAY OK-NOM LINE 5 POSITION 32 SIZE 44 HIGH.
       DIS-HE-03.
           DISPLAY VT-MATRICULE LINE 6 POSITION 25.
       DIS-HE-04.
           MOVE VT-PERSON TO REG-PERSON HE-Z6.
           DISPLAY HE-Z6 LINE  7 POSITION 21 SIZE 6
           IF REG-PERSON > 0
              CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           IF REG-PERSON > 0
              DISPLAY LNK-TEXT LINE 7 POSITION 35 SIZE 45
           ELSE
              DISPLAY SPACES LINE 7 POSITION 35 SIZE 45.
       DIS-HE-05.
           MOVE VT-PROP TO MS-NUMBER LNK-NUM HE-Z4.
           DISPLAY HE-Z4 LINE 8 POSITION 23.
           MOVE "PO" TO LNK-AREA.
           MOVE 08352000 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-06.
           DISPLAY VT-MARQUE LINE 9 POSITION 25 SIZE 10.
           MOVE VT-MARQUE TO VM-KEY.
           CALL "6-VM" USING LINK-V VM-RECORD FAKE-KEY.
           DISPLAY VM-NOM LINE 9 POSITION 35.
       DIS-HE-07.
           DISPLAY VT-MODELE  LINE 10 POSITION 25.
       DIS-HE-08.
           MOVE VT-COULEUR-1 TO MS-NUMBER LNK-NUM HE-Z4.
           DISPLAY HE-Z4 LINE 11 POSITION 23.
           MOVE "CL" TO LNK-AREA.
           MOVE 11352000 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-09.
           MOVE VT-COULEUR-2 TO MS-NUMBER LNK-NUM HE-Z4.
           DISPLAY HE-Z4 LINE 12 POSITION 23.
           MOVE "CL" TO LNK-AREA.
           MOVE 12352000 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-10.
           MOVE VT-TYPE TO MS-NUMBER LNK-NUM HE-Z4.
           DISPLAY HE-Z4 LINE 13 POSITION 23.
           MOVE "AU" TO LNK-AREA.
           MOVE 13352000 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-11.
           MOVE VT-CARB TO MS-NUMBER LNK-NUM HE-Z4.
           DISPLAY HE-Z4 LINE 14 POSITION 23.
           MOVE "CB" TO LNK-AREA.
           MOVE 14352000 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-12.
           MOVE VT-PUISSANCE TO HE-Z4.
           DISPLAY HE-Z4 LINE 15 POSITION 23.
       DIS-HE-13.
           MOVE VT-PAX TO HE-Z4.
           DISPLAY HE-Z4 LINE 16 POSITION 23.
       DIS-HE-14.
           DISPLAY VT-CHASSIS LINE 17 POSITION 25.
       DIS-HE-15.
           DISPLAY VT-MOTEUR  LINE 18 POSITION 25.
       DIS-HE-16.
           MOVE VT-ASSUREUR TO CR-NUMBER HE-Z6.
           DISPLAY HE-Z6 LINE 19 POSITION 21 SIZE 6
           IF CR-NUMBER > 0
              CALL "6-CREDIT" USING LINK-V CR-RECORD "N" FAKE-KEY
              DISPLAY CR-NOM LINE 19 POSITION 35 SIZE 45
           ELSE
              DISPLAY SPACES LINE 19 POSITION 35 SIZE 45.
       DIS-HE-17.
           DISPLAY VT-ASSURANCE LINE 20 POSITION 25.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 32 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           MOVE "EE" TO LNK-AREA.
           MOVE 00042500 TO LNK-POSITION.
           PERFORM LIGNE VARYING IDX FROM 1 BY 1 UNTIL IDX > CHOIX-MAX.

       LIGNE.
           MOVE IDX TO HE-Z2.
           IF IDX < CHOIX-MAX
              DISPLAY HE-Z2 LINE IP-LINE(IDX) POSITION 1 LOW.
           MOVE IP-ECRAN(IDX) TO LNK-NUM.
           MOVE IP-LINE(IDX)  TO LNK-LINE.
           MOVE 4             TO LNK-COL.
           MOVE "L" TO LNK-LOW.
           CALL "0-DMESS" USING LINK-V.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CANCEL "2-VOIT".
           CANCEL "6-VOIT".
           CANCEL "2-VM".
           CANCEL "6-VM".
           CLOSE VOITURE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
             IF DECISION = 99 
                DISPLAY VT-USER LINE 24 POSITION 5
                DISPLAY VT-ST-JOUR  LINE 24 POSITION 15
                DISPLAY VT-ST-MOIS  LINE 24 POSITION 18
                DISPLAY VT-ST-ANNEE LINE 24 POSITION 21
                DISPLAY VT-ST-HEURE LINE 24 POSITION 30
                DISPLAY VT-ST-MIN   LINE 24 POSITION 33
                GO AVANT-DEC
             END-IF.
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

