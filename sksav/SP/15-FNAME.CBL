      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 15-FNAME REPRISE NOMS FICHIERS         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    15-FNAME.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.

           COPY "FILENAME.FC".

           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, LNK-TEXT-1
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.
      *袴袴袴袴袴袴�

       FILE SECTION.
      *컴컴컴컴컴컴
           COPY "FILENAME.FDE".

       FD  TF-TRANS
           LABEL RECORD STANDARD
           DATA RECORD TFP-RECORD.

       01  TFP-RECORD.
           02 TFP-REC-1    PIC X(120).
           02 TFP-REC-2 REDEFINES TFP-REC-1.
              03 TFP-ID     PIC X.
              03 TFP-FILLER PIC X(119).

       WORKING-STORAGE SECTION.
       01  ESPACE   PIC X VALUE SPACES.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FILENAME TF-TRANS.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-15-FNAME.

           INITIALIZE IDX FN-RECORD.
      *    OPEN I-O  FILENAME.
           OPEN OUTPUT FILENAME.
           OPEN INPUT TF-TRANS.
           PERFORM LECT-TRANS THRU END-TRANS.
           PERFORM END-PROGRAM.

       LECT-TRANS.
           INITIALIZE TFP-RECORD.
           READ TF-TRANS AT END GO END-TRANS.
           MOVE 0 TO IDX-3.
           INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS BEFORE "-".
           IF  IDX-3 > 50
           AND IDX-3 < 80
              GO LECT-TRANS.
           MOVE 1 TO IDX-3.
           IF LNK-TEXT-2 = "MATASS."
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "MATASS.".
           IF LNK-TEXT-2 = "MICROF."
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "MICROF.".
           IF LNK-TEXT-2 = "RMAS."
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "RMAS.".
           IF LNK-TEXT-2 = "SNREDO."
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "SNREDO.".
           IF LNK-TEXT-2 = "SNNT."
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "SNNT.".
           IF LNK-TEXT-2 = "PMNT."
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "PMNT.".
           IF LNK-TEXT-2 = "AFFRET"
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "AFFR".
           IF LNK-TEXT-2 = "INTRET"
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "INTR".
           IF LNK-TEXT-2 = "SALMAN."
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "SALMAN.".
           IF LNK-TEXT-2 = "SALRET."
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "SALRET.".
           IF LNK-TEXT-2 = "MALRET."
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "MALRET.".
           IF LNK-TEXT-2 = "CHAPRO."
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "CHAPRO.".
           IF LNK-TEXT-2 = "CALCUL."
              INSPECT TFP-RECORD TALLYING IDX-3 FOR CHARACTERS
              BEFORE "CALCUL.".
           IF IDX-3 > 50
              GO LECT-TRANS.
           UNSTRING TFP-RECORD INTO FN-NOM WITH POINTER IDX-3.
           ADD 1 TO IDX.
           MOVE IDX TO FN-NUMBER.
           MOVE LNK-AREA TO FN-AREA.
           WRITE FN-RECORD INVALID REWRITE FN-RECORD.
           INITIALIZE TFP-RECORD.
           GO LECT-TRANS.
       END-TRANS.
           CLOSE TF-TRANS.
           CLOSE FILENAME.

       END-PROGRAM.
           EXIT PROGRAM.


      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".

