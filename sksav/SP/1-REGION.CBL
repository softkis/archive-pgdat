      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-REGION GESTION DES REGIONS                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-REGION.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "REGION.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "REGION.FDE".

       WORKING-STORAGE SECTION.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  LANGUE                PIC XX. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON REGION.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-1-REGION.
       
           OPEN I-O REGION.

           PERFORM AFFICHAGE-ECRAN .
           MOVE LNK-LANGUAGE TO LANGUE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0029000022 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0100000005 TO EXC-KFR (1)
                      MOVE 0000080000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 THRU 3 PERFORM AVANT-DESCR
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT GEO-CODE
             LINE  4 POSITION 30 SIZE 3
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-DESCR.
           COMPUTE LIN-IDX = 3 + INDICE-ZONE.
           COMPUTE IDX = INDICE-ZONE - 1.
           ACCEPT GEO-DESC(IDX)
             LINE  LIN-IDX POSITION 20 SIZE 60
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY
           WHEN 2 CALL "2-REGION" USING LINK-V GEO-RECORD
                  CANCEL "2-REGION"
                  PERFORM AFFICHAGE-ECRAN
           WHEN 5 PERFORM COPIE
           WHEN OTHER CALL "6-REGION" USING LINK-V GEO-RECORD
                EXC-KEY.
           PERFORM AFFICHAGE-DETAIL.

       COPIE.
           MOVE GEO-CODE TO IDX-1.
           ACCEPT GEO-CODE
             LINE  4 POSITION 60 SIZE 3
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-REGION" USING LINK-V GEO-RECORD FAKE-KEY.
           MOVE IDX-1 TO GEO-CODE.
           
       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 MOVE GEO-CODE TO LNK-VAL
                    WRITE GEO-RECORD INVALID 
                        REWRITE GEO-RECORD
                    END-WRITE   
                    MOVE 1 TO DECISION
                    PERFORM AFFICHAGE-DETAIL
            WHEN  8 DELETE REGION INVALID CONTINUE
                    END-DELETE
                    INITIALIZE GEO-RECORD
                    MOVE 1 TO DECISION
                    PERFORM AFFICHAGE-DETAIL
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION > 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       DIS-HE-01.
           DISPLAY GEO-CODE   LINE  4 POSITION 30.
           MOVE GEO-CODE TO LNK-VAL.
       DIS-HE-02.
           DISPLAY GEO-DESC(1) LINE 5 POSITION 20.
       DIS-HE-03.
           DISPLAY GEO-DESC(2) LINE 6 POSITION 20.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 24 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE GEO-CODE TO LNK-VAL.
           CLOSE REGION.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
           
