      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-TVA-A IMPRESSION TVA FACTURE              �
      *  � PAR FOURNISSEUR DE MOIS DEBUT A MOIS FIN              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-TVA-A.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".
           COPY "FACTURE.FC".
           SELECT OPTIONAL TVACUM ASSIGN TO RANDOM, TVAC-NAME,
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is CUM-KEY,
                  STATUS FS-HELP.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM130.FDE".
           COPY "FACTURE.FDE".

       FD  TVACUM 
           LABEL RECORD STANDARD
           DATA RECORD CUM-RECORD.
      *  ENREGISTREMENT FICHIER CODE SALAIRE CUMULES 
      
       01  CUM-RECORD.
           02 CUM-KEY.
              03 CUM-TVA      PIC 99.
              03 CUM-ETR      PIC 9.

           02 CUM-REC-DET.
              03 CUM-COMPTEUR PIC 9(4).
              03 CUM-UNITE    PIC 9(9)V999.
              03 CUM-TOTAL    PIC 9(9)V999.
              03 CUM-TT       PIC 9(9)V999.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "130      ".
       01  CHOIX-MAX             PIC 99 VALUE 11.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "CREDIT.REC".
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.
       01  MOYENNE               PIC 9(8)V999.

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  LAST-CREDIT             PIC 9(6) VALUE 0.
       01  UNITES                PIC X VALUE "N".
       01  COMPTEUR              PIC 9999 COMP-1.


       01  TEST-EXTENSION.
           04 TEST-ALPHA  PIC X(10).
           04 TEST-NUMBER PIC 9(10).

           COPY "V-VH00.CPY".
       01  FACTURE-NAME.
           02 IDENTITE           PIC X(4) VALUE "A-AC".
           02 FIRME-FACTURE      PIC 9999.

       01  FORM-NAME.
           02 FILLER             PIC X(4) VALUE "FORM".
           02 FORM-LANGUE        PIC X.
           02 FORM-EXTENSION     PIC X(4) VALUE ".LCD".

       01  TVAC-NAME.
           02 FILLER             PIC XXXX VALUE "TVAC".
           02 FIRME-TVAC         PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-TVAC          PIC XXX.

           COPY "V-VAR.CPY".
        
       01  CLIENT                PIC 9(6) VALUE 0.
       01  BEG-CREDIT              PIC 9(6).
       01  BEG-MATCHCODE         PIC X(10).
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM FACTURE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-TVA-A.
       
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           MOVE LNK-MOIS TO SAVE-MOIS MOIS-DEBUT MOIS-FIN.
           MOVE LNK-USER TO USER-TVAC.
           MOVE FR-KEY   TO FIRME-TVAC FIRME-FACTURE.
           OPEN INPUT FACTURE.
           DELETE FILE TVACUM.
           OPEN I-O TVACUM.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE TEST-EXTENSION.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�



       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7 THRU 8
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 11    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN 11 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-8
           WHEN 11 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-7.
           ACCEPT MOIS-DEBUT
             LINE 15 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT MOIS-FIN
             LINE 17 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-07.

       APRES-8.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 THRU 6
      *             MOVE 1 TO LNK-STATUS
                    IF EXC-KEY = 6
                       MOVE 1 TO UNITES
                    END-IF
                    PERFORM TRAITEMENT-RUN
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       START-CREDIT.
           MOVE FR-KEY TO CR-FIRME CR-FIRME-A.
           IF CR-NUMBER > 0 
              SUBTRACT 1 FROM CR-NUMBER.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-CREDIT.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-CREDIT.
           IF CR-NUMBER = 0
           OR CR-NUMBER > END-NUMBER
           OR CR-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-CREDIT-1.
           PERFORM DIS-HE-01.
           PERFORM TOTAL-FACTURE.
       READ-CREDIT-2.
           CONTINUE.
       READ-CREDIT-3.
           IF  CR-NUMBER    < END-NUMBER
           AND CR-MATCHCODE < END-MATCHCODE
              GO READ-CREDIT
           END-IF.
       READ-EXIT.
           PERFORM WRITE-LIST THRU WRITE-LIST-END.


       NEXT-CREDIT.
           CALL "6-CREDIT" USING LINK-V CR-RECORD A-N EXC-KEY.

       FILL-POINTS.
           MOVE 1 TO POINTS.
           MOVE 5 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

       DIS-HE-01.
           MOVE CR-NUMBER TO HE-Z8.
           DISPLAY HE-Z8     LINE  6 POSITION 25.
           DISPLAY CR-NOM    LINE  6 POSITION 46 SIZE 35.
           MOVE END-NUMBER TO HE-Z8.
           DISPLAY HE-Z8     LINE  7 POSITION 25.

       DIS-HE-07.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 15 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 15351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-08.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 17 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 17351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 1513 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE TVACUM.
           DELETE FILE TVACUM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXC".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".


       TRAITEMENT-RUN.
           MOVE CR-NUMBER    TO BEG-CREDIT.
           MOVE CR-MATCHCODE TO BEG-MATCHCODE.
           PERFORM START-CREDIT.
           PERFORM READ-CREDIT THRU READ-EXIT.
           PERFORM END-PROGRAM.


       GET-FAL.
           INITIALIZE IDX-1.
           PERFORM READ-FAL THRU READ-FAL-END.

       READ-FAL.
           ADD 1 TO IDX-1.
           IF IDX-1 = 20
              GO READ-FAL-END
           END-IF.
           IF FAC-BTVA(IDX-1) > 0
              PERFORM COMPTER-ENREGISTRER.
           GO READ-FAL.
       READ-FAL-END.
           EXIT.
           
       COMPTER-ENREGISTRER.
           PERFORM DIS-HE-01.
           INITIALIZE CUM-RECORD.
           MOVE IDX-1 TO CUM-TVA.
           PERFORM ENREGISTRER.
           MOVE 9999 TO CUM-TVA.
           PERFORM ENREGISTRER.

       ENREGISTRER.
           READ TVACUM INVALID INITIALIZE CUM-REC-DET END-READ.
           ADD 1 TO CUM-COMPTEUR COMPTEUR CUM-UNITE.
           ADD FAC-BTVA(IDX-1) TO CUM-TOTAL.
           ADD FAC-TVA(IDX-1)  TO CUM-TT.
           WRITE CUM-RECORD INVALID REWRITE CUM-RECORD.

       WRITE-LIST.
           INITIALIZE CUM-RECORD.
           START TVACUM KEY > CUM-KEY INVALID KEY
                GO WRITE-LIST-END.

       READ-TVACUM.
           READ TVACUM NEXT AT END 
                IF LIN-NUM > LIN-IDX
                   PERFORM PAGE-DATE
                   PERFORM TRANSMET
                END-IF
                GO WRITE-LIST-END
           END-READ.
           IF COUNTER = 0 
              MOVE 65 TO IMPL-MAX-LINE
              MOVE 0 TO LIN-NUM
              ADD 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= IMPL-MAX-LINE
              PERFORM PAGE-DATE
              PERFORM TRANSMET
              INITIALIZE LIN-NUM 
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           PERFORM FILL-TEXTE.
           ADD 1 TO LIN-NUM.
           GO READ-TVACUM.
       WRITE-LIST-END.
           PERFORM END-PROGRAM.

       FILL-TEXTE.
           ADD 1 TO LIN-NUM.
           MOVE 1 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE CUM-TVA TO VH-00.
           IF CUM-TVA NOT = 99
              PERFORM FILL-FORM
           ELSE 
              MOVE 0 TO CAR-NUM
              MOVE "SOMME" TO ALPHA-TEXTE
           END-IF.


           MOVE 35 TO COL-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE CUM-COMPTEUR  TO VH-00.
           PERFORM FILL-FORM.

           DIVIDE CUM-TOTAL BY CUM-COMPTEUR GIVING MOYENNE.
           MOVE 39 TO COL-NUM.
           MOVE 8 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE MOYENNE  TO VH-00.     
           PERFORM FILL-FORM.
           MOVE 0 TO MOYENNE.
           IF CUM-UNITE > 0
              DIVIDE CUM-TOTAL BY CUM-UNITE GIVING MOYENNE.
           MOVE 50 TO COL-NUM.
           MOVE 6 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE MOYENNE  TO VH-00.     
           PERFORM FILL-FORM.

           MOVE 60 TO COL-NUM.
           MOVE 6 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE CUM-UNITE  TO VH-00.     
           PERFORM FILL-FORM.
           MOVE 69 TO COL-NUM.
           MOVE 8 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE CUM-TOTAL TO VH-00.    
           PERFORM FILL-FORM.
           MOVE 79 TO COL-NUM.
           MOVE 8 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE CUM-TT TO VH-00.    
           PERFORM FILL-FORM.
        
       PAGE-DATE.
           MOVE  3 TO LIN-NUM .
           MOVE 51 TO COL-NUM.
           MOVE MOIS-DEBUT TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           PERFORM FILL-FORM.
           IF MOIS-DEBUT NOT = MOIS-FIN
              MOVE MOIS-FIN TO LNK-NUM
              PERFORM MOIS-NOM-1
              ADD 2 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.
           ADD 2 TO COL-NUM.
           MOVE LNK-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  2 TO LIN-NUM .
           ADD 1 TO PAGE-NUMBER.
           MOVE  4 TO CAR-NUM.
           MOVE 73 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  5 TO LIN-NUM.
           MOVE 67 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 70 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 73 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

      *FILL-FIRME.
           MOVE FR-NOM TO ALPHA-TEXTE.
           MOVE 3 TO LIN-NUM.
           MOVE 11 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE FR-KEY TO VH-00.
           MOVE 4 TO CAR-NUM.
           MOVE 4 TO COL-NUM.
           PERFORM FILL-FORM.


       TOTAL-FACTURE.
           INITIALIZE FAC-RECORD.
           MOVE CR-NUMBER  TO FAC-CLIENT.
           START FACTURE KEY > FAC-KEY-2 INVALID
              PERFORM READ-END
              GO TOTAL-FACTURE-END.
           PERFORM READ-FACTURE THRU READ-END.
       TOTAL-FACTURE-END.
           EXIT.

       READ-FACTURE.
           READ FACTURE NEXT NO LOCK AT END 
              GO READ-END.
           IF CR-NUMBER NOT = FAC-CLIENT
              GO READ-END.
           IF LNK-ANNEE < FAC-ANNEE
              GO READ-FACTURE.
           IF LNK-ANNEE > FAC-ANNEE
              GO READ-FACTURE.
           IF MOIS-DEBUT > FAC-MOIS 
              GO READ-FACTURE.
           IF MOIS-FIN < FAC-MOIS 
              GO READ-FACTURE.
           PERFORM GET-FAL.
           GO READ-FACTURE.
       READ-END.
