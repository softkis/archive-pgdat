      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-PRMSOC IMPRESSION / CREATION SOCOM        �
      *  � EX        3-PRIME0 Valable pour septembre 2008        �
      *  � PRIME ABSENTEISME 135 euro pour < 3 absences          �
      *  � 50% si plus - pr굎ence au 1.1.2008 (anciennet�)       �
      *  � cong� parental = prorata temporis                     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-PRMSOC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM130.FC".
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM130.FDE".
           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  PRECISION             PIC 9 VALUE 0.
       01  JOB-STANDARD          PIC X(10) VALUE "130       ".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "TAUXCC.REC".
           COPY "STATUT.REC".
           COPY "LIVRE.REC".
           COPY "COUT.REC".
           COPY "CODPAR.REC".
           COPY "CODPAIE.REC".
           COPY "IMPRLOG.REC".
           COPY "LIVRE.CUM".
           COPY "CCOL.REC".
           COPY "CALEN.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
SU         COPY "CODTXT.REC".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  TIRET-TEXTE           PIC X VALUE "�".

       01  ACCIDENT              PIC 9 VALUE 0.    
       01  PERIODE               PIC 99 VALUE 0.    
       01  JRS-INTERRUPTION      PIC 999.    
       01  CREATE-CODE           PIC X VALUE "N".
       01  SAVE-CODE             PIC X VALUE "N".

       01  HELP-CUMUL.
           02 H-J OCCURS 12.
              03 HR-TOT-JOUR     PIC 99V99 OCCURS 31.

       01  HELP-PERIODES.
           02 H-P OCCURS 30.
              03 JOUR-DEBUT      PIC 99.
              03 MOIS-DEBUT      PIC 99.
              03 JOUR-FIN        PIC 99.
              03 MOIS-FIN        PIC 99.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".PFA".
       01  JOURS-NAME.
           02 JOURS-ID           PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z2Z2 PIC ZZ,ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS FORM. 

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-PRMSOC.

           MOVE 1 TO STATUT.
           PERFORM AFFICHAGE-ECRAN .
           CALL "0-TODAY" USING TODAY.
           CALL "6-GCP" USING LINK-V CP-RECORD.
           MOVE FR-PRIME TO LNK-NUM HE-Z2.
           DISPLAY HE-Z2 LINE 18 POSITION 4.
           MOVE "PF" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           DISPLAY LNK-TEXT LINE 18 POSITION 9 SIZE 40.

           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           OPEN INPUT JOURS.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.

           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT CREATE-CODE
             LINE 16 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION EXC-KEY CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           IF CREATE-CODE = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           MOVE " " TO LNK-YN.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           MOVE LNK-YN TO SAVE-CODE.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           IF REG-ANCIEN-A < 2008
              CONTINUE
           ELSE
              IF REG-ANCIEN-M > 1
              OR REG-ANCIEN-J > 1
                 GO READ-PERSON-1
              END-IF
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM CAR-RECENTE.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           MOVE 0 TO IDX-4.
           PERFORM JRS-PRORATA VARYING IDX FROM 1 BY 1 UNTIL IDX > 8.
           INITIALIZE ACCIDENT.
           PERFORM CUMUL-JOURS.
           PERFORM FULL-PROCESS.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.
           PERFORM END-PROGRAM.
                
       FULL-PROCESS.
           PERFORM TEST-LINE.
           PERFORM FILL-FILES.
           PERFORM DIS-HE-01.

       TEST-LINE.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM FILL-FIRME
              COMPUTE LIN-NUM = LIN-IDX
           END-IF.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           MOVE CCOL-CALENDRIER TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.

       LIVRE.
           ADD 1 TO LNK-MOIS.
           CALL "4-LPCUM" USING LINK-V REG-RECORD LCUM-RECORD.
           SUBTRACT 1 FROM LNK-MOIS.

       CUMUL-JOURS.
           INITIALIZE JRS-RECORD HELP-CUMUL.
           MOVE FR-KEY TO JRS-FIRME-2.
           MOVE REG-PERSON TO JRS-PERSON-2.
           MOVE 1 TO JRS-OCCUPATION-2.
           START JOURS KEY >= JRS-KEY-2 INVALID CONTINUE
                NOT INVALID
                PERFORM READ-HEURES THRU READ-HEURES-END.

       READ-HEURES.
           READ JOURS NEXT AT END
               GO READ-HEURES-END.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR JRS-MOIS > LNK-MOIS
              GO READ-HEURES-END
           END-IF.
           IF JRS-COMPLEMENT NOT = 0
           OR JRS-OCCUPATION =  0
           OR JRS-OCCUPATION = 10
           OR JRS-OCCUPATION > 11
              GO READ-HEURES
           END-IF.
           INITIALIZE L-RECORD 
           MOVE JRS-MOIS TO L-MOIS.
           MOVE JRS-OCCUPATION TO L-SUITE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-FLAG-ACCIDENT NOT = 0
              MOVE 1 TO ACCIDENT
           END-IF.
           IF L-FLAG-ACCIDENT NOT = 0
           OR L-FLAG-HOPITAL  NOT = 0
           OR L-FLAG-AGREE    NOT = 0
              GO READ-HEURES
           END-IF.
           PERFORM ADD-HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.
           GO READ-HEURES.
       READ-HEURES-END.
           INITIALIZE HELP-PERIODES PERIODE JRS-INTERRUPTION.
           PERFORM T-P VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > LNK-MOIS.

       ADD-HEURES.
           IF JRS-HRS(IDX) NOT = 0
              MOVE JRS-HRS(IDX) TO HR-TOT-JOUR(JRS-MOIS, IDX) 
           END-IF.

       T-P.
           PERFORM TEST-JOURS 
           VARYING IDX FROM 1 BY 1 UNTIL IDX > MOIS-JRS(IDX-1).

       TEST-JOURS.
           IF HR-TOT-JOUR(IDX-1, IDX)  = 0
              IF  CAL-JOUR(IDX-1, IDX) NOT = 1
              AND SEM-IDX(IDX-1, IDX)  < 6
                ADD 1 TO JRS-INTERRUPTION
              END-IF
           ELSE
              IF JRS-INTERRUPTION > 1
                 ADD 1 TO PERIODE
              END-IF
              IF PERIODE = 0
                 ADD 1 TO PERIODE
              END-IF
              IF JOUR-DEBUT(PERIODE) = 0
                 MOVE IDX   TO JOUR-DEBUT(PERIODE) 
                 MOVE IDX-1 TO MOIS-DEBUT(PERIODE) 
              END-IF
              MOVE IDX   TO JOUR-FIN(PERIODE) 
              MOVE IDX-1 TO MOIS-FIN(PERIODE) 
              MOVE 0 TO JRS-INTERRUPTION
           END-IF.

       JRS-PRORATA.
           ADD PRES-TOT(IDX) TO IDX-4.

       FILL-FILES.

      * DONNEES PERSONNE
           MOVE  0 TO DEC-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  6 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           PERFORM LIVRE.

           INITIALIZE CSP-RECORD CS-RECORD.
           MOVE 334 TO CD-NUMBER CSP-CODE.
           CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY.
SU         CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           PERFORM VAL-PREDEFINIE VARYING IDX FROM 1 BY 1 UNTIL IDX > 4.
           MOVE CSP-DONNEE(1) TO VH-00.
           MOVE 55 TO COL-NUM.
           MOVE  5 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE CSP-DONNEE(2) TO VH-00.
           MOVE 45 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           PERFORM FILL-FORM.

           IF PERIODE > 3
              MOVE 50 TO CSP-DONNEE(3)
           END-IF.
           MOVE CSP-DONNEE(3) TO VH-00
           MOVE 100 TO COL-NUM
           MOVE 3 TO CAR-NUM
           PERFORM FILL-FORM
           ADD 1 TO COL-NUM
           MOVE " %" TO ALPHA-TEXTE
           PERFORM FILL-FORM.
           MOVE IDX-4   TO CSP-DONNEE(4).
           MOVE CSP-DONNEE(4) TO VH-00.
           MOVE 70 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           PERFORM FILL-FORM.
           MOVE PERIODE TO CSP-DONNEE(5).
           MOVE 6 TO LNK-NUM
           CALL "4-NOTPOL" USING LINK-V CS-RECORD CSP-RECORD.
           MOVE CSP-DONNEE(6) TO VH-00
           MOVE 109 TO COL-NUM
           MOVE 5 TO CAR-NUM
           MOVE 2 TO DEC-NUM
           PERFORM FILL-FORM.

      *    absence volontaire
      *    IF LCUM-UNITE(1, 40) NOT = 0
      *       MOVE 0 TO PRIME 
      *       MOVE "- ! -" TO ALPHA-TEXTE
      *       MOVE 88 TO COL-NUM
      *       PERFORM FILL-FORM
      *    END-IF.

           IF JOUR-DEBUT(1) NOT = 0
              ADD 1 TO LIN-NUM
              PERFORM FILL-HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 7
              PERFORM FILL-DECOMPTE
           END-IF.
           MOVE 5 TO COL-NUM.
           ADD 1 TO LIN-NUM.
           IF LIN-NUM < 70
              PERFORM TIRET UNTIL COL-NUM > 124.
           ADD 1 TO LIN-NUM.
           IF CREATE-CODE  NOT = "N" 
              PERFORM WRITE-CS.

       VAL-PREDEFINIE.
           MOVE CS-VAL-PREDEFINIE(IDX) TO CSP-DONNEE(IDX).

       WRITE-CS.
           IF CSP-TOTAL NOT = 0 
              CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY
           ELSE
              CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD DEL-KEY
           END-IF.
           CALL "4-RECAL" USING LINK-V REG-RECORD PRESENCES.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       FILL-HEURES.
           IF JOUR-DEBUT(IDX)  NOT = 0
              COMPUTE COL-NUM = 8 + (IDX - 1) * 15
              MOVE JOUR-DEBUT(IDX) TO VH-00
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
              MOVE "." TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE MOIS-DEBUT(IDX) TO VH-00
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
      *       ADD 1 TO COL-NUM
              MOVE "<>" TO ALPHA-TEXTE
              PERFORM FILL-FORM
      *       ADD 1 TO COL-NUM
              MOVE JOUR-FIN(IDX) TO VH-00
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
              MOVE "." TO ALPHA-TEXTE
              PERFORM FILL-FORM
              MOVE MOIS-FIN(IDX) TO VH-00
              MOVE 2 TO CAR-NUM
              PERFORM FILL-FORM
           END-IF.

       FILL-DECOMPTE.
           MOVE   4 TO CAR-NUM.
           MOVE 119 TO COL-NUM.
           COMPUTE VH-00 = PERIODE
           PERFORM FILL-FORM.

       FILL-FIRME.

      *    PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  3 TO CAR-NUM.
           MOVE  6 TO LIN-NUM.
           MOVE 118 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

           MOVE  2 TO LIN-NUM.
           MOVE 80 TO COL-NUM.
           MOVE LNK-ANNEE TO ALPHA-TEXTE.

           PERFORM FILL-FORM.
           MOVE  4 TO LIN-NUM.
           MOVE  2 TO CAR-NUM.
           MOVE 111 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 114 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 117 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE 4 TO LIN-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE 8 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           MOVE 15 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 5 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-MAISON TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-RUE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 6 TO LIN-NUM.
           MOVE 15 TO COL-NUM.
           MOVE FR-PAYS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-CODE-POST TO VH-00.
           MOVE 4 TO CAR-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       DIS-HE-01.
           DISPLAY A-N LINE 4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 20 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           IF A-N = "N"
              DISPLAY LNK-TEXT LINE 6 POSITION 27 SIZE 34
           ELSE
              DISPLAY SPACES   LINE 6 POSITION 37 SIZE 1
              DISPLAY LNK-TEXT LINE 6 POSITION 38 SIZE 23.
       DIS-HE-STAT.
           DISPLAY STAT-NOM  LINE 9 POSITION 35.
       DIS-HE-05.
           MOVE COUT TO HE-Z4.
           DISPLAY HE-Z4  LINE 10 POSITION 29.
           DISPLAY COUT-NOM LINE 10 POSITION 35.
       DIS-HE-END.
           EXIT.

       TRANSMET.
           MOVE SAVE-CODE TO LNK-YN.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 512 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF COUNTER > 0 
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".

