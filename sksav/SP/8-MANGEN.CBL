      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-MANGEN EXPLOITATION CHANTIERS             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-MANGEN.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURNAL.FC".
           COPY "JOURS.FC".
           COPY "INTERCOM.FC".
           SELECT OPTIONAL T-DATA ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.


       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "JOURNAL.FDE".
           COPY "JOURS.FDE".

       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire JOURNAL COMPTABLE

       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-MOIS   PIC 99.
              03 INTER-POSTE  PIC 9(8).
              03 INTER-TOT    PIC 9.
              03 INTER-PERS   PIC 9(8).

           02 INTER-REC-DET.
              03 INTER-UNITE    PIC 9(6)V99.
              03 INTER-COMPTEUR PIC 999.
              03 INTER-VALEURS.
                 04 INTER-V    PIC S9(8)V99 OCCURS 8.
              03 INTER-VALS REDEFINES INTER-VALEURS.
                 04 INTER-BASE    PIC S9(8)V99.
                 04 INTER-NUIT    PIC S9(8)V99.
                 04 INTER-DIM     PIC S9(8)V99.
                 04 INTER-FERIE   PIC S9(8)V99.
                 04 INTER-HS      PIC S9(8)V99.
                 04 INTER-PRIME   PIC S9(8)V99.
                 04 INTER-PATRON  PIC S9(8)V99.
                 04 INTER-TOTAL   PIC S9(8)V99.

       FD  T-DATA
           LABEL RECORD STANDARD
           DATA RECORD IS T-RECORD.

       01  T-RECORD.
           02  T-FIRME           PIC 9999.
           02  T-FILLER          PIC X.
           02  T-ANNEE           PIC 9999.
           02  T-FILLER          PIC X.
           02  T-MOIS            PIC 99.
           02  T-FILLER          PIC X.
           02  T-POSTE           PIC 9(8).
           02  T-FILLER          PIC X.
           02  T-PERS            PIC 9(6).
           02  T-FILLER          PIC X.
           02  T-HEURES          PIC 999,99.
           02  T-FILLER          PIC X.
           02  T-COUT            PIC 999999,99.
           02  T-FILLER          PIC X.
           02  T-COUT-MOY        PIC 99999,99.
           02  T-FILLER          PIC X.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 11.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "MESSAGE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "POCL.REC".
           COPY "PARMOD.REC".

       01  NOT-OPEN        PIC 9 VALUE 0.
       01  MOIS-NUM        PIC 99 VALUE 0.    

       01  JOURNAL-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-JO".
           02 FIRME-JOURNAL      PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 ANNEE-JOURNAL      PIC 999.

       01  INTER-NAME.
           02 FILLER             PIC XXXX VALUE "IPMG".
           02 FIRME-INTER        PIC 9999.
       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       01  CUMULS.
           03 CUM-UNITE   PIC 9(6)V99.
           03 CUM-VALEURS.
              04 CUM-V    PIC S9(8)V99 OCCURS 8.

           COPY "V-VAR.CPY".
           COPY "V-VH00.CPY".
        
       01  COMPTEUR              PIC 99 COMP-1.
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  POSTE-DEB             PIC 9(8) VALUE 0.
       01  POSTE-FIN             PIC 9(8) VALUE 99999999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURNAL JOURS INTER.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-MANGEN.
       
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           MOVE LNK-SUFFIX TO ANNEE-JOURNAL ANNEE-JOURS.
           MOVE FR-KEY     TO FIRME-JOURNAL FIRME-INTER.
           OPEN INPUT JOURNAL.
           OPEN INPUT JOURS.
           DELETE FILE INTER.
           OPEN I-O INTER WITH LOCK.
           INITIALIZE LIN-NUM LIN-IDX.

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-MOIS TO SAVE-MOIS MOIS-DEBUT MOIS-FIN.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6 THRU 7
                      MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 11    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-9 
           WHEN 10 PERFORM AVANT-PATH
           WHEN 11 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-8
           WHEN  9 PERFORM APRES-9
           WHEN 11 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT POSTE-DEB
           LINE 12 POSITION 25 SIZE 8
           TAB UPDATE NO BEEP CURSOR  1 
           ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-7.
           ACCEPT POSTE-FIN
           LINE 13 POSITION 25 SIZE 8
           TAB UPDATE NO BEEP CURSOR  1 
           ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-8.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-9.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 22 POSITION 40 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-6.
           MOVE POSTE-DEB TO PC-NUMBER.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 5 CALL "1-POCL" USING LINK-V
                  IF LNK-VAL > 0
                     MOVE LNK-VAL TO PC-NUMBER POSTE-DEB
                  END-IF
                  CANCEL "1-POCL"
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
           WHEN  2 THRU 3 
                  IF EXC-KEY = 2
                     MOVE "A" TO A-N LNK-A-N
                  ELSE
                     MOVE "N" TO A-N LNK-A-N
                  END-IF
                  CALL "2-POCL" USING LINK-V PC-RECORD
                  IF PC-NUMBER > 0
                     MOVE PC-NUMBER TO POSTE-DEB
                  END-IF
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-POSTE
                   MOVE PC-NUMBER TO POSTE-DEB
           END-EVALUATE.
           PERFORM DIS-HE-06.

       APRES-7.
           IF POSTE-DEB > POSTE-FIN
              MOVE POSTE-DEB TO POSTE-FIN
           END-IF.
           MOVE POSTE-FIN TO PC-NUMBER.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 5 CALL "1-POCL" USING LINK-V
                  IF LNK-VAL > 0
                     MOVE LNK-VAL TO PC-NUMBER POSTE-FIN
                  END-IF
                  CANCEL "1-POCL"
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
           WHEN  2 THRU 3 CALL "2-POCL" USING LINK-V PC-RECORD
                   IF PC-NUMBER > 0
                      MOVE PC-NUMBER TO POSTE-FIN
                   END-IF
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-POSTE
                   MOVE PC-NUMBER TO POSTE-FIN
           END-EVALUATE.
           PERFORM DIS-HE-07.

       APRES-8.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-08.

       APRES-9.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-09.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.
           PERFORM END-PROGRAM.

           
       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM DIS-HE-01.
           PERFORM CAR-RECENTE.
           IF STATUT > 0 AND NOT = CAR-STATUT  
              GO READ-PERSON-2
           END-IF.
           IF COUT > 0 AND NOT = CAR-COUT
              GO READ-PERSON-2
           END-IF.
           PERFORM READ-HEURES.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.
           PERFORM FULL-PROCESS.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       READ-HEURES.
           INITIALIZE JRS-RECORD PC-RECORD.
           MOVE FR-KEY TO JRS-FIRME-2.
           MOVE REG-PERSON TO JRS-PERSON-2.
           MOVE MOIS-DEBUT TO JRS-MOIS-2.
           START JOURS KEY >= JRS-KEY-2 INVALID CONTINUE
           NOT INVALID PERFORM READ-JOURS THRU READ-JRS-END.

       READ-JOURS.
           READ JOURS NEXT AT END
               GO READ-JRS-END
           END-READ.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR JRS-MOIS   > MOIS-FIN
              GO READ-JRS-END
           END-IF.
           IF JRS-POSTE = 0
           OR JRS-COMPLEMENT > 0
           OR JRS-OCCUPATION > 0
              GO READ-JOURS.

           IF JRS-POSTE < POSTE-DEB
           OR JRS-POSTE > POSTE-FIN
              GO READ-JOURS.

           MOVE JRS-POSTE TO PC-NUMBER.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY.
           IF PC-PRODUCTIF = "N"
              GO READ-JOURS.
           PERFORM WRITE-INTER.
           GO READ-JOURS.
       READ-JRS-END.
           EXIT.

       WRITE-INTER.
           INITIALIZE INTER-RECORD CUMULS.
           MOVE JRS-POSTE  TO INTER-POSTE.
           MOVE JRS-MOIS   TO INTER-MOIS.
           MOVE REG-PERSON TO INTER-PERS.
           READ INTER INVALID CONTINUE.
           ADD JRS-HRS(32) TO INTER-UNITE.
           PERFORM CUMUL-JOURNAL.
           WRITE INTER-RECORD INVALID REWRITE INTER-RECORD END-WRITE.

       CUMUL-JOURNAL.
           INITIALIZE JRL-RECORD.
           MOVE REG-PERSON TO JRL-PERS.
           MOVE JRS-MOIS   TO JRL-MOIS.
           START JOURNAL KEY > JRL-KEY INVALID CONTINUE
              NOT INVALID
              PERFORM LECTURE-JOURNAL THRU LECTURE-JOURNAL-END.

       LECTURE-JOURNAL.
           READ JOURNAL NEXT AT END GO LECTURE-JOURNAL-END.
           IF REG-PERSON NOT = JRL-PERS
           OR JRL-MOIS   NOT = JRS-MOIS
              GO LECTURE-JOURNAL-END.
           IF JRL-POSTE NOT = JRS-POSTE
           OR JRL-EXTENSION NOT = JRS-EXTENSION
              GO LECTURE-JOURNAL.
           MOVE 1 TO IDX-2.
           MOVE 100 TO IDX-1.
           PERFORM PROC.
           PERFORM PROC VARYING IDX-1 FROM 91 BY 1 UNTIL IDX-1 > 95.
           MOVE 2 TO IDX-2.
           PERFORM PROC VARYING IDX-1 FROM 61 BY 1 UNTIL IDX-1 > 70.
           MOVE 3 TO IDX-2.
           MOVE 80 TO IDX-1.
           PERFORM PROC.
           MOVE 4 TO IDX-2.
           PERFORM PROC VARYING IDX-1 FROM 71 BY 1 UNTIL IDX-1 > 79.
           MOVE 5 TO IDX-2.
           PERFORM PROC VARYING IDX-1 FROM 96 BY 1 UNTIL IDX-1 > 99.
           MOVE 6 TO IDX-2.
           PERFORM PROC VARYING IDX-1 FROM 51 BY 1 UNTIL IDX-1 > 60.
           PERFORM PROC VARYING IDX-1 FROM 81 BY 1 UNTIL IDX-1 > 83.
           MOVE 7 TO IDX-2.
           PERFORM PROC VARYING IDX-1 FROM 161 BY 1 UNTIL IDX-1 > 170.
           GO LECTURE-JOURNAL.
       LECTURE-JOURNAL-END.
           EXIT.

       PROC.
           ADD JRL-V(IDX-1) TO INTER-V(IDX-2) INTER-TOTAL 
                               CUM-V(IDX-2) CUM-V(8).
       TOT.
           ADD CUM-V(IDX-1) TO INTER-V(IDX-1).

       FULL-PROCESS.
           INITIALIZE INTER-RECORD PC-RECORD LIN-NUM MOIS-NUM.
           START INTER KEY > INTER-KEY INVALID CONTINUE
            NOT INVALID
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END GO READ-INTER-END.
           IF NOT-OPEN = 0
              OPEN OUTPUT T-DATA
              MOVE 1 TO NOT-OPEN
           END-IF.
           INITIALIZE T-RECORD.

           MOVE FR-KEY      TO T-FIRME.
           MOVE LNK-ANNEE   TO T-ANNEE.
           MOVE INTER-MOIS  TO T-MOIS.
           MOVE INTER-POSTE TO T-POSTE.
           MOVE INTER-PERS  TO T-PERS.
           MOVE INTER-UNITE TO T-HEURES.
           MOVE INTER-TOTAL TO T-COUT.
           COMPUTE SH-00 = INTER-TOTAL / INTER-UNITE + ,005.
           MOVE SH-00 TO T-COUT-MOY.
           WRITE T-RECORD.
           GO READ-INTER.
       READ-INTER-END.
           EXIT.

       NEXT-POSTE.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" SAVE-KEY.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 27 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 6 POSITION 47 SIZE 33.
           IF A-N = "N"
              DISPLAY SPACES LINE 6 POSITION 35 SIZE 10.
       DIS-HE-STAT.
           DISPLAY STAT-NOM  LINE 9 POSITION 35.
       DIS-HE-05.
           MOVE COUT-NUMBER TO HE-Z4.
           DISPLAY HE-Z4  LINE 10 POSITION 29.
           DISPLAY COUT-NOM LINE 10 POSITION 35.
       DIS-HE-06.
           MOVE POSTE-DEB TO HE-Z8.
           DISPLAY HE-Z8 LINE 12 POSITION 25.
           IF POSTE-DEB NOT = 0
              MOVE POSTE-DEB TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
           ELSE
              INITIALIZE PC-RECORD
           END-IF.
           DISPLAY PC-NOM  LINE 12 POSITION 35 SIZE 45.
       DIS-HE-07.
           MOVE POSTE-FIN TO HE-Z8.
           DISPLAY HE-Z8 LINE 13 POSITION 25.
           IF POSTE-FIN NOT = 0
              MOVE POSTE-FIN TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
           ELSE
              INITIALIZE PC-RECORD
           END-IF.
           DISPLAY PC-NOM  LINE 13 POSITION 35 SIZE 45.

       DIS-HE-08.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 19 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-09.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1317 TO LNK-VAL.
           MOVE "MANG" TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE 13 TO LIN-IDX.
           MOVE 0 TO COMPTEUR.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE JOURNAL.
           MOVE SAVE-MOIS TO LNK-MOIS.
           CLOSE INTER.
           DELETE FILE INTER.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".




