      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-MISFAC FACTURE INTERIMAIRES PAR MISSIONS  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-MISFAC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FACDINT.FC".
           COPY "INTERCOM.FC".
           
       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FACDINT.FDE".

       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire FACTURE COMPTABLE

       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-CLIENT  PIC 9(8).
              03 INTER-REF     PIC X(20).
              03 INTER-PERSON  PIC 9(8).
              03 INTER-MISSION PIC X(8).
              03 INTER-MISS REDEFINES INTER-MISSION PIC 9(8).
              03 INTER-ANNEE   PIC 9999.
              03 INTER-MOIS    PIC 99.
              03 INTER-CODE    PIC 9(4).

           02 INTER-REC-DET.
              03 INTER-VALEURS.
                 04 INTER-DONNEE     PIC S9(8)V9(5) OCCURS 6.
              03 INTER-VAL-DET REDEFINES INTER-VALEURS.
                 04 INTER-DONNEE-1   PIC S9(8)V9(5).
                 04 INTER-DONNEE-2   PIC S9(8)V9(5).
                 04 INTER-POURCENT   PIC S9(8)V9(5).
                 04 INTER-UNITE      PIC S9(8)V9(5).
                 04 INTER-UNITAIRE   PIC S9(8)V9(5).
                 04 INTER-TOTAL      PIC S9(8)V9(5).
              03 INTER-TEXTE         PIC X(30).

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "MISSION.REC".
           COPY "JOURS.REC".
           COPY "CODPAIE.REC".
           COPY "CODPAR.REC".

       01  SAL-ACT               PIC 9(6)V99.
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02  SAL-ACT-A         PIC 9(6).
           02  SAL-ACT-B         PIC 99.
       01  FACTEUR               PIC 9(3)V9(6).

       01  FID-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FD".
           02 FIRME-FID          PIC 9999 VALUE 0.

       01  INTER-NAME.
           02 FILLER             PIC XXXX VALUE "IFID".
           02 FIRME-INTER        PIC 9999.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "CCOL.REC".
       01  POSTES.
           02 POSTE-DEB          PIC 9(8).
           02 POSTE-FIN          PIC 9(8).

       PROCEDURE DIVISION USING LINK-V 
                                REG-RECORD 
                                CCOL-RECORD
                                POSTES.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FID
               INTER.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-4-MISFAC.

           IF FIRME-FID = 0
              MOVE FR-KEY TO FIRME-FID FIRME-INTER
              OPEN I-O FID
              OPEN I-O INTER WITH LOCK
       
           CALL "6-GCP" USING LINK-V CP-RECORD.
           INITIALIZE INTER-RECORD.


           INITIALIZE JRS-RECORD MISS-RECORD.
           MOVE LNK-MOIS TO JRS-MOIS.
           PERFORM NEXT-JOUR THRU NEXT-JOUR-END.
           EXIT PROGRAM.

       NEXT-JOUR.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD NX-KEY.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
           OR JRS-OCCUPATION NOT = 0
           OR JRS-POSTE > POSTE-FIN
              GO NEXT-JOUR-END
           END-IF.
           IF JRS-POSTE = 0
           OR JRS-POSTE < POSTE-DEB
              GO NEXT-JOUR
           END-IF.
           IF JRS-COMPLEMENT = 0
              IF MISS-CLIENT > 0
                 PERFORM AJUSTE
              END-IF
              PERFORM READ-MISS
              IF MISS-CLIENT = 0
                 GO NEXT-JOUR
              END-IF
           ELSE
              IF MISS-CLIENT > 0
                 PERFORM MAKE-COMPL
              END-IF
           END-IF.
           GO NEXT-JOUR.
       NEXT-JOUR-END.
           IF MISS-CLIENT > 0
              PERFORM AJUSTE
           END-IF.

       READ-MISS.
           INITIALIZE INTER-RECORD.
           MOVE JRS-PERSON TO MISS-PERSON.
           MOVE JRS-POSTE  TO MISS-CLIENT.
           MOVE 1 TO IDX-1.
           STRING JRS-EXTENSION DELIMITED BY SIZE INTO MISS-CONTRAT
           POINTER IDX-1 ON OVERFLOW CONTINUE END-STRING.
           CALL "6-MISS" USING LINK-V REG-RECORD MISS-RECORD FAKE-KEY.
           IF  MISS-FACTURE-IDX > 0
           AND MISS-FACTURE-R NOT = SPACES
               COMPUTE MISS-FACTURE =
               MISS-FACTURE-IDX * MOIS-IDX(LNK-MOIS) + ,005
           END-IF.
           IF MISS-HOR-MEN = 1
           AND MISS-FACTURE > 200
              PERFORM FACTEUR
              COMPUTE MISS-FACTURE = MISS-FACTURE / FACTEUR.

           IF MISS-CLIENT > 0
              PERFORM SAL-BASE.

       SAL-BASE.
           MOVE 2 TO INTER-CODE.
           MOVE JRS-HRS(32)  TO INTER-UNITE.
           MOVE MISS-FACTURE TO INTER-UNITAIRE. 
           PERFORM WRITE-CSP.
           PERFORM PRIME THRU PRIME-END VARYING IDX FROM 1 BY 1 UNTIL 
                                                IDX > 20.

       MAKE-COMPL.
           MOVE CCOL-TAUX(JRS-COMPLEMENT) TO INTER-POURCENT.
           IF MISS-TAUX(JRS-COMPLEMENT) > 0
              MOVE MISS-TAUX(JRS-COMPLEMENT) TO INTER-POURCENT.
           MOVE JRS-HRS(32) TO INTER-UNITE.
           IF INTER-POURCENT > 0
              MOVE MISS-FACTURE TO INTER-DONNEE-2
           ELSE
              MOVE MISS-FACTURE TO INTER-UNITAIRE. 
           COMPUTE IDX-1 = 60 + JRS-COMPLEMENT.
           MOVE COD-PAR(IDX-1, 1) TO INTER-CODE.
           PERFORM WRITE-CSP.

       WRITE-CSP.
           IF INTER-UNITAIRE = 0
              COMPUTE SAL-ACT = INTER-DONNEE-2 * INTER-POURCENT / 100 
              + ,005
              MOVE SAL-ACT TO INTER-UNITAIRE 
           END-IF.
           IF INTER-TOTAL = 0
              COMPUTE SAL-ACT = INTER-UNITE * INTER-UNITAIRE + ,005
              MOVE SAL-ACT TO INTER-TOTAL
           END-IF.
           MOVE JRS-PERSON     TO INTER-PERSON.
           MOVE JRS-EXTENSION  TO INTER-MISSION.
           MOVE LNK-ANNEE      TO INTER-ANNEE.
           MOVE LNK-MOIS       TO INTER-MOIS.
           MOVE MISS-REFERENCE TO INTER-REF.
           MOVE MISS-CLIENT    TO INTER-CLIENT.
           WRITE INTER-RECORD INVALID REWRITE INTER-RECORD.
           INITIALIZE INTER-RECORD.

       PRIME.
           IF MISS-HOR-JOUR(IDX)   = "M" 
           OR MISS-HOR-JOUR(IDX)   = "G" 
           OR MISS-CODE-PRIME(IDX) = 0
              GO PRIME-END.
           IF MISS-HOR-JOUR(IDX)   = "T" 
              PERFORM PRECARITE
              GO PRIME-END.
           IF  MISS-FACT-INDEX(IDX) > 0
           AND MISS-FACT-R(IDX) NOT = SPACES
               COMPUTE MISS-FACT-PRIME(IDX) =
               MISS-FACT-INDEX(IDX) * MOIS-IDX(LNK-MOIS) + ,005
           END-IF.
           IF MISS-FACT-PRIME(IDX) = 0
           AND MISS-PRIME-PC(IDX) = 0
              GO PRIME-END.
           COMPUTE SAL-ACT = 
                   MISS-PRIME-100(IDX) * MOIS-IDX(LNK-MOIS) + ,0001.
           IF MISS-INDEX-P(IDX) NOT = "N"
              MOVE SAL-ACT TO MISS-FACT-PRIME(IDX).
           IF MISS-FACT-PRIME(IDX) = 0
              MOVE MISS-FACTURE TO MISS-FACT-PRIME(IDX).
           MOVE MISS-PRIME-PC(IDX) TO INTER-POURCENT.
           IF INTER-POURCENT > 0
              MOVE MISS-FACT-PRIME(IDX) TO INTER-DONNEE-2
           ELSE
              MOVE MISS-FACT-PRIME(IDX) TO INTER-UNITAIRE.
           MOVE JRS-HRS(32) TO INTER-UNITE.
           IF MISS-HOR-JOUR(IDX) = "J" 
              MOVE JRS-JOURS  TO INTER-UNITE.
           IF MISS-HOR-JOUR(IDX) = "C" 
              MOVE MOIS-JRS(LNK-MOIS) TO INTER-UNITE
              IF  MISS-FIN-A = LNK-ANNEE
              AND MISS-FIN-M = LNK-MOIS
                  MOVE MISS-FIN-J TO INTER-UNITE
              END-IF
              IF  MISS-DEBUT-A = LNK-ANNEE
              AND MISS-DEBUT-M = LNK-MOIS
                  SUBTRACT MISS-DEBUT-J FROM INTER-UNITE
                  ADD 1 TO INTER-UNITE
              END-IF
           END-IF.
           IF MISS-HOR-JOUR(IDX) = "F" 
              MOVE INTER-UNITAIRE TO INTER-TOTAL
              MOVE 0 TO INTER-UNITE INTER-UNITAIRE.
           MOVE MISS-CODE-PRIME(IDX) TO INTER-CODE.
           MOVE MISS-TEXTE(IDX) TO INTER-TEXTE.
           PERFORM WRITE-CSP.
       PRIME-END.
           EXIT.
           
       AJUSTE.
           INITIALIZE FID-RECORD INTER-RECORD.
           MOVE MISS-CLIENT    TO FID-CLIENT.
           MOVE REG-PERSON     TO FID-PERSON.
           MOVE MISS-CONTRAT   TO FID-MISSION.
           MOVE LNK-ANNEE      TO FID-ANNEE.
           MOVE LNK-MOIS       TO FID-MOIS.
           START FID KEY >= FID-KEY-2 INVALID CONTINUE
              NOT INVALID
           PERFORM READ-FID THRU READ-FID-END.
           INITIALIZE MISS-RECORD.

       READ-FID.
           READ FID NEXT NO LOCK AT END 
              GO READ-FID-END.
           IF MISS-CLIENT    NOT = FID-CLIENT
           OR REG-PERSON     NOT = FID-PERSON
           OR MISS-CONTRAT   NOT = FID-MISSION
           OR LNK-ANNEE      NOT = FID-ANNEE
           OR LNK-MOIS       NOT = FID-MOIS
              GO READ-FID-END.
           MOVE MISS-CLIENT    TO INTER-CLIENT.
           MOVE REG-PERSON     TO INTER-PERSON.
           MOVE MISS-CONTRAT   TO INTER-MISS.
           MOVE MISS-REFERENCE TO INTER-REF.
           MOVE LNK-ANNEE      TO INTER-ANNEE.
           MOVE LNK-MOIS       TO INTER-MOIS.
           MOVE FID-CODE       TO INTER-CODE.
           READ INTER INVALID GO READ-FID.
           MOVE INTER-UNITE TO SH-00.
           MOVE FID-UNITE TO SAL-ACT.
           COMPUTE SH-00 = SH-00 - SAL-ACT.
           IF SH-00 <= 0
              DELETE INTER INVALID CONTINUE
           ELSE
              MOVE SH-00 TO INTER-UNITE
              COMPUTE SAL-ACT = INTER-UNITE * INTER-UNITAIRE + ,005
              MOVE SAL-ACT TO INTER-TOTAL
              REWRITE INTER-RECORD INVALID CONTINUE END-REWRITE
           END-IF.
           GO READ-FID.
       READ-FID-END.
           EXIT.

       FACTEUR.
           MOVE 0 TO FACTEUR.
           PERFORM FACT VARYING IDX FROM 1 BY 1 UNTIL IDX > 
           MOIS-JRS(LNK-MOIS).
           COMPUTE FACTEUR = FACTEUR / 40 * MISS-HRS-BASE.
           
       FACT.
           IF SEM-IDX(LNK-MOIS, IDX) < 6
              ADD 8 TO FACTEUR.

       PRECARITE.
           INITIALIZE CSP-RECORD INTER-RECORD.
           MOVE JRS-PERSON  TO INTER-PERSON.
           MOVE JRS-POSTE   TO CSP-POSTE.
           MOVE MISS-CLIENT TO INTER-CLIENT.
           MOVE MISS-REFERENCE TO INTER-REF.
           MOVE JRS-EXTENSION TO CSP-EXTENSION INTER-MISSION.
           MOVE MISS-CODE-PRIME(IDX) TO CSP-CODE INTER-CODE.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           MOVE MISS-FACT-PRIME(IDX) TO INTER-POURCENT.
           ADD 100 TO INTER-POURCENT.
           MOVE CSP-TOTAL TO INTER-DONNEE-2.
           COMPUTE SAL-ACT = INTER-DONNEE-2 * INTER-POURCENT / 100 
              + ,005.
           MOVE SAL-ACT TO INTER-TOTAL.
           MOVE LNK-ANNEE      TO INTER-ANNEE.
           MOVE LNK-MOIS       TO INTER-MOIS.
           WRITE INTER-RECORD INVALID REWRITE INTER-RECORD.
           INITIALIZE INTER-RECORD.
           
           COPY "XACTION.CPY".
              