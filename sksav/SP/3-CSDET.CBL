      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 3-CSDET IMPRESSION LISTE CODES SALAIRES DETAIL �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-CSDET.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.
       FILE SECTION.

       FD  TF-TRANS
           RECORD VARYING DEPENDING ON IDX-4
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 120 DEPENDING IDX-4.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  TYPE-MESS             PIC XXXX VALUE "LEX ".
       01  JOB-STANDARD          PIC X(10) VALUE "160       ".
       01  NOT-OPEN              PIC 9 VALUE 0.

       01  HE-INPUT-PARAMETER.
      *    LINE COL SIZE HE-SCREEN    LLCCSZECRA
           02 IP-101 PIC 9(16)  VALUE 0505253017253004.
           02 IP-102 PIC 9(16)  VALUE 0605253018253004.
           02 IP-103 PIC 9(16)  VALUE 0805250096283001.
           02 IP-DEC PIC 9(16)  VALUE 2305250099250000.

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
            03 I-PP OCCURS 4.
               04 IP-LINE  PIC 99.
               04 IP-COL   PIC 99.
               04 IP-SIZE  PIC 99.
               04 IP-ECRAN PIC 9999.
               04 IP-ACC   PIC 99.
               04 IP-TXT   PIC 99.
               04 IP-LEN   PIC 99.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "CSDET.REC".
           COPY "CSDEF.REC".
           COPY "CODTXT.REC".
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".
           COPY "V-VAR.CPY".
           COPY "LIVREX.REC".
           COPY "PARMOD.REC".

       01  DSP-IDX               PIC ZZ.
       01  STRUCTURE-YN          PIC X.
       01  CHOIX                 PIC 99 VALUE 0.
       01  ASCII-FILE            PIC 9 VALUE 0.
       01  TXT-RECORD.
           02 TXT-NUMBER         PIC Z(6).
           02 TXT-NUMBER-T REDEFINES TXT-NUMBER PIC X(6).
           02 TXT-FILLER         PIC X VALUE ";".
           02 TXT-NOM            PIC X(30).
           02 TXT-FILLER         PIC X VALUE ";".
           02 TXT-ANNEE          PIC Z(4).
           02 TXT-ANNEE-T REDEFINES TXT-ANNEE PIC X(4).
           02 TXT-FILLER         PIC X VALUE ";".
           02 TXT-MOIS           PIC Z(5).
           02 TXT-MOIS-T REDEFINES TXT-MOIS PIC X(5).
           02 TXT-FILLER         PIC X VALUE ";".
           02 TXT-D OCCURS 6.
              03 TXT-DESC        PIC X(10).
              03 TXT-FILL        PIC X.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(160) OCCURS 45.
       01  CS-PLAGE.
           02 CS-DF              PIC 9(4) OCCURS 2.
       01  CS-IDX                PIC 9.
           
           COPY "V-VH00.CPY".

       01   ECR-DISPLAY.
            02 HE-Z2 PIC Z(2).
            02 HE-Z4 PIC Z(4).
            02 HE-Z6 PIC Z(6).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TF-TRANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-CSDET.

           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           CALL "0-TODAY" USING TODAY.
           MOVE    1 TO CS-DF(1).
           MOVE 9999 TO CS-DF(2).

           MOVE 0 TO CAR-NUM DEC-NUM LIN-NUM LIN-IDX.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions

           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0027000000 TO EXC-KFR(6).
           MOVE 0052535400 TO EXC-KFR(11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1  THRU 3
                   MOVE 0029230000 TO EXC-KFR(1)
                   MOVE 0000000065 TO EXC-KFR(13)
                   MOVE 6600000000 TO EXC-KFR(14) 
           WHEN 4  MOVE 0000000025 TO EXC-KFR(1)
                   MOVE 1700000078 TO EXC-KFR(2).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 MOVE INDICE-ZONE TO CS-IDX
                   PERFORM AVANT-CS
           WHEN  2 MOVE INDICE-ZONE TO CS-IDX
                   PERFORM AVANT-CS
           WHEN  3 PERFORM AVANT-SORT
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 MOVE INDICE-ZONE TO CS-IDX
                   PERFORM APRES-CS
           WHEN  2 MOVE INDICE-ZONE TO CS-IDX
                   PERFORM APRES-CS
           WHEN  3 PERFORM APRES-SORT
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
           END-EVALUATE.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.


      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-CS.
           ACCEPT CS-DF(CS-IDX) LINE IP-LINE(INDICE-ZONE) 
                            POSITION IP-ACC(INDICE-ZONE) 
                            SIZE IP-LEN(INDICE-ZONE) 
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.
           IF CS-DF(1) > CS-DF(2) 
              MOVE CS-DF(1) TO CS-DF(2) 
           END-IF.
           IF CS-DF(1) < 1 
              MOVE 1 TO CS-DF(1)
           END-IF.

       AVANT-SORT.
           ACCEPT CHOIX LINE IP-LINE(INDICE-ZONE) 
                            POSITION IP-ACC(INDICE-ZONE) 
                            SIZE IP-LEN(INDICE-ZONE) 
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.


       AVANT-PATH.
           MOVE "AA" TO LNK-AREA.
           MOVE 32 TO LNK-NUM.
           MOVE "L" TO LNK-LOW.
           PERFORM DISPLAY-MESSAGE.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 24 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       NEXT-CSDEF.
           CALL "6-CSDEF" USING LINK-V CD-RECORD EXC-KEY.
       READ-CSDEF.
           CALL "6-CSDEF" USING LINK-V CD-RECORD NUL-KEY.
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

--     APRES-CS.
           MOVE 0 TO INPUT-ERROR.
           MOVE CS-DF(CS-IDX) TO CD-NUMBER.
           EVALUATE EXC-KEY
           WHEN 2 CALL "2-CSDEF" USING LINK-V CD-RECORD
                  MOVE CD-NUMBER TO CS-DF(CS-IDX)
                  PERFORM AFFICHAGE-ECRAN 
                  MOVE CS-IDX TO IDX
                  PERFORM AFFICHAGE-DETAIL 
                  MOVE IDX TO CS-IDX
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR
                  CANCEL "2-CSDEF" 
           WHEN 3 CALL "2-CSD" USING LINK-V CTX-RECORD
                  IF CTX-NUMBER > 0
                     MOVE CTX-NUMBER TO CD-NUMBER
                     CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY
                  END-IF
                  MOVE CD-NUMBER TO CS-DF(CS-IDX)
                  PERFORM AFFICHAGE-ECRAN 
                  MOVE CS-IDX TO IDX
                  MOVE IDX TO CS-IDX
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR
                  CANCEL "2-CSD" 
           WHEN 13 IF CS-IDX = 1
                      PERFORM READ-CSDEF
                   ELSE
                      CALL "6-CSDEF" USING LINK-V CD-RECORD SAVE-KEY
                   END-IF
           WHEN OTHER PERFORM NEXT-CSDEF
           END-EVALUATE.
           MOVE CD-NUMBER TO CS-DF(CS-IDX).
           PERFORM DIS-CS.

       APRES-SORT.
           MOVE CHOIX TO MS-NUMBER.
           MOVE TYPE-MESS TO LNK-AREA.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-MESS" USING LINK-V MS-RECORD
                    CANCEL "2-MESS"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN  65 THRU 66 PERFORM NEXT-MESSAGES
           END-EVALUATE.
           MOVE MS-NUMBER TO CHOIX.
           PERFORM DIS-SORT.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-CODSAL
                    PERFORM READ-CODSAL THRU READ-CODSAL-END
           WHEN 10 MOVE 1 TO ASCII-FILE
                   PERFORM AVANT-PATH
                   PERFORM START-CODSAL
                   PERFORM READ-CODSAL THRU READ-CODSAL-END
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       START-CODSAL.
           INITIALIZE CD-RECORD.

       READ-CODSAL.
           CALL "6-CSDEF" USING LINK-V CD-RECORD NX-KEY.
           IF ASCII-FILE > 0
           AND CD-NUMBER > CS-DF(2)
              GO READ-CODSAL-END
           END-IF.
           IF CD-NUMBER = 0
           OR CD-NUMBER > CS-DF(2)
              IF LIN-NUM > 2
              OR COUNTER > 0
                 PERFORM TRANSMET
                 GO READ-CODSAL-END
              END-IF
           END-IF.
           IF CD-NUMBER < CS-DF(1)
              MOVE CS-DF(1) TO CD-NUMBER
           END-IF.
           INITIALIZE CS-RECORD.
           CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
           MOVE CD-NUMBER TO CTX-NUMBER.
           CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           IF ASCII-FILE > 0
              PERFORM WRITE-TEXT
           GO READ-CODSAL.

           IF LIN-NUM > 44
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              INITIALIZE FORMULAIRE
              PERFORM PAGE-DATE
           END-IF.
           IF CHOIX = 0
              PERFORM FILL-TEXTE
           ELSE
              PERFORM ANALYSE.
           GO READ-CODSAL.
       READ-CODSAL-END.
           PERFORM END-PROGRAM.

       DIS-HE-01.
           PERFORM DIS-CS VARYING CS-IDX FROM 1 BY 1 UNTIL CS-IDX > 2.
       DIS-HE-03.
           PERFORM DIS-SORT.
       DIS-HE-END.
           EXIT.

       DIS-CS.
           MOVE CS-DF(CS-IDX) TO CTX-NUMBER HE-Z4.
           CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           DISPLAY HE-Z4 LINE IP-LINE(CS-IDX) POSITION IP-ACC(CS-IDX).
           DISPLAY CTX-NOM LINE IP-LINE(CS-IDX) POSITION 
           IP-TXT(CS-IDX) SIZE 45.

       DIS-SORT.
           MOVE CHOIX TO MS-NUMBER HE-Z4.
           MOVE TYPE-MESS TO LNK-AREA.
           COMPUTE COL-IDX = 
           IP-COL(INDICE-ZONE) - 5 + IP-SIZE(INDICE-ZONE).
           DISPLAY HE-Z4 LINE IP-LINE(INDICE-ZONE) POSITION COL-IDX.
           CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY.
           DISPLAY MS-DESCRIPTION LINE IP-LINE(INDICE-ZONE) 
                              POSITION IP-TXT(INDICE-ZONE).

       FILL-TEXTE.
           ADD 1 TO LIN-NUM.
           MOVE 4 TO CAR-NUM.
           MOVE CD-NUMBER TO VH-00.
           MOVE 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE CTX-NOM TO ALPHA-TEXTE.
           MOVE 6 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 4 TO CAR-NUM.
           MOVE CS-ANNEE TO VH-00.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 2 TO CAR-NUM.
           MOVE CS-MOIS TO VH-00.
           MOVE 36 TO COL-NUM.
           PERFORM FILL-FORM.
           IF CHOIX = 0
              PERFORM DESC.


       DESC.
           MOVE CTX-DESCRIPTION( 1) TO ALPHA-TEXTE.
           MOVE 42 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE CTX-DESCRIPTION( 2) TO ALPHA-TEXTE.
           MOVE 53 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE 3 TO CAR-NUM.
           MOVE CS-VAL-PREDEFINIE(3) TO VH-00.
           MOVE 64 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE CTX-DESCRIPTION( 4) TO ALPHA-TEXTE.
           MOVE 69 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE CTX-DESCRIPTION(5) TO ALPHA-TEXTE.
           MOVE 81 TO COL-NUM.
           PERFORM FILL-FORM.
           IF DECISION NOT = 0
               MOVE  4 TO CAR-NUM
               MOVE 87 TO COL-NUM
               MOVE CS-LIVRE(6, 1) TO VH-00
               PERFORM FILL-FORM
           END-IF.

       PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  2 TO CAR-NUM LIN-NUM.
           MOVE 73 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 67 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 70 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 73 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.
           MOVE MENU-DESCRIPTION TO ALPHA-TEXTE.
           MOVE 5 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.

       AFFICHAGE-ECRAN.
           MOVE 32 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           MOVE "EE" TO LNK-AREA.
           PERFORM LIN VARYING IDX FROM 1 BY 1 UNTIL IDX > 4.

       LIN.
           MOVE IDX TO HE-Z2.
           IF IDX < 4
              DISPLAY HE-Z2 LINE IP-LINE(IDX) POSITION 1 LOW.
           MOVE IP-ECRAN(IDX) TO LNK-NUM.
           MOVE IP-LINE(IDX)  TO LNK-LINE.
           MOVE IP-COL(IDX)   TO LNK-COL.
           MOVE IP-SIZE(IDX)  TO LNK-SIZE.
           MOVE "L" TO LNK-LOW.
           CALL "0-DMESS" USING LINK-V.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L160O" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF LIN-NUM > 1
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "L160O" USING LINK-V FORMULAIRE.
           CANCEL "L160O".
           MOVE " " TO LNK-YN.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XACTION.CPY".

       ANALYSE.
           MOVE 0 TO NOT-FOUND IDX-4.
           PERFORM COLS VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 8.
       COLS.
           PERFORM FIELDS VARYING IDX-2 FROM 1 BY 1 UNTIL IDX-2 > 20.

       FIELDS.
           EVALUATE CHOIX 
           WHEN 1 IF CS-LIVRE(IDX-1, IDX-2) > 0 AND < 301 
                     PERFORM FILL-DETAIL
                  END-IF
           WHEN 2 IF CS-LIVRE(IDX-1, IDX-2) > 400 AND < 501 
                     PERFORM FILL-DETAIL
                  END-IF
           WHEN 3 IF CS-LIVRE(IDX-1, IDX-2) > 500 AND < 601 
                     PERFORM FILL-DETAIL
                  END-IF
           WHEN 4 IF CS-LIVRE(IDX-1, IDX-2) > 700 AND < 801 
                     PERFORM FILL-DETAIL
                  END-IF
           WHEN 5 IF CS-LIVRE(IDX-1, IDX-2) > 800 AND < 900 
                     PERFORM FILL-DETAIL
                  END-IF
           WHEN 6 IF CS-LIVRE(IDX-1, IDX-2) > 1000 AND < 1201 
                     PERFORM FILL-DETAIL
                  END-IF
           END-EVALUATE.

       FILL-DETAIL. 
           IF NOT-FOUND = 0
              PERFORM FILL-TEXTE
              MOVE 1 TO NOT-FOUND.
           ADD 1 TO IDX-4.
           MOVE CS-LIVRE(IDX-1, IDX-2) TO LEX-NUMBER VH-00.
           CALL "6-LEX" USING LINK-V LEX-RECORD EXC-KEY.
           MOVE 4 TO CAR-NUM.
           IF CHOIX = 2
              COMPUTE COL-NUM = 25 + IDX-4 * 14
              MOVE 2 TO CAR-NUM
           ELSE
              COMPUTE COL-NUM = 10 + IDX-4 * 30.
           PERFORM FILL-FORM.
           MOVE LEX-DESCRIPTION TO ALPHA-TEXTE.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

       WRITE-TEXT.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-TRANS
              MOVE 1 TO NOT-OPEN 
              MOVE "NUMERO" TO TXT-NUMBER-T
              MOVE "ANNEE " TO TXT-ANNEE-T
              MOVE "MOIS  " TO TXT-MOIS-T
              MOVE "NOM   " TO TXT-NOM
              MOVE "TEXT  " TO TXT-DESC(1) TXT-DESC(2) TXT-DESC(3)
                               TXT-DESC(4) TXT-DESC(5) TXT-DESC(6) 
              MOVE ";" TO TXT-FILL(1) TXT-FILL(2) TXT-FILL(3)
                          TXT-FILL(4) TXT-FILL(5) TXT-FILL(6) 
              MOVE 113 TO IDX-4 
              WRITE TF-RECORD FROM TXT-RECORD
           END-IF.
           MOVE CD-NUMBER TO TXT-NUMBER.
           MOVE CTX-NOM   TO TXT-NOM.
           MOVE CS-ANNEE  TO TXT-ANNEE.
           MOVE CS-MOIS   TO TXT-MOIS.
           PERFORM DESCR VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.
           WRITE TF-RECORD FROM TXT-RECORD.

       DESCR.
           MOVE CTX-DESCRIPTION(IDX) TO TXT-DESC(IDX).
           MOVE ";" TO TXT-FILL(IDX).
