      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-SAP TRANSFERT COMPTABILITE SAP            �
      *  � I.E.E.                                                �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-SAP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "INTERCOM.FC".
           SELECT OPTIONAL TF-COMPTA ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.


       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire JOURNAL COMPTABLE

       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-MOIS   PIC 99.
              03 INTER-SUITE  PIC 99.
              03 INTER-STAT   PIC 99.
              03 INTER-COUT   PIC 9(8).
              03 INTER-PERS   PIC 9(8).
              03 INTER-DC     PIC 9.
              03 INTER-COMPTE PIC X(30).
              03 INTER-POSTE  PIC 9(10).
              03 INTER-EXT    PIC X(50).
              03 INTER-LIB    PIC 9999.
              03 INTER-PERS-2 PIC 9(8).

           02 INTER-REC-DET.
              03 INTER-VALUE PIC S9(8)V99.
              03 INTER-UNITE PIC S9(8)V99.
              03 INTER-PERIODE PIC 99.

       FD  TF-COMPTA
           RECORD VARYING DEPENDING ON IDX-4
           DATA RECORD TF-RECORD.
       01  TF-RECORD.
           02 TFR PIC X OCCURS 1 TO 2350 DEPENDING IDX-4.


       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01  NOT-OPEN PIC 9 VALUE 0.
       01  NUMERO-JOURNAL        PIC 999 VALUE 0.
       01  NUMERO-COMPTA         PIC 99999 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "COUT.REC".
           COPY "COMPTE.REC".
           COPY "LIBELLE.REC".
           COPY "POCL.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".

       01  HELP-XX.
           02 HELP-X                PIC S9(9)V99 OCCURS 2.

       01  COUNTER  PIC 99999 VALUE 0.

       01  TF-DATE.
           03 TF-DATE-J  PIC 99.
           03 TF-DATE-M  PIC 99.
           03 TF-DATE-A  PIC 9999.
       
       01  INTER-NAME.
           02 FILLER             PIC XXXX VALUE "INTC".
           02 FIRME-INTER        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-INTER         PIC XXX.

       01  ECR-DISPLAY.
           02 HE-Z8 PIC -Z(8),ZZ.
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).

      ******************************************************************
      *
       01  BGR00.
      *
      * Batch Input Interface Record Type
           05   STYPE                          PIC 9 VALUE 0.
      * Queue group name
           05   GROUPE                         PIC X(12) VALUE "FI-DOC".
      * Client
           05   MANDT                          PIC 999 VALUE 001.
      * Queue user ID / for historical reasons
           05   USNAM                          PIC X(12) VALUE "IT_1".
      * Queue start date
           05   STARTDATE                      PIC X(8)
                                               VALUE "00000000".
      * Indicator: Hold batch input session after processing ?
           05   XKEEP                          PIC X VALUE "/".
      * No Batch Input Exists for this Field
           05   NODATA                         PIC X VALUE "/".

      ******************************************************************
      *       MEMBER GENERATED FROM SAP DATA DICTIONARY                *
      *                 T A B L E    BBKPF                             *
      *       DATE: 06.06.2000     TIME: 11:29:47                      *
      *             PLEASE DO NOT CHANGE MANUALLY                      *
      ******************************************************************
      *
       01  BBKPF.
      *
      * Batch Input Interface Record Type
           05   STYPE                          PIC 9 VALUE 1.
      * Transaction code
           05   TCODE                          PIC X(20) VALUE "FB01".
      * Date (batch input)
           05   BLDAT                          PIC 9(8) VALUE 0.
qq    * date jour mois ann괻 23.04.2004 

      * Document type
           05   BLART                          PIC X(2) VALUE "AB".
      * Company code
           05   BUKRS                          PIC X(4) VALUE "0001".
      * Date (batch input)
           05   BUDAT                          PIC 9(8) VALUE 0.
qq    * date jour mois ann괻 23.04.2004 
      * Fiscal Month (Batch Input Field)
           05   MONAT                          PIC 99.
      * Currency key
           05   WAERS                          PIC X(5) VALUE "EUR".
      * Exchange Rate Direct Quotation (Batch Input Field)
           05   KURSF                          PIC X(10) VALUE "/".
      * Accounting document number
           05   BELNR                          PIC X(10) VALUE "/".
      * Date (batch input)
           05   WWERT                          PIC X(8) VALUE "/".
      * Reference document number
           05   XBLNR                          PIC X(16) VALUE "/".
      * Number of Cross-Company Code Posting Transaction
           05   BVORG                          PIC X(16) VALUE "/".
      * Document header text
           05   BKTXT                          PIC X(25) VALUE "/".
      * Trading partner's business area
           05   PARGB                          PIC X(4) VALUE "/".
      * Clearing Procedure
           05   AUGLV                          PIC X(8) VALUE "/".
      * Company ID of trading partner
           05   VBUND                          PIC X(6) VALUE "/".
      * Calculate tax automatically
           05   XMWST                          PIC X(1) VALUE "/".
      * Document type
           05   DOCID                          PIC X(10) VALUE "/".
      * SAP ArchiveLink: Document ID
           05   BARCD                          PIC X(40) VALUE "/".
      * Date (batch input)
           05   STODT                          PIC X(8) VALUE "/".
      * Branch number
           05   BRNCH                          PIC X(4) VALUE "/".
      * Number of pages of invoice (batch input field)
           05   NUMPG                          PIC X(3) VALUE "/".
      * Reason for Reversal
           05   STGRD                          PIC X(2) VALUE "/".
      * Indirect Exchange Rate (Batch Input Session)
           05   KURSF-M                        PIC X(10) VALUE "/".
      * Record End Indicator for Batch Input Interface
           05   SENDE                          PIC X(3) VALUE "/##".

      ******************************************************************
      *       MEMBER GENERATED FROM SAP DATA DICTIONARY                *
      *                 T A B L E    BBSEG                             *
      *       DATE: 06.06.2000     TIME: 11:29:50                      *
      *             PLEASE DO NOT CHANGE MANUALLY                      *
      ******************************************************************
      *
       01  BBSEG.
      *
      * Batch Input Interface Record Type
           05   STYPE                          PIC 9 VALUE 2.
      * Table name
           05   TBNAM                          PIC X(30) VALUE "BBSEG".
      * Posting Key for the Next Line Item
           05   NEWBS                          PIC X(2) VALUE "/".
      * Do Not Use Field - Use Field NEWKO
           05   DUMMY                          PIC X(10) VALUE "/".
      * Special G/L Indicator for the Next Line Item
           05   NEWUM                          PIC X(1) VALUE "/".
      * Company Code for the Next Line Item
           05   NEWBK                          PIC X(4) VALUE "/".
      * Amount in document currency (batch input field)
           05   WRBTR                          PIC Z(13),ZZ.
      * Amount in local currency (batch input field)
           05   DMBTR                          PIC X(16) VALUE "/".
      * Tax amount in document currency (batch input field)
           05   WMWST                          PIC X(16) VALUE "/".
      * Tax Amount in Local Currency (Batch Input Field)
           05   MWSTS                          PIC X(16) VALUE "/".
      * Tax on sales/purchases code
           05   MWSKZ                          PIC X(2) VALUE "/".
      * Indicator: Line item not liable to cash discount?
           05   XSKRL                          PIC X(1) VALUE "/".
      * Additional Tax in Document Currency (Batch Input Field)
           05   FWZUZ                          PIC X(16) VALUE "/".
      * Additional Tax in Local Currency (Batch Input Field)
           05   HWZUZ                          PIC X(16) VALUE "/".
      * Business area
           05   GSBER                          PIC X(4) VALUE "/".
      * Cost center
           05   KOSTL                          PIC X(10) VALUE "/".
      * Not to be used anymore (controlling area)
           05   DUMMY4                         PIC X(4) VALUE "0001".
      * Order number
           05   AUFNR                          PIC X(12) VALUE "/".
      * Purchasing document number
           05   EBELN                          PIC X(10) VALUE "/".
      * Line Item Number of Purchasing Document (Batch Input Field)
           05   EBELP                          PIC X(5) VALUE "/".
      * Old: Project number : Do not use any more!! --> PS-POSNR
           05   PROJN                          PIC X(16) VALUE "/".
      * Material number
           05   MATNR                          PIC X(18) VALUE "/".
      * Plant
           05   WERKS                          PIC X(4) VALUE "/".
      * Quantity (Batch Input Field)
           05   MENGE                          PIC X(17) VALUE "/".
      * Base unit of measure
           05   MEINS                          PIC X(3) VALUE "/".
      * Sales document
           05   VBEL2                          PIC X(10) VALUE "/".
      * Sales Document Line Item (Batch Input Field)
           05   POSN2                          PIC X(6) VALUE "/".
      * Schedule Line (Batch Input Field)
           05   ETEN2                          PIC X(4) VALUE "/".
      * Personnel Number (Batch Input Field)
           05   PERNR                          PIC X(8) VALUE "/".
      * Consolidation transaction type
           05   BEWAR                          PIC X(3) VALUE "/".
      * Date (batch input)
           05   VALUT                          PIC X(8) VALUE "/".
      * Date (batch input)
           05   ZFBDT                          PIC X(8) VALUE "/".
      * Exempted from interest calculation
           05   ZINKZ                          PIC X(2) VALUE "/".
      * Assignment number
           05   ZUONR                          PIC X(18) VALUE "/".
      * Financial Budget Item (Batch Input Field)
           05   FKONT                          PIC X(3) VALUE "/".
      * Indicator: Post retirement of assets directly ?
           05   XAABG                          PIC X(1) VALUE "/".
      * Item text
           05   SGTXT                          PIC X(50) VALUE "/".
      * Subsidy indicator for determining the reduction rates
           05   BLNKZ                          PIC X(2) VALUE "/".
      * Base Amount for Determining Preferential Amount (Batch Inp.)
           05   BLNBT                          PIC X(16) VALUE "/".
      * Preference Percentage Rate (Batch Input Field)
           05   BLNPZ                          PIC X(8) VALUE "/".
      * Dunning area
           05   MABER                          PIC X(2) VALUE "/".
      * Amount Subject to Discount (in Doc.Curr.)(Batch Input Field)
           05   SKFBT                          PIC X(16) VALUE "/".
      * Discount amount in document currency (batch input field)
           05   WSKTO                          PIC X(16) VALUE "/".
      * Terms of payment key
           05   ZTERM                          PIC X(4) VALUE "/".
      * Cash Discount Days 1 (Batch Input Field)
           05   ZBD1T                          PIC X(3) VALUE "/".
      * Cash Discount Percentage 1 (Batch Input Field)
           05   ZBD1P                          PIC X(6) VALUE "/".
      * Cash Discount Days 2 (Batch Input Field)
           05   ZBD2T                          PIC X(3) VALUE "/".
      * Cash Discount Percentage 2 (Batch Input Field)
           05   ZBD2P                          PIC X(6) VALUE "/".
      * Period for Net Amount Conditions (Batch Input Field)
           05   ZBD3T                          PIC X(3) VALUE "/".
      * Payment Block Key
           05   ZLSPR                          PIC X(1) VALUE "/".
      * Number of the Invoice the Transaction Belongs to
           05   REBZG                          PIC X(10) VALUE "/".
      * Fiscal Year of the Relevant Invoice (for Credit Memo)
           05   REBZJ                          PIC X(4) VALUE "/".
      * Line Item in the Relevant Invoice (Batch Input)
           05   REBZZ                          PIC X(3) VALUE "/".
      * Payment method
           05   ZLSCH                          PIC X(1) VALUE "/".
      * Invoice List Number (Batch Input Field)
           05   SAMNR                          PIC X(8) VALUE "/".
      * Fixed Payment Terms
           05   ZBFIX                          PIC X(1) VALUE "/".
      * Withholding Tax Code
           05   QSSKZ                          PIC X(2) VALUE "/".
      * Withholding Tax Base Amount (Batch Input Field)
           05   QSSHB                          PIC X(16) VALUE "/".
      * Withholding Tax-Exempt Amount (in Doc.Curr.) (Batch Input)
           05   QSFBT                          PIC X(16) VALUE "/".
      * POR subscriber number
           05   ESRNR                          PIC X(11) VALUE "/".
      * POR Check Digit (Batch Input Field)
           05   ESRPZ                          PIC X(2) VALUE "/".
      * POR reference number
           05   ESRRE                          PIC X(27) VALUE "/".
      * Date (batch input)
           05   FDTAG                          PIC X(8) VALUE "/".
      * Planning level
           05   FDLEV                          PIC X(2) VALUE "/".
      * Main asset number
           05   ANLN1                          PIC X(12) VALUE "/".
      * Asset sub-number
           05   ANLN2                          PIC X(4) VALUE "/".
      * Date (batch input)
           05   BZDAT                          PIC X(8) VALUE "/".
      * Asset Transaction Type
           05   ANBWA                          PIC X(3) VALUE "/".
      * Settlement Period (Batch Input Field)
           05   ABPER                          PIC X(7) VALUE "/".
      * Rate-Hedged Amount in Foreign Currency (Batch Input Field)
           05   GBETR                          PIC X(16) VALUE "/".
      * Fixed Exchange Rate Direct Quotation (Batch Input Field)
           05   KURSR                          PIC X(10) VALUE "/".
      * Dunning block
           05   MANSP                          PIC X(1) VALUE "/".
      * Dunning key
           05   MSCHL                          PIC X(1) VALUE "/".
      * Short key for a house bank
           05   HBKID                          PIC X(5) VALUE "/".
      * Partner bank type
           05   BVTYP                          PIC X(4) VALUE "/".
      * Document Number of the Bill of Exchange Payment Request
           05   ANFBN                          PIC X(10) VALUE "/".
      * Company Code in Which Bill of Exch.Payment Request Is Posted
           05   ANFBU                          PIC X(4) VALUE "/".
      * Fiscal Year of the B/Ex. Pmnt Request Document (Batch Input)
           05   ANFBJ                          PIC X(4) VALUE "/".
      * State central bank indicator
           05   LZBKZ                          PIC X(3) VALUE "/".
      * Supplying country
           05   LANDL                          PIC X(3) VALUE "/".
      * Service indicator (foreign payment)
           05   DIEKZ                          PIC X(1) VALUE "/".
      * Date (batch input)
           05   ZOLLD                          PIC X(8) VALUE "/".
      * Customs Tariff Number
           05   ZOLLT                          PIC X(8) VALUE "/".
      * Date (batch input)
           05   VRSDT                          PIC X(8) VALUE "/".
      * Insurance indicator
           05   VRSKZ                          PIC X(1) VALUE "/".
      * Assignment Number for Special G/L Accounts
           05   HZUON                          PIC X(18) VALUE "/".
      * Indicator: Individual Payee in Document
           05   REGUL                          PIC X(1) VALUE "/".
      * Name 1
           05   NAME1                          PIC X(35) VALUE "/".
      * Name 2
           05   NAME2                          PIC X(35) VALUE "/".
      * Name 3
           05   NAME3                          PIC X(35) VALUE "/".
      * Name 4
           05   NAME4                          PIC X(35) VALUE "/".
      * House number and street
           05   STRAS                          PIC X(35) VALUE "/".
      * City
           05   ORT01                          PIC X(35) VALUE "/".
      * Postal Code
           05   PSTLZ                          PIC X(10) VALUE "/".
      * Country key
           05   LAND1                          PIC X(3) VALUE "/".
      * Region (State, Province, County)
           05   REGIO                          PIC X(3) VALUE "/".
      * Bank key
           05   BANKL                          PIC X(15) VALUE "/".
      * Bank country key
           05   BANKS                          PIC X(3) VALUE "/".
      * Bank account number
           05   BANKN                          PIC X(18) VALUE "/".
      * Bank control key
           05   BKONT                          PIC X(2) VALUE "/".
      * Tax number 1
           05   STCD1                          PIC X(16) VALUE "/".
      * Tax number 2
           05   STCD2                          PIC X(11) VALUE "/".
      * Date (batch input)
           05   MADAT                          PIC X(8) VALUE "/".
      * Dunning Level (Batch Input Field)
           05   MANST                          PIC X(1) VALUE "/".
      * Reporting Country for Delivery of Goods within the EU
           05   EGMLD                          PIC X(3) VALUE "/".
      * Do not use any more (ctry of destin.for deliv.of gds)
           05   DUMMY2                         PIC X(3) VALUE "/".
      * VAT registration number
           05   STCEG                          PIC X(20) VALUE "/".
      * Indicator: Business Partner Subject to Equalization Tax?
           05   STKZA                          PIC X(1) VALUE "/".
      * Indicator: Business partner subject to tax on sales/purch. ?
           05   STKZU                          PIC X(1) VALUE "/".
      * P.O. Box
           05   PFACH                          PIC X(10) VALUE "/".
      * P.O. Box postal code
           05   PSTL2                          PIC X(10) VALUE "/".
      * Language Key
           05   SPRAS                          PIC X(1) VALUE "/".
      * Indicator: Capital Goods Affected?
           05   XINVE                          PIC X(1) VALUE "/".
      * Account or Matchcode for the Next Line Item
           05   NEWKO                          PIC X(17) VALUE "/".
      * Asset transaction type
           05   NEWBW                          PIC X(3) VALUE "/".
      * Alternative Head Office (Batch Input Field)
           05   KNRZE                          PIC X(17) VALUE "/".
      * General ledger account
           05   HKONT                          PIC X(10) VALUE "/".
      * Profit center
           05   PRCTR                          PIC X(10) VALUE "/".
      * Contract number
           05   VERTN                          PIC X(13) VALUE "/".
      * Contract type
           05   VERTT                          PIC X(1) VALUE "/".
      * Flow type
           05   VBEWA                          PIC X(4) VALUE "/".
      * Tax Base Amount in Local Currency
           05   HWBAS                          PIC X(16) VALUE "/".
      * Tax Base Amount in Document Currency
           05   FWBAS                          PIC X(16) VALUE "/".
      * Commitment item
           05   FIPOS                          PIC X(14) VALUE "/".
      * Joint Venture
           05   VNAME                          PIC X(6) VALUE "/".
      * Equity group
           05   EGRUP                          PIC X(3) VALUE "/".
      * Billing indicator
           05   BTYPE                          PIC X(2) VALUE "/".
      * Number for Profitability Segments (CO-PA) (Batch Input Fld)
           05   PAOBJNR                        PIC X(10) VALUE "/".
      * Cost object
           05   KSTRG                          PIC X(12) VALUE "/".
      * Internal key for real estate object
           05   IMKEY                          PIC X(8) VALUE "/".
      * No longer use this field (use PROJK)
           05   DUMMY3                         PIC X(8) VALUE "/".
      * Partner account number
           05   VPTNR                          PIC X(10) VALUE "/".
      * Network number for account assignment
           05   NPLNR                          PIC X(12) VALUE "/".
      * Operation/activity number
           05   VORNR                          PIC X(4) VALUE "/".
      * Indicator: Triangular deal within the EU ?
           05   XEGDR                          PIC X(1) VALUE "/".
      * Recovery indicator
           05   RECID                          PIC X(2) VALUE "/".
      * Partner profit center
           05   PPRCT                          PIC X(10) VALUE "/".
      * Project Account Assignment (PS-PSP-PNR Batch Input Field)
           05   PROJK                          PIC X(24) VALUE "/".
      * Payment method supplement
           05   UZAWE                          PIC X(2) VALUE "/".
      * Jurisdiction for tax calculation - tax jurisdiction code
           05   TXJCD                          PIC X(15) VALUE "/".
      * Funds center
           05   FISTL                          PIC X(16) VALUE "/".
      * Fund
           05   GEBER                          PIC X(10) VALUE "/".
      * Amount in Second Local Currency
           05   DMBE2                          PIC X(16) VALUE "/".
      * Amount in Third Local Currency
           05   DMBE3                          PIC X(16) VALUE "/".
      * Trading partner's business area
           05   PARGB                          PIC X(4) VALUE "/".
      * Business partner reference key
           05   XREF1                          PIC X(12) VALUE "/".
      * Business partner reference key
           05   XREF2                          PIC X(12) VALUE "/".
      * Document number for earmarked funds
           05   KBLNR                          PIC X(10) VALUE "/".
      * Line Item
           05   KBLPOS                         PIC X(3) VALUE "/".
      * Date (batch input)
           05   WDATE                          PIC X(8) VALUE "/".
      * Indicator for the bill of exchange protest
           05   WGBKZ                          PIC X(1) VALUE "/".
      * Indicator: Bill of exchange was accepted
           05   XAKTZ                          PIC X(1) VALUE "/".
      * Name of bill of exchange drawer
           05   WNAME                          PIC X(30) VALUE "/".
      * City of bill of exchange drawer
           05   WORT1                          PIC X(30) VALUE "/".
      * Bill of exchange drawee
           05   WBZOG                          PIC X(30) VALUE "/".
      * City of bill of exchange drawee
           05   WORT2                          PIC X(30) VALUE "/".
      * Bank address where a bill of exchange can be paid (domestic)
           05   WBANK                          PIC X(60) VALUE "/".
      * State central bank location
           05   WLZBP                          PIC X(60) VALUE "/".
      * Percentage Rate for Discounting (Batch Input Field)
           05   DISKP                          PIC X(8) VALUE "/".
      * Discount Days (Batch Input Field)
           05   DISKT                          PIC X(3) VALUE "/".
      * Charge for bill of exchange collection (batch input field)
           05   WINFW                          PIC X(16) VALUE "/".
      * cHARGE FOR BILL OF EXCHANGE COLLECTION (BATCH INPUT FIELD)
           05   WINHW                          PIC X(16) VALUE "/".
      * Planned usage of the bill of exchange
           05   WEVWV                          PIC X(1) VALUE "/".
      * Bill of exchange status
           05   WSTAT                          PIC X(1) VALUE "/".
      * Tax code for bill of exchange charges
           05   WMWKZ                          PIC X(2) VALUE "/".
      * Bill of exchange tax code
           05   WSTKZ                          PIC X(1) VALUE "/".
      * Product number
           05   RKE-ARTNR                      PIC X(18) VALUE "/".
      * Volume rebate group
           05   RKE-BONUS                      PIC X(2) VALUE "/".
      * Industry key
           05   RKE-BRSCH                      PIC X(4) VALUE "/".
      * Company code
           05   RKE-BUKRS                      PIC X(4) VALUE "/".
      * Sales district
           05   RKE-BZIRK                      PIC X(6) VALUE "/".
      * Form of manufacture
           05   RKE-EFORM                      PIC X(5) VALUE "/".
      * Billing type
           05   RKE-FKART                      PIC X(4) VALUE "/".
      * State
           05   RKE-GEBIE                      PIC X(4) VALUE "/".
      * Business area
           05   RKE-GSBER                      PIC X(4) VALUE "/".
      * Sales order number
           05   RKE-KAUFN                      PIC X(10) VALUE "/".
      * Customer group
           05   RKE-KDGRP                      PIC X(2) VALUE "/".
      * Character field, length 6
           05   RKE-KDPOS                      PIC X(6) VALUE "/".
      * Customer
           05   RKE-KNDNR                      PIC X(10) VALUE "/".
      * Controlling area
           05   RKE-KOKRS                      PIC X(4) VALUE "/".
      * Cost object
           05   RKE-KSTRG                      PIC X(12) VALUE "/".
      * Country key
           05   RKE-LAND1                      PIC X(3) VALUE "/".
      * ABC indicator
           05   RKE-MAABC                      PIC X(1) VALUE "/".
      * Material group
           05   RKE-MATKL                      PIC X(9) VALUE "/".
      * Profit center
           05   RKE-PRCTR                      PIC X(10) VALUE "/".
      * Character field, length 24
           05   RKE-PSPNR                      PIC X(24) VALUE "/".
      * Order number
           05   RKE-RKAUFNR                    PIC X(12) VALUE "/".
      * Division
           05   RKE-SPART                      PIC X(2) VALUE "/".
      * Sales office
           05   RKE-VKBUR                      PIC X(4) VALUE "/".
      * Sales group
           05   RKE-VKGRP                      PIC X(3) VALUE "/".
      * Sales organization
           05   RKE-VKORG                      PIC X(4) VALUE "/".
      * Distribution channel
           05   RKE-VTWEG                      PIC X(2) VALUE "/".
      * Plant
           05   RKE-WERKS                      PIC X(4) VALUE "/".
      * Character field, length 2
           05   RKE-KMBRND                     PIC X(2) VALUE "/".
      * Character field, length 2
           05   RKE-KMCATG                     PIC X(2) VALUE "/".
      * Customer hierarchy Level 1
           05   RKE-KMHI01                     PIC X(10) VALUE "/".
      * Customer hierarchy Level 2
           05   RKE-KMHI02                     PIC X(10) VALUE "/".
      * Customer hierarchy Level 3
           05   RKE-KMHI03                     PIC X(10) VALUE "/".
      * Customer group
           05   RKE-KMKDGR                     PIC X(2) VALUE "/".
      * Country key
           05   RKE-KMLAND                     PIC X(3) VALUE "/".
      * Material group
           05   RKE-KMMAKL                     PIC X(9) VALUE "/".
      * Nielsen ID
           05   RKE-KMNIEL                     PIC X(2) VALUE "/".
      * Character field, length 2
           05   RKE-KMSTGE                     PIC X(2) VALUE "/".
      * Sales office
           05   RKE-KMVKBU                     PIC X(4) VALUE "/".
      * Sales group
           05   RKE-KMVKGR                     PIC X(3) VALUE "/".
      * Character field, length 8
           05   RKE-KMVTNR                     PIC X(8) VALUE "/".
      * Partner profit center
           05   RKE-PPRCTR                     PIC X(10) VALUE "/".
+     * Material Pricing Group
+          05   RKE-KONDM                      PIC X(2) VALUE "/".
+     * Product hierarchy
+          05   RKE-PRODH                      PIC X(18) VALUE "/".
      * Company ID of trading partner
           05   VBUND                          PIC X(6) VALUE "/".
      * Functional Area
           05   FKBER                          PIC X(4) VALUE "/".
      * Date (batch input)
           05   DABRZ                          PIC X(8) VALUE "/".
      * Indicator: Determine tax base?
           05   XSTBA                          PIC X(1) VALUE "/".
      * Reason Code for Payments
           05   RSTGR                          PIC X(3) VALUE "/".
      * Commitment Item - for IS-PS Only (Batch Input Field)
           05   FIPEX                          PIC X(24) VALUE "/".
      * Indicator: Negative posting
           05   XNEGP                          PIC X(1) VALUE "/".
      * Activity Code for Gross Income Tax
           05   GRICD                          PIC X(2) VALUE "/".
      * Region (State, Province, County)
           05   GRIRG                          PIC X(3) VALUE "/".
      * Distribution Type for Employment Tax
           05   GITYP                          PIC X(2) VALUE "/".
      * Tax category
           05   FITYP                          PIC X(2) VALUE "/".
      * Tax number type
           05   STCDT                          PIC X(2) VALUE "/".
      * Indicator: Is business partner a natural person?
           05   STKZN                          PIC X(1) VALUE "/".
      * Tax Number 3
           05   STCD3                          PIC X(18) VALUE "/".
      * Tax Number 4
           05   STCD4                          PIC X(18) VALUE "/".
      * Reference key for line item
           05   XREF3                          PIC X(20) VALUE "/".
      * Payment Reference
           05   KIDNO                          PIC X(30) VALUE "/".
      * Instruction Key 1 (Batch Input Field)
           05   DTWS1                          PIC X(2) VALUE "/".
      * Instruction Key 2 (Batch Input Field)
           05   DTWS2                          PIC X(2) VALUE "/".
      * Instruction Key 3 (Batch Input Field)
           05   DTWS3                          PIC X(2) VALUE "/".
      * Instruction Key 4 (Batch Input Field)
           05   DTWS4                          PIC X(2) VALUE "/".
      * Instruction key for data medium exchange
           05   DTAWS                          PIC X(2) VALUE "/".
      * Currency for Automatic Payment
           05   PYCUR                          PIC X(5) VALUE "/".
      * Amount in Payment Currency (Batch Input Field)
           05   PYAMT                          PIC X(16) VALUE "/".
      * Business Place
           05   BUPLA                          PIC X(4) VALUE "/".
      * Section code
           05   SECCO                          PIC X(4) VALUE "/".
      * Activity Type
           05   LSTAR                          PIC X(6) VALUE "/".
      * Goods Recipient for EU Triangular Deals
           05   EGDEB                          PIC X(10) VALUE "/".
      * Number of business entity
           05   WENR                           PIC X(8) VALUE "/".
      * Building number
           05   GENR                           PIC X(8) VALUE "/".
      * Property number for BE
           05   GRNR                           PIC X(8) VALUE "/".
      * Number of rental unit
           05   MENR                           PIC X(8) VALUE "/".
      * Rental agreement number
           05   MIVE                           PIC X(13) VALUE "/".
      * Service charge key
           05   NKSL                           PIC X(4) VALUE "/".
      * Settlement unit
           05   EMPSL                          PIC X(5) VALUE "/".
      * Management contract number
           05   SVWNR                          PIC X(13) VALUE "/".
      * Correction item number
           05   SBERI                          PIC X(10) VALUE "/".
      * Credit control area
           05   KKBER                          PIC X(4) VALUE "/".
      * Payee/Payer
           05   EMPFB                          PIC X(10) VALUE "/".
      * Indirect Hedged Exchange Rate (Batch Input Field)
           05   KURSR-M                        PIC X(10) VALUE "/".
      * Name of representative
           05   J-1KFREPRE                     PIC X(10) VALUE "/".
      * Type of business
           05   J-1KFTBUS                      PIC X(30) VALUE "/".
      * Type of industry
           05   J-1KFTIND                      PIC X(30) VALUE "/".
      * Record End Indicator for Batch Input Interface
           05   SENDE                          PIC X(3) VALUE "/##".

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               TF-COMPTA
               INTER.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-SAP.
       

           CALL "0-TODAY" USING TODAY.
           MOVE TODAY-ANNEE TO TF-DATE-A.
           MOVE TODAY-MOIS  TO TF-DATE-M.
           MOVE TODAY-JOUR  TO TF-DATE-J.
           MOVE FR-KEY TO FIRME-INTER.
           MOVE LNK-USER  TO USER-INTER.
           INITIALIZE COUT-RECORD.
           OPEN INPUT INTER.
           INITIALIZE HELP-XX.
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END GO READ-INTER-END.
      *    IF INTER-LIB = 0
      *       GO READ-INTER.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-COMPTA
              MOVE 1 TO NOT-OPEN
           END-IF.
           IF FR-KEY  NOT = COUT-FIRME
           OR INTER-COUT NOT = COUT-NUMBER
              PERFORM READ-COUT.
           PERFORM FILL-TEXTE.
           GO READ-INTER.
       READ-INTER-END.
           MOVE COUNTER TO HE-Z8.
           DISPLAY HE-Z8 LINE 24 POSITION 30.
           IF HELP-X(1) NOT = HELP-X(2) 
           OR COUNTER > 950
              PERFORM AA.
           PERFORM END-PROGRAM.

       FILL-TEXTE.
           IF INTER-VALUE > 0
              IF INTER-DC = 1
                 MOVE 40 TO NEWBS
              ELSE
                 MOVE 50 TO NEWBS
              END-IF
           ELSE
              IF INTER-DC = 1
                 MOVE 50 TO NEWBS
              ELSE
                 MOVE 40 TO NEWBS
              END-IF
           END-IF.
           ADD INTER-VALUE TO HELP-X(INTER-DC).
           MOVE HELP-X(1) TO HE-Z8.
           DISPLAY HE-Z8 LINE 23 POSITION 40.
           MOVE HELP-X(2) TO HE-Z8.
           DISPLAY HE-Z8 LINE 23 POSITION 55.

           MOVE INTER-COMPTE TO NEWKO.
           MOVE INTER-VALUE TO WRBTR.
           MOVE INTER-MOIS  TO MONAT.
           MOVE TF-DATE TO STARTDATE BLDAT BUDAT.
           MOVE "/" TO SGTXT KOSTL AUFNR.
           MOVE INTER-LIB   TO LIB-NUMBER.
           CALL "6-LIB" USING LINK-V LIB-RECORD FAKE-KEY.
           MOVE LIB-DESCRIPTION TO SGTXT.
           IF INTER-PERS-2 > 0
              MOVE INTER-PERS-2 TO REG-PERSON
              CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY
              MOVE REG-MATRICULE TO PR-MATRICULE
              CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY
              CALL "4-PRNOM"  USING LINK-V PR-RECORD
              MOVE LNK-TEXT TO SGTXT
           END-IF.
           IF INTER-POSTE NOT = 0
              IF INTER-POSTE > 99999
                 MOVE INTER-POSTE TO HE-Z6 
                 MOVE HE-Z6 TO KOSTL 
              ELSE
                 MOVE INTER-POSTE TO HE-Z5 
                 MOVE HE-Z5 TO AUFNR 
           END-IF.
           IF NUMERO-COMPTA = 0
              MOVE 38 TO IDX-4
              INSPECT BGR00 CONVERTING
              "굤닀뀑뙎봺뼏꽇쉸" TO "eeeeaiioouuuaOUA"
              WRITE TF-RECORD FROM BGR00
              ADD 1 TO COUNTER
              MOVE FR-COMPTA TO BUKRS
              MOVE 232 TO IDX-4
              INSPECT BBKPF CONVERTING
              "굤닀뀑뙎봺뼏꽇쉸" TO "eeeeaiioouuuaOUA"
              ADD 1 TO COUNTER
              WRITE TF-RECORD FROM BBKPF.
           ADD 1 TO NUMERO-COMPTA.
           MOVE 2347 TO IDX-4.
           INSPECT BBSEG CONVERTING
           "굤닀뀑뙎봺뼏꽇쉸" TO "eeeeaiioouuuaOUA".
           ADD 1 TO COUNTER.
           WRITE TF-RECORD FROM BBSEG.
           MOVE HELP-X(1) TO HE-Z8.
           DISPLAY HE-Z8 LINE 24 POSITION 40.
           MOVE HELP-X(2) TO HE-Z8.
           DISPLAY HE-Z8 LINE 24 POSITION 55.

       READ-COUT.
           MOVE FR-KEY TO COUT-FIRME.
           MOVE INTER-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.

       END-PROGRAM.
           CLOSE INTER.
           IF NOT-OPEN = 1
              CLOSE TF-COMPTA.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------
           COPY "XACTION.CPY".
