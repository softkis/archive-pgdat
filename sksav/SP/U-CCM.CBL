      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME U-CCM CONVERSION CENTRES DE COUT MULTIPLES  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    U-CCM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CCM2.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CCM2.FDE".
               
       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "CCM.LNK".
           COPY "REGISTRE.REC".

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

      *DECLARATIVES.

      *FILE-ERR-PROC SECTION.

      *    USE AFTER ERROR PROCEDURE ON CCM.

      *FILE-ERROR-PROC.
      *    CALL "C$RERR" USING EXTENDED-STATUS.
      *    CALL "0-ERROR" USING LINK-V.
      *    EXIT  program.
      *END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-U-CCM.

           OPEN INPUT CCM.

           INITIALIZE CCM-RECORD.
           START CCM KEY >= CCM-KEY INVALID EXIT PROGRAM
              NOT INVALID
              PERFORM CCM-C THRU CCM-C-END.
       CCM-C.
           READ CCM NEXT NO LOCK AT END 
              GO CCM-C-END.
           INITIALIZE LINK-RECORD.
           MOVE CCM-KEY TO LINK-KEY.
           PERFORM CONV VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.
           MOVE CCM-OCCURS TO LINK-OCCURS.
           MOVE CCM-TOTAL  TO LINK-TOTAL.
           MOVE CCM-STAMP  TO LINK-STAMP.
           MOVE CCM-PERSON TO REG-PERSON.
           MOVE CCM-FIRME  TO FR-KEY.
           CALL "6-CCM" USING LINK-V LINK-RECORD REG-RECORD WR-KEY.
           GO CCM-C.
       CCM-C-END.
           EXIT PROGRAM.

       CONV.
           MOVE CCM-TAUX(IDX) TO LINK-TAUX(IDX).
           MOVE CCM-COUT(IDX) TO LINK-COUT(IDX).


      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".


