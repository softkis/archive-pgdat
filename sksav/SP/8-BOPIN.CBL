      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-BOPIN   TRANSFERT COMPTABILITE BOP        �
      *  � IMPRIMERIE CENTRALE - SANS POSTES DE FRAIS NI PERSONNE�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-BOPIN.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "INTERCOM.FC".
           SELECT OPTIONAL TF-COMPTA ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.


       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire JOURNAL COMPTABLE

       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-MOIS   PIC 99.
              03 INTER-SUITE  PIC 99.
              03 INTER-STAT   PIC 99.
              03 INTER-COUT   PIC 9(8).
              03 INTER-COUT-1 REDEFINES INTER-COUT.
                 04 INTER-FIL PIC 99999.
                 04 INTER-CT  PIC 999.
              03 INTER-PERS   PIC 9(8).
              03 INTER-DC     PIC 9.
              03 INTER-COMPTE PIC X(30).
              03 INTER-POSTE  PIC 9(10).
              03 INTER-EXT    PIC X(50).
              03 INTER-LIB    PIC 9999.
              03 INTER-PERS-2 PIC 9(8).


           02 INTER-REC-DET.
              03 INTER-VALUE PIC S9(8)V99.
              03 INTER-UNITE PIC S9(8)V99.
              03 INTER-PERIODE PIC 99.

       FD  TF-COMPTA
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.

       01  TF-RECORD PIC X(200).
        

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01  NOT-OPEN PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "COUT.REC".
           COPY "COMPTE.REC".
           COPY "LIBELLE.REC".
           COPY "POCL.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".

       01  HELP-XX.
           02 HELP-X                PIC S9(9)V99 OCCURS 2.

       01  COUNTER  PIC 999 VALUE 0.

       01  TF-HELP.
           02 TF-HEADER   PIC X(4) VALUE "SAL".
           02 TF-ANNEE    PIC 9999.
           02 TF-FILL-A   PIC X VALUE " ".
           02 TF-ANNEE-1  PIC 9999.
           02 TF-FILL-B   PIC X(7) VALUE SPACES.
           02 TF-MOIS     PIC ZZ.
           02 TF-FILL-C   PIC X(9) VALUE SPACES.
           02 TF-MOIS-2   PIC ZZ.
           02 TF-FILL-D   PIC X(9) VALUE SPACES.
           02 TF-LIGNE    PIC Z(3).
           02 TF-FILL-E   PIC X(8) VALUE SPACES.
           02 TF-FILL-0   PIC X VALUE "B".

           02 TF-DATE.
              03 TF-DATE-J  PIC 99.
              03 TF-FILL-1  PIC X VALUE "/".
              03 TF-DATE-M  PIC 99.
              03 TF-FILL-2  PIC X VALUE "/".
              03 TF-DATE-A  PIC ZZZZ.
              03 TF-FILL-3  PIC X VALUE " ".
           02 TF-FILL-0   PIC X VALUE "A".
           02 TF-COMPTE   PIC X(10).
           02 TF-LETTRAGE PIC X(20).
           02 TF-COUT     PIC X(10).

           02 TF-MONTANT  PIC Z(11),ZZ.
           02 TF-DC       PIC X.
           02 TF-LIBELLE  PIC X(40).
       
       01  INTER-NAME.
           02 I-NAME             PIC XXXX VALUE "INTC".
           02 FIRME-INTER        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-INTER         PIC XXX.

       01  ECR-DISPLAY.
           02 HE-Z8 PIC -Z(8),ZZ.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               TF-COMPTA
               INTER.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-BOPIN.
       
           ADD 1 TO  PARMOD-PROG-NUMBER-1.
           CALL "0-TODAY" USING TODAY.
           MOVE FR-KEY   TO FIRME-INTER.
           MOVE LNK-USER TO USER-INTER.
           IF MENU-PROG-NAME = "3-JM"
              MOVE "ICCM" TO I-NAME.
           INITIALIZE COUT-RECORD.
           OPEN INPUT INTER.
           INITIALIZE HELP-XX.
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END GO READ-INTER-END.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-COMPTA
              MOVE 1 TO NOT-OPEN
           END-IF.
           IF INTER-COUT > 0
              IF FR-KEY     NOT = COUT-FIRME
              OR INTER-COUT NOT = COUT-NUMBER
                 PERFORM READ-COUT.
           PERFORM FILL-TEXTE.
           GO READ-INTER.
       READ-INTER-END.
           IF HELP-X(1) NOT = HELP-X(2)
              ACCEPT ACTION NO BEEP.
           PERFORM END-PROGRAM.

       FILL-TEXTE.
           INITIALIZE TF-RECORD.
           ADD INTER-VALUE TO HELP-X(INTER-DC).
           IF INTER-VALUE < 0
              IF INTER-DC = 1
                 ADD 1 TO INTER-DC
              ELSE
                 SUBTRACT 1 FROM INTER-DC
              END-IF
           END-IF.

           IF INTER-DC = 1
              MOVE "D" TO TF-DC 
           ELSE 
              MOVE "C" TO TF-DC 
           END-IF.

           MOVE INTER-LIB TO LIB-NUMBER.
           CALL "6-LIB" USING LINK-V LIB-RECORD FAKE-KEY.
           MOVE LIB-DESCRIPTION TO TF-LIBELLE.
      *    IF INTER-MOIS > 0
DUMMY      IF INTER-MOIS > 13
              MOVE 0 TO COL-IDX
              INSPECT TF-LIBELLE TALLYING COL-IDX FOR CHARACTERS 
              BEFORE "    "
              ADD 2 TO COL-IDX
              IF COL-IDX < 39
                 STRING INTER-MOIS DELIMITED BY SIZE INTO TF-LIBELLE
                 WITH POINTER COL-IDX
              END-IF
              ADD 1 TO COL-IDX
              IF COL-IDX < 39
                 STRING LNK-ANNEE DELIMITED BY SIZE INTO TF-LIBELLE
                 WITH POINTER COL-IDX
              END-IF
           END-IF.
           MOVE FR-COMPTA TO TF-HEADER.
           MOVE LNK-ANNEE TO TF-ANNEE TF-ANNEE-1 TF-DATE-A.
           MOVE INTER-MOIS   TO TF-MOIS .
           IF INTER-MOIS = 0
              MOVE LNK-MOIS  TO TF-MOIS.
           MOVE TF-MOIS   TO TF-MOIS-2 TF-DATE-M IDX.
           MOVE MOIS-JRS(IDX) TO TF-DATE-J.
           MOVE INTER-COMPTE TO TF-COMPTE.
      *    MOVE TODAY-ANNEE  TO TF-DATE-A.
      *    MOVE TODAY-MOIS   TO TF-DATE-M.
      *    MOVE TODAY-JOUR   TO TF-DATE-J.

           MOVE INTER-COUT  TO TF-COUT.
           IF COUT-ANALYTIC NOT = SPACES
              MOVE COUT-ANALYTIC TO TF-COUT.
           MOVE INTER-VALUE TO TF-MONTANT.
           ADD 1 TO COUNTER.
           MOVE COUNTER TO TF-LIGNE.
           INSPECT TF-HELP CONVERTING
           "굤닀뀑뙏뱚뼏꽇쉸릥�" TO "郵幽攝專抄化斡寶�譽".
           WRITE TF-RECORD FROM TF-HELP.
           MOVE HELP-X(1) TO HE-Z8.
           DISPLAY HE-Z8 LINE 24 POSITION 40.
           MOVE HELP-X(2) TO HE-Z8.
           DISPLAY HE-Z8 LINE 24 POSITION 55.

       READ-COUT.
           MOVE FR-KEY TO COUT-FIRME.
           MOVE INTER-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.

       END-PROGRAM.
           CLOSE INTER.
           IF NOT-OPEN = 1
              CLOSE TF-COMPTA.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

