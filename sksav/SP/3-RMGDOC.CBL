      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-RMGDOC  SAISIE DOCUMENTS RMG              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-RMGDOC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           SELECT OPTIONAL HELP-FILE ASSIGN TO DISK HELP-NAME
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-HELP.

       DATA DIVISION.

       FILE SECTION.

       FD  HELP-FILE
           BLOCK CONTAINS 1 RECORDS
           RECORD CONTAINS 80 CHARACTERS
           LABEL RECORD STANDARD
           DATA RECORD IS REC-HELP.
       01  REC-HELP PIC X(80).

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 19.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "LIVRE.REC".
           COPY "LIVRE.LNK".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CONTRAT.REC".
           COPY "CCOL.REC".
           COPY "FICHE.REC".
           COPY "METIER.REC".
           COPY "PRESENCE.REC".
           COPY "DOC.REC".
           COPY "PARMOD.REC".
           COPY "HORSEM.REC".
           COPY "POSE.REC".
           COPY "V-BASES.REC".

       01  HELP-1      PIC 9(6)V99.
       01  HELP-NAME   PIC X(12).
       01  TEST-TEXT   PIC X(6).
       01  LINE-NUMBER PIC 999.
       01  COUNTER     PIC 999.
       01  EFFACER     PIC X(6) VALUE SPACES.
       01  FORMULAIRE.
           02 FORM-LINE  PIC X(80) OCCURS 240.
       01  FORM-PAGES REDEFINES FORMULAIRE.
           02 FORM-PAGE OCCURS 4.
              03 FORM-LINES PIC X(80) OCCURS 60.
  
       01   ECR-DISPLAY.
            02 HE-Z2 PIC ZZ  BLANK WHEN ZERO.
            02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4).
            02 HE-Z6 PIC Z(6).
            02 HE-Z8 PIC Z(8)-.
            02 HE-Z2Z2 PIC ZZ,ZZ. 
            02 HE-Z4Z2 PIC -ZZZZ,ZZ BLANK WHEN ZERO. 
            02 HE-Z6Z2 PIC Z(6),ZZ. 
            02 HE-Z6Z4 PIC Z(6),Z(4). 
            02 HE-Z8Z3 PIC Z(8),ZZZ BLANK WHEN ZERO.
           02 HE-DATE.
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       
       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON HELP-FILE.
       
       FILE-ERROR-PROC.
           IF FS-HELP  = "35"
              MOVE "AA" TO LNK-AREA
              MOVE 21 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           ELSE
           CALL "C$RERR" USING EXTENDED-STATUS
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-RMGDOC.

           PERFORM AFFICHAGE-ECRAN .
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1  MOVE 0163640015 TO EXC-KFR(1)
                   MOVE 0000000065 TO EXC-KFR(13)
                   MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                   MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3  MOVE 0017000007 TO EXC-KFR(1)
                   MOVE 1700000000 TO EXC-KFR(2)
                   MOVE 0000000065 TO EXC-KFR(13)
                   MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9 THRU 18 MOVE 0000000025 TO EXC-KFR(1)
           WHEN 19 MOVE 0000000005 TO EXC-KFR(1)
                   MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 THRU 18 PERFORM AVANT-TXT
           WHEN 19 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7 
           WHEN  9 THRU 18 PERFORM APRES-TXT
           WHEN 19 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           MOVE FR-KEY TO REG-FIRME.
           IF LNK-PERSON NOT = 0 
              MOVE LNK-PERSON TO REG-PERSON 
           ELSE 
              ACCEPT REG-PERSON 
              LINE 3 POSITION 15 SIZE 6
              TAB UPDATE NO BEEP CURSOR 1
              ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           IF DOC-SUITE = 0
              MOVE 1 TO DOC-SUITE
           END-IF.
           ACCEPT DOC-SUITE 
             LINE  5 POSITION 27 SIZE 3
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4.
           IF MENU-EXTENSION-1  NOT = SPACES
           AND DOC-FORM = SPACES
              MOVE MENU-EXTENSION-1 TO DOC-FORM 
           END-IF.
           ACCEPT DOC-FORM 
             LINE 6 POSITION 27 SIZE 12
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "UPPER"
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-5.
           ACCEPT DOC-TITRE 
             LINE  7 POSITION 27 SIZE 50
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-6.
           IF DOC-LANGUE = SPACES
              MOVE "F" TO DOC-LANGUE.
           ACCEPT DOC-LANGUE
             LINE  8 POSITION 27 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.           
           MOVE 09270000 TO LNK-POSITION.
           CALL "0-GDATE" USING DOC-DATE
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-DATE TO DOC-DATE
              MOVE 0 TO LNK-NUM
              MOVE 13 TO EXC-KEY
              PERFORM DIS-HE-07.

       AVANT-8.
           MOVE DOC-FORM TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           IF DOC-AUTEUR = SPACES
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R"
              MOVE PARMOD-SETTINGS TO DOC-AUTEUR.

           ACCEPT DOC-AUTEUR 
             LINE 10 POSITION 27 SIZE 50
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE DOC-AUTEUR TO PARMOD-SETTINGS.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       AVANT-TXT.
           COMPUTE IDX = INDICE-ZONE - 8.
           COMPUTE LIN-IDX = IDX + 11
           ACCEPT DOC-T(IDX)
           LINE  LIN-IDX POSITION 10 SIZE 70
           TAB UPDATE NO BEEP CURSOR  1 
           ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE
              IF PRES-TOT(LNK-MOIS) = 0
                 MOVE 1 TO INPUT-ERROR
              END-IF.
           IF LNK-COMPETENCE < REG-COMPETENCE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           INITIALIZE DOC-RECORD.
           PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 
                MOVE "A" TO A-N 
                PERFORM NEXT-REGIS
                IF INPUT-ERROR = 0 
                AND REG-PERSON > 0 
                  IF PRES-TOT(LNK-MOIS) = 0
                     MOVE 1 TO INPUT-ERROR
                  END-IF
                END-IF
                IF LNK-COMPETENCE < REG-COMPETENCE
                   MOVE 1 TO INPUT-ERROR
                END-IF
                INITIALIZE DOC-RECORD
           END-EVALUATE.
           IF REG-PERSON > 0 
              IF DOC-SUITE = 0
                 INITIALIZE DOC-RECORD
                 MOVE 65 TO SAVE-KEY 
                 PERFORM READ-WRITE
              END-IF
           ELSE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.
           

       APRES-3.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 5 IF DOC-FORM = SPACES
                     MOVE 1 TO INPUT-ERROR
                  ELSE
                     MOVE 0 TO INPUT-ERROR
                     PERFORM READ-FORM
                  END-IF
                  IF INPUT-ERROR = 0
                     PERFORM PRINT-JOB
                  END-IF
                  MOVE 3 TO DECISION
           WHEN 2 CALL "2-DOC" USING LINK-V DOC-RECORD PR-RECORD 
                  REG-RECORD 
                  CANCEL "2-DOC"
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR
           WHEN OTHER PERFORM READ-WRITE
           END-EVALUATE.
           IF DOC-SUITE = 0
              MOVE 1 TO INPUT-ERROR
           END-IF
           PERFORM AFFICHAGE-DETAIL.

       APRES-4.
           IF DOC-FORM = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-6.
           IF  DOC-LANGUE NOT = "F"
           AND DOC-LANGUE NOT = "D"
           AND DOC-LANGUE NOT = "E"
               MOVE "F" TO DOC-LANGUE
               MOVE 1 TO INPUT-ERROR.

       APRES-7.
           IF DOC-A < REG-ANCIEN-A
              MOVE 1 TO INPUT-ERROR
              MOVE REG-ANCIEN-A TO DOC-A
           END-IF.
           IF DOC-A < 2000
              MOVE 1 TO INPUT-ERROR
              MOVE 20000101 TO DOC-DATE
           END-IF.

       APRES-TXT.
           EVALUATE EXC-KEY 
              WHEN 5 THRU 6 PERFORM APRES-DEC
           END-EVALUATE.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 MOVE 99 TO SAVE-KEY
                  PERFORM READ-WRITE
                  MOVE 4 TO DECISION
           WHEN 8 MOVE 98 TO SAVE-KEY
                  PERFORM READ-WRITE
                  MOVE 3 TO DECISION
                  PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF LNK-COMPETENCE < REG-COMPETENCE
                    GO NEXT-REGIS
                 END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-03.
           MOVE DOC-SUITE TO HE-Z3.
           DISPLAY HE-Z3  LINE 5 POSITION 27.
       DIS-HE-04.
           DISPLAY DOC-FORM   LINE 6 POSITION 27.
       DIS-HE-05.
           DISPLAY DOC-TITRE  LINE 7 POSITION 27.
       DIS-HE-06.
           DISPLAY DOC-LANGUE LINE 8 POSITION 27.
       DIS-HE-07.
           MOVE DOC-A TO HE-AA.
           MOVE DOC-M TO HE-MM.
           MOVE DOC-J TO HE-JJ.
           DISPLAY HE-DATE LINE  9 POSITION 27.
       DIS-HE-08.
           DISPLAY DOC-AUTEUR LINE 10 POSITION 27.
       DIS-HE-09.
           PERFORM DIS-TEXT VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > 10.
       DIS-HE-END.
           EXIT.

       DIS-TEXT.
           COMPUTE LIN-IDX = IDX-4 + 11.
           DISPLAY DOC-T(IDX-4) LINE LIN-IDX POSITION 10.

       READ-WRITE.
           CALL "6-DOC" USING LINK-V REG-RECORD DOC-RECORD SAVE-KEY.

       AFFICHAGE-ECRAN.
           MOVE 1223 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CANCEL "6-DOC".
           CANCEL "RMG-DOC".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


       READ-FORM.
           INITIALIZE FORMULAIRE LINE-NUMBER LIN-IDX SAVE-KEY
           CAR-RECORD POSE-RECORD HJS-RECORD FICHE-RECORD CON-RECORD.
           MOVE LNK-ANNEE TO SAVE-ANNEE.
           MOVE LNK-MOIS  TO SAVE-MOIS.
           MOVE DOC-A TO LNK-ANNEE.
           MOVE DOC-M TO LNK-MOIS.
           CALL "0-JRSEM"  USING LINK-V.
           CALL "1-LINDEX" USING LINK-V.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE 65 TO SAVE-KEY.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD SAVE-KEY.

           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL" USING LINK-V CCOL-RECORD.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD SAVE-KEY.
           CALL "6-GHJS"  USING LINK-V HJS-RECORD  CAR-RECORD PRESENCES.
           MOVE CAR-EQUIPE TO POSE-CODE.
           CALL "6-POSE" USING LINK-V POSE-RECORD FAKE-KEY.
           CALL "4-SALBAS" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 POSE-RECORD 
                                 HJS-RECORD 
                                 BASES-REMUNERATION.

           MOVE DOC-FORM TO HELP-NAME.
           OPEN INPUT HELP-FILE.
           PERFORM LECT-HELP UNTIL LINE-NUMBER > 240.
           CLOSE HELP-FILE.
           
           CALL "4-DOCFIL" USING LINK-V
                                 PR-RECORD
                                 REG-RECORD
                                 CAR-RECORD
                                 CON-RECORD
                                 CCOL-RECORD
                                 FICHE-RECORD
                                 MET-RECORD
                                 BASES-REMUNERATION
                                 DOC-RECORD
                                 FORMULAIRE.

           MOVE SAVE-ANNEE TO LNK-ANNEE.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           CALL "0-JRSEM"  USING LINK-V.
           CALL "1-LINDEX" USING LINK-V.

       LECT-HELP.
           INITIALIZE REC-HELP.
           ADD 1 TO LINE-NUMBER.
           READ HELP-FILE AT END MOVE 241 TO LINE-NUMBER.
           IF LINE-NUMBER < 241
              PERFORM FILL-FORM
           END-IF.

       FILL-FORM.
           MOVE 0 TO IDX-2 IDX-3.
           INSPECT REC-HELP TALLYING IDX-3 FOR ALL "<FIN".
           INSPECT REC-HELP TALLYING IDX-2 FOR ALL "<PAGE".
           PERFORM FILL-FILES.

       FILL-FILES.
           IF IDX-2 NOT = 0 
           AND IDX-2 < 80
              DIVIDE LINE-NUMBER BY 60 GIVING IDX-1 REMAINDER IDX-4
              IF IDX-4 NOT = 0 ADD 1 TO IDX-1 END-IF
              COMPUTE LINE-NUMBER = IDX-1 * 60 + 1
           END-IF.
           MOVE REC-HELP TO FORM-LINE(LINE-NUMBER).
           IF IDX-3 NOT = 0 
              MOVE 241 TO LINE-NUMBER.


       PRINT-JOB.
           INITIALIZE COUNTER IDX-1 IDX-2.
           MOVE 2 TO IDX-4.
           PERFORM LECT-WRITE VARYING LINE-NUMBER FROM 240 BY -1 UNTIL
           LINE-NUMBER <= IDX-4.
           IF IDX-4 > 2
              DIVIDE IDX-4 BY 60 GIVING IDX-1 REMAINDER IDX-2
              IF IDX-2 > 0
                 ADD 1 TO IDX-1
              END-IF
           END-IF.
           move 1 to idx-1.
           
           PERFORM TRANSMET VARYING IDX-2 FROM 1 BY 1 UNTIL IDX-2
           > IDX-1.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
      *       CALL "RMG-DOC" USING LINK-V FORM-PAGE(1)
              MOVE 0 TO LNK-VAL
              CANCEL "RMG-DOC"
           END-IF.
              

       LECT-WRITE.
           IF FORM-LINE(LINE-NUMBER) NOT = SPACES
              MOVE LINE-NUMBER TO IDX-4.

       TRANSMET.
           MOVE 55 TO LNK-LINE.
           move 1 to idx-2.
      *    CALL "RMG-DOC" USING LINK-V FORM-PAGE(IDX-2).
           perform D-L varying line-number From 1 by 1 
           until line-number > 20.
           PERFORM AA.
           ADD 1 TO COUNTER.


       d-l.
           display FORM-LINE(LINE-NUMBER) LINE LINE-NUMBER POSITION 1.
