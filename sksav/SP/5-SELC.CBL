      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 5-SEL SELECTION PRECISIONS                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    5-SELC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "OCCOM.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  COMPTEUR              PIC 99 VALUE 0.

       01  HE-SEL.
           02 H-S PIC 99       OCCURS 20.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       START-DISPLAY SECTION.
             
       START-5-SELC.

           MOVE PARMOD-SETTINGS TO HE-SEL.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.
           MOVE 1 TO IDX-1.
           PERFORM AVANT-ALL THRU AVANT-ALL-END.
           PERFORM END-PROGRAM.
           

       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).
           MOVE 0067000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IDX-1 + 3.
           ACCEPT H-S(IDX-1)
             LINE LIN-IDX POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
           OR EXC-KEY = 67 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.
           PERFORM APRES-YN.
           PERFORM AFFICHAGE-LIGNE.
           IF INPUT-ERROR = 0
           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM IDX-1
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 13 ADD 1 TO IDX-1
                WHEN 53 ADD 1 TO IDX-1
           END-EVALUATE.
           IF IDX-1 = 0
              MOVE 1 TO IDX-1
           END-IF.
           IF IDX-1 > 20
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.
       AVANT-ALL-END.
           EXIT.

       APRES-YN.
           IF H-S(IDX-1) < 31
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 0 TO H-S(IDX-1)
              MOVE 1 TO INPUT-ERROR.


       AFFICHAGE-DETAIL.
           PERFORM AFFICHAGE-LIGNE
                   VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 10.

       AFFICHAGE-LIGNE.
           COMPUTE LIN-IDX = IDX-1 + 3.
           DISPLAY H-S(IDX-1) LINE LIN-IDX POSITION 35.
           MOVE H-S(IDX-1) TO OCO-NUMBER.
           CALL "6-OCCOM" USING LINK-V OCO-RECORD FAKE-KEY.
           DISPLAY OCO-NOM LINE LIN-IDX POSITION 40 SIZE 25.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.


       AFFICHAGE-ECRAN.
           MOVE 1410 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           PERFORM AFFICHAGE-DETAIL.


       END-PROGRAM.
           MOVE HE-SEL TO PARMOD-SETTINGS.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

