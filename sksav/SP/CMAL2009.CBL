      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME CMAL2009 IMPORTATION                        �
      *  � CODES SALAIRES MALADIE PAIE 2009                      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    CMAL2009.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CODPAR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴
           COPY "CODPAR.FDE".


       WORKING-STORAGE SECTION.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".


       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CODPAR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.

       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-CMAL2009.



           MOVE 0 TO LNK-VAL.
           DISPLAY "ADAPTATION CODES SALAIRE ! " LINE 24 POSITION 10.
           PERFORM COPIES VARYING IDX FROM 411 BY 1 UNTIL IDX > 419.
           PERFORM CP.
           EXIT PROGRAM.

       COPIES.
           MOVE IDX TO LNK-NUM.
           CALL "4-CSIMP" USING LINK-V.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".

        CP.
           OPEN I-O   CODPAR.
           INITIALIZE CP-RECORD.
           START CODPAR KEY >= CP-KEY INVALID GO CP-END.
           PERFORM CODPAR THRU CP-END.

       CODPAR.
           READ CODPAR NEXT NO LOCK AT END GO CP-END.
           PERFORM WRITE-CODPAR.
           GO CODPAR.
       CP-END.
           EXIT PROGRAM.

       WRITE-CODPAR.
           MOVE 411 TO COD-PAR(1, 2) 
           MOVE 412 TO COD-PAR(2, 2) 
           MOVE 413 TO COD-PAR(3, 2) 
           MOVE 414 TO COD-PAR(4, 2) 
           MOVE 415 TO COD-PAR(5, 2) 
           MOVE 416 TO COD-PAR(6, 2) 
           MOVE 417 TO COD-PAR(7, 2) 
           MOVE 418 TO COD-PAR(8, 2) 
           MOVE 419 TO COD-PAR(9, 2) 
           WRITE CP-RECORD INVALID REWRITE CP-RECORD.
