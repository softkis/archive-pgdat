      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-CSTXT GESTION DES TEXTES CODSAL           �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-CSTXT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CSDEF.FC".
           COPY "CODTXT.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CSDEF.FDE".
           COPY "CODTXT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01  CHOIX-MAX             PIC 99 VALUE 13.

       01  HE-INPUT-PARAMETER.
      *    LINE COL SIZE HE-SCREEN    LLCCSZECRA
           02 IP-101 PIC 9(10)  VALUE 0425080014.
           02 IP-102 PIC 9(10)  VALUE 0525011000.
           02 IP-103 PIC 9(10)  VALUE 0625080001.
           02 IP-104 PIC 9(10)  VALUE 0825080029.
           02 IP-105 PIC 9(10)  VALUE 0925080029.
           02 IP-106 PIC 9(10)  VALUE 1025080029.
           02 IP-DEC PIC 9(10)  VALUE 2335020099.

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
            03 I-PP OCCURS 7.
               04 IP-LINE  PIC 99.
               04 IP-COL   PIC 99.
               04 IP-SIZE  PIC 99.
               04 IP-ECRAN PIC 9999.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  LANGUE                PIC X.
       01   ECR-DISPLAY.
            02 HE-Z2 PIC ZZ.
            02 HE-Z4 PIC Z(4).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       SCREEN SECTION.

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CSDEF CODTEXT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-AA-COD.
       
           MOVE LNK-LANGUAGE TO LANGUE SAVE-LANGUAGE.
           OPEN INPUT CSDEF.
           OPEN I-O CODTEXT.

           INITIALIZE CD-RECORD CTX-RECORD.
           PERFORM AFFICHAGE-ECRAN .
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

           EVALUATE INDICE-ZONE
           WHEN 1 MOVE 0023002200 TO EXC-KFR (1)
                  MOVE 0000000065 TO EXC-KFR(13)
                  MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5 THRU 12 MOVE 0000000005 TO EXC-KFR (1)
           WHEN CHOIX-MAX MOVE 0000000005 TO EXC-KFR (1)
                          MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.
           EVALUATE INDICE-ZONE
           WHEN 1 PERFORM AVANT-1
           WHEN 2 PERFORM AVANT-2
           WHEN 3 PERFORM AVANT-DESCR
           WHEN 4 PERFORM AVANT-COMM 
           WHEN CHOIX-MAX PERFORM AVANT-DEC 
           WHEN OTHER     PERFORM AVANT-TEXTE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2
           WHEN 5 THRU 12 PERFORM APRES-TEXTE
           WHEN CHOIX-MAX PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 52 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.
           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT CD-NUMBER
             LINE 4 POSITION 20 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-2.
           ACCEPT LANGUE
             LINE  5 POSITION 20 SIZE 1 
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-DESCR.
           ACCEPT CTX-NOM
             LINE 7 POSITION COL-IDX SIZE 25
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-TEXTE.
           COMPUTE IDX = INDICE-ZONE - 4.
           COMPUTE LIN-IDX = IDX + 12.
           ACCEPT CTX-DESCRIPTION(IDX)
             LINE LIN-IDX POSITION COL-IDX SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-COMM.
           COMPUTE LIN-IDX = IDX-4 + 7.
           ACCEPT CTX-COMMENTAIRE
             LINE LIN-IDX POSITION 20 SIZE 60
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY 
             WHEN 4 PERFORM COPY-CODTXT
             WHEN 2 CALL "2-CSDEF" USING LINK-V CD-RECORD
                    CANCEL "2-CSDEF"
                    PERFORM AFFICHAGE-ECRAN 
             WHEN OTHER PERFORM NEXT-CSDEF.
           PERFORM AFFICHAGE-DETAIL.
       
       APRES-2.
           EVALUATE LANGUE 
                WHEN "F"   MOVE 0 TO INPUT-ERROR 
                           MOVE 1 TO IDX-4
                WHEN "D"   MOVE 0 TO INPUT-ERROR 
                           MOVE 2 TO IDX-4
                WHEN "E"   MOVE 0 TO INPUT-ERROR 
                           MOVE 3 TO IDX-4
                WHEN OTHER MOVE 1 TO INPUT-ERROR 
                           MOVE LNK-LANGUAGE TO LANGUE.
           IF INPUT-ERROR = 0
              COMPUTE COL-IDX = IDX-4 * 25 - 20
              PERFORM DIS-LANGUE.
       

       APRES-TEXTE.
           EVALUATE EXC-KEY 
            WHEN 5 PERFORM APRES-DEC.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN 5 CALL "6-CSTXT" USING LINK-V CTX-RECORD WR-KEY
                   MOVE 1 TO DECISION
                   PERFORM AFFICHAGE-DETAIL
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       COPY-CODTXT.  
           MOVE CD-NUMBER TO IDX-2.
           ACCEPT CD-NUMBER
             LINE  4 POSITION 60 SIZE 4
             TAB UPDATE NO BEEP CURSOR 4
             ON EXCEPTION EXC-KEY CONTINUE.
           PERFORM COPIES VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > 3.
           MOVE IDX-2 TO CD-NUMBER.

       COPIES.
           EVALUATE IDX-4 
              WHEN 1 MOVE "F" TO CTX-LANGUE 
              WHEN 2 MOVE "D" TO CTX-LANGUE 
              WHEN 3 MOVE "E" TO CTX-LANGUE 
           END-EVALUATE.
           MOVE CD-NUMBER TO CTX-NUMBER.
           CALL "6-CSTXT" USING LINK-V CTX-RECORD NUL-KEY.
           MOVE IDX-2 TO CTX-NUMBER.
           CALL "6-CSTXT" USING LINK-V CTX-RECORD WR-KEY.

       NEXT-CSDEF.
           CALL "6-CSDEF" USING LINK-V CD-RECORD EXC-KEY.

       AFFICHAGE-ECRAN.
           MOVE 32 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           DISPLAY "A" LINE 13 POSITION 1 LOW.
           DISPLAY "B" LINE 14 POSITION 1 LOW.
           DISPLAY "C" LINE 15 POSITION 1 LOW.
           DISPLAY "D" LINE 16 POSITION 1 LOW.
           DISPLAY "E" LINE 17 POSITION 1 LOW.
           DISPLAY "F" LINE 18 POSITION 1 LOW.
           DISPLAY "G" LINE 19 POSITION 1 LOW.
           DISPLAY "H" LINE 20 POSITION 1 LOW.
           MOVE "EE" TO LNK-AREA.
           MOVE 00042500 TO LNK-POSITION.
           PERFORM LIGNE VARYING IDX FROM 1 BY 1 UNTIL IDX > 7.

       LIGNE.
           MOVE IDX TO HE-Z2.
           IF IDX < CHOIX-MAX
              DISPLAY HE-Z2 LINE IP-LINE(IDX) POSITION 1 LOW.
           MOVE IP-ECRAN(IDX) TO LNK-NUM.
           MOVE IP-LINE(IDX)  TO LNK-LINE.
           MOVE 4             TO LNK-COL.
           MOVE "L" TO LNK-LOW.
           CALL "0-DMESS" USING LINK-V.

       AFFICHAGE-DETAIL.
           MOVE CD-NUMBER TO HE-Z4.
           DISPLAY HE-Z4 LINE 4 COL 20.
           DISPLAY CD-DEFINITION LINE 4 POSITION 25 SIZE 55.
           PERFORM DIS-LANGUE VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > 3.

       DIS-LANGUE.
           COMPUTE COL-IDX = IDX-4 * 25 - 20.
           EVALUATE IDX-4 
              WHEN 1 MOVE "F" TO LNK-LANGUAGE 
              WHEN 2 MOVE "D" TO LNK-LANGUAGE 
              WHEN 3 MOVE "E" TO LNK-LANGUAGE 
           END-EVALUATE.
           MOVE CD-NUMBER TO CTX-NUMBER.
           CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           PERFORM DIS-TEXT VARYING IDX FROM 1 BY 1 UNTIL IDX > 8.
           PERFORM DIS-COM.
           PERFORM DIS-NOM.

       DIS-TEXT.
           COMPUTE LIN-IDX = IDX + 12.
           DISPLAY CTX-DESCRIPTION(IDX) LINE LIN-IDX POSITION COL-IDX.

       DIS-COM.
           COMPUTE LIN-IDX = IDX-4 + 7.
           DISPLAY CTX-COMMENTAIRE LINE LIN-IDX POSITION 20.

       DIS-NOM.
           COMPUTE COL-IDX = IDX-4 * 25 - 20.
           DISPLAY CTX-NOM LINE 7 POSITION COL-IDX SIZE 25.

       END-PROGRAM.
           CLOSE CSDEF.
           CLOSE CODTEXT.
           MOVE SAVE-LANGUAGE TO LNK-LANGUAGE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XACTION.CPY".
           
           
       