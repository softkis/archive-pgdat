      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-PCL AFFICHAGE PERSONNES REFERENCE POSTE   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-PCL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.


       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "POCL.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "PCREF.REC".
           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  CHOIX                 PIC X(10) VALUE SPACES.
       01  COMPTEUR              PIC 99.
       01  ACTION-CALL           PIC 9 VALUE 0.
       
       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4) BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HE-Z8 PIC Z(8) BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-PCL .

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 .

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 .

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 09 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT PC-NUMBER
             LINE  3 POSITION 22 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           
      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3 
                   IF EXC-KEY = 2
                      MOVE "A" TO A-N LNK-A-N
                   ELSE
                      MOVE "N" TO A-N LNK-A-N
                   END-IF
                   CALL "2-POCL" USING LINK-V PC-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER MOVE "N" TO A-N 
                      PERFORM NEXT-POCL
           END-EVALUATE.
      *    IF PC-NUMBER NOT = 0
               PERFORM AFFICHAGE-DETAIL
               PERFORM AFFICHE-DEBUT THRU AFFICHE-END.
           IF EXC-KEY = 13 MOVE 52 TO EXC-KEY.

       NEXT-POCL.
           CALL "6-POCL" USING LINK-V PC-RECORD A-N EXC-KEY.

       DIS-HE-01.
           MOVE PC-NUMBER TO HE-Z8.
           DISPLAY HE-Z8 LINE  3 POSITION 22.
           DISPLAY PC-NOM LINE 3 POSITION 30 .
       DIS-HE-END.
           EXIT.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE NOT-FOUND COMPTEUR CHOIX.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO NOT-FOUND SAVE-KEY.
           INITIALIZE PCR-RECORD.
           PERFORM READ-POCL THRU READ-POCL-END.
       AFFICHE-END.
           EXIT.

       READ-POCL.
           CALL "6-PCREF" USING LINK-V PC-RECORD REG-RECORD
                      PCR-RECORD NX-KEY.
           IF PCR-PERSON = 0
              GO READ-POCL-END.
           MOVE PCR-PERSON TO REG-PERSON.
           PERFORM PRESENCE.
           IF PRES-TOT(LNK-MOIS) = 0
              GO READ-POCL.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              MOVE 4 TO LIN-IDX
           END-IF.
           IF EXC-KEY = 52 GO READ-POCL-END.
           GO READ-POCL.
       READ-POCL-END.
           PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX.
           ADD 1 TO COMPTEUR.
           MOVE PCR-PERSON TO REG-PERSON HE-Z6.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.
           DISPLAY HE-Z6 LINE LIN-IDX POSITION 2 LOW.
           DISPLAY PR-NOM LINE LIN-IDX POSITION 10.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 12 TO COL-IDX.
           DISPLAY PR-PRENOM LINE LIN-IDX POSITION COL-IDX LOW.
           DISPLAY PCR-ANNEE LINE LIN-IDX POSITION 70.
           DISPLAY PCR-MOIS  LINE LIN-IDX POSITION 76.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052000000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.

           ACCEPT CHOIX 
           LINE 23 POSITION 30 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 66 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.

       INTERRUPT-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 2212 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CANCEL "2-POCL".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

