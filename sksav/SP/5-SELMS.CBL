      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 5-SELMS SELECTION MESSAGES                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    5-SELMS.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "MESSAGE.REC".
           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 20. 
       01  SAVE-AREA             PIC XXXX.

       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-Z2 PIC ZZ BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6). 
           02 HE-Z8 PIC Z(8). 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-SMS.

           MOVE LNK-AREA TO SAVE-AREA.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1 THRU 20
                      MOVE 0017000005 TO EXC-KFR(1)
           END-EVALUATE.


       TRAITEMENT-ECRAN-01.

           PERFORM DISPLAY-F-KEYS.

           EVALUATE INDICE-ZONE
           WHEN  1 THRU 20 PERFORM AVANT-MESS
           END-EVALUATE.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 1 
              MOVE 1 TO LNK-VAL
              CALL "0-BOOK" USING LINK-V
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              GO TRAITEMENT-ECRAN
           END-IF.

           EVALUATE INDICE-ZONE
           WHEN  1 THRU 20 PERFORM APRES-MESS.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-MESS.
           COMPUTE IDX = INDICE-ZONE .
           COMPUTE LIN-IDX = IDX + 2.
           ACCEPT PARMOD-SELECT(IDX)
           LINE  LIN-IDX POSITION  6 SIZE 6
           TAB UPDATE NO BEEP CURSOR  1 
           ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-MESS.
           COMPUTE IDX = INDICE-ZONE.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 
                MOVE EXC-KEY TO SAVE-KEY
                MOVE PARMOD-SELECT(IDX) TO MS-NUMBER 
                PERFORM NEXT-MESS
                MOVE MS-NUMBER TO PARMOD-SELECT(IDX)
           WHEN   5 PERFORM END-PROGRAM
           WHEN   2 CALL "2-MESS" USING LINK-V MS-RECORD
                    IF MS-NUMBER > 0
                       MOVE MS-NUMBER TO PARMOD-SELECT(IDX) 
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.
           COMPUTE IDX = INDICE-ZONE.
           PERFORM DIS-MESS.
           PERFORM DOUBLON VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 20.
           IF PARMOD-SELECT(IDX) = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF  PARMOD-SELECT(IDX) > 0
           AND MS-NUMBER = 0
               MOVE 1 TO INPUT-ERROR.

       DOUBLON.
           IF IDX-1 NOT = IDX
              IF PARMOD-SELECT(IDX) = PARMOD-SELECT(IDX-1)  
                  MOVE 0 TO PARMOD-SELECT(IDX-1)
                  COMPUTE IDX-4 = IDX-1 + 2
                  DISPLAY SPACES LINE IDX-4 POSITION 1 SIZE 80
              END-IF
           END-IF.


       AJAX.
           IF PARMOD-SELECT(IDX-1) = 0
              MOVE 0 TO IDX 
           END-IF.
           IF IDX = 0
              MOVE 0 TO PARMOD-SELECT(IDX-1)
           END-IF.


       NEXT-MESS.
           CALL "6-MESS" USING LINK-V MS-RECORD SAVE-KEY.
           
       DIS-HE-MESS.
           PERFORM DIS-MESS VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.
       DIS-HE-END.
           EXIT.

       DIS-MESS.
           COMPUTE LIN-IDX = IDX + 2.
           MOVE PARMOD-SELECT(IDX) TO HE-Z6.
           DISPLAY HE-Z6 LINE  LIN-IDX POSITION 6.
           IF PARMOD-SELECT(IDX) NOT = 0
              MOVE PARMOD-SELECT(IDX) TO MS-NUMBER
              CALL "6-MESS" USING LINK-V MS-RECORD FAKE-KEY
           ELSE
              INITIALIZE MS-RECORD
           END-IF.
           DISPLAY MS-DESCRIPTION LINE LIN-IDX POSITION 16 SIZE 20.


       AFFICHAGE-ECRAN.
           MOVE 1410 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE SAVE-AREA TO LNK-AREA.
           DISPLAY LNK-AREA LINE 3 POSITION 75.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-MESS.

       END-PROGRAM.
           MOVE 1 TO IDX.
           PERFORM AJAX VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 20.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
