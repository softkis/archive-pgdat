      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-BARDEF DEFINITION BAREMES DE REMUNERATION �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-BARDEF.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "BARDEF.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "BARDEF.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 8. 

       01   ECR-DISPLAY.
            02 HE-01 PIC Z(4).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON BARDEF.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-1-BARDEF.
       
           OPEN I-O BARDEF.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0029000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 8     MOVE 0100000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-DESCR
           WHEN  3 PERFORM AVANT-3
           WHEN  4 PERFORM AVANT-4
           WHEN  5 PERFORM AVANT-5
           WHEN  6 PERFORM AVANT-6
           WHEN  7 PERFORM AVANT-7
           WHEN  8 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN  9 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  8 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT BDF-CODE
             LINE  4 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-DESCR.        
           ACCEPT BDF-NOM
             LINE  5 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3.
           ACCEPT BDF-POINTS
             LINE   6 POSITION 30 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
       AVANT-4.
           ACCEPT BDF-DIGIT
             LINE   7 POSITION 30 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-5.
           ACCEPT BDF-DECIM
             LINE   8 POSITION 30 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-6.
           ACCEPT BDF-ARRONDI
             LINE   9 POSITION 30 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           ACCEPT BDF-ETAPES
             LINE  10 POSITION 30 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN   2 CALL "2-BARDEF" USING LINK-V BDF-RECORD
                    CANCEL "2-BARDEF"
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-BDF
           END-EVALUATE.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
           
       APRES-3.
           IF BDF-POINTS > 10
              MOVE 10 TO BDF-POINTS
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-4.
           IF BDF-DIGIT < 1
              MOVE 1 TO BDF-DIGIT
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF BDF-DIGIT > 6
              MOVE 6 TO BDF-DIGIT
              MOVE 1 TO INPUT-ERROR
           END-IF.


       APRES-5.
           IF BDF-DECIM > 5
              MOVE 5 TO BDF-DECIM
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-6.
           IF BDF-ARRONDI > 5
              MOVE 5 TO BDF-ARRONDI
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 COMPUTE BDF-SIZE = BDF-DIGIT + BDF-DECIM
                    IF BDF-DECIM > 0
                       ADD 1 TO BDF-SIZE
                    END-IF
                    CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO BDF-TIME
                    MOVE LNK-USER TO BDF-USER
                     WRITE BDF-RECORD INVALID 
                        REWRITE BDF-RECORD
                    END-WRITE   
                    MOVE 1 TO DECISION
                    PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN  8 DELETE BARDEF INVALID CONTINUE
                    END-DELETE
                    INITIALIZE BDF-RECORD
                    MOVE 1 TO DECISION
                    PERFORM DIS-HE-01 THRU DIS-HE-END
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
               MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-BDF.
           CALL "6-BARDEF" USING LINK-V BDF-RECORD SAVE-KEY.

       DIS-HE-01.
           DISPLAY BDF-KEY  LINE  4 POSITION 30.
           IF EXC-KEY NOT = 5 MOVE BDF-CODE TO LNK-TEXT.
       DIS-HE-02.
           DISPLAY BDF-NOM  LINE  5 POSITION 30.
       DIS-HE-03.
           DISPLAY BDF-POINTS LINE 6 POSITION 29.
       DIS-HE-04.
           DISPLAY BDF-DIGIT LINE  7 POSITION 30.
       DIS-HE-05.
           DISPLAY BDF-DECIM LINE  8 POSITION 30.
       DIS-HE-06.
           DISPLAY BDF-ARRONDI LINE 9 POSITION 30.
       DIS-HE-07.
           DISPLAY BDF-ETAPES  LINE 10 POSITION 30.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE  40 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE BARDEF.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
