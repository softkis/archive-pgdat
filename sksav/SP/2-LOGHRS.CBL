      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-LOGHRS INTERROGATION HEURES PERSONNE MOIS �
      *  � FICHIER LOG                                           �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-LOGHRS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "LHEURES.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "LHEURES.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "POCL.REC".
           COPY "OCCOM.REC".
           COPY "OCCUP.REC".
           COPY "OCCTXT.REC".

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  PRECISION             PIC 9 VALUE 0.
       01  HELP-VAL              PIC ZZ BLANK WHEN ZERO.
       01  HELP-1                PIC 9999.
       01  HELP-2                PIC 9.
       01  CHOIX                 PIC X.
       01  CUMUL                 PIC 9999V99.
       
       01  LHRS-NAME.
           02 HEURES-ID           PIC X(9) VALUE "L-HEURES.".
           02 ANNEE-HEURES        PIC 999.
  
       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ.
           02 HE-Z3Z2 PIC Z(3),ZZ.
           02 HE-TEMP PIC ZZ.ZZ BLANK WHEN ZERO.
           02 HE-Z4Z2 PIC Z(4),ZZ.
           02 HE-Z2Z2 PIC ZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON LHEURES.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-LOGHRS.
       


           MOVE LNK-SUFFIX TO ANNEE-HEURES.
           OPEN INPUT LHEURES.
           PERFORM AFFICHAGE-ECRAN .
           MOVE 1 TO INDICE-ZONE.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640000 TO EXC-KFR (1)
                      MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
           WHEN 2     MOVE 0000000065 TO EXC-KFR (13)
                      MOVE 6600000000 TO EXC-KFR (14)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF EXC-KEY > 66 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              MOVE CHOIX-MAX TO INDICE-ZONE
              GO TRAITEMENT-ECRAN
           END-IF.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 .

           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           MOVE FR-KEY TO REG-FIRME.
           IF LNK-PERSON NOT = 0 
              MOVE LNK-PERSON TO REG-PERSON 
              MOVE 27 TO EXC-KEY
              MOVE 0 TO LNK-PERSON
           ELSE    
           ACCEPT REG-PERSON 
             LINE  3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 PERFORM CHANGE-MOIS
                             MOVE 99 TO EXC-KEY
           END-EVALUATE.

       AVANT-2.            
           ACCEPT REG-MATCHCODE
             LINE  3 POSITION 33 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-REGIS
           END-EVALUATE.                     
           PERFORM DIS-HE-01.
           PERFORM PRESENCE.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
                   PERFORM DIS-HE-01
                   PERFORM AFFICHAGE-DETAIL
           END-EVALUATE.                     
           
        NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           PERFORM DISPLAY-MESSAGE.

       DIS-HE-01.
           MOVE REG-PERSON TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
           DISPLAY REG-MATCHCODE LINE  3 POSITION 33.


       AFFICHAGE-ECRAN.
           MOVE 2310 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY SPACES LINE 5 POSITION 1 SIZE 80.
           DISPLAY SPACES LINE 6 POSITION 1 SIZE 80.

       AFFICHAGE-DETAIL.
           PERFORM AFFICHAGE-HEURES.

       AFFICHAGE-HEURES.
           MOVE 4 TO LIN-IDX.
           INITIALIZE LHRS-RECORD NOT-FOUND IDX CUMUL.
           MOVE FR-KEY     TO LHRS-FIRME.
           MOVE LNK-MOIS   TO LHRS-MOIS.
           MOVE REG-PERSON TO LHRS-PERSON.
           START LHEURES KEY >= LHRS-KEY 
               INVALID MOVE 4 TO LIN-IDX
               PERFORM READ-HRS-END
               NOT INVALID PERFORM READ-HEURES THRU READ-HRS-END.

       READ-HEURES.
           READ LHEURES NEXT NO LOCK AT END
               GO READ-HRS-END
           END-READ.
           IF FR-KEY     NOT = LHRS-FIRME
           OR REG-PERSON NOT = LHRS-PERSON
           OR LNK-MOIS   NOT = LHRS-MOIS 
              GO READ-HRS-END
           END-IF.
           ADD 1 TO IDX.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           IF LHRS-POSTE NOT = 0
              MOVE LHRS-POSTE TO PC-NUMBER HE-Z8
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              MOVE LHRS-POSTE TO PC-NUMBER 
              DISPLAY PC-NOM LINE LIN-IDX POSITION 12 LOW
              DISPLAY HE-Z8 LINE LIN-IDX POSITION 2
              DISPLAY LHRS-EXTENSION LINE LIN-IDX POSITION 50 SIZE 8 LOW
           ELSE
              IF LHRS-OCCUP NOT = 0
                 MOVE LHRS-OCCUP TO OCC-KEY HE-Z8
                 CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY
                 MOVE OCC-KEY TO OT-NUMBER
                 CALL "6-OCCTXT" USING LINK-V OT-RECORD FAKE-KEY
                 DISPLAY OT-NOM LINE LIN-IDX POSITION 12 LOW
                 DISPLAY HE-Z8 LINE LIN-IDX POSITION 2
              END-IF
           END-IF.
           DISPLAY LHRS-JOUR  LINE LIN-IDX POSITION 01 REVERSE.
           DISPLAY LHRS-BATCH LINE LIN-IDX POSITION 03 LOW.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 52 
                 GO READ-HRS-END
              END-IF
              MOVE 4 TO LIN-IDX
           END-IF.
           GO READ-HEURES.
       READ-HRS-END.
           ADD 1 TO LIN-IDX.
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 23.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052000000 TO EXC-KFR (11).
           MOVE 0067000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.

           ACCEPT CHOIX 
           LINE 23 POSITION 30 SIZE 1
           TAB UPDATE NO BEEP CURSOR 1
           ON EXCEPTION EXC-KEY CONTINUE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 52 
              PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM 5
              BY 1 UNTIL LIN-IDX > 23
              GO INTERRUPT-END.

       INTERRUPT-END.
           EXIT.

       END-PROGRAM.
           CLOSE LHEURES.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS THRU SUBTRACT-END
             WHEN 58 PERFORM ADD-MOIS THRU ADD-END
           END-EVALUATE.

           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-DETAIL.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0 
           OR REG-PERSON = 0
              GO SUBTRACT-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO SUBTRACT-END.
           GO SUBTRACT-MOIS.
       SUBTRACT-END.
           IF LNK-MOIS = 0
              IF REG-PERSON = 0
                 MOVE 1 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.

       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
           OR REG-PERSON = 0
              GO ADD-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO ADD-END.
           GO ADD-MOIS.
       ADD-END.
           IF LNK-MOIS > 12
              IF REG-PERSON = 0
                 MOVE 12 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.

       DIS-DET-LIGNE.
           MOVE SEM-IDX(LNK-MOIS, LHRS-JOUR) TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "SW" TO LNK-AREA.
           MOVE 01010300 TO LNK-POSITION.
           MOVE LIN-IDX TO LNK-LINE.
           CALL "0-DMESS" USING LINK-V.

           MOVE LHRS-DEBUT TO HE-TEMP.
           DISPLAY HE-TEMP LINE LIN-IDX POSITION 05.
           MOVE LHRS-FIN TO HE-TEMP.
           DISPLAY HE-TEMP LINE LIN-IDX POSITION 12.
           DISPLAY LHRS-ST-JOUR  LINE LIN-IDX POSITION 20.
           DISPLAY LHRS-ST-MOIS  LINE LIN-IDX POSITION 24
           DISPLAY LHRS-ST-ANNEE LINE LIN-IDX POSITION 27.
           DISPLAY LHRS-ST-HEURE LINE LIN-IDX POSITION 33 LOW.
           DISPLAY LHRS-ST-MIN   LINE LIN-IDX POSITION 36 LOW.
           DISPLAY LHRS-USER     LINE LIN-IDX POSITION 40.
           IF LHRS-FIN = 0
              IF LHRS-DATE  NOT = LHRS-MJ
              OR LHRS-DEBUT NOT = LHRS-HM
                 DISPLAY LHRS-USER LINE LIN-IDX POSITION 40 REVERSE.
           IF LHRS-FIN > 0
              IF LHRS-DATE  NOT = LHRS-MJ
              OR LHRS-FIN   NOT = LHRS-HM
                 DISPLAY LHRS-USER LINE LIN-IDX POSITION 40 REVERSE.

       XX.
           MOVE LHRS-TOTAL-BRUT TO HE-Z2Z2.
           INSPECT HE-Z2Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z2Z2  LINE LIN-IDX POSITION 19.
           MOVE LHRS-REPOS TO HE-TEMP.
           DISPLAY HE-TEMP  LINE LIN-IDX POSITION 26.
           MOVE LHRS-TOTAL-NET TO HE-Z2Z2.
           INSPECT HE-Z2Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z2Z2  LINE LIN-IDX POSITION 33.
           ADD  LHRS-TOTAL-NET TO CUMUL.
           MOVE CUMUL TO HE-Z4Z2.
           INSPECT HE-Z4Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z4Z2  LINE LIN-IDX POSITION 40.
           MOVE LHRS-TARIF-HRS(1) TO HE-Z2Z2.
           INSPECT HE-Z2Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z2Z2  LINE LIN-IDX POSITION 48.
           MOVE LHRS-TARIF-JRS(10) TO HE-Z2Z2.
           INSPECT HE-Z2Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z2Z2  LINE LIN-IDX POSITION 56.
           MOVE LHRS-TARIF-JRS(1) TO HE-Z2Z2.
           INSPECT HE-Z2Z2 REPLACING ALL ",00" BY "   ".
           DISPLAY HE-Z2Z2  LINE LIN-IDX POSITION 64.


           