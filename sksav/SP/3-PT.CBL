      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-PT IMPRESSION PLAN DE TRAVAIL PERSONNEL   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-PT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PLANS.FC".
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "PLANS.FDE".
           COPY "FORM80.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80       ".
       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  PRECISION             PIC 9 VALUE 0.
       01  TIRET-TEXTE           PIC X VALUE "-".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".
           COPY "PRESENCE.REC".
           COPY "EQUIPE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "PLAGE.REC".
           COPY "OCCUP.REC".
           COPY "OCCTXT.REC".
           COPY "CONTRAT.REC".
           COPY "FICHE.REC".
           COPY "METIER.REC".
           COPY "V-BASES.REC".
           COPY "DOC.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

       01  TEXTES                PIC X(19200).

       01  PLANS-NAME.
           02 PLANS-ID           PIC X(8) VALUE "S-PLANS.".
           02 ANNEE-PLANS        PIC 999.

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".PT1".

           COPY "V-VH00.CPY".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 999999.
       01  SAVE-PERSON           PIC 9(8) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  EQUIPE                PIC 99 VALUE 0.
       01  CUMUL                 PIC 99999V99.
       01  CUMUL-T               PIC 99999V99.
       01  MOIS-NUM              PIC 99.
       01  PR-NUM                PIC 9(8).
       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.
       01  JOUR-DEBUT            PIC 99 VALUE 1.
       01  JOUR-FIN              PIC 99 VALUE 31.
       01  JOUR-IDX              PIC 99.
       01  TEST-LINE             PIC 99.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HE-TEMP PIC ZZ.ZZ BLANK WHEN ZERO.
           02 HE-FIN.
              03 HE-X PIC X VALUE ":".
              03 HE-F PIC ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
      *        FORM 
               PLANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-PT.
       
           MOVE LNK-SUFFIX TO ANNEE-PLANS.
           MOVE MOIS-JRS(LNK-MOIS) TO JOUR-FIN.
           OPEN INPUT PLANS.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-MOIS TO SAVE-MOIS MOIS-DEBUT MOIS-FIN.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7 THRU 8
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 IF MENU-PROG-NUMBER = 0
                      PERFORM AVANT-7 
                   ELSE
                      PERFORM AVANT-7-A
                   END-IF
           WHEN  8 IF MENU-PROG-NUMBER = 0
                      PERFORM AVANT-8 
                   ELSE
                      PERFORM AVANT-8-A
                   END-IF
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 IF MENU-PROG-NUMBER = 0
                      PERFORM APRES-7 
                   ELSE
                      PERFORM APRES-7-A
                   END-IF
           WHEN  8 IF MENU-PROG-NUMBER = 0
                      PERFORM APRES-8 
                   ELSE
                      PERFORM APRES-8-A
                   END-IF
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-7.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7-A.
           ACCEPT JOUR-DEBUT
             LINE 19 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8-A.
           ACCEPT JOUR-FIN
             LINE 20 POSITION 35 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-07.

       APRES-8.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.


       APRES-7-A.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM JOUR-DEBUT
             WHEN 66 ADD 1 TO JOUR-DEBUT
           END-EVALUATE.
           IF JOUR-DEBUT < 1 
              MOVE 1 TO JOUR-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF JOUR-DEBUT > MOIS-JRS(LNK-MOIS)
              MOVE MOIS-JRS(LNK-MOIS) TO JOUR-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF   JOUR-DEBUT >  JOUR-FIN
           MOVE JOUR-DEBUT TO JOUR-FIN.
           PERFORM DIS-HE-07.

       APRES-8-A.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM JOUR-FIN
             WHEN 66 ADD 1 TO JOUR-FIN
           END-EVALUATE.
           IF JOUR-FIN < 1 
              MOVE 1 TO JOUR-FIN
              MOVE 1 TO INPUT-ERROR.
           IF JOUR-FIN > MOIS-JRS(LNK-MOIS)
              MOVE MOIS-JRS(LNK-MOIS) TO JOUR-FIN
              MOVE 1 TO INPUT-ERROR.
           IF JOUR-DEBUT > JOUR-FIN
              MOVE JOUR-DEBUT TO JOUR-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.
       
       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM FULL-PROCESS
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       FULL-PROCESS.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           PERFORM READ-PERSON THRU READ-EXIT.
       START-PR-END.
           EXIT.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-1
           END-IF.
           IF STATUT > 0 AND NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF COUT > 0 AND NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           IF EQUIPE > 0 AND NOT = CAR-EQUIPE
              GO READ-PERSON-1
           END-IF.
           PERFORM FILL-FILES VARYING MOIS-COURANT FROM MOIS-DEBUT 
                           BY 1 UNTIL MOIS-COURANT > MOIS-FIN.
           PERFORM DIS-HE-01.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD PV-KEY.
           MOVE CAR-METIER TO MET-CODE.
           MOVE PR-LANGUAGE TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE EQ-NUMBER TO HE-Z2.
           DISPLAY HE-Z2  LINE 11 POSITION 31.
           DISPLAY EQ-NOM LINE 11 POSITION 35.
       DIS-HE-07.
           IF MENU-PROG-NUMBER = 0
              PERFORM DIS-MOIS-DEB
           ELSE
              PERFORM DIS-JOUR-DEB
           END-IF.
       DIS-HE-08.
           IF MENU-PROG-NUMBER = 0
              PERFORM DIS-MOIS-FIN
           ELSE
              PERFORM DIS-JOUR-FIN
           END-IF.
       DIS-HE-END.
           EXIT.

       DIS-MOIS-DEB.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 19 POSITION 35.
           MOVE "MO" TO LNK-AREA.
           MOVE 19401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       DIS-MOIS-FIN.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 35.
           MOVE "MO" TO LNK-AREA.
           MOVE 20401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-JOUR-DEB.
           MOVE JOUR-DEBUT TO HE-Z2.
           DISPLAY HE-Z2 LINE 19 POSITION 35.
           MOVE SEM-IDX(LNK-MOIS, JOUR-DEBUT) TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "SW" TO LNK-AREA.
           MOVE 19401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       DIS-JOUR-FIN.
           MOVE JOUR-FIN TO HE-Z2.
           DISPLAY HE-Z2 LINE 20 POSITION 35.
           MOVE SEM-IDX(LNK-MOIS, JOUR-FIN) TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "SW" TO LNK-AREA.
           MOVE 20401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       AFFICHAGE-ECRAN.
           MOVE 1326 TO LNK-VAL.
           ADD MENU-PROG-NUMBER TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       FILL-FILES.
           INITIALIZE PT-RECORD.
           MOVE FR-KEY TO PT-FIRME.
           MOVE REG-PERSON TO PT-PERSON.
           MOVE MOIS-COURANT TO PT-MOIS.
           READ PLANS INVALID INITIALIZE PT-RECORD
                NOT INVALID INITIALIZE CUMUL.
           IF PT-FIRME NOT = 0
              PERFORM FILL-LIGNE VARYING JOUR-IDX 
              FROM JOUR-DEBUT BY 1 UNTIL JOUR-IDX > JOUR-FIN
              IF LIN-NUM >= LIN-IDX
                 PERFORM TRANSMET
                 MOVE 0 TO LIN-NUM 
              END-IF
           END-IF.

       FILL-LIGNE.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              COMPUTE TEST-LINE = IMPL-MAX-LINE - 1
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= 67
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM 
           END-IF.
           IF LIN-NUM = 0
              MOVE PR-LANGUAGE TO FORM-LANGUE
              PERFORM READ-FORM
              PERFORM ENTETE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           IF PRES-JOUR(LNK-MOIS, JOUR-IDX) > 0
              PERFORM READ-PLAGE.

       ENTETE.
           MOVE FORMULAIRE TO TEXTES.
           MOVE TODAY-DATE TO DOC-DATE.
           CALL "4-DOCFIL" USING LINK-V
                                 PR-RECORD
                                 REG-RECORD
                                 CAR-RECORD
                                 CON-RECORD
                                 CCOL-RECORD
                                 FICHE-RECORD
                                 MET-RECORD
                                 BASES-REMUNERATION
                                 DOC-RECORD
                                 TEXTES.
           MOVE TEXTES TO FORMULAIRE.

           MOVE 12 TO LIN-NUM.
           MOVE  6 TO CAR-NUM.
           MOVE  28 TO COL-NUM.
           ADD 1 TO PAGE-NUMBER.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.

       READ-PLAGE.
           MOVE PT-PLAGE(JOUR-IDX) TO PLG-KEY.
           CALL "6-PLAGE" USING LINK-V PLG-RECORD FAKE-KEY.
           MOVE JOUR-IDX TO VH-00.
           MOVE 2 TO CAR-NUM.
           MOVE 5 TO COL-NUM.
           PERFORM FILL-FORM.
           COMPUTE LNK-NUM = SEM-IDX(MOIS-COURANT, JOUR-IDX) + 200.
           PERFORM SEM-NOM.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.
           IF PT-PLAGE(JOUR-IDX) > SPACES
              PERFORM HORAIRES
              ADD 1 TO LIN-NUM
           ELSE
              ADD 2 TO LIN-NUM.
           MOVE 5 TO COL-NUM.
           PERFORM TIRET UNTIL COL-NUM > 75.
           ADD 1 TO LIN-NUM.

       HORAIRES.
           MOVE PT-DEBUT(JOUR-IDX, 1) TO HE-TEMP.
           MOVE HE-TEMP TO ALPHA-TEXTE.
           MOVE 12 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE PT-FIN(JOUR-IDX, 1) TO HE-TEMP.
           MOVE HE-TEMP TO ALPHA-TEXTE.
           MOVE 18 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE PT-REPOS(JOUR-IDX, 1) TO HE-TEMP.
           MOVE HE-TEMP TO ALPHA-TEXTE.
           MOVE 24 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE PT-DEBUT(JOUR-IDX, 2) TO HE-TEMP.
           MOVE HE-TEMP TO ALPHA-TEXTE.
           MOVE 32 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE PT-FIN(JOUR-IDX, 2) TO HE-TEMP.
           MOVE HE-TEMP TO ALPHA-TEXTE.
           MOVE 38 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE PT-REPOS(JOUR-IDX, 2) TO HE-TEMP.
           MOVE HE-TEMP TO ALPHA-TEXTE.
           MOVE 44 TO COL-NUM.
           PERFORM FILL-FORM.
           
           COMPUTE VH-00 = PT-VAL(JOUR-IDX).
           ADD VH-00 TO CUMUL
           MOVE  2 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE CUMUL TO VH-00.
           MOVE 3 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE 56 TO COL-NUM
           PERFORM FILL-FORM.
           IF PLG-OCC > 0
              PERFORM DONNEES-OCC.
           ADD 1 TO LIN-NUM.
           MOVE 8 TO COL-NUM.
           MOVE PLG-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           IF PT-COMMENT(JOUR-IDX) > SPACES
              ADD 1 TO LIN-NUM
              MOVE 8 TO COL-NUM
              MOVE PT-COMMENT(JOUR-IDX) TO ALPHA-TEXTE
              PERFORM FILL-FORM.

       DONNEES-OCC.
           MOVE 63 TO COL-NUM.
           MOVE PLG-OCC TO OCC-KEY.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY.
           MOVE OCC-KEY TO OT-NUMBER
           CALL "6-OCCTXT" USING LINK-V OT-RECORD FAKE-KEY
           MOVE OT-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XEQUIPE.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XSEMNOM.CPY".
           COPY "XACTION.CPY".


