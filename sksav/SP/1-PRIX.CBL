      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-PRIX PRIX ARTICLES                        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-PRIX.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PRIX.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "PRIX.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "ARTICLE.REC".
           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 33. 
       01  PX-OCCURS           PIC 99 VALUE 0. 

       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-Z2 PIC ZZ BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6). 
           02 HE-Z8 PIC Z(8). 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
      *袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴�

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PRIX. 
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-PRIX.

           OPEN I-O   PRIX .

           CANCEL "6-PRIX".
           PERFORM AFFICHAGE-ECRAN .
           INITIALIZE PX-RECORD.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0163604000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2 THRU 3
                      MOVE 0017000022 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(2)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN OTHER MOVE 0000000005 TO EXC-KFR(1)
           END-EVALUATE.


       TRAITEMENT-ECRAN-01.

           PERFORM DISPLAY-F-KEYS.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 THRU 18 PERFORM AVANT-COUT
           WHEN 19 THRU 33 PERFORM AVANT-TAUX
           END-EVALUATE.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 1 
              MOVE 1 TO LNK-VAL
              CALL "0-BOOK" USING LINK-V
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              GO TRAITEMENT-ECRAN
           END-IF.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 THRU 18 PERFORM APRES-COUT
           WHEN 19 THRU 33 PERFORM APRES-TAUX.
      *     WHEN CHOIX-MAX  PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-1.
           IF ART-NUMBER = 0
              INITIALIZE PX-RECORD.
           ACCEPT ART-NUMBER 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           IF PX-ANNEE = 0 
              MOVE LNK-ANNEE TO PX-ANNEE
           END-IF.
           ACCEPT PX-ANNEE
             LINE  5 POSITION 32 SIZE  4
             TAB UPDATE NO BEEP CURSOR 1 
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3. 
           IF PX-MOIS = 0 
              MOVE LNK-MOIS TO PX-MOIS
           END-IF.
           ACCEPT PX-MOIS  
             LINE  6 POSITION 34 SIZE 2 
             TAB UPDATE NO BEEP CURSOR 1 
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-COUT.
           COMPUTE IDX = INDICE-ZONE - 3.
           COMPUTE LIN-IDX = IDX + 5.
           ACCEPT PX-VALEUR(IDX)
           LINE  LIN-IDX POSITION  6 SIZE 8
           TAB UPDATE NO BEEP CURSOR  1 
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-TAUX.
           COMPUTE IDX = INDICE-ZONE - 18.
           COMPUTE LIN-IDX = IDX + 5.
           IF PX-VALEUR(IDX) NOT = 0
              ACCEPT PX-CR(IDX)
              LINE LIN-IDX POSITION 60 SIZE 4
              TAB UPDATE NO BEEP CURSOR 1 
              ON EXCEPTION  EXC-KEY  CONTINUE
           ELSE 
              SUBTRACT 2 FROM INDICE-ZONE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 CONTINUE
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   CALL "2-ARTIC" USING LINK-V ART-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  OTHER PERFORM NEXT-ARTIC.
           IF ART-NUMBER = 0 
              MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0 
           AND ART-NUMBER > 0 
              PERFORM PX-RECENT.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 
                   MOVE EXC-KEY TO SAVE-KEY
                   PERFORM PX-NEXT
                   PERFORM AFFICHAGE-DETAIL
                   MOVE 1 TO INPUT-ERROR
           WHEN  8 PERFORM DEL-PRIX
           WHEN  2 CALL "2-PRIX" USING LINK-V PX-RECORD ART-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM AFFICHAGE-DETAIL
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN  5 PERFORM COPIE
                   PERFORM AFFICHAGE-DETAIL
                   MOVE 1 TO INPUT-ERROR
           END-EVALUATE.

       APRES-3.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 
                   MOVE EXC-KEY TO SAVE-KEY
                   PERFORM PX-NEXT
                   MOVE 1 TO INPUT-ERROR
           WHEN  2 CALL "2-PRIX" USING LINK-V PX-RECORD ART-RECORD 
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DISPLAY-F-KEYS
                   MOVE 1 TO INPUT-ERROR
           WHEN  8 PERFORM DEL-PRIX
           WHEN  5 PERFORM COPIE
                   PERFORM AFFICHAGE-DETAIL
                   MOVE 1 TO INPUT-ERROR
           END-EVALUATE.
           IF PX-MOIS > 12
              MOVE 12 TO PX-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF PX-MOIS < 1
              MOVE 1 TO PX-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF INPUT-ERROR = 0
              READ PRIX INVALID 
                MOVE "AA" TO LNK-AREA
                MOVE 13 TO LNK-NUM
                PERFORM DISPLAY-MESSAGE
              END-READ
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-COUT.
           EVALUATE EXC-KEY
           WHEN   5 IF INPUT-ERROR = 0
                       PERFORM SAVE-PRIX
                       PERFORM AFFICHAGE-DETAIL
                       MOVE 1 TO INDICE-ZONE
                    END-IF
           END-EVALUATE.

       APRES-TAUX.
           COMPUTE IDX = INDICE-ZONE - 18.
           IF  PX-VALEUR(IDX) > 0
           AND PX-CR(IDX) = 0
               MOVE 1 TO PX-CR(IDX) INPUT-ERROR.
           EVALUATE EXC-KEY
           WHEN   5 IF INPUT-ERROR = 0
                       PERFORM SAVE-PRIX
                       PERFORM AFFICHAGE-DETAIL
                       MOVE 1 TO INDICE-ZONE
                    END-IF
           END-EVALUATE.
           COMPUTE IDX = INDICE-ZONE - 18.
           PERFORM DIS-TAUX.
           IF IDX = 1
           AND EXC-KEY = 27
              COMPUTE INDICE-ZONE = 5 + PX-OCCURS
           END-IF.

       SAVE-PRIX.
           MOVE ART-NUMBER TO PX-ARTICLE.
           MOVE 0 TO PX-OCCURS IDX-1.
           PERFORM TEST-VIDE VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           IF PX-OCCURS = 0 
              PERFORM DEL-PRIX
           ELSE
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-TIME TO PX-TIME
              MOVE LNK-USER TO PX-USER
              WRITE PX-RECORD INVALID REWRITE PX-RECORD END-WRITE   
           END-IF.

       DEL-PRIX.
           DELETE PRIX INVALID CONTINUE END-DELETE.
           MOVE 1  TO DECISION.
           PERFORM PX-RECENT.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       NEXT-ARTIC.
           CALL "6-ARTIC" USING LINK-V ART-RECORD EXC-KEY.


       PX-RECENT.
           INITIALIZE PX-RECORD SAVE-KEY.
           MOVE 9999 TO PX-ANNEE.
           PERFORM PX-NEXT.

       PX-NEXT.
           CALL "6-PRIX" USING LINK-V PX-RECORD ART-RECORD SAVE-KEY.
             
       TEST-VIDE.
           IF  PX-VALEUR(IDX) > 0 
      *    AND PX-CR(IDX) > 0 
              ADD 1 TO PX-OCCURS
           ELSE
              MOVE 0 TO PX-VALEUR(IDX) PX-CR(IDX) 
              MOVE 1 TO IDX-1
           END-IF.

       DIS-HE-01.
           MOVE ART-NUMBER  TO HE-Z6.
           IF ART-NUMBER NOT = 0
              MOVE ART-NUMBER TO LNK-VAL.
           DISPLAY HE-Z6 LINE 3 POSITION 15.
           DISPLAY ART-NOM  LINE 3 POSITION 44 SIZE 35.

       DIS-HE-02.
           DISPLAY PX-ANNEE LINE 5 POSITION 32.
       DIS-HE-03.
           MOVE PX-MOIS TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 6 POSITION 34.
           MOVE "MO" TO LNK-AREA.
           MOVE 06441200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-COUT.
           PERFORM DIS-COUT VARYING IDX FROM 1 BY 1 UNTIL IDX > 15.
       DIS-HE-TAUX.
           PERFORM DIS-TAUX VARYING IDX FROM 1 BY 1 UNTIL IDX > 15.
       DIS-HE-END.
           EXIT.

       DIS-COUT.
           COMPUTE LIN-IDX = IDX + 5.
           MOVE PX-VALEUR(IDX) TO HE-Z8.
           DISPLAY HE-Z8 LINE  LIN-IDX POSITION 6.

       DIS-TAUX. 
           COMPUTE LIN-IDX = IDX + 5.
           MOVE PX-CR(IDX) TO HE-Z4.
           DISPLAY HE-Z4 LINE  LIN-IDX POSITION 60.
           

       AFFICHAGE-ECRAN.
           MOVE  170 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE PRIX.
           CANCEL "2-PRIX".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
       COPIE.
           IF ART-NUMBER = 0
              INITIALIZE PX-RECORD.
           ACCEPT ART-NUMBER 
             LINE 3 POSITION 65 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF ART-NUMBER = 0
              CONTINUE
           ELSE
              MOVE ART-NUMBER TO PX-ARTICLE
              WRITE PX-RECORD INVALID CONTINUE END-WRITE.
