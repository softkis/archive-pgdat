      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-DATEV2 TRANSFERT COMPTABILITE DATEV       �
      *  � WEILAND BAU VERSION 2                                 �
      *  � JOUR = TOUJOURS DERNIER JOUR DU MOIS                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-DATEV2.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "INTERCOM.FC".
           SELECT OPTIONAL TF-COMPTA ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.


       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire JOURNAL COMPTABLE

       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-MOIS   PIC 99.
              03 INTER-SUITE  PIC 99.
              03 INTER-STAT   PIC 99.
              03 INTER-COUT   PIC 9(8).
              03 INTER-COUT-1 REDEFINES INTER-COUT.
                 04 INTER-FIL PIC 99999.
                 04 INTER-CT  PIC 999.
              03 INTER-PERS   PIC 9(8).
              03 INTER-DC     PIC 9.
              03 INTER-COMPTE PIC X(30).
              03 INTER-POSTE  PIC 9(10).
              03 INTER-RUB    PIC 9(8).
              03 INTER-EXT    PIC X(42).
              03 INTER-LIB    PIC 9999.
              03 INTER-PERS-2 PIC 9(8).


           02 INTER-REC-DET.
              03 INTER-VALUE PIC S9(8)V99.
              03 INTER-UNITE PIC S9(8)V99.
              03 INTER-PERIODE PIC 99.

       FD  TF-COMPTA
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.

       01  TF-RECORD.
           02 TF-FIRME    PIC X(4).
           02 TF-FILL-A   PIC X.
      *    02 TF-PERS     PIC 9(8).
      *    02 TF-FILL-B   PIC X.
           02 TF-ANNEE    PIC 9999.
           02 TF-FILL-C   PIC X.
           02 TF-MOIS     PIC 99.
           02 TF-FILL-D   PIC X.

           02 TF-DATE.
              03 TF-DATE-J  PIC 99.
              03 TF-DATE-M  PIC 99.
              03 TF-DATE-A  PIC 9999.
              03 TF-FILL-3  PIC X.
           02 TF-COMPTE   PIC X(4).
           02 TF-FILL-F   PIC X.
           02 TF-COMPTE-1 PIC X(4).
           02 TF-FILL-F1  PIC X.
           02 TF-POSTE    PIC X(10).
           02 TF-POSTE-N REDEFINES TF-POSTE PIC Z(10).
           02 TF-FILL-H   PIC X.
      *    02 TF-POSTE-A  PIC Z(8).
      *    02 TF-FILL-I   PIC X.
           02 TF-HEURES   PIC 9(4),99.
           02 TF-FILL-J   PIC X.
           02 TF-MONTANT  PIC 9(6),99.
           02 TF-DC       PIC X.
           02 TF-FILL-K   PIC X.
           02 TF-DEVISE   PIC XXX.
           02 TF-FILL-L   PIC X.
           02 TF-LIBELLE  PIC X(40).
           02 TF-FILL-M   PIC X.
      *    02 TF-LIBELLE2 PIC X(40).
      *    02 TF-FILL-N   PIC X.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01  NOT-OPEN PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "COUT.REC".
           COPY "COMPTE.REC".
           COPY "LIBELLE.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "POCL.REC".

       01  HELP-XX.
           02 HELP-X                PIC S9(9)V99 OCCURS 2.
       
       01  INTER-NAME.
           02 FILLER             PIC XXXX VALUE "INTC".
           02 FIRME-INTER        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-INTER         PIC XXX.

       01  ECR-DISPLAY.
           02 HE-Z8 PIC -Z(8),ZZ.
           02 HE-Z4 PIC Z(4).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               TF-COMPTA
               INTER.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-DATEV2.
       

           CALL "0-TODAY" USING TODAY.
           MOVE FR-KEY TO FIRME-INTER.
           MOVE LNK-USER  TO USER-INTER.
           INITIALIZE COUT-RECORD.
           OPEN INPUT INTER.
           INITIALIZE HELP-XX.
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END GO READ-INTER-END.
           IF INTER-LIB = 0
              GO READ-INTER.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-COMPTA
              MOVE 1 TO NOT-OPEN
           END-IF.
           IF FR-KEY  NOT = COUT-FIRME
           OR INTER-COUT NOT = COUT-NUMBER
              PERFORM READ-COUT.
           PERFORM FILL-TEXTE.
           GO READ-INTER.
       READ-INTER-END.
           IF HELP-X(1) NOT = HELP-X(2)
              ACCEPT ACTION NO BEEP.
           PERFORM END-PROGRAM.

       FILL-TEXTE.
           INITIALIZE TF-RECORD.
           IF INTER-DC = 1
              MOVE "S" TO TF-DC 
           ELSE 
              MOVE "H" TO TF-DC 
           END-IF.

           MOVE INTER-LIB   TO LIB-NUMBER.
           CALL "6-LIB" USING LINK-V LIB-RECORD FAKE-KEY.
           MOVE LIB-DESCRIPTION TO TF-LIBELLE.
           IF INTER-PERS-2 > 0
              MOVE INTER-PERS-2 TO REG-PERSON
              CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY
              MOVE REG-MATRICULE TO PR-MATRICULE
              CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY
              CALL "4-PRNOM"  USING LINK-V PR-RECORD
              MOVE LNK-TEXT TO TF-LIBELLE
           END-IF.
           IF INTER-MOIS > 0
              MOVE 0 TO COL-IDX
              INSPECT TF-LIBELLE TALLYING COL-IDX FOR CHARACTERS 
              BEFORE "    "
              ADD 3 TO COL-IDX
              IF COL-IDX < 39
                 COMPUTE LNK-NUM = INTER-MOIS + 100 
                 MOVE "MO" TO LNK-AREA
                 CALL "0-GMESS" USING LINK-V
                 STRING LNK-TEXT DELIMITED BY SIZE INTO TF-LIBELLE
                 WITH POINTER COL-IDX
              END-IF
           END-IF.
           MOVE LNK-ANNEE  TO TF-ANNEE.
           MOVE FR-COMPTA  TO TF-FIRME.
           IF FR-COMPTA = SPACES
              MOVE FR-KEY TO HE-Z4
              MOVE HE-Z4  TO TF-FIRME.
      *    MOVE INTER-PERS TO TF-PERS.

           MOVE INTER-COMPTE TO TF-COMPTE.
           MOVE PARMOD-PATH3 TO TF-COMPTE-1.
           MOVE LNK-ANNEE    TO TF-DATE-A.
           IF INTER-MOIS > 0
              MOVE INTER-MOIS   TO TF-MOIS TF-DATE-M
              MOVE MOIS-JRS(INTER-MOIS) TO TF-DATE-J
           ELSE
              MOVE LNK-MOIS   TO TF-MOIS TF-DATE-M
              MOVE MOIS-JRS(LNK-MOIS) TO TF-DATE-J.

           INSPECT TF-COMPTE REPLACING ALL "###" BY INTER-CT.

           IF TF-DC = "S"
              MOVE INTER-POSTE TO TF-POSTE-N PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              IF PC-ANALYTIQUE > SPACES
                 MOVE PC-ANALYTIQUE TO TF-POSTE
              END-IF
      *       MOVE PC-NOM   TO TF-LIBELLE2
      *       MOVE INTER-RUB TO TF-POSTE-A
           END-IF.
      *    MOVE INTER-COUT  TO TF-COUT.
           MOVE INTER-VALUE TO TF-MONTANT.
           MOVE INTER-UNITE TO TF-HEURES.
           INSPECT TF-MONTANT REPLACING ALL " " BY "0".
           INSPECT TF-HEURES  REPLACING ALL " " BY "0".
           ADD INTER-VALUE TO HELP-X(INTER-DC).
           MOVE "EUR" TO TF-DEVISE.
           MOVE ";" TO TF-FILL-A
                       TF-FILL-C 
                       TF-FILL-D 
                       TF-FILL-F 
                       TF-FILL-F1
                       TF-FILL-H 
                       TF-FILL-J 
                       TF-FILL-K 
                       TF-FILL-L 
                       TF-FILL-M 
                       TF-FILL-3.
           WRITE TF-RECORD.
           MOVE HELP-X(1) TO HE-Z8.
           DISPLAY HE-Z8 LINE 24 POSITION 40.
           MOVE HELP-X(2) TO HE-Z8.
           DISPLAY HE-Z8 LINE 24 POSITION 55.

       READ-COUT.
           MOVE FR-KEY TO COUT-FIRME.
           MOVE INTER-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.

       END-PROGRAM.
           CLOSE INTER.
           IF NOT-OPEN = 1
              CLOSE TF-COMPTA.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------
           COPY "XACTION.CPY".
