      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-HRS1 IMPRESSION HEURES PERSONNE MOIS      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-HRS1.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "HEURES.FC".
           COPY "FORM160.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "HEURES.FDE".
           COPY "FORM160.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "160       ".
       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  PRECISION             PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "IMPRLOG.REC".
           COPY "OCCOM.REC".
           COPY "OCCUP.REC".
           COPY "OCCTXT.REC".
           COPY "POCL.REC".
           COPY "RUB.REC".
           COPY "JOURS.REC".

       01 HE-OCCOM.
          02 H-R PIC 99 OCCURS 30.
          02 H-N PIC X(11) OCCURS 30.
          02 H-I PIC 99 OCCURS 30.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(160) OCCURS 45.

       01  HRS-NAME.
           02 HEURES-ID           PIC X(9) VALUE "S-HEURES.".
           02 ANNEE-HEURES        PIC 999.

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".HRS".

           COPY "V-VH00.CPY".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 999999.
       01  SAVE-PERSON           PIC 9(8) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CUMUL.
           02  CUMUL1            PIC 99999V99.
           02  CUMULS            PIC 99999V99 OCCURS 30.
       01  LAST-PC               PIC 9(8) VALUE 0.
       01  CUMUL-T               PIC 99999V99.
       01  MOIS-NUM              PIC 99.
       01  PR-NUM                PIC 9(8).
       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.
       01  TEST-LINE             PIC 99.
       01  FEUILLE               PIC X VALUE "O".
       01  SH-02                 PIC 9999V99.
       01  SH-00-R REDEFINES SH-02.
           02  SH-INT          PIC 9(4).
           02  SH-DEC          PIC V99.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HE-TEMP PIC ZZ.ZZ BLANK WHEN ZERO.
           02 HE-FIN.
              03 HE-X PIC X VALUE ":".
              03 HE-F PIC ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               FORM 
               HEURES.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-HRS1.
       
           MOVE LNK-SUFFIX TO ANNEE-HEURES.
           OPEN I-O HEURES.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.
           INITIALIZE LIN-NUM LIN-IDX HE-OCCOM.

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-MOIS TO SAVE-MOIS MOIS-DEBUT MOIS-FIN.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7 THRU 8
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-8 
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  8 PERFORM APRES-8
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT FEUILLE
             LINE 12 POSITION 32 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY CONTINUE.

       AVANT-7.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-8.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           IF FEUILLE = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-07.

       APRES-8.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-08.
       
       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM FULL-PROCESS
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       FULL-PROCESS.
           MOVE REG-PERSON TO SAVE-PERSON.
           MOVE PR-MATCHCODE TO SAVE-MATCHCODE.
           PERFORM START-PR THRU START-PR-END VARYING MOIS-COURANT 
           FROM MOIS-DEBUT BY 1 UNTIL MOIS-COURANT > MOIS-FIN.
           IF LIN-NUM > LIN-IDX
              PERFORM TRANSMET.
           PERFORM END-PROGRAM.

       START-PR.
           MOVE SAVE-PERSON TO REG-PERSON.
           MOVE SAVE-MATCHCODE TO PR-MATCHCODE.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           INITIALIZE CUMUL CUMUL-T NOT-FOUND.
           MOVE 66 TO EXC-KEY.
           PERFORM READ-PERSON THRU READ-EXIT.
       START-PR-END.
           EXIT.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-1
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           PERFORM NON-PRODUCTIFS.
           PERFORM FILL-FILES.
           PERFORM RESET-FILE.
           PERFORM DIS-HE-01.
           IF FEUILLE NOT = "N"
           AND LIN-NUM > 0
              PERFORM TRANSMET
              MOVE 0 TO MOIS-NUM
           END-IF.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
       FILL-FILES.
           INITIALIZE HRS-RECORD PR-NUM OCC-KEY LAST-PC.
           MOVE FR-KEY TO HRS-FIRME.
           MOVE REG-PERSON TO HRS-PERSON.
           MOVE MOIS-COURANT TO HRS-MOIS.
           START HEURES KEY >= HRS-KEY INVALID CONTINUE
                NOT INVALID 
           INITIALIZE CUMUL 
           PERFORM READ-HRS THRU READ-HRS-END.

       READ-HRS.
           READ HEURES NEXT AT END 
                GO READ-HRS-END 
           END-READ.
           IF FR-KEY       NOT = HRS-FIRME
           OR REG-PERSON   NOT = HRS-PERSON
           OR MOIS-COURANT NOT = HRS-MOIS 
              GO READ-HRS-END.
           IF  HRS-OCCUP > 0
           AND HRS-USER NOT = "?&"
              GO READ-HRS.
           PERFORM FILL-LIGNE.
           GO READ-HRS.
       READ-HRS-END.
           IF  LIN-NUM > LIN-IDX
           AND MENU-BATCH > 0
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM PR-NUM OCC-KEY MOIS-NUM LAST-PC
           END-IF.

       FILL-LIGNE.
           IF COUNTER = 0 
              MOVE 45 TO IMPL-MAX-LINE
              COMPUTE TEST-LINE = IMPL-MAX-LINE - 1
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= IMPL-MAX-LINE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM PR-NUM OCC-KEY
              MOVE 99 TO MOIS-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM ENTETE
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           IF HRS-MOIS NOT = MOIS-NUM
              IF MOIS-NUM NOT = 99
                 INITIALIZE CUMUL
              END-IF
              MOVE HRS-MOIS TO MOIS-NUM LNK-NUM
              PERFORM MOIS-NOM-1
              MOVE 7 TO COL-NUM
              PERFORM FILL-FORM
              MOVE LNK-ANNEE TO VH-00
              MOVE 4 TO CAR-NUM
              ADD 1 TO COL-NUM
              PERFORM FILL-FORM
              PERFORM DONNEES-PERSONNE
           END-IF.

           IF HRS-PERSON NOT = PR-NUM
              IF LIN-NUM < TEST-LINE
                 PERFORM DONNEES-PERSONNE
              ELSE
                 PERFORM TRANSMET
                 MOVE 0 TO LIN-NUM PR-NUM OCC-KEY
                 PERFORM READ-FORM
                 PERFORM ENTETE
                 MOVE LIN-IDX TO LIN-NUM
                 MOVE HRS-MOIS TO MOIS-NUM
                 PERFORM MOIS-NOM
                 MOVE 7 TO COL-NUM
                 PERFORM FILL-FORM
                 MOVE LNK-ANNEE TO VH-00
                 MOVE 4 TO CAR-NUM
                 ADD 1 TO COL-NUM
                 PERFORM FILL-FORM
                 PERFORM DONNEES-PERSONNE
              END-IF
           END-IF.

           MOVE HRS-JOUR TO VH-00.
           MOVE 2 TO CAR-NUM.
           MOVE 3 TO COL-NUM.
           PERFORM FILL-FORM.
           COMPUTE LNK-NUM = SEM-IDX(MOIS-COURANT, HRS-JOUR) + 200.
           PERFORM SEM-NOM.
           ADD 1 TO COL-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = HRS-DEBUT / 100.
           MOVE 2 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE 10 TO COL-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = HRS-FIN   / 100.
           MOVE 2 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE 16 TO COL-NUM.
           PERFORM FILL-FORM.

           IF HRS-REPOS > 0
              COMPUTE VH-00 = HRS-REPOS / 100
              MOVE 2 TO CAR-NUM
              MOVE 2 TO DEC-NUM
              MOVE 21 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.

           MOVE 25 TO COL-NUM.
           MOVE "          ^" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE HRS-TOTAL-NET TO VH-00.
           MOVE 3 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE 27 TO COL-NUM.
           PERFORM FILL-FORM.

           IF HRS-USER NOT = "?&"
              ADD HRS-TOTAL-NET TO CUMUL1.
           ADD HRS-TOTAL-NET TO CUMULS(30)
           MOVE 148 TO COL-NUM.
           MOVE CUMUL1 TO VH-00.
           MOVE 3 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
      *     ADD 1 TO LIN-NUM.
           PERFORM FILL-FORM.
      *     SUBTRACT 1 FROM LIN-NUM.

           MOVE CUMULS(30) TO VH-00.
           MOVE 3 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           MOVE 27 TO COL-NUM.
           ADD 1 TO LIN-NUM.
           PERFORM FILL-FORM.
           SUBTRACT 1 FROM LIN-NUM.


           PERFORM COMPL VARYING IDX FROM 1 BY 1 UNTIL IDX > 14.
           IF HRS-OCCUP = 0
              IF HRS-POSTE > 0
                 MOVE HRS-POSTE TO PC-NUMBER VH-00
                 PERFORM DONNEES-POSTE
              END-IF
              IF HRS-RUB > 0
                 MOVE HRS-RUB TO RUB-NUMBER
                 CALL "6-RUB" USING LINK-V RUB-RECORD FAKE-KEY
                 MOVE RUB-NOM TO ALPHA-TEXTE
                 ADD 1 TO COL-NUM
                 PERFORM FILL-FORM
              END-IF
           ELSE
              MOVE HRS-OCCUP TO OCC-KEY 
              PERFORM DONNEES-OCC
           END-IF.
QQ         ADD 1 TO LIN-NUM.
           IF MENU-BATCH > 0
              ADD 1 TO LIN-NUM.

       COMPL.
           MOVE H-R(IDX) TO IDX-1.
           IF IDX-1 > 0
              PERFORM COMPL1.

       COMPL1.
           COMPUTE COL-NUM = 61 + IDX * 6.
           MOVE "         ^" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE COL-NUM = 61 + IDX * 6.
           IF IDX-1 < 21
              MOVE HRS-TARIF-IDX(IDX-1) TO VH-00
           ELSE
              SUBTRACT 20 FROM IDX-1
              MOVE HRS-UNITE(IDX-1) TO VH-00
           END-IF.
           ADD VH-00 TO CUMULS(IDX).
           MOVE 3 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE COL-NUM = 61 + IDX * 6.
           MOVE CUMULS(IDX) TO VH-00.
           ADD  1 TO LIN-NUM.
           MOVE 3 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.
           SUBTRACT 1 FROM LIN-NUM.


       DONNEES-PERSONNE.
           ADD 1 TO LIN-NUM.
           MOVE REG-PERSON TO VH-00.
           MOVE 6  TO CAR-NUM.
           MOVE 7 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 15 TO COL-NUM.
           MOVE PR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PR-PRENOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE HRS-PERSON TO PR-NUM.
           ADD 1 TO LIN-NUM.

       DONNEES-OCC.
           MOVE 43 TO COL-NUM.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY.
           MOVE OCC-KEY TO OT-NUMBER
           CALL "6-OCCTXT" USING LINK-V OT-RECORD FAKE-KEY
           MOVE OT-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE 0 TO LAST-PC.

       DONNEES-POSTE.
           MOVE 34 TO COL-NUM.
           MOVE 8 TO CAR-NUM.
           PERFORM FILL-FORM.
           IF PC-NUMBER NOT = LAST-PC
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              MOVE PC-NOM TO ALPHA-TEXTE
              MOVE "^" TO A-T(25)
              ADD 1 TO COL-NUM
              PERFORM FILL-FORM.
           MOVE PC-NUMBER TO LAST-PC.

       ENTETE.
      * DONNEES FIRME
           MOVE  3 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE 147 TO COL-NUM.
QQ         ADD 1 TO PAGE-NUMBER.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 65 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 75 TO COL-NUM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

      *    DATE EDITION
           MOVE 135 TO COL-NUM.
           MOVE TODAY-JOUR TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 138 TO COL-NUM.
           MOVE TODAY-MOIS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 141 TO COL-NUM.
           MOVE TODAY-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           PERFORM COMPS VARYING IDX FROM 1 BY 1 UNTIL IDX > 14.
           
       COMPS.
           COMPUTE SH-02 = IDX / 2.
           COMPUTE LIN-NUM = 5.
           COMPUTE COL-NUM = 61 + (12 * SH-02).
           IF SH-DEC = 0 ADD 1 TO LIN-NUM.
           MOVE H-N(IDX) TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COPY "XDIS.CPY".
       DIS-HE-06.
           DISPLAY FEUILLE LINE 12 POSITION 32.

       DIS-HE-07.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 19 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-08.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1331 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           INITIALIZE OCO-RECORD IDX-1.
           PERFORM READ-OCCOM THRU READ-OCCOM-END.

       READ-OCCOM.
           CALL "6-OCCOM" USING LINK-V OCO-RECORD NX-KEY.
           IF OCO-NUMBER = 0
              GO READ-OCCOM-END
           END-IF.
           ADD 1 TO IDX-1.
           MOVE OCO-NUMBER TO H-R(IDX-1)
           MOVE OCO-NOM    TO H-N(IDX-1)
           MOVE IDX-1      TO H-I(OCO-NUMBER)
           GO READ-OCCOM.
       READ-OCCOM-END.
           EXIT.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L160O" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           INITIALIZE FORMULAIRE LIN-NUM LIN-IDX LAST-PC.

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "L160O" USING LINK-V FORMULAIRE.
           CANCEL "L160O".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XSEMNOM.CPY".


       NON-PRODUCTIFS.
           INITIALIZE JRS-RECORD .
           MOVE FR-KEY     TO JRS-FIRME-2.
           MOVE MOIS-DEBUT TO JRS-MOIS-2.
           MOVE REG-PERSON TO JRS-PERSON-2.
           PERFORM NEXT-JOUR THRU NEXT-JOUR-END.

       NEXT-JOUR.
           CALL "6-JOUR2" USING LINK-V JRS-RECORD NX-KEY.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR MOIS-FIN   <     JRS-MOIS 
              GO NEXT-JOUR-END
           END-IF.
           IF JRS-OCCUPATION = 0
           OR JRS-COMPLEMENT > 0
              GO NEXT-JOUR
           END-IF.
           PERFORM ADD-HRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.
           GO NEXT-JOUR.
       NEXT-JOUR-END.

       ADD-HRS.
           IF JRS-HRS(IDX) > 0
              PERFORM MAKE-KEY.
       MAKE-KEY.
           INITIALIZE HRS-RECORD.
           MOVE JRS-FIRME TO 
                HRS-FIRME HRS-FIRME-A HRS-FIRME-B HRS-FIRME-C.
           MOVE JRS-PERSON TO 
                HRS-PERSON HRS-PERSON-A HRS-PERSON-B HRS-PERSON-C.
           MOVE JRS-OCCUPATION TO 
                HRS-OCCUP HRS-OCCUP-A HRS-OCCUP-B HRS-OCCUP-C.
           MOVE JRS-MOIS TO HRS-MOIS HRS-MOIS-A HRS-MOIS-B HRS-MOIS-C.
           MOVE IDX TO HRS-JOUR HRS-JOUR-A HRS-JOUR-B HRS-JOUR-C.
           MOVE "?&" TO HRS-USER.
           MOVE JRS-HRS(IDX) TO HRS-TOTAL-NET.
           WRITE HRS-RECORD INVALID 
                 REWRITE HRS-RECORD
           END-WRITE.

       RESET-FILE.
           INITIALIZE HRS-RECORD.
           MOVE FR-KEY TO HRS-FIRME.
           MOVE REG-PERSON TO HRS-PERSON.
           MOVE MOIS-DEBUT TO HRS-MOIS.
           START HEURES KEY >= HRS-KEY INVALID KEY CONTINUE
                NOT INVALID 
           PERFORM RESET-HRS THRU RESET-HRS-END.

       RESET-HRS.
           READ HEURES NEXT AT END 
                GO RESET-HRS-END 
           END-READ.
           IF FR-KEY       NOT = HRS-FIRME
           OR REG-PERSON   NOT = HRS-PERSON
           OR MOIS-FIN     <     HRS-MOIS 
              GO RESET-HRS-END.
           IF HRS-USER = "?&"
              DELETE HEURES INVALID CONTINUE.
           GO RESET-HRS.
       RESET-HRS-END.
