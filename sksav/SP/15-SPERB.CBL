      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 15-SPERA   TRANSFERT SPERALUX SOLID         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    15-SPERA.

       ENVIRONMENT DIVISION.
 
       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.

           SELECT PICKUP ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-PICKUP.

           SELECT PROBLEME ASSIGN TO DISK, "HR-PROB"
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

           COPY "HEURES.FC".
           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.

       FD  PICKUP
           LABEL RECORD STANDARD
           DATA RECORD IS PICK-RECORD.

       01  PICK-RECORD.
           02 PICK-FIRME           PIC 9(4).
           02 PICK-PERS            PIC 9(6).
           02 PICK-JOUR            PIC 99.
           02 PICK-MOIS            PIC 99.
           02 PICK-ANNEE           PIC 9999.

           02 PICK-HEURES.
              03 PICK-HEURE-DEBUT  PIC 9999.
              03 PICK-HEURE-DR REDEFINES PICK-HEURE-DEBUT.
                 04 PICK-HR        PIC 99.
                 04 PICK-MIN       PIC 99.
              03 PICK-HEURE-FIN    PIC 9999.
              03 PICK-HEURE-FR REDEFINES PICK-HEURE-FIN.
                 04 PICK-HRF       PIC 99.
                 04 PICK-MINF      PIC 99.

           02 PICK-PRIMES.  
              03 PICK-ESSEN-1      PIC 999V99.
              03 PICK-ESSEN-2      PIC 999V99.
              03 PICK-NACHTZULAGE  PIC 999V99.
              03 PICK-AUSLAND      PIC 999V99.
           02 PICK-PR REDEFINES PICK-PRIMES.
              03 PICK-PRIME        PIC 999V99 OCCURS 4.

           COPY "HEURES.FDE".
           COPY "TRIPR.FDE".

       FD  PROBLEME
           LABEL RECORD STANDARD
           DATA RECORD IS PROB-RECORD.

       01  PROB-RECORD.
           02 PROB-FIRME   PIC 9(4).
           02 PROB-TEST-F  PIC X.
           02 PROB-PERS    PIC 9(6).
           02 PROB-TEST-P  PIC X.
           02 PROB-FILLER  PIC X.
           02 PROB-ANNEE   PIC 9999.
           02 PROB-TEST-A  PIC X.
           02 PROB-FILLER  PIC X.
           02 PROB-MOIS    PIC 99.
           02 PROB-TEST-M  PIC X.
           02 PROB-FILLER  PIC X.
           02 PROB-JOUR    PIC 99.
           02 PROB-FILLER  PIC X.
           02 PROB-DEBUT   PIC 9999.
           02 PROB-FILLER  PIC X.
           02 PROB-FIN     PIC 9999.
           02 PROB-FILLER  PIC X.
           02 PROB-LINE    PIC 9999.
           02 PROB-FILLER  PIC X.


       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX PIC 99 VALUE 2.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "V-MINUTE.CPY".
           COPY "PARMOD.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CCOL.REC".
           COPY "CALEN.REC".
           COPY "LIVRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "CODPAIE.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
SU         COPY "CODTXT.REC".

       01  TARIF                PIC 99.
       01  JOUR-IDX             PIC 99.
       01  JOUR                 PIC 99.
       01  HELP-1               PIC 99.
       01  HELP-2               PIC 99.
       77  HELP-3     PIC 99V99 COMP-3.
       77  HELP-4     PIC S99V99 COMP-3.
       77  HELP-5     PIC 99.
       77  HELP-6     PIC 99.

       01  NOM-TRANSFERT        PIC X(30) VALUE SPACES.
       
       01  TEST-ROUND           PIC 999V99999.
       01  COMPTEUR             PIC 9(5).
       01  COMPTEUR-FAUTES      PIC 9(5) VALUE 0.

       01  HRS-NAME.
           02 FILLER            PIC X(9) VALUE "S-HEURES.".
           02 ANNEE-HEURES       PIC 999.

       01  TRIPR-NAME.
           02 FILLER             PIC XXX VALUE "TRI".
           02 FIRME-TRIPR        PIC 99999.
           02 FILLER             PIC XXXX VALUE ".DSK".
           02 USER-TRIPR         PIC XXXXXXXX.

       01  HE-Z4 PIC Z(4).
       01  HE-Z6 PIC Z(6).


       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
          COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               PICKUP 
               PROBLEME
               TRIPR
               HEURES.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-15-SPERA.
       
       TEST-OPEN.
           MOVE LNK-SUFFIX TO ANNEE-HEURES.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           OPEN I-O HEURES.
           MOVE FR-KEY   TO FIRME-TRIPR.
           MOVE LNK-USER TO USER-TRIPR.

           DELETE FILE TRIPR.
           OPEN I-O TRIPR.
           INITIALIZE TRIPR-RECORD.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0027000000 TO EXC-KFR(6).
           MOVE 0052530000 TO EXC-KFR(11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-PATH 
           WHEN  2 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  2 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 09 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\SOLID\FILE" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 14 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-DEC.
           INITIALIZE COMPTEUR PROB-RECORD IDX-4.
           EVALUATE EXC-KEY
               WHEN  5 OPEN INPUT PICKUP
                       OPEN OUTPUT PROBLEME
                       PERFORM LECT-PICKUP THRU END-PICKUP
                       PERFORM START-TRI
                       PERFORM END-PROGRAM
               WHEN 68 CALL "5-SELCS" USING LINK-V PARMOD-RECORD
                       PERFORM AFFICHAGE-ECRAN 
                       PERFORM DISPLAY-F-KEYS
           END-EVALUATE.

       LECT-PICKUP.
           INITIALIZE PICK-RECORD.
           READ PICKUP AT END GO END-PICKUP.
           ADD 1 TO COMPTEUR.
           MOVE 0 TO INPUT-ERROR
           DISPLAY PICK-RECORD LINE 24 POSITION 10.
           IF PICK-PERS < 1
              GO LECT-PICKUP
           END-IF.
           IF PICK-PERS NOT = TRIPR-PERSON
              PERFORM WRITE-TRI.
           IF PICK-FIRME NOT = FR-KEY
              MOVE "?" TO PROB-TEST-F 
              MOVE 1 TO INPUT-ERROR.
           IF PICK-ANNEE NOT = LNK-ANNEE
              MOVE "?" TO PROB-TEST-A 
              MOVE 1 TO INPUT-ERROR.
           IF PICK-MOIS  NOT = LNK-MOIS
              MOVE "?" TO PROB-TEST-M 
              MOVE 1 TO INPUT-ERROR.

           IF INPUT-ERROR = 1
              PERFORM WRITE-PROB
              GO LECT-PICKUP
           END-IF.
           PERFORM WRITE-OCC.
           PERFORM TEST-CSP VARYING IDX FROM 1 BY 1 UNTIL IDX > 4.
           GO LECT-PICKUP.
       END-PICKUP.
           CLOSE PICKUP.

       WRITE-OCC.
           IF PICK-PERS NOT = TRIPR-PERSON
              PERFORM WRITE-TRI.

           INITIALIZE HRS-RECORD.
           MOVE FR-KEY TO HRS-FIRME  HRS-FIRME-A HRS-FIRME-B
           HRS-FIRME-C.
           MOVE PICK-PERS TO HRS-PERSON HRS-PERSON-A HRS-PERSON-B
           HRS-PERSON-C.
           MOVE PICK-MOIS TO HRS-MOIS HRS-MOIS-A HRS-MOIS-B HRS-MOIS-C.
           MOVE PICK-JOUR TO HRS-JOUR HRS-JOUR-A HRS-JOUR-B HRS-JOUR-C.
           MOVE PICK-HEURE-DEBUT TO HRS-DEBUT
           HRS-INTERVENT HRS-INTERVENT-A HRS-INTERVENT-B HRS-INTERVENT-C.
           MOVE PICK-HEURE-FIN TO HRS-FIN.

           PERFORM NDF.
           IF HRS-OCCUP NOT = 0
              INITIALIZE HRS-TARIF.
           IF HRS-TOTAL-BRUT > 0
              MOVE TODAY-TIME TO HRS-TIME
              MOVE LNK-USER TO HRS-USER
              MOVE "X" TO HRS-BATCH
              IF LNK-SQL = "Y" 
                 CALL "9-HEURES" USING LINK-V HRS-RECORD WR-KEY 
              END-IF
              WRITE HRS-RECORD INVALID REWRITE HRS-RECORD END-WRITE.

       NDF.
           INITIALIZE HRS-TARIF.
           MOVE HRS-HR-D TO HELP-1 IDX-3.
           MOVE HRS-HR-F TO HELP-2 IDX-4.
           IF HELP-2 < HELP-1 ADD 24 TO HELP-2 IDX-4.
           SUBTRACT 1 FROM IDX-4.
           COMPUTE HRS-TOTAL-BRUT = HELP-2 - HELP-1.
           IF HRS-HR-D = HRS-HR-F
              COMPUTE IDX-2 = HRS-HR-D + 1
              IF IDX-2 > 24 SUBTRACT 24 FROM IDX-2 END-IF
              MOVE 0 TO TEST-ROUND
              IF HRS-MIN-F > 0 
                 COMPUTE TEST-ROUND = (MINUTE(HRS-MIN-F) / 100)
              END-IF
              IF HRS-MIN-D > 0 
                 COMPUTE TEST-ROUND = 
                 TEST-ROUND - (MINUTE(HRS-MIN-D) / 100)
              END-IF
              ADD TEST-ROUND TO HRS-TOTAL-BRUT
              IF CCOL-TARIF-NUIT(IDX-2) NOT = 0 
                 MOVE CCOL-TARIF-NUIT(IDX-2) TO TARIF
                 MOVE TEST-ROUND TO HRS-TARIF-HRS(TARIF)
              END-IF
           ELSE 
              IF HRS-MIN-D > 0
                 COMPUTE IDX-2 = HRS-HR-D + 1
                 IF IDX-2 > 24 SUBTRACT 24 FROM IDX-2 END-IF
                 SUBTRACT 1 FROM HRS-TOTAL-BRUT
                 ADD 1 TO HELP-1 IDX-3
                 COMPUTE TEST-ROUND = (100 - MINUTE(HRS-MIN-D)) / 100
                 ADD TEST-ROUND TO HRS-TOTAL-BRUT
                 IF CCOL-TARIF-NUIT(IDX-2) NOT = 0 
                    MOVE CCOL-TARIF-NUIT(IDX-2) TO TARIF
                    MOVE TEST-ROUND TO HRS-TARIF-HRS(TARIF)
                 END-IF
              END-IF
              IF HRS-MIN-F > 0
                 COMPUTE IDX-2 = HRS-HR-F + 1
                 IF IDX-2 > 24 SUBTRACT 24 FROM IDX-2 END-IF
                 COMPUTE TEST-ROUND = (MINUTE(HRS-MIN-F)) / 100
                 ADD TEST-ROUND TO HRS-TOTAL-BRUT
                 IF CCOL-TARIF-NUIT(IDX-2) NOT = 0 
                    MOVE CCOL-TARIF-NUIT(IDX-2) TO TARIF
                    ADD TEST-ROUND TO HRS-TARIF-HRS(TARIF)
                 END-IF
              END-IF
              PERFORM ADD-NUIT VARYING IDX FROM IDX-3 BY 1 UNTIL
                    IDX > IDX-4
           END-IF.
           MOVE HRS-TOTAL-BRUT TO HRS-TOTAL-NET.
           PERFORM DIM-FER.

       ADD-NUIT.
           MOVE IDX TO IDX-2.
           ADD 1 TO IDX-2.
           IF IDX-2 > 24 SUBTRACT 24 FROM IDX-2.
           IF CCOL-TARIF-NUIT(IDX-2) NOT = 0 
              MOVE CCOL-TARIF-NUIT(IDX-2) TO TARIF
              ADD 1 TO HRS-TARIF-HRS(TARIF)
           END-IF.

       WRITE-TRI.
           MOVE PICK-PERS TO TRIPR-PERSON REG-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF PRES-TOT(LNK-MOIS) = 0
              MOVE 0 TO REG-PERSON 
           END-IF.
           IF REG-PERSON = 0
              MOVE "?" TO PROB-TEST-P 
              MOVE 1 TO INPUT-ERROR 
              MOVE 0 TO TRIPR-PERSON 
           ELSE
              WRITE TRIPR-RECORD INVALID CONTINUE 
              NOT INVALID PERFORM CLEAN-OLD-TOTAL
              END-WRITE
              INITIALIZE CAR-RECORD CCOL-RECORD
              CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY
              MOVE CAR-CCOL TO LNK-VAL
              COMPUTE LNK-NUM = 0
              CALL "6-GCCOL"  USING LINK-V CCOL-RECORD
              MOVE CCOL-CALENDRIER TO CAL-NUMBER
              CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY
           END-IF.

       DIM-FER.
           IF CAL-JOUR(LNK-MOIS, PICK-JOUR) = 0
           AND SEM-IDX(LNK-MOIS, PICK-JOUR) = 7 
              IF HRS-HR-D < 22
                 PERFORM DIMANCHE
              END-IF
           END-IF.
           IF  CAL-JOUR(LNK-MOIS, PICK-JOUR) > 0
           AND CAL-JOUR(LNK-MOIS, PICK-JOUR) < 3
              IF HRS-HR-D < 22
                 PERFORM FERIES
              END-IF
           END-IF.
           PERFORM DELIMITE-NET VARYING IDX FROM 1 BY 1 UNTIL
                    IDX > 20.

       DIMANCHE.
           PERFORM CALCULS-1.
           MOVE HELP-3 TO HRS-TARIF-JRS(10).
       SAMEDIS.
           PERFORM CALCULS-1.
           MOVE HELP-3 TO HRS-TARIF-JRS(9).

       FERIES.
           PERFORM CALCULS-1.
           MOVE HELP-3 TO HRS-TARIF-JRS(1).
           IF HRS-TARIF-JRS(10) > HELP-3
              SUBTRACT HELP-3 FROM HRS-TARIF-JRS(10).

       DEB-FERIE.
           PERFORM CALCULS.
           MOVE HELP-3 TO HRS-TARIF-JRS(1).
           IF HRS-TARIF-JRS(10) > HELP-3
              SUBTRACT HELP-3 FROM HRS-TARIF-JRS(10).

       SAMEDI.
           PERFORM CALCULS.
           MOVE HELP-3 TO HRS-TARIF-JRS(10).

       CALCULS.
           MOVE HRS-HR-D  TO HELP-1.
           MOVE HRS-HR-F  TO HELP-2.
           MOVE HRS-MIN-D TO HELP-5.
           MOVE HRS-MIN-F TO HELP-6.
           MOVE 0 TO HELP-3.
           IF HELP-1 < 22
              MOVE 22 TO HELP-1
              MOVE 0  TO HELP-5
           END-IF.
           IF HELP-2 < HELP-1 ADD 24 TO HELP-2.
           IF HELP-1 = HELP-2
              COMPUTE IDX-2 = HELP-1 + 1
              MOVE 0 TO TEST-ROUND
              IF HELP-6 > 0
                 COMPUTE TEST-ROUND = (MINUTE(HELP-6)) / 100
              END-IF
              IF HELP-5 > 0
                 COMPUTE TEST-ROUND =
                         TEST-ROUND - (MINUTE(HELP-5)) / 100
              END-IF
              ADD TEST-ROUND TO HELP-3
           ELSE 
              IF HELP-5 > 0
                 COMPUTE IDX-2 = HELP-1 + 1
                 ADD 1 TO HELP-1 IDX-3
                 COMPUTE TEST-ROUND = (100 - MINUTE(HELP-5)) / 100
                 ADD TEST-ROUND TO HELP-3
              END-IF
           COMPUTE HELP-3 = HELP-3 + HELP-2 - HELP-1
              IF HELP-6 > 0
                 COMPUTE IDX-2 = HELP-2 + 1
                 COMPUTE TEST-ROUND = (MINUTE(HELP-6)) / 100
                 ADD  TEST-ROUND TO HELP-3
               END-IF
           END-IF.

       CALCULS-1.
           MOVE HRS-HR-D  TO HELP-1.
           MOVE HRS-HR-F  TO HELP-2.
           MOVE HRS-MIN-D TO HELP-5.
           MOVE HRS-MIN-F TO HELP-6.
           MOVE 0 TO HELP-3.
           IF HELP-2 > 22
           OR HELP-2 < HELP-1 
              MOVE 22 TO HELP-2
              MOVE 0  TO HELP-6
           END-IF.
           IF HELP-1 = HELP-2
              COMPUTE IDX-2 = HELP-1 + 1
              MOVE 0 TO TEST-ROUND
              IF HELP-6 > 0
                 COMPUTE TEST-ROUND = (MINUTE(HELP-6)) / 100
              END-IF
              IF HELP-5 > 0
                 COMPUTE TEST-ROUND =
                         TEST-ROUND - (MINUTE(HELP-5)) / 100
              END-IF
              ADD TEST-ROUND TO HELP-3
           ELSE 
              IF HELP-5 > 0
                 COMPUTE IDX-2 = HELP-1 + 1
                 ADD 1 TO HELP-1 IDX-3
                 COMPUTE TEST-ROUND = (100 - MINUTE(HELP-5)) / 100
                 ADD TEST-ROUND TO HELP-3
              END-IF
           COMPUTE HELP-3 = HELP-3 + HELP-2 - HELP-1
              IF HELP-6 > 0
                 COMPUTE IDX-2 = HELP-2 + 1
                 COMPUTE TEST-ROUND = (MINUTE(HELP-6)) / 100
                 ADD  TEST-ROUND TO HELP-3
               END-IF
           END-IF.

       DELIMITE-NET.
           IF HRS-TOTAL-NET < HRS-TARIF-IDX(IDX)
              MOVE HRS-TOTAL-NET TO HRS-TARIF-IDX(IDX)
           END-IF.


       CLEAN-OLD-TOTAL. 
           INITIALIZE HRS-RECORD.
           MOVE FR-KEY    TO HRS-FIRME-A.
           MOVE PICK-PERS TO HRS-PERSON-A.
           MOVE PICK-MOIS TO HRS-MOIS-A.
           START HEURES KEY >= HRS-KEY-A INVALID CONTINUE
           NOT INVALID PERFORM DELETE-HEURES THRU DELETE-HRS-END.
           PERFORM DEL-CSP VARYING IDX FROM 1 BY 1 UNTIL IDX > 4.

       DELETE-HEURES.
           READ HEURES NEXT AT END
               GO DELETE-HRS-END
           END-READ.
           IF FR-KEY NOT = HRS-FIRME
           OR PICK-PERS NOT = HRS-PERSON
           OR LNK-MOIS  NOT = HRS-MOIS
              GO DELETE-HRS-END
           END-IF.
           IF HRS-BATCH NOT = "X"
              GO DELETE-HEURES.
           IF LNK-SQL = "Y" 
              CALL "9-HEURES" USING LINK-V HRS-RECORD DEL-KEY 
           END-IF.
           DELETE HEURES INVALID CONTINUE.
           GO DELETE-HEURES.
       DELETE-HRS-END.
           EXIT.


       WRITE-PROB.
           MOVE PICK-FIRME       TO PROB-FIRME.
           MOVE PICK-PERS        TO PROB-PERS.
           MOVE PICK-ANNEE       TO PROB-ANNEE.
           MOVE PICK-MOIS        TO PROB-MOIS.
           MOVE PICK-JOUR        TO PROB-JOUR.
           MOVE PICK-HEURE-DEBUT TO PROB-DEBUT.
           MOVE PICK-HEURE-FIN   TO PROB-FIN.
           MOVE COMPTEUR         TO PROB-LINE.
           WRITE PROB-RECORD.
           ADD 1 TO COMPTEUR-FAUTES.
           MOVE COMPTEUR-FAUTES TO HE-Z4.
           DISPLAY HE-Z4 LINE 20 POSITION 20.
           DISPLAY PROB-RECORD LINE 20 POSITION 10 SIZE 24.
           INITIALIZE PROB-RECORD.

       START-TRI.
           INITIALIZE TRIPR-RECORD.
           START TRIPR KEY > TRIPR-KEY INVALID CONTINUE
           NOT INVALID PERFORM READ-TRI THRU READ-TRI-END.
       READ-TRI.
           READ TRIPR NEXT AT END 
               GO READ-TRI-END
           END-READ.
           MOVE TRIPR-PERSON TO REG-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           IF REG-PERSON = 0
              GO READ-TRI.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
     ?*    CALL "4-HRCRE" USING LINK-V REG-RECORD.
           CALL "4-HRADD" USING LINK-V REG-RECORD.
           CALL "4-A" USING LINK-V PR-RECORD REG-RECORD.
           PERFORM MALADIE THRU MALADIE-END.
           PERFORM DIS-HE-01.
           DELETE TRIPR INVALID CONTINUE.
           GO READ-TRI.
       READ-TRI-END.
           EXIT.

       MALADIE.
           INITIALIZE L-RECORD.
           MOVE FR-KEY     TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON. 
           MOVE LNK-MOIS   TO L-MOIS.
       MALADIE-1.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD NX-KEY.
           IF REG-PERSON NOT = L-PERSON
           OR LNK-MOIS   NOT = L-MOIS
              GO MALADIE-END.
           IF L-SUITE > 0
              MOVE L-SUITE TO LNK-SUITE 
              PERFORM DJT.
           GO MALADIE-1.
       MALADIE-END.
           MOVE 0 TO LNK-SUITE.

       DJT.
           CALL "4-DJT" USING LINK-V REG-RECORD.
           MOVE LNK-VAL   TO L-DATE-REPRISE.
           MOVE LNK-VAL-2 TO L-DATE-DERNIER.
           MOVE LNK-POSITION TO L-DATE-3M.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD WR-KEY.
       DIS-HE-01.
           MOVE REG-PERSON    TO HE-Z6.
           DISPLAY HE-Z6     LINE  7 POSITION 35.
           DISPLAY PR-NOM    LINE  7 POSITION 45.
           DISPLAY PR-PRENOM LINE  8 POSITION 45.

       AFFICHAGE-ECRAN.
           MOVE 2555 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           PERFORM AFFICHAGE-DETAIL.
           PERFORM DIS-CODSAL VARYING IDX FROM 1 BY 1 UNTIL IDX > 15.

       DIS-CODSAL.
           COMPUTE LIN-IDX = IDX + 4.
           IF PARMOD-SEL(IDX) NOT = 0
              MOVE PARMOD-SEL(IDX) TO HE-Z4 CD-NUMBER
              DISPLAY HE-Z4 LINE LIN-IDX POSITION 53
              CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY
SU            CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY
SU            MOVE CD-NUMBER TO CTX-NUMBER
              CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY
              DISPLAY CTX-NOM LINE LIN-IDX POSITION 58 SIZE 23
           END-IF.

       AFFICHAGE-DETAIL.
           DISPLAY PARMOD-PATH LINE 14 POSITION 25 SIZE 40.

       END-PROGRAM.
           CLOSE HEURES.
           CLOSE TRIPR.
           DELETE FILE TRIPR.
           IF COMPTEUR-FAUTES > 0
              MOVE "SL" TO LNK-AREA
              MOVE 0 TO LNK-POSITION
              MOVE 46 TO LNK-NUM
              CALL "0-DMESS" USING LINK-V
              ACCEPT ACTION
           END-IF.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".

       TEST-CSP.
           IF  PICK-PRIME(IDX) NOT = 0
           AND PARMOD-SEL(IDX) NOT = 0
              PERFORM CSP
           END-IF.

       CSP.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY    TO CSP-FIRME.
           MOVE LNK-MOIS  TO CSP-MOIS.
           MOVE PICK-PERS TO CSP-PERSON.
           MOVE PARMOD-SEL(IDX) TO CSP-CODE.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           ADD 1                TO CSP-UNITE.
           ADD PICK-PRIME(IDX)  TO CSP-TOTAL.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD WR-KEY.

       DEL-CSP.
           INITIALIZE CSP-RECORD.
           MOVE FR-KEY    TO CSP-FIRME.
           MOVE LNK-MOIS  TO CSP-MOIS.
           MOVE PICK-PERS TO CSP-PERSON.
           MOVE PARMOD-SEL(IDX) TO CSP-CODE.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD DEL-KEY.


