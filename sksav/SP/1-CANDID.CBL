      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-CANDID  SIGNALETIQUE DES CANDIDATS        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-CANDID.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CANDID.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CANDID.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01  ECRAN-SUITE.
           02 ECR-S1             PIC 999 VALUE 260.
           02 ECR-S2             PIC 999 VALUE 261.
           02 ECR-S3             PIC 999 VALUE 262.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S              PIC 999 OCCURS 3.
       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE 6.
           02  CHOIX-MAX-2       PIC 99 VALUE 7.
           02  CHOIX-MAX-3       PIC 99 VALUE 2.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 3.

       01  ETAT-CIVIL.
           02 ETAT-1             PIC X VALUE "C".
           02 ETAT-2             PIC X VALUE "M".
           02 ETAT-3             PIC X VALUE "V".
           02 ETAT-4             PIC X VALUE "D".
           02 ETAT-5             PIC X VALUE "S".
       01  ETAT-CIVIL-R REDEFINES ETAT-CIVIL.
           02 ETAT               PIC X OCCURS 5.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "MESSAGE.REC".
           COPY "PERSON.REC".
       01  ACTION-CALL           PIC 9 VALUE 0.
       77  HELP-1     PIC 9(8).

       01  X-PASSWORD  PIC X(22).
       01  NO-PASSWORD PIC 9.

       77  WHELP-1    PIC S9(6).

       01   ECR-DISPLAY.
            02 HE-Z2 PIC Z(2). 
            02 HE-Z3 PIC Z(3). 
            02 HE-Z4 PIC Z(4). 
            02 HE-Z6 PIC Z(6). 
            02 HE-Z8 PIC Z(8).
            02 HE-10 PIC Z(10). 
            02 HE-DATE .
               03 HE-JJ PIC ZZ.
               03 FILLER PIC X VALUE ".".
               03 HE-MM PIC ZZ.
               03 FILLER PIC X VALUE ".".
               03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CANDIDAT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-CANDID.

           OPEN I-O   CANDIDAT.

           PERFORM AFFICHAGE-ECRAN .
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0100000000 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           IF ECRAN-IDX = 1
              MOVE 0000680000 TO EXC-KFR (14) 
           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640421 TO EXC-KFR(1)
                      MOVE 0084000000 TO EXC-KFR(2) 
                      MOVE 0000080000 TO EXC-KFR(4) 
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2 IF ACTION-CALL = 0
                      MOVE 0000000061 TO EXC-KFR(1)
                  ELSE
                      MOVE 0000000000 TO EXC-KFR(1)
                  END-IF
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0000000034 TO EXC-KFR(1)
                      MOVE 0000000012 TO EXC-KFR(2)
                      MOVE 0000000012 TO EXC-KFR(4)
           WHEN CHOIX-MAX(1)
                      MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(4) 
                      MOVE 0052000000 TO EXC-KFR(11).

           IF ECRAN-IDX = 2
              MOVE 0067680000 TO EXC-KFR (14) 
              MOVE 0012130000 TO EXC-KFR (3)
           EVALUATE INDICE-ZONE
           WHEN 5   MOVE 0029000000 TO EXC-KFR(1)
                    MOVE 0000000065 TO EXC-KFR(13)
                    MOVE 6667680000 TO EXC-KFR(14)
           WHEN CHOIX-MAX(2)
                    MOVE 0100000005 TO EXC-KFR(1)
                    MOVE 0052000000 TO EXC-KFR(11)
            END-EVALUATE 
           END-IF.

           IF ECRAN-IDX = 3
              MOVE 0067000000 TO EXC-KFR (14) 
              MOVE 0012130000 TO EXC-KFR (3)
              EVALUATE INDICE-ZONE
                 WHEN CHOIX-MAX(3)
                 MOVE 0100000005 TO EXC-KFR (1)
                 MOVE 0052000000 TO EXC-KFR (11)
              END-EVALUATE 
           END-IF.

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.
 
           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1-1 
           WHEN  2 PERFORM AVANT-1-2 
           WHEN  3 PERFORM AVANT-1-3 
           WHEN  4 PERFORM AVANT-1-4 
           WHEN  5 PERFORM AVANT-1-5 
           WHEN CHOIX-MAX(1)
                PERFORM AVANT-DEC
              END-EVALUATE 
           END-IF.

           IF ECRAN-IDX = 2
           EVALUATE INDICE-ZONE
           WHEN  1 THRU 4 PERFORM AVANT-INFO
           WHEN  5 PERFORM AVANT-2-5 
           WHEN CHOIX-MAX(2)
                PERFORM AVANT-DEC
              END-EVALUATE 
           END-IF.
           IF EXC-KEY > 66 
           AND EXC-KEY < 69 
           AND CAN-KEY = 0
               MOVE 98 TO EXC-KEY.
           IF ECRAN-IDX = 3
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-3-1 
           WHEN CHOIX-MAX(3)
                PERFORM AVANT-DEC
              END-EVALUATE 
           END-IF.

           IF EXC-KEY = 1 
              MOVE 01101000 TO LNK-VAL
              PERFORM HELP-SCREEN
              MOVE 98 TO EXC-KEY
           END-IF.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF  EXC-KEY > 66 
           AND EXC-KEY < 69 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
              IF ECRAN-IDX > 1
                 MOVE 1 TO INDICE-ZONE
              END-IF
              GO TRAITEMENT-ECRAN
           END-IF.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1-1 
           WHEN  3 PERFORM APRES-1-3 
           WHEN  4 PERFORM APRES-1-4 
           WHEN  5 PERFORM APRES-1-5
           WHEN CHOIX-MAX(1)
                 PERFORM APRES-DEC
              END-EVALUATE 
           END-IF.


           IF ECRAN-IDX = 2
           EVALUATE INDICE-ZONE
           WHEN  5 PERFORM APRES-2-5
           WHEN CHOIX-MAX(2)
                   PERFORM APRES-DEC
           END-EVALUATE
               IF INDICE-ZONE < CHOIX-MAX(2)
                  PERFORM ECRAN-2
               END-IF
           END-IF.

           IF ECRAN-IDX = 3
           EVALUATE INDICE-ZONE
           WHEN CHOIX-MAX(3)
                PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.


           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.
       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.            
           IF LNK-MATRICULE > 0
           AND ACTION-CALL = 0
              PERFORM NEXT-NUMBER
              MOVE 13 TO EXC-KEY
              MOVE LNK-MATRICULE TO CAN-MATRICULE
              PERFORM GET-PERS
              PERFORM AFFICHAGE-DETAIL
              MOVE 3 TO INDICE-ZONE
              MOVE 1 TO ACTION-CALL
           ELSE
           ACCEPT CAN-PERSON 
             LINE 3 POSITION 26 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
      *    IF  EXC-KEY = 12
      *    AND LNK-SQL = "Y"
      *       INITIALIZE CAN-RECORD
      *       PERFORM SQL THRU SQL-END.

       AVANT-1-2.
           IF LNK-MATRICULE > 0
              MOVE LNK-MATRICULE TO CAN-MATRICULE
           ELSE
              MOVE 04310000 TO LNK-POSITION
              CALL "0-GDATE" USING CAN-DATE-NAISS
                                   LNK-POSITION
                                   LNK-NUM
                                   EXC-KEY
           END-IF.
 
       AVANT-1-3.           
           IF LNK-MATRICULE = 0
           ACCEPT CAN-SNOCS
             LINE  4 POSITION 59 SIZE 3 
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-4.            
           IF CAN-DEBUT-A = 0 
              MOVE LNK-ANNEE TO CAN-DEBUT-A
              MOVE LNK-MOIS  TO CAN-DEBUT-M
              MOVE 1         TO CAN-DEBUT-J
           END-IF.
           MOVE 06310000 TO LNK-POSITION.
           CALL "0-GDATE" USING CAN-DATE-DEBUT
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-ANNEE TO CAN-DEBUT-A
              MOVE TODAY-MOIS  TO CAN-DEBUT-M
              MOVE TODAY-JOUR  TO CAN-DEBUT-J
              MOVE 0 TO LNK-NUM
              MOVE 13 TO EXC-KEY
              PERFORM DIS-E1-04.

       AVANT-1-5.
           MOVE 08310000 TO LNK-POSITION.
           CALL "0-GDATE" USING CAN-DATE-FIN
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           IF EXC-KEY = 5
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-ANNEE TO CAN-FIN-A
              MOVE TODAY-MOIS  TO CAN-FIN-M
              MOVE TODAY-JOUR  TO CAN-FIN-J
              MOVE 0 TO LNK-NUM
              MOVE 13 TO EXC-KEY
              PERFORM DIS-E1-05.

       AVANT-INFO.        
           COMPUTE IDX = INDICE-ZONE .
           COMPUTE LIN-IDX = 4 + IDX.
           ACCEPT CAN-INFO(IDX)
             LINE  LIN-IDX POSITION 25 SIZE 60
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-2-5.            
           IF CAN-ETAT-CIVIL = SPACES
              MOVE "C" TO CAN-ETAT-CIVIL
           END-IF.
           ACCEPT CAN-ETAT-CIVIL
             LINE 10 POSITION 25 SIZE 1 
             TAB UPDATE NO BEEP CURSOR 1 
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3-1.        
           ACCEPT CAN-CODE       
             LINE  5 POSITION 25 SIZE 10
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 2 TO LNK-PRESENCE
                   CALL "2-CANDID" USING LINK-V CAN-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN  5 PERFORM NEXT-NUMBER
           WHEN  7 CALL "2-REGMAT" USING LINK-V CAN-RECORD
                   PERFORM GET-PERS
                   CANCEL "2-REGMAT"
                   CANCEL "6-REGMAT"
                   PERFORM AFFICHAGE-ECRAN 
           WHEN 18 PERFORM APRES-DEC
           WHEN OTHER MOVE CAN-PERSON TO HELP-1
                   PERFORM NEXT-CANDID
                   IF CAN-PERSON = 0
                      MOVE HELP-1 TO CAN-PERSON
                   END-IF
           END-EVALUATE.
           PERFORM TEST-MATRICULE.

       APRES-1-2.
           EVALUATE EXC-KEY
           WHEN 2 MOVE "A" TO LNK-A-N
                  CALL "2-PERSON" USING LINK-V PR-RECORD
                  MOVE PR-MATRICULE TO CAN-MATRICULE
                  MOVE PR-MATCHCODE TO CAN-MATCHCODE
                  PERFORM AFFICHAGE-ECRAN
                  PERFORM AFFICHAGE-DETAIL
                  CANCEL "2-PERSON"
           WHEN 5 IF ACTION-CALL = 0
                  MOVE CAN-MATRICULE TO LNK-MATRICULE
                  CALL "1-PERSON" USING LINK-V 
                  PERFORM AFFICHAGE-ECRAN
                  PERFORM AFFICHAGE-DETAIL
                  CANCEL "1-PERSON" 
                  MOVE 13 TO EXC-KEY
                  END-IF
           END-EVALUATE.
           IF  PR-NAISS-J > 0
           AND PR-NAISS-M > 0
               PERFORM APRES-DATE.
           IF INPUT-ERROR = 0
           IF PR-NAISS-A < 1900
              MOVE 1 TO INPUT-ERROR
              MOVE 1900 TO PR-NAISS-A.

       APRES-1-3.
           IF  PR-SNOCS > 9
           AND PR-SNOCS < 999
              CALL "0-MATR" USING PR-MATRICULE LINK-V END-CALL
              MOVE LNK-NUM TO INPUT-ERROR.
           PERFORM GET-PERS.
           PERFORM AFFICHAGE-DETAIL.
           IF PR-NOM = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           

       APRES-1-4.
           COMPUTE HELP-1 = PR-DATE-NAISS + 140000.
           IF EXC-KEY = 10
              COMPUTE HELP-1 = PR-DATE-NAISS + 130000
           END-IF.
           IF EXC-KEY = 20
              COMPUTE HELP-1 = PR-DATE-NAISS 
           END-IF.
           IF CAN-DATE-APPROUVE > 0
              MOVE CAN-DATE-APPROUVE TO HELP-1
           END-IF.
           MOVE 0 TO INPUT-ERROR.
           IF CAN-DATE-DEBUT < HELP-1
              MOVE HELP-1 TO CAN-DATE-DEBUT
              MOVE 1 TO INPUT-ERROR.
           COMPUTE HELP-1 = PR-NAISS-A + 100.
           MOVE 0 TO INPUT-ERROR.
           IF CAN-DEBUT-A > HELP-1
              MOVE HELP-1 TO CAN-DEBUT-A
              MOVE 1 TO INPUT-ERROR.

           PERFORM APRES-DATE.
           IF EXC-KEY = 10
           OR EXC-KEY = 20
              MOVE CAN-DATE-DEBUT TO CAN-DATE-APPROUVE
              MOVE 13 TO EXC-KEY
           END-IF.
           PERFORM DIS-E1-04.

       APRES-1-5.
           IF  CAN-DATE-FIN > 0
           AND CAN-DATE-FIN < CAN-DATE-DEBUT 
              MOVE CAN-DATE-DEBUT TO CAN-DATE-FIN    
              MOVE 10 TO LNK-NUM
           END-IF.
           PERFORM APRES-DATE.
           PERFORM DIS-E1-05.


       TEST-CANDIDAT.
           PERFORM MAKE-KEY.
           DISPLAY SPACES LINE 6 POSITION 35 SIZE 15.
           READ CANDIDAT INVALID CONTINUE
           NOT INVALID
               IF CAN-MATRICULE NOT = PR-MATRICULE
               DISPLAY CAN-MATRICULE LINE 6 POSITION 35
                  MOVE 1 TO INPUT-ERROR
               END-IF
           END-READ.
           PERFORM AFFICHAGE-DETAIL.

       APRES-2-5.
           EVALUATE CAN-ETAT-CIVIL
                WHEN "C" MOVE 0 TO INPUT-ERROR
                         MOVE 1 TO CAN-ETAT 
                WHEN "M" MOVE 0 TO INPUT-ERROR
                         MOVE 2 TO CAN-ETAT 
                WHEN "V" MOVE 0 TO INPUT-ERROR
                         MOVE 3 TO CAN-ETAT 
                WHEN "D" MOVE 0 TO INPUT-ERROR
                         MOVE 4 TO CAN-ETAT 
                WHEN "S" MOVE 0 TO INPUT-ERROR
                         MOVE 5 TO CAN-ETAT 
                WHEN OTHER MOVE 1 TO INPUT-ERROR CAN-ETAT
           END-EVALUATE.
           MOVE CAN-ETAT TO MS-NUMBER.
           MOVE "EC" TO LNK-AREA.
           EVALUATE EXC-KEY
           WHEN  65 THRU 66 PERFORM NEXT-MESSAGES
           WHEN   2 CALL "2-MESS" USING LINK-V MS-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "2-MESS"
           END-EVALUATE.
           MOVE MS-NUMBER TO CAN-ETAT.
           IF CAN-ETAT = 0
              MOVE 1 TO CAN-ETAT
           END-IF.
           MOVE ETAT(CAN-ETAT) TO CAN-ETAT-CIVIL.
           PERFORM DIS-E2-05.


       APRES-DATE.
           IF LNK-NUM NOT = 0
              MOVE "AA" TO LNK-AREA
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           
       APRES-DEC.
           EVALUATE EXC-KEY
            WHEN  5 CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO CAN-TIME
                    MOVE LNK-USER TO CAN-USER
                    PERFORM MAKE-KEY
                    WRITE CAN-RECORD INVALID 
                    REWRITE CAN-RECORD END-WRITE   
                    IF LNK-SQL = "Y"
                       CALL "9-CANDID" USING LINK-V CAN-RECORD WR-KEY
                    END-IF
                    MOVE 1 TO DECISION 
                    IF LNK-MATRICULE > 0
                    CALL "6-CANDID" USING LINK-V CAN-RECORD "N" FAKE-KEY
                       GO END-PROGRAM
                    END-IF
            WHEN 18 DELETE CANDIDAT INVALID CONTINUE END-DELETE
                    IF LNK-SQL = "Y"
                       CALL "9-CANDID" USING LINK-V CAN-RECORD DEL-KEY
                    END-IF
                    INITIALIZE CAN-RECORD
                    PERFORM AFFICHAGE-DETAIL
                    MOVE 1 TO DECISION ECRAN-IDX
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-NUMBER.
           MOVE FR-KEY TO CAN-FIRME.
           MOVE 999999 TO CAN-PERSON.
           START CANDIDAT KEY < CAN-KEY INVALID 
               MOVE 0 TO CAN-PERSON
               NOT INVALID 
           READ CANDIDAT PREVIOUS AT END 
               MOVE 0 TO CAN-PERSON
           END-READ.
           IF FR-KEY NOT = CAN-FIRME
              MOVE 0 TO CAN-PERSON.
           ADD 1 TO CAN-PERSON.
           INITIALIZE CAN-REC-DET.
      *    PERFORM MAKE-KEY.
                
       TEST-MATRICULE.
           IF  LNK-MATRICULE > 0
           AND CAN-MATRICULE = 0
              MOVE LNK-MATRICULE TO CAN-MATRICULE.
           IF CAN-MATRICULE > 0
              PERFORM GET-PERS
              PERFORM AFFICHAGE-DETAIL.
                
       MAKE-KEY.
           MOVE FR-KEY TO CAN-FIRME CAN-FIRME-A.
           MOVE PR-MATCHCODE TO CAN-MATCHCODE.
           MOVE CAN-PERSON TO CAN-PERSON-A.
           MOVE PR-CODE-SEXE TO CAN-SEXE.

       
       NEXT-PERS.
           CALL "6-PERSON" USING LINK-V PR-RECORD A-N EXC-KEY.

       NEXT-CANDID.
           CALL "6-CANDID" USING LINK-V CAN-RECORD A-N EXC-KEY.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE CAN-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD A-N FAKE-KEY.


       DIS-00.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 11.

       DIS-E1-01.
           MOVE CAN-PERSON TO HE-Z6.
           IF CAN-PERSON NOT = 0
              MOVE CAN-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 LINE 3 POSITION 26.

       DIS-E1-02.
           MOVE PR-NAISS-A TO HE-AA.
           MOVE PR-NAISS-M TO HE-MM.
           MOVE PR-NAISS-J TO HE-JJ.
           DISPLAY HE-DATE  LINE 4 POSITION 31.
           DISPLAY PR-SNOCS LINE 4 POSITION 59.

           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 10 POSITION 21.

           DISPLAY PR-MAISON LINE 11 POSITION 21.
           DISPLAY PR-RUE LINE 11 POSITION 32.

           DISPLAY PR-PAYS LINE 12 POSITION 21.
           MOVE PR-CODE-POST TO HE-Z6.
           DISPLAY HE-Z6 LINE 12 POSITION 25.
           DISPLAY PR-LOCALITE LINE 12 POSITION 32.

       DIS-E1-04.
           MOVE CAN-DEBUT-A TO HE-AA.
           MOVE CAN-DEBUT-M TO HE-MM.
           MOVE CAN-DEBUT-J TO HE-JJ.
           DISPLAY HE-DATE  LINE 6 POSITION 31.

       DIS-E1-05.
           MOVE CAN-FIN-A TO HE-AA.
           MOVE CAN-FIN-M TO HE-MM.
           MOVE CAN-FIN-J TO HE-JJ.
           DISPLAY HE-DATE  LINE 8 POSITION 31.

       DIS-E2-01.
           DISPLAY CAN-INFO-1 LINE 5 POSITION 25.
       DIS-E2-02.
           DISPLAY CAN-INFO-2 LINE 6 POSITION 25.
       DIS-E2-03.
           DISPLAY CAN-INFO-3 LINE 7 POSITION 25.
       DIS-E2-04.
           DISPLAY CAN-INFO-4 LINE 8 POSITION 25.
       DIS-E2-05.
           DISPLAY CAN-ETAT-CIVIL LINE 10 POSITION 25.
           MOVE CAN-ETAT TO LNK-NUM.
           MOVE "EC" TO LNK-AREA.
           COMPUTE LNK-POSITION = 10301500
           CALL "0-DMESS" USING LINK-V.

       DIS-E3-01.
           DISPLAY CAN-CODE LINE 5 POSITION 25.

       DIS-STAMP.
           MOVE CAN-ST-ANNEE TO HE-AA.
           MOVE CAN-ST-MOIS  TO HE-MM.
           MOVE CAN-ST-JOUR  TO HE-JJ.
           DISPLAY HE-DATE  LINE 22 POSITION 50.
           MOVE CAN-ST-MIN TO HE-Z2.
           DISPLAY HE-Z2  LINE 22 POSITION 62.
           DISPLAY CAN-USER LINE 22 POSITION 70.

       x.
           PERFORM TEST-MATRICULE.

       AFFICHAGE-ECRAN.
           MOVE ECR-S(ECRAN-IDX) TO LNK-VAL.
           MOVE "    " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           IF ECRAN-IDX > 1
               PERFORM DIS-00
           END-IF.

       AFFICHAGE-DETAIL.
           MOVE INDICE-ZONE TO DECISION.
           EVALUATE ECRAN-IDX
               WHEN 1 PERFORM ECRAN-1 VARYING INDICE-ZONE FROM
               1 BY 1 UNTIL INDICE-ZONE = CHOIX-MAX(ECRAN-IDX)
               WHEN 2 PERFORM ECRAN-2 VARYING INDICE-ZONE FROM
               1 BY 1 UNTIL INDICE-ZONE = CHOIX-MAX(ECRAN-IDX)
               WHEN 3 PERFORM ECRAN-3 VARYING INDICE-ZONE FROM
               1 BY 1 UNTIL INDICE-ZONE = CHOIX-MAX(ECRAN-IDX)
           END-EVALUATE.
           MOVE DECISION TO INDICE-ZONE.

       ECRAN-1.
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM DIS-E1-01 
               WHEN  2 PERFORM DIS-E1-02 
               WHEN  3 PERFORM DIS-E1-02 
               WHEN  4 PERFORM DIS-E1-04 
               WHEN  5 PERFORM DIS-E1-05 
           END-EVALUATE.

       ECRAN-2.
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM DIS-E2-01
               WHEN  2 PERFORM DIS-E2-02
               WHEN  3 PERFORM DIS-E2-03
               WHEN  4 PERFORM DIS-E2-04
               WHEN  5 PERFORM DIS-E2-05 
           END-EVALUATE.

       ECRAN-3.
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM DIS-E3-01
           END-EVALUATE.

       END-PROGRAM.
           CLOSE CANDIDAT.
           MOVE 0 TO LNK-MATRICULE.
           IF PR-NOM = SPACES
              CANCEL "6-CANDID" 
           END-IF.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
             IF DECISION = 99 
                DISPLAY CAN-USER LINE 24 POSITION 5
                DISPLAY CAN-ST-JOUR  LINE 24 POSITION 15
                DISPLAY CAN-ST-MOIS  LINE 24 POSITION 18
                DISPLAY CAN-ST-ANNEE LINE 24 POSITION 21
                DISPLAY CAN-ST-HEURE LINE 24 POSITION 30
                DISPLAY CAN-ST-MIN   LINE 24 POSITION 33
                GO AVANT-DEC
             END-IF.
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

