      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-RAPC IMPRESSION RAPPORTS VALEURS NEGATIVES�
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  3-RAPC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "RAPPORT.FC".
           COPY "FORM130.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "RAPPORT.FDE".
           COPY "FORM130.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  JOB-STANDARD          PIC X(10) VALUE "130       ".
       01  TIRET-TEXTE           PIC X(7) VALUE "------+".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
           COPY "IMPRLOG.REC".
       01  NOT-OPEN              PIC 9 VALUE 0.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

           COPY "V-VH00.CPY".

       01  RAP-NAME.
           02 FILLER             PIC X(7) VALUE "S-RAPP.".
           02 ANNEE-RAP          PIC 999.

       01  FORM-NAME             PIC X(9) VALUE "FORMF.RAP".

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
                RAPPORT
                FORM.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-RAPC .

       
           IF NOT-OPEN = 0
              MOVE LNK-SUFFIX TO ANNEE-RAP
              OPEN I-O RAPPORT
              MOVE 1 TO NOT-OPEN.

           IF LNK-VAL = 99
              PERFORM END-PROGRAM.

           CALL "0-TODAY" USING TODAY.
           INITIALIZE RAP-RECORD.
           MOVE FR-KEY TO RAP-FIRME.
           MOVE PARMOD-MOIS-DEB  TO RAP-MOIS.
           START RAPPORT KEY >= RAP-KEY INVALID CONTINUE
           NOT INVALID PERFORM READ-RAP THRU READ-RAP-END.
           EXIT PROGRAM.

       READ-RAP.
           READ RAPPORT NEXT AT END 
              GO READ-RAP-END.
           IF FR-KEY  NOT = RAP-FIRME
           OR PARMOD-MOIS-FIN < RAP-MOIS 
              GO READ-RAP-END.
           MOVE 0 TO SH-00.
           PERFORM TEST-X VARYING IDX FROM 1 BY 1 UNTIL IDX > 19.
           IF SH-00 NOT = 0
              PERFORM FULL-PROCESS.


           GO READ-RAP.
       READ-RAP-END.
           EXIT.

       FULL-PROCESS.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= 64
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM PAGE-DATE 
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           PERFORM DONNEES-FIRME.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF COUNTER > 0
              IF LIN-IDX < LIN-NUM
                 PERFORM TRANSMET
              END-IF
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE
              CANCEL "P130"
              CLOSE RAPPORT.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".

       DONNEES-FIRME.
           MOVE  0 TO DEC-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  5 TO COL-NUM.
           MOVE FR-KEY TO VH-00.
           PERFORM FILL-FORM.
           MOVE 13 TO COL-NUM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 50 TO COL-NUM.
           MOVE FR-COMPTA TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 90 TO COL-NUM.
           EVALUATE FR-FREQ-FACT  
                WHEN 1 MOVE 104 TO LNK-NUM
                WHEN 2 MOVE 108 TO LNK-NUM
                WHEN 3 MOVE 105 TO LNK-NUM
                WHEN 6 MOVE 106 TO LNK-NUM
                WHEN 9 MOVE 99  TO LNK-NUM
           END-EVALUATE.
           MOVE "AA" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE FR-FREQ-FACT  TO VH-00.
           MOVE 5 TO CAR-NUM.
           PERFORM FILL-FORM.

           MOVE RAP-MOIS TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           MOVE 70 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE LNK-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           PERFORM FILL-X VARYING IDX FROM 1 BY 1 UNTIL IDX > 19.

           ADD 1 TO LIN-NUM.
           MOVE 3 TO COL-NUM.
           PERFORM TIRET UNTIL COL-NUM > 126.
           ADD 1 TO LIN-NUM.

       FILL-X.
           MOVE  4 TO CAR-NUM.
           COMPUTE COL-NUM = (IDX * 7) - 2.
           MOVE RAP-X(IDX) TO VH-00.
           PERFORM FILL-FORM.

       TEST-X.
           IF RAP-X(IDX) > 0
              MOVE 0 TO RAP-X(IDX).
           ADD RAP-X(IDX) TO SH-00.

       PAGE-DATE.
           CALL "0-TODAY" USING TODAY.
           ADD 1 TO PAGE-NUMBER.
           MOVE  4 TO CAR-NUM.
           MOVE  2 TO LIN-NUM.
           MOVE 115 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 90 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 93 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 96 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.
