      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-CAMION  MARQUER FIRMES CAMIONNEURS        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-CAMION.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FIRME.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FIRME.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01   CHOIX-MAX         PIC 99 VALUE 4.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".
       77  HELP-1     PIC 9(8).
       01  ACTION-CALL           PIC 9 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2). 
           02 HE-Z3 PIC Z(3). 
           02 HE-Z4 PIC Z(4). 
           02 HE-Z6 PIC Z(6).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FIRME.
       
       FILE-ERROR-PROC.
           CALL "C$RERR"  USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-CAMION.

           OPEN I-O   FIRME.
           INITIALIZE FIRME-RECORD.

           PERFORM AFFICHAGE-ECRAN.
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0027000000 TO EXC-KFR(6).
           MOVE 0052530000 TO EXC-KFR(11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0063640421 TO EXC-KFR(1)
                      MOVE 0000080000 TO EXC-KFR(4) 
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN CHOIX-MAX
                      MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.
 
           EVALUATE INDICE-ZONE
           WHEN 1 PERFORM AVANT-1 
           WHEN 2 PERFORM AVANT-2 
           WHEN 3 PERFORM AVANT-3 
           WHEN CHOIX-MAX PERFORM AVANT-DEC.


           IF EXC-KEY = 1 
              MOVE 01201000 TO LNK-VAL
              PERFORM HELP-SCREEN
              MOVE 98 TO EXC-KEY
           END-IF.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN CHOIX-MAX PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT FIRME-KEY 
             LINE 3 POSITION 15 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT FIRME-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.


       AVANT-3.
           ACCEPT FIRME-TRANSPORT 
             LINE 5 POSITION 24 SIZE 1
             TAB UPDATE NO BEEP CURSOR 1
           CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM AFFICHAGE-DETAIL.
 
      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 2 TO LNK-PRESENCE
                   CALL "2-FIRME" USING LINK-V 
                   MOVE FR-RECORD TO FIRME-RECORD
                   PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-FIRME
           END-EVALUATE.
           PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN  13 CONTINUE
           WHEN  OTHER PERFORM NEXT-FIRME
           END-EVALUATE.                     
           PERFORM AFFICHAGE-DETAIL.


       APRES-3.
           IF  FIRME-TRANSPORT NOT = " " AND NOT = "N" 
           AND NOT = "O" AND NOT = "J" AND NOT = "Y"
               MOVE 1 TO INPUT-ERROR
               MOVE " " TO FIRME-TRANSPORT.
           PERFORM AFFICHAGE-DETAIL.
           
       APRES-DEC.
           EVALUATE EXC-KEY
            WHEN  5 CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO FIRME-TIME
                    MOVE LNK-USER TO FIRME-USER
                    IF LNK-SQL = "Y" 
                       CALL "9-FIRME" USING LINK-V FR-RECORD WR-KEY 
                    END-IF
                    REWRITE FIRME-RECORD INVALID CONTINUE END-REWRITE   
                    MOVE 1 TO DECISION 
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       
       NEXT-FIRME.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-FIRMES" USING LINK-V FIRME-RECORD A-N EXC-KEY.

       DIS-HE-01.
           MOVE FIRME-KEY  TO HE-Z4.
           DISPLAY HE-Z4 LINE 3 POSITION 15.
           DISPLAY FIRME-NOM LINE 3 POSITION 47 SIZE 33.

       DIS-HE-02.
           DISPLAY FIRME-MATCHCODE LINE 3 POSITION 33.

       DIS-HE-03.
           DISPLAY FIRME-TRANSPORT LINE  5 POSITION 24.
       DIS-HE-END.

       AFFICHAGE-ECRAN.
           MOVE 209 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE FIRME.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
       
       