      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 7-PRGAR COMPTEUR HEURES MALADIE POUR        �
      *  � PRIME GARAGISTES                                      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    7-PRGAR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".

        
       01  JOURS-NAME.
           02 JOURS-ID           PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".

       PROCEDURE DIVISION USING LINK-V REG-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-FA-PRIME.

           INITIALIZE LNK-VAL.
           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           IF LNK-MOIS NOT = 12
              SUBTRACT 1 FROM ANNEE-JOURS.
           OPEN INPUT JOURS.
           MOVE 12 TO IDX-4
           PERFORM C-H.
           CLOSE JOURS.
           IF LNK-MOIS = 12
              EXIT PROGRAM.
           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN INPUT JOURS.
           PERFORM C-H VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > LNK-MOIS.
           CLOSE JOURS.
           EXIT PROGRAM.

       C-H.
           INITIALIZE JRS-RECORD.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE REG-PERSON TO JRS-PERSON.
           MOVE IDX-4      TO JRS-MOIS.
           START JOURS KEY > JRS-KEY INVALID CONTINUE
                NOT INVALID
                PERFORM READ-HEURES THRU READ-HEURES-END.

       READ-HEURES.
           READ JOURS NEXT AT END
               GO READ-HEURES-END.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR JRS-MOIS   NOT = IDX-4
           OR JRS-OCCUPATION > 50
              GO READ-HEURES-END
           END-IF.
           IF JRS-COMPLEMENT NOT = 0
           OR JRS-OCCUPATION =  0
              GO READ-HEURES
           END-IF.
           IF JRS-OCCUPATION = 11
           OR JRS-OCCUPATION = 40
              ADD JRS-HRS(32) TO LNK-VAL.  
           IF  JRS-OCCUPATION >  0
           AND JRS-OCCUPATION < 10
              ADD JRS-HRS(32) TO LNK-VAL.  
           GO READ-HEURES.
       READ-HEURES-END.

