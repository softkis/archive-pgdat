      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-ACARRI MODULE GESTION ARCHIVE CARRIERE    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-ACARRI.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CARRIERE.FCA".

       DATA DIVISION.

       FILE SECTION.

           COPY "CARRIERE.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  ACTION   PIC X.
       01  TODAY.
           02 TODAY-DATE.
              03 TODAY-ANNEE  PIC 9999.
              03 TODAY-MOIS   PIC 99.
              03 TODAY-JOUR   PIC 99.
           02 TODAY-TEMPS.
              03 TODAY-HEURE  PIC 99.
              03 TODAY-MIN    PIC 99.
              03 TODAY-SECS   PIC 9999.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "CARRIERE.LNK".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CARRIERE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-ACARRI.
       
           IF NOT-OPEN = 0
              OPEN I-O CARRIERE
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CAR-RECORD.
           IF EXC-KEY = 98
           OR EXC-KEY = 99
              PERFORM WRITE-CAR
              EXIT PROGRAM
           END-IF.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ CARRIERE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ CARRIERE NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 0 PERFORM START-3
               READ CARRIERE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE CAR-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START CARRIERE KEY < CAR-A-KEY INVALID GO EXIT-1.
       START-2.
           START CARRIERE KEY > CAR-A-KEY INVALID GO EXIT-1.
       START-3.
           START CARRIERE KEY <= CAR-A-KEY INVALID GO EXIT-1.

       WRITE-CAR.
           MOVE CAR-KEY  TO CAR-A-KEY.
           MOVE LNK-USER TO CAR-AR-USER.
           CALL "0-TODAY" USING TODAY.
           MOVE TODAY    TO CAR-AR-TIME.
           IF EXC-KEY = 99
              MOVE "W" TO CAR-ACTION
           ELSE
              MOVE "D" TO CAR-ACTION
           END-IF.
           WRITE CAR-RECORD INVALID REWRITE CAR-RECORD END-WRITE.

           COPY "XMESSAGE.CPY".

