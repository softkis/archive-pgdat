      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-ARRIBA TRANSFERT COMPTABILITE             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-ARRIBA.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "INTERCOM.FC".
           SELECT OPTIONAL TF-COMPTA ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.


       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire JOURNAL COMPTABLE

       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-MOIS   PIC 99.
              03 INTER-SUITE  PIC 99.
              03 INTER-STAT   PIC 99.
              03 INTER-COUT   PIC 9(8).
              03 INTER-PERS   PIC 9(8).
              03 INTER-DC     PIC 9.
              03 INTER-COMPTE PIC X(30).
              03 INTER-POSTE  PIC 9(10).
              03 INTER-EXT    PIC X(50).
              03 INTER-LIB    PIC 9999.
              03 INTER-PERS-2 PIC 9(8).

           02 INTER-REC-DET.
              03 INTER-VALUE PIC S9(8)V99.
              03 INTER-UNITE PIC S9(8)V99.
              03 INTER-PERIODE PIC 99.

       FD  TF-COMPTA
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.

       01  TF-RECORD.
           02 TF-DETAIL     PIC X(678).


       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01  NOT-OPEN PIC 9 VALUE 0.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "COUT.REC".
           COPY "COMPTE.REC".
           COPY "LIBELLE.REC".
           COPY "POCL.REC".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".


       01  TF-RECORD-1.
           02 TF-NUMERO     PIC Z(15).
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-FIRME      PIC X(5).
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-MANDANT    PIC 9(5) VALUE 30.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-DECOMPTE   PIC 9(5) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-ANNEE      PIC ZZZZ.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-MOIS       PIC 99.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-TYPE       PIC 9 VALUE 1.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-COMPTE     PIC X(11).
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-CODE       PIC 9(5) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-IMPOT      PIC 9(5) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-CONTRA     PIC X VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-CONTRA-NR  PIC X(11) VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-DEBIT      PIC Z(15),ZZ VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-CREDIT     PIC Z(15),ZZ VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".

           02 TF-BIDON      PIC 9(11) VALUE 599999.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-QUANT      PIC Z(15),ZZ VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-MONTANT    PIC Z(15),ZZ VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-PRESTA     PIC Z(15),ZZ VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".

           02 TF-POSTE      PIC 9(11).
           02 TF-FILLER     PIC X VALUE ";".

           02 TF-QUANT-1    PIC Z(15),ZZ VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-MONTANT-1  PIC Z(15),ZZ VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-PRESTA-1   PIC Z(15),ZZ VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-LIBELLE    PIC X(35).
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-DC         PIC X .
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-PLAN       PIC 9(6) VALUE 3.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-MANDANT-1  PIC 9(6) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-PLAN-1     PIC 9(6) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-MANDANT-2  PIC 9(6) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-PLAN-2     PIC 9(6) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-MANDANT-3  PIC 9(6) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-PLAN-3     PIC 9(6) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-CONDITION  PIC 9(6) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-DEVISE-E   PIC X(5) VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-REPRISE    PIC 9 VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-ERREUR     PIC X(255) VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-DEVISE     PIC X(5) VALUE "EUR".
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-GROUPE     PIC 9(5) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-DATE.
              03 TF-DATE-A  PIC 9999.
              03 TF-DATE-M  PIC 99.
              03 TF-DATE-J  PIC 99.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-DATE-MAJ   PIC 9(8) VALUE 0.
           02 TF-FILLER     PIC X VALUE ";".
           02 TF-AUTEUR     PIC X(31) VALUE SPACES.
           02 TF-FILLER     PIC X VALUE ";".

       01  HELP-XX.
           02 HELP-X                PIC S9(9)V99 OCCURS 2.
       
       01  INTER-NAME.
           02 FILLER             PIC XXXX VALUE "INTC".
           02 FIRME-INTER        PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 USER-INTER         PIC XXX.

       01  ECR-DISPLAY.
           02 HE-Z8 PIC -Z(8),ZZ.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               TF-COMPTA
               INTER.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-ARRIBA.
       
           CALL "0-TODAY" USING TODAY.
           MOVE FR-KEY TO FIRME-INTER.
           MOVE LNK-USER  TO USER-INTER.
           INITIALIZE COUT-RECORD.
           OPEN INPUT INTER.
           INITIALIZE HELP-XX.
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END GO READ-INTER-END.
           IF INTER-LIB = 0
              GO READ-INTER.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-COMPTA
              MOVE 1 TO NOT-OPEN
           END-IF.
           IF FR-KEY  NOT = COUT-FIRME
           OR INTER-COUT NOT = COUT-NUMBER
              PERFORM READ-COUT.
           PERFORM FILL-TEXTE.
           GO READ-INTER.
       READ-INTER-END.
           IF HELP-X(1) NOT = HELP-X(2)
              ACCEPT ACTION NO BEEP.
           PERFORM END-PROGRAM.

       FILL-TEXTE.
           INITIALIZE TF-RECORD.

           IF INTER-DC = 1
              MOVE "S" TO TF-DC 
           ELSE 
              MOVE "H" TO TF-DC 
           END-IF.

           MOVE INTER-LIB   TO LIB-NUMBER.
           CALL "6-LIB" USING LINK-V LIB-RECORD FAKE-KEY.
           MOVE LIB-DESCRIPTION TO TF-LIBELLE.
           IF INTER-PERS-2 > 0
              MOVE INTER-PERS-2 TO REG-PERSON
              CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY
              MOVE REG-MATRICULE TO PR-MATRICULE
              CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY
              CALL "4-PRNOM"  USING LINK-V PR-RECORD
              MOVE LNK-TEXT TO TF-LIBELLE
           END-IF.
           IF INTER-MOIS > 0
              MOVE 0 TO COL-IDX
              INSPECT TF-LIBELLE TALLYING COL-IDX FOR CHARACTERS 
              BEFORE "    "
              ADD 3 TO COL-IDX
              IF COL-IDX < 39
                 COMPUTE LNK-NUM = INTER-MOIS + 100 
                 MOVE "MO" TO LNK-AREA
                 CALL "0-GMESS" USING LINK-V
                 STRING LNK-TEXT DELIMITED BY SIZE INTO TF-LIBELLE
                 WITH POINTER COL-IDX
              END-IF
           END-IF.
           MOVE LNK-ANNEE  TO TF-ANNEE.

           MOVE INTER-MOIS   TO TF-MOIS.
           MOVE INTER-POSTE  TO TF-POSTE.
           IF INTER-MOIS = 0
              MOVE LNK-MOIS  TO TF-MOIS.
           MOVE INTER-COMPTE TO TF-COMPTE.
           MOVE TODAY-ANNEE  TO TF-DATE-A.
           MOVE TODAY-MOIS   TO TF-DATE-M.
           MOVE TODAY-JOUR   TO TF-DATE-J.

           ADD 1 TO PARMOD-PROG-NUMBER-1.
           MOVE PARMOD-PROG-NUMBER-1 TO TF-NUMERO. 
           MOVE FR-COMPTA TO TF-FIRME.
           MOVE INTER-VALUE TO TF-MONTANT.
           MOVE INTER-UNITE TO TF-QUANT.
           INSPECT TF-MONTANT REPLACING ALL " " BY "0".
      *    INSPECT TF-RECORD CONVERTING
      *    "굤닀뀑뙏뱚뼏꼦텤쉸" TO "eeeeaiioouuuaaAOUA".
           ADD INTER-VALUE TO HELP-X(INTER-DC).
           WRITE TF-RECORD FROM TF-RECORD-1.
           MOVE HELP-X(1) TO HE-Z8.
           DISPLAY HE-Z8 LINE 24 POSITION 40.
           MOVE HELP-X(2) TO HE-Z8.
           DISPLAY HE-Z8 LINE 24 POSITION 55.

       READ-COUT.
           MOVE FR-KEY TO COUT-FIRME.
           MOVE INTER-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.

       END-PROGRAM.
           CLOSE INTER.
           IF NOT-OPEN = 1
              CLOSE TF-COMPTA.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------


