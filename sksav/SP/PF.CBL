      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME PF ADAPTATON MENU POSTES DE FRAIS           �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    POF.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MENU.FC".
           COPY "MENUCOPY.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "MENU.FDE".
           COPY "MENUCOPY.FDE".

       WORKING-STORAGE SECTION.
             
       01  MENUS.
           02  MEN-1    PIC 9(10) VALUE 0404000000.
           02  MEN-2    PIC 9(10) VALUE 0404010000.
           02  MEN-3    PIC 9(10) VALUE 0404020000.
           02  MEN-4    PIC 9(10) VALUE 0404030000.
           02  MEN-5    PIC 9(10) VALUE 0404040000.
           02  MEN-6    PIC 9(10) VALUE 0404050000.
           02  MEN-7    PIC 9(10) VALUE 0404060000.
           02  MEN-8    PIC 9(10) VALUE 0404070000.
           02  MEN-9    PIC 9(10) VALUE 0404080000.
           02  MEN-10   PIC 9(10) VALUE 0404130000.
           02  MEN-11   PIC 9(10) VALUE 0102020300.
           02  MEN-12   PIC 9(10) VALUE 0202120000.
           02  MEN-13   PIC 9(10) VALUE 0203100000.
       01  COD-M REDEFINES MENUS.
           02  MEN PIC 9(10) OCCURS 13.


      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON MENU MENUCOPY.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  program.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-PF.


           OPEN I-O MENU.
           OPEN INPUT MENUCOPY.

           MOVE 0 TO LNK-VAL.
           DISPLAY "ADAPTATION MENU            " LINE 24 POSITION 10.
           PERFORM MENU VARYING IDX FROM 1 BY 1 UNTIL IDX > 13.
           EXIT PROGRAM.

       MENU.
           MOVE MEN(IDX) TO MNC-KEY.
           READ MENUCOPY INVALID CONTINUE
           NOT INVALID
              PERFORM TRANSLATE.

       TRANSLATE.
           MOVE MNC-RECORD TO MN-RECORD.
           WRITE MN-RECORD INVALID REWRITE MN-RECORD END-WRITE.
           CALL "4-MENUD" USING LINK-V MN-RECORD.
           CALL "4-MENUE" USING LINK-V MN-RECORD.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".


