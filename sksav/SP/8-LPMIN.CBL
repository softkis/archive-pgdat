      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-LPMIN TRANSFER ASCII LIVRE MINIMUM        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-LPMIN.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

       FD  TF-TRANS
           LABEL RECORD STANDARD
           DATA RECORD TLP-RECORD.
      *  ENREGISTREMENT FICHIER DE TRANSFERT ASCII LIVRE DE PAIE
      
       01  TLP-RECORD.
           02 TLP-PERS            PIC Z(6).
           02 TLP-FILLER          PIC X.
           02 TLP-NOM             PIC X(50).
           02 TLP-FILLER          PIC X.
           02 TLP-MATR            PIC X(13).
           02 TLP-FILLER          PIC X.
           02 TLP-CLASSE-I.
              05 TLP-CLASSE       PIC 9.
              05 TLP-CLASSE-A     PIC XXX.
              05 TLP-CLASSE-E     PIC ZZ.
           02 TLP-FILLER          PIC X.
           02 TLP-TAUX            PIC ZZ,ZZ.
           02 TLP-FILLER          PIC X.

           02 TLP-ANNEE           PIC 99999.
           02 TLP-FILLER          PIC X.
           02 TLP-MOIS            PIC 9999.
           02 TLP-FILLER          PIC X.
           02 TLP-MOIS-FIN        PIC 9999. 
           02 TLP-FILLER          PIC X.

           02 TLP-SALAIRE.
             03 TLP-S OCCURS 9.
                04 TLP-VAL          PIC -Z(7),ZZ.
                04 TLP-FILLER       PIC X.
          02 TLP-SAL-N REDEFINES TLP-SALAIRE.
             03 TLP-N OCCURS 9.
                04 TLP-NUM          PIC -Z(10).
                04 TLP-FILLER       PIC X.
          02 TLP-SAL-R REDEFINES TLP-SALAIRE.
1            03 TLP-BRUT            PIC -Z(7),ZZ.
             03 TLP-FILLER          PIC X.
2            03 TLP-COTIS           PIC -Z(7),ZZ.
             03 TLP-FILLER          PIC X.
3            03 TLP-ABAT-TOT        PIC -Z(7),ZZ.
             03 TLP-FILLER          PIC X.
4            03 TLP-EXEMPTION       PIC -Z(7),ZZ.
             03 TLP-FILLER          PIC X.
5            03 TLP-IMPOSABLE       PIC -Z(7),ZZ.
             03 TLP-FILLER          PIC X.
6            03 TLP-IMPOTS          PIC -Z(7),ZZ.
             03 TLP-FILLER          PIC X.
7            03 TLP-DECOMPTE        PIC -Z(7),ZZ.
             03 TLP-FILLER          PIC X.
8            03 TLP-COTIS-DEP       PIC -Z(7),ZZ.
             03 TLP-FILLER          PIC X.
9            03 TLP-NET             PIC -Z(7),ZZ.
             03 TLP-FILLER          PIC X.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  PRECISION             PIC 9 VALUE 1.
       01  TXT-RECORD.
           02 TXT-PERS            PIC X(6) VALUE "PERSON".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-NOM             PIC X(50) VALUE "NOM - PRENOM".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-MATR            PIC X(13) VALUE "MATRICULE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-CLASSE          PIC X(6) VALUE "CLASSE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-TAUX            PIC X(5) VALUE "TAUX ".
           02 TXT-FILLER          PIC X VALUE ";".

           02 TXT-ANNEE           PIC XXXXX VALUE "ANNEE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-MOIS            PIC XXXX VALUE "MOIS".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-MOIS            PIC XXXX VALUE "MOIS".
           02 TXT-FILLER          PIC X VALUE ";".

           02 TXT-SALAIRE.
1            03 TXT-BRUT            PIC X(11) VALUE "BRUT".
             03 TXT-FILLER          PIC X VALUE ";".
2            03 TXT-COTIS           PIC X(11) VALUE "COTISATION".
             03 TXT-FILLER          PIC X VALUE ";".
3            03 TXT-ABAT-TOT        PIC X(11) VALUE "ABATTEMENT".
             03 TXT-FILLER          PIC X VALUE ";".
4            03 TXT-EXEMPTION       PIC X(11) VALUE "EXEMPTION".
             03 TXT-FILLER          PIC X VALUE ";".
5            03 TXT-IMPOTS          PIC X(11) VALUE "IMPOSABLE".
             03 TXT-FILLER          PIC X VALUE ";".
6            03 TXT-IMPOTS          PIC X(11) VALUE "IMPOT".
             03 TXT-FILLER          PIC X VALUE ";".
7            03 TXT-DECOMPTE        PIC X(11) VALUE "CRED/DECPT".
             03 TXT-FILLER          PIC X VALUE ";".
8            03 TXT-COTIS-DEP-SAL   PIC X(11) VALUE "DEPENDANCE".
             03 TXT-FILLER          PIC X VALUE ";".
9            03 TXT-NET             PIC X(11) VALUE "NET".
             03 TXT-FILLER          PIC X VALUE ";".


      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "PARMOD.REC".
           COPY "LIVRE.REC".
           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       01  NOT-OPEN              PIC 99 VALUE 0.
       01  MOIS-DEBUT            PIC 99 VALUE 1.
       01  MOIS-FIN              PIC 99 VALUE 12.
       01  MOIS-COURANT          PIC 99.

       01 HELP-CUMUL.
          02 HELP-DEBUT            PIC 99.
          02 HELP-FIN              PIC 99.
          02 HELP-MONTANT.
             03 HELP-VAL             PIC S9(8)V99 OCCURS 9.

       01 TOT-CUMUL.
          02 TOT-DEBUT            PIC 99.
          02 TOT-FIN              PIC 99.
          02 TOT-MONTANT.
             03 TOT-VAL             PIC S9(8)V99 OCCURS 9.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TF-TRANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-LPMIN.
       
           INITIALIZE PARMOD-RECORD TOT-CUMUL.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-MOIS TO SAVE-MOIS.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6 THRU 7
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-PATH
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6
           WHEN  7 PERFORM APRES-7
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================


       AVANT-6.
           ACCEPT MOIS-DEBUT
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           ACCEPT MOIS-FIN
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 22 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       

       APRES-6.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
             WHEN 66 ADD 1 TO MOIS-DEBUT
           END-EVALUATE.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-06.

       APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
             WHEN 66 ADD 1 TO MOIS-FIN
           END-EVALUATE.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-07.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TRAITEMENT.
           OPEN OUTPUT TF-TRANS.
           WRITE TLP-RECORD FROM TXT-RECORD.
           INITIALIZE TLP-RECORD.
           INSPECT TLP-RECORD REPLACING ALL " " BY ";".
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           INITIALIZE L-RECORD HELP-CUMUL.
           PERFORM ADD-UP THRU ADD-END VARYING MOIS-COURANT FROM 
           MOIS-DEBUT BY 1 UNTIL MOIS-COURANT > MOIS-FIN.
           IF HELP-VAL(1) > 0
              PERFORM WRITE-FILES.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.
           IF TOT-VAL(1) NOT = 0
              INITIALIZE TLP-RECORD
              MOVE TOT-CUMUL TO HELP-CUMUL
              INSPECT TLP-RECORD REPLACING ALL " " BY ";"
              INITIALIZE TLP-PERS TLP-MATR TLP-ANNEE TLP-MOIS 
              TLP-MOIS-FIN TLP-NOM TLP-CLASSE-I TLP-TAUX
              PERFORM LOAD-VAL VARYING IDX FROM 1 BY 1 UNTIL IDX > 9
              WRITE TLP-RECORD.

       ADD-UP.
           MOVE MOIS-COURANT TO LNK-MOIS L-MOIS.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF STATUT > 0 AND NOT = L-STATUT
              GO ADD-END
           END-IF.
           IF COUT > 0 AND NOT = L-COUT
              GO ADD-END
           END-IF.
           IF L-JOUR-DEBUT > 0
              PERFORM CUMULS
           END-IF.
       ADD-END.
           EXIT.

       WRITE-FILES.
           MOVE REG-PERSON TO TLP-PERS.
           MOVE REG-MATRICULE TO TLP-MATR.
           MOVE LNK-ANNEE  TO TLP-ANNEE.
           MOVE HELP-DEBUT TO TLP-MOIS.
           MOVE HELP-FIN   TO TLP-MOIS-FIN. 
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT  TO TLP-NOM.
           PERFORM LOAD-VAL VARYING IDX FROM 1 BY 1 UNTIL IDX > 9.
           WRITE TLP-RECORD.

       CUMULS.
           MOVE L-CLASSE     TO TLP-CLASSE.
           MOVE L-CLASSE-A   TO TLP-CLASSE-A.
           MOVE L-CLASSE-E   TO TLP-CLASSE-E.
           MOVE L-TAUX       TO TLP-TAUX.   
           IF HELP-DEBUT = 0
              MOVE L-MOIS TO HELP-DEBUT
           END-IF.
           MOVE L-MOIS TO HELP-FIN.
           COMPUTE SH-00 = L-IMPOS(1) + L-IMPOS(5) + L-DC(205).  
      *    ADD  L-IMPOSABLE-BRUT TO HELP-VAL(1) TOT-VAL(1).
           ADD  SH-00 TO HELP-VAL(1) TOT-VAL(1).
           ADD  L-COT-SAL(1) TO HELP-VAL(2) TOT-VAL(2).
           ADD  L-COT-SAL(2) TO HELP-VAL(2) TOT-VAL(2).
           ADD  L-COT-SAL(3) TO HELP-VAL(2) TOT-VAL(2).
           ADD  L-COT-SAL(4) TO HELP-VAL(2) TOT-VAL(2).
           ADD  L-COT-SAL(6) TO HELP-VAL(2) TOT-VAL(2).
           ADD  L-COT-SAL(7) TO HELP-VAL(2) TOT-VAL(2).

           ADD  L-COT-SAL-NP(1) TO HELP-VAL(2) TOT-VAL(2).
           ADD  L-COT-SAL-NP(2) TO HELP-VAL(2) TOT-VAL(2).
           ADD  L-COT-SAL-NP(3) TO HELP-VAL(2) TOT-VAL(2).
           ADD  L-COT-SAL-NP(4) TO HELP-VAL(2) TOT-VAL(2).
           ADD  L-COT-SAL-NP(6) TO HELP-VAL(2) TOT-VAL(2).
           ADD  L-COT-SAL-NP(7) TO HELP-VAL(2) TOT-VAL(2).

           ADD  L-ABATTEMENT TO HELP-VAL(3) TOT-VAL(3).
           ADD  L-SHS-AJUSTE TO HELP-VAL(4) TOT-VAL(4).
           ADD  L-NDF-FRANCHISE TO HELP-VAL(4) TOT-VAL(4).
           ADD  L-ETR-FRANCHISE TO HELP-VAL(4) TOT-VAL(4).
           ADD  L-CON-FRANCHISE TO HELP-VAL(4) TOT-VAL(4).
           ADD  L-HYP-FRANCHISE TO HELP-VAL(4) TOT-VAL(4).
           ADD  L-DOM-FRANCHISE TO HELP-VAL(4) TOT-VAL(4).
           ADD  L-AGR-FRANCHISE TO HELP-VAL(4) TOT-VAL(4).
           ADD  L-IMPOSABLE-NET TO HELP-VAL(5) TOT-VAL(5).
           ADD  L-IMPOT         TO HELP-VAL(6) TOT-VAL(6).
           ADD  L-CAISSE-DEP-SAL TO HELP-VAL(8) TOT-VAL(8).
           ADD  L-SALAIRE-NET   TO HELP-VAL(9) TOT-VAL(9).

           ADD  L-IMPOSABLE-BRUT-NP TO HELP-VAL(1) TOT-VAL(1).
      *    ADD  L-COTISATIONS-NP    TO HELP-VAL(2) TOT-VAL(2).
           ADD  L-ABATTEMENT-NP     TO HELP-VAL(3) TOT-VAL(3).
           ADD  L-FRANCHISE-NP1     TO HELP-VAL(4) TOT-VAL(4).
           ADD  L-FRANCHISE-NP2     TO HELP-VAL(4) TOT-VAL(4).
           ADD  L-IMPOSABLE-NET-NP  TO HELP-VAL(5) TOT-VAL(5).
           ADD  L-IMPOT-NP          TO HELP-VAL(6) TOT-VAL(6).
           ADD  L-CAISSE-DEP-SAL-NP TO HELP-VAL(8) TOT-VAL(8).
           ADD  L-SALAIRE-NET-NP TO HELP-VAL(9) TOT-VAL(9).
           SUBTRACT L-DECOMPTE-IMPOT FROM HELP-VAL(7) TOT-VAL(7).
           SUBTRACT L-CREDIT-IMPOT FROM HELP-VAL(7) TOT-VAL(7).


       LOAD-VAL.
           MOVE HELP-VAL(IDX) TO TLP-VAL(IDX).


       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE MOIS-DEBUT TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 19 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-07.
           MOVE MOIS-FIN TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.
       DIS-HE-IDX.

       AFFICHAGE-ECRAN.
           MOVE  605 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".

