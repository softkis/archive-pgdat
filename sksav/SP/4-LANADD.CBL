      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *  � PROGRAMME 4-LANADD CUMUL LIVRE DE PAIE ANNEE           �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-LANADD.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

           COPY "LIV-ANN.REC".
       01  IDX          PIC 9999 VALUE 0.
       01  EXC-KEY      PIC 9(4) COMP-1.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "LIV-ANN.LNK".

       PROCEDURE DIVISION USING LINK-V REG-RECORD LINK-RECORD.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-LANADD.
       
           INITIALIZE LA-RECORD.
           MOVE FR-KEY      TO LA-FIRME  
           MOVE REG-PERSON  TO LA-PERSON. 
           MOVE LNK-ANNEE-D TO LA-ANNEE. 
           MOVE 13 TO EXC-KEY.
           PERFORM READ-LIVRE-ALL THRU READ-LIVRE-END.
           EXIT PROGRAM.

       READ-LIVRE-ALL.
           CALL "6-LPANN" USING LINK-V REG-RECORD LA-RECORD EXC-KEY.
           IF LA-FIRME  NOT = FR-KEY
           OR LA-PERSON NOT = REG-PERSON
           OR LA-ANNEE  > LNK-LIMITE
              GO READ-LIVRE-END.
           PERFORM CUMUL.
           MOVE 66 TO EXC-KEY.
           GO READ-LIVRE-ALL.
       READ-LIVRE-END.
           EXIT.

       CUMUL.
           PERFORM CUMUL-DC  VARYING IDX FROM 1 BY 1 UNTIL IDX > 300.
           PERFORM CUMUL-UN  VARYING IDX FROM 1 BY 1 UNTIL IDX > 200.
           PERFORM CUMUL-JRS VARYING IDX FROM 1 BY 1 UNTIL IDX > 70.
           PERFORM CUMUL-COT VARYING IDX FROM 1 BY 1 UNTIL IDX > 70.
           PERFORM CUMUL-ABA VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.
           PERFORM CUMUL-IMP VARYING IDX FROM 1 BY 1 UNTIL IDX > 40.
           PERFORM CUMUL-SNO VARYING IDX FROM 1 BY 1 UNTIL IDX > 10.

       CUMUL-DC.
           ADD LA-DC(IDX)        TO LINK-DC(IDX).
       CUMUL-UN.
           ADD LA-UNITE(IDX)     TO LINK-UNITE(IDX).
       CUMUL-JRS.
           ADD LA-JRS(IDX)       TO LINK-JRS(IDX).
       CUMUL-COT.
           ADD LA-COT(IDX)       TO LINK-COT(IDX).
       CUMUL-ABA.
           ADD LA-ABAT(IDX)      TO LINK-ABAT(IDX).
       CUMUL-IMP.
           ADD LA-IMPOS(IDX)     TO LINK-IMPOS(IDX).
       CUMUL-SNO.
           ADD LA-SNOCS(IDX)     TO LINK-SNOCS(IDX).

      
