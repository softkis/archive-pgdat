      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-CRIT MODULE GENERAL LECTURE CRITERES      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-CRIT.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CRITERE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CRITERE.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN  PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "CRITERE.LNK".
           COPY "DOMAINE.REC".

       01  EXC-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD DOM-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CRITERE .
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-CRIT.
       
           IF NOT-OPEN = 0
              OPEN INPUT CRITERE
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CRI-RECORD.
           MOVE DOM-CODE TO CRI-DOMAINE.
           
           EVALUATE EXC-KEY 
               WHEN 66 PERFORM START-1
               READ CRITERE NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN 65 PERFORM START-2
               READ CRITERE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
               WHEN OTHER GO EXIT-3
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF DOM-CODE NOT = CRI-DOMAINE
              GO EXIT-1
           END-IF.
           MOVE CRI-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-3.
           READ CRITERE NO LOCK INVALID GO EXIT-4.
           GO EXIT-2.

       EXIT-4.
           READ CRITERE NO LOCK INVALID INITIALIZE CRI-REC-DET.
           GO EXIT-2.

       START-1.
           START CRITERE KEY > CRI-KEY INVALID GO EXIT-1.
       START-2.
           START CRITERE KEY < CRI-KEY INVALID GO EXIT-1.

