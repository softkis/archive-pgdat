      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-CATSAL SOCIMMO                            �
      *  � SALAIRE HORAIRE MOYEN PAR CATEGORIE                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-CATSAL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK PARMOD-PATH,
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

           SELECT OPTIONAL INTER ASSIGN TO RANDOM, "INTER.CAT",
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is INTER-KEY,
                  STATUS FS-INTER.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

       FD  TF-TRANS
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.

       01  TF-RECORD  PIC X(85).


       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-CODE PIC X(8).
              03 INTER-EQ   PIC 9(4).
           02 INTER-SALAIRE PIC 9(8)V9999.
           02 INTER-EFFECTIF PIC 9999.

           
           

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 4.
       01  PRECISION             PIC 9 VALUE 1.
       01  NOT-OPEN              PIC 9 VALUE 0.


      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "REGISTRE.REC".
           COPY "PERSON.REC".
           COPY "PRESENCE.REC".
           COPY "CARRIERE.REC".
           COPY "EQUIPE.REC".
           COPY "MESSAGE.REC".
           COPY "PARMOD.REC".
           COPY "V-BASES.REC".
           COPY "CCOL.REC".
           COPY "HORSEM.REC".
           COPY "POSE.REC".

           COPY "V-VAR.CPY".

        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  CHOIX                 PIC 99 VALUE 0.
       01  TAUX-1                PIC 999V9999.
       01  TAUX-2                PIC 999V9999.

       01  R-RECORD.
           02 TF-EQUIPE   PIC X(25).
           02 TF-DEL-1    PIC X VALUE ";".
           02 TF-EQ-CODE  PIC X(10).
           02 TF-DEL-2    PIC X VALUE ";".
           02 TF-EFFECTIF PIC Z(10).
           02 TF-DEL-2    PIC X VALUE ";".
           02 TF-SAL-1    PIC Z(6),ZZZZ.
           02 TF-DEL-3    PIC X VALUE ";".
           02 TF-SAL-2    PIC Z(6),ZZZZ.
           02 TF-DEL-4    PIC X VALUE ";".
           02 TF-SAL-3    PIC Z(6),ZZZZ.
           02 TF-DEL-5    PIC X VALUE ";".

       01  TXT-RECORD.
           02 TXT-MOIS     PIC X(25) VALUE SPACES.
           02 TXT-DELIM    PIC X VALUE ";".
           02 TXT-EQ-CODE  PIC X(11) VALUE "CATEGORIE ;".
           02 TXT-EFFECTIF PIC X(11) VALUE "EFFECTIF  ;".
           02 TXT-SAL-1    PIC X(12) VALUE "  BASE     ;".
           02 TXT-SAL-2    PIC Z(3),ZZZZ.
           02 TXT-DEL-2    PIC X(4) VALUE " % ;".
           02 TXT-SAL-3    PIC Z(3),ZZZZ.
           02 TXT-DEL-3    PIC X(4) VALUE " % ;".


       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z3Z4 PIC ZZZ,ZZZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z5 PIC Z(5).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TF-TRANS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-CATSAL.

           DELETE FILE INTER.
           OPEN I-O INTER.
           CALL "0-TODAY" USING TODAY.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           COMPUTE TAUX-1 = PARMOD-PROG-NUMBER-1 / 10000.
           COMPUTE TAUX-2 = PARMOD-PROG-NUMBER-2 / 10000.
           PERFORM AFFICHAGE-ECRAN .
           PERFORM AFFICHAGE-DETAIL.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 4     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-PATH
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  4 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\PATH\FILENAME" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 12 POSITION 35 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

       AVANT-2.
           MOVE TAUX-1 TO HE-Z3Z4.
           ACCEPT HE-Z3Z4
             LINE 19 POSITION 31 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z3Z4 REPLACING ALL "." BY ",".
           MOVE HE-Z3Z4 TO TAUX-1. 
           COMPUTE PARMOD-PROG-NUMBER-1 = TAUX-1 * 10000.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
           PERFORM DIS-HE-02.

       AVANT-3.
           MOVE TAUX-2 TO HE-Z3Z4.
           ACCEPT HE-Z3Z4
             LINE 20 POSITION 31 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z3Z4 REPLACING ALL "." BY ",".
           MOVE HE-Z3Z4 TO TAUX-2. 
           COMPUTE PARMOD-PROG-NUMBER-2 = TAUX-2 * 10000.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".
           PERFORM DIS-HE-03.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       TRAITEMENT.
           INITIALIZE REG-RECORD.
           PERFORM READ-PERSON THRU READ-EXIT.
           PERFORM START-INTER.
    
       READ-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY.
           IF REG-PERSON = 0
              GO READ-EXIT.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF PRES-TOT(LNK-MOIS) = 0
              GO READ-PERSON
           END-IF.
           INITIALIZE CAR-RECORD INTER-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF CAR-EQUIPE = 0
              GO READ-PERSON
           END-IF.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           PERFORM DIS-HE-01.
           MOVE CAR-EQUIPE TO INTER-EQ EQ-NUMBER.
           CALL "6-EQUIPE" USING LINK-V EQ-RECORD FAKE-KEY.
           MOVE EQ-NOM TO INTER-CODE.
           READ INTER INVALID CONTINUE.
           ADD 1 TO INTER-EFFECTIF.
           PERFORM SALAIRE.
           ADD BAS-HORAIRE TO INTER-SALAIRE.
           WRITE INTER-RECORD INVALID REWRITE INTER-RECORD. 
           GO READ-PERSON.
       READ-EXIT.
           EXIT.

       P-LINE.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-TRANS
              MOVE 1 TO NOT-OPEN
              PERFORM ENTETE
              WRITE TF-RECORD FROM TXT-RECORD
           END-IF.
           WRITE TF-RECORD FROM R-RECORD.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
       
       ENTETE.
           MOVE LNK-ANNEE TO TXT-MOIS.
           MOVE LNK-MOIS  TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE 6 TO IDX-4.
           STRING LNK-TEXT DELIMITED BY " " INTO TXT-MOIS
           WITH POINTER IDX-4.
           MOVE TAUX-1 TO TXT-SAL-2.
           MOVE TAUX-2 TO TXT-SAL-3.
                 
       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 27 SIZE 6
           DISPLAY PR-NOM LINE 6 POSITION 47 SIZE 33.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 48 TO COL-IDX.
           IF COL-IDX < 80
              COMPUTE IDX = 80 - COL-IDX
              DISPLAY PR-PRENOM LINE 6 POSITION COL-IDX SIZE IDX LOW.

       DIS-HE-02.
           MOVE TAUX-1 TO HE-Z3Z4.
           DISPLAY HE-Z3Z4 LINE 19 POSITION 31.
       DIS-HE-03.
           MOVE TAUX-2 TO HE-Z3Z4.
           DISPLAY HE-Z3Z4 LINE 20 POSITION 31.
       DIS-HE-END.
           EXIT.


       AFFICHAGE-ECRAN.
           MOVE 705 TO LNK-VAL.
           MOVE "SOCI" TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 4.
           DISPLAY SPACES LINE  4 POSITION 1 SIZE 80.
           DISPLAY SPACES LINE  6 POSITION 1 SIZE 80.
           DISPLAY SPACES LINE  7 POSITION 1 SIZE 80.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE INTER.
           DELETE FILE INTER.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".



       START-INTER.
           INITIALIZE INTER-RECORD.
           START INTER KEY > INTER-KEY INVALID KEY CONTINUE
                NOT INVALID 
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END 
               GO READ-INTER-END
           END-READ.
           MOVE INTER-EQ TO EQ-NUMBER.
           CALL "6-EQUIPE" USING LINK-V EQ-RECORD FAKE-KEY.
           MOVE EQ-TEXTE-IDX(1) TO TF-EQUIPE.
           MOVE EQ-NOM          TO TF-EQ-CODE.
           MOVE INTER-EFFECTIF TO TF-EFFECTIF.
           COMPUTE SH-00 = INTER-SALAIRE / INTER-EFFECTIF + ,00005.
           MOVE SH-00  TO TF-SAL-1.
           COMPUTE SH-00 = INTER-SALAIRE / INTER-EFFECTIF *
           (100 + TAUX-1) / 100 + ,00005.
           MOVE SH-00  TO TF-SAL-2.
           COMPUTE SH-00 = INTER-SALAIRE / INTER-EFFECTIF *
           (100 + TAUX-2) / 100 + ,00005.
           MOVE SH-00  TO TF-SAL-3.
           PERFORM P-LINE.
           GO READ-INTER.
       READ-INTER-END.

       SALAIRE.
           CALL "6-GHJS" USING LINK-V HJS-RECORD  CAR-RECORD PRESENCES.
           MOVE CAR-EQUIPE TO POSE-CODE.
           CALL "6-POSE" USING LINK-V POSE-RECORD FAKE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           CALL "4-SALBAS" USING LINK-V 
                                 CCOL-RECORD
                                 CAR-RECORD
                                 PRESENCES 
                                 POSE-RECORD 
                                 HJS-RECORD 
                                 BASES-REMUNERATION.
