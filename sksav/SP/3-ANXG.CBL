      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-ANXG IMPRESSION ANNEXE G CONVENTIONNES    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-ANXG.
      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM160.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM160.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  CHOIX-MAX             PIC 99 VALUE 6.
       01  PRECISION             PIC 9 VALUE 1.
       01  JOB-STANDARD          PIC X(10) VALUE "160       ".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CONTRAT.REC".
           COPY "PRESENCE.REC".
           COPY "BAREME.REC".
           COPY "FICHE.REC".
           COPY "POINTS.REC".
           COPY "METIER.REC".
           COPY "STATUT.REC".
           COPY "COUT.REC".
           COPY "LIVRE.REC".
           COPY "POCL.REC".
           COPY "V-VAR.CPY".
           COPY "IMPRLOG.REC".
SU         COPY "CSDEF.REC".
SU         COPY "CSDET.REC".
SU         COPY "CODTXT.REC".
           COPY "CODFIX.REC".
           COPY "CODPAIE.REC".

       01  END-NUMBER            PIC  9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SEMESTRE              PIC    99 VALUE 12.
       01  MALADIE               PIC  9(8) VALUE 0.
       01  MALADIE-TOT           PIC  9(8) VALUE 0.
       01  BASE-SALAIRE          PIC  9(8) VALUE 0.
       01  DRAP-FORM             PIC     9 VALUE 0.
       01  FAMILLE               PIC     9 VALUE 0.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(160) OCCURS 45.

           COPY "V-VH00.CPY".

       01  FORM-NAME             PIC X(8) VALUE "ANNEXE.G".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).

       01  INDEMNITES  PIC 9(7)V99.
       01  TOT-POINT   PIC 9(4)V99.

       01  TOTAUX.
           02 DETS OCCURS 2.
              03 HRS   PIC 9(4)V99.
              03 BRUT  PIC 9(7)V99.
              03 IND   PIC 9(7)V99.
              03 MAL   PIC 9(7)V99.
              03 TOT   PIC 9(7)V99.
              03 PP    PIC 9(7)V99.
              03 TOTS  PIC 9(7)V99.
              03 GRAT  PIC 9(7)V99.
              03 PPNP  PIC 9(7)V99.


       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-ANXG.

           MOVE LNK-MOIS TO SAVE-MOIS SEMESTRE.
           IF SAVE-MOIS NOT = 12
              MOVE 6 TO SEMESTRE LNK-MOIS.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0000000025 TO EXC-KFR (1)
                      MOVE 1700000000 TO EXC-KFR (2)
                      MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5
           WHEN  6 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82
              PERFORM END-PROGRAM
           END-IF.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 52 MOVE 27 TO EXC-KEY.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================


       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN  5 PERFORM START-PERSON
                   PERFORM READ-PERSON 
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           MOVE 66 TO EXC-KEY.

       READ-PERSON.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              PERFORM END-PROGRAM.
           IF LNK-COMPETENCE < REG-COMPETENCE
              GO READ-PERSON
           END-IF.
           IF REG-ANCIEN-A = LNK-ANNEE 
           AND REG-ANCIEN-M > SEMESTRE
               GO READ-PERSON.
           MOVE SEMESTRE TO LNK-MOIS.
           INITIALIZE CAR-RECORD TOTAUX.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF COUT > 0 AND NOT = CAR-COUT
              GO READ-PERSON
           END-IF.
           MOVE CAR-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.
           PERFORM LIVRE-ANNEE.
           IF TOTS(2) NOT = 0
              INITIALIZE CON-RECORD FICHE-RECORD
              CALL "1-GCONTR" USING LINK-V REG-RECORD CON-RECORD
              CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD 
              NUL-KEY
              MOVE CAR-METIER TO MET-CODE
              CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY
              PERFORM ENTETE
              PERFORM FULL-PROCESS
           END-IF.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
               GO READ-PERSON
           ELSE
               PERFORM END-PROGRAM.
                
       FULL-PROCESS.
           PERFORM TRANSMET.
           PERFORM DIS-HE-01.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.


       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.


           COPY "XDIS.CPY".
       DIS-HE-END.
           EXIT.

       ENTETE.
           COMPUTE LIN-NUM =  5.
           MOVE 9 TO COL-NUM.
           MOVE LNK-ANNEE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
      * DONNEES FIRME
           COMPUTE LIN-NUM =  1.
           MOVE 20 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

      * DONNEES PERSONNE
           COMPUTE LIN-NUM = 3.
           MOVE 91 TO COL-NUM.
           MOVE PR-NAISS-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 88 TO COL-NUM.
           MOVE PR-NAISS-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 85 TO COL-NUM.
           MOVE PR-NAISS-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           MOVE 34 TO COL-NUM.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 4.
           MOVE  6 TO CAR-NUM.
           MOVE 34 TO COL-NUM.
           MOVE REG-PERSON TO VH-00.
           PERFORM FILL-FORM.
           MOVE  9 TO COL-NUM.
           MOVE COUT-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           IF PR-CODE-SEXE = 1
           EVALUATE FICHE-ETAT-CIVIL
              WHEN "C" MOVE "C굃ibataire" TO ALPHA-TEXTE
              WHEN "M" MOVE "Mari�"       TO ALPHA-TEXTE
              WHEN "D" MOVE "Divorc�"     TO ALPHA-TEXTE
              WHEN "S" MOVE "S굋ar�"      TO ALPHA-TEXTE
              WHEN "V" MOVE "Veuf"        TO ALPHA-TEXTE
           END-EVALUATE
           ELSE
           EVALUATE FICHE-ETAT-CIVIL
              WHEN "C" MOVE "C굃ibataire" TO ALPHA-TEXTE
              WHEN "M" MOVE "Mari괻"      TO ALPHA-TEXTE
              WHEN "D" MOVE "Divorc괻"    TO ALPHA-TEXTE
              WHEN "S" MOVE "S굋ar괻"     TO ALPHA-TEXTE
              WHEN "V" MOVE "Veuve"       TO ALPHA-TEXTE
           END-EVALUATE
           END-IF.
           MOVE 78 TO COL-NUM.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 5.
           MOVE 34 TO COL-NUM.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE CAR-POSITION TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           COMPUTE LIN-NUM = 6.
           MOVE 74 TO COL-NUM.
           MOVE REG-ANCIEN-J TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 77 TO COL-NUM.
           MOVE REG-ANCIEN-M TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 80 TO COL-NUM.
           MOVE REG-ANCIEN-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM = 7.
           IF CON-FIN-A > 0
              MOVE 74 TO COL-NUM
              MOVE  2 TO CAR-NUM
              MOVE CON-FIN-J TO VH-00
              PERFORM FILL-FORM
              MOVE 77 TO COL-NUM
              MOVE  2 TO CAR-NUM
              MOVE CON-FIN-M TO VH-00
              PERFORM FILL-FORM
              MOVE 80 TO COL-NUM
              MOVE  4 TO CAR-NUM
              MOVE CON-FIN-A TO VH-00
              PERFORM FILL-FORM.
           IF CAR-POSTE-FRAIS NOT = 0
              MOVE CAR-POSTE-FRAIS TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              MOVE PC-NOM TO ALPHA-TEXTE
              MOVE 15 TO COL-NUM
              PERFORM FILL-FORM.

      *    DATE EDITION
           COMPUTE LIN-NUM = 1.
           MOVE 120 TO COL-NUM.
           MOVE TODAY-JOUR TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 123 TO COL-NUM.
           MOVE TODAY-MOIS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 126 TO COL-NUM.
           MOVE TODAY-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.


       LIVRE-ANNEE.
           MOVE 1 TO IDX-3.
           IF REG-ANCIEN-A = LNK-ANNEE
              MOVE REG-ANCIEN-M TO IDX-3
           END-IF.
           INITIALIZE DRAP-FORM.
           PERFORM ANNEE VARYING LNK-MOIS FROM IDX-3 BY 1 
                           UNTIL LNK-MOIS > SEMESTRE.
           PERFORM FILL-TOTAL VARYING IDX-2 FROM 1 BY 1 UNTIL IDX-2 > 2.

       ANNEE.
           INITIALIZE L-RECORD.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF LNK-MOIS > 6
              MOVE 3 TO IDX-2
           ELSE
              MOVE 0 TO IDX-2
           END-IF.
           IF L-FIRME NOT = 0
              IF COUNTER = 0 
                 PERFORM READ-IMPRIMANTE 
                 MOVE 1 TO COUNTER
              END-IF
              IF DRAP-FORM = 0
                 PERFORM READ-FORM
                 MOVE 1 TO DRAP-FORM
              END-IF
              PERFORM FILL-NORMAL
              PERFORM CARRIERE
           END-IF.
           MOVE 0 TO VH-00.
           PERFORM MALADIE THRU MALADIE-FIN.

       MALADIE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD NX-KEY.
           IF LNK-MOIS NOT = L-MOIS
           OR L-FIRME = 0
              GO MALADIE-FIN.
           IF L-FLAG-AVANCE = 0
              ADD L-IMPOSABLE-BRUT TO VH-00
           END-IF.
           GO MALADIE.
       MALADIE-FIN.
           MOVE 90 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  1 TO DEC-NUM.
           PERFORM FILL-FORM.
           ADD VH-00 TO MAL(2).
           IF IDX-2 = 0
              ADD VH-00 TO MAL(1).

       FILL-NORMAL.
           COMPUTE LIN-NUM = 11 + LNK-MOIS * 2 + IDX-2.

           MOVE 0 TO SH-00.
           PERFORM DEBIT VARYING IDX-1 FROM 61 BY 1 UNTIL IDX-1 > 80.
           PERFORM DEBIT VARYING IDX-1 FROM 91 BY 1 UNTIL IDX-1 > 99.
           MOVE 120 TO IDX-1.
           PERFORM DEBIT.
           ADD SH-00 TO IND(2).
           IF IDX-2 = 0
              ADD SH-00 TO IND(1).
      *    COMPUTE VH-00 = (L-IMPOSABLE-BRUT - SH-00) / L-UNI-MOY-MOIS.
           MOVE L-SAL-HOR TO VH-00.
           MOVE  60 TO COL-NUM.
           MOVE   2 TO CAR-NUM.
           MOVE   4 TO DEC-NUM.
           PERFORM FILL-FORM.

           IF L-UNI-MOY-MOIS = 0
              COMPUTE L-UNI-MOY-MOIS = CAR-HRS-MOIS / L-UNI-REGUL-THEO
              * L-UNI-REGUL-LIM
           END-IF.


           COMPUTE VH-00 = L-UNI-MOY-MOIS.
           MOVE  68 TO COL-NUM.
           MOVE   4 TO CAR-NUM.
           PERFORM FILL-FORM.
           ADD VH-00 TO HRS(2).
           IF IDX-2 = 0
              ADD VH-00 TO HRS(1).

           COMPUTE VH-00 = L-IMPOSABLE-BRUT - SH-00.
           MOVE  73 TO COL-NUM.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.
           ADD VH-00 TO BRUT(2).
           IF IDX-2 = 0
              ADD VH-00 TO BRUT(1).

           MOVE SH-00 TO VH-00 INDEMNITES.
           MOVE  82 TO COL-NUM.
           MOVE   4 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = L-IMPOSABLE-BRUT.
           MOVE 104 TO COL-NUM.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.
           ADD VH-00 TO TOT(2).
           IF IDX-2 = 0
              ADD VH-00 TO TOT(1).

           MOVE 0 TO SH-00.
           PERFORM DEBIT VARYING IDX-1 FROM 161 BY 1 UNTIL IDX-1 > 170.

           COMPUTE VH-00 = SH-00.
           MOVE 113 TO COL-NUM.
           MOVE   4 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.
           ADD VH-00 TO PP(2).
           IF IDX-2 = 0
              ADD VH-00 TO PP(1).

           COMPUTE VH-00 = L-IMPOSABLE-BRUT + SH-00.
           MOVE 121 TO COL-NUM.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.
           ADD VH-00 TO TOTS(2).
           IF IDX-2 = 0
              ADD VH-00 TO TOTS(1).
           MOVE 0 TO SH-00.
           PERFORM DEBIT VARYING IDX-1 FROM 111 BY 1 UNTIL IDX-1 > 114.
           PERFORM DEBIT VARYING IDX-1 FROM 116 BY 1 UNTIL IDX-1 > 119.
           PERFORM DEBIT VARYING IDX-1 FROM 121 BY 1 UNTIL IDX-1 > 130.
           ADD SH-00 TO GRAT(2) TOT(2) TOTS(2).
           IF IDX-2 = 0
              ADD SH-00 TO GRAT(1).
           MOVE 0 TO SH-00.
           PERFORM DEBIT VARYING IDX-1 FROM 171 BY 1 UNTIL IDX-1 > 180.
           ADD SH-00 TO PPNP(2) PP(2) TOTS(2).
           IF IDX-2 = 0
              ADD SH-00 TO PPNP(1).



       DEBIT.
           ADD L-DEB(IDX-1) TO SH-00.

       FILL-TOTAL.
           IF IDX-2 = 1 
              MOVE 25 TO LIN-NUM 
            ELSE
              MOVE 40 TO LIN-NUM
              PERFORM GRAT
              MOVE 42 TO LIN-NUM
           END-IF.
           COMPUTE VH-00 = HRS(IDX-2).
           MOVE  68 TO COL-NUM.
           MOVE   4 TO CAR-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = BRUT(IDX-2).
           MOVE  73 TO COL-NUM.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = IND(IDX-2).
           MOVE  82 TO COL-NUM.
           MOVE   4 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = MAL(IDX-2).
           MOVE  90 TO COL-NUM.
           MOVE   4 TO CAR-NUM.
           MOVE   1 TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = GRAT(IDX-2).
           MOVE 97 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
      *    PERFORM FILL-FORM.

           COMPUTE VH-00 = TOT(IDX-2).
           MOVE 104 TO COL-NUM.
           MOVE   5 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = PP(IDX-2).
           MOVE 113 TO COL-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE  4 TO CAR-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = TOTS(IDX-2).
           MOVE 121 TO COL-NUM.
           MOVE   5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           PERFORM FILL-FORM.

       GRAT.
           COMPUTE VH-00 = GRAT(IDX-2).
           MOVE 104 TO COL-NUM.
           MOVE  5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = PPNP(IDX-2).
           MOVE 113 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           PERFORM FILL-FORM.

           COMPUTE VH-00 = GRAT(IDX-2) + PPNP(IDX-2).
           MOVE 121 TO COL-NUM.
           MOVE  5 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           PERFORM FILL-FORM.

       CARRIERE.
           INITIALIZE BAR-RECORD CAR-RECORD TOT-POINT PTS-RECORD
           CSP-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF CAR-ECHELON > 0
              MOVE CAR-BAREME TO BAR-BAREME
              MOVE CAR-GRADE  TO BAR-GRADE
              MOVE CAR-ANNEE  TO BAR-ANNEE
              MOVE CAR-MOIS   TO BAR-MOIS
              CALL "6-BAREME" USING LINK-V BAR-RECORD NUL-KEY
              MOVE BAR-DESCRIPTION TO ALPHA-TEXTE
              MOVE  7 TO COL-NUM
              PERFORM FILL-FORM
              MOVE CAR-ECHELON TO VH-00
              MOVE 15 TO COL-NUM
              MOVE  2 TO CAR-NUM
              PERFORM FILL-FORM
              MOVE BAR-POINTS(CAR-ECHELON) TO VH-00 TOT-POINT
              MOVE 19 TO COL-NUM
              MOVE  4 TO CAR-NUM
              PERFORM FILL-FORM.

           IF BAR-POINT > 0
              CALL "6-POINT" USING LINK-V PTS-RECORD
              COMPUTE VH-00 = PTS-I100(BAR-POINT) * MOIS-IDX(LNK-MOIS) 
              MOVE  51 TO COL-NUM
              MOVE   3 TO CAR-NUM
              MOVE   4 TO DEC-NUM
              PERFORM FILL-FORM.

           INITIALIZE CSF-RECORD VH-00 SH-00.
           MOVE FR-KEY TO CSF-FIRME.
           MOVE REG-PERSON TO LNK-PERSON.
           PERFORM READ-FIX THRU READ-FIX-END.
           MOVE 26 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           PERFORM FILL-FORM.
           MOVE SH-00 TO VH-00.
           MOVE 34 TO COL-NUM.
           MOVE  3 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           PERFORM FILL-FORM.

           MOVE TOT-POINT TO VH-00.
           MOVE 42 TO COL-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           PERFORM FILL-FORM.


       READ-FIX.
           CALL "6-CODFIX" USING LINK-V CSF-RECORD NX-KEY.
           IF FR-KEY NOT = CSF-FIRME
           OR CAR-PERSON NOT = CSF-PERSON
              GO READ-FIX-END
           END-IF.
           MOVE CSF-CODE TO CD-NUMBER.
           CALL "6-CSDEF" USING LINK-V CD-RECORD FAKE-KEY.
           INITIALIZE CS-RECORD.
SU         CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
SU         MOVE CD-NUMBER TO CTX-NUMBER.
SU         CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY.
           IF CS-PERMIS(CAR-STATUT) = 0
           OR CS-PERIODE(LNK-MOIS)  = 0
           OR CS-LIVRE(6, 1) > 100
           OR CS-MALADIE = 2
              GO READ-FIX
           END-IF.
           IF CAR-HOR-MEN = 1
              IF CS-PERMIS(10) = 1 
                 GO READ-FIX
              END-IF
           ELSE
              IF CS-PERMIS(10) = 2 
                 GO READ-FIX
              END-IF
           END-IF.
           INSPECT CTX-NOM CONVERTING
           "abcdefghijklmnopqrstuvwxyz굤닀뀑뙎봺뼏꽇쉸�" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZEEEEAIIOOUUUAOUAE".
           MOVE 0 TO COL-IDX
           INSPECT CTX-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "FAMI".
           IF COL-IDX < 30 
              MOVE 1 TO FAMILLE
           ELSE
              MOVE 0 TO FAMILLE
           END-IF.
           MOVE CD-NUMBER  TO CSP-CODE.
           CALL "6-CODPAI" USING LINK-V CSP-RECORD REG-RECORD FAKE-KEY.
           MOVE 0 TO IDX-4.
           PERFORM POINTS VARYING IDX-3 FROM 1 BY 1 UNTIL IDX-3 > 6.
           IF IDX-4 > 0
              PERFORM PREDEF VARYING IDX-3 FROM 1 BY 1 UNTIL IDX-3 = 
              IDX-4.
           GO READ-FIX.
       READ-FIX-END.
           EXIT.

       POINTS.
           IF  CS-PROCEDURE(IDX-3) > 500
           AND CS-PROCEDURE(IDX-3) < 511
              MOVE IDX-3 TO IDX-4
           END-IF.

       PREDEF.
           IF CSP-DONNEE(IDX-3) NOT = 0
              IF FAMILLE = 1
                 ADD CSP-DONNEE(IDX-3) TO VH-00 TOT-POINT
              ELSE
                 ADD CSP-DONNEE(IDX-3) TO SH-00 TOT-POINT
              END-IF
           END-IF.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 45 TO LNK-LINE.
           CALL "L160O" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.


       AFFICHAGE-ECRAN.
           MOVE 1203 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE SAVE-MOIS TO LNK-MOIS.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "L160O" USING LINK-V FORMULAIRE.
           CANCEL "L160O".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XACTION.CPY".

