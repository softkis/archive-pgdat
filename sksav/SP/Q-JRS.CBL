      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME R-JRS              PARAM륳R�                �
      *  � TRANSFERT JOURS  SM3                                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    R-JRS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURS.FDE".
           

       WORKING-STORAGE SECTION.
             

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "REGISTRE.REC".

       01  NOT-OPEN              PIC 9 VALUE 0.
       01  INTER-BIN             PIC 9(4) USAGE IS BINARY VALUE 0.
       01  INTER-MED REDEFINES INTER-BIN.
           02  INTER-MED1        PIC X.
           02  TABS              PIC X.

       01  JOURS-NAME.
           02 JOURS-ID           PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON JOURS.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  program.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-R-JRS.

           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN INPUT JOURS.
             

       CUMUL-JRS.
           INITIALIZE JRS-RECORD IDX-1.
           START JOURS KEY >= JRS-KEY INVALID EXIT PROGRAM
              NOT INVALID
              PERFORM C-C THRU C-C-END.
       C-C.
           READ JOURS NEXT NO LOCK AT END 
              GO C-C-END.
           DISPLAY JRS-KEY LINE 24 POSITION 1.
           MOVE JRS-EXTENSION TO INTER-MED
           MOVE INTER-BIN TO IDX-3
      *    if idx-3 = 0
              DISPLAY IDX-3 LINE 10 POSITION 5
              ADD 1 TO IDX-1
              DISPLAY IDX-1 LINE 10 POSITION 10
              ACCEPT ACTION NO BEEP
      *    END-IF.
           go c-c.
       C-C-END.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".

             