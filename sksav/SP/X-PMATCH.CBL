      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME X-PMATCH MODULE GENERAL MATCHCODE PERSONNES �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    X-PMATCH.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PERSON.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "PERSON.FDE".

       WORKING-STORAGE SECTION.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PERSONNE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-X-PMATCH.
       
           OPEN I-O PERSONNE

           INITIALIZE PR-RECORD.
           START PERSONNE KEY > PR-KEY  INVALID EXIT PROGRAM.
           PERFORM X THRU XX.

       X.
           READ PERSONNE NEXT NO LOCK AT END EXIT PROGRAM.
           INSPECT PR-MATCHCODE CONVERTING
           "abcdefghijklmnopqrstuvwxyz뇘뀈굤닀땶뱮걭뿖쉸�.,;-_'+&*" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZCAAAEEEEIIOOUUUOUAE         ".
           REWRITE PR-RECORD INVALID CONTINUE.
           GO X.
       XX. 
           EXIT.


