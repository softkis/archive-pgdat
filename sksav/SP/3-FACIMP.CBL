      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-FACIMP IMPRESSION IMPAYES CLIENTS         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-FACIMP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".
           COPY "FACTURE.FC".
           COPY "VIRDEB.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".
           COPY "FACTURE.FDE".
           COPY "VIRDEB.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "80       ".
       01  TIRET-TEXTE           PIC X(7) VALUE "_______".
       01  CHOIX-MAX             PIC 99 VALUE 6.
       01  PRECISION             PIC 99 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "IMPRLOG.REC".
           COPY "MESSAGE.REC".
           COPY "ECHEANCE.REC".
           COPY "POCL.REC".

       01  MOIS-DEBUT      PIC 99 VALUE 1.
       01  MOIS-FIN        PIC 99 VALUE 12.
       01  LAST-FIRME      PIC 9(8) VALUE 0.
       01  COMPTEUR        PIC 9(5) VALUE 0.
       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.

       01  PAGES                  PIC 99 VALUE 0.
       01  LICENSES               PIC  9 VALUE 0.

       01  TOTAUX.
           02 TOTAL              PIC 9(8)V99.
           02 TVA                PIC 9(8)V99.
           02 A-PAYER            PIC 9(8)V99.
           02 SOLDES             PIC 9(8)V99.

       01  TOTAUX-G.
           02 TOTAL-G            PIC 9(8)V99.
           02 TVA-G              PIC 9(8)V99.
           02 A-PAYER-G          PIC 9(8)V99.
           02 SOLDES-G           PIC 9(8)V99.

           COPY "V-VH00.CPY".

       01  FORM-NAME             PIC X(9) VALUE "POLIST.F".
       01  FACTURE-NAME.
           02 IDENTITE           PIC X(4) VALUE "A-FA".
           02 FIRME-FACTURE      PIC 9999.

       01  VIR-NAME.
           02 FILLER      PIC X(8) VALUE "A-VIRDEB".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM FACTURE VIRDEB.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-FACIMP.
       
           MOVE LNK-MOIS TO SAVE-MOIS MOIS-FIN.
           CALL "0-TODAY" USING TODAY.
           INITIALIZE TOTAUX-G.
           MOVE FR-KEY TO FIRME-FACTURE.
           OPEN I-O FACTURE.
           OPEN I-O VIRDEB.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0000000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0012000000 TO EXC-KFR(3)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 54 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================
       AVANT-4.
           ACCEPT MOIS-DEBUT
             LINE 14 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-DEBUT
                     PERFORM APRES-4
             WHEN 66 ADD      1 TO   MOIS-DEBUT
                     PERFORM APRES-4
             END-EVALUATE.

       AVANT-5.
           ACCEPT MOIS-FIN
             LINE 15 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-FIN
                     PERFORM APRES-5
             WHEN 66 ADD      1 TO   MOIS-FIN
                     PERFORM APRES-5
             END-EVALUATE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-4.
           IF MOIS-DEBUT < 1 
              MOVE 1 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > 12
              MOVE 12 TO MOIS-DEBUT
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN.
           PERFORM DIS-HE-04.
           PERFORM DIS-HE-05.

       APRES-5.
           IF MOIS-FIN < 1 
              MOVE 1 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-FIN > 12
              MOVE 12 TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-DEBUT > MOIS-FIN
              MOVE MOIS-DEBUT TO MOIS-FIN
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-05.
       
       APRES-DEC.
           MOVE "N" TO LNK-YN.
           MOVE 0 TO LNK-VAL.
           IF EXC-KEY = 12 
              MOVE 6 TO EXC-KEY LICENSES
           END-IF.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM START-CLIENT
                  PERFORM READ-CLIENT THRU READ-EXIT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-CLIENT.
           MOVE FR-KEY TO PC-FIRME PC-FIRME-A.
           IF PC-NUMBER > 0 
              SUBTRACT 1 FROM PC-NUMBER.
           MOVE PC-NUMBER TO PC-NUMBER-A.
           IF A-N = "A"
              MOVE 99999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-CLIENT.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-POCL.
           PERFORM DIS-HE-01.
           IF PC-NUMBER = 0
           OR PC-NUMBER > END-NUMBER
           OR PC-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           IF PC-FIN-A > 0
              CALL "6-LIC" USING LINK-V PC-RECORD "N" WR-KEY.
           INITIALIZE TOTAUX.
           PERFORM TOTAL-FACTURE THRU TOTAL-FACTURE-END.
           IF  PC-NUMBER    < END-NUMBER
           AND PC-MATCHCODE < END-MATCHCODE
              GO READ-CLIENT
           END-IF.
       READ-EXIT.

       NEXT-MESSAGES.
           CALL "6-MESS" USING LINK-V MS-RECORD EXC-KEY.

       NEXT-POCL.
           CALL "6-POCL" USING LINK-V PC-RECORD A-N EXC-KEY.
           IF PC-NUMBER > 0
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PC-DATE-FIN = 0
                    CONTINUE
                 ELSE
                    IF PC-FIN-A < TODAY-ANNEE
                       GO NEXT-POCL
                    END-IF
                 END-IF
              END-IF
           END-IF.
 

       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE PC-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 6 POSITION 25.
           DISPLAY PC-NOM LINE 6 POSITION 47 SIZE 33.
           IF A-N = "N"
              DISPLAY SPACES LINE 6 POSITION 35 SIZE 10.
       DIS-HE-04.
           DISPLAY MOIS-DEBUT LINE 14 POSITION 31.
           MOVE MOIS-DEBUT TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 14351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-05.
           DISPLAY MOIS-FIN LINE 15 POSITION 31.
           MOVE MOIS-FIN TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE 15351500 TO LNK-POSITION
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1601 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.


       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF COUNTER > 0
              IF LIN-IDX < LIN-NUM
                 PERFORM TRANSMET
              END-IF
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE
              CANCEL "P080"
              CLOSE FACTURE.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXP".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".

       TOTAL-FACTURE.
           INITIALIZE FAC-RECORD.
           MOVE PC-NUMBER  TO FAC-CLIENT.
           MOVE 1 TO PAGES.
           START FACTURE KEY > FAC-KEY-2 INVALID
              PERFORM READ-END
              GO TOTAL-FACTURE-END.
           PERFORM READ-FACTURE THRU READ-END.
       TOTAL-FACTURE-END.
           EXIT.

       READ-FACTURE.
           READ FACTURE NEXT NO LOCK AT END 
              GO READ-END.
           IF PC-NUMBER NOT = FAC-CLIENT
              GO READ-END.
           IF LNK-ANNEE < FAC-ANNEE
              GO READ-FACTURE.
           IF LNK-ANNEE > FAC-ANNEE
              GO READ-FACTURE.
           IF MOIS-DEBUT > FAC-MOIS 
              GO READ-FACTURE.
           IF MOIS-FIN < FAC-MOIS 
              GO READ-FACTURE.
           MOVE FAC-A-PAYER TO SH-00.
           PERFORM TOTAL-VIREMENT THRU TOTAL-VIREMENT-END.
           IF SH-00 > 0
              PERFORM FULL-PROCESS.
           GO READ-FACTURE.
       READ-END.
           IF PC-NUMBER = LAST-FIRME
      *       ADD  1 TO LIN-NUM
              MOVE 3 TO COL-NUM
              PERFORM TIRET UNTIL COL-NUM > 75
              ADD 1 TO LIN-NUM.

       FULL-PROCESS.
           IF LICENSES NOT = 0
              MOVE FAC-NUMBER TO PC-FLAG-10
              CALL "6-LIC" USING LINK-V PC-RECORD "N" WR-KEY.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM >= 64
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM LAST-FIRME
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              PERFORM PAGE-DATE 
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           IF PC-NUMBER NOT = LAST-FIRME
              PERFORM DONNEES-FIRME
              MOVE PC-NUMBER TO LAST-FIRME.
           PERFORM DETAILS.

       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       DETAILS.
           ADD 1 TO COMPTEUR.
           MOVE COMPTEUR TO VH-00.
           MOVE  5 TO CAR-NUM.
           MOVE  3 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FAC-NUMBER TO VH-00.
           MOVE  8 TO CAR-NUM.
           MOVE  10 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FAC-EDIT-A TO HE-AA.
           MOVE FAC-EDIT-M TO HE-MM.
           MOVE FAC-EDIT-J TO HE-JJ.
           MOVE HE-DATE TO ALPHA-TEXTE.
           MOVE 25 TO COL-NUM.
           PERFORM FILL-FORM.

            
           IF FAC-ECHEANCE = 0
           AND FAC-EDIT > 0
              MOVE PC-ECHEANCE TO ECH-CODE
              CALL "6-ECHEA" USING LINK-V ECH-RECORD FAKE-KEY
              MOVE FAC-EDIT TO FAC-ECHEANCE
              IF ECH-FIN-MOIS = 1
                 MOVE MOIS-JRS(FAC-EDIT-M) TO FAC-ECHEA-M
              END-IF
              ADD ECH-JOURS TO FAC-ECHEA-J
              IF FAC-ECHEA-J > MOIS-JRS(FAC-EDIT-M)
                 SUBTRACT MOIS-JRS(FAC-EDIT-M) FROM FAC-ECHEA-J
                 ADD 1 TO FAC-ECHEA-M
              END-IF
              ADD ECH-MOIS TO FAC-ECHEA-M
              IF FAC-ECHEA-M > 12
                 SUBTRACT 12 FROM FAC-ECHEA-M
                 ADD 1 TO FAC-ECHEA-A
              END-IF
              REWRITE FAC-RECORD INVALID CONTINUE
           END-IF.

           IF FAC-MOIS = 0
              MOVE FAC-EDIT-M TO FAC-MOIS
              MOVE FAC-EDIT-A TO FAC-ANNEE
              REWRITE FAC-RECORD INVALID CONTINUE
           END-IF.

           MOVE FAC-TOTAL TO VH-00.
           ADD  FAC-TOTAL TO TOTAL TOTAL-G.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 40 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FAC-TVA(1) TO VH-00.
           ADD  FAC-TVA(1) TO TVA TVA-G.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE FAC-A-PAYER TO VH-00.
           ADD  FAC-A-PAYER TO A-PAYER A-PAYER-G.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 60 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE SH-00 TO VH-00.
           ADD  SH-00 TO SOLDES SOLDES-G.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 70 TO COL-NUM.
           PERFORM FILL-FORM.

           ADD 1 TO LIN-NUM.
           IF FAC-TOTAL NOT = TOTAL
              PERFORM TOTAUX
              ADD 1 TO LIN-NUM.
           PERFORM TOTAL-G.

       TOTAUX.
           MOVE TOTAL TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 40 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE TVA   TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE A-PAYER TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 60 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE SOLDES TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 70 TO COL-NUM.
           PERFORM FILL-FORM.

       TOTAL-G.
           MOVE LIN-NUM TO IDX-1.
           MOVE 66 TO LIN-NUM.

           MOVE TOTAL-G TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 40 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE TVA-G TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 50 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE A-PAYER-G TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 60 TO COL-NUM.
           PERFORM FILL-FORM.

           MOVE SOLDES-G TO VH-00.
           MOVE  6 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           MOVE 70 TO COL-NUM.
           PERFORM FILL-FORM.


           MOVE IDX-1 TO LIN-NUM.

       PAGE-DATE.
           CALL "0-TODAY" USING TODAY.
           ADD 1 TO PAGE-NUMBER.
           MOVE  3 TO CAR-NUM.
           MOVE  4 TO LIN-NUM.
           MOVE  75 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 35 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 38 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 41 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

           MOVE 13 TO COL-NUM.
           MOVE FR-NOM TO ALPHA-TEXTE.
           MOVE 2 TO LIN-NUM.
           PERFORM FILL-FORM.

       DONNEES-FIRME.
           MOVE  0 TO DEC-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE  4 TO COL-NUM.
           MOVE PC-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE PC-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 2 TO COL-NUM.
           MOVE "/" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 2 TO COL-NUM.
           MOVE PC-PHONE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 2 TO COL-NUM.
           MOVE "/" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 2 TO COL-NUM.
           MOVE PC-FAX TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.

       TOTAL-VIREMENT.
           INITIALIZE VIR-RECORD.
           MOVE FR-KEY  TO VIR-FIRME.
           MOVE PC-NUMBER TO VIR-DEBIT.
           MOVE FAC-NUMBER  TO VIR-PIECE.
           START VIRDEB KEY >= VIR-KEY INVALID 
                GO TOTAL-VIREMENT-END.
           PERFORM READ-VIREM THRU READ-VIR-END.
       TOTAL-VIREMENT-END.

       READ-VIREM.
           READ VIRDEB NEXT AT END 
              GO READ-VIR-END.
           IF FR-KEY     NOT = VIR-FIRME
           OR PC-NUMBER  NOT = VIR-DEBIT
           OR FAC-NUMBER NOT = VIR-PIECE
              GO READ-VIR-END.
           IF VIR-PAYEMENT = 0
              MOVE VIR-ESCOMPTE TO VIR-PAYEMENT
              MOVE 0 TO VIR-ESCOMPTE 
              REWRITE VIR-RECORD INVALID CONTINUE END-REWRITE
           END-IF.
           SUBTRACT VIR-PAYEMENT FROM SH-00.
           SUBTRACT VIR-ESCOMPTE FROM SH-00.
           GO READ-VIREM.
       READ-VIR-END.
           EXIT.
