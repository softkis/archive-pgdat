      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-PRIX FORMULES FACTURATION                 �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-PRIX.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  SAL-ACT              PIC S9(8)V9(5).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02  SAL-INT          PIC S9(8).
           02  SAL-DEC          PIC V9(5).
       01  SH-00                PIC S9(8)V9(5).
       01  IDX-1                PIC 9(4).
       01  ARRONDI              PIC 9.
       01  ACTION               PIC 9.
       01  ESC-KEY              PIC 9(4) COMP-1.

       01  H-Z.
           03 ZZ-05          PIC Z(8),Z(5).
       01  H-Z0 REDEFINES H-Z.
           03 ZZ-00          PIC Z(8).
           03 FILLER         PIC Z(6).
       01  H-Z1 REDEFINES H-Z.
           03 ZZ-01          PIC Z(8),Z.
           03 FILLER         PIC Z(4).
       01  H-Z2 REDEFINES H-Z.
           03 ZZ-02          PIC Z(8),ZZ.
           03 FILLER         PIC ZZZ.
       01  H-Z3 REDEFINES H-Z.
           03 ZZ-03          PIC Z(8),ZZZ.
           03 FILLER         PIC ZZ.
       01  H-Z4 REDEFINES H-Z.
           03 ZZ-04          PIC Z(8),ZZZZ.
           03 FILLER         PIC Z.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "FACTLINE.REC".
           COPY "ARTICLE.REC".

       PROCEDURE DIVISION USING LINK-V ART-RECORD FAL-RECORD.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-PRIX.
       
       BASES-SALAIRE.
             MOVE 0 TO ARRONDI SH-00 SAL-ACT.

       FORMULES. 
           EVALUATE ART-FORMULE(LNK-NUM)
             WHEN 1 COMPUTE SAL-ACT = FAL-UNITAIRE * FAL-UNITE
             WHEN 2 COMPUTE SAL-ACT = FAL-UNITAIRE * FAL-UNITE / 100
             WHEN 3 COMPUTE SAL-ACT = FAL-UNITAIRE * FAL-UNITE 
                                    * FAL-POURCENT 
             WHEN 4 COMPUTE SAL-ACT = FAL-UNITAIRE * FAL-UNITE 
                                    * FAL-POURCENT / 100   
             WHEN 5 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-POURCENT
             WHEN 6 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-POURCENT / 100
             WHEN 7 COMPUTE SAL-ACT = FAL-DONNEE-1 * MOIS-IDX(LNK-MOIS)
             WHEN 8 COMPUTE SAL-ACT = FAL-DONNEE-2 * MOIS-IDX(LNK-MOIS)
             WHEN 9 COMPUTE SAL-ACT = FAL-POURCENT * MOIS-IDX(LNK-MOIS)
             WHEN 10 COMPUTE SAL-ACT = FAL-UNITAIRE * FAL-UNITE
                     IF SAL-ACT = 0
                        MOVE FAL-TOTAL TO SAL-ACT 
                     END-IF
             WHEN 11 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-UNITAIRE
             WHEN 12 COMPUTE SAL-ACT = FAL-DONNEE-2 * FAL-UNITAIRE
             WHEN 13 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-DONNEE-2
             WHEN 14 COMPUTE SAL-ACT = FAL-DONNEE-1 / FAL-DONNEE-2
             WHEN 15 COMPUTE SAL-ACT = FAL-DONNEE-1 + FAL-UNITE
             WHEN 16 COMPUTE SAL-ACT = FAL-DONNEE-2 + FAL-UNITE
             WHEN 17 COMPUTE SAL-ACT = FAL-DONNEE-1 - FAL-UNITE
             WHEN 18 COMPUTE SAL-ACT = FAL-DONNEE-2 - FAL-UNITE
             WHEN 19 COMPUTE SAL-ACT = FAL-UNITE + FAL-UNITAIRE 
                     MOVE 1 TO ARRONDI
             WHEN 20 COMPUTE SAL-ACT = FAL-UNITE - FAL-UNITAIRE 
                     MOVE 1 TO ARRONDI

             WHEN 21 COMPUTE SAL-ACT = FAL-DONNEE-1 + FAL-DONNEE-2
                     MOVE 1 TO ARRONDI
             WHEN 22 COMPUTE SAL-ACT = FAL-DONNEE-1 - FAL-DONNEE-2
                     MOVE 1 TO ARRONDI
             WHEN 23 COMPUTE SAL-ACT = FAL-DONNEE-2 - FAL-DONNEE-1
                     MOVE 1 TO ARRONDI
             WHEN 24 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-DONNEE-2
             WHEN 25 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-POURCENT
             WHEN 26 COMPUTE SAL-ACT = FAL-DONNEE-2 * FAL-POURCENT
             WHEN 27 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-DONNEE-2
                                     * FAL-POURCENT 
             WHEN 28 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-DONNEE-2
                                     * FAL-POURCENT / 100
             WHEN 29 COMPUTE SAL-ACT = FAL-DONNEE-1 / FAL-DONNEE-2
                     MOVE 1 TO ARRONDI
             WHEN 30 COMPUTE SAL-ACT = FAL-DONNEE-2 / FAL-DONNEE-1
                     MOVE 1 TO ARRONDI
             WHEN 31 COMPUTE SAL-ACT = FAL-DONNEE-1 / FAL-POURCENT
             WHEN 32 COMPUTE SAL-ACT = FAL-DONNEE-2 / FAL-POURCENT
             WHEN 33 COMPUTE SAL-ACT = FAL-DONNEE-2 * FAL-POURCENT / 10
             WHEN 34 COMPUTE SAL-ACT = FAL-DONNEE-2 * FAL-POURCENT / 100
             WHEN 35 COMPUTE SAL-ACT = FAL-DONNEE-2 * FAL-POURCENT / 1000
             WHEN 36 COMPUTE SAL-ACT = FAL-DONNEE-2 * FAL-POURCENT 
                                     / 1000
             WHEN 37 COMPUTE SAL-ACT = FAL-DONNEE-1 / FAL-DONNEE-2
                                     * FAL-POURCENT 
             WHEN 38 COMPUTE SAL-ACT = FAL-DONNEE-2 / FAL-DONNEE-1
                                     * FAL-POURCENT 
             WHEN 39 COMPUTE SAL-ACT = FAL-DONNEE-1 / FAL-DONNEE-2
                                     * FAL-POURCENT / 100
             WHEN 40 COMPUTE SAL-ACT = FAL-DONNEE-2 / FAL-DONNEE-1
                                     * FAL-POURCENT / 100
             WHEN 41 MOVE FAL-UNITAIRE TO SAL-ACT
                     IF SAL-ACT > FAL-DONNEE-1 
                        MOVE FAL-DONNEE-1 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 42 MOVE FAL-UNITAIRE TO SAL-ACT
                     IF SAL-ACT > FAL-DONNEE-2 
                        MOVE FAL-DONNEE-2 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 43 MOVE FAL-UNITAIRE TO SAL-ACT
                     IF SAL-ACT < FAL-DONNEE-1 
                        MOVE FAL-DONNEE-1 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 44 MOVE FAL-UNITAIRE TO SAL-ACT
                     IF SAL-ACT < FAL-DONNEE-2 
                        MOVE FAL-DONNEE-2 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 45 COMPUTE SAL-ACT = FAL-UNITE * FAL-POURCENT / 100   
             WHEN 46 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-DONNEE-2
                                     / FAL-POURCENT 
             WHEN 47 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-DONNEE-2
                                     / FAL-POURCENT / 100

             WHEN 48 COMPUTE SAL-ACT = FAL-DONNEE-1 * MOIS-IDX(LNK-MOIS)
             WHEN 49 COMPUTE SAL-ACT = FAL-DONNEE-2 * MOIS-IDX(LNK-MOIS)
             WHEN 50 COMPUTE SAL-ACT = FAL-POURCENT * MOIS-IDX(LNK-MOIS)
             WHEN 61 COMPUTE SAL-ACT = FAL-DONNEE-1 + FAL-DONNEE-2
                     MOVE 1 TO ARRONDI
             WHEN 62 COMPUTE SAL-ACT = FAL-DONNEE-1 - FAL-DONNEE-2
                     MOVE 1 TO ARRONDI
             WHEN 63 COMPUTE SAL-ACT = FAL-DONNEE-2 - FAL-DONNEE-1
                     MOVE 1 TO ARRONDI
             WHEN 64 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-DONNEE-2
             WHEN 65 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-DONNEE-2
                                     * FAL-POURCENT 
             WHEN 66 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-DONNEE-2
                                     * FAL-POURCENT / 100

             WHEN 67 COMPUTE SAL-ACT = FAL-DONNEE-1 / FAL-DONNEE-2
             WHEN 68 COMPUTE SAL-ACT = FAL-DONNEE-2 / FAL-DONNEE-1

             WHEN 69 COMPUTE SAL-ACT = FAL-DONNEE-1 / FAL-DONNEE-2
                                     * FAL-POURCENT 
             WHEN 70 COMPUTE SAL-ACT = FAL-DONNEE-2 / FAL-DONNEE-1
                                     * FAL-POURCENT 
             WHEN 71 COMPUTE SAL-ACT = FAL-DONNEE-1 / FAL-DONNEE-2
                                     * FAL-POURCENT / 100
             WHEN 72 COMPUTE SAL-ACT = FAL-DONNEE-2 / FAL-DONNEE-1
                                     * FAL-POURCENT / 100

             WHEN 75 MOVE FAL-UNITE TO SAL-ACT
                     IF SAL-ACT > FAL-DONNEE-1 
                        MOVE FAL-DONNEE-1 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 76 MOVE FAL-UNITE TO SAL-ACT
                     IF SAL-ACT > FAL-DONNEE-2 
                        MOVE FAL-DONNEE-2 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 77 MOVE FAL-UNITE TO SAL-ACT
                     IF SAL-ACT < FAL-DONNEE-1 
                        MOVE FAL-DONNEE-1 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 78 MOVE FAL-UNITE TO SAL-ACT
                     IF SAL-ACT < FAL-DONNEE-2 
                        MOVE FAL-DONNEE-2 TO SAL-ACT
                     END-IF
                     MOVE 1 TO ARRONDI
             WHEN 79 COMPUTE SAL-ACT = FAL-TOTAL / FAL-UNITAIRE

             WHEN 81 COMPUTE SAL-ACT = FAL-TOTAL * FAL-POURCENT / 100 
             WHEN 82 COMPUTE SAL-ACT = FAL-TOTAL / FAL-DONNEE-2
             WHEN 83 COMPUTE SAL-ACT = FAL-UNITE * FAL-POURCENT / 100 
             WHEN 84 COMPUTE SAL-ACT = FAL-UNITE / FAL-DONNEE-2
             WHEN 85 COMPUTE SAL-ACT = FAL-UNITAIRE * FAL-POURCENT / 100 
             WHEN 86 COMPUTE SAL-ACT = FAL-UNITAIRE / FAL-DONNEE-2
             WHEN 87 COMPUTE SAL-ACT = FAL-TOTAL * FAL-POURCENT
             WHEN 88 COMPUTE SAL-ACT = FAL-UNITE * FAL-POURCENT
             WHEN 89 COMPUTE SAL-ACT = FAL-UNITAIRE * FAL-POURCENT

             WHEN 90 IF FAL-DONNEE-1 > 0
                        MOVE 0 TO FAL-UNITAIRE  FAL-UNITE FAL-TOTAL
                     END-IF
             WHEN 97 IF FAL-UNITAIRE = 0
                        MOVE 0 TO FAL-UNITE FAL-TOTAL
                     END-IF

             WHEN 99 IF FAL-TOTAL = 0
                        MOVE 0 TO FAL-UNITAIRE  FAL-UNITE
                     END-IF
             WHEN 101 COMPUTE SAL-ACT = FAL-UNITAIRE * FAL-UNITE
             WHEN 102 COMPUTE SAL-ACT = FAL-DONNEE-1 * FAL-UNITAIRE
             WHEN 103 COMPUTE SAL-ACT = FAL-DONNEE-2 * FAL-UNITAIRE
            WHEN 121 COMPUTE SAL-ACT = FAL-DONNEE-1 * MOIS-IDX(LNK-MOIS)
             WHEN 141 IF FAL-DONNEE-1 = 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 142 IF FAL-DONNEE-2 = 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 143 IF FAL-POURCENT = 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 144 IF FAL-DONNEE-1 > 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 145 IF FAL-DONNEE-2 > 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 146 IF FAL-POURCENT > 0 
                        MOVE 1 TO SAL-ACT  
                     END-IF
             WHEN 161 COMPUTE SAL-ACT = (FAL-DONNEE-1 + FAL-UNITE)
                                      * FAL-UNITAIRE

COMET        WHEN 162 COMPUTE SH-00 = FAL-DONNEE-1 - FAL-DONNEE-2
                                    + FAL-UNITE - FAL-UNITAIRE
                                    IF SH-00 < 0
                                       INITIALIZE FAL-VALEURS
                                    ELSE
                                       COMPUTE SAL-ACT = FAL-COMPL-1
                                               * FAL-COMPL-2 / 100
                                       MOVE SAL-ACT TO FAL-UNITAIRE 
                                       COMPUTE SAL-ACT = FAL-UNITAIRE 
                                       * SH-00
                                    END-IF
presta       WHEN 165  MOVE FAL-UNITE TO SAL-ACT 
                       MOVE SAL-INT TO FAL-DONNEE(1)
                       MOVE SAL-DEC TO FAL-DONNEE(2)
             WHEN 181 COMPUTE SAL-ACT = FAL-COMPL-1 * FAL-POURCENT
           END-EVALUATE.
           IF SAL-ACT < 0
              MOVE 0 TO SAL-ACT
           END-IF.

           IF ARRONDI = 0
              PERFORM ARRONDI
           ELSE
              PERFORM REMISE
           END-IF.


       ARRONDI.
           EVALUATE ART-FORMULE(LNK-NUM)
              WHEN   1 THRU  20 MOVE 6 TO IDX-1
              WHEN  21 THRU  60 MOVE 5 TO IDX-1
              WHEN  61 THRU  80 MOVE 4 TO IDX-1
              WHEN  81 THRU  90 MOVE 1 TO IDX-1
              WHEN 101 THRU 120 MOVE 7 TO IDX-1
              WHEN 121 THRU 140 MOVE 2 TO IDX-1
              WHEN 141 THRU 160 MOVE 4 TO IDX-1
              WHEN 161 THRU 180 MOVE 6 TO IDX-1
              WHEN 181 THRU 200 MOVE 8 TO IDX-1
           END-EVALUATE.
           EVALUATE ART-DECIMALES(IDX-1)
              WHEN 0 ADD ,5      TO SAL-ACT
              WHEN 1 ADD ,05     TO SAL-ACT
              WHEN 2 ADD ,005    TO SAL-ACT
              WHEN 3 ADD ,0005   TO SAL-ACT
              WHEN 4 ADD ,00005  TO SAL-ACT
              WHEN 5 ADD ,000005 TO SAL-ACT
           END-EVALUATE.
           MOVE SAL-ACT TO ZZ-04.
           EVALUATE ART-DECIMALES(IDX-1)
              WHEN 0 MOVE ZZ-00 TO SAL-ACT
              WHEN 1 MOVE ZZ-01 TO SAL-ACT
              WHEN 2 MOVE ZZ-02 TO SAL-ACT
              WHEN 3 MOVE ZZ-03 TO SAL-ACT
              WHEN 4 MOVE ZZ-04 TO SAL-ACT
              WHEN 5 MOVE ZZ-05 TO SAL-ACT
           END-EVALUATE.

           PERFORM REMISE.
           
       REMISE.
           EVALUATE ART-FORMULE(LNK-NUM)
              WHEN   1 THRU  20 MOVE SAL-ACT TO FAL-TOTAL
              WHEN  21 THRU  60 MOVE SAL-ACT TO FAL-UNITAIRE
              WHEN  61 THRU  80 MOVE SAL-ACT TO FAL-UNITE
              WHEN  81 THRU  90 MOVE SAL-ACT TO FAL-DONNEE-1
              WHEN 101 THRU 120 MOVE SAL-ACT TO FAL-COMPL-1
              WHEN 121 THRU 140 MOVE SAL-ACT TO FAL-DONNEE-2
              WHEN 141 THRU 160 MOVE SAL-ACT TO FAL-UNITE
              WHEN 161 THRU 180 MOVE SAL-ACT TO FAL-TOTAL
              WHEN 181 THRU 200 MOVE SAL-ACT TO FAL-COMPL-2
           END-EVALUATE.
           MOVE 0 TO LNK-NUM.
           EXIT PROGRAM.

           COPY "Xaction.CPY".


