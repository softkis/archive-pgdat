      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-DOMAIN RECHERCHE DOMAINES                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-DOMAIN.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "DOMAINE.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  ARROW                 PIC X VALUE ">".

       01  CHOIX-A               PIC X(50).
       01  CHOIX-Z               PIC X(20).

       01  COMPTEUR              PIC 99.

       01 HE-REG.
          02 H-R PIC X(20) OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z8 PIC Z(8).
           02 HE-TEMP.
              03 HE-JJ    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-MM    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-AA    PIC ZZZZ.

      * crit둹es 

       01  CRIT-FLAG             PIC 9 VALUE 0.
       01  CRITERES-1            PIC X(50).
       01  HLP-CRIT REDEFINES CRITERES-1.
           03 CRIT               PIC X OCCURS 50.

       01  TST                  PIC X(4).

       01  ITEMS.
           02 ITEM-A OCCURS 10.
              03 ITEM-IDX        PIC 99.
              03 ITEM            PIC X(10).
              03 ITEM-A REDEFINES ITEM.
                 04 ITEM-2       PIC X(2).
                 04 ITEM-F2      PIC X(8).
              03 ITEM-B REDEFINES ITEM.
                 04 ITEM-3       PIC X(3).
                 04 ITEM-F3      PIC X(7).
              03 ITEM-C REDEFINES ITEM.
                 04 ITEM-4       PIC X(4).
                 04 ITEM-F4      PIC X(6).
              03 ITEM-D REDEFINES ITEM.
                 04 ITEM-5       PIC X(5).
                 04 ITEM-F5      PIC X(5).
              03 ITEM-E REDEFINES ITEM.
                 04 ITEM-6       PIC X(6).
                 04 ITEM-F6      PIC X(4).
              03 ITEM-F REDEFINES ITEM.
                 04 ITEM-7       PIC X(7).
                 04 ITEM-F7      PIC X(3).
              03 ITEM-G REDEFINES ITEM.
                 04 ITEM-8       PIC X(8).
                 04 ITEM-F8      PIC X(2).
              03 ITEM-H REDEFINES ITEM.
                 04 ITEM-9       PIC X(9).
                 04 ITEM-F9      PIC X(1).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "DOMAINE.LNK".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       START-DISPLAY SECTION.
             
       START-2-DOMAIN.

           MOVE LNK-A-N TO A-N.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE DOM-RECORD IDX-1 EXC-KEY COMPTEUR CHOIX-A.
           MOVE 4 TO LIN-IDX.
           PERFORM READ-DOMAIN THRU READ-DOMAIN-END.
           IF EXC-KEY = 66 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-DOMAIN.
           MOVE 66 TO EXC-KEY.
           CALL "6-DOMAIN" USING LINK-V DOM-RECORD EXC-KEY.
           IF  CRIT-FLAG = 0
           AND DOM-CODE > CHOIX-Z
              INITIALIZE DOM-CODE
           END-IF.
           IF DOM-CODE = SPACES
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-REG IDX-1
                 GO READ-DOMAIN
              END-IF
              GO READ-DOMAIN-END
           END-IF.
           IF CRIT-FLAG = 1
              PERFORM RECH-CRIT
              IF INPUT-ERROR = 1
                 GO READ-DOMAIN
              END-IF
           END-IF.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 INITIALIZE HE-REG IDX-1
                 GO READ-DOMAIN
              END-IF
              IF CHOIX-A NOT = SPACES
                 GO READ-DOMAIN-END
              END-IF
           END-IF.
           GO READ-DOMAIN.
       READ-DOMAIN-END.
           IF CHOIX-A = SPACES
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82
              AND NOT = 68
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX COMPTEUR IDX-1.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           DISPLAY DOM-CODE  LINE LIN-IDX POSITION 3.
           MOVE DOM-CODE TO H-R(IDX-1).
           DISPLAY DOM-NOM LINE LIN-IDX POSITION 24 SIZE 40.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 4400000000 TO EXC-KFR(1).
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0000530000 TO EXC-KFR(11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE CHOIX-A.
           ACCEPT CHOIX-A
              LINE 3 POSITION 10 SIZE 40
              TAB UPDATE NO BEEP CURSOR  1
              CONTROL "UPPER"
              ON EXCEPTION EXC-KEY CONTINUE
              END-ACCEPT.
           INSPECT CHOIX-A CONVERTING
           "abcdefghijklmnopqrstuvwxyz릠뒋뎲땶뱮걭뾼솞�;:" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZEEEEEAIIOOUUUAOUA  ".
           MOVE CHOIX-A TO CHOIX-Z.
           INSPECT CHOIX-Z CONVERTING " " TO "z".
           DISPLAY CHOIX-A LINE 3 POSITION 10 SIZE 40.
           MOVE 0 TO IDX-1 IDX CRIT-FLAG.
           INSPECT CHOIX-A TALLYING IDX FOR CHARACTERS BEFORE "?".
           IF IDX < 40 
           OR EXC-KEY = 1
              MOVE 1 TO CRIT-FLAG
              PERFORM HOMOGENIZE
              INITIALIZE DOM-CODE
              MOVE 4 TO LIN-IDX
           ELSE
              MOVE 0 TO IDX-1 CRIT-FLAG
              MOVE CHOIX-A TO DOM-CODE
           END-IF.
           IF CRIT-FLAG = 0
              DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF CHOIX-A > SPACES
              INITIALIZE DOM-RECORD
              IF CRIT-FLAG = 0
                 MOVE CHOIX-A TO DOM-CODE
              END-IF
              MOVE 66 TO EXC-KEY
           END-IF.
           INITIALIZE CHOIX-A.
       INTERRUPT-END.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IDX-1.

       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IDX-1 + 4.
           MOVE ">" TO ARROW.
           ACCEPT ARROW
             LINE  LIN-IDX POSITION 1 SIZE 1
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 1.
           IF EXC-KEY = 54
              GO AVANT-ALL-END.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN OTHER
                     MOVE H-R(IDX-1) TO CHOIX-A
                     PERFORM END-PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-R(IDX-1) = SPACES
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.

       RECH-CRIT.
           INSPECT DOM-RECORD CONVERTING
           "abcdefghijklmnopqrstuvwxyz릠뒋뎲땶뱮걭뾼솞�;:" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZEEEEEAIIOOUUUAOUA  ".
           MOVE 0 TO INPUT-ERROR IDX-3.
           PERFORM TEST-CRIT THRU TEST-CRIT-END.

        TEST-CRIT.
           ADD 1 TO IDX-3.
           IF ITEM-IDX(IDX-3) < 2
              GO TEST-CRIT-END
           END-IF.
           MOVE 0 TO IDX.
           EVALUATE ITEM-IDX(IDX-3)
              WHEN 2 INSPECT DOM-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-2(IDX-3)
              WHEN 3 INSPECT DOM-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-3(IDX-3)
              WHEN 4 INSPECT DOM-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-4(IDX-3)
              WHEN 5 INSPECT DOM-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-5(IDX-3)
              WHEN 6 INSPECT DOM-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-6(IDX-3)
              WHEN 7 INSPECT DOM-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-7(IDX-3)
              WHEN 8 INSPECT DOM-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-8(IDX-3)
              WHEN 9 INSPECT DOM-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-9(IDX-3)
              WHEN 10 INSPECT DOM-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM(IDX-3)
           END-EVALUATE.
           IF IDX > 660
              MOVE 1 TO INPUT-ERROR
              GO TEST-CRIT-END
           END-IF.
           IF IDX-3 < 10
              GO TEST-CRIT.
        TEST-CRIT-END.
           EXIT.
                        
       HOMOGENIZE.
           MOVE CHOIX-A TO CRITERES-1.
           INSPECT CRITERES-1 CONVERTING
           "abcdefghijklmnopqrstuvwxyz릠뒋뎲땶뱮걭뾼솞�;:?" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZEEEEEAIIOOUUUAOUA   ".
           MOVE 1 TO IDX-1 IDX-2 IDX.
           PERFORM PROPER-E THRU PROPER-END.

        PROPER-E.
           IF CRIT(IDX) > SPACES 
           AND IDX-1 < 11
              STRING CRIT(IDX) DELIMITED BY SIZE INTO ITEM(IDX-2) WITH 
              POINTER IDX-1 ON OVERFLOW CONTINUE
           ELSE
              IF IDX-1 > 2
                 ADD 1 TO IDX-2
               END-IF
               MOVE 1 TO IDX-1
           END-IF.
           COMPUTE ITEM-IDX(IDX-2) = IDX-1 - 1.
           ADD 1 TO IDX.
           IF IDX < 41
           AND IDX-2 < 11
              GO PROPER-E.
        PROPER-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 2320 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       END-PROGRAM.
           MOVE CHOIX-A TO DOM-CODE.
           IF DOM-CODE NOT = SPACES
              CALL "6-DOMAIN" USING LINK-V DOM-RECORD FAKE-KEY.
           IF DOM-CODE NOT = SPACES
              MOVE DOM-RECORD TO LINK-RECORD.
              
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


