      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME U-CSMOD MODIFICATION CODES SALAIRES SU      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    U-CSMOD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

               
       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�


       01  TYPES PIC 9.
       01  PAG   PIC 9 VALUE 6.


           COPY "V-VAR.CPY".
           COPY "CSDET.REC".
           COPY "CSDEF.REC".
           COPY "CODTXT.REC".
           COPY "CSDET.LNK".

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-U-CSMOD.
           PERFORM AFFICHAGE-ECRAN.
           INITIALIZE CD-RECORD CS-RECORD.
           PERFORM CS-C THRU CS-END.

       CS-C.
           CALL "6-CSDEF" USING LINK-V CD-RECORD NX-KEY.
           IF CD-NUMBER = 0
              EXIT PROGRAM.
           CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD NUL-KEY.
           IF CS-ANNEE >= 2009
              GO CS-C.

           PERFORM PAGES.

           IF PAG = 0
              GO CS-C.

           INITIALIZE TYPES IDX.
           PERFORM ANALYSE THRU ANALYSE-END.
           IF TYPES = 5
           OR TYPES = 0
              GO CS-C.
           IF TYPES = 7
              PERFORM NATURE VARYING IDX FROM 1 BY 1 UNTIL IDX > 20
              GO CS-W
           END-IF.
           IF TYPES = 4
              PERFORM SHS VARYING IDX FROM 1 BY 1 UNTIL IDX > 20
              GO CS-W
           END-IF.
           IF TYPES = 3
              PERFORM HS VARYING IDX FROM 1 BY 1 UNTIL IDX > 20
              GO CS-W
           END-IF.
           IF TYPES = 8
              PERFORM FER VARYING IDX FROM 1 BY 1 UNTIL IDX > 20
           END-IF.
           IF TYPES = 6
              PERFORM CHOM VARYING IDX FROM 1 BY 1 UNTIL IDX > 20
           END-IF.
           IF TYPES = 2
              PERFORM COMPL VARYING IDX FROM 1 BY 1 UNTIL IDX > 20
           END-IF.
           INITIALIZE IDX-1 IDX-2.
           PERFORM TESTS   VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.
           PERFORM NOUVEAU VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.
       CS-W.
           MOVE 2009 TO CS-ANNEE.
           MOVE    1 TO CS-MOIS.
           CALL "6-CS" USING LINK-V CS-RECORD CD-RECORD WR-KEY.
           GO CS-C.


       CS-END.
           EXIT PROGRAM.

       ANALYSE.
           ADD 1 TO IDX.
           IF IDX > 20
              GO ANALYSE-END.
           IF CS-LIVRE(PAG, IDX) > 130
           OR CS-LIVRE(PAG, IDX) < 11
              GO ANALYSE.
           IF CS-LIVRE(PAG, IDX) > 116 AND < 120
              GO ANALYSE-END.
           IF CS-LIVRE(PAG, IDX) = 121
           OR CS-LIVRE(PAG, IDX) = 87
              GO ANALYSE-END.

      *1   02  R-BASE   PIC 9.
      *2   02  R-COMP   PIC 9.
      *3   02  R-HS     PIC 9.
      *4   02  R-SHS    PIC 9.
      *5   02  R-GRAT   PIC 9.
      *6   02  R-CHOM   PIC 9.
      *7   02  R-NAT    PIC 9.
      *8   02  R-FERIE  PIC 9.

           IF CS-LIVRE(PAG, IDX) = 15
           OR CS-LIVRE(PAG, IDX) = 16
           OR CS-LIVRE(PAG, IDX) = 27
              MOVE 8 TO TYPES
              GO ANALYSE-END.

           IF CS-LIVRE(PAG, IDX) > 83 AND < 86
              MOVE 7 TO TYPES
              GO ANALYSE-END.
           IF CS-LIVRE(PAG, IDX) > 40 AND < 44
              MOVE 6 TO TYPES
              GO ANALYSE-END.
           IF CS-LIVRE(PAG, IDX) > 100 AND < 104
              MOVE 6 TO TYPES
              GO ANALYSE-END.
           IF CS-LIVRE(PAG, IDX) > 10 AND < 51
              MOVE 1 TO TYPES
              GO ANALYSE-END.
           IF CS-LIVRE(PAG, IDX) > 87 AND < 95
              MOVE 1 TO TYPES
              GO ANALYSE-END.
           IF CS-LIVRE(PAG, IDX) = 100
              MOVE 1 TO TYPES
              GO ANALYSE-END.
           IF CS-LIVRE(PAG, IDX) = 95
              MOVE 3 TO TYPES
              GO ANALYSE-END.
           IF CS-LIVRE(PAG, IDX) > 95 AND < 99
              MOVE 4 TO TYPES
              GO ANALYSE-END.
           IF CS-LIVRE(PAG, IDX) > 110 AND < 136
              MOVE 5 TO TYPES
              GO ANALYSE-END.
           MOVE 2 TO TYPES.
       ANALYSE-END.
            MOVE CS-NUMBER TO CTX-NUMBER
            CALL "6-CSTXT" USING LINK-V CTX-RECORD FAKE-KEY
            DISPLAY CS-NUMBER LINE 10 POSITION 10
            DISPLAY CTX-NOM LINE 10 POSITION 16 SIZE 50.
            DISPLAY types LINE 11 POSITION 10.

       NATURE.
           IF CS-LIVRE(PAG, IDX) = 701
             MOVE 704 TO CS-LIVRE(PAG, IDX) 
           END-IF.

       SHS.
           IF CS-LIVRE(PAG, IDX) > 700 AND < 710
             MOVE 0 TO CS-LIVRE(PAG, IDX) 
           END-IF.
           IF CS-LIVRE(PAG, IDX) > 400 AND < 411
             MOVE 0 TO CS-LIVRE(PAG, IDX) 
           END-IF.
           IF CS-LIVRE(PAG, IDX) = 602
             MOVE 0 TO CS-LIVRE(PAG, IDX) 
           END-IF.

       FER.
           IF CS-LIVRE(4, IDX) > 700 AND < 710
             MOVE 0 TO CS-LIVRE(4, IDX) 
           END-IF.

       HS.
           IF CS-LIVRE(PAG, IDX) = 701
             MOVE 708 TO CS-LIVRE(PAG, IDX) 
           END-IF.
HRS        IF CS-LIVRE(4, IDX) = 702
             MOVE 709 TO CS-LIVRE(4, IDX) 
           END-IF.
           IF CS-LIVRE(PAG, IDX) > 401 AND < 405
             MOVE 0 TO CS-LIVRE(PAG, IDX) 
           END-IF.
           IF CS-LIVRE(PAG, IDX) > 405 AND < 410
             MOVE 0 TO CS-LIVRE(PAG, IDX) 
           END-IF.
           IF CS-LIVRE(PAG, IDX) = 602
             MOVE 0 TO CS-LIVRE(PAG, IDX) 
           END-IF.

       CHOM.
           IF CS-LIVRE(PAG, IDX) > 402 AND < 405
             MOVE 0 TO CS-LIVRE(PAG, IDX) 
           END-IF.
           IF CS-LIVRE(PAG, IDX) > 405 AND < 411
              MOVE 0 TO CS-LIVRE(PAG, IDX) 
           END-IF.

       COMPL.
           IF CS-LIVRE(PAG, IDX) = 701
             MOVE 707 TO CS-LIVRE(PAG, IDX) 
           END-IF.

       TESTS.
           IF CS-LIVRE(PAG, IDX) = 406
             MOVE 1 TO IDX-1 
           END-IF.
           IF CS-LIVRE(PAG, IDX) = 407
             MOVE 1 TO IDX-2 
           END-IF.
       
       NOUVEAU.
           IF CS-LIVRE(PAG, IDX) = 0
           AND IDX-1 = 0
             MOVE 406 TO CS-LIVRE(PAG, IDX) 
             MOVE 1 TO IDX-1
           END-IF.
           IF CS-LIVRE(PAG, IDX) = 0
           AND IDX-2 = 0
             MOVE 407 TO CS-LIVRE(PAG, IDX) 
             MOVE 1 TO IDX-2
           END-IF.

       AFFICHAGE-ECRAN.
           MOVE 32 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".

       PAGES.
           INITIALIZE PAG IDX-1.
           PERFORM TEST-A THRU TEST-A-END.

       TEST-A.
           INITIALIZE IDX-2.
           ADD 1 TO IDX-1.
           IF IDX-1 > 6
             GO TEST-A-END.
           PERFORM TEST-B THRU TEST-B-END.
           IF PAG > 0
             GO TEST-A-END.
           GO TEST-A.
       TEST-A-END.

       TEST-B.
           ADD 1 TO IDX-2.
           IF IDX-2 > 20
              GO TEST-B-END.
           IF CS-LIVRE(IDX-1, IDX-2) > 10 AND < 131
              MOVE IDX-1 TO PAG
              GO TEST-B-END
           END-IF.
           GO TEST-B.
       TEST-B-END.
