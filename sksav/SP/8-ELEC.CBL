      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-ELEC TRANSFERT ELECTEURS                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-ELEC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           SELECT OPTIONAL TF-TRANS ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-TRANS.

           COPY "TRIPR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

       FD  TRIPR
           LABEL RECORD STANDARD
           DATA RECORD TRIPR-RECORD.
      *  ENREGISTREMENT tri personnel
       01  TRIPR-RECORD.
           02 TRIPR-KEY.
              03 TRIPR-CHOIX.
                 04 TRIPR-ALPHA  PIC X(10).
                 04 TRIPR-NAISS  PIC 9(8).
                 04 TRIPR-SNOCS  PIC 999.
           02 TRIPR-FIRME     PIC 9(6).
           02 TRIPR-PERSON    PIC 9(8).
           02 TRIPR-COUT      PIC 9(8).
           02 TRIPR-NOM       PIC X(50).
           02 TRIPR-METIER    PIC X(30).
           02 TRIPR-COUT-NOM  PIC X(30).
           02 TRIPR-ANCIEN    PIC 9(8).
           02 TRIPR-ELIGIBLE  PIC X.

       FD  TF-TRANS
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.
      *  ENREGISTREMENT FICHIER DE TRANSFERT ASCII LIVRE DE PAIE
      
       01  TF-RECORD.
           02 TF-NOM             PIC X(40).
           02 TF-FILLER          PIC X.
           02 TF-FIRME           PIC Z(6).
           02 TF-FILLER          PIC X.
           02 TF-PERSON          PIC Z(8).
           02 TF-FILLER          PIC X.
           02 TF-METIER          PIC X(40).
           02 TF-FILLER          PIC X.
           02 TF-COUT            PIC Z(8).
           02 TF-FILLER          PIC X.
           02 TF-COUT-NOM        PIC X(30).
           02 TF-FILLER          PIC X.
           02 TF-NAISS           PIC 9(9).
           02 TF-FILLER          PIC X.
           02 TF-ANCIEN          PIC 9(8).
           02 TF-FILLER         PIC X.
           02 TF-ELIGIBLE       PIC X(8).
           02 TF-FILLER         PIC X.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  TXT-RECORD.
           02 TXT-NOM             PIC X(40) VALUE "NOM - PRENOM".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-FIRME           PIC X(6) VALUE "FIRME".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-PERS            PIC X(8) VALUE "PERSONNE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXC-METIER          PIC X(40) VALUE "METIER".
           02 TXC-FILLER          PIC X VALUE ";".
           02 TXT-COUT            PIC X(8) VALUE "CENTRE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-COUT-NOM        PIC X(30) VALUE "DEPARTEMENT".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-MATR            PIC X(9) VALUE "NAISSANCE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-ENTREE          PIC X(8) VALUE "ENTREE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXC-ELIGIBLE        PIC X(9) VALUE "ELIGIBLE".
           02 TXC-FILLER          PIC X VALUE ";".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "V-VAR.CPY".

       01  TRIPR-NAME.
           02 FILLER             PIC XXXX VALUE "IELE".
           02 FIRME-TRIPR        PIC 9999 VALUE 0.
           02 FILLER             PIC X VALUE ".".
           02 USER-TRIPR         PIC XXX.
        
       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       01  NOT-OPEN              PIC 99 VALUE 0.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "PARMOD.REC".

       PROCEDURE DIVISION USING LINK-V PARMOD-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TF-TRANS TRIPR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-ELEC.
       
           MOVE LNK-USER TO USER-TRIPR.
      *    MOVE FR-KEY TO FIRME-TRIPR.
           OPEN INPUT TRIPR.
           INITIALIZE TRIPR-RECORD.
           PERFORM TRAITEMENT.
           PERFORM END-PROGRAM.
           
       TRAITEMENT.
           PERFORM START-TRIPR.
           PERFORM READ-TRIPR THRU READ-EXIT.

       START-TRIPR.
           START TRIPR KEY > TRIPR-KEY INVALID PERFORM END-PROGRAM.

       READ-TRIPR.
           READ TRIPR NEXT NO LOCK AT END PERFORM END-PROGRAM.
           PERFORM WRITE-FILES.
           GO READ-TRIPR.
       READ-EXIT.

       WRITE-FILES.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-TRANS
              MOVE 1 TO NOT-OPEN
              WRITE TF-RECORD FROM TXT-RECORD
              INITIALIZE TF-RECORD
              INSPECT TF-RECORD REPLACING ALL " " BY ";"
           END-IF.
           MOVE TRIPR-NAISS       TO TF-NAISS.
           MOVE TRIPR-FIRME       TO TF-FIRME.
           MOVE TRIPR-PERSON      TO TF-PERSON.
           MOVE TRIPR-COUT        TO TF-COUT.
           MOVE TRIPR-NOM         TO TF-NOM .
           MOVE TRIPR-METIER      TO TF-METIER.
           MOVE TRIPR-COUT-NOM    TO TF-COUT-NOM.
           MOVE TRIPR-ANCIEN      TO TF-ANCIEN.
           MOVE TRIPR-ELIGIBLE    TO TF-ELIGIBLE.
           WRITE TF-RECORD.

       END-PROGRAM.
           IF NOT-OPEN = 1
              CLOSE TRIPR
              DELETE FILE TRIPR
           END-IF.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XACTION.CPY".


