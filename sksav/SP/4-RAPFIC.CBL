      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-RAPFIC MISE A JOUR RAPPORT DES FIRMES     �
      *  � PROPOSITION FACTURATION FICEL                         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  쿞al   Trait  Fonct  Pens   Etud   Grat   Malad  Matern Virem  Vir/r  Acompt Saisie Paiem  Syndic Entr괻 Sortie Embauc  SS굏r �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-RAPFIC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "RAPPORT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "RAPPORT.FDE".

       WORKING-STORAGE SECTION.

           COPY "LIVRE.REC".
           COPY "VIREMENT.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "CONTRAT.REC".
           COPY "V-VAR.CPY".

       01  STEPS                 PIC 99.
       01  S-KEY    PIC 9(4) COMP-1 VALUE 65.
       01  NOT-OPEN              PIC 9 VALUE 0.

       01  RAP-NAME.
           02 FILLER             PIC X(7) VALUE "S-RAPP.".
           02 ANNEE-RAP          PIC 999.

       01  HELP-CUMUL.
SAL        02 HELP-1  PIC S9(4).
TRAIT      02 HELP-2  PIC S9(4).
FONCT      02 HELP-3  PIC S9(4).
PENS       02 HELP-4  PIC S9(4).
ETUD       02 HELP-5  PIC S9(4).
GRAT       02 HELP-6  PIC S9(4).
MALAD      02 HELP-7  PIC S9(4).
MATER      02 HELP-8  PIC S9(4).
VIREM      02 HELP-9  PIC S9(4).
VIRECT     02 HELP-10 PIC S9(4).
ACOMP1     02 HELP-11 PIC S9(4).
SAICES     02 HELP-12 PIC S9(4).
PAYEM      02 HELP-13 PIC S9(4).
SYNDIC     02 HELP-14 PIC S9(4).
ENTREE     02 HELP-15 PIC S9(4).
SORTIE     02 HELP-16 PIC S9(4).
EMBAUC     02 HELP-17 PIC S9(4).
SS-ETR     02 HELP-18 PIC S9(4).
RES        02 HELP-19 PIC S9(4).
RES        02 HELP-20 PIC S9(4).
RES1       02 HELP-21 PIC S9(4).
RES2       02 HELP-22 PIC S9(4).
RES3       02 HELP-23 PIC S9(4).
RES4       02 HELP-24 PIC S9(4).
RES5       02 HELP-25 PIC S9(4).
RES6       02 HELP-26 PIC S9(4).
RES7       02 HELP-27 PIC S9(4).
RES7       02 HELP-28 PIC S9(4).
RES9       02 HELP-29 PIC S9(4).
RES10      02 HELP-30 PIC S9(4).
RES11      02 HELP-31 PIC S9(4).
RES12      02 HELP-32 PIC S9(4).
RES13      02 HELP-33 PIC S9(4).
RES14      02 HELP-34 PIC S9(4).
RES15      02 HELP-35 PIC S9(4).
RES16      02 HELP-36 PIC S9(4).
RES17      02 HELP-37 PIC S9(4).
RES17      02 HELP-38 PIC S9(4).
RES19      02 HELP-39 PIC S9(4).
RES20      02 HELP-40 PIC S9(4).
       01  HELP-CUMUL-R REDEFINES HELP-CUMUL.
           02 HELP-X  PIC S9(4)  OCCURS 40.

      *쿞A  쿒R  쿘A  쿎M  쿣I  쿣R  쿌C  쿞C  쿛A  쿞Y  쿐N  쿞O  쿐M  �   �

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON RAPPORT.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-RAPFIC.
       
           IF FR-FREQ-FACT = 9
              EXIT PROGRAM
           END-IF.
           IF NOT-OPEN = 0
              MOVE LNK-SUFFIX TO ANNEE-RAP
              OPEN I-O   RAPPORT
              MOVE 1 TO NOT-OPEN
           END-IF.
           MOVE LNK-MOIS TO SAVE-MOIS.
           PERFORM TRAITEMENT.
           MOVE SAVE-MOIS TO LNK-MOIS.
           PERFORM MAKE-RAPPORT.
           PERFORM END-PROGRAM.

       TRAITEMENT.
           MOVE 1 TO STEPS.
           IF MENU-BATCH > 0
              MOVE MENU-BATCH TO STEPS.
           EVALUATE FR-FREQ-FACT
              WHEN  1 MOVE  1 TO STEPS
              WHEN  2 MOVE  2 TO STEPS
              WHEN  3 MOVE  3 TO STEPS
              WHEN  6 MOVE  6 TO STEPS
           END-EVALUATE.
           DIVIDE LNK-MOIS BY STEPS GIVING IDX-1 REMAINDER IDX-2.
           IF IDX-1 = 0
           OR IDX-2 NOT = 0
              PERFORM END-PROGRAM.
           COMPUTE IDX-4 = LNK-MOIS - STEPS + 1.
           INITIALIZE HELP-CUMUL.
           PERFORM READ-PERSON THRU READ-EXIT.

       READ-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY.
           IF REG-PERSON > 0
              CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES
           ELSE
              GO READ-EXIT.
           IF PRES-ANNEE = 0
              GO READ-PERSON.
           PERFORM XX VARYING LNK-MOIS FROM IDX-4 BY 1 
                        UNTIL LNK-MOIS > SAVE-MOIS.
           GO READ-PERSON.
       READ-EXIT.
           EXIT.

       XX.
           IF PRES-TOT(LNK-MOIS) NOT = 0
              PERFORM CUMUL-LIVRE 
              PERFORM READ-CONTRAT
              PERFORM START-VIREM.

       CUMUL-LIVRE.
           INITIALIZE L-RECORD SAVE-KEY.
           PERFORM LECTURE-LIVRE THRU LECTURE-LIVRE-END.

       LECTURE-LIVRE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD SAVE-KEY.
           MOVE 66 TO SAVE-KEY.
           IF FR-KEY     NOT = L-FIRME 
           OR LNK-MOIS   NOT = L-MOIS
           OR REG-PERSON NOT = L-PERSON
              GO LECTURE-LIVRE-END.
           EVALUATE L-SUITE 
                WHEN 0 IF L-REGIME = 5
                          ADD 1 TO HELP-5
                       ELSE
                          ADD 1 TO HELP-X(L-STATUT)
                       END-IF
                       IF L-IMPOSABLE-BRUT-NP > L-CONGE-COMP
                          ADD 1 TO HELP-6 
                       END-IF
FICEL                  IF L-ACOMPTE-1 > 0
                          ADD 1 TO HELP-11 
                       END-IF
                       IF L-ACOMPTE-2 > 0
                          ADD 1 TO HELP-11 
                       END-IF
                       IF L-ACOMPTE-GRAT > 0
                          ADD 1 TO HELP-11 
                       END-IF
                       IF L-ACOMPTE-MAL > 0
                          ADD 1 TO HELP-11 
                       END-IF
                       IF L-ACOMPTE-CHEQUE > 0
                          ADD 1 TO HELP-11 
                       END-IF
                       IF L-REGIME > 99
                          ADD 1 TO HELP-18
                       END-IF
                WHEN 1 THRU 9 ADD 1 TO HELP-7  
                WHEN 10       ADD 1 TO HELP-8  
           END-EVALUATE.
           GO LECTURE-LIVRE.
       LECTURE-LIVRE-END.
           EXIT.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

       MAKE-RAPPORT.
           INITIALIZE RAP-RECORD.
           MOVE 1 TO IDX-3.
           MOVE FR-KEY   TO RAP-FIRME.
           MOVE LNK-MOIS TO RAP-MOIS.
           START RAPPORT KEY >= RAP-KEY INVALID PERFORM READ-RAP-END
           NOT INVALID
               PERFORM READ-RAPPORT THRU READ-RAP-END.
           
       READ-RAPPORT.
           READ RAPPORT NEXT AT END 
              GO READ-RAP-END.
           IF FR-KEY   NOT = RAP-FIRME
           OR LNK-MOIS NOT = RAP-MOIS 
              GO READ-RAP-END.
           COMPUTE IDX-3 = RAP-SUITE + 1.
           PERFORM MEM-UP VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > 25.
           GO READ-RAPPORT.
       READ-RAP-END.
           MOVE 0 TO DECISION.
           PERFORM TEST-UP VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > 25.
           IF DECISION = 1
              PERFORM WRITE-RAPPORT.

       MEM-UP.
           SUBTRACT RAP-X(IDX-4) FROM HELP-X(IDX-4).

       TEST-UP.
           IF HELP-X(IDX-4) < 0
              MOVE 0 TO HELP-X(IDX-4).
           IF HELP-X(IDX-4) > 0
              MOVE 1 TO DECISION.
           
           
       WRITE-RAPPORT.
           INITIALIZE RAP-RECORD.
           MOVE FR-KEY     TO RAP-FIRME RAP-FIRME-A.
           MOVE LNK-MOIS   TO RAP-MOIS  RAP-MOIS-A.
           MOVE IDX-3      TO RAP-SUITE RAP-SUITE-A.
           CALL "0-TODAY" USING TODAY
           MOVE TODAY-TIME TO RAP-TIME
           MOVE LNK-USER TO RAP-USER
           MOVE HELP-CUMUL TO RAP-0.
           WRITE RAP-RECORD INVALID REWRITE RAP-RECORD.

       READ-CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           CALL "6-CONTR" USING LINK-V CON-RECORD REG-RECORD S-KEY.
           IF  CON-DEBUT-A = LNK-ANNEE
           AND CON-DEBUT-M = LNK-MOIS
               ADD 1 TO HELP-15 
           END-IF.
           IF  CON-FIN-A = LNK-ANNEE
           AND CON-FIN-M = LNK-MOIS
               ADD 1 TO HELP-16
           END-IF.


       START-VIREM.
           INITIALIZE VIR-RECORD.
           PERFORM READ-VIREM THRU READ-VIR-END.

       READ-VIREM.
           CALL "6-VIREM" USING LINK-V REG-RECORD VIR-RECORD NX-KEY.
           IF FR-KEY     NOT = VIR-FIRME
           OR REG-PERSON NOT = VIR-PERSON
           OR VIR-MOIS   NOT = LNK-MOIS 
              GO READ-VIR-END.
           EVALUATE VIR-TYPE 
              WHEN 0 IF VIR-SUITE = 1
                        ADD 1 TO HELP-9
                     ELSE
                        ADD 1 TO HELP-10
                     END-IF
ACOMPT*       WHEN 1 ADD 1 TO HELP-11
              WHEN 2 THRU 3 ADD 1 TO HELP-12
              WHEN 7 ADD 1 TO HELP-12
              WHEN 9 ADD 1 TO HELP-14
              WHEN OTHER ADD 1 TO HELP-13
           END-EVALUATE.
           GO READ-VIREM.
       READ-VIR-END.
           EXIT.
           