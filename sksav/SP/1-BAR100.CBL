      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-BAR100 BAREMES DE REMUNERATION I 100      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-BAR100.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "BAREME.FC".
           SELECT OPTIONAL BARCOPIE ASSIGN TO RANDOM, "X-BAREME",
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is CPY-KEY,
                  ALTERNATE RECORD KEY is CPY-KEY-1,
                  STATUS FS-HELP.

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "BAREME.FDE".
       FD  BARCOPIE
           LABEL RECORD STANDARD
           DATA RECORD CPY-RECORD.

       01  CPY-RECORD.
           02 CPY-KEY.
              03 CPY-BAREME       PIC X(10).
              03 CPY-GRADE        PIC 999.
              03 CPY-DATE.
                 04 CPY-ANNEE     PIC 9999.
                 04 CPY-MOIS      PIC 99.

           02 CPY-KEY-1.  
              03 CPY-BAREME-1     PIC X(10).
              03 CPY-DATE-1.
                 04 CPY-ANNEE-1   PIC 9999.
                 04 CPY-MOIS-1    PIC 99.
              03 CPY-GRADE-1      PIC 999.

           02 CPY-REC-DET.
              03 CPY-DESCRIPTION  PIC X(10).
              03 CPY-POINT        PIC 9.
              03 CPY-ECHELONS.
                 04 CPY-ECHELON PIC 9(6)V9999 OCCURS 80.
              03 CPY-ECHELONS-100 REDEFINES CPY-ECHELONS.
                 04 CPY-ECHELON-I100 PIC 9(5)V9(5) OCCURS 80.
              03 CPY-ECHELONS-P REDEFINES CPY-ECHELONS.
                 04 CPY-POINTS  PIC 9(10) OCCURS 80.
              03 CPY-STAMP.
                 04 CPY-TIME.
                    05 CPY-ST-ANNEE PIC 9999.
                    05 CPY-ST-MOIS  PIC 99.
                    05 CPY-ST-JOUR  PIC 99.
                    05 CPY-ST-HEURE PIC 99.
                    05 CPY-ST-MIN   PIC 99.
                 04 CPY-USER        PIC X(10).

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE 6.
           02  CHOIX-MAX-2       PIC 99 VALUE 80.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 2.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "INDEX.REC".
           COPY "V-VAR.CPY".
           COPY "BARDEF.REC".

       01  COPIE                 PIC 9(8)V9(4).
       01  SAL-ACT               PIC 9(8)V9(6).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02  SAL-ACT-A         PIC 9(8) .
           02  SAL-ACT-B         PIC 9(6) .

       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-Z3 PIC Z(3).
           02 HE-Z2 PIC Z(2).
           02 HE-Z8 PIC Z(8). 
           02 HE-Z6 PIC Z(6). 
           02 HE-Z6Z5 PIC Z(6),ZZZZZ.
           02 HE-Z7Z3 PIC Z(7),ZZZ. 


       01  H-Z1.
           03 FILLER         PIC Z(7).
           03 ZZ-01          PIC Z,Z(5).
       01  H-Z2 REDEFINES H-Z1.
           03 FILLER         PIC Z(6).
           03 ZZ-02          PIC ZZ,Z(5).
       01  H-Z3 REDEFINES H-Z1.
           03 FILLER         PIC Z(5).
           03 ZZ-03          PIC Z(4),Z(5).
       01  H-Z4 REDEFINES H-Z1.
           03 FILLER         PIC Z(4).
           03 ZZ-04          PIC Z(4),Z(5).
       01  H-Z5 REDEFINES H-Z1.
           03 FILLER         PIC Z(3).
           03 ZZ-05          PIC Z(5),Z(5).
       01  H-Z6 REDEFINES H-Z1.
           03 FILLER         PIC ZZ.
           03 ZZ-06          PIC Z(6),Z(5).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON BAREME.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-BAR100.
       
           OPEN I-O   BAREME.
           INITIALIZE BAR-RECORD.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 
           IF ECRAN-IDX = 1 
           AND INDICE-ZONE < 1
              MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0029000015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0000002416 TO EXC-KFR(1)
           WHEN 3     MOVE 0000002400 TO EXC-KFR(1)
           WHEN 4     MOVE 2600080000 TO EXC-KFR(2)
                      MOVE 2600000000 TO EXC-KFR(4)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14).

           PERFORM DISPLAY-F-KEYS.
           
       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5.

           IF ECRAN-IDX = 2
              PERFORM AVANT-ALL THRU AVANT-ALL-END.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4.

           IF ECRAN-IDX = 1
           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN.

           IF ECRAN-IDX = 1
              IF EXC-KEY = 13 
              AND INPUT-ERROR = 0
                 ADD 1 TO INDICE-ZONE
              END-IF
              IF INDICE-ZONE > CHOIX-MAX(1)
                 ADD 1 TO ECRAN-IDX
                 MOVE 1 TO INDICE-ZONE 
              END-IF
           ELSE
              IF INDICE-ZONE = 0
                 MOVE 1 TO ECRAN-IDX
                 MOVE 4 TO INDICE-ZONE
           END-IF.

           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1. 
           ACCEPT BDF-CODE
             LINE  4 POSITION 15 SIZE 10
             TAB UPDATE NO BEEP CURSOR 1 HIGH
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.            
           ACCEPT BAR-ANNEE
             LINE  5 POSITION 15 SIZE  4
             TAB UPDATE NO BEEP CURSOR  1 HIGH
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3. 
           ACCEPT BAR-MOIS  
             LINE  5 POSITION 31 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1 HIGH
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4. 
           ACCEPT BAR-GRADE
             LINE 6 POSITION 15 SIZE 3
             TAB UPDATE NO BEEP CURSOR  1 HIGH
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-5. 
           ACCEPT BAR-DESCRIPTION
             LINE 6 POSITION 40 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1 HIGH
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-BARDEF" USING LINK-V
                    MOVE LNK-TEXT TO BDF-CODE
                    PERFORM NEXT-BDF
                    PERFORM AFFICHAGE-ECRAN 
           WHEN   2 CALL "2-BARDEF" USING LINK-V BDF-RECORD
                    CANCEL "2-BARDEF"
                    PERFORM AFFICHAGE-ECRAN 
           WHEN OTHER PERFORM NEXT-BDF
           END-EVALUATE.
           IF BDF-CODE NOT = BAR-BAREME
              INITIALIZE BAR-RECORD.
           MOVE BDF-CODE TO BAR-BAREME.
           IF BDF-DIGIT < 1
              MOVE 2 TO BDF-DIGIT
           END-IF.
           MOVE BDF-DIGIT TO BDF-SIZE.
           IF BDF-DECIM > 0
              COMPUTE BDF-SIZE = BDF-DIGIT + BDF-DECIM + 1.
           IF BAR-BAREME = SPACES
           OR BDF-POINTS > 0
              MOVE 1 TO INPUT-ERROR 
           END-IF.
           IF INPUT-ERROR = 0
              AND BAR-GRADE = 0
              PERFORM RECENT THRU RECENT-FIN
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           IF 1950 > BAR-ANNEE
              MOVE 1 TO INPUT-ERROR
              MOVE LNK-ANNEE TO BAR-ANNEE.
           EVALUATE EXC-KEY
           WHEN 4 PERFORM BAR-NEXT
                  PERFORM DIS-E1-01 THRU DIS-E2
                  MOVE 1 TO INPUT-ERROR 
           WHEN 5 PERFORM IMPORT
           END-EVALUATE.

       APRES-3.
           IF BAR-MOIS > 12
              MOVE 12 TO BAR-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF BAR-MOIS < 1
              MOVE 1 TO BAR-MOIS
              MOVE 1 TO INPUT-ERROR.
           EVALUATE EXC-KEY
           WHEN 4 PERFORM BAR-NEXT
                  PERFORM DIS-E1-01 THRU DIS-E2
                  MOVE 1 TO INPUT-ERROR 
           END-EVALUATE.
           IF INPUT-ERROR = 0
              PERFORM INDEX-RECENT.

       APRES-4.
           EVALUATE EXC-KEY
           WHEN 65 PERFORM BAR-PREV
                  PERFORM DIS-E1-01 THRU DIS-E2
                  MOVE 1 TO INPUT-ERROR 
           WHEN 66 PERFORM BAR-SUITE
                  PERFORM DIS-E1-01 THRU DIS-E2
                  MOVE 1 TO INPUT-ERROR 
           WHEN 6 PERFORM CALCULE VARYING IDX FROM 1 BY 1 UNTIL IDX > 80
                  MOVE 1 TO INPUT-ERROR 
           WHEN 8 DELETE BAREME INVALID CONTINUE END-DELETE
                  INITIALIZE BAR-REC-DET BAR-GRADE
           WHEN 16 PERFORM CALCULE VARYING IDX FROM 1 BY 1 UNTIL IDX > 
                   80
                   MOVE 1 TO INPUT-ERROR 
           END-EVALUATE.
           IF BAR-GRADE = SPACES 
           OR BAR-GRADE = 0 
              MOVE 1 TO INPUT-ERROR. 
           IF INPUT-ERROR = 0
              READ BAREME INVALID 
                   INITIALIZE BAR-REC-DET
              END-READ
           END-IF.
           IF  EXC-KEY NOT = 6
           AND EXC-KEY NOT = 16
              PERFORM DIS-E1-01 THRU DIS-E2.



       BAR-PREV.
           MOVE 0 TO NOT-FOUND .
           MOVE BDF-CODE TO BAR-BAREME-1.
           MOVE BAR-DATE TO SAVE-DATE BAR-DATE-1.
           MOVE BAR-GRADE TO BAR-GRADE-1.
           START BAREME KEY < BAR-KEY-1  INVALID KEY
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ BAREME PREVIOUS AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           IF BDF-CODE NOT = BAR-BAREME
           OR SAVE-DATE   NOT = BAR-DATE
              MOVE 1 TO NOT-FOUND.
           IF NOT-FOUND = 1 
              INITIALIZE BAR-RECORD 
              MOVE BDF-CODE TO BAR-BAREME
              MOVE SAVE-DATE   TO BAR-DATE.

       BAR-SUITE.
           MOVE 0 TO NOT-FOUND .
           MOVE BDF-CODE TO BAR-BAREME-1.
           MOVE BAR-DATE TO SAVE-DATE BAR-DATE-1.
           MOVE BAR-GRADE TO BAR-GRADE-1.
           START BAREME KEY > BAR-KEY-1  INVALID KEY
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ BAREME NEXT AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           IF BDF-CODE NOT = BAR-BAREME
           OR SAVE-DATE   NOT = BAR-DATE
              MOVE 1 TO NOT-FOUND.
           IF NOT-FOUND = 1 
              INITIALIZE BAR-RECORD 
              MOVE BDF-CODE TO BAR-BAREME
              MOVE SAVE-DATE   TO BAR-DATE.


       BAR-NEXT.
           MOVE 0 TO NOT-FOUND .
           MOVE BDF-CODE  TO BAR-BAREME-1.
           MOVE BAR-ANNEE TO BAR-ANNEE-1.
           START BAREME KEY > BAR-KEY-1 INVALID
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ BAREME NEXT AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           IF BDF-CODE NOT = BAR-BAREME
              MOVE 1 TO NOT-FOUND.
           IF NOT-FOUND = 1 
              INITIALIZE BAR-RECORD 
              MOVE BDF-CODE TO BAR-BAREME.

       RECENT.
           INITIALIZE BAR-RECORD.
           MOVE BDF-CODE  TO BAR-BAREME BAR-BAREME-1.
           MOVE LNK-ANNEE TO BAR-ANNEE BAR-ANNEE-1.
           MOVE LNK-MOIS  TO BAR-MOIS  BAR-MOIS-1.
           MOVE 999 TO BAR-GRADE-1.
           START BAREME KEY <= BAR-KEY-1 INVALID GO RECENT-FIN.
           READ BAREME PREVIOUS NO LOCK AT END GO RECENT-FIN END-READ
           MOVE 0 TO BAR-GRADE-1.
           START BAREME KEY >= BAR-KEY-1 INVALID GO RECENT-FIN.
           READ BAREME NEXT NO LOCK AT END GO RECENT-FIN END-READ.
       RECENT-FIN.
           PERFORM DIS-E2.

       NEXT-BDF.
           CALL "6-BARDEF" USING LINK-V BDF-RECORD EXC-KEY.
           IF SAVE-KEY > 64
           AND BDF-POINTS > 0
           AND BDF-KEY > SPACES
               GO NEXT-BDF
           END-IF.

       INDEX-RECENT.
           MOVE BAR-ANNEE TO INDEX-ANNEE.
           MOVE BAR-MOIS  TO INDEX-MOIS.
           CALL "6-INDEX" USING LINK-V INDEX-RECORD NUL-KEY.

       AVANT-ALL.
           MOVE 0 TO COPIE.
           MOVE 0100007305 TO EXC-KFR (1).
           MOVE 0000080000 TO EXC-KFR (2).
           MOVE 0000130028 TO EXC-KFR (3).
           MOVE 0000000065 TO EXC-KFR (13).
           MOVE 0052535400 TO EXC-KFR (11).
           MOVE 6600000000 TO EXC-KFR (14).
           IF INDICE-ZONE > 1
              MOVE 0122007305 TO EXC-KFR (1)
           END-IF.
           IF INDICE-ZONE > 2
              MOVE 0122287305 TO EXC-KFR (1)
           END-IF.
           PERFORM DISPLAY-F-KEYS.
           MOVE INDICE-ZONE TO HE-Z2 IDX.
           PERFORM LIN-COL.
           ADD 3 TO COL-IDX.
           IF INDICE-ZONE > 1
              MOVE BAR-ECHELON-I100(IDX-1) TO COPIE
           END-IF.
           DISPLAY HE-Z2 LINE 6 POSITION 70.
           PERFORM ACCEPT-PARAM.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.
           EVALUATE EXC-KEY
              WHEN  1 MOVE 01004000 TO LNK-VAL
                      MOVE INDICE-ZONE TO IDX-3
                      PERFORM HELP-SCREEN
                      PERFORM DIS-E2 VARYING IDX FROM 1 BY 1 
                      UNTIL IDX > 80
                      PERFORM DISPLAY-F-KEYS
                      PERFORM DISPLAY-F-KEYS
                      MOVE IDX-3 TO INDICE-ZONE 
                      GO AVANT-ALL
              WHEN  2 ADD COPIE TO BAR-ECHELON-I100(INDICE-ZONE) 
                      PERFORM DIS-EL
                      ADD 1 TO INDICE-ZONE
              WHEN 3  IF IDX > 2
                         COMPUTE IDX-1 = IDX - 2
                         COMPUTE IDX-2 = IDX - 1
                         COMPUTE BAR-ECHELON-I100(IDX) =
                         BAR-ECHELON-I100(IDX-2) * 2 
                         - BAR-ECHELON-I100(IDX-1)
                      END-IF
                      PERFORM DIS-EL
                      ADD 1 TO INDICE-ZONE
              WHEN  4 ADD 1 TO INDICE-ZONE
              WHEN  5 PERFORM BAR-WRITE
                      MOVE 0 TO INDICE-ZONE 
              WHEN  8 MOVE 0 TO BAR-ECHELON-I100(INDICE-ZONE)
                      PERFORM DIS-EL
                      ADD  1 TO INDICE-ZONE
              WHEN 54 MOVE 0 TO INDICE-ZONE 
              WHEN 13 ADD  1 TO INDICE-ZONE
              WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
              WHEN 52 SUBTRACT 1 FROM INDICE-ZONE
              WHEN 53 ADD  1 TO INDICE-ZONE
              WHEN 65 IF INDICE-ZONE > 16
                         SUBTRACT 16 FROM INDICE-ZONE
                      END-IF
              WHEN 66 IF INDICE-ZONE < 65
                         ADD 16 TO INDICE-ZONE
                      END-IF
           END-EVALUATE.
           IF INDICE-ZONE > 80 
              MOVE 80 TO INDICE-ZONE
           END-IF.
           IF INDICE-ZONE > 0
              GO AVANT-ALL
           END-IF.
       AVANT-ALL-END.
           EXIT.

       ACCEPT-PARAM.
           MOVE BAR-ECHELON-I100(INDICE-ZONE) TO ZZ-06.
           EVALUATE BDF-DIGIT
           WHEN 1 ACCEPT ZZ-01
             LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 2 ACCEPT ZZ-02
             LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           WHEN 3 ACCEPT ZZ-03
             LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 4 ACCEPT ZZ-04
             LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           WHEN 5 ACCEPT ZZ-05
             LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 6 ACCEPT ZZ-06
             LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           END-EVALUATE.
           INSPECT ZZ-06 REPLACING ALL "." BY ",".
           MOVE ZZ-06 TO BAR-ECHELON-I100(INDICE-ZONE) SH-00
           IF EXC-KEY = 4
              COMPUTE BAR-ECHELON-I100(INDICE-ZONE) = 
              BAR-ECHELON-I100(INDICE-ZONE) / INDEX-FACT
              + ,000001
           END-IF.
           PERFORM DIS-EL.

       LIN-COL.
           COMPUTE IDX-1 = IDX - 1.
           DIVIDE IDX-1 BY 16 GIVING COL-IDX REMAINDER LIN-IDX.
           COMPUTE LIN-IDX = LIN-IDX + 7.
           COMPUTE COL-IDX = 1 + (COL-IDX * 16).

       BAR-WRITE.  
           MOVE BDF-CODE TO BAR-BAREME BAR-BAREME-1.
           MOVE BAR-ANNEE  TO BAR-ANNEE-1.
           MOVE BAR-MOIS   TO BAR-MOIS-1.
           MOVE BAR-GRADE  TO BAR-GRADE-1.
           MOVE BDF-POINTS TO BAR-POINT.
           display bar-key-1 line 1 position 1.
           display bar-bareme line 2 position 1.
           WRITE BAR-RECORD INVALID REWRITE BAR-RECORD.

       IMPORT.
           INITIALIZE CPY-RECORD.
           MOVE BDF-CODE TO CPY-BAREME.
           OPEN INPUT BARCOPIE.
           START BARCOPIE KEY > CPY-KEY INVALID EXIT PROGRAM
              NOT INVALID
              PERFORM C-C THRU C-C-END.
       C-C.
           READ BARCOPIE NEXT NO LOCK AT END 
              GO C-C-END.
           IF BDF-CODE NOT = CPY-BAREME
              GO C-C-END.
           MOVE CPY-RECORD TO BAR-RECORD.
           WRITE BAR-RECORD INVALID REWRITE BAR-RECORD.
           GO C-C.
       C-C-END.
           EXIT.

       DIS-E1-01.
           DISPLAY BDF-CODE LINE 4 POSITION 15.
           DISPLAY BDF-NOM  LINE 4 POSITION 30.
       DIS-E1-02.
           MOVE BAR-ANNEE TO HE-Z4.
           DISPLAY HE-Z4 LINE 5 POSITION 15.
       DIS-E1-03.
           MOVE BAR-MOIS TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 5 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 05401200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       DIS-E1-04.
           DISPLAY BAR-GRADE LINE 6 POSITION 15.
       DIS-E1-05.
           DISPLAY BAR-DESCRIPTION LINE 6 POSITION 40.
       DIS-E1-END.
           EXIT.
       
       DIS-E2.
           PERFORM DIS-ECHELON VARYING IDX FROM 1 BY 1 UNTIL IDX > 80.

       DIS-ECHELON.
           PERFORM LIN-COL.
           ADD 3 TO COL-IDX.
           PERFORM DIS-EL.

       DIS-EL.
           MOVE BAR-ECHELON-I100(IDX) TO ZZ-06.
           DISPLAY SPACES LINE LIN-IDX POSITION COL-IDX SIZE 12.
           PERFORM DISPLAY-ZZ.

       DISPLAY-ZZ.
           EVALUATE BDF-DIGIT
           WHEN 1 DISPLAY ZZ-01
                  LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
           WHEN 2 DISPLAY ZZ-02
                  LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
           WHEN 3 DISPLAY ZZ-03
                  LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
           WHEN 4 DISPLAY ZZ-04
                  LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
           WHEN 5 DISPLAY ZZ-05
                  LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
           WHEN 6 DISPLAY ZZ-06
                  LINE LIN-IDX POSITION COL-IDX SIZE BDF-SIZE
           END-EVALUATE.

       CALCULE.
           MOVE 0 TO SAL-ACT.
           IF BAR-ECHELON-I100(IDX) > 0
              COMPUTE SAL-ACT = BAR-ECHELON-I100(IDX) 
                              * MOIS-IDX(LNK-MOIS) + ,00009.
           IF SAL-ACT > 5000
              MOVE 0 TO SAL-ACT-B.
           MOVE SAL-ACT TO HE-Z6Z5.
           PERFORM LIN-COL.
           ADD 2 TO COL-IDX.
           DISPLAY HE-Z6Z5 LINE LIN-IDX POSITION COL-IDX.

       DIS-NO.
           MOVE IDX TO HE-Z2.
           PERFORM LIN-COL.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION COL-IDX LOW.

       AFFICHAGE-ECRAN.
           MOVE 45 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           PERFORM DIS-NO VARYING IDX FROM 1 BY 1 UNTIL IDX > 80.

       AFFICHAGE-DETAIL.
           PERFORM DIS-E1-01 THRU DIS-E1-END.

       END-PROGRAM.
           MOVE 1 TO INDICE-ZONE.
           MOVE BAR-GRADE TO LNK-VAL.
           CLOSE BAREME.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
