      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 7-HPJ TRANSFERT PLAN DE TRAVAIL -> HEURES   �
      *  � POSTES DE FRAIS / NETTO-SERVICE                       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    7-HPJ.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "HOPJOUR.FC".
           COPY "HEURES.FC".
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "HOPJOUR.FDE".
           COPY "HEURES.FDE".
           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 10.
       01  PRECISION             PIC 9 VALUE 0.
           COPY "V-MINUTE.CPY".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "FICHE.REC".
           COPY "CCOL.REC".
           COPY "CALEN.REC".
           COPY "PRESENCE.REC".
           COPY "EQUIPE.REC".
           COPY "COUT.REC".
           COPY "STATUT.REC".
           COPY "POCL.REC".
           COPY "PCREF.REC".

       01  HRS-NAME.
           02 FILLER             PIC X(9) VALUE "S-HEURES.".
           02 ANNEE-HEURES       PIC 999.

       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS       PIC 999.

           COPY "V-VAR.CPY".

       01  ABSENCES.
           02 ABS OCCURS 3.
              03 INTER PIC 99V99 OCCURS 31.
        
       01  END-NUMBER            PIC 9(8) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  EQUIPE                PIC 99 VALUE 0.
       01  POST                  PIC 9(8) VALUE 0.
       01  DETAIL-YN.
           02 DETAIL-CONGE       PIC X VALUE " ".
           02 DETAIL-MALADIE     PIC X VALUE " ".
       01  DETAIL-YN-R REDEFINES DETAIL-YN.
           02 DETAIL-H           PIC X OCCURS 2.

       77  ANNEE      PIC 9(4).
       77  TARIF      PIC 99.
       77  JOUR-IDX   PIC 99.
       77  JOUR-DEBUT PIC 99.
       77  JOUR-FIN   PIC 99.
       77  REFER    PIC 99.
       77  HELP-1     PIC 9(4).
       01  HELP-2     PIC 9(4).
       01  HELP-2-R REDEFINES HELP-2.
           02 HELP-2A    PIC 99.
           02 HELP-2B    PIC 99.
       77  HELP-3     PIC 99V99 COMP-3.
       77  HELP-4     PIC S99V99 COMP-3.
       01  TEST-ROUND            PIC 999V99999.

       01  TST.
           03 MOIS  PIC 99.
           03 JOUR  PIC 99.
           03 BISEX PIC 9.
           03 M-ANNEE PIC X(24).
           03 M-A REDEFINES M-ANNEE.
              04 M-J    PIC 99 OCCURS 12.
           03 SEM-M OCCURS 12.
              04 SEM-I    PIC  9 OCCURS 31.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       01  TOT-OCCUP.
           02 TOT-OCC-IDX OCCURS 200.
              03 OCCUP-UNITE     PIC 9(8)V999 COMP-3.
              03 OCCUP-JOURS     PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               HEURES
               JOURS
               HOPJOUR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-7-HPJ.
       
           INITIALIZE PCR-RECORD.
           MOVE LNK-SUFFIX TO ANNEE-HEURES ANNEE-JOURS.
           OPEN INPUT HOPJOUR.
           OPEN I-O   HEURES.
           OPEN INPUT JOURS.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 3 THRU 6
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 10    MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11)
                      MOVE 0000680000 TO EXC-KFR(14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 THRU 9 PERFORM AVANT-YN
           WHEN 10 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7 
           WHEN  8 THRU 9 PERFORM APRES-YN
           WHEN 10 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-7.
           ACCEPT POST
           LINE 13 POSITION 25 SIZE 8
           TAB UPDATE NO BEEP CURSOR  1 
           ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-YN.
           COMPUTE IDX     = INDICE-ZONE - 7.
           COMPUTE LIN-IDX = INDICE-ZONE + 10.
           ACCEPT DETAIL-H(IDX)
             LINE LIN-IDX POSITION 32 SIZE 1 
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE DETAIL-H(IDX) TO ACTION.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-7.
           MOVE POST TO PC-NUMBER.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 2 THRU 3 CALL "2-POCL" USING LINK-V PC-RECORD
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM AFFICHAGE-DETAIL
           WHEN OTHER PERFORM NEXT-POSTE
           END-EVALUATE.
           IF PC-NUMBER > 0
              MOVE PC-NUMBER TO POST
           END-IF.
           PERFORM DIS-HE-07.

       APRES-YN.
           IF ACTION = "Y" OR "J" OR "O" OR "N" OR " "
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM FULL-PROCESS
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       FULL-PROCESS.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           PERFORM READ-PERSON THRU READ-EXIT.
       START-PR-END.
           EXIT.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-1
           END-IF.
           IF STATUT > 0 AND NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF COUT > 0 AND NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           IF EQUIPE > 0 AND NOT = CAR-EQUIPE
              GO READ-PERSON-1
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM TOTAL-ABSENCES.
           PERFORM FILL-FILES.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD FICHE-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD SAVE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           MOVE CCOL-CALENDRIER TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
           
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       NEXT-POSTE.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" SAVE-KEY.

       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.


       FILL-FILES.
           PERFORM DELETE-HRS THRU DELETE-END.
           INITIALIZE HPJ-RECORD.
           MOVE FR-KEY     TO HPJ-FIRME.
           MOVE REG-PERSON TO HPJ-PERSON.
           PERFORM CREATE-HRS THRU CREATE-END.
           PERFORM CALC-SAL.

       CALC-SAL.
           CALL "4-HRCRE" USING LINK-V REG-RECORD.
           CALL "4-A" USING LINK-V PR-RECORD REG-RECORD.

       DELETE-HRS.
           INITIALIZE HRS-RECORD.
           MOVE FR-KEY     TO HRS-FIRME.
           MOVE REG-PERSON TO HRS-PERSON.
           MOVE LNK-MOIS   TO HRS-MOIS.
           START HEURES KEY > HRS-KEY INVALID GO DELETE-END.
       DELETE-1.
           READ HEURES NEXT NO LOCK AT END
                GO DELETE-END
           END-READ.
           IF FR-KEY     NOT = HRS-FIRME
           OR REG-PERSON NOT = HRS-PERSON
           OR LNK-MOIS   NOT = HRS-MOIS 
              GO DELETE-END
           END-IF.
           IF HRS-BATCH NOT = "X"
              GO DELETE-1.
           IF  POST NOT = 0
           AND HRS-POSTE NOT = POST
              GO DELETE-1.
           IF LNK-SQL = "Y" 
              CALL "9-HEURES" USING LINK-V HRS-RECORD DEL-KEY 
           END-IF.
           DELETE HEURES INVALID CONTINUE.
           GO DELETE-1.
       DELETE-END.
           INITIALIZE HRS-RECORD.

       HRS-TOTAL.
           INITIALIZE HRS-TOTAL-BRUT.
           INITIALIZE HRS-TARIF.
           MOVE HRS-HR-D TO HELP-1 IDX-3.
           MOVE HRS-HR-F TO HELP-2 IDX-4.
           IF HELP-2 < HELP-1 ADD 24 TO HELP-2 IDX-4.
           SUBTRACT 1 FROM IDX-4.
           IF HRS-HR-D = HRS-HR-F
              COMPUTE IDX-2 = HRS-HR-D + 1
              MOVE 0 TO TEST-ROUND
              IF HRS-MIN-F > 0 
                 COMPUTE TEST-ROUND = (MINUTE(HRS-MIN-F) / 100)
              END-IF
              IF HRS-MIN-D > 0 
                 COMPUTE TEST-ROUND = 
                 TEST-ROUND - (MINUTE(HRS-MIN-D) / 100)
              END-IF
              ADD TEST-ROUND TO HRS-TOTAL-BRUT
              IF CCOL-TARIF-NUIT(IDX-2) NOT = 0 
                 MOVE CCOL-TARIF-NUIT(IDX-2) TO TARIF
                 MOVE TEST-ROUND TO HRS-TARIF-HRS(TARIF)
              END-IF
           ELSE 
              IF HRS-MIN-D > 0
                 COMPUTE IDX-2 = HRS-HR-D + 1
                 ADD 1 TO HELP-1 IDX-3
                 COMPUTE TEST-ROUND = (100 - MINUTE(HRS-MIN-D)) / 100
                 ADD TEST-ROUND TO HRS-TOTAL-BRUT
                 IF CCOL-TARIF-NUIT(IDX-2) NOT = 0 
                    MOVE CCOL-TARIF-NUIT(IDX-2) TO TARIF
                    MOVE TEST-ROUND TO HRS-TARIF-HRS(TARIF)
                 END-IF
              END-IF
           COMPUTE HRS-TOTAL-BRUT = HRS-TOTAL-BRUT + HELP-2 - HELP-1
              IF HRS-MIN-F > 0
                 COMPUTE IDX-2 = HRS-HR-F + 1
                 COMPUTE TEST-ROUND = (MINUTE(HRS-MIN-F)) / 100
                 ADD TEST-ROUND TO HRS-TOTAL-BRUT
                 IF CCOL-TARIF-NUIT(IDX-2) NOT = 0 
                    MOVE CCOL-TARIF-NUIT(IDX-2) TO TARIF
                    ADD TEST-ROUND TO HRS-TARIF-HRS(TARIF)
                 END-IF
               END-IF
               PERFORM ADD-NUIT VARYING IDX FROM IDX-3 BY 1 UNTIL
                    IDX > IDX-4
           END-IF.
           IF  HRS-DEBUT = 1
           AND HRS-FIN   = 1
              MOVE ,01 TO HRS-TOTAL-BRUT 
           END-IF. 
           MOVE HRS-TOTAL-BRUT TO HRS-TOTAL-NET.
           IF HRS-REPOS > 0
              MOVE HRS-REPOS TO HELP-2
              IF HELP-2B > 0
                 COMPUTE TEST-ROUND = (MINUTE(HELP-2B)) / 100
                 SUBTRACT TEST-ROUND FROM HRS-TOTAL-NET
              END-IF
              SUBTRACT HELP-2A FROM HRS-TOTAL-NET
           END-IF.
           PERFORM DIM-FER.
           PERFORM DELIMITE-NET VARYING IDX FROM 1 BY 1 UNTIL
                    IDX > 20.

       DIM-FER.
           IF SEM-IDX(LNK-MOIS, JOUR-IDX) = 7 
              IF CAL-JOUR(LNK-MOIS, JOUR-IDX) = 0
                  MOVE HRS-TOTAL-NET TO HRS-TARIF-JRS(10)
              END-IF
           END-IF.
           IF CAL-JOUR(LNK-MOIS, JOUR-IDX) > 0
              MOVE CAL-JOUR(LNK-MOIS, JOUR-IDX) TO IDX
              MOVE HRS-TOTAL-NET TO HRS-TARIF-JRS(IDX)
           END-IF.
           
       ADD-NUIT.
           MOVE IDX TO IDX-2.
           ADD 1 TO IDX-2.
           IF IDX-2 > 24 SUBTRACT 24 FROM IDX-2.
           IF CCOL-TARIF-NUIT(IDX-2) NOT = 0 
              MOVE CCOL-TARIF-NUIT(IDX-2) TO TARIF
              ADD 1 TO HRS-TARIF-HRS(TARIF)
           END-IF.

       DELIMITE-NET.
           IF HRS-TOTAL-NET < HRS-TARIF-IDX(IDX)
              MOVE HRS-TOTAL-NET TO HRS-TARIF-IDX(IDX)
           END-IF.

       CREATE-HRS.
           INITIALIZE JRS-RECORD. 
           MOVE FR-KEY TO JRS-FIRME.
           MOVE LNK-MOIS  TO JRS-MOIS.
           MOVE REG-PERSON TO JRS-PERSON.
           MOVE 99 TO JRS-OCCUPATION.
           

           INITIALIZE HPJ-RECORD.
           MOVE FR-KEY     TO HPJ-FIRME.
           MOVE REG-PERSON TO HPJ-PERSON.
           START HOPJOUR KEY > HPJ-KEY INVALID GO CREATE-END.
       CREATE-1.
           READ HOPJOUR NEXT NO LOCK AT END
                GO CREATE-END
           END-READ.
           IF FR-KEY     NOT = HPJ-FIRME
           OR REG-PERSON NOT = HPJ-PERSON
              GO CREATE-END
           END-IF.
           IF POST > 0
           AND HPJ-POSTE NOT = POST
              GO CREATE-1
           END-IF.
           MOVE 1 TO JOUR-DEBUT.
           MOVE MOIS-JRS(LNK-MOIS) TO JOUR-FIN.
           IF HPJ-PERIODE(LNK-MOIS) = 0
              GO CREATE-1
           END-IF.
           IF HPJ-A > LNK-ANNEE
              GO CREATE-1
           END-IF.
           IF  HPJ-A = LNK-ANNEE
           AND HPJ-M > LNK-MOIS
               GO CREATE-1
           END-IF.
           IF  HPJ-DATE-FIN > 0
           AND HPJ-F-A < LNK-ANNEE
               GO CREATE-1
           END-IF.
           IF  HPJ-F-A = LNK-ANNEE
           AND HPJ-F-M < LNK-MOIS
               GO CREATE-1
           END-IF.
           IF  HPJ-A = LNK-ANNEE
           AND HPJ-M = LNK-MOIS
              MOVE HPJ-J TO JOUR-DEBUT
           END-IF.
           IF  HPJ-F-A = LNK-ANNEE
           AND HPJ-F-M = LNK-MOIS
              MOVE HPJ-F-J TO JOUR-FIN
           END-IF.
           IF HPJ-POSTE NOT = PC-NUMBER
              MOVE HPJ-POSTE TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
           END-IF.
           IF  PC-DATE-FIN > 0
           AND PC-FIN-A < LNK-ANNEE
               GO CREATE-1
           END-IF.
           IF  PC-FIN-A = LNK-ANNEE
           AND PC-FIN-M < LNK-MOIS
               GO CREATE-1
           END-IF.
           IF  PC-FIN-A = LNK-ANNEE
           AND PC-FIN-M = LNK-MOIS
           AND PC-FIN-J < JOUR-FIN
              MOVE PC-FIN-J TO JOUR-FIN
           END-IF.
           IF  HPJ-ESPACE   = 0
           AND HPJ-SEMAINES = 0
              MOVE 1 TO HPJ-ESPACE 
           END-IF.
           IF HPJ-ESPACE > 1
              PERFORM TEST-X
           END-IF.
           IF HPJ-ESPACE > 0
              PERFORM FILL-LIGNE VARYING JOUR-IDX 
              FROM JOUR-DEBUT BY 1 UNTIL JOUR-IDX > JOUR-FIN
           END-IF.

           IF HPJ-ESPACE = 0
              PERFORM FILL-LIGNE-S VARYING JOUR-IDX 
              FROM JOUR-DEBUT BY 1 UNTIL JOUR-IDX > JOUR-FIN
              IF HPJ-SEM(5) > 0
                 COMPUTE REFER = MOIS-JRS(LNK-MOIS) - 6
                 IF REFER < JOUR-FIN
                    PERFORM LAST-LIGNE VARYING JOUR-IDX 
                    FROM REFER BY 1 UNTIL JOUR-IDX > JOUR-FIN
                 END-IF
              END-IF
           END-IF.
           GO CREATE-1.
       CREATE-END.
           INITIALIZE HPJ-RECORD.
           CALL "6-JOUR" USING LINK-V REG-RECORD JRS-RECORD WR-KEY.

       FILL-LIGNE.
           IF PRES-JOUR(LNK-MOIS, JOUR-IDX) > 0
              PERFORM MAKE-PLAGE THRU MAKE-PLAGE-END.

       MAKE-PLAGE.
           MOVE SEM-IDX(LNK-MOIS, JOUR-IDX) TO IDX-1.
           IF HPJ-JOUR(IDX-1) = 0 
              GO MAKE-PLAGE-END.
           IF HPJ-ESPACE > 1
              IF SEM-I(LNK-MOIS, JOUR-IDX) = 0
                 GO MAKE-PLAGE-END
              END-IF
           END-IF.
           PERFORM PLANS.

           IF INTER(1, JOUR-IDX) > 0
           AND DETAIL-MALADIE NOT = "N"
              GO MAKE-PLAGE-END
           END-IF.
           IF INTER(2, JOUR-IDX) > 0
           AND DETAIL-CONGE NOT = "N"
              GO MAKE-PLAGE-END
           END-IF.
           IF  CAL-JOUR(LNK-MOIS, JOUR-IDX) = 1
           AND HPJ-TRAV-FERIE = "N"
              GO MAKE-PLAGE-END.
           PERFORM PLAGES.
       MAKE-PLAGE-END.
           EXIT.

       FILL-LIGNE-S.
           IF PRES-JOUR(LNK-MOIS, JOUR-IDX) > 0
              PERFORM MAKE-JOUR THRU MAKE-JOUR-END.

       MAKE-JOUR.
           COMPUTE REFER = (JOUR-IDX + 6) / 7.
           IF HPJ-SEM(REFER) = 0 
              GO MAKE-JOUR-END.
           MOVE SEM-IDX(LNK-MOIS, JOUR-IDX) TO IDX-1.
           IF HPJ-JOUR(IDX-1) = 0 
              GO MAKE-JOUR-END.
           PERFORM PLANS.
           IF INTER(1, JOUR-IDX) > 0
           AND DETAIL-MALADIE NOT = "N"
              GO MAKE-JOUR-END
           END-IF.
           IF INTER(2, JOUR-IDX) > 0
           AND DETAIL-CONGE NOT = "N"
              GO MAKE-JOUR-END
           END-IF.
           IF  CAL-JOUR(LNK-MOIS, JOUR-IDX) = 1
           AND HPJ-TRAV-FERIE = "N"
              GO MAKE-JOUR-END.
           PERFORM PLAGES.
       MAKE-JOUR-END.
           EXIT.

       LAST-LIGNE.
           IF PRES-JOUR(LNK-MOIS, JOUR-IDX) > 0
              PERFORM LAST-JOUR THRU LAST-JOUR-END.

       LAST-JOUR.
           MOVE SEM-IDX(LNK-MOIS, JOUR-IDX) TO IDX-1.
           IF HPJ-JOUR(IDX-1) = 0 
              GO LAST-JOUR-END.
           IF INTER(1, JOUR-IDX) > 0
           AND DETAIL-MALADIE NOT = "N"
              GO LAST-JOUR-END
           END-IF.
           IF INTER(2, JOUR-IDX) > 0
           AND DETAIL-CONGE NOT = "N"
              GO LAST-JOUR-END
           END-IF.
           IF  CAL-JOUR(LNK-MOIS, JOUR-IDX) = 1
           AND HPJ-TRAV-FERIE = "N"
              GO LAST-JOUR-END.
           PERFORM PLAGES.
       LAST-JOUR-END.
           EXIT.

       PLAGES.
           MOVE HPJ-DEBUT(1) TO HRS-INTERVENT HRS-DEBUT.
           MOVE HPJ-FIN(1)   TO HRS-FIN.
           MOVE HPJ-REPOS(1) TO HRS-REPOS.
           PERFORM HRS-TOTAL.
           PERFORM WRITE-HRS.
           IF HPJ-DEBUT(2) > 0
              MOVE HPJ-DEBUT(2) TO HRS-INTERVENT HRS-DEBUT
              MOVE HPJ-FIN(2)   TO HRS-FIN
              MOVE HPJ-REPOS(2) TO HRS-REPOS
              PERFORM HRS-TOTAL
              PERFORM WRITE-HRS
           END-IF.

       WRITE-HRS.
           MOVE FR-KEY TO HRS-FIRME HRS-FIRME-A HRS-FIRME-B HRS-FIRME-C.
           MOVE REG-PERSON TO HRS-PERSON HRS-PERSON-A HRS-PERSON-B 
                              HRS-PERSON-C.
           MOVE PC-NUMBER TO HRS-POSTE HRS-POSTE-A HRS-POSTE-B 
                             HRS-POSTE-C.
           MOVE LNK-MOIS TO HRS-MOIS HRS-MOIS-A HRS-MOIS-B HRS-MOIS-C.
           MOVE JOUR-IDX TO HRS-JOUR HRS-JOUR-A HRS-JOUR-B HRS-JOUR-C.
           MOVE HRS-INTERVENT TO HRS-INTERVENT-A HRS-INTERVENT-B 
                                 HRS-INTERVENT-C.
           MOVE TODAY-TIME TO HRS-TIME.
           MOVE LNK-USER TO HRS-USER.
           MOVE "X" TO HRS-BATCH.
           IF LNK-SQL = "Y" 
              CALL "9-HEURES" USING LINK-V HRS-RECORD WR-KEY 
           END-IF.
           WRITE HRS-RECORD INVALID CONTINUE END-WRITE.
           IF PC-NUMBER  NOT = PCR-POSTE
           OR REG-PERSON NOT = PCR-PERSON
              CALL "6-PCREF" USING LINK-V PC-RECORD REG-RECORD 
              PCR-RECORD WR-KEY
           END-IF.

       PLANS.
           ADD HPJ-TOTAL-NET(1) TO JRS-HRS(JOUR-IDX) JRS-HRS(32).
           ADD HPJ-TOTAL-NET(2) TO JRS-HRS(JOUR-IDX) JRS-HRS(32).

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE EQ-NUMBER TO HE-Z2.
           DISPLAY HE-Z2  LINE 11 POSITION 31.
           DISPLAY EQ-NOM LINE 11 POSITION 35.
       DIS-HE-07.
           MOVE POST TO HE-Z8.
           DISPLAY HE-Z8 LINE 13 POSITION 25.
           IF POST NOT = 0
              MOVE POST TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
           ELSE
              INITIALIZE PC-RECORD
           END-IF.
           DISPLAY PC-NOM  LINE 13 POSITION 35.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE  326 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XEQUIPE.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XACTION.CPY".
           COPY "XECRAN.CPY".

       TEST-X.
           INITIALIZE TST JOUR.
           MOVE HPJ-JOUR1-REF TO SH-00.
           COMPUTE IDX-1 = HPJ-ESPACE * 7.
           PERFORM TEST-Y VARYING ANNEE FROM HPJ-A BY 1 UNTIL ANNEE
           > LNK-ANNEE.
           SUBTRACT HELP-1 FROM SH-00.
           MOVE 1 TO MOIS.
           MOVE SH-00 TO IDX IDX-2.
           IF SH-00 < 0
              MOVE 1 TO IDX
           ELSE 
              MOVE SH-00 TO JOUR
              SUBTRACT 1 FROM JOUR
              MOVE 0 TO IDX-2
           END-IF.
           IF IDX-2 > IDX-1
              SUBTRACT IDX-1 FROM IDX-2 
           END-IF.
           MOVE "312831303130313130313031" TO M-ANNEE
           MOVE 365 TO IDX-4
           IF BISEX = 0
              MOVE 366 TO IDX-4
              MOVE 29  TO M-J(2)
           END-IF.
           PERFORM SEM VARYING IDX FROM IDX BY 1 UNTIL IDX > IDX-4.

       TEST-Y.
           DIVIDE ANNEE BY 4 GIVING IDX-3 REMAINDER BISEX.
           IF ANNEE > HPJ-A
              ADD 1 TO HELP-1
              IF BISEX = 1
                 ADD 1 TO HELP-1
              END-IF
              IF HELP-1 >= IDX-1
                 SUBTRACT IDX-1 FROM HELP-1
              END-IF
           END-IF.

       SEM.
           ADD 1 TO JOUR IDX-2.
           IF JOUR > M-J(MOIS)
              MOVE 1 TO JOUR
              ADD  1 TO MOIS
           END-IF.
           IF IDX-2 < 8
              MOVE 1 TO SEM-I(MOIS, JOUR)
           END-IF.
           IF IDX-2 = IDX-1
              MOVE 0 TO IDX-2
           END-IF.

       TOTAL-ABSENCES.
           INITIALIZE JRS-RECORD ABSENCES NOT-FOUND. 
           MOVE FR-KEY TO JRS-FIRME.
           MOVE LNK-MOIS  TO JRS-MOIS.
           MOVE REG-PERSON TO JRS-PERSON.
           START JOURS KEY > JRS-KEY INVALID CONTINUE
              NOT INVALID 
              PERFORM READ-ABSENCES THRU END-ABSENCES.
            
       READ-ABSENCES.
           READ JOURS NEXT NO LOCK AT END
               GO END-ABSENCES
           END-READ.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
              GO END-ABSENCES
           END-IF.
           PERFORM CUM-HRS VARYING IDX-2 FROM 1 BY 1 UNTIL IDX-2 > 31.
           GO READ-ABSENCES.
       END-ABSENCES.
           EXIT.

       CUM-HRS.
           EVALUATE JRS-OCCUPATION
              WHEN  0 CONTINUE
              WHEN  1 THRU 11 ADD JRS-HRS(IDX-2) TO INTER(1, IDX-2)
              WHEN 20 THRU 30 ADD JRS-HRS(IDX-2) TO INTER(2, IDX-2)
              WHEN OTHER      ADD JRS-HRS(IDX-2) TO INTER(3, IDX-2)
           END-EVALUATE.
