      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 8-IEEABS FICHIER STATISTIQUES IEE MALADIE   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    8-IEEABS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".
           SELECT OPTIONAL TF-ABSENCES ASSIGN TO DISK, PARMOD-PATH
             ORGANIZATION IS LINE SEQUENTIAL
             FILE STATUS FS-HELP.

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURS.FDE".
       FD  TF-ABSENCES 
           LABEL RECORD STANDARD
           DATA RECORD TF-RECORD.

      *  ENREGISTREMENT FICHIER DE TRANSFER ASCII PERSONNE
      
       01  TF-RECORD.
           02 TF-FIRME           PIC 9(5).
           02 TF-FILLER-1        PIC X.
           02 TF-NUMBER          PIC 9(6).
           02 TF-FILLER-2        PIC X.
           02 TF-NOM             PIC X(35).
           02 TF-FILLER-4        PIC X.
           02 TF-PERIODE         PIC 9999.
           02 TF-FILLER-3        PIC X.
           02 TF-JOURS           PIC 9999.
           02 TF-FILLER-3        PIC X.
           02 TF-PERIODES.
                 04 TF-HP OCCURS 12.
                    05 TF-JOUR-DEBUT      PIC 999.
                    05 TF-FILLER          PIC X.
                    05 TF-MOIS-DEBUT      PIC 99999.
                    05 TF-FILLER          PIC X.
                    05 TF-JOUR-FIN        PIC 999.
                    05 TF-FILLER          PIC X.
                    05 TF-MOIS-FIN        PIC 99999.
                    05 TF-FILLER          PIC X.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 9.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "PRESENCE.REC".
      *    COPY "TAUXCC.REC".
           COPY "STATUT.REC".
           COPY "COUT.REC".
           COPY "LIVRE.REC".
           COPY "CCOL.REC".
           COPY "CALEN.REC".
           COPY "PARMOD.REC".

           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(6) VALUE 999999.
       01  SAVE-NUMBER           PIC 9(6) VALUE 999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  MOIS-D                PIC 99 VALUE 1.
       01  MOIS-F                PIC 99 VALUE 12.

       01  TXT-RECORD.
           02 TXT-FIRME           PIC X(5) VALUE "FIRMA".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-PERS            PIC X(6) VALUE "PERSON".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-NOM             PIC X(35) VALUE "NAME".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-PERIODE         PIC XXXX VALUE "ZAHL".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-JOURS           PIC XXXX VALUE "TAGE".
           02 TXT-FILLER          PIC X VALUE ";".
           02 TXT-PERIODES.
              04 TXT-HP OCCURS 12.
                 05 TXT-JOUR-DEBUT      PIC XXX.
                 05 TXT-FILLER          PIC X.
                 05 TXT-MOIS-DEBUT      PIC X(5).
                 05 TXT-FILLER          PIC X.
                 05 TXT-JOUR-FIN        PIC XXX.
                 05 TXT-FILLER          PIC X.
                 05 TXT-MOIS-FIN        PIC X(5).
                 05 TXT-FILLER          PIC X.

       01  PERIODE               PIC 99 VALUE 0.    
       01  PERIODE-1             PIC 999 VALUE 0.    
       01  PERIODES              PIC 9999 VALUE 0.    
       01  NOT-OPEN              PIC 9 VALUE 0.
       01  JOURS-MALADIE         PIC 999.    
       01  JRS-INTERRUPTION      PIC 999.    

       01  HELP-CUMUL.
           02 H-J OCCURS 12.
              03 HR-TOT-JOUR     PIC 99V99 OCCURS 31.
              03 HR-MAL-JOUR     PIC S99V99 OCCURS 31.

       01  HELP-PERIODES.
           02 H-P OCCURS 30.
              03 JOUR-DEBUT      PIC 99.
              03 MOIS-DEBUT      PIC 99.
              03 JOUR-FIN        PIC 99.
              03 MOIS-FIN        PIC 99.

       01  REPARTITION.
           02 REP OCCURS 12     PIC 9(4).

       01  JOURS-NAME.
           02 JOURS-ID           PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z2Z2 PIC ZZ,ZZ. 
           02 HE-Z4Z2 PIC ZZZZ,ZZ. 

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS TF-ABSENCES. 

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-8-IEEABS.

           INITIALIZE REPARTITION.
           MOVE 1 TO STATUT.
           PERFORM AFFICHAGE-ECRAN .
           CALL "0-TODAY" USING TODAY.
           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN INPUT JOURS.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4 THRU 5
                      MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 6 THRU 7
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 9     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN  7 PERFORM AVANT-7 
           WHEN  8 PERFORM AVANT-PATH
           WHEN  9 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.

           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN  7 PERFORM APRES-7
           WHEN  9 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-6.
           ACCEPT MOIS-D
             LINE 19 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-7.
           ACCEPT MOIS-F
             LINE 20 POSITION 31 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-PATH.
           IF PARMOD-PATH = SPACES
              MOVE "C:\TEMP\FILE.TXT" TO PARMOD-PATH
           END-IF.
           ACCEPT PARMOD-PATH
             LINE 22 POSITION 25 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-6.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-D
             WHEN 66 ADD 1 TO MOIS-D
           END-EVALUATE.
           IF MOIS-D < 1 
              MOVE 1 TO MOIS-D
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-D > 12
              MOVE 12 TO MOIS-D
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-D > MOIS-F
              MOVE MOIS-D TO MOIS-F.
           PERFORM DIS-HE-06 THRU DIS-HE-07.

       APRES-7.
           EVALUATE EXC-KEY
             WHEN 65 SUBTRACT 1 FROM MOIS-F
             WHEN 66 ADD 1 TO MOIS-F
           END-EVALUATE.
           IF MOIS-F < 1 
              MOVE 1 TO MOIS-F
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-F > 12
              MOVE 12 TO MOIS-F
              MOVE 1 TO INPUT-ERROR.
           IF MOIS-D > MOIS-F
              MOVE MOIS-D TO MOIS-F
              MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-HE-06 THRU DIS-HE-07.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM TRAITEMENT
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           MOVE 1 TO INPUT-ERROR.
           PERFORM DISPLAY-MESSAGE.
           
       TRAITEMENT.
           PERFORM START-PERSON.
           PERFORM READ-PERSON THRU READ-EXIT.

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
           PERFORM CAR-RECENTE.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-1
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-1
           END-IF.
           PERFORM DIS-HE-01.
           PERFORM CUMUL-JOURS.
           IF  JOUR-DEBUT(1) = 99
           AND JOUR-DEBUT(2) = 0
              MOVE 0 TO JOUR-DEBUT(1).
           IF JOUR-DEBUT(1) NOT = 0
              PERFORM FULL-PROCESS.
       READ-PERSON-1.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON.
       READ-EXIT.
           PERFORM END-PROGRAM.
                
       FULL-PROCESS.
           IF JOUR-DEBUT(1) NOT = 0
              PERFORM FILL-FILES.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           MOVE CCOL-CALENDRIER TO CAL-NUMBER.
           CALL "6-CALEN" USING LINK-V CAL-RECORD FAKE-KEY.

       CUMUL-JOURS.
           INITIALIZE JOURS-MALADIE HELP-PERIODES HELP-CUMUL PERIODE.
           PERFORM C-H VARYING IDX-4 FROM 1 BY 1 UNTIL IDX-4 > 12.
           INITIALIZE HELP-PERIODES PERIODE PERIODE-1 JRS-INTERRUPTION.
           PERFORM T-P VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 12.
           IF HR-TOT-JOUR(1, 1) NOT = 0 
              CALL "IEE-ABS1" USING LINK-V REG-RECORD
              IF LNK-VAL = 1 
                 MOVE 99 TO JOUR-DEBUT(1) 
                 SUBTRACT 1 FROM PERIODE
                 SUBTRACT PERIODE-1 FROM JOURS-MALADIE
              END-IF
           END-IF.
           PERFORM REP VARYING IDX FROM 1 BY 1 UNTIL IDX > 30.

       C-H.
           INITIALIZE JRS-RECORD.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE REG-PERSON TO JRS-PERSON.
           MOVE IDX-4      TO JRS-MOIS.
           START JOURS KEY >= JRS-KEY INVALID CONTINUE
                NOT INVALID
                PERFORM READ-HEURES THRU READ-HEURES-END.

       REP.
           IF JOUR-DEBUT(IDX) > 0 AND < 99
              MOVE MOIS-DEBUT(IDX) TO IDX-3
              ADD 1 TO REP(IDX-3)
           END-IF.

       READ-HEURES.
           READ JOURS NEXT AT END
               GO READ-HEURES-END.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR JRS-MOIS   NOT = IDX-4
              GO READ-HEURES-END
           END-IF.
           IF JRS-COMPLEMENT NOT = 0
           OR JRS-OCCUPATION =  0
           OR JRS-OCCUPATION = 10
           OR JRS-OCCUPATION > 11
           OR JRS-MOIS       < MOIS-D
           OR JRS-MOIS       > MOIS-F
              GO READ-HEURES
           END-IF.
           INITIALIZE L-RECORD.
           MOVE JRS-MOIS TO L-MOIS.
           MOVE JRS-OCCUPATION TO L-SUITE.
           CALL "4-NXLP" USING LINK-V REG-RECORD L-RECORD FAKE-KEY.
           IF L-FLAG-ACCIDENT NOT = 0
              GO READ-HEURES
           END-IF.
           PERFORM ADD-HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 31.
           GO READ-HEURES.
       READ-HEURES-END.
           INITIALIZE HELP-PERIODES PERIODE JRS-INTERRUPTION.

       ADD-HEURES.
           IF JRS-HRS(IDX) NOT = 0
              MOVE JRS-HRS(IDX) TO HR-TOT-JOUR(JRS-MOIS, IDX) 
                                   HR-MAL-JOUR(JRS-MOIS, IDX) 
           END-IF.
           IF JRS-HRS(IDX) > 0
              ADD 1 TO JOURS-MALADIE 
           END-IF.

       T-P.
           PERFORM TEST-JOURS 
           VARYING IDX FROM 1 BY 1 UNTIL IDX > MOIS-JRS(IDX-1).

       TEST-JOURS.
           IF HR-TOT-JOUR(IDX-1, IDX) = 0
              IF CAL-JOUR(IDX-1, IDX) NOT = 1
              AND SEM-IDX(IDX-1, IDX) < 6
                 ADD 1 TO JRS-INTERRUPTION
              END-IF
           ELSE
              IF JRS-INTERRUPTION > 1
                 ADD 1 TO PERIODE
              END-IF
              IF PERIODE = 0
                 ADD 1 TO PERIODE
              END-IF
              IF JOUR-DEBUT(PERIODE) = 0
                 MOVE IDX   TO JOUR-DEBUT(PERIODE) 
                 MOVE IDX-1 TO MOIS-DEBUT(PERIODE) 
              END-IF
              MOVE IDX   TO JOUR-FIN(PERIODE) 
              MOVE IDX-1 TO MOIS-FIN(PERIODE) 
              MOVE 0 TO JRS-INTERRUPTION
           END-IF.
           IF PERIODE = 1
           AND HR-MAL-JOUR(IDX-1, IDX) > 0
              ADD 1 TO PERIODE-1
           END-IF.

       FILL-FILES.
           IF NOT-OPEN = 0
              OPEN OUTPUT TF-ABSENCES
              MOVE 1 TO NOT-OPEN
              INITIALIZE TXT-PERIODES 
              INSPECT TXT-PERIODES REPLACING ALL " " BY ";"
              PERFORM FILL-TEXTE VARYING IDX FROM 1 BY 1 UNTIL IDX > 12
              WRITE TF-RECORD FROM TXT-RECORD
           END-IF.
           INITIALIZE TF-RECORD.
           INSPECT TF-RECORD REPLACING ALL " " BY ";".
           MOVE REG-FIRME  TO TF-FIRME.
           MOVE REG-PERSON TO TF-NUMBER.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           MOVE LNK-TEXT TO TF-NOM.
           MOVE PERIODE  TO TF-PERIODE.
           MOVE JOURS-MALADIE TO TF-JOURS.
           MOVE 0 TO IDX-2.
           PERFORM FILL-HEURES VARYING IDX FROM 1 BY 1 UNTIL IDX > 12.
           WRITE TF-RECORD.

       FILL-HEURES.
           IF JOUR-DEBUT(IDX) = 99
              CONTINUE
           ELSE
              ADD 1 TO IDX-2
              MOVE JOUR-DEBUT(IDX) TO TF-JOUR-DEBUT(IDX-2) 
              MOVE MOIS-DEBUT(IDX) TO TF-MOIS-DEBUT(IDX-2)
              MOVE JOUR-FIN(IDX)   TO TF-JOUR-FIN(IDX-2)
              MOVE MOIS-FIN(IDX)   TO TF-MOIS-FIN(IDX-2)
           END-IF.

       FILL-TEXTE.
           MOVE "VON"   TO TXT-JOUR-DEBUT(IDX). 
           MOVE "DATUM" TO TXT-MOIS-DEBUT(IDX).
           MOVE "BIS"   TO TXT-JOUR-FIN(IDX).
           MOVE "DATUM" TO TXT-MOIS-FIN(IDX).

           COPY "XDIS.CPY".
       DIS-HE-06.
           MOVE MOIS-D TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 19 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 19351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-07.
           MOVE MOIS-F TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 20 POSITION 31.
           MOVE "MO" TO LNK-AREA.
           MOVE 20351200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 605  TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY SPACES LINE 19 POSITION 1 SIZE 80.
           DISPLAY SPACES LINE 20 POSITION 1 SIZE 80.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CANCEL "IEE-ABS1".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

