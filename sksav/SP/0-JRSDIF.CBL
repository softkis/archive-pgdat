
      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-JRSDIF DIFFERENCE JOURS ENTRE DEUX DATES  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-JRSDIF.

       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  IDX      PIC 9(4).
       77  IDX-1    PIC 9(5).
       77  IDX-2    PIC 99.
       77  IDX-3    PIC 9(4).
       77  IDX-4    PIC 9(4).
       01  SH-00    PIC S99 COMP-3.
       77  FEBRUAR-1 PIC 99.
       77  FEBRUAR-2 PIC 99.

       01 DATE- PIC 9(8).
       01 DATE-X REDEFINES DATE-.
          02 ANNEE          PIC 9999.
          02 MOIS           PIC 99.
          02 JOUR           PIC 99.

       01  DATES.
           02 DATE-1 PIC 9(8).
           02 DATE-R1 REDEFINES DATE-1.
               03 A1 PIC 9999.
               03 M1 PIC 99.
               03 J1 PIC 99.
           02 DATE-2 PIC 9(8).
           02 DATE-R2 REDEFINES DATE-2.
               03 A2 PIC 9999.
               03 M2 PIC 99.
               03 J2 PIC 99.


       01  ANNEE-BASE.
           02 BASE-ANNEE PIC X(24) VALUE "312831303130313130313031".
           02 BASE-JOURS REDEFINES BASE-ANNEE.
              03 BASE-JRS PIC 99 OCCURS 12.
       01  JOUR-SEMAINE   PIC 9.
       01  ESC-KEY        PIC 9(4) COMP-1.
       01  ACTION         PIC X.

       LINKAGE SECTION.

           COPY "V-LINK.CPY".
 
       PROCEDURE DIVISION USING LINK-V.

       START-CALC SECTION.
             
       START-PROGRAMME-0-JRSDIFF.
                     
       JOURS-DIFFERENCE.

           MOVE LNK-VAL TO DATE-.
           PERFORM CHECK.
           MOVE DATE- TO DATE-1.
           MOVE BASE-JRS(2) TO FEBRUAR-1.
                     

           MOVE LNK-VAL-2 TO DATE-.
           MOVE 28 TO BASE-JRS(2).
           PERFORM CHECK.
           MOVE BASE-JRS(2) TO FEBRUAR-2.
           MOVE DATE- TO DATE-2.
           IF DATE-2 <= DATE-1 
              MOVE 0 TO LNK-POSITION
              EXIT PROGRAM
           END-IF.

           COMPUTE LNK-POSITION = BASE-JRS(M1) - J1 + J2.
           IF A1 = A2
              IF M1 = M2
                 COMPUTE LNK-POSITION =  J2 - J1 + 1
                 EXIT PROGRAM
              END-IF
              COMPUTE IDX-1 = M1 + 1
              IF IDX-1 < M2
                 PERFORM RESTE VARYING IDX-1 FROM IDX-1 BY 1 UNTIL 
                 IDX-1 = M2
              END-IF
              EXIT PROGRAM
           END-IF.

           IF M1 < 12
              MOVE FEBRUAR-1 TO BASE-JRS(2) 
              COMPUTE IDX-1 = M1 + 1
              PERFORM RESTE VARYING IDX-1 FROM IDX-1 BY 1 UNTIL IDX-1
              > 12.

           IF M2 > 1
              MOVE FEBRUAR-2 TO BASE-JRS(2) 
              COMPUTE IDX-1 = M2 + 1
              PERFORM RESTE VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 = M2.

           COMPUTE SH-00 = A2 - A1 - 1.
           IF SH-00 > 0
              COMPUTE LNK-POSITION = LNK-POSITION + (SH-00 * 365)
              COMPUTE IDX-1 = A1 + 1
              PERFORM BISEX VARYING IDX-1 FROM IDX-1 BY 1 UNTIL IDX-1 
              = A2.

           EXIT PROGRAM.

       RESTE.
           COMPUTE LNK-POSITION = LNK-POSITION + BASE-JRS(IDX-1).

       BISEX.
           DIVIDE IDX-1 BY   4 GIVING IDX-2 REMAINDER IDX-3.
           DIVIDE IDX-1 BY 100 GIVING IDX-2 REMAINDER IDX-4.
           IF  IDX-3 = 0
           AND IDX-4 > 0
              ADD 1 TO LNK-POSITION.
           


       CHECK.
           IF ANNEE < 1900
              MOVE 1900 TO ANNEE
           END-IF.
           DIVIDE ANNEE BY   4 GIVING IDX-2 REMAINDER IDX-3.
           DIVIDE ANNEE BY 100 GIVING IDX-2 REMAINDER IDX-4.
           IF  IDX-3 = 0
           AND IDX-4 > 0
              MOVE 29 TO BASE-JRS(2).
           IF MOIS < 1 
              MOVE 1 TO MOIS 
           END-IF.
           IF MOIS > 12
              MOVE 12 TO MOIS 
           END-IF.
           IF JOUR < 1 
              MOVE 1 TO JOUR 
           END-IF.
           IF JOUR > BASE-JRS(MOIS)
              MOVE BASE-JRS(MOIS) TO JOUR 
           END-IF.

           COPY "XACTION.CPY".
