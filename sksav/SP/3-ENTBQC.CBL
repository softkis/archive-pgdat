      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-ENTBQC IMPRESSION ENTREE BANQUE CENTRALE  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-ENTBQC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CONTRAT.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CONTRAT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 7.
       01  PRECISION             PIC 9 VALUE 1.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "FICHE.REC".
           COPY "PRESENCE.REC".
           COPY "COUT.REC".
           COPY "PAYS.REC".
           COPY "POCL.REC".
           COPY "STATUT.REC".
           COPY "METIER.REC".
           COPY "PARMOD.REC".
           COPY "ENPF.LNK".
           COPY "V-VAR.CPY".
        
       01  END-NUMBER            PIC 9(8) VALUE 99999999.
       01  SAVE-NUMBER           PIC 9(8) VALUE 99999999.
       01  END-MATCHCODE         PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SAVE-MATCHCODE        PIC X(10) VALUE "ZZZZZZZZZZ".
       01  SIGNATAIRE            PIC X(30).
       01  REEDITION             PIC X VALUE "N".
       01  CHOIX                 PIC 99 VALUE 0.
       01  HEURES                PIC 99 VALUE 0.
       01  COUNTER               PIC 999 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z3Z2 PIC ZZZ,ZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).
           02 HE-Z3 PIC Z(3).
           02 HE-Z4 PIC Z(4).
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CONTRAT.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-ENTBQC.
       
           MOVE LNK-LANGUAGE TO SAVE-LANGUAGE.
           MOVE LNK-MOIS TO SAVE-MOIS.
           INITIALIZE PARMOD-RECORD.
           MOVE MENU-PROG-NAME TO PARMOD-MODULE.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-SETTINGS TO SIGNATAIRE.

           OPEN I-O   CONTRAT.

           CALL "0-TODAY" USING TODAY.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 2     MOVE 0063640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 4     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 5     MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 7     MOVE 0000000025 TO EXC-KFR(1)
                      MOVE 1700000000 TO EXC-KFR(2)
                      MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 IF A-N = "A"
                      PERFORM AVANT-2A
                   ELSE
                      PERFORM AVANT-2N
                   END-IF
           WHEN  3 IF A-N = "A"
                      PERFORM AVANT-3A
                   ELSE
                      PERFORM AVANT-3N
                   END-IF
           WHEN  4 PERFORM AVANT-STAT
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-YN
           WHEN  7 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-STAT
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-YN
           WHEN  7 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
                MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-YN.
           ACCEPT REEDITION
             LINE 13 POSITION 35 SIZE 1 
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE REEDITION TO ACTION.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-YN.
           IF ACTION = "Y" OR "J" OR "O" OR "N" OR " " OR "X"
              MOVE 0 TO INPUT-ERROR
           ELSE
              MOVE 1 TO INPUT-ERROR.

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE "N" TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
           WHEN 5 PERFORM START-PERSON
                  PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       START-PERSON.
           MOVE FR-KEY TO REG-FIRME REG-FIRME-A.
           IF REG-PERSON > 0 
              SUBTRACT 1 FROM REG-PERSON.
           MOVE REG-PERSON TO REG-PERSON-A.
           IF A-N = "A"
              MOVE 999999 TO END-NUMBER
           ELSE
              MOVE "ZZZZZZZZZZ" TO END-MATCHCODE
           END-IF.
           PERFORM READ-PERSON THRU READ-EXIT.

       READ-PERSON.
           MOVE 66 TO EXC-KEY.
           PERFORM NEXT-REGIS.
           IF REG-PERSON = 0
           OR REG-PERSON > END-NUMBER
           OR REG-MATCHCODE > END-MATCHCODE
              GO READ-EXIT.
       READ-PERSON-1.
           PERFORM CAR-RECENTE.
           IF CAR-IN-ACTIF = 1 
              GO READ-PERSON-3
           END-IF.
           IF  STATUT > 0
           AND STATUT NOT = CAR-STATUT  
              GO READ-PERSON-3
           END-IF.
           IF  COUT > 0 
           AND COUT NOT = CAR-COUT
              GO READ-PERSON-3
           END-IF.
           IF CAR-OCCUPATION = 1 
           OR CAR-OCCUPATION = 3 
           OR CAR-OCCUPATION = 5 
           OR CAR-OCCUPATION = 10
           OR CAR-OCCUPATION = 11
           OR CAR-OCCUPATION = 13
           OR CAR-OCCUPATION = 14
              CONTINUE
           ELSE
              GO READ-PERSON-3
           END-IF.
           PERFORM CONTRAT THRU CONTRAT-END.
       READ-PERSON-2.
           CONTINUE.
       READ-PERSON-3.
           IF  REG-PERSON    < END-NUMBER
           AND REG-MATCHCODE < END-MATCHCODE
              GO READ-PERSON
           END-IF.
       READ-EXIT.

       CAR-RECENTE.
           INITIALIZE CAR-RECORD FICHE-RECORD.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NUL-KEY.
           IF CAR-STATUT = 0
              CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD NX-KEY.
      *    CALL "6-FICHE" USING LINK-V REG-RECORD FICHE-RECORD NUL-KEY.
           MOVE PR-LANGUAGE TO PAYS-LANGUE.
           MOVE PR-NATIONALITE TO PAYS-CODE.
           CALL "6-PAYS" USING LINK-V PAYS-RECORD FAKE-KEY.

       NEXT-REGIS.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF PRES-ANNEE = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.
 
       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       CONTRAT.
           INITIALIZE CON-RECORD.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE 12        TO CON-DEBUT-M.
           MOVE 32        TO CON-DEBUT-J.
           MOVE FR-KEY TO CON-FIRME.
           MOVE REG-PERSON TO CON-PERSON.
           START CONTRAT KEY < CON-KEY INVALID GO CONTRAT-END.
       CONTRAT-1.
           READ CONTRAT PREVIOUS NO LOCK AT END GO CONTRAT-END END-READ.
           IF FR-KEY     NOT = CON-FIRME 
           OR REG-PERSON NOT = CON-PERSON
              GO CONTRAT-END.
           IF CON-RECT-ENTREE = "X"
              INITIALIZE CON-DATE-NOTIFE
           END-IF.
           IF REEDITION NOT = "X"
              IF LNK-ANNEE  NOT = CON-DEBUT-A
                 GO CONTRAT-END
              END-IF
              IF REEDITION = "N"
              AND CON-NOTIFE-A > 0
                 GO CONTRAT-1
              END-IF
           END-IF.
           MOVE CON-DEBUT-M TO LNK-MOIS.
           MOVE TODAY-ANNEE TO CON-NOTIFE-A.
           MOVE TODAY-MOIS  TO CON-NOTIFE-M.
           MOVE TODAY-JOUR  TO CON-NOTIFE-J.
           PERFORM FULL-PROCESS.
           GO CONTRAT-1.
       CONTRAT-END.
           EXIT.

       FULL-PROCESS.
           PERFORM CAR-RECENTE.
           MOVE CAR-METIER TO MET-CODE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE CAR-METIER TO MET-CODE.
           MOVE PR-LANGUAGE TO MET-LANGUE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           IF MET-PROF > 1
           OR CAR-PROF > 1
              PERFORM TRANSMET.
           PERFORM DIS-HE-01.

           
       ENTREE-SORTIE.
           IF LNK-VAL > 1 
              MOVE "SL" TO LNK-AREA
              MOVE 2 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.


       DIS-HE-01.
           DISPLAY A-N LINE  4 POSITION 32.
           MOVE REG-PERSON  TO HE-Z6.
           DISPLAY HE-Z6 LINE 6 POSITION 27 SIZE 6
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 6 POSITION 47 SIZE 33.
           IF A-N = "N"
              DISPLAY SPACES LINE 6 POSITION 35 SIZE 10.
       DIS-HE-STAT.
           DISPLAY STAT-NOM  LINE 9 POSITION 35.
       DIS-HE-05.
           MOVE COUT-NUMBER TO HE-Z4.
           DISPLAY HE-Z4  LINE 10 POSITION 29.
           DISPLAY COUT-NOM LINE 10 POSITION 35.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1200 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           DISPLAY MENU-DESCRIPTION LINE 1 POSITION 3 LOW.
           DISPLAY SPACES LINE 18 POSITION 2 SIZE 70.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           PERFORM TEXTES.
           MOVE 0 TO LNK-VAL.
           MOVE PR-LANGUAGE TO LNK-LANGUAGE.
           CALL "ENBQC" USING LINK-V LINK-RECORD.
           MOVE SPACES TO CON-RECT-ENTREE.
QQ         REWRITE CON-RECORD INVALID CONTINUE.
           IF LNK-SQL = "Y" 
              CALL "9-CONTR" USING LINK-V CON-RECORD WR-KEY 
           END-IF.
           ADD 1 TO COUNTER.

       END-PROGRAM.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "ENBQC" USING LINK-V LINK-RECORD.
           MOVE 0 TO LNK-PERSON LNK-SUITE.
           MOVE SAVE-MOIS TO LNK-MOIS.
           MOVE SAVE-LANGUAGE TO LNK-LANGUAGE.
           CANCEL "ENBQC".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XALPHNUM.NXX".
           COPY "XSTATUT.CPY".
           COPY "XCOUT.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       TEXTES.
      *PR-MATR. 
           INITIALIZE LINK-FIELDS.
           MOVE 1 TO IDX.
           STRING FR-ETAB-A DELIMITED BY SIZE INTO LINK-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-ETAB-N DELIMITED BY SIZE INTO LINK-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-SNOCS DELIMITED BY SIZE INTO LINK-MATRICULE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-EXTENS DELIMITED BY SIZE INTO LINK-MATRICULE
           WITH POINTER IDX.


           MOVE PR-MAT(1)  TO LINK-MATA.
           MOVE PR-MAT(2)  TO LINK-MATB.
           MOVE PR-MAT(3)  TO LINK-MATC.
           MOVE PR-MAT(4)  TO LINK-MATD.
           MOVE PR-MAT(5)  TO LINK-MATE.
           MOVE PR-MAT(6)  TO LINK-MATF.
           MOVE PR-MAT(7)  TO LINK-MATG.
           MOVE PR-MAT(8)  TO LINK-MATH.
           IF PR-SNOCS > 2
              MOVE PR-MAT(9)  TO LINK-MATI
              MOVE PR-MAT(10) TO LINK-MATJ
              MOVE PR-MAT(11) TO LINK-MATK
           END-IF.
           IF CON-RECT-ENTREE = "X"
              MOVE "X" TO LINK-RECTIF
           END-IF.
      *    IF CAR-REGIME = 5
      *       MOVE "X" TO LINK-ETUDIANT
      *    END-IF.
           MOVE FR-NOM    TO  LINK-NOMEMPL.

           MOVE 1 TO IDX.
           STRING FR-MAISON DELIMITED BY "  " INTO LINK-RUEEMPL
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-RUE DELIMITED BY SIZE INTO LINK-RUEEMPL
           WITH POINTER IDX.

           MOVE 1 TO IDX.
           STRING FR-PAYS DELIMITED BY "  " INTO LINK-CPOSEMPL
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-CP-LUX DELIMITED BY SIZE INTO LINK-CPOSEMPL
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING FR-LOCALITE DELIMITED BY SIZE INTO LINK-CPOSEMPL
           WITH POINTER IDX.
           MOVE FR-PHONE TO LINK-TELEFON.

           MOVE PR-NOM TO LINK-NOM.
           IF PR-NOM-JF NOT = SPACES
              MOVE PR-NOM-JF TO LINK-NOM
              MOVE PR-NOM TO LINK-NOMMARI
           END-IF.
           MOVE PR-PRENOM TO LINK-PRENOM.


           MOVE 1 TO IDX.
           STRING PR-MAISON DELIMITED BY "  " INTO LINK-RUE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING PR-RUE DELIMITED BY SIZE INTO LINK-RUE
           WITH POINTER IDX.

           MOVE 1 TO IDX.
           STRING PR-PAYS DELIMITED BY "  " INTO LINK-LOCAL
           WITH POINTER IDX.
           ADD 1 TO IDX.
           IF PR-PAYS = "D"
           OR PR-PAYS = "F"
           OR PR-PAYS = "I"
           OR PR-PAYS = "CZ"
              STRING PR-CP5 DELIMITED BY SIZE INTO LINK-LOCAL 
              WITH POINTER IDX END-STRING
           ELSE
              STRING PR-CP4 DELIMITED BY SIZE INTO LINK-LOCAL 
              WITH POINTER IDX END-STRING
           END-IF.
           ADD 1 TO IDX.
           STRING PR-LOCALITE DELIMITED BY SIZE INTO LINK-LOCAL
           WITH POINTER IDX.

           MOVE CON-DEBUT-J TO LINK-JOUENT.
           MOVE CON-DEBUT-M TO LINK-MOIENT.
           MOVE CON-DEBUT-A TO LINK-ANNENT.
           COMPUTE HEURES = CAR-HRS-JOUR * CAR-JRS-SEMAINE.
           MOVE HEURES TO LINK-HRSSEM.

           MOVE MET-NOM(PR-CODE-SEXE) TO LINK-ACTPRE.
           IF LINK-ACTPRE = SPACES
              MOVE CAR-POSITION TO LINK-ACTPRE
           END-IF.
           IF CAR-STATEC = 1 
              MOVE "X" TO LINK-APPRENTI 
           END-IF.
           MOVE PAYS-NATION TO LINK-NATION.
           MOVE FR-LOCALITE TO LINK-LIEU.
      *    MOVE SIGNATAIRE  TO LINK-SIGNATAIRE.
           CALL "0-TODAY" USING TODAY.
           EVALUATE CAR-OCCUPATION
              WHEN  1 MOVE "X" TO LINK-REGIME-01
              WHEN  3 MOVE "X" TO LINK-REGIME-03
              WHEN 10 MOVE "X" TO LINK-REGIME-10
              WHEN 11 MOVE "X" TO LINK-REGIME-11
              WHEN 13 MOVE "X" TO LINK-REGIME-13
              WHEN 14 MOVE "X" TO LINK-REGIME-14
              WHEN  5 MOVE "X" TO LINK-REGIME-05
           END-EVALUATE.
           MOVE 1 TO IDX.
           STRING TODAY-JOUR DELIMITED BY SIZE INTO LINK-DATE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING TODAY-MOIS DELIMITED BY SIZE INTO LINK-DATE
           WITH POINTER IDX.
           ADD 1 TO IDX.
           STRING TODAY-ANNEE DELIMITED BY SIZE INTO LINK-DATE
           WITH POINTER IDX.

           MOVE CAR-PROF TO LINK-CODEMP.
           IF CAR-PROF < 1
              MOVE MET-PROF TO LINK-CODEMP.
           IF CON-FIN-A > 0
              MOVE CON-FIN-J TO LINK-JOUR-FIN
              MOVE CON-FIN-M TO LINK-MOIS-FIN
              MOVE CON-FIN-A TO LINK-ANNEE-FIN
              MOVE "X" TO LINK-DETERMINE
           END-IF.
           IF CAR-METIER = "STAGE"
              IF  CAR-SAL-ACT = 0
              AND CAR-INDEXE  = "N"
                 MOVE "X" TO LINK-STAGE-NI
              ELSE
                 MOVE "X" TO LINK-STAGE-IN
              END-IF
           END-IF.
           IF CAR-METIER = "ET"
              MOVE "X" TO LINK-ETUDIANT
           END-IF.
              
           MOVE 1 TO IDX-1.
           PERFORM FILL-PAYS VARYING IDX FROM 1 BY 1 UNTIL IDX > 7.

       FILL-PAYS.
           IF REG-PAYS(IDX) > SPACES 
              EVALUATE REG-PAYS(IDX) 
                WHEN "L"  MOVE "X" TO LINK-LIEU-LU
                WHEN "D"  MOVE "X" TO LINK-LIEU-DE
                WHEN "B"  MOVE "X" TO LINK-LIEU-BE
                WHEN "F"  MOVE "X" TO LINK-LIEU-FR
                WHEN "NL" MOVE "X" TO LINK-LIEU-NL
                WHEN OTHER PERFORM STRING-PAYS
              END-EVALUATE
           END-IF.
           IF REG-PAYS(1) = SPACES
              MOVE "X" TO LINK-LIEU-LU
           END-IF.
           MOVE FR-CODE-POST TO HE-Z4.
           MOVE HE-Z4 TO LINK-LIEU-CPOST.
           MOVE FR-LOCALITE TO LINK-LIEU-LOCALITE.
           IF REG-CDPOST > SPACES
              MOVE REG-CDPOST TO LINK-LIEU-CPOST
           END-IF.
           IF REG-LOCALITE > SPACES
              MOVE REG-LOCALITE TO LINK-LIEU-LOCALITE
           END-IF.
           IF CAR-POSTE-FRAIS NOT = 0
              MOVE CAR-POSTE-FRAIS TO PC-NUMBER
              CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY
              IF PC-CODE-POST > 0
              AND PC-LOCALITE > SPACES
                 MOVE PC-CODE-POST TO LINK-LIEU-CPOST
                 MOVE PC-LOCALITE  TO LINK-LIEU-LOCALITE
              END-IF
           END-IF.


       STRING-PAYS.
           STRING REG-PAYS(IDX) DELIMITED BY SPACES INTO LINK-LIEU-AUTRE
           WITH POINTER IDX-1.
           ADD 2 TO IDX-1.
