      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-PRIX MODULE GENERAL LECTURE PRIX          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-PRIX.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PRIX.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "PRIX.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "ARTICLE.REC".
           COPY "PRIX.LNK".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD ART-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PRIX.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-REG.
       
           IF NOT-OPEN = 0
              OPEN I-O PRIX
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO PX-RECORD.
           MOVE ART-NUMBER  TO PX-ARTICLE.
           IF PX-ANNEE = 0
              MOVE LNK-ANNEE TO PX-ANNEE
              MOVE LNK-MOIS  TO PX-MOIS
           END-IF.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ PRIX PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ PRIX NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 0 PERFORM START-3
               READ PRIX PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ PRIX NO LOCK INVALID INITIALIZE PX-REC-DET
                END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF ART-NUMBER NOT = PX-ARTICLE
              GO EXIT-1
           END-IF.
           MOVE PX-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START PRIX KEY < PX-KEY INVALID GO EXIT-1.
       START-2.
           START PRIX KEY > PX-KEY INVALID GO EXIT-1.
       START-3.
           START PRIX KEY <= PX-KEY INVALID GO EXIT-1.


