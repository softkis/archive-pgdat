      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME U-CIS MODIF PLAN CIS          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    U-CIS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PLAN.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴
           COPY "PLAN.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

           COPY "LIBELLE.REC".
           COPY "V-VAR.CPY".

       LINKAGE SECTION.

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON PLAN.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.

       END DECLARATIVES.

       START-DISPLAY SECTION.

       START-PROGRAMME-U-CIS.
       
           OPEN I-O PLAN.
           INITIALIZE PLAN-RECORD LIB-RECORD.
           START PLAN KEY >= PLAN-KEY INVALID GO PLAN-END.
           PERFORM PLAN THRU PLAN-END.
           MOVE 31  TO LIB-NUMBER.
           MOVE "F" TO LIB-LANGUAGE.
           MOVE "Cr괺it d'imp뱓" TO LIB-DESCRIPTION.
           CALL "6-LIB" USING LINK-V LIB-RECORD WR-KEY.
           MOVE "D" TO LIB-LANGUAGE.
           MOVE "Steuerbonus" TO LIB-DESCRIPTION.
           CALL "6-LIB" USING LINK-V LIB-RECORD WR-KEY.
           MOVE "E" TO LIB-LANGUAGE.
           MOVE "Tax credit" TO LIB-DESCRIPTION.
           CALL "6-LIB" USING LINK-V LIB-RECORD WR-KEY.
           EXIT PROGRAM.

       PLAN.
           READ PLAN NEXT NO LOCK AT END GO PLAN-END.
           PERFORM WRITE-PLAN.
           GO PLAN.
       PLAN-END.
           EXIT.

       WRITE-PLAN.
           MOVE PLAN-COM(133) TO PLAN-COM(198).
           MOVE 31 TO PLAN-LIB(198).
           WRITE PLAN-RECORD INVALID REWRITE PLAN-RECORD.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XACTION.CPY".

