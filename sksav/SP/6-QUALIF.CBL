      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-QUALIF MODULE GENERAL LECTURE QUALIF      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-QUALIF.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "QUALIF.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "QUALIF.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "QUALIF.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING  LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON QUALIF.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-QUALIF.
       
           IF NOT-OPEN = 0
              OPEN INPUT QUALIF
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO Q-RECORD.
           MOVE LNK-NUM TO Q-TYPE.
           MOVE 0 TO LNK-VAL.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ QUALIF PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ QUALIF NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ QUALIF NO LOCK INVALID INITIALIZE Q-REC-DET 
           END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF LNK-NUM NOT = Q-TYPE 
              GO EXIT-1
           END-IF.
           MOVE Q-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START QUALIF KEY < Q-KEY INVALID GO EXIT-1.
       START-2.
           START QUALIF KEY > Q-KEY INVALID GO EXIT-1.


