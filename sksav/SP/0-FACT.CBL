      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-FACT FACTURE + ARTICLES                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-FACT.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FACTURE.FC".
           COPY "FACTLINE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FACTURE.FDE".
           COPY "FACTLINE.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX             PIC 99 VALUE 13.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "POCL.REC".
           COPY "ARTICLE.REC".
           COPY "ECHEANCE.REC".
           COPY "PARMOD.REC".
           COPY "PRIX.REC".
           COPY "TVA.REC".

       01  COMPTEUR              PIC 99 COMP-1.
       01  MODIFICATION          PIC 9 VALUE 0.
       01  LAST-FIRME            PIC 9(6) VALUE 0.
       01  LAST-MOIS             PIC 9(2) VALUE 0.
       01  DATE-MAJ        PIC 9(8).
       01  DATE-MAJ-R REDEFINES DATE-MAJ.
           02 D-MAJ-A      PIC 9999.
           02 D-MAJ-M      PIC 99.
           02 D-MAJ-J      PIC 99.

       01  SAL-ARR               PIC 9(8)V99.
       01  SAL-TOT               PIC 9(8)V9(5).
       01  SAL-ACT               PIC S9(8)V9(5).
       01  SAL-ACT-R REDEFINES SAL-ACT.
           02 SAL-ACT-A          PIC S9(8).
           02 SAL-ACT-B          PIC 9(5).
       01  IND-TEXTE             PIC X(25).

       01  VAL-HLP-08.
           02 VH-08          PIC 9(8)V99999 OCCURS 6.

       01  H-Z1.
           03 FILLER         PIC Z(7).
           03 ZZ-01          PIC Z,Z(5).
       01  H-Z2 REDEFINES H-Z1.
           03 FILLER         PIC Z(6).
           03 ZZ-02          PIC ZZ,Z(5).
       01  H-Z3 REDEFINES H-Z1.
           03 FILLER         PIC Z(5).
           03 ZZ-03          PIC ZZZ,Z(5).
       01  H-Z4 REDEFINES H-Z1.
           03 FILLER         PIC Z(4).
           03 ZZ-04          PIC Z(4),Z(5).
       01  H-Z5 REDEFINES H-Z1.
           03 FILLER         PIC Z(3).
           03 ZZ-05          PIC Z(5),Z(5).
       01  H-Z6 REDEFINES H-Z1.
           03 FILLER         PIC ZZ.
           03 ZZ-06          PIC Z(6),Z(5).
       01  H-Z7 REDEFINES H-Z1.
           03 FILLER         PIC Z.
           03 ZZ-07          PIC Z(7),Z(5).
       01  H-Z8 REDEFINES H-Z1.
           03 ZZ-08          PIC Z(8),Z(5).

       01  HELP-VAL              PIC Z BLANK WHEN ZERO.
       01  HELP-1                PIC 9999.
       01  HELP-2                PIC 9.
       01  HELP-3                PIC S9(6)V99 COMP-3.

       01  FACTURE-NAME.
           02 IDENTITE           PIC X(4) VALUE "A-FA".
           02 FIRME-FACTURE      PIC 9999.
       01  FAL-NAME.
           02 IDENTITE           PIC X(4) VALUE "A-FD".
           02 FIRME-FAL          PIC 9999.

       01   ECR-DISPLAY.
            02 HE-Z2 PIC ZZ  BLANK WHEN ZERO.
            02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
            02 HE-Z4 PIC Z(4).
            02 HE-Z6 PIC Z(6).
            02 HE-Z8 PIC Z(8)-.
            02 HE-Z2Z2 PIC ZZ,ZZ. 
            02 HE-Z4Z2 PIC -ZZZZ,ZZ BLANK WHEN ZERO. 
            02 HE-Z6Z2 PIC Z(6),ZZ. 
            02 HE-Z6Z4 PIC Z(6),Z(4). 
            02 HE-Z8Z5 PIC Z(8),Z(5) BLANK WHEN ZERO.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FACTURE FAL.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-FACT .
       
           INITIALIZE PARMOD-RECORD TVA-RECORD FAL-RECORD PC-RECORD
           LNK-PRESENCE.
           CALL "6-TVA" USING LINK-V TVA-RECORD NUL-KEY.
           MOVE FR-KEY TO PARMOD-FIRME.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE FR-KEY TO FIRME-FACTURE FIRME-FAL.
           OPEN I-O  FACTURE.
           OPEN I-O  FAL.
           CALL "0-TODAY" USING TODAY.
           PERFORM AFFICHAGE-ECRAN .
           MOVE 0 TO MODIFICATION.
           INITIALIZE DATE-MAJ.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0063640415 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 2  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 3  MOVE 0163640015 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 5  MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
              WHEN 6  MOVE 0164002400 TO EXC-KFR(1)
                      MOVE 4100081774 TO EXC-KFR(2)
                      MOVE 0000130000 TO EXC-KFR(3)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600680000 TO EXC-KFR(14)
           WHEN 13 MOVE 0100000005 TO EXC-KFR(1)
                   MOVE 1900080000 TO EXC-KFR(2)
                   MOVE 0052000000 TO EXC-KFR(11)
                   MOVE 0000680000 TO EXC-KFR(14)
                   IF MENU-BATCH = 1
                      MOVE 1200080037 TO EXC-KFR(2)
                   END-IF
           END-EVALUATE.

           IF INDICE-ZONE < 7 OR INDICE-ZONE > 12
               PERFORM DISPLAY-F-KEYS
           ELSE
               COMPUTE IDX-1 = INDICE-ZONE - 6
               IF ART-SAISIE(IDX-1) > 0 
                  PERFORM DISPLAY-F-KEYS
               END-IF
           END-IF.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-6 
           WHEN 13 PERFORM AVANT-DEC
           WHEN OTHER PERFORM AVANT-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           IF EXC-KEY NOT = 27
           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6 
           WHEN 13 PERFORM APRES-DEC
           WHEN OTHER PERFORM APRES-ALL.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT PC-NUMBER 
              LINE 3 POSITION 15 SIZE 6
              TAB UPDATE NO BEEP CURSOR 1
              ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-2.
           ACCEPT PC-MATCHCODE
             LINE 3 POSITION 30 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE 0 TO LNK-PERSON.

       AVANT-3.
           ACCEPT FAC-NUMBER
             LINE 4 POSITION 20 SIZE 8
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION EXC-KEY CONTINUE.


       AVANT-4.
           MOVE 05200000 TO LNK-POSITION.
           CALL "0-GDATE" USING DATE-MAJ
                                LNK-POSITION
                                LNK-NUM
                                EXC-KEY.
           PERFORM DIS-HE-04.

       AVANT-5.
           IF FAC-MOIS = 0
              MOVE LNK-MOIS TO FAC-MOIS
           END-IF.
           ACCEPT FAC-MOIS
             LINE 6 POSITION 26 SIZE 2
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.

       AVANT-6.            
           ACCEPT ART-NUMBER
             LINE  7 POSITION 24 SIZE 4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
      *    PERFORM RECHARGE VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.
           COMPUTE IDX = INDICE-ZONE - 6.
           IF ART-PROCEDURE(IDX) NOT = 0
           AND FAL-DONNEE(IDX) = 0
              PERFORM C-PROCEDURE
              PERFORM RECHARGE.
           COMPUTE LIN-IDX = IDX + 7.
           COMPUTE COL-IDX = 24 - ART-COLONNES(IDX).
           COMPUTE SIZ-IDX = ART-DECIMALES(IDX) + ART-COLONNES(IDX).
           IF ART-DECIMALES(IDX) > 0 ADD 1 TO SIZ-IDX.
           IF ART-SAISIE(IDX) > 0 
              PERFORM AVANT-ALL-1
           END-IF.
           IF IDX = 6
              IF EXC-KEY = 5 
                 MOVE VH-08(6) TO FAL-TOTAL
                 COMPUTE FAL-UNITE = FAL-TOTAL / FAL-UNITAIRE
                 MOVE FAL-UNITE TO VH-08(4)
                 PERFORM DIS-HE-ALL
              END-IF
              IF EXC-KEY = 6 
                 MOVE VH-08(6) TO FAL-TOTAL
                 COMPUTE FAL-UNITAIRE = FAL-TOTAL / FAL-UNITE
                 MOVE FAL-UNITAIRE TO VH-08(5)
                 PERFORM DIS-HE-ALL
              END-IF
           END-IF.
             

       RECHARGE.
           MOVE FAL-DONNEE(IDX) TO VH-08(IDX).

       AVANT-ALL-1.
           IF FAL-DONNEE(IDX) = 0
           AND ART-VAL-PREDEFINIE(IDX) NOT = 0
              MOVE ART-VAL-PREDEFINIE(IDX) TO VH-08(IDX).
           MOVE VH-08(IDX) TO ZZ-08.
           COMPUTE SIZ-IDX = ART-DECIMALES(IDX) + ART-COLONNES(IDX).
           IF ART-DECIMALES(IDX) > 0 ADD 1 TO SIZ-IDX.
           EVALUATE ART-COLONNES(IDX)
           WHEN 1 ACCEPT ZZ-01
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 2 ACCEPT ZZ-02
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           WHEN 3 ACCEPT ZZ-03
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 4 ACCEPT ZZ-04
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           WHEN 5 ACCEPT ZZ-05
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 6 ACCEPT ZZ-06
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           WHEN 7 ACCEPT ZZ-07
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE 
             END-ACCEPT
           WHEN 8 ACCEPT ZZ-08
             LINE LIN-IDX POSITION COL-IDX SIZE SIZ-IDX
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE
             END-ACCEPT
           END-EVALUATE.
           INSPECT ZZ-08 REPLACING ALL "." BY ",".
           MOVE ZZ-08 TO VH-08(IDX).

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY
           WHEN   5 CALL "1-CLIENT" USING LINK-V
                    IF LNK-VAL > 0
                       MOVE LNK-VAL TO PC-NUMBER
                    END-IF
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "1-CLIENT"
           WHEN   2 THRU 3 
                    IF EXC-KEY = 2 
                       MOVE "A" TO LNK-A-N
                    ELSE
                       MOVE "N" TO LNK-A-N
                    END-IF
                    CALL "2-POCL" USING LINK-V PC-RECORD
                    PERFORM AFFICHAGE-ECRAN 
                    PERFORM AFFICHAGE-DETAIL
                    CANCEL "2-POCL"
           WHEN OTHER MOVE "N" TO A-N
                      PERFORM NEXT-POCL
           END-EVALUATE.
           IF PC-NUMBER = 0
           OR LNK-STATUS > 0
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR = 0
           AND PC-NUMBER > 0 
              INITIALIZE FAL-RECORD VAL-HLP-08 
              PERFORM AFFICHAGE-DETAIL.

       APRES-2.
           MOVE "A" TO A-N 
           EVALUATE EXC-KEY
           WHEN 27 MOVE 0 TO LNK-PERSON
           WHEN 13 CONTINUE
           WHEN OTHER PERFORM NEXT-POCL
           END-EVALUATE.                     
           IF PC-NUMBER = 0
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-01.

       APRES-3.
           IF FAC-NUMBER = 0
              PERFORM NEXT-NUMBER
           END-IF.
           EVALUATE EXC-KEY
              WHEN 5 PERFORM NEXT-NUMBER
              WHEN 65 PERFORM FACTURE-RECENTE
              WHEN 66 PERFORM FACTURE-SUIVANTE
           END-EVALUATE.
           READ FACTURE NO LOCK INVALID INITIALIZE FAC-REC-DET
                MOVE PC-NUMBER TO FAC-CLIENT 
                NOT INVALID MOVE FAC-EDIT TO DATE-MAJ             
                END-READ.
           MOVE FAC-CLIENT TO PC-NUMBER.
           PERFORM READ-POCL.
           PERFORM DIS-HE-01 THRU DIS-HE-04.

--     APRES-5.
           IF EXC-KEY = 65
              SUBTRACT 1 FROM FAC-MOIS.
           IF EXC-KEY = 66
              ADD 1 TO FAC-MOIS.
           IF FAC-MOIS < 1
              MOVE 1 TO FAC-MOIS 
           END-IF.
           IF FAC-MOIS > 12
              MOVE 12 TO FAC-MOIS 
           END-IF.
           MOVE LNK-ANNEE TO FAC-ANNEE. 
           PERFORM DIS-HE-05.

--     APRES-6.
           MOVE 0 TO INPUT-ERROR.
           MOVE EXC-KEY TO SAVE-KEY.
           EVALUATE EXC-KEY
           WHEN 1 MOVE 01001001 TO LNK-VAL
                  PERFORM HELP-SCREEN
                  MOVE 1 TO INPUT-ERROR
           WHEN 4 PERFORM RECAP-FAL
                  PERFORM RECHARGE VARYING IDX FROM 1 BY 1 UNTIL 
                  IDX > 6
                  PERFORM DIS-HE-ALL
                  MOVE 13 TO SAVE-KEY
                  PERFORM NEXT-CS 
                  PERFORM DIS-HE-06 
           WHEN 6 PERFORM READ-FAL
                  PERFORM ART-TEXT THRU ART-TEXT-END 
                  MOVE 1 TO INPUT-ERROR
           WHEN 2 CALL "2-ARTIC" USING LINK-V ART-RECORD
                  PERFORM AFFICHAGE-ECRAN 
                  IF ART-NUMBER > 0
                     PERFORM LOAD-FAL
                  END-IF
                  PERFORM AFFICHAGE-DETAIL
                  PERFORM DISPLAY-F-KEYS
                  MOVE 1 TO INPUT-ERROR
                  CANCEL "2-ARTIC" 
           WHEN  8 PERFORM DEL-FAL
           WHEN  9 IF MODIFICATION = 1
                     PERFORM CALCUL
                     INITIALIZE FAL-RECORD
                  END-IF
                  MOVE PC-NUMBER TO LNK-PERSON
                  CALL "2-MISFAC" USING LINK-V
                  IF LNK-PERSON NOT = PC-NUMBER
                     MOVE LNK-PERSON TO PC-NUMBER
                     MOVE 13 TO EXC-KEY
                     MOVE 0 TO LNK-PERSON
                  END-IF
                  CANCEL "2-MISFAC"
                  PERFORM AFFICHAGE-ECRAN 
                  PERFORM DIS-HE-01 THRU DIS-HE-03
                  MOVE 1 TO INPUT-ERROR
                  MOVE 98 TO EXC-KEY
           WHEN OTHER PERFORM NEXT-CS 
                  PERFORM GET-FAL THRU GET-FAL-END
                  PERFORM RECHARGE VARYING IDX FROM 1 BY 1 UNTIL 
                  IDX > 6
                  PERFORM DIS-HE-ALL
           END-EVALUATE.
           IF ART-NUMBER = 0
              MOVE 1 TO INPUT-ERROR
           ELSE       
              PERFORM TEST-CS.
           PERFORM DIS-HE-06.
      
       LOAD-FAL.
           PERFORM READ-FAL.
           IF IND-TEXTE NOT = SPACES
              MOVE IND-TEXTE TO FAL-TEXTE
              INITIALIZE IND-TEXTE.
           PERFORM RECHARGE VARYING IDX FROM 1 BY 1 UNTIL IDX > 6.
           PERFORM DIS-HE-06.
           PERFORM DIS-HE-ALL.


       READ-FAL.
           IF INPUT-ERROR = 0
              PERFORM FAL-KEY
              READ FAL NO LOCK INVALID
                   INITIALIZE FAL-REC-DET
              END-READ
           END-IF.    

       TEST-CS.
           MOVE 0 TO LNK-NUM INPUT-ERROR.
           IF ART-NUMBER = 0 
              MOVE 1 TO LNK-NUM.
           IF LNK-NUM NOT = 0 
              IF LNK-NUM = 1 
                 MOVE "AA" TO LNK-AREA
              ELSE
                 MOVE "SL" TO LNK-AREA
              END-IF
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
              MOVE 0 TO LNK-NUM.

       SET-UP.
           PERFORM VAL-PREDEFINIE.
           PERFORM C-PROCEDURE.
           PERFORM MINI-MAX.


       VAL-PREDEFINIE.
           IF FAL-DONNEE(IDX) = 0
           AND ART-VAL-PREDEFINIE(IDX) NOT = 0
              MOVE ART-VAL-PREDEFINIE(IDX) TO FAL-DONNEE(IDX).

       C-PROCEDURE.
           IF ART-PROCEDURE(IDX) > 0
              MOVE ART-PROCEDURE(IDX) TO LNK-NUM
              MOVE IDX TO LNK-INDEX
              CALL "4-PROCFA" USING LINK-V ART-RECORD 
              PX-RECORD FAL-RECORD PC-RECORD TVA-RECORD.

       APRES-ALL.
           COMPUTE IDX = INDICE-ZONE - 6.
           MOVE VH-08(IDX) TO FAL-DONNEE(IDX).
           PERFORM MINI-MAX.
           PERFORM DIS-HE-ALL.

       APRES-DEC.
           EVALUATE EXC-KEY 
           WHEN 5 THRU 6 
           PERFORM FAL-KEY
                  CALL "0-TODAY" USING TODAY
                  MOVE TODAY-TIME TO FAL-TIME
                  MOVE LNK-USER TO FAL-USER
                  PERFORM WRITE-FAL
                  INITIALIZE VAL-HLP-08 
      *           INITIALIZE FAL-RECORD ART-RECORD VAL-HLP-08 
                  MOVE 1 TO MODIFICATION
                  MOVE 7  TO DECISION
                  PERFORM DIS-HE-03 THRU DIS-HE-END
                  IF EXC-KEY = 6
                     MOVE 1 TO DECISION
                  END-IF
           WHEN 8 PERFORM DEL-FAL
                  MOVE 7 TO DECISION
                  PERFORM DIS-HE-03 THRU DIS-HE-END.
           IF DECISION < 3 AND MODIFICATION = 1
              PERFORM CALCUL
              INITIALIZE FAC-RECORD
           END-IF.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       CALCUL.
           CANCEL "6-FAL".
           INITIALIZE FAL-RECORD FAC-REC-DET.
           MOVE FAC-NUMBER TO FAL-NUMBER.
           START FAL KEY > FAL-KEY-3 INVALID CONTINUE
              NOT INVALID
           PERFORM READ-FAC-DET THRU READ-FAL-END.
           PERFORM TVA VARYING IDX FROM 1 BY 1 UNTIL IDX > 20.
           COMPUTE FAC-A-PAYER = FAC-TOTAL-TVA + FAC-TOTAL.

           MOVE TODAY-DATE TO  FAC-ECHEANCE.
           IF DATE-MAJ > 0
              MOVE DATE-MAJ TO FAC-ECHEANCE
           END-IF.
           IF ECH-FIN-MOIS = 1
              MOVE MOIS-JRS(TODAY-MOIS) TO FAC-ECHEA-M
           END-IF.
           ADD ECH-JOURS TO FAC-ECHEA-J.
           IF FAC-ECHEA-J > MOIS-JRS(TODAY-MOIS)
              SUBTRACT MOIS-JRS(TODAY-MOIS) FROM FAC-ECHEA-J
              ADD 1 TO FAC-ECHEA-M
           END-IF.
           ADD ECH-MOIS TO FAC-ECHEA-M.
           IF FAC-ECHEA-M > 12
              SUBTRACT 12 FROM FAC-ECHEA-M
              ADD 1 TO FAC-ECHEA-A
           END-IF.
           MOVE DATE-MAJ TO FAC-EDIT.
           MOVE LNK-ANNEE TO FAC-ANNEE.
           MOVE LNK-MOIS  TO FAC-MOIS.
       
           IF FAC-TOTAL > 0
              WRITE FAC-RECORD INVALID REWRITE FAC-RECORD.
           INITIALIZE FAC-RECORD.
           PERFORM AFFICHAGE-DETAIL.

       READ-FAC-DET.
           READ FAL NEXT NO LOCK AT END 
              GO READ-FAL-END.
           IF FAC-NUMBER NOT = FAL-NUMBER 
              GO READ-FAL-END.
           MOVE FAL-CODE   TO ART-NUMBER.
           CALL "6-ARTIC" USING LINK-V ART-RECORD FAKE-KEY.
           IF ART-NEGATIF = "-"
              PERFORM MINUS-
           ELSE
              PERFORM PLUS-.

           GO READ-FAC-DET.
       READ-FAL-END.
           EXIT.

       PLUS-.
           IF ART-TVA > 0
              IF PC-TVA-PAYS = "LU"
              OR PC-TVA-PAYS = "  "
                 ADD FAL-TOTAL TO FAC-BTVA(ART-TVA)
              ELSE
                 COMPUTE IDX-1 = ART-TVA + 10
                 ADD FAL-TOTAL TO FAC-BTVA(IDX-1)
              END-IF
           END-IF.
           ADD FAL-TOTAL TO FAC-TOTAL.

       MINUS-.
           IF ART-TVA > 0
              IF PC-TVA-PAYS = "LU"
              OR PC-TVA-PAYS = "  "
                 SUBTRACT FAL-TOTAL FROM FAC-BTVA(ART-TVA)
              ELSE
                 COMPUTE IDX-1 = ART-TVA + 10
                 SUBTRACT FAL-TOTAL FROM FAC-BTVA(IDX-1)
              END-IF
           END-IF.
           SUBTRACT FAL-TOTAL FROM FAC-TOTAL.

       NEXT-POCL.
           IF FR-CLIENT-YN = "N"
              MOVE 0 TO PC-FIRME PC-FIRME-C PC-FIRME-A
           ELSE
              MOVE FR-KEY TO PC-FIRME PC-FIRME-C PC-FIRME-A
           END-IF.
           CALL "6-POCL" USING LINK-V PC-RECORD A-N EXC-KEY.
           IF  PC-NUMBER  > 0
           AND LNK-STATUS > 0
           AND EXC-KEY NOT = 13
              GO NEXT-POCL.

       READ-POCL.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY.
           
       NEXT-CS.
           MOVE 0 TO INPUT-ERROR.
           CALL "6-ARTIC" USING LINK-V ART-RECORD SAVE-KEY.
           INITIALIZE PX-RECORD.
           CALL "6-PRIX"  USING LINK-V PX-RECORD ART-RECORD NUL-KEY.
           IF ART-NUMBER NOT = 0
              PERFORM TEST-CS
              IF INPUT-ERROR = 1
              AND SAVE-KEY NOT = 13
                 GO NEXT-CS
              END-IF
           END-IF.

       RECAP-FAL.
           MOVE FAC-NUMBER TO FAL-NUMBER.
      *    PERFORM FAL-KEY.
           START FAL KEY > FAL-KEY-3
                INVALID INITIALIZE FAL-RECORD 
                NOT INVALID READ FAL NEXT NO LOCK AT END
                INITIALIZE FAL-RECORD END-READ.
           IF FAL-NUMBER NOT = FAC-NUMBER
              INITIALIZE FAL-RECORD ART-NUMBER
           ELSE
              ADD 1 TO COMPTEUR
              MOVE FAL-CODE   TO ART-NUMBER
              MOVE FAL-CLIENT TO PC-NUMBER
              MOVE FAL-NUMBER TO FAC-NUMBER
              PERFORM READ-POCL.
           PERFORM DIS-HE-04.

       NEXT-FAL.
           PERFORM FAL-KEY.
           START FAL KEY > FAL-KEY-3 INVALID GO NEXT-FAL-END.
       NEXT-FAL-1.
           READ FAL NEXT NO LOCK AT END
              GO NEXT-FAL-END
           END-READ.
           IF FAC-NUMBER NOT = FAL-NUMBER
              GO NEXT-FAL-END
           END-IF.
           ADD 1 TO COMPTEUR.
           MOVE FAL-CODE TO ART-NUMBER.
           MOVE 13 TO SAVE-KEY.
           PERFORM NEXT-CS.
           GO NEXT-FAL-1.
       NEXT-FAL-END.
           EXIT.

       DIS-HE-01.
           MOVE PC-NUMBER  TO HE-Z6.
           DISPLAY HE-Z6 LINE 3 POSITION 15 SIZE 6
           DISPLAY PC-NOM LINE 3 POSITION 47 SIZE 33.

       DIS-HE-02.
           DISPLAY PC-MATCHCODE LINE 3 POSITION 30.
       DIS-HE-03.
           MOVE FAC-NUMBER TO HE-Z8.
           DISPLAY HE-Z8  LINE 4 POSITION 20.
       DIS-HE-04.
           MOVE D-MAJ-A TO HE-AA.
           IF HE-AA = SPACES
              INITIALIZE DATE-MAJ.
           MOVE D-MAJ-M TO HE-MM.
           MOVE D-MAJ-J TO HE-JJ.
           DISPLAY HE-DATE LINE 5 POSITION 20.
       DIS-HE-05.
           MOVE FAC-MOIS TO HE-Z2 LNK-NUM.
           DISPLAY HE-Z2 LINE 6 POSITION 26.
           MOVE "MO" TO LNK-AREA.
           MOVE 06301200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       DIS-HE-06.
           MOVE ART-NUMBER    TO HE-Z4.
           DISPLAY HE-Z4  LINE  7 POSITION 24.
           IF FAL-TEXTE = SPACES
              DISPLAY ART-NOM LINE 7 POSITION 30 SIZE 50
           ELSE
              DISPLAY FAL-TEXTE LINE 7 POSITION 30 SIZE 50
           END-IF.
           PERFORM DISPLAY-TEXTE VARYING IDX-2 FROM 1 BY 1
           UNTIL IDX-2 > 6.
       DIS-HE-END.

       DIS-HE-ALL.
           PERFORM DIS-VAL VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 6.


       DIS-VAL.
           COMPUTE LIN-IDX = IDX-1 + 7.
           DISPLAY SPACES LINE LIN-IDX POSITION 16 SIZE 20.
           MOVE 24 TO COL-IDX.
           IF ART-DECIMALES(IDX-1) > 0
              COMPUTE COL-IDX = 25 + ART-DECIMALES(IDX-1).
           MOVE VH-08(IDX-1) TO HE-Z8Z5.
           DISPLAY HE-Z8Z5 LINE LIN-IDX POSITION 16.
           DISPLAY "      " LINE LIN-IDX POSITION COL-IDX.

       DISPLAY-TEXTE.
           COMPUTE LIN-IDX = IDX-2 + 7.
           DISPLAY ART-DESCRIPTION(IDX-2) LINE LIN-IDX POSITION 5 
           SIZE 10 LOW.


       ART-TEXT.
           MOVE FAL-TEXTE TO IND-TEXTE.
           MOVE 0000000000 TO EXC-KFR(1).
           MOVE 0000080000 TO EXC-KFR(2).
           PERFORM DISPLAY-F-KEYS.
           ACCEPT IND-TEXTE LINE 5 POSITION 30 SIZE 35
           TAB UPDATE NO BEEP CURSOR 1
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO ART-TEXT.
           IF EXC-KEY = 98 GO ART-TEXT.
           MOVE IND-TEXTE TO FAL-TEXTE.
           REWRITE FAL-RECORD INVALID CONTINUE 
           NOT INVALID INITIALIZE IND-TEXTE
           END-REWRITE.
           PERFORM DIS-HE-03.
       ART-TEXT-END.
           EXIT.

       WRITE-FAL.
           IF FAL-UNITE NOT = 0 
           OR FAL-UNITAIRE NOT = 0 
           OR FAL-TOTAL NOT = 0
              IF FAL-CODE NOT = 0
                 MOVE 1 TO MODIFICATION
                 MOVE LNK-ANNEE TO FAL-ANNEE
                 MOVE LNK-MOIS  TO FAL-MOIS
                 WRITE FAL-RECORD INVALID REWRITE FAL-RECORD END-WRITE
      *        CALL "6-FAL" USING LINK-V FAL-RECORD PC-RECORD WR-KEY
              END-IF
           END-IF.

       DEL-FAL.
           CANCEL "6-FAL".
           PERFORM FAL-KEY.
           CALL "6-FAL" USING LINK-V FAL-RECORD PC-RECORD DEL-KEY.
           MOVE 1 TO MODIFICATION.
           INITIALIZE FAL-RECORD.

       FAL-KEY.
      *    INITIALIZE FAL-RECORD.
           MOVE FAC-NUMBER TO FAL-NUMBER FAL-NUM-1.
           MOVE ART-NUMBER TO FAL-CODE.
           MOVE PC-NUMBER  TO FAL-CLIENT.


       MINI-MAX.
           IF ART-VAL-MAX(IDX) NOT = 0
              IF FAL-DONNEE(IDX) > ART-VAL-MAX(IDX) 
                 MOVE ART-VAL-MAX(IDX) TO FAL-DONNEE(IDX)
              END-IF
           END-IF.
           IF FAL-DONNEE(IDX) < ART-VAL-MIN(IDX) 
              MOVE ART-VAL-MIN(IDX) TO FAL-DONNEE(IDX)
           END-IF.
           IF ART-FORMULE(IDX) NOT = 0
              MOVE IDX TO LNK-NUM
              CALL "4-PRIX" USING LINK-V ART-RECORD FAL-RECORD
              PERFORM RECHARGE VARYING IDX FROM 1 BY 1 UNTIL IDX > 6
              MOVE LNK-NUM TO IDX
           END-IF.
            
           


       AFFICHAGE-ECRAN.
           MOVE 256 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           IF MODIFICATION = 1
              PERFORM CALCUL
           END-IF.
           CLOSE FAL.
           CANCEL "6-FAL".
           CANCEL "4-PROCFA".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
             IF DECISION = 99 
                DISPLAY FAL-USER LINE 24 POSITION 5
                DISPLAY FAL-ST-JOUR  LINE 24 POSITION 15
                DISPLAY FAL-ST-MOIS  LINE 24 POSITION 18
                DISPLAY FAL-ST-ANNEE LINE 24 POSITION 21
                DISPLAY FAL-ST-HEURE LINE 24 POSITION 30
                DISPLAY FAL-ST-MIN   LINE 24 POSITION 33
                GO AVANT-DEC
             END-IF.
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".


       FACTURE-RECENTE.
           START FACTURE KEY < FAC-NUMBER INVALID INITIALIZE FAC-RECORD
                NOT INVALID 
                   READ FACTURE PREVIOUS NO LOCK AT END 
               INITIALIZE FAC-RECORD
           END-READ.
           PERFORM CLIENT.

       FACTURE-SUIVANTE.
           START FACTURE KEY > FAC-NUMBER INVALID INITIALIZE FAC-RECORD
                NOT INVALID 
                   READ FACTURE NEXT NO LOCK AT END 
               INITIALIZE FAC-RECORD
           END-READ.
           PERFORM CLIENT.

       CLIENT.
           MOVE FAC-CLIENT TO PC-NUMBER.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY.
           PERFORM DIS-HE-01 THRU DIS-HE-02.


       TVA.
           IF TVA-TAUX(IDX) > 0
           AND FAC-BTVA(IDX) > 0
              COMPUTE FAC-TVA(IDX) = FAC-BTVA(IDX) * TVA-TAUX(IDX)
              / 100 + ,005 
              ADD FAC-TVA(IDX) TO FAC-TOTAL-TVA
           END-IF.

       NEXT-NUMBER.
           INITIALIZE FAC-RECORD.
           MOVE 999999 TO FAC-NUMBER.
           START FACTURE KEY < FAC-NUMBER INVALID 
               MOVE 0 TO FAC-NUMBER
               NOT INVALID 
           READ FACTURE PREVIOUS AT END 
               MOVE 0 TO FAC-NUMBER
           END-READ.
           ADD 1 TO FAC-NUMBER.
           INITIALIZE FAC-REC-DET.


       GET-FAL.
           INITIALIZE FAL-RECORD.
           MOVE FAC-NUMBER TO FAL-NUMBER.
           START FAL KEY > FAL-KEY-3 INVALID GO GET-FAL-END.
       GET-FAL-1.
           READ FAL NEXT NO LOCK AT END
              INITIALIZE FAL-RECORD
              GO GET-FAL-END
           END-READ.
           IF FAC-NUMBER NOT = FAL-NUMBER
              INITIALIZE FAL-RECORD
              GO GET-FAL-END
           END-IF.
           IF FAL-CODE = ART-NUMBER 
              GO GET-FAL-END
           END-IF.
           GO GET-FAL-1.
       GET-FAL-END.
           EXIT.
