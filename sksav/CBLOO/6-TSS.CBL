      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-TSS RECHERCHE TAUX COTISATIONS SECURITE SOCIALE �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-TSS.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "TAUXSS.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "TAUXSS.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  ACTION   PIC 9 VALUE 0.
       01  REGIME-NUMBER PIC 999 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "TAUXSS.LNK".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TAUXSS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-TSS.
       
           IF NOT-OPEN = 0
              OPEN I-O TAUXSS
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO SS-RECORD.
           MOVE SS-REGIME TO REGIME-NUMBER.
           IF SS-ANNEE = 0
              MOVE LNK-ANNEE TO SS-ANNEE
              MOVE LNK-MOIS  TO SS-MOIS
           END-IF.
           START TAUXSS KEY <= SS-KEY INVALID CONTINUE
              NOT INVALID 
              READ TAUXSS PREVIOUS NO LOCK
              AT END INITIALIZE SS-RECORD END-READ.
           IF SS-REGIME NOT = REGIME-NUMBER 
              INITIALIZE SS-RECORD.
           IF SS-ABAT(1) < 0
              INITIALIZE SS-ABATTEMENTS.
           MOVE SS-RECORD TO LINK-RECORD.
           
           EXIT PROGRAM.


      
