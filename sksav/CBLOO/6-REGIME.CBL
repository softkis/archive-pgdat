      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-REGIME MODULE GENERAL LECTURE REGIME      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-REGIME.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "REGIME.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "REGIME.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".


      *  ENREGISTREMENT FICHIER REGIME
       01  LINK-RECORD.
           02 LINK-KEY.
              03 LINK-NUMBER   PIC 99.

           02 LINK-REC-DET.
              03 LINK-NOM      PIC X(50).
              03 FILLER        PIC X(100).
                                
       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON REGIME.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-REGIME.
       
           EVALUATE EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN  9 MOVE 66 TO EXC-KEY
           END-EVALUATE.

           IF NOT-OPEN = 0
              OPEN INPUT REGIME
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO REGIME-RECORD.
           MOVE 0 TO LNK-VAL.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ REGIME PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ REGIME NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ REGIME NO LOCK INVALID INITIALIZE 
                REGIME-REC-DET END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE REGIME-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START REGIME KEY < REGIME-KEY INVALID GO EXIT-1.
       START-2.
           START REGIME KEY > REGIME-KEY INVALID GO EXIT-1.


               