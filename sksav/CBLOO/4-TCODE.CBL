      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-TCODE CODE DATE + HEURE                   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-TCODE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
       01  INTER-BIN             PIC 9(4) USAGE IS BINARY VALUE 9.
       01  INTER-MED REDEFINES INTER-BIN.
           02  INTER-MED1        PIC X.
           02  TT                PIC X.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".


       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-PROGRAMME-4-TCODE.

           INITIALIZE LNK-TEXT.
           MOVE 1 TO IDX.
           CALL "0-TODAY" USING TODAY.

           COMPUTE INTER-BIN = 32 + TODAY-JOUR.  
           PERFORM STR.
           COMPUTE INTER-BIN = 32 + TODAY-MOIS.  
           PERFORM STR.
           COMPUTE INTER-BIN = 64 + TODAY-HEURE. 
           PERFORM STR.
           COMPUTE INTER-BIN = 64 + TODAY-MIN.   
           PERFORM STR.
           COMPUTE INTER-BIN = 56 + TD-ANNEE.
           PERFORM STR.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

       STR.
           STRING TT DELIMITED BY SIZE INTO LNK-TEXT WITH POINTER IDX.

