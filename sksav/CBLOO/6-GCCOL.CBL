      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-GCCOL RECHERCHE CONTRAT COLLECTIF RECENT  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-GCCOL.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "CCOL.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CCOL.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  NOT-OPEN     PIC 9 VALUE 0.
       01  NOT-FOUND    PIC 9.
       01  ACTION       PIC X.

       01  START-DATE   PIC 9(6) VALUE 0.
       01  START-DATE-R REDEFINES START-DATE.
           02 START-ANNEE PIC 9999.
           02 START-MOIS  PIC 99.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "CCOL.LNK".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CCOL .
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-GCCOL.
       
           IF NOT-OPEN = 0
              OPEN INPUT CCOL
              MOVE 1 TO NOT-OPEN.

           INITIALIZE CCOL-RECORD START-DATE-R.
           MOVE LNK-NUM TO START-DATE.
           MOVE 0 TO LNK-NUM.
           IF START-ANNEE = 0
              MOVE LNK-ANNEE TO START-ANNEE
              MOVE LNK-MOIS  TO START-MOIS
           END-IF.
           PERFORM CCOL-RECENT THRU CCOL-END.
           MOVE CCOL-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       CCOL-RECENT.
           PERFORM PREV.
           IF NOT-FOUND = 0 
              GO CCOL-END
           END-IF.
           MOVE 0 TO LNK-VAL.
           PERFORM PREV.
           IF NOT-FOUND = 0 
              GO CCOL-END
           END-IF.
           PERFORM NEX.
           IF NOT-FOUND = 0 
              GO CCOL-END
           END-IF.
           MOVE 1 TO LNK-STATUS LNK-VAL.
           CALL "4-MCCOL" USING LINK-V.
           CANCEL "4-MCCOL".
           PERFORM PREV.
       CCOL-END.
           EXIT.

       PREV.
           PERFORM CC-KEY.
           START CCOL KEY <= CCOL-KEY INVALID
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ CCOL PREVIOUS NO LOCK AT END 
                   INITIALIZE CCOL-RECORD
           END-READ.
           IF  FR-KEY  = CCOL-FIRME 
           AND LNK-VAL = CCOL-TYPE
              MOVE CCOL-RECORD TO LINK-RECORD
              EXIT PROGRAM
           ELSE
              MOVE 1 TO NOT-FOUND.

       NEX.
           PERFORM CC-KEY.
           START CCOL KEY >= CCOL-KEY INVALID KEY
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ CCOL NEXT NO LOCK AT END 
                   INITIALIZE CCOL-RECORD
           END-READ.
           IF FR-KEY NOT = CCOL-FIRME 
           AND LNK-VAL  NOT = CCOL-TYPE
              MOVE 1 TO NOT-FOUND.

       CC-KEY.
           INITIALIZE CCOL-RECORD NOT-FOUND.
           MOVE LNK-VAL TO CCOL-TYPE.
           MOVE START-ANNEE TO CCOL-ANNEE.
           MOVE START-MOIS  TO CCOL-MOIS.
           MOVE FR-KEY   TO CCOL-FIRME.
