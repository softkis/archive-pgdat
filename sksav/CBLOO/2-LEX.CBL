      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-LEX RECHERCHE DESCRIPTIFS LIVRE DE PAIE   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-LEX.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "LIVREX.REC".

       01  CHOIX-MAX      PIC 99 VALUE 1.
       01  CHOIX          PIC 9(4).
       01  COMPTEUR       PIC 99.

       01 HE-LEX.
          02 H-R PIC 9(4) OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-TEMP.
              03 HE-JJ    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-MM    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-AA    PIC ZZZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "LIVREX.LNK".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       START-DISPLAY SECTION.
             
       START-2-LEX.

           PERFORM AFFICHAGE-ECRAN .
           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE LEX-RECORD IDX-1 EXC-KEY COMPTEUR CHOIX HE-LEX.
           MOVE  4 TO LIN-IDX.
           MOVE 13 TO EXC-KEY.
           PERFORM READ-LEX THRU READ-LEX-END.
           IF EXC-KEY = 66 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-LEX.
           CALL "6-LEX" USING LINK-V LEX-RECORD EXC-KEY.
           IF LEX-DESCRIPTION = SPACES
           AND EXC-KEY = 13
              MOVE 66 TO EXC-KEY
              GO READ-LEX
           END-IF.
           MOVE 66 TO EXC-KEY.
           IF LEX-DESCRIPTION = SPACES
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 23
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 INITIALIZE HE-LEX IDX-1
                 GO READ-LEX
              END-IF
              GO READ-LEX-END.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 23
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
                 IF CHOIX > 0
                    MOVE CHOIX TO LEX-NUMBER
                 END-IF
                 INITIALIZE HE-LEX IDX-1
                 GO READ-LEX
              END-IF
              IF CHOIX NOT = 0
                 GO READ-LEX-END
              END-IF
              INITIALIZE HE-LEX IDX-1
           END-IF.
           MOVE 66 TO EXC-KEY.
           GO READ-LEX.
       READ-LEX-END.
           IF CHOIX = 0
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82 AND NOT = 68
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 23
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX COMPTEUR IDX-1.
           MOVE LEX-NUMBER TO HE-Z4 H-R(IDX-1).
           DISPLAY HE-Z4  LINE LIN-IDX POSITION 1.
           DISPLAY LEX-DESCRIPTION LINE LIN-IDX POSITION 6.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0018000000 TO EXC-KFR(1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0000530000 TO EXC-KFR (11).
           MOVE 0000000065 TO EXC-KFR (13).
           MOVE 6667000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE CHOIX.
           ACCEPT CHOIX 
           LINE 3 POSITION 30 SIZE 4
           TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 2 
              MOVE CHOIX TO LEX-NUMBER
              MOVE 0 TO CHOIX
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF CHOIX NOT = 0
           AND EXC-KEY = 13
              PERFORM END-PROGRAM
           END-IF.
      *    INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IDX-1.

       AFFICHAGE-ECRAN.
           MOVE 2012 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       END-PROGRAM.
           MOVE CHOIX TO LEX-NUMBER.
           CALL "6-LEX" USING LINK-V LEX-RECORD FAKE-KEY.
           IF LEX-DESCRIPTION NOT = SPACES
              MOVE LEX-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IDX-1 + 4.
           MOVE H-R(IDX-1) TO HE-Z4.
           ACCEPT HE-Z4
             LINE  LIN-IDX POSITION 1 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 82 
              MOVE H-R(IDX-1) TO CHOIX
              PERFORM END-PROGRAM
           END-IF.
           MOVE H-R(IDX-1) TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 1.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN OTHER
                     MOVE H-R(IDX-1) TO CHOIX
                     PERFORM END-PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-R(IDX-1) = 0
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.
                        