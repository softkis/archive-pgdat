      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-DOCFAC COMPLETER DOCUMENTS FACTURE        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-DOCFAC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

       01  IDX-1                 PIC 9(5).
       01  IDX-2                 PIC 9(5).
       01  IDX-3                 PIC 9(5).
       01  IDX-4                 PIC 9(5).
       01  TEST-TEXT             PIC X(6).
       01  TEST-TEXT-R REDEFINES TEST-TEXT.
           02 TEST-TEXT-1       PIC X.
           02 TEST-TEXT-2       PIC X.
           02 TEST-TEXT-3       PIC X(4).
       01  LINE-NUMBER           PIC 999.
       01  LIN-NUM               PIC 999.
       01  DEC-NUM               PIC 999.
       01  EFFACER               PIC X(6) VALUE SPACES.
       01  action pic x.
       01  ALPHA-TEXTE PIC X(60).
       01  SH-00                PIC 9(8)V9(4).


       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ  BLANK WHEN ZERO.
           02 HE-Z2Z2 PIC ZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z3Z2 PIC ZZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).
           02 HE-Z6Z2 PIC ZZ.ZZZ,ZZ.
           02 HE-Z8 PIC Z.ZZZ.ZZZ.
           02 HE-DATE.
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HE-MATR.
              03 HM-AA PIC 9999.
              03 FILLER PIC X VALUE " ".
              03 HM-MM PIC 99.
              03 FILLER PIC X VALUE " ".
              03 HM-JJ PIC 99.
              03 FILLER PIC X VALUE " ".
              03 HM-SN PIC 999.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "FACTINT.REC".

       01  TEXTES.
           02 FORM-LINE PIC X(19200).

       PROCEDURE DIVISION USING LINK-V FI-RECORD TEXTES.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-DOCFAC .


           INSPECT TEXTES REPLACING ALL ">DEB" BY "    ".
           INSPECT TEXTES REPLACING ALL ">FIN" BY "    ".
           MOVE 0 TO IDX-1.
           PERFORM TEST-FORM THRU TEST-FORM-END.
           EXIT PROGRAM.

       TEST-FORM.
           INSPECT TEXTES TALLYING IDX-1 FOR CHARACTERS BEFORE ">".
           IF  IDX-1 NOT = 0 
           AND IDX-1 < 19200
              PERFORM FILL-IN
           ELSE
              GO TEST-FORM-END
           END-IF.
           MOVE 0 TO IDX-1.
           GO TEST-FORM.
       TEST-FORM-END.
           EXIT.

       FILL-IN.
           ADD 1 TO IDX-1.
           UNSTRING TEXTES INTO TEST-TEXT WITH POINTER IDX-1.
           SUBTRACT 6 FROM IDX-1.
           MOVE IDX-1 TO IDX-4.
           EVALUATE TEST-TEXT
                WHEN ">NO   " PERFORM FACT-NR 
                WHEN ">DA   " PERFORM FACT-DATE
                WHEN ">REF  " PERFORM FACT-REF
                WHEN ">PAGE " PERFORM FACT-PAGE
                WHEN OTHER    PERFORM ERASE-IT
           END-EVALUATE.

       FACT-NR.
           MOVE FI-NUMBER TO HE-Z8.
           STRING HE-Z8 DELIMITED BY SIZE INTO TEXTES POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.

       FACT-PAGE.
           MOVE LNK-VAL TO HE-Z5.
           STRING HE-Z5 DELIMITED BY SIZE INTO TEXTES POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.

       FACT-REF.
           IF FI-REFERENCE > SPACES
              STRING FI-REFERENCE DELIMITED BY SIZE INTO TEXTES
              POINTER IDX-1 ON OVERFLOW CONTINUE END-STRING
           ELSE
              PERFORM ERASE-IT
           END-IF.

       FACT-DATE.
           MOVE FI-EDIT-A TO HE-AA.
           MOVE FI-EDIT-M TO HE-MM.
           MOVE FI-EDIT-J TO HE-JJ.
           PERFORM STRING-DATE.

       STRING-DATE.
           STRING HE-DATE DELIMITED BY SIZE INTO TEXTES POINTER IDX-1.

       STRING-ALPHA.
           STRING ALPHA-TEXTE DELIMITED BY "  " INTO TEXTES
           POINTER IDX-1 ON OVERFLOW CONTINUE END-STRING.

       ERASE-IT.
           STRING EFFACER DELIMITED BY SIZE INTO TEXTES POINTER IDX-4 
           ON OVERFLOW CONTINUE END-STRING.


