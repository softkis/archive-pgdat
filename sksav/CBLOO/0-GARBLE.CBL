      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-GARBLE.CBL EN-DECRYPTAGE DU MOT DE PASSE  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.             0-GARBLE.

       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.

       01  METHODE      PIC 9(4).
       01  METHODE-R REDEFINES METHODE.
           02 MET1      PIC 99.
           02 MET2      PIC 99.

       01  CATCH-TIME.
           02 CT-HEURE           PIC 99.
           02 CT-MIN             PIC 99.
           02 CT-SECS            PIC 9999.

       01  BC.
           02 B01 PIC 9999 COMP-6 VALUE 1021.
           02 B02 PIC 9999 COMP-6 VALUE  913.
           02 B03 PIC 9999 COMP-6 VALUE 2227.
           02 B04 PIC 9999 COMP-6 VALUE 7130.
           02 B05 PIC 9999 COMP-6 VALUE 1661.
           02 B06 PIC 9999 COMP-6 VALUE 8945.
           02 B07 PIC 9999 COMP-6 VALUE 4567.
           02 B08 PIC 9999 COMP-6 VALUE 3355.
           02 B09 PIC 9999 COMP-6 VALUE 3782.
           02 B10 PIC 9999 COMP-6 VALUE 6351.
           02 B11 PIC 9999 COMP-6 VALUE 3400.
           02 B12 PIC 9999 COMP-6 VALUE 1144.
           02 B13 PIC 9999 COMP-6 VALUE 7549.
           02 B14 PIC 9999 COMP-6 VALUE 8199.
           02 B15 PIC 9999 COMP-6 VALUE 1713.
           02 B16 PIC 9999 COMP-6 VALUE 2780.
           02 B17 PIC 9999 COMP-6 VALUE 5366.
           02 B18 PIC 9999 COMP-6 VALUE 0711.
           02 B19 PIC 9999 COMP-6 VALUE 2001.
           02 B20 PIC 9999 COMP-6 VALUE 7324.
           02 B21 PIC 9999 COMP-6 VALUE 9013.
           02 B22 PIC 9999 COMP-6 VALUE 4823.
           02 B23 PIC 9999 COMP-6 VALUE 6585.
           02 B24 PIC 9999 COMP-6 VALUE 1942.
           02 B25 PIC 9999 COMP-6 VALUE 5804.
           02 B26 PIC 9999 COMP-6 VALUE 1951.
           02 B27 PIC 9999 COMP-6 VALUE 1711.
           02 B28 PIC 9999 COMP-6 VALUE 8520.
           02 B29 PIC 9999 COMP-6 VALUE 9630.
           02 B30 PIC 9999 COMP-6 VALUE 7411.
           02 B31 PIC 9999 COMP-6 VALUE 9513.
           02 B32 PIC 9999 COMP-6 VALUE 1674.
           02 B33 PIC 9999 COMP-6 VALUE 1021.
           02 B34 PIC 9999 COMP-6 VALUE 5574.
           02 B35 PIC 9999 COMP-6 VALUE 6842.
           02 B36 PIC 9999 COMP-6 VALUE 1245.
           02 B37 PIC 9999 COMP-6 VALUE 6259.
           02 B38 PIC 9999 COMP-6 VALUE 3589.
           02 B39 PIC 9999 COMP-6 VALUE 3420.
           02 B40 PIC 9999 COMP-6 VALUE 2106.
           02 B41 PIC 9999 COMP-6 VALUE 4677.
           02 B42 PIC 9999 COMP-6 VALUE 4711.
           02 B43 PIC 9999 COMP-6 VALUE 8024.
           02 B44 PIC 9999 COMP-6 VALUE 7366.
           02 B45 PIC 9999 COMP-6 VALUE 5777.
           02 B46 PIC 9999 COMP-6 VALUE 4444.
           02 B47 PIC 9999 COMP-6 VALUE 9764.
           02 B48 PIC 9999 COMP-6 VALUE 3366.
           02 B49 PIC 9999 COMP-6 VALUE 6912.
           02 B50 PIC 9999 COMP-6 VALUE 5021.
           02 B51 PIC 9999 COMP-6 VALUE 1122.
           02 B52 PIC 9999 COMP-6 VALUE 8462.
           02 B53 PIC 9999 COMP-6 VALUE 9713.
           02 B54 PIC 9999 COMP-6 VALUE 3719.
           02 B55 PIC 9999 COMP-6 VALUE 2684.
           02 B56 PIC 9999 COMP-6 VALUE 6842.
           02 B57 PIC 9999 COMP-6 VALUE 8426.
           02 B58 PIC 9999 COMP-6 VALUE 5150.
           02 B59 PIC 9999 COMP-6 VALUE 1925.
           02 B60 PIC 9999 COMP-6 VALUE 2954.
           02 B61 PIC 9999 COMP-6 VALUE 4711.
           02 B62 PIC 9999 COMP-6 VALUE 1221.
           02 B63 PIC 9999 COMP-6 VALUE 5289.
           02 B64 PIC 9999 COMP-6 VALUE 6203.
           02 B65 PIC 9999 COMP-6 VALUE 4235.
           02 B66 PIC 9999 COMP-6 VALUE 1793.
           02 B67 PIC 9999 COMP-6 VALUE 7562.
           02 B68 PIC 9999 COMP-6 VALUE 7800.
           02 B69 PIC 9999 COMP-6 VALUE 3654.
           02 B70 PIC 9999 COMP-6 VALUE 2713.
       01  BB REDEFINES BC.
           02  B-C PIC 9999 COMP-6 OCCURS 70.


       01  IDX                 PIC 9(4).
       01  IDX-1               PIC 9(4).
       01  IDX-2               PIC 9(4).
       01  IDX-3               PIC 9(4).

       01  INTER-BIN           PIC 9(4) USAGE IS BINARY VALUE 0.
       01  INTER-MED REDEFINES INTER-BIN.
           02  INTER-MED1      PIC X.
           02  INTER-MED2      PIC X.

       01  PASS-CLEAR.
           02 P-CHAR           PIC X OCCURS 10.
           
       01  PASS-GARBLED.
           02 P-COMP           PIC 9999 COMP-6 OCCURS 11.
           
       01  PASS-GARB-RED REDEFINES PASS-GARBLED PIC X(22).
                       
       LINKAGE SECTION. 

           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
      
       PASSWORD-CONVERT SECTION.
             
       START-PASSWORD-CONVERT.
                     
           IF LNK-STATUS = 0 
              MOVE LNK-TEXT TO PASS-GARB-RED
              MOVE P-COMP(11) TO METHODE
              PERFORM CLEAR-PASS 
              MOVE PASS-CLEAR TO LNK-TEXT 
           ELSE
              MOVE LNK-TEXT TO PASS-CLEAR
              ACCEPT CATCH-TIME FROM TIME
              DIVIDE CT-SECS BY 21 GIVING IDX-2 REMAINDER MET1
              MOVE CT-MIN TO MET2
              IF MET1 = 0 MOVE 1 TO MET1 END-IF
              IF MET2 = 0 MOVE 1 TO MET2 END-IF
              PERFORM GARBLE-PASS
              MOVE METHODE TO P-COMP(11) 
              MOVE PASS-GARB-RED TO LNK-TEXT
           END-IF.

           EXIT PROGRAM.
           
       CLEAR-PASS.
           EVALUATE MET1
            WHEN  1 PERFORM U-01 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  2 PERFORM U-02 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  3 PERFORM U-03 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  4 PERFORM U-04 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  5 PERFORM U-05 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  6 PERFORM U-06 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  7 PERFORM U-07 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  8 PERFORM U-08 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  9 PERFORM U-09 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 10 PERFORM U-10 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 11 PERFORM U-11 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 12 PERFORM U-12 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 13 PERFORM U-13 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 14 PERFORM U-14 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 15 PERFORM U-15 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 16 PERFORM U-16 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 17 PERFORM U-17 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 18 PERFORM U-18 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 19 PERFORM U-19 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 20 PERFORM U-20 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
           END-EVALUATE.
    
       GARBLE-PASS.
           EVALUATE MET1
            WHEN  1 PERFORM G-01 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  2 PERFORM G-02 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  3 PERFORM G-03 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  4 PERFORM G-04 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  5 PERFORM G-05 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  6 PERFORM G-06 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  7 PERFORM G-07 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  8 PERFORM G-08 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN  9 PERFORM G-09 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 10 PERFORM G-10 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 11 PERFORM G-11 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 12 PERFORM G-12 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 13 PERFORM G-13 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 14 PERFORM G-14 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 15 PERFORM G-15 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 16 PERFORM G-16 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 17 PERFORM G-17 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 18 PERFORM G-18 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 19 PERFORM G-19 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
            WHEN 20 PERFORM G-20 VARYING IDX FROM 1 BY 1 UNTIL IDX > 10
           END-EVALUATE.

       IX.
           COMPUTE IDX-1 = MET2 + IDX - 1.
           DIVIDE IDX-1 BY 2 GIVING IDX-2 REMAINDER IDX-3.
           IF LNK-STATUS = 1
              PERFORM G
           END-IF.

       G.
           MOVE 0 TO INTER-BIN.
           MOVE P-CHAR(IDX) TO INTER-MED2.
           MOVE INTER-BIN TO P-COMP(IDX).

       U.
           MOVE P-COMP(IDX) TO INTER-BIN.
           MOVE INTER-MED2 TO P-CHAR(IDX).

       G-01.
           PERFORM IX.
           ADD B-C(IDX-1) TO P-COMP(IDX).

       U-01.
           PERFORM IX.
           SUBTRACT B-C(IDX-1) FROM P-COMP(IDX).
           PERFORM U.

       G-02.
           PERFORM IX.
           COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX).

       U-02.
           PERFORM G-02.
           PERFORM U.

       G-03.
           PERFORM IX.
           IF IDX-3 = 0
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              ADD B-C(IDX-1) TO P-COMP(IDX).

       U-03.
           PERFORM IX.
           IF IDX-3 = 0
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              SUBTRACT B-C(IDX-1) FROM P-COMP(IDX).
           PERFORM U.

       G-04.
           PERFORM IX.
           IF IDX-3 = 1
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              ADD B-C(IDX-1) TO P-COMP(IDX).
           PERFORM U.

       U-04.
           PERFORM IX.
           IF IDX-3 = 1
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              SUBTRACT B-C(IDX-1) FROM P-COMP(IDX).
           PERFORM U.

       G-05.
           PERFORM IX.
           IF IDX > 5
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              ADD B-C(IDX-1) TO P-COMP(IDX).

       U-05.
           PERFORM IX.
           IF IDX > 5
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              SUBTRACT B-C(IDX-1) FROM P-COMP(IDX).
           PERFORM U.

       G-06.
           PERFORM IX.
           IF IDX < 6
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              ADD B-C(IDX-1) TO P-COMP(IDX).
           PERFORM U.

       U-06.
           PERFORM IX.
           IF IDX < 6
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              SUBTRACT B-C(IDX-1) FROM P-COMP(IDX).
           PERFORM U.

       G-07.
           PERFORM IX.
           IF IDX < 6
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              ADD B-C(IDX-1) TO P-COMP(IDX)
              ADD MET2 TO P-COMP(IDX).

       U-07.
           PERFORM IX.
           IF IDX < 6
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              SUBTRACT MET2 FROM P-COMP(IDX)
              SUBTRACT B-C(IDX-1) FROM P-COMP(IDX).
           PERFORM U.


       G-08.
           PERFORM IX.
           IF IDX-3 = 0
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              ADD B-C(IDX-1) TO P-COMP(IDX).
           ADD MET1 TO P-COMP(IDX).

       U-08.
           PERFORM IX.
           SUBTRACT MET1 FROM P-COMP(IDX).
           IF IDX-3 = 0
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              SUBTRACT B-C(IDX-1) FROM P-COMP(IDX).
           PERFORM U.

       G-09.
           PERFORM IX.
           IF IDX-3 = 1
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              ADD B-C(IDX-1) TO P-COMP(IDX).
           ADD MET1 TO P-COMP(IDX).

       U-09.
           PERFORM IX.
           SUBTRACT MET1 FROM P-COMP(IDX).
           IF IDX-3 = 1
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              SUBTRACT B-C(IDX-1) FROM P-COMP(IDX).
           PERFORM U.

       G-10.
           PERFORM IX.
           IF IDX-3 = 1
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
              ADD MET2 TO P-COMP(IDX)
           ELSE
              ADD B-C(IDX-1) TO P-COMP(IDX)
              SUBTRACT MET2 FROM P-COMP(IDX).

       U-10.
           PERFORM IX.
           IF IDX-3 = 1
              SUBTRACT MET2 FROM P-COMP(IDX)
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              ADD MET2 TO P-COMP(IDX)
              SUBTRACT B-C(IDX-1) FROM P-COMP(IDX).
           PERFORM U.

       G-11.
           PERFORM IX.
           IF IDX-3 = 1
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
              ADD MET1 TO P-COMP(IDX)
           ELSE
              ADD B-C(IDX-1) TO P-COMP(IDX)
              SUBTRACT MET2 FROM P-COMP(IDX).

       U-11.
           PERFORM IX.
           IF IDX-3 = 1
              SUBTRACT MET1 FROM P-COMP(IDX)
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              ADD MET2 TO P-COMP(IDX)
              SUBTRACT B-C(IDX-1) FROM P-COMP(IDX).
           PERFORM U.


       G-12.
           PERFORM IX.
           IF IDX > 5
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
              ADD MET2 TO P-COMP(IDX)
           ELSE
              ADD B-C(IDX-1) TO P-COMP(IDX)
              SUBTRACT MET2 FROM P-COMP(IDX).

       U-12.
           PERFORM IX.
           IF IDX > 5
              SUBTRACT MET2 FROM P-COMP(IDX)
              COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX)
           ELSE
              ADD MET2 TO P-COMP(IDX)
              SUBTRACT B-C(IDX-1) FROM P-COMP(IDX).
           PERFORM U.

       G-13.
           PERFORM IX.
           COMPUTE P-COMP(IDX) = B-C(IDX-1) + P-COMP(IDX) + IDX.

       U-13.
           PERFORM IX.
           COMPUTE P-COMP(IDX) = P-COMP(IDX) - IDX - B-C(IDX-1).
           PERFORM U.

       G-14.
           PERFORM IX.
           COMPUTE P-COMP(IDX) = B-C(IDX-1) + P-COMP(IDX) + MET1 - MET2.

       U-14.
           PERFORM IX.
           COMPUTE P-COMP(IDX) = P-COMP(IDX) - B-C(IDX-1) - MET1 + MET2.
           PERFORM U.

       G-15.
           PERFORM IX.
           COMPUTE P-COMP(IDX) = B-C(IDX-1) - P-COMP(IDX) + MET1 - MET2.

       U-15.
           PERFORM G-15.
           PERFORM U.

       G-16.
           EVALUATE IDX 
            WHEN  1 PERFORM G-01
            WHEN  2 PERFORM G-02
            WHEN  3 PERFORM G-03
            WHEN  4 PERFORM G-04
            WHEN  5 PERFORM G-05
            WHEN  6 PERFORM G-06
            WHEN  7 PERFORM G-07
            WHEN  8 PERFORM G-08
            WHEN  9 PERFORM G-09
            WHEN 10 PERFORM G-10
           END-EVALUATE.

       U-16.
           EVALUATE IDX 
            WHEN  1 PERFORM U-01
            WHEN  2 PERFORM U-02
            WHEN  3 PERFORM U-03
            WHEN  4 PERFORM U-04
            WHEN  5 PERFORM U-05
            WHEN  6 PERFORM U-06
            WHEN  7 PERFORM U-07
            WHEN  8 PERFORM U-08
            WHEN  9 PERFORM U-09
            WHEN 10 PERFORM U-10
           END-EVALUATE.
           PERFORM U.

       G-17.
           EVALUATE IDX 
            WHEN  1 PERFORM G-06
            WHEN  2 PERFORM G-07
            WHEN  3 PERFORM G-08
            WHEN  4 PERFORM G-09
            WHEN  5 PERFORM G-10
            WHEN  6 PERFORM G-01
            WHEN  7 PERFORM G-02
            WHEN  8 PERFORM G-03
            WHEN  9 PERFORM G-04
            WHEN 10 PERFORM G-05
           END-EVALUATE.

       U-17.
           EVALUATE IDX 
            WHEN  1 PERFORM U-06
            WHEN  2 PERFORM U-07
            WHEN  3 PERFORM U-08
            WHEN  4 PERFORM U-09
            WHEN  5 PERFORM U-10
            WHEN  6 PERFORM U-01
            WHEN  7 PERFORM U-02
            WHEN  8 PERFORM U-03
            WHEN  9 PERFORM U-04
            WHEN 10 PERFORM U-05
           END-EVALUATE.
           PERFORM U.

       G-18.
           EVALUATE IDX 
            WHEN  1 PERFORM G-06
            WHEN  2 PERFORM G-07
            WHEN  3 PERFORM G-08
            WHEN  4 PERFORM G-09
            WHEN  5 PERFORM G-10
            WHEN  6 PERFORM G-11
            WHEN  7 PERFORM G-12
            WHEN  8 PERFORM G-13
            WHEN  9 PERFORM G-14
            WHEN 10 PERFORM G-15
           END-EVALUATE.

       U-18.
           EVALUATE IDX 
            WHEN  1 PERFORM U-06
            WHEN  2 PERFORM U-07
            WHEN  3 PERFORM U-08
            WHEN  4 PERFORM U-09
            WHEN  5 PERFORM U-10
            WHEN  6 PERFORM U-11
            WHEN  7 PERFORM U-12
            WHEN  8 PERFORM U-13
            WHEN  9 PERFORM U-14
            WHEN 10 PERFORM U-15
           END-EVALUATE.
           PERFORM U.

       G-19.
           EVALUATE IDX 
            WHEN  1 PERFORM G-11
            WHEN  2 PERFORM G-12
            WHEN  3 PERFORM G-13
            WHEN  4 PERFORM G-14
            WHEN  5 PERFORM G-15
            WHEN  6 PERFORM G-01
            WHEN  7 PERFORM G-02
            WHEN  8 PERFORM G-03
            WHEN  9 PERFORM G-04
            WHEN 10 PERFORM G-05
           END-EVALUATE.

       U-19.
           EVALUATE IDX 
            WHEN  1 PERFORM U-11
            WHEN  2 PERFORM U-12
            WHEN  3 PERFORM U-13
            WHEN  4 PERFORM U-14
            WHEN  5 PERFORM U-15
            WHEN  6 PERFORM U-01
            WHEN  7 PERFORM U-02
            WHEN  8 PERFORM U-03
            WHEN  9 PERFORM U-04
            WHEN 10 PERFORM U-05
           END-EVALUATE.
           PERFORM U.

       G-20.
           EVALUATE IDX 
            WHEN  1 PERFORM G-10
            WHEN  2 PERFORM G-09
            WHEN  3 PERFORM G-08
            WHEN  4 PERFORM G-07
            WHEN  5 PERFORM G-06
            WHEN  6 PERFORM G-05
            WHEN  7 PERFORM G-04
            WHEN  8 PERFORM G-03
            WHEN  9 PERFORM G-02
            WHEN 10 PERFORM G-01
           END-EVALUATE.

       U-20.
           EVALUATE IDX 
            WHEN  1 PERFORM U-10
            WHEN  2 PERFORM U-09
            WHEN  3 PERFORM U-08
            WHEN  4 PERFORM U-07
            WHEN  5 PERFORM U-06
            WHEN  6 PERFORM U-05
            WHEN  7 PERFORM U-04
            WHEN  8 PERFORM U-03
            WHEN  9 PERFORM U-02
            WHEN 10 PERFORM U-01
           END-EVALUATE.
           PERFORM U.
