      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-HSSEM REPORT HRS SUPP MOIS PRECEDENT      �
      *  � POUR DECOMPTE PAR SEMAINE                             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-HSSEM.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "OCCUP.REC".
           COPY "CARRIERE.REC".
           COPY "CCOL.REC".
           COPY "PRESENCE.REC".
           COPY "HORSEM.REC".
           COPY "POSE.REC".
           COPY "CALEN.REC".
             
           COPY "V-VAR.CPY".
        
       01  JOUR                  PIC 99.
       01  LAST-DAY              PIC 99.
       01  FIRST-DAY             PIC 9.


       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.

       01  HELP-1                PIC 9999.
       01  HELP-2                PIC 9999.
       01  HELP-3                PIC S9(6)V99 COMP-3.
       01  HELP-4                PIC S9(4)V99 VALUE 0.
       01  HRS-THEORIQUES        PIC 99V99 VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z3Z2 PIC Z(3),ZZ BLANK WHEN ZERO.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".


       PROCEDURE DIVISION USING LINK-V REG-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-HSSEM.
       
           MOVE LNK-MOIS  TO SAVE-MOIS.
           MOVE LNK-ANNEE TO SAVE-ANNEE.
           COMPUTE LAST-DAY = SEM-IDX(LNK-MOIS, 1) - 2.


           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0
              SUBTRACT 1 FROM LNK-ANNEE
              MOVE 12 TO LNK-MOIS
           END-IF.
           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN INPUT JOURS.
           PERFORM RECENT.

           INITIALIZE LNK-VAL IDX. 
           COMPUTE LAST-DAY = MOIS-JRS(LNK-MOIS) - LAST-DAY.
           PERFORM HEURES-THEORIQUES VARYING
           JOUR FROM LAST-DAY BY 1 UNTIL JOUR > MOIS-JRS(LNK-MOIS).

           PERFORM TOTAL-HEURES.
           MOVE SAVE-MOIS  TO LNK-MOIS.
           MOVE SAVE-ANNEE TO LNK-ANNEE.
           COMPUTE LNK-VAL   = HELP-4.
           COMPUTE LNK-VAL-2 = HRS-THEORIQUES.
           EXIT PROGRAM.
           
       TOTAL-HEURES.
           INITIALIZE JRS-RECORD.
           MOVE FR-KEY     TO JRS-FIRME.
           MOVE LNK-MOIS   TO JRS-MOIS.
           MOVE REG-PERSON TO JRS-PERSON.
           START JOURS KEY >= JRS-KEY INVALID CONTINUE
                NOT INVALID
                PERFORM READ-HEURES THRU READ-HEURES-END.


       READ-HEURES.
           READ JOURS NEXT NO LOCK AT END
               GO READ-HEURES-END
           END-READ.
           IF FR-KEY     NOT = JRS-FIRME
           OR REG-PERSON NOT = JRS-PERSON
           OR LNK-MOIS   NOT = JRS-MOIS 
              GO READ-HEURES-END
           END-IF.
           IF JRS-POSTE > 0
           OR JRS-COMPLEMENT > 0 
              GO READ-HEURES
           END-IF.
           IF JRS-OCCUPATION = 99
              GO READ-HEURES-END
           END-IF.
           MOVE JRS-OCCUPATION TO OCC-KEY.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD FAKE-KEY.
           IF OCC-HRS-SUPPL > 0
              PERFORM ADD-HEURES VARYING JOUR FROM LAST-DAY BY 1 UNTIL
              JOUR > MOIS-JRS(LNK-MOIS)
           END-IF.
           GO READ-HEURES.
       READ-HEURES-END.
           EXIT.


       ADD-HEURES.
           IF JRS-HRS(JOUR) > 0
              ADD JRS-HRS(JOUR) TO HELP-4
           END-IF.


       HEURES-THEORIQUES.
           ADD 1 TO IDX.
           IF PRES-JOUR(LNK-MOIS, JOUR) = 1
              IF POSE-FIRME > 0
                 IF POSE-JOUR(LNK-MOIS, JOUR) > 0
                    ADD CCOL-DUREE-JOUR TO HRS-THEORIQUES 
                 END-IF
              ELSE
                 ADD HJS-HEURES(IDX) TO HRS-THEORIQUES 
              END-IF
           END-IF.


       RECENT.
           INITIALIZE POSE-RECORD HJS-RECORD CAR-RECORD SAVE-KEY.
           CALL "6-CARRI" USING LINK-V REG-RECORD CAR-RECORD SAVE-KEY.
           MOVE CAR-CCOL TO LNK-VAL.
           COMPUTE LNK-NUM = 0.
           CALL "6-GCCOL"  USING LINK-V CCOL-RECORD.
           CALL "6-GHJS" USING LINK-V HJS-RECORD  CAR-RECORD PRESENCES.
           MOVE CAR-EQUIPE TO POSE-CODE.
           CALL "6-POSE" USING LINK-V POSE-RECORD FAKE-KEY.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
