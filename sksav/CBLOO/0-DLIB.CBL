      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-DLIB  AFFICHAGE DES LIBELLES              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  0-DLIB.

       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  FAKE-KEY              PIC 9(4) COMP-1 VALUE 13.
       01  ACTION                PIC X.
           COPY "LIBELLE.REC".

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       SCREEN SECTION.

       01  ECRAN-DEFAULT.
           02 HE-Z PIC ZZZZZ USING LNK-NUM .        

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-DLIB.
           MOVE LNK-NUM TO LIB-NUMBER. 
           MOVE LNK-LANGUAGE TO LIB-LANGUAGE.
           IF LNK-POSITION = 0
              MOVE 24105000 TO LNK-POSITION
              DISPLAY ECRAN-DEFAULT
           END-IF.
           CALL "6-LIB" USING LINK-V LIB-RECORD FAKE-KEY.
           IF LNK-LOW = "L" 
              DISPLAY LIB-DESCRIPTION LINE LNK-LINE POSITION LNK-COL
              SIZE LNK-SIZE LOW
           ELSE
              DISPLAY LIB-DESCRIPTION LINE LNK-LINE POSITION LNK-COL
              SIZE LNK-SIZE.
           INITIALIZE LNK-NUM LNK-LOW LNK-POSITION.
           EXIT PROGRAM.
      