      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-POSE MODULE GENERAL LECTURE POSE          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-POSE.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "POSE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "POSE.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "POSE.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON POSE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-POSE.
       
           IF NOT-OPEN = 0
              OPEN INPUT POSE
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO POSE-RECORD.
           MOVE LNK-ANNEE TO POSE-ANNEE.
           MOVE FR-KEY TO POSE-FIRME.
           MOVE 0 TO LNK-VAL.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ POSE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ POSE NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ POSE NO LOCK 
                INVALID INITIALIZE POSE-RECORD END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY NOT = POSE-FIRME 
           OR LNK-ANNEE NOT = POSE-ANNEE
              GO EXIT-1
           END-IF.
           MOVE POSE-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START POSE KEY < POSE-KEY INVALID GO EXIT-1.
       START-2.
           START POSE KEY > POSE-KEY INVALID GO EXIT-1.


               