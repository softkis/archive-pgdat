      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-AGENCE RECHERCHE AGENCES                  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-AGENCE.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "AGENCE.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.

       01  CHOIX                 PIC X(10).
       01  COMPTEUR              PIC 99.

       01 HE-AGENCE.
          02 H-R PIC X(10) OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-TEMP.
              03 HE-JJ    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-MM    PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-AA    PIC ZZZZ.


       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "AGENCE.LNK".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       START-DISPLAY SECTION.
             
       START-2-AGENCE.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE AG-RECORD IDX-1 SAVE-KEY COMPTEUR CHOIX.
           MOVE 4 TO LIN-IDX.
           PERFORM READ-AG THRU READ-AG-END.
           IF EXC-KEY = 9 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-AG.
           CALL "6-AGENCE" USING LINK-V AG-RECORD SAVE-KEY.
           MOVE 66 TO SAVE-KEY.
           IF AG-KEY = SPACES
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65 
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-AGENCE IDX-1 SAVE-KEY
                 GO READ-AG
              END-IF
              GO READ-AG-END.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              OR EXC-KEY = 13
                 INITIALIZE HE-AGENCE IDX-1 SAVE-KEY
                 GO READ-AG
              END-IF
              IF CHOIX NOT = SPACES
                 GO READ-AG-END
              END-IF
           END-IF.
           GO READ-AG.
       READ-AG-END.
           IF CHOIX = SPACES
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82 AND NOT = 68
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX COMPTEUR IDX-1.
           MOVE AG-KEY TO H-R(IDX-1).
           DISPLAY AG-KEY LINE LIN-IDX POSITION 2 SIZE 10.
           DISPLAY AG-NOM LINE LIN-IDX POSITION 13.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0000530000 TO EXC-KFR (11).
           MOVE 5600000000 TO EXC-KFR (12).
           MOVE 0067000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE CHOIX.
           ACCEPT CHOIX 
           LINE 3 POSITION 20 SIZE 10
           TAB UPDATE NO BEEP CURSOR 1
           CONTROL "UPPER"
           ON EXCEPTION EXC-KEY CONTINUE.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 OR 67
              PERFORM END-PROGRAM.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF CHOIX NOT = SPACES
              MOVE CHOIX TO AG-KEY
              MOVE 0 TO SAVE-KEY
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.

           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IDX-1.

       AFFICHAGE-ECRAN.
           MOVE 2014 TO LNK-VAL.
           MOVE " " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       END-PROGRAM.
           MOVE CHOIX TO AG-KEY.
           CALL "6-AGENCE" USING LINK-V AG-RECORD FAKE-KEY.
           IF AG-KEY NOT = SPACES
              MOVE AG-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IDX-1 + 4.
           ACCEPT H-R(IDX-1)
             LINE  LIN-IDX POSITION 2 SIZE 10
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 82 
              MOVE H-R(IDX-1) TO CHOIX
           PERFORM END-PROGRAM.
           DISPLAY H-R(IDX-1) LINE LIN-IDX POSITION 2.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           EVALUATE EXC-KEY
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN OTHER
                     MOVE H-R(IDX-1) TO CHOIX
                     PERFORM END-PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-R(IDX-1) = SPACES
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.
                        