      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-PAREX  ANALYTIQUE PARAMETRES LECTURE      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-PAREX.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "PAREX.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "PAREX.FDE".

       WORKING-STORAGE SECTION.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       LINKAGE SECTION.

           COPY "V-LINK.CPY".
           COPY "PAREX.LNK".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.

           USE AFTER ERROR PROCEDURE ON PAREX.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  program.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-PAREX.

           OPEN INPUT PAREX.
           MOVE FR-KEY TO PAREX-FIRME.
           READ PAREX INVALID 
               INITIALIZE PAREX-RECORD
           END-READ.
           IF PAREX-FIRME = 0
              READ PAREX INVALID CONTINUE END-READ
           END-IF.
           MOVE PAREX-RECORD TO LINK-RECORD
           CLOSE PAREX.
           EXIT PROGRAM.