      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-STATUT MODULE LECTURE STATUT              �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-STATUT.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "STATUT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "STATUT.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN  PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "STATUT.LNK".

       01  EXC-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON STATUT .
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-STATUT.
       
           IF NOT-OPEN = 0
              OPEN INPUT STATUT
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO STAT-RECORD.
           IF STAT-LANGUE = SPACES
              MOVE LNK-LANGUAGE TO STAT-LANGUE 
           END-IF.
           
           EVALUATE EXC-KEY 
               WHEN 65 PERFORM START-1
               READ STATUT PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               IF STAT-LANGUE NOT = LNK-LANGUAGE
                   GO EXIT-1
               END-IF
               GO EXIT-2
               WHEN 66 PERFORM START-2
               READ STATUT NEXT NO LOCK AT END GO EXIT-1 END-READ
               IF STAT-LANGUE NOT = LNK-LANGUAGE
                   GO EXIT-1
               END-IF
               GO EXIT-2
               WHEN 4 GO EXIT-2
               WHEN OTHER GO EXIT-3
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE STAT-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-3.
           READ STATUT NO LOCK INVALID GO EXIT-1
                 NOT   INVALID GO EXIT-2.

       START-1.
           START STATUT KEY < STAT-KEY INVALID GO EXIT-1.
       START-2.
           START STATUT KEY > STAT-KEY INVALID GO EXIT-1.

