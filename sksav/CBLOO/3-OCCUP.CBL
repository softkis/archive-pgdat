      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-OCCUP IMPRESSION LISTE OCCUPATIONS        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-OCCUP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FORM80.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  JOB-STANDARD          PIC X(10) VALUE "80        ".

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "IMPRLOG.REC".
           COPY "OCCUP.REC".
           COPY "V-VAR.CPY".

       01  DSP-IDX               PIC ZZ.

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 70.
           
           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".LOC".

       01   ECR-DISPLAY.
            02 HE-Z2 PIC Z(2).
            02 HE-Z6 PIC Z(6).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-OCCUP.

           CALL "0-TODAY" USING TODAY.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.

           MOVE 0 TO CAR-NUM DEC-NUM LIN-NUM LIN-IDX.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res sp괹ifiques touches de fonctions

           MOVE 0000000025 TO EXC-KFR(1).
           MOVE 1700000000 TO EXC-KFR(2).


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           PERFORM APRES-DEC.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY 
            WHEN  5 PERFORM START-OCCUP
                    PERFORM READ-OCCUP 
           END-EVALUATE.
           IF DECISION NOT = 0
               COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
               MOVE CHOIX-MAX TO INDICE-ZONE.

       START-OCCUP.
           INITIALIZE OCC-RECORD.
           PERFORM READ-OCCUP.

       READ-OCCUP.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD SAVE-KEY.
           CALL "6-OCCTXT" USING LINK-V OCC-RECORD.
           MOVE 66 TO SAVE-KEY.
           IF OCC-NOM = SPACES
              PERFORM END-PROGRAM
           END-IF.
      *    IF LNK-LANGUAGE NOT = "F"
      *       CALL "A-CODTXT" USING LINK-V OCC-RECORD.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           IF LIN-NUM > 65
              PERFORM PAGE-DATE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              MOVE LIN-IDX TO LIN-NUM
           END-IF.
           ADD 1 TO LIN-NUM.
           PERFORM FILL-TEXTE.
           PERFORM DIS-HE-01.
           GO READ-OCCUP.

       DIS-HE-01.
           DISPLAY OCC-KEY LINE 10 POSITION 10.
           DISPLAY OCC-NOM LINE 10 POSITION 20.
       DIS-HE-END.
           EXIT.

       FILL-TEXTE.
           MOVE 4 TO CAR-NUM.
           MOVE OCC-KEY TO VH-00.
           MOVE 1 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE OCC-NOM TO ALPHA-TEXTE.
           MOVE 6 TO COL-NUM.
           PERFORM FILL-FORM.
           PERFORM DETAIL-PARAM VARYING IDX-1 
            FROM 1 BY 1 UNTIL IDX-1 > 8.

       DETAIL-PARAM.
           IF OCC-IDX(IDX-1) > 0
               MOVE OCC-IDX(IDX-1) TO VH-00
               COMPUTE COL-NUM = 30 + 5 * IDX-1
               MOVE 5 TO CAR-NUM
               PERFORM FILL-FORM.

       PAGE-DATE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  3 TO LIN-NUM CAR-NUM.
           MOVE 74 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE  2 TO LIN-NUM.
           MOVE 67 TO COL-NUM.
           MOVE TODAY-JOUR  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  2 TO CAR-NUM.
           MOVE 70 TO COL-NUM.
           MOVE TODAY-MOIS  TO VH-00.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE 73 TO COL-NUM.
           MOVE TODAY-ANNEE TO VH-00.
           PERFORM FILL-FORM.

       AFFICHAGE-ECRAN.
           MOVE 1028 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.
       TRANSMET.
           MOVE 0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P080" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           IF LIN-NUM > LIN-IDX
              PERFORM PAGE-DATE
              PERFORM TRANSMET
           END-IF.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P080" USING LINK-V FORMULAIRE.
           CANCEL "P080".
           MOVE " " TO LNK-YN.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".


