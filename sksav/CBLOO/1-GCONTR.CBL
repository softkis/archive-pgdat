      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-GCONTR  RECHERCHE  DES CONTRATS           �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-GCONTR.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "CONTRAT.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "CONTRAT.FDE".

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  NOT-OPEN  PIC 9 VALUE 0.
       01  NOT-FOUND PIC 9 VALUE 0.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "CONTRAT.LNK".

       PROCEDURE DIVISION USING LINK-V REG-RECORD LINK-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CONTRAT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-GCONTR.

           IF NOT-OPEN = 0
              OPEN INPUT CONTRAT
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO CON-RECORD.
           MOVE FR-KEY TO CON-FIRME.
           MOVE REG-PERSON TO CON-PERSON.
           INITIALIZE NOT-FOUND.
           EVALUATE CON-DEBUT-A 
                WHEN    0  PERFORM CONTRAT-RECENT
                WHEN    1  PERFORM CONTRAT-SUIVANT
                WHEN 9999  PERFORM CONTRAT-PRECEDENT
                WHEN OTHER PERFORM CONTRAT-PRECEDENT
           END-EVALUATE.
           PERFORM TEST-CONTRAT.
           MOVE CON-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       CONTRAT-RECENT.
           MOVE LNK-ANNEE TO CON-DEBUT-A.
           MOVE LNK-MOIS  TO CON-DEBUT-M.
           MOVE MOIS-JRS(LNK-MOIS) TO CON-DEBUT-J.
           PERFORM CONTRAT-PRECEDENT.

       CONTRAT-PRECEDENT.
           START CONTRAT KEY <= CON-KEY INVALID
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ CONTRAT PREVIOUS AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           PERFORM TEST-CONTRAT.
           IF CON-FIRME = 0
              MOVE FR-KEY TO CON-FIRME
              MOVE REG-PERSON TO CON-PERSON
              MOVE 0 TO NOT-FOUND
              PERFORM CONTRAT-SUIVANT.

       CONTRAT-SUIVANT.
           START CONTRAT KEY > CON-KEY INVALID 
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ CONTRAT NEXT AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.

       TEST-CONTRAT.
           IF REG-PERSON NOT = CON-PERSON
           OR FR-KEY     NOT = CON-FIRME
           OR NOT-FOUND  NOT = 0
              INITIALIZE CON-RECORD
           END-IF.
