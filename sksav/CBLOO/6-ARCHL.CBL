      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-ARCHL LECTURE  LIVRE DE PAIE ARCHIVE      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-ARCHL.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "LIVRE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "LIVRE.FDE".

       WORKING-STORAGE SECTION.

           COPY "V-VAR.CPY".
       01  NOT-OPEN              PIC 9 VALUE 0.

       01  LIVRE-NAME.
           02 FILLER             PIC X(9) VALUE "S-LIVARC.".
           02 ANNEE-LIVRE        PIC 999  VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "REGISTRE.REC".
           COPY "LIVRE.LNK".

       01  EXE-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING 
                          LINK-V 
                          LINK-RECORD
                          REG-RECORD 
                          EXE-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON LIVRE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-A-NXLP.

           IF ANNEE-LIVRE NOT = 0
           AND LNK-SUFFIX NOT = ANNEE-LIVRE
              CLOSE LIVRE
              MOVE 0 TO ANNEE-LIVRE
           END-IF.

           IF ANNEE-LIVRE = 0
              MOVE LNK-SUFFIX TO ANNEE-LIVRE
              OPEN I-O LIVRE
           END-IF.

           MOVE LINK-RECORD TO L-RECORD.
           MOVE FR-KEY TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON.
           IF L-MOIS = 0
           AND EXE-KEY NOT = 66
           AND EXE-KEY NOT = 67
              MOVE LNK-MOIS TO L-MOIS
           END-IF.
           IF L-SUITE = 0
           AND LNK-SUITE > 0
           AND EXE-KEY NOT = 66
              MOVE LNK-SUITE TO L-SUITE
           END-IF.
           IF L-ARCHIVE = 0
           AND LNK-NUM > 0
           AND EXE-KEY NOT = 66
              MOVE LNK-NUM TO L-ARCHIVE
           END-IF.
           MOVE L-FIRME   TO L-FIRME-1   L-FIRME-2.
           MOVE L-PERSON  TO L-PERSON-1  L-PERSON-2.
           MOVE L-MOIS    TO L-MOIS-1    L-MOIS-2.
           MOVE L-SUITE   TO L-SUITE-1   L-SUITE-2.
           MOVE L-ARCHIVE TO L-ARCHIVE-1 L-ARCHIVE-2.

           IF EXE-KEY = 99
              CALL "0-TODAY" USING TODAY
              MOVE TODAY-TIME TO L-TIME
              MOVE LNK-USER TO L-USER
              WRITE L-RECORD INVALID REWRITE L-RECORD
              EXIT PROGRAM
           END-IF.
           IF EXE-KEY = 98
              DELETE LIVRE INVALID CONTINUE
              EXIT PROGRAM
           END-IF.
           MOVE 0 TO NOT-FOUND.
           EVALUATE EXE-KEY 
               WHEN 65 PERFORM PREV-LIVRE
               WHEN 66 PERFORM NEXT-LIVRE
               WHEN 67 PERFORM START-LIVRE
               WHEN 13 PERFORM READ-LIVRE
               WHEN  0 MOVE 0 TO LNK-NUM
                       PERFORM START-LIVRE
                       PERFORM NEXT-ARCHIVE
           END-EVALUATE.
           MOVE L-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-LIVRE.
           START LIVRE KEY >= L-KEY INVALID 
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ LIVRE NEXT NO LOCK AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           PERFORM TEST-LIVRE.

       NEXT-LIVRE.
           START LIVRE KEY > L-KEY INVALID 
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ LIVRE NEXT NO LOCK AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           PERFORM TEST-LIVRE.

       PREV-LIVRE.
           START LIVRE KEY < L-KEY INVALID KEY
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ LIVRE PREVIOUS NO LOCK AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           PERFORM TEST-LIVRE.

       READ-LIVRE.
           READ LIVRE NO LOCK INVALID INITIALIZE L-RECORD.

       TEST-LIVRE.
           IF FR-KEY     NOT = L-FIRME
           OR REG-PERSON NOT = L-PERSON
              MOVE 1 TO NOT-FOUND.
           IF NOT-FOUND = 1 
              INITIALIZE L-RECORD.

       NEXT-ARCHIVE.
           IF NOT-FOUND = 1 
           OR L-MOIS NOT = LNK-MOIS
              EXIT PROGRAM
           END-IF.
           MOVE L-ARCHIVE TO LNK-NUM.
           PERFORM NEXT-LIVRE.
           GO NEXT-ARCHIVE.
           