      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-OCCOM MODULE GENERAL LECTURE COMPLEMENT   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-OCCOM.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "OCCOM.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "OCCOM.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "OCCOM.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON COMPLEMENT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-OCCOM.
       
           IF NOT-OPEN = 0
              OPEN I-O COMPLEMENT
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO OCO-RECORD.
           IF EXC-KEY < 98
              MOVE FR-KEY TO OCO-FIRME.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ COMPLEMENT PREVIOUS NO LOCK AT END GO EXIT-1 
               END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ COMPLEMENT NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 98 DELETE COMPLEMENT INVALID CONTINUE END-DELETE
                   EXIT PROGRAM
           WHEN 99 WRITE  OCO-RECORD INVALID CONTINUE END-WRITE
                   EXIT PROGRAM
           WHEN OTHER READ COMPLEMENT NO LOCK INVALID INITIALIZE 
                OCO-REC-DET END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY NOT = OCO-FIRME 
              GO EXIT-1
           END-IF.
           MOVE OCO-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START COMPLEMENT KEY < OCO-KEY INVALID GO EXIT-1.
       START-2.
           START COMPLEMENT KEY > OCO-KEY INVALID GO EXIT-1.


               