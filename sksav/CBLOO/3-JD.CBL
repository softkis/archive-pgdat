      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-JD IMPRESSION JOURNAL DE PAIE COMPTA      �
      *  � PRESENTATION DETAIL PAR COMPTE                        �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-JD.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURNAL.FC".
           COPY "FORM130.FC".
           COPY "INTERCOM.FC".
           COPY "PLAN.FC".
           COPY "PAREX.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "JOURNAL.FDE".
           COPY "FORM130.FDE".
           COPY "PLAN.FDE".
           COPY "PAREX.FDE".

       FD  INTER
           LABEL RECORD STANDARD
           DATA RECORD INTER-RECORD.
      *    Fichier interm괺iaire JOURNAL COMPTABLE

       01  INTER-RECORD.
           02 INTER-KEY.
              03 INTER-MOIS   PIC 99.
              03 INTER-SUITE  PIC 99.
              03 INTER-COUT   PIC 9(8).
              03 INTER-DC     PIC 9.
              03 INTER-COMPTE PIC X(20).
              03 INTER-POSTE  PIC 9(8).
              03 INTER-LIB    PIC 9999.

           02 INTER-REC-DET.
              03 INTER-VALUE PIC S9(8)V99.

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  JOB-STANDARD          PIC X(10) VALUE "130       ".

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  TIRET-TEXTE           PIC X VALUE ".".
       01  DEBIT                 PIC X(20).
       01  CREDIT                PIC X(20).

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "COUT.REC".
           COPY "IMPRLOG.REC".
           COPY "POCL.REC".
           COPY "COMPTE.REC".
           COPY "LIBELLE.REC".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(130) OCCURS 70.

       01  PROC-IDX        PIC 9 VALUE 0.
       01  DC-IDX          PIC 9.
       01  CENTRE-COUT     PIC 9999 VALUE 0.
       01  COUT            PIC 9999 VALUE 0.
       01  POST            PIC 9(8) VALUE 0.
           
           COPY "V-VH00.CPY".

       01  JOURNAL-NAME.
           02 PRECISION          PIC X(4) VALUE "S-JO".
           02 FIRME-JOURNAL      PIC 9999.
           02 FILLER             PIC X VALUE ".".
           02 ANNEE-JOURNAL      PIC 999.

       01  FORM-NAME.
           02  FILLER            PIC X(4) VALUE "FORM".
           02  FORM-LANGUE       PIC X.
           02  FORM-EXTENSION    PIC X(4) VALUE ".CPT".

       01  INTER-NAME.
           02 FILLER             PIC XX VALUE "IN".
           02 FIRME-INTER        PIC 999999.
           02 FILLER             PIC XXXX VALUE ".COM".

       01  CUMUL.
           02 CUMUL-D            PIC S9(8)V99.
           02 CUMUL-C            PIC S9(8)V99.
       01  CUMUL-R REDEFINES CUMUL.
           02 CUMUL-DC           PIC S9(8)V99 OCCURS 2.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC -Z(6),ZZ.



       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON 
               JOURNAL 
               FORM 
               INTER
               PLAN  
               PAREX.

       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-JD.
       
           CALL "0-TODAY" USING TODAY.
           MOVE LNK-SUFFIX TO ANNEE-JOURNAL.
           MOVE FR-KEY  TO FIRME-JOURNAL 
                           FIRME-INTER 
                           PAREX-FIRME.
           OPEN INPUT PAREX.
           READ PAREX INVALID 
               INITIALIZE PAREX-RECORD
           END-READ.
           IF PAREX-FIRME = 0
              READ PAREX INVALID CONTINUE END-READ
           END-IF.
           CLOSE PAREX.

           OPEN I-O   PLAN.
           OPEN I-O   JOURNAL.

           MOVE LNK-LANGUAGE TO FORM-LANGUE.


           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN CHOIX-MAX  MOVE 0100000025 TO EXC-KFR (1)
                           MOVE 1700000000 TO EXC-KFR (2)
                           MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN CHOIX-MAX PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-DEC.
           IF EXC-KEY = 6 
              MOVE "Y" TO LNK-YN
              MOVE 5 TO EXC-KEY
           ELSE
              MOVE " " TO LNK-YN
           END-IF.
           EVALUATE EXC-KEY 
            WHEN 5 PERFORM CUMUL-JOURNAL
                   PERFORM END-PROGRAM
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           

       CUMUL-VALEURS.
           MOVE 1 TO DC-IDX.
           PERFORM PROC VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 200.
           MOVE 2 TO DC-IDX.
           PERFORM PROC VARYING IDX-1 FROM 201 BY 1 UNTIL IDX-1 > 300.

       PROC.
           IF JRL-V(IDX-1) NOT =  0
              MOVE JRL-V(IDX-1) TO VH-00
              PERFORM WRITE-INTER
           END-IF.

       WRITE-INTER.
           INITIALIZE INTER-RECORD.
           MOVE DC-IDX TO INTER-DC.
           MOVE PLAN-LIB(IDX-1) TO INTER-LIB.
           MOVE PLAN-COM(IDX-1) TO INTER-COMPTE.
           MOVE JRL-MOIS  TO INTER-MOIS.
           MOVE JRL-SUITE TO INTER-SUITE.
           MOVE JRL-COUT  TO INTER-COUT.
           IF PAREX-PRINT(IDX-1) > 0
              MOVE JRL-POSTE TO INTER-POSTE.
           PERFORM WRITE-REC-INTER.
           IF INTER-LIB NOT = 0
              INITIALIZE INTER-LIB
              PERFORM WRITE-REC-INTER
           ELSE
              PERFORM WRITE-REC-INTER
           END-IF.
           INITIALIZE INTER-POSTE INTER-LIB.
           IF PAREX-PRINT(IDX-1) > 0
              PERFORM WRITE-REC-INTER.
        
       WRITE-REC-INTER.
           READ INTER INVALID MOVE 0 TO INTER-VALUE.
           ADD VH-00 TO INTER-VALUE.
           WRITE INTER-RECORD INVALID REWRITE INTER-RECORD END-WRITE.

       CUMUL-JOURNAL.
           DELETE FILE INTER.
           INITIALIZE JRL-RECORD.
           OPEN I-O INTER WITH LOCK.
           MOVE 0 TO NOT-FOUND.
           START JOURNAL KEY > JRL-KEY-4 INVALID CONTINUE
              NOT INVALID
              PERFORM LECTURE-JOURNAL THRU LECTURE-JOURNAL-END.

       LECTURE-JOURNAL.
           READ JOURNAL NEXT AT END GO LECTURE-JOURNAL-END.
           IF JRL-ANNEE-C > 0 
              GO LECTURE-JOURNAL-END.
           MOVE 0 TO PAGE-NUMBER.
           INITIALIZE CUMUL.
           PERFORM READ-COUT.
           PERFORM READ-PLAN THRU READ-PLAN-END.
           PERFORM CUMUL-VALEURS.
           PERFORM READ-POCL.
           PERFORM DIS-HE-01.
           MOVE TODAY-ANNEE TO JRL-ANNEE-C.
           MOVE TODAY-MOIS  TO JRL-MOIS-C.
           MOVE TODAY-JOUR  TO JRL-JOUR-C.
           WRITE JRL-RECORD INVALID REWRITE JRL-RECORD END-WRITE.
           GO LECTURE-JOURNAL.
       LECTURE-JOURNAL-END.
           PERFORM FULL-PROCESS.
           CLOSE INTER.
           DELETE FILE INTER.

       FULL-PROCESS.
           INITIALIZE INTER-RECORD LIN-NUM CENTRE-COUT.
           START INTER KEY > INTER-KEY INVALID CONTINUE
            NOT INVALID
           PERFORM READ-INTER THRU READ-INTER-END.

       READ-INTER.
           READ INTER NEXT AT END GO READ-INTER-END.
           IF COUNTER = 0 
              PERFORM READ-IMPRIMANTE 
              MOVE 0 TO LIN-NUM
              MOVE 1 TO COUNTER
           END-IF.
           PERFORM TEST-PRINTER.
           MOVE INTER-COUT TO CENTRE-COUT.
           PERFORM FILL-TEXTE.
           GO READ-INTER.
       READ-INTER-END.
           PERFORM TEST-PRINTER.
           PERFORM TOTALS.
           IF LIN-NUM >= LIN-IDX
              PERFORM ENTETE
              PERFORM TRANSMET
           END-IF.


       TEST-PRINTER.
           IF  CENTRE-COUT > 0
           AND INTER-COUT NOT = CENTRE-COUT 
              PERFORM TOTALS
              INITIALIZE CUMUL
              MOVE 70 TO LIN-NUM
           END-IF.
           IF LIN-NUM > 68
              PERFORM ENTETE
              PERFORM TRANSMET
              MOVE 0 TO LIN-NUM
           END-IF.
           IF LIN-NUM = 0
              PERFORM READ-FORM
              COMPUTE LIN-NUM = LIN-IDX - 1
           END-IF.


       FILL-TEXTE.
           ADD 1 TO LIN-NUM.
           PERFORM TRAITS.
           IF INTER-POSTE = 0
              IF INTER-LIB NOT = 0
                 PERFORM LIBELLES
              ELSE 
                 PERFORM COMPTE
              END-IF
           ELSE 
              PERFORM POSTE   
           END-IF.
           PERFORM TRAITS.

       TRAITS.
           MOVE "�" TO ALPHA-TEXTE.
           MOVE 54 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE "�" TO ALPHA-TEXTE.
           MOVE 101 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE "�" TO ALPHA-TEXTE.
           MOVE 115 TO COL-NUM.
           PERFORM FILL-FORM.

       COMPTE.
           ADD 1 TO LIN-NUM.
           PERFORM TEST-LINE.
           PERFORM TRAITS.
           MOVE LNK-LANGUAGE TO COM-LANGUAGE.
           MOVE INTER-COMPTE TO COM-NUMBER.
           CALL "6-COMPTE" USING LINK-V COM-RECORD FAKE-KEY.
           MOVE COM-NOM TO ALPHA-TEXTE.
           IF INTER-DC = 1
              MOVE  6 TO COL-NUM
           ELSE
              MOVE 55 TO COL-NUM.
           PERFORM FILL-FORM.
           PERFORM TRAITS.
           ADD 1 TO LIN-NUM.
           IF INTER-DC = 1
              MOVE  6 TO COL-NUM
           ELSE
              MOVE 55 TO COL-NUM.

           PERFORM TIRET UNTIL COL-NUM > 100.
           IF INTER-DC = 1
              MOVE  6 TO COL-NUM
           ELSE
              MOVE 55 TO COL-NUM.
           MOVE INTER-COMPTE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  8 TO CAR-NUM.
           MOVE  2 TO DEC-NUM.
           COMPUTE COL-NUM = 90 + 13 * INTER-DC.
           MOVE INTER-VALUE TO VH-00.
           ADD  INTER-VALUE TO CUMUL-DC(INTER-DC).
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.

       TOTALS.
           ADD 1 TO LIN-NUM.
           PERFORM TEST-LINE.
           MOVE   8 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE 103 TO COL-NUM.
           MOVE CUMUL-D TO VH-00.
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.
           MOVE   8 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           MOVE 116 TO COL-NUM.
           MOVE CUMUL-C TO VH-00.
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.

       TEST-LINE.
           IF LIN-NUM > 68
              PERFORM ENTETE
              PERFORM TRANSMET
              PERFORM READ-FORM
              COMPUTE LIN-NUM = LIN-IDX 
           END-IF.

       DIS-HE-01.
           MOVE JRL-COUT TO HE-Z6.
           DISPLAY HE-Z6 LINE 14 POSITION 13.
           MOVE JRL-SUITE TO HE-Z6.
           DISPLAY HE-Z6  LINE 14 POSITION 20.
           DISPLAY COUT-NOM LINE 14 POSITION 30.
           DISPLAY PC-NOM  LINE 15 POSITION 30.
       DIS-HE-END.
           EXIT.

       ENTETE.
           MOVE 2 TO LIN-NUM.

           IF EXC-KEY = 5 
              MOVE 35 TO COL-NUM
              MOVE JRL-MOIS TO LNK-NUM
              PERFORM MOIS-NOM-1.
           IF FR-FREQ-COMPTA = 2 
              MOVE 107 TO LNK-NUM
              MOVE "AA" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO ALPHA-TEXTE 
           END-IF.
           PERFORM FILL-FORM.

           ADD 1 TO COL-NUM.
           MOVE LNK-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

           IF FR-FREQ-COMPTA = 1
              MOVE 105 TO LNK-NUM
              MOVE "AA" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO ALPHA-TEXTE 
              MOVE 50 TO COL-NUM
              PERFORM FILL-FORM
              COMPUTE VH-00 = JRL-MOIS / 3
              MOVE 1 TO CAR-NUM
              ADD  1 TO COL-NUM
              PERFORM FILL-FORM
           END-IF.

      * DONNEES FIRME
           COMPUTE LIN-NUM =  3.
           MOVE 10 TO COL-NUM.
           MOVE FR-ETAB-A TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 15 TO COL-NUM.
           MOVE FR-ETAB-N TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 20 TO COL-NUM.
           MOVE FR-SNOCS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 24 TO COL-NUM.
           MOVE FR-EXTENS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM =  4.
           MOVE FR-KEY TO VH-00.
           MOVE 4 TO CAR-NUM.
           MOVE 3 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 10 TO COL-NUM.
           MOVE FR-NOM TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM =  5.
           MOVE 10 TO COL-NUM.
           MOVE FR-MAISON TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 15 TO COL-NUM.
           MOVE FR-RUE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           COMPUTE LIN-NUM =  6.
           MOVE 10 TO COL-NUM.
           MOVE FR-PAYS TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  6 TO CAR-NUM.
           ADD 1 TO COL-NUM.
           MOVE FR-CODE-POST TO  VH-00.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE FR-LOCALITE TO  ALPHA-TEXTE.
           PERFORM FILL-FORM.

      *    DATE EDITION
           CALL "0-TODAY" USING TODAY.
           COMPUTE LIN-NUM =  4.
           MOVE 116 TO COL-NUM.
           MOVE TODAY-JOUR TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 119 TO COL-NUM.
           MOVE TODAY-MOIS TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 122 TO COL-NUM.
           MOVE TODAY-ANNEE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD 1 TO LIN-NUM.
           MOVE 116 TO COL-NUM.
           MOVE TODAY-HEURE TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE ":" TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE TODAY-MIN TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
            
           PERFORM READ-COUT.
           MOVE COUT-NUMBER TO VH-00.
           MOVE 4 TO CAR-NUM.
           MOVE 70 TO COL-NUM.
           PERFORM FILL-FORM.
           ADD  1 TO COL-NUM.
           MOVE COUT-NOM  TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           ADD  1 TO COL-NUM.
           MOVE COUT-ANALYTIC TO ALPHA-TEXTE.
           PERFORM FILL-FORM.

      * NUMERO PAGE.
           ADD 1 TO PAGE-NUMBER.
           MOVE  2 TO LIN-NUM.
           MOVE  4 TO CAR-NUM.
           MOVE 122 TO COL-NUM.
           MOVE PAGE-NUMBER TO VH-00.
           PERFORM FILL-FORM.
           IF EXC-KEY = 5
              PERFORM DATE-MAJ.

       DATE-MAJ.
           COMPUTE LIN-NUM =  3.
           MOVE 116 TO COL-NUM.
           MOVE JRL-JOUR-M TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 119 TO COL-NUM.
           MOVE JRL-MOIS-M TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE 122 TO COL-NUM.
           MOVE JRL-ANNEE-M TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE  4 TO CAR-NUM.
           MOVE  6 TO LIN-NUM.
           MOVE 122 TO COL-NUM.
           MOVE JRL-SUITE TO VH-00.
           PERFORM FILL-FORM.

       READ-COUT.
           MOVE FR-KEY    TO COUT-FIRME. 
           MOVE JRL-COUT  TO COUT-NUMBER.
           IF CENTRE-COUT NOT = 0
              MOVE CENTRE-COUT TO COUT-NUMBER.
           CALL "6-COUT" USING LINK-V COUT-RECORD FAKE-KEY.

       READ-POCL.
           MOVE JRL-POSTE TO PC-NUMBER.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY.

       POSTE.
           IF INTER-LIB = 0
              PERFORM POSTES
           ELSE 
              PERFORM LIBELLES
           END-IF.

       POSTES.
           MOVE INTER-POSTE TO PC-NUMBER.
           CALL "6-POCL" USING LINK-V PC-RECORD "N" FAKE-KEY.
           MOVE PC-NUMBER TO VH-00.
           IF INTER-DC = 1
              MOVE  2 TO COL-NUM
           ELSE
              MOVE 55 TO COL-NUM.
           MOVE 8 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           PERFORM FILL-FORM.
           ADD 1 TO COL-NUM.
           MOVE PC-NOM TO ALPHA-TEXTE.
           PERFORM FILL-FORM.
           MOVE   8 TO CAR-NUM.
           MOVE   2 TO DEC-NUM.
           IF INTER-DC = 1
              MOVE 41 TO COL-NUM
           ELSE
              MOVE 88 TO COL-NUM.
           MOVE INTER-VALUE TO VH-00.
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.

       LIBELLES.
           MOVE INTER-LIB TO LIB-NUMBER.
           CALL "6-LIB" USING LINK-V LIB-RECORD EXC-KEY.
           MOVE LIB-DESCRIPTION TO ALPHA-TEXTE.
           IF INTER-DC = 1
              MOVE 11 TO COL-NUM
           ELSE
              MOVE 60 TO COL-NUM.
           PERFORM FILL-FORM.
           MOVE 1 TO POINTS.
           MOVE 8 TO CAR-NUM.
           MOVE 2 TO DEC-NUM.
           IF INTER-DC = 1
              MOVE 31 TO COL-NUM
           ELSE
              MOVE 78 TO COL-NUM.
           MOVE INTER-VALUE TO VH-00.
           MOVE 1 TO POINTS.
           PERFORM FILL-FORM.

       READ-PLAN.
           INITIALIZE PLAN-RECORD.
           IF COUT-PLAN > 0
              MOVE COUT-PLAN TO PLAN-NUMERO
              READ PLAN NO LOCK INVALID CONTINUE
              NOT INVALID GO READ-PLAN-END END-READ
           END-IF.
           INITIALIZE PLAN-RECORD.
           READ PLAN NO LOCK INVALID CONTINUE
               NOT INVALID MOVE 0 TO NOT-FOUND.
       READ-PLAN-END.
           EXIT.


       TIRET.
           STRING TIRET-TEXTE DELIMITED BY SIZE
           INTO FORM-LINE(LIN-NUM) POINTER COL-NUM 
           ON OVERFLOW CONTINUE END-STRING.

       AFFICHAGE-ECRAN.
           MOVE 1431 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       TRANSMET.
           MOVE  0 TO LNK-VAL.
           MOVE 70 TO LNK-LINE.
           CALL "P130" USING LINK-V FORMULAIRE.
           ADD 1 TO COUNTER.
           MOVE 0 TO LIN-NUM.

       END-PROGRAM.
           CLOSE JOURNAL.
           CLOSE PLAN.
           IF COUNTER > 0
              MOVE 99 TO LNK-VAL
              CALL "P130" USING LINK-V FORMULAIRE.
           CANCEL "P130".
           DELETE FILE INTER.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XFORM.CPY".
           COPY "XFILL2.CPY".
           COPY "XPRINT.CPY".
           COPY "XMOISNOM.CPY".
