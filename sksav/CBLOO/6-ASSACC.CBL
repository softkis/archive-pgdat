      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-ASSACC RECHERCHE TAUX ASSURANCE ACCIDENTS       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-ASSACC.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "TXACCID.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "TXACCID.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "TXACCID.LNK".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TXACC.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-ASSACC.
       
           IF NOT-OPEN = 0
              OPEN I-O TXACC
              MOVE 1 TO NOT-OPEN.

           MOVE LNK-ANNEE TO TXA-ANNEE.
           READ TXACC NO LOCK INVALID INITIALIZE TXA-RECORD END-READ.
           MOVE TXA-RECORD TO LINK-RECORD.
           
           EXIT PROGRAM.


      
