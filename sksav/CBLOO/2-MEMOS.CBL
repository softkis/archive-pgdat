      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-MEMOS MEMOS DU MOIS                       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-MEMOS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MEMO.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "MEMO.FDE".

       WORKING-STORAGE SECTION.

           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 1.

       01  CHOIX                 PIC 9(6) VALUE 0.

       01  ECR-DISPLAY.
           02 HE-Z4 PIC ZZZZ.
           02 HE-Z6 PIC Z(6).
           02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MEMO.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-2-MEMOS.

           OPEN INPUT MEMO.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM AFFICHE-DEBUT.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴

       AFFICHE-DEBUT.
           INITIALIZE REG-RECORD NOT-FOUND EXC-KEY CHOIX.
           MOVE FR-KEY TO REG-FIRME.
           MOVE 2 TO LIN-IDX.
           PERFORM READ-PERSON THRU READ-PERSON-END.
           GO AFFICHE-DEBUT.

       READ-PERSON.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" NX-KEY.
           IF REG-PERSON = 0
              GO READ-PERSON-END
           END-IF.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.
           IF PRES-TOT(LNK-MOIS) = 0
              GO READ-PERSON
           END-IF.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           MOVE FR-KEY TO MEM-FIRME.
           MOVE REG-PERSON TO MEM-PERSON.
           MOVE LNK-ANNEE TO MEM-ANNEE.
           MOVE LNK-MOIS  TO MEM-MOIS.
           READ MEMO NO LOCK INVALID 
              GO READ-PERSON
           END-READ.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 20
               PERFORM INTERRUPT THRU INTERRUPT-END
           END-IF.
           GO READ-PERSON.
       READ-PERSON-END.
           ADD 1 TO LIN-IDX.
           PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21.
           PERFORM INTERRUPT THRU INTERRUPT-END.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX.
           MOVE REG-PERSON TO HE-Z6.
           DISPLAY HE-Z6  LINE LIN-IDX POSITION 1.
           CALL "4-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE LIN-IDX POSITION 8.
           DISPLAY MEM-TEXTE(1) LINE LIN-IDX POSITION 40 size 39.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR(3).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           ACCEPT CHOIX 
           LINE 23 POSITION 30 SIZE 6
           TAB UPDATE NO BEEP CURSOR  1
           ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 66
              MOVE 13 TO EXC-KEY
           END-IF.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.

           IF CHOIX NOT = 0
              COMPUTE REG-PERSON = CHOIX - 1
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 2 TO LIN-IDX.

       AFFICHAGE-ECRAN.
           MOVE 2239 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       END-PROGRAM.
           CLOSE MEMO.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".

