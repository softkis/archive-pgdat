      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-HOC INTERROGATION OCCUPATION MOIS         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-HOC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "JOURS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "JOURS.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "OCCUP.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  HELP-VAL              PIC ZZ BLANK WHEN ZERO.
       01  HELP-1                PIC 9999.
       01  HELP-2                PIC 9.
       01  CHOIX                 PIC X.
       01  COMPTEUR              PIC 99.
       
       01  JOURS-NAME.
           02 FILLER             PIC X(8) VALUE "S-JOURS.".
           02 ANNEE-JOURS        PIC 999.
  
       01   ECR-DISPLAY.
            02 HE-Z2 PIC ZZ.
            02 HE-Z3Z2 PIC ZZZ,ZZ.
            02 HE-Z4 PIC Z(4).
            02 HE-Z6 PIC Z(6).
            02 HE-Z8 PIC Z(8).

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.
      *袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴�

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON JOURS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-2-HOC.
       
           MOVE LNK-SUFFIX TO ANNEE-JOURS.
           OPEN INPUT JOURS.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
       TRAITEMENT-ECRAN-01.
      
      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
              WHEN 1  MOVE 0064000000 TO EXC-KFR(1)
                      MOVE 0000130000 TO EXC-KFR(3)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
           END-EVALUATE.

           PERFORM DISPLAY-F-KEYS.


           PERFORM AVANT-1.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           
           PERFORM APRES-1.

           GO TRAITEMENT-ECRAN-01.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT OCC-KEY
             LINE  3 POSITION 2 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 PERFORM CHANGE-MOIS
             MOVE 27 TO EXC-KEY
           END-EVALUATE.

       APRES-1.
           EVALUATE EXC-KEY
           WHEN  2 CALL "2-OCCUP" USING LINK-V OCC-RECORD 
                   CANCEL "2-OCCUP"
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM DISPLAY-F-KEYS
           WHEN  OTHER PERFORM NEXT-OCCUP 
           END-EVALUATE.
           IF OCC-NOM = SPACES
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM DIS-01 THRU DIS-END.
           IF INPUT-ERROR = 0
              PERFORM AFFICHAGE-DETAIL.

       AFFICHAGE-ECRAN.
           MOVE 2351 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           PERFORM AFFICHAGE-CALENDRIER.

       NEXT-OCCUP.
           CALL "6-OCCUP" USING LINK-V OCC-RECORD EXC-KEY.
           CALL "6-OCCTXT" USING LINK-V OCC-RECORD.

       AFFICHAGE-CALENDRIER.
           MOVE  5 TO LNK-LINE.
           MOVE 19 TO LNK-COL.
           MOVE LNK-MOIS TO LNK-SIZE.
           CALL "0-DSCAL" USING LINK-V.

       AFFICHAGE-DETAIL.
           MOVE 5 TO LIN-IDX.
           PERFORM AFFICHAGE-JOURS.

       AFFICHAGE-JOURS.
           INITIALIZE JRS-RECORD NOT-FOUND IDX.
           MOVE FR-KEY TO JRS-FIRME.
           MOVE LNK-MOIS  TO JRS-MOIS.
           START JOURS KEY >= JRS-KEY INVALID CONTINUE
               NOT INVALID PERFORM READ-JOURS THRU READ-JRS-END.

       READ-JOURS.
           READ JOURS NEXT NO LOCK AT END
               GO READ-JRS-END
           END-READ.
           IF FR-KEY NOT = JRS-FIRME
           OR LNK-MOIS  NOT = JRS-MOIS 
              GO READ-JRS-END
           END-IF.
           IF JRS-OCCUPATION NOT = OCC-KEY
           OR JRS-COMPLEMENT NOT = 0
           OR JRS-POSTE      NOT = 0
              GO READ-JOURS
           END-IF.
           ADD 1 TO IDX.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.
           MOVE JRS-PERSON TO REG-PERSON HE-Z6.
           CALL "6-REGIS" USING LINK-V REG-RECORD "N" FAKE-KEY.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.
           DISPLAY HE-Z6 LINE LIN-IDX POSITION 2 LOW.
           DISPLAY PR-NOM LINE LIN-IDX POSITION 10.
           MOVE 0 TO COL-IDX
           INSPECT PR-NOM TALLYING COL-IDX FOR CHARACTERS BEFORE "    ".
           ADD 12 TO COL-IDX.
           DISPLAY PR-PRENOM LINE LIN-IDX POSITION COL-IDX LOW.
           ADD 1 TO LIN-IDX.
           MOVE 17 TO COL-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 78.
           PERFORM DISPLAY-JOUR 
                   VARYING IDX-1 FROM 1 BY 1 UNTIL IDX-1 > 31.
           MOVE JRS-HRS(32) TO HE-Z3Z2.
           DISPLAY HE-Z3Z2 LINE LIN-IDX POSITION 4.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              MOVE 5 TO LIN-IDX
           END-IF.
           IF EXC-KEY = 27
           OR EXC-KEY = 52
              GO READ-JRS-END.
           GO READ-JOURS.
       READ-JRS-END.
           ADD 1 TO LIN-IDX.
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 23.

       DISPLAY-JOUR.
           ADD 2 TO COL-IDX.
           DIVIDE IDX-1 BY 2 GIVING IDX-3 REMAINDER IDX-4.
           IF JRS-HRS(IDX-1) > 0
              MOVE JRS-HRS(IDX-1) TO HELP-VAL
              IF IDX-4 = 0
               DISPLAY HELP-VAL LINE LIN-IDX POSITION COL-IDX HIGH
              ELSE
               DISPLAY HELP-VAL LINE LIN-IDX POSITION COL-IDX REVERSE
              END-IF
           END-IF.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052000000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           ACCEPT CHOIX 
           LINE 24 POSITION 30 SIZE 1
           TAB UPDATE NO BEEP CURSOR 1
           ON EXCEPTION EXC-KEY CONTINUE.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 66
              MOVE 13 TO EXC-KEY.
           IF EXC-KEY = 82 OR 67
              PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
       INTERRUPT-END.
           EXIT.

       END-PROGRAM.
           CLOSE JOURS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

       DIS-01.
           MOVE OCC-KEY TO HE-Z2.
           DISPLAY HE-Z2   LINE 3 POSITION 2.
           DISPLAY OCC-NOM LINE 3 POSITION 5 SIZE 20.
       DIS-END.
           EXIT.

       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 SUBTRACT 1 FROM LNK-MOIS
             WHEN 58 ADD 1 TO LNK-MOIS
           END-EVALUATE.
           IF LNK-MOIS = 0 
              MOVE 1 TO LNK-MOIS
           END-IF.
           IF LNK-MOIS > 12 
              MOVE 12 TO LNK-MOIS
           END-IF.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-CALENDRIER.
           PERFORM AFFICHAGE-DETAIL.

