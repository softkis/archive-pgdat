      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-ARCHIV  CONTROLE + MODIFICATION ARCHIVES  �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  1-ARCHIV .

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       DATA DIVISION.


       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  CHOIX-MAX             PIC 99 VALUE 2.
       01  PRECISION             PIC 9 VALUE 0.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "PRESENCE.REC".
           COPY "LIVRE.REC".
           COPY "CODARCH.REC".
           COPY "V-VAR.CPY".
       01  ARC-IDX               PIC 99 COMP-1.
       01  SUITE-IDX             PIC 99 VALUE 2.
       01  INTERMEDIATE          PIC 999V99.
       01  ARROW                 PIC X VALUE ">".

       01  ECR-DISPLAY.
           02 HE-Z1 PIC Z BLANK WHEN ZERO.
           02 HE-Z2 PIC Z(2) BLANK WHEN ZERO.
           02 HE-Z6 PIC Z(6) BLANK WHEN ZERO.
           02 HEZ4Z2 PIC Z(4),ZZ.
           02 HEZ5Z2 PIC Z(5),ZZ.
           02 HEZ5Z1 PIC -Z(5),Z.
           02 HE-DATE .
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC 99.

       01 HE-VIR.
          02 H-V OCCURS 15.
             03 HE-LP-SUITE  PIC 99.
             03 HE-LP-ARCHIV PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-ARCHIV.
       
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.


      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1     MOVE 0163640400 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN 2     MOVE 0129000000 TO EXC-KFR(1)
                      MOVE 0000000065 TO EXC-KFR(13) 
                      MOVE 6600000000 TO EXC-KFR(14)
           WHEN OTHER MOVE 0000080000 TO EXC-KFR (2).
           IF INDICE-ZONE = CHOIX-MAX
                      MOVE 0052000000 TO EXC-KFR (11).


           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  OTHER PERFORM AVANT-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  OTHER PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 53 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           MOVE FR-KEY TO REG-FIRME.
           ACCEPT REG-PERSON 
             LINE 3 POSITION 15 SIZE 6
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           EVALUATE EXC-KEY
             WHEN 56 THRU 58 
                PERFORM CHANGE-MOIS
             MOVE 27 TO EXC-KEY
           END-EVALUATE.

       AVANT-2.
           ACCEPT REG-MATCHCODE
             LINE 3 POSITION 33 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 
           CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
           COMPUTE ARC-IDX = INDICE-ZONE - 2.
           COMPUTE LIN-IDX = ARC-IDX + 6.
           MOVE HE-LP-SUITE(ARC-IDX) TO SUITE-IDX.
      *    ACCEPT SUITE-IDX
           MOVE ">" TO ARROW.
           ACCEPT ARROW
             LINE  LIN-IDX POSITION 9 SIZE 1
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "REVERSE"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           DISPLAY SPACES LINE LIN-IDX POSITION 9 SIZE 1.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE "N" TO A-N LNK-A-N.
           EVALUATE EXC-KEY
           WHEN  1 MOVE  504014 TO LNK-VAL
                   PERFORM HELP-SCREEN
                   MOVE 1 TO INPUT-ERROR
           WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   CALL "2-REGIS" USING LINK-V REG-RECORD
                   PERFORM AFFICHAGE-ECRAN 
                   PERFORM GET-PERS
           WHEN  OTHER PERFORM NEXT-REGIS.
           PERFORM DIS-HE-01.
           IF LNK-COMPETENCE < REG-COMPETENCE
              INITIALIZE REG-REC-DET 
              MOVE "GN" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR = 0 
           AND REG-PERSON > 0 
              PERFORM PRESENCE.
           PERFORM AFFICHAGE-DETAIL.
           IF INPUT-ERROR = 0 
           AND PRES-TOT(LNK-MOIS) > 0
              AND EXC-KEY NOT = 53
              PERFORM TOTAL-LIVRE THRU TOTAL-LIVRE-END.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              IF  ARC-IDX = 0 
              AND REG-PERSON NOT = 0
                 GO APRES-1 
              END-IF.
           
       APRES-2.
           EVALUATE EXC-KEY
           WHEN 65 THRU 66 PERFORM NEXT-REGIS
           END-EVALUATE.                     
           PERFORM DIS-HE-01.
           PERFORM TOTAL-LIVRE THRU TOTAL-LIVRE-END.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              IF  ARC-IDX = 0 
              AND REG-PERSON NOT = 0
                 GO APRES-2
              END-IF.
           
       APRES-DEC.
           INITIALIZE L-RECORD.
           MOVE FR-KEY TO L-FIRME.
           MOVE REG-PERSON TO L-PERSON.
           MOVE LNK-MOIS  TO L-MOIS.
           MOVE HE-LP-SUITE(ARC-IDX) TO L-SUITE LNK-SUITE.
           MOVE HE-LP-ARCHIV(ARC-IDX) TO L-ARCHIVE LNK-NUM.
           MOVE LNK-MOIS TO L-MOIS.
           EVALUATE EXC-KEY 
           WHEN  8 
                 PERFORM DEL-CODPAIE
                 CALL "6-ARCHL" USING LINK-V L-RECORD REG-RECORD DEL-KEY
                 MOVE 2 TO DECISION
           END-EVALUATE.
           IF  EXC-KEY > 1
           AND EXC-KEY < 11
               PERFORM TOTAL-LIVRE THRU TOTAL-LIVRE-END
           END-IF.

       NEXT-REGIS.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 2
              MOVE "A" TO A-N.
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.
           IF REG-PERSON > 0
              PERFORM PRESENCE
              IF EXC-KEY = 65
              OR EXC-KEY = 66
              IF LNK-COMPETENCE < REG-COMPETENCE
                 GO NEXT-REGIS
              END-IF
                 IF PRES-TOT(LNK-MOIS) = 0
                    GO NEXT-REGIS
                 END-IF
              END-IF
           END-IF.
           PERFORM GET-PERS.

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD "N" FAKE-KEY.

       PRESENCE.
           CALL "4-JRPRES" USING LINK-V REG-RECORD PRESENCES.

       ENTREE-SORTIE.
           MOVE "SL" TO LNK-AREA.
           MOVE 2 TO LNK-NUM.
           MOVE 0 TO LNK-PERSON.
           PERFORM DISPLAY-MESSAGE.

      *    HISTORIQUE DES LIVRES
      *    컴컴컴컴컴컴컴컴컴컴컴컴

       TOTAL-LIVRE.
           MOVE 0 TO ARC-IDX.
           MOVE 6 TO LIN-IDX.
           INITIALIZE L-RECORD.
           MOVE LNK-MOIS  TO L-MOIS.
           MOVE 66 TO SAVE-KEY. 
           PERFORM READ-LIVRE THRU READ-LP-END.
           COMPUTE CHOIX-MAX = 2 + ARC-IDX.
       TOTAL-LIVRE-END.
           EXIT.

       READ-LIVRE.
           PERFORM NXLP.
           IF FR-KEY     NOT = L-FIRME
           OR REG-PERSON NOT = L-PERSON
           OR LNK-MOIS   NOT = L-MOIS 
              GO READ-LP-END.
           ADD 1 TO ARC-IDX.
           MOVE L-SUITE TO HE-LP-SUITE(ARC-IDX).
           MOVE L-ARCHIVE TO HE-LP-ARCHIV(ARC-IDX).
           PERFORM DIS-HIS-LIGNE.
           IF LIN-IDX > 22
              GO READ-LP-END.
           GO READ-LIVRE.
       READ-LP-END.
           ADD 1 TO LIN-IDX .
           PERFORM CLEAN-SCREEN VARYING LIN-IDX FROM LIN-IDX
           BY 1 UNTIL LIN-IDX > 24.

       NXLP.
           MOVE LNK-MOIS TO L-MOIS.
           CALL "6-ARCHL" USING LINK-V L-RECORD REG-RECORD SAVE-KEY.

       CLEAN-SCREEN.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.


       DEL-CODPAIE.
           INITIALIZE CSA-RECORD.
           MOVE FR-KEY     TO CSA-FIRME.
           MOVE REG-PERSON TO CSA-PERSON.
           MOVE LNK-MOIS   TO CSA-MOIS.
           MOVE LNK-NUM    TO CSA-ARCHIV.
           PERFORM READ-CODPAIE THRU READ-CODPAIE-END.

       READ-CODPAIE.
           CALL "6-CODARC" USING LINK-V CSA-RECORD REG-RECORD NX-KEY.
           IF FR-KEY     NOT = CSA-FIRME
           OR REG-PERSON NOT = CSA-PERSON
           OR LNK-MOIS   NOT = CSA-MOIS 
           OR LNK-NUM    NOT = CSA-ARCHIV
           OR CSA-SUITE  > 0
              GO READ-CODPAIE-END 
           END-IF.
           CALL "6-CODARC" USING LINK-V CSA-RECORD REG-RECORD DEL-KEY.
           GO READ-CODPAIE.
       READ-CODPAIE-END.

       CHANGE-MOIS.
           MOVE LNK-MOIS TO SAVE-MOIS.
           EVALUATE EXC-KEY
             WHEN 56 PERFORM SUBTRACT-MOIS THRU SUBTRACT-END
             WHEN 58 PERFORM ADD-MOIS THRU ADD-END
           END-EVALUATE.
           PERFORM AFFICHAGE-ECRAN.
           PERFORM AFFICHAGE-DETAIL.
           DISPLAY LNK-MOIS LINE 1 POSITION 74.
           PERFORM AFFICHAGE-DETAIL.

       SUBTRACT-MOIS.
           SUBTRACT 1 FROM LNK-MOIS.
           IF LNK-MOIS = 0 
           OR REG-PERSON = 0
              GO SUBTRACT-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO SUBTRACT-END.
           GO SUBTRACT-MOIS.
       SUBTRACT-END.
           IF LNK-MOIS = 0
              IF REG-PERSON = 0
                 MOVE 1 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.
       ADD-MOIS.
           ADD 1 TO LNK-MOIS.
           IF LNK-MOIS > 12
           OR REG-PERSON = 0
              GO ADD-END.
           IF PRES-TOT(LNK-MOIS) NOT = 0 
              GO ADD-END.
           GO ADD-MOIS.
       ADD-END.
           IF LNK-MOIS > 12
              IF REG-PERSON = 0
                 MOVE 12 TO LNK-MOIS
              ELSE
                 MOVE SAVE-MOIS TO LNK-MOIS
              END-IF
           END-IF.


       DIS-HE-01.
           MOVE REG-PERSON  TO HE-Z6.
           IF REG-PERSON NOT = 0
              MOVE REG-PERSON TO LNK-VAL.
           DISPLAY HE-Z6 
             LINE 3 POSITION 15 SIZE 6
           CALL "0-PRNOM" USING LINK-V PR-RECORD.
           DISPLAY LNK-TEXT LINE 3 POSITION 47 SIZE 33.
       DIS-HE-02.
           DISPLAY REG-MATCHCODE LINE 3 POSITION 33.
       DIS-HE-END.
           EXIT.

       DIS-HIS-LIGNE.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 1 SIZE 80.
           MOVE L-JOUR-DEBUT TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 1 LOW.
           MOVE L-JOUR-FIN TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 4 LOW.

           MOVE L-MOIS TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 6 REVERSE.
           MOVE L-SUITE TO HE-Z2.
           DISPLAY HE-Z2 LINE LIN-IDX POSITION 8 LOW.
           DISPLAY L-ARCHIVE LINE LIN-IDX POSITION 10.


           MOVE L-ST-ANNEE TO HE-AA.
           MOVE L-ST-MOIS  TO HE-MM.
           MOVE L-ST-JOUR  TO HE-JJ.
           DISPLAY HE-DATE LINE  LIN-IDX POSITION 13.
           MOVE  L-IMPOSABLE-BRUT TO HEZ5Z2.
           DISPLAY HEZ5Z2  LINE  LIN-IDX POSITION 21.
           COMPUTE SH-00 = L-COTISATIONS + L-CAISSE-DEP-SAL.
           MOVE SH-00 TO HEZ4Z2.
           DISPLAY HEZ4Z2 LINE LIN-IDX POSITION 29 LOW.
           MOVE L-IMPOT TO HEZ4Z2.
           DISPLAY HEZ4Z2 LINE LIN-IDX POSITION 36.

           MOVE  L-SALAIRE-NET TO HEZ5Z2.
           DISPLAY HEZ5Z2 LINE LIN-IDX POSITION 43.

           MOVE  L-IMPOSABLE-BRUT-NP TO HEZ5Z2.
           DISPLAY HEZ5Z2 LINE LIN-IDX POSITION 51.
           COMPUTE SH-00 = L-COTISATIONS-NP + L-CAISSE-DEP-SAL-NP.
           MOVE SH-00 TO HEZ4Z2.
           DISPLAY HEZ4Z2 LINE LIN-IDX POSITION 59 LOW.
           COMPUTE SH-00 = L-IMPOT-NP - L-DECOMPTE-IMPOT.
           MOVE SH-00 TO HEZ5Z1.
           DISPLAY HEZ5Z1 LINE LIN-IDX POSITION 66.
           MOVE  L-SALAIRE-NET-NP TO HEZ5Z2.
           DISPLAY HEZ5Z2 LINE LIN-IDX POSITION 73.

       AFFICHAGE-ECRAN.
           MOVE 2515 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           MOVE 0 TO LNK-SUITE.
           CANCEL "6-ARCHL".
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".

