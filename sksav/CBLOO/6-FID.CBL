      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-FID LECTURE ARTICLES FACTURATION          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-FID.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FACDINT.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "FACDINT.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       01  INPUT-ERROR PIC 9 VALUE 0.
       01  IDX-1       PIC 9 VALUE 0.
       01  IDX                   PIC 9(4)  VALUE 0.
       01  SH-00  PIC S9(10)V999999 COMP-3.
       01  action      PIC 9.
       01  TODAY.
           02 TODAY-TIME     PIC 9(12).
           02 TODAY-FILLER   PIC 9999.

       01  FID-NAME.
           02 IDENTITE           PIC X(4) VALUE "S-FD".
           02 FIRME-FID          PIC 9999 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".
           COPY "FACDINT.LNK".
           COPY "REGISTRE.REC".
       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD REG-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FID.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-FID.


           IF  FIRME-FID > 0
           AND FIRME-FID NOT = FR-KEY
              CLOSE FID
              MOVE 0 TO FIRME-FID
           END-IF.

           IF FIRME-FID = 0
              MOVE FR-KEY TO FIRME-FID
              OPEN I-O FID
           END-IF.

           MOVE LINK-RECORD TO FID-RECORD.
           MOVE REG-PERSON  TO FID-PERSON.
           MOVE LNK-ANNEE   TO FID-ANNEE.
           IF FID-MOIS = 0
              MOVE LNK-MOIS TO FID-MOIS.
           PERFORM RECHERCHE.

       RECHERCHE.
           EVALUATE EXC-KEY 
           WHEN 65 START FID KEY < FID-KEY INVALID GO EXIT-1
               NOT INVALID
               READ FID PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               END-START
           WHEN 66 START FID KEY > FID-KEY INVALID GO EXIT-1
               NOT INVALID
               READ FID NEXT NO LOCK AT END GO EXIT-1 END-READ
               END-START
           WHEN 0 START FID KEY <= FID-KEY INVALID GO EXIT-1
               NOT INVALID
               READ FID PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               END-START
           WHEN 98 DELETE FID INVALID CONTINUE END-DELETE
           WHEN 99 MOVE 0 TO SH-00
                   PERFORM TEST-0 VARYING IDX FROM 1 BY 1 UNTIL IDX > 6
                      IF SH-00 > 0
                      CALL "0-TODAY" USING TODAY
                      MOVE TODAY-TIME TO FID-TIME
                      MOVE LNK-USER TO FID-USER
                      WRITE FID-RECORD INVALID REWRITE FID-RECORD 
                      END-WRITE
                   ELSE
                      DELETE FID INVALID CONTINUE END-DELETE
                   END-IF
           WHEN OTHER READ FID NO LOCK INVALID GO EXIT-2 END-READ
           END-EVALUATE.
           IF REG-PERSON NOT = FID-PERSON
           OR LNK-ANNEE  NOT = FID-ANNEE
           OR LNK-MOIS   NOT = FID-MOIS
              GO EXIT-1
           END-IF.
           MOVE FID-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           INITIALIZE LINK-REC-DET.
           EXIT PROGRAM.

       TEST-0.
           ADD FID-DONNEE(IDX) TO SH-00.