      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-TPCC MODULE GENERAL LECTURE TP-CCOL       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-TPCC.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "TP-CCOL.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "TP-CCOL.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "TP-CCOL.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING  LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TP-CCOL.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-REG.
       
           IF NOT-OPEN = 0
              OPEN INPUT TP-CCOL
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO TP-CCOL-RECORD.
           MOVE FR-KEY TO TP-CCOL-FIRME.
           MOVE 0 TO LNK-VAL.
           
           EVALUATE EXC-KEY 
           WHEN 0 PERFORM START-0
               READ TP-CCOL PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 65 PERFORM START-1
               READ TP-CCOL PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ TP-CCOL NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ TP-CCOL NO LOCK INVALID 
                      INITIALIZE TP-CCOL-REC-DET END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY NOT = TP-CCOL-FIRME 
              GO EXIT-1
           END-IF.
           MOVE TP-CCOL-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-0.
           INITIALIZE TP-CCOL-RECORD.
           MOVE FR-KEY TO TP-CCOL-FIRME.
           MOVE LNK-ANNEE TO TP-CCOL-ANNEE
           MOVE LNK-MOIS  TO TP-CCOL-MOIS.
           START TP-CCOL KEY <= TP-CCOL-KEY INVALID GO EXIT-1.
       START-1.
           START TP-CCOL KEY < TP-CCOL-KEY INVALID GO EXIT-1.
       START-2.
           START TP-CCOL KEY > TP-CCOL-KEY INVALID GO EXIT-1.


