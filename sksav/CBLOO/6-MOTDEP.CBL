      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-MOTDEP MODULE LECTURE MOTIFS DE DEPART    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-MOTDEP.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MOTDEP.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "MOTDEP.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN  PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".


       01  LINK-RECORD.
           02 LINK-KEY.
              03  LINK-LANGUE      PIC XX.
              03  LINK-CODE        PIC 99.
           02 LINK-KEY-1.
              03  LINK-CODE-1      PIC 99.
              03  LINK-LANGUE-1    PIC XX.

           02 LINK-REC-DET.
              03 LINK-NOM          PIC X(25).
              03 LINK-FILLER       PIC X(130).
                              
       01  EXC-KEY               PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MOTDEP .
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-MOTDEP.
       
           IF NOT-OPEN = 0
              OPEN INPUT MOTDEP
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO MD-RECORD.
           IF MD-LANGUE = SPACES
              MOVE LNK-LANGUAGE TO MD-LANGUE 
           END-IF.
           
           EVALUATE EXC-KEY 
               WHEN 65 PERFORM START-1
               READ MOTDEP PREVIOUS AT END GO EXIT-1 END-READ
               IF MD-LANGUE NOT = LNK-LANGUAGE
                   GO EXIT-1
               END-IF
               GO EXIT-2
               WHEN 66 PERFORM START-2
               READ MOTDEP NEXT AT END GO EXIT-1 END-READ
               IF MD-LANGUE NOT = LNK-LANGUAGE
                  GO EXIT-1
               END-IF
               GO EXIT-2
               WHEN 4 GO EXIT-2
               WHEN OTHER GO EXIT-3
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE MD-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       EXIT-3.
           READ MOTDEP INVALID GO EXIT-1
                 NOT   INVALID GO EXIT-2.

       START-1.
           START MOTDEP KEY < MD-KEY INVALID GO EXIT-1.
       START-2.
           START MOTDEP KEY > MD-KEY INVALID GO EXIT-1.

