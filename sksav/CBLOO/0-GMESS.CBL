      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-GMESS  RECHERCHE DES MESSAGES             �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.  0-GMESS.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "MESSAGE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "MESSAGE.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  NOT-FOUND   PIC 9.
       01  NOT-OPEN    PIC 9 VALUE 0.
       01  ACTION      PIC X.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON MESSAGES.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-GMESS.
           MOVE LNK-NUM      TO MS-NUMBER. 
           MOVE LNK-LANGUAGE TO MS-LANGUAGE.
           MOVE LNK-AREA     TO MS-AREA.
           MOVE 0 TO NOT-FOUND.
           IF NOT-OPEN = 0
              OPEN INPUT MESSAGES
              MOVE 1 TO NOT-OPEN.

           READ MESSAGES INVALID 
                MOVE 1 TO NOT-FOUND
                MOVE "F" TO MS-LANGUAGE.
           IF NOT-FOUND = 1
              READ MESSAGES INVALID INITIALIZE MS-DESCRIPTION.
           MOVE MS-DESCRIPTION TO LNK-TEXT.
           MOVE 0 TO LNK-NUM.
           EXIT PROGRAM.

      