      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-OCCTRA GESTION DES TEXTES OCCUPATIONS     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-OCCTRA.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "OCCUP.FC".
           COPY "OCCTEXT.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "OCCUP.FDE".
           COPY "OCCTEXT.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX             PIC 99 VALUE 3.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       SCREEN SECTION.


       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON OCCUPATION OCCTEXT.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME.
       
           IF LNK-LANGUAGE = "F" EXIT PROGRAM.
           MOVE LNK-LANGUAGE TO OT-LANGUE.
           OPEN INPUT OCCUPATION.
           OPEN I-O OCCTEXT.

           INITIALIZE OCC-RECORD OT-RECORD.
           MOVE LNK-LANGUAGE TO OT-LANGUE.
           PERFORM AFFICHAGE-ECRAN .
           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

           EVALUATE INDICE-ZONE
               WHEN 1 MOVE 0000002200 TO EXC-KFR(1)
                      MOVE 0023000000 TO EXC-KFR(2)
                      MOVE 0000000065 TO EXC-KFR(13)
                      MOVE 6600000000 TO EXC-KFR(14)
               WHEN 2 MOVE 0000000005 TO EXC-KFR(1)
                      MOVE 0023000000 TO EXC-KFR(2)
           WHEN CHOIX-MAX MOVE 0000000005 TO EXC-KFR(1)
                          MOVE 0000080000 TO EXC-KFR(2)
                          MOVE 0052000000 TO EXC-KFR(11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.
           EVALUATE INDICE-ZONE
           WHEN 1 PERFORM AVANT-1
           WHEN 2 PERFORM AVANT-DESCR
           WHEN CHOIX-MAX PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           IF EXC-KEY = 53 MOVE 13 TO EXC-KEY.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1
           WHEN  2 PERFORM APRES-2
           WHEN CHOIX-MAX PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 52 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.
           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT OCC-KEY
             LINE  4 POSITION 20 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-DESCR.        
           ACCEPT OT-NOM
             LINE  5 POSITION 50 SIZE 25
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           EVALUATE EXC-KEY 
                WHEN 65 PERFORM PREV-OCCUP
                       PERFORM READ-OCCTEXT
                WHEN 66 PERFORM NEXT-OCCUP
                       PERFORM READ-OCCTEXT
                WHEN 4 PERFORM COPY-OCCTEXT
                WHEN 7 PERFORM SEARCH-BEGIN THRU SEARCH-END
                WHEN OTHER PERFORM READ-OCCUP.
           PERFORM AFFICHAGE-DETAIL.
       
       APRES-2.
           EVALUATE EXC-KEY 
            WHEN 5 WRITE OT-RECORD INVALID REWRITE OT-RECORD END-WRITE
                   PERFORM SEARCH-BEGIN THRU SEARCH-END
            WHEN 7 PERFORM SEARCH-BEGIN THRU SEARCH-END.
           PERFORM AFFICHAGE-DETAIL.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN 5 WRITE OT-RECORD INVALID REWRITE OT-RECORD END-WRITE
                   MOVE 2 TO DECISION
                   PERFORM AFFICHAGE-DETAIL
            WHEN  8 DELETE OCCTEXT INVALID CONTINUE
                    END-DELETE
                    INITIALIZE OT-RECORD
                    MOVE 1 TO DECISION
                    PERFORM AFFICHAGE-DETAIL
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
               MOVE CHOIX-MAX TO INDICE-ZONE.

       READ-OCCUP.
           READ OCCUPATION INVALID 
              INITIALIZE OCC-REC-DET 
           END-READ.
           PERFORM READ-OCCTEXT.

       READ-OCCTEXT.
           MOVE 0 TO NOT-FOUND.
           MOVE OCC-KEY TO OT-NUMBER.
           READ OCCTEXT INVALID 
                INITIALIZE OT-REC-DET
                MOVE 1 TO NOT-FOUND
           END-READ.

       COPY-OCCTEXT.  
           MOVE OCC-KEY TO IDX-2.
           ACCEPT OCC-KEY
             LINE  4 POSITION 60 SIZE 2
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           PERFORM READ-OCCTEXT.
           MOVE IDX-2 TO OCC-KEY OT-NUMBER.

       PREV-OCCUP.
           START OCCUPATION KEY < OCC-KEY INVALID
                INITIALIZE OCC-RECORD
                NOT INVALID
           READ OCCUPATION PREVIOUS AT END 
                INITIALIZE OCC-RECORD
                MOVE 1 TO INDICE-ZONE
                END-READ.

       NEXT-OCCUP.
           START OCCUPATION KEY > OCC-KEY INVALID
                INITIALIZE OCC-RECORD
                NOT INVALID
           READ OCCUPATION NEXT AT END 
                INITIALIZE OCC-RECORD
                MOVE 1 TO INDICE-ZONE
                END-READ.
       
       SEARCH-BEGIN.
           START OCCUPATION KEY > OCC-KEY INVALID 
               GO SEARCH-END
               NOT INVALID
           READ OCCUPATION NEXT AT END
               GO SEARCH-END
           END-READ.
           PERFORM READ-OCCTEXT.
           IF NOT-FOUND = 1
              INITIALIZE OT-REC-DET
              MOVE 1 TO INPUT-ERROR
              GO SEARCH-END
           END-IF.
           GO SEARCH-BEGIN.
       SEARCH-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 35 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           
       AFFICHAGE-DETAIL.
           DISPLAY OCC-KEY LINE  4 COL 20.
           DISPLAY OCC-NOM LINE  5 COL 20 LOW.
           DISPLAY OT-NOM LINE  5 COL 50.

       END-PROGRAM.
           CLOSE OCCUPATION.
           CLOSE OCCTEXT.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           
      