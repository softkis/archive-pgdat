      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-IMPTAB TABLEAU DES IMPRIMANTES            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-IMPTAB.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "IMPRTAB.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "IMPRTAB.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX         PIC 99 VALUE 20.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON IMPTAB.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-0-IMPTAB.
       
           OPEN INPUT IMPTAB.

           PERFORM AFFICHAGE-ECRAN .
           READ IMPTAB AT END INITIALIZE IMPTAB-RECORD.
           PERFORM DIS-E1.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0100000005 TO EXC-KFR (1).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           PERFORM AVANT-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN  9 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TO TRAITEMENT-ECRAN.
           
           EVALUATE EXC-KEY
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF
                WHEN 53 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.
           IF INDICE-ZONE > CHOIX-MAX
                 MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-ALL.
           MOVE 1 TO IDX-1.
           PERFORM AVANT-ALL-1 THRU AVANT-ALL-END.
           
       AVANT-ALL-1.
           IF INDICE-ZONE =  0 MOVE  1 TO INDICE-ZONE.
           IF INDICE-ZONE > 20 MOVE 20 TO INDICE-ZONE.
           IF IDX-1 > 3 MOVE 3 TO IDX-1.
           PERFORM ACCEPT-PARAM.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TO AVANT-ALL-1.
           IF EXC-KEY = 52 AND  INDICE-ZONE > 1
                SUBTRACT 1 FROM INDICE-ZONE.
           IF EXC-KEY = 53 ADD 1 TO INDICE-ZONE.
           EVALUATE EXC-KEY
                WHEN  1 MOVE 200 TO LNK-VAL
                        PERFORM HELP-SCREEN
                        MOVE 1 TO INPUT-ERROR
                WHEN  5 GO WRITE-TAB
                WHEN 13 ADD  1 TO IDX-1
                WHEN 27 SUBTRACT 1 FROM IDX-1
           END-EVALUATE.
      *     IF IDX-1 >  0 AND IDX-1 < 4 
               GO TO AVANT-ALL-1.
       AVANT-ALL-END.
           EXIT.

       ACCEPT-PARAM.
           COMPUTE LIN-IDX = INDICE-ZONE + 2.
           EVALUATE IDX-1
                WHEN 1 PERFORM ACCEPT-SYSTEM
                WHEN 2 PERFORM ACCEPT-TYPE
                WHEN 3 PERFORM ACCEPT-NAME
           END-EVALUATE.

       ACCEPT-SYSTEM.
           ACCEPT IMPTAB-SYSTEM(INDICE-ZONE)
             LINE LIN-IDX POSITION 5 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
       ACCEPT-TYPE.
           ACCEPT IMPTAB-TYPE(INDICE-ZONE)
             LINE LIN-IDX POSITION 20 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
           ON EXCEPTION  EXC-KEY  CONTINUE.
       ACCEPT-NAME.
           ACCEPT IMPTAB-NAME(INDICE-ZONE)
             LINE LIN-IDX POSITION 35 SIZE 30
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       DIS-E1.
           PERFORM DISPLAY-DETAIL VARYING IDX FROM 1 BY 1
           UNTIL IDX > 20.

       DISPLAY-DETAIL.
           COMPUTE LIN-IDX = IDX + 2.
           DISPLAY IMPTAB-SYSTEM(IDX) LINE LIN-IDX POSITION 5.
           DISPLAY IMPTAB-TYPE(IDX)   LINE LIN-IDX POSITION 20.
           DISPLAY IMPTAB-NAME(IDX)   LINE LIN-IDX POSITION 35.
      
       
       AFFICHAGE-ECRAN.
           MOVE 60 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-E1.

       WRITE-TAB.
           CLOSE IMPTAB.
           OPEN  OUTPUT IMPTAB.
           WRITE IMPTAB-RECORD 
           GO END-PROGRAM.

       END-PROGRAM.
           CLOSE IMPTAB.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
           
