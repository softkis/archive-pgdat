      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-OCCUP MODULE GENERAL LECTURE OCCUPATION   �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-OCCUP.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "OCCUP.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "OCCUP.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "OCCUP.LNK".

       01  EXC-KEY           PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON OCCUPATION.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-OCCUP.
       
           IF NOT-OPEN = 0
              OPEN INPUT OCCUPATION
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO OCC-RECORD.
           MOVE 0 TO LNK-VAL.
           EVALUATE EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN  9 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ OCCUPATION PREVIOUS NO LOCK AT END GO EXIT-1 
               END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ OCCUPATION NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ OCCUPATION NO LOCK INVALID INITIALIZE 
               OCC-REC-DET END-READ
               GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           MOVE OCC-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START OCCUPATION KEY < OCC-KEY INVALID GO EXIT-1.
       START-2.
           START OCCUPATION KEY > OCC-KEY INVALID GO EXIT-1.


               