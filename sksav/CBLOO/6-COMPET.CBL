      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-COMPET MODULE GENERAL COMPETENCES         �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-COMPET.
      
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "COMPETE.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "COMPETE.FDE".

       WORKING-STORAGE SECTION.

       01  NOT-OPEN PIC 9 VALUE 0.
       01  IDX-1    PIC 9 VALUE 0.
       01  IDX-2    PIC 9 VALUE 0.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "COMPETE.LNK".
           COPY "REGISTRE.REC".

       01  EXC-KEY  PIC 9(4) COMP-1.

       PROCEDURE DIVISION USING LINK-V REG-RECORD LINK-RECORD EXC-KEY.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON COMPETE.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-REG.
       
           IF NOT-OPEN = 0
              OPEN INPUT COMPETE
              MOVE 1 TO NOT-OPEN.

           MOVE LINK-RECORD TO COM-RECORD.
           MOVE FR-KEY TO COM-FIRME.
           MOVE REG-PERSON TO COM-PERSON.
           MOVE LNK-NUM TO COM-TYPE.
           EVALUATE EXC-KEY 
           WHEN 65 PERFORM START-1
               READ COMPETE PREVIOUS NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 66 PERFORM START-2
               READ COMPETE NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN 0 PERFORM START-3
               READ COMPETE NEXT NO LOCK AT END GO EXIT-1 END-READ
               GO EXIT-2
           WHEN OTHER READ COMPETE NO LOCK INVALID CONTINUE END-READ
              GO EXIT-2
           END-EVALUATE.

       EXIT-1.
           INITIALIZE LINK-RECORD.
           EXIT PROGRAM.

       EXIT-2.
           IF FR-KEY     NOT = COM-FIRME 
           OR REG-PERSON NOT = COM-PERSON
           OR LNK-NUM    NOT = COM-TYPE
              GO EXIT-1
           END-IF.
           MOVE COM-RECORD TO LINK-RECORD.
           EXIT PROGRAM.

       START-1.
           START COMPETE KEY < COM-KEY INVALID GO EXIT-1.
       START-2.
           START COMPETE KEY > COM-KEY INVALID GO EXIT-1.
       START-3.
           START COMPETE KEY >= COM-KEY INVALID GO EXIT-1.

