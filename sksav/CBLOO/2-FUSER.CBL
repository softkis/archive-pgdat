      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 2-FUSER PERMISSIONS COMPETENCES FIRMES      �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    2-FUSER.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".
           COPY "FIRME.REC".
           COPY "FUSER.REC".

       01  CHOIX-MAX             PIC 99 VALUE 1.
       01  SAVE-FIRME            PIC 9999.

       01  CHOIX                 PIC 9(4).
       01  CHOIX-A               PIC X(10).
       01  CHOIX-A1 REDEFINES CHOIX-A.
           02 CHOIX-1            PIC 9.
           02 CHOIX-F1           PIC X(9).
       01  CHOIX-A2 REDEFINES CHOIX-A.
           02 CHOIX-2            PIC 9(2).
           02 CHOIX-F2           PIC X(8).
       01  CHOIX-A3 REDEFINES CHOIX-A.
           02 CHOIX-3            PIC 9(3).
           02 CHOIX-F3           PIC X(7).
       01  CHOIX-A4 REDEFINES CHOIX-A.
           02 CHOIX-4            PIC 9(4).
           02 CHOIX-F4           PIC X(6).
       01  COMPTEUR              PIC 99.
       01  PRECISION             PIC 9.

       01 HE-REG.
          02 H-R PIC Z(4) OCCURS 20.

       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-TEMP.
              03 HE-JOUR  PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-MOIS  PIC ZZ.
              03 FILLER   PIC X VALUE ".".
              03 HE-ANNEE PIC ZZZZ.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       START-DISPLAY SECTION.
             
       START-2-FUSER.

           MOVE LNK-VAL TO PRECISION.
           MOVE LNK-A-N TO A-N.
           PERFORM AFFICHAGE-ECRAN.

           PERFORM AFFICHE-DEBUT THRU AFFICHE-END.

      *    DETAIL AFFICHAGE 
      *    컴컴컴컴컴컴컴컴
 
       AFFICHE-DEBUT.
           INITIALIZE FIRME-RECORD IDX-1 EXC-KEY COMPTEUR CHOIX.
           MOVE 4 TO LIN-IDX.
           PERFORM READ-FR THRU READ-FR-END.
           IF EXC-KEY = 66 GO AFFICHE-DEBUT.
           
       AFFICHE-END.
           PERFORM END-PROGRAM.

       READ-FR.
           MOVE 66 TO EXC-KEY.
           CALL "6-FIRMES" USING LINK-V FIRME-RECORD A-N EXC-KEY.
           IF FIRME-KEY < 1
              PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 66
                 INITIALIZE HE-REG IDX-1
                 GO READ-FR
              END-IF
              GO READ-FR-END.
           PERFORM DIS-DET-LIGNE.
           IF LIN-IDX > 21
              PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 66 
                 INITIALIZE HE-REG IDX-1
                 GO READ-FR
              END-IF
              IF CHOIX NOT = 0
                 GO READ-FR-END
              END-IF
           END-IF.
           GO READ-FR.
       READ-FR-END.
           IF CHOIX = 0
              MOVE 5 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              IF EXC-KEY NOT = 82 AND NOT = 68
                 ADD 1 TO LIN-IDX      
                 PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
                 PERFORM INTERRUPT THRU INTERRUPT-END
              END-IF
           END-IF.

      *    컴컴컴컴컴컴컴컴컴컴컴컴

       DIS-DET-LIGNE.
           ADD 1 TO LIN-IDX COMPTEUR IDX-1.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 80.
           MOVE FIRME-KEY TO H-R(IDX-1) FUS-FIRME.
           DISPLAY H-R(IDX-1) LINE LIN-IDX POSITION 2.
           DISPLAY FIRME-NOM LINE LIN-IDX POSITION 7 SIZE 20.
           CALL "6-FUSER" USING LINK-V FUS-RECORD FAKE-KEY.
           IF FUS-FIRME NOT = 0
              DISPLAY FUS-YN LINE LIN-IDX POSITION 50
              DISPLAY FUS-COMPETENCE LINE LIN-IDX POSITION 60
           END-IF.

       CLEAN-SCREEN.
           ADD 1 TO LIN-IDX.
           DISPLAY SPACES LINE LIN-IDX POSITION 2 SIZE 80.

       INTERRUPT.

           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000900 TO EXC-KFR (2).
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0000530000 TO EXC-KFR (11).
           MOVE 0067000000 TO EXC-KFR (14).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

       INTERRUPT-01.
           INITIALIZE CHOIX CHOIX-A.
           IF A-N = "N"
              ACCEPT CHOIX 
              LINE 3 POSITION 30 SIZE 4
              TAB UPDATE NO BEEP CURSOR  1
              ON EXCEPTION EXC-KEY CONTINUE
           ELSE
              ACCEPT CHOIX-A
              LINE 3 POSITION 30 SIZE 10
              TAB UPDATE NO BEEP CURSOR  1
              CONTROL "UPPER"
              ON EXCEPTION EXC-KEY CONTINUE.
              MOVE 0 TO IDX-1.
           IF CHOIX-1 NUMERIC
              INSPECT CHOIX-A TALLYING IDX-1 FOR CHARACTERS BEFORE " "
              EVALUATE IDX-1
                 WHEN 1 MOVE CHOIX-1 TO CHOIX
                 WHEN 2 MOVE CHOIX-2 TO CHOIX
                 WHEN 3 MOVE CHOIX-3 TO CHOIX
                 WHEN 4 MOVE CHOIX-4 TO CHOIX
              END-EVALUATE
              IF CHOIX NOT = 0
                 PERFORM END-PROGRAM
              END-IF
           END-IF.
           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY = 67 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO INTERRUPT-01.
           IF EXC-KEY = 98 GO INTERRUPT-01.
           IF EXC-KEY = 53
              MOVE 1 TO IDX-1
              PERFORM AVANT-ALL THRU AVANT-ALL-END
              GO INTERRUPT.
           IF A-N = "N"
              IF CHOIX NOT = 0
                 PERFORM END-PROGRAM
              END-IF
           ELSE
              IF CHOIX-A > SPACES
                 INITIALIZE FIRME-RECORD
                 MOVE CHOIX-A TO FIRME-MATCHCODE
                 MOVE 66 TO EXC-KEY
              END-IF
           END-IF.
           INITIALIZE CHOIX.
       INTERRUPT-END.
           MOVE 4 TO LIN-IDX.
           MOVE 0 TO IDX-1.

       AFFICHAGE-ECRAN.
           MOVE 2102 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       END-PROGRAM.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       AVANT-ALL.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000000012 TO EXC-KFR(1).
           MOVE 0000080000 TO EXC-KFR(2).
           MOVE 0000130000 TO EXC-KFR(3).
           MOVE 0000000065 TO EXC-KFR(13).
           MOVE 6600000000 TO EXC-KFR(14).
           MOVE 0052530000 TO EXC-KFR(11).

      * param둻res sp괹ifiques touches de fonctions

           PERFORM DISPLAY-F-KEYS.

           COMPUTE LIN-IDX = IDX-1 + 4.
           MOVE H-R(IDX-1) TO HE-Z4.
           ACCEPT HE-Z4
             LINE  LIN-IDX POSITION 2 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "REVERSE"
             ON EXCEPTION EXC-KEY CONTINUE.
           IF EXC-KEY = 82
              EXIT PROGRAM.
           MOVE H-R(IDX-1) TO HE-Z4.
           DISPLAY HE-Z4 LINE LIN-IDX POSITION 2.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL.

           MOVE H-R(IDX-1) TO FUS-FIRME.
           EVALUATE EXC-KEY
                WHEN 5 PERFORM YN
                WHEN 8 CALL "6-FUSER" USING LINK-V FUS-RECORD DEL-KEY
                       DISPLAY FUS-YN LINE LIN-IDX POSITION 50
                       DISPLAY " " LINE LIN-IDX POSITION 60
                WHEN 52 SUBTRACT 1 FROM IDX-1
                WHEN 53 ADD 1 TO IDX-1
                WHEN 65 THRU 66 PERFORM COMPS
                WHEN OTHER
                     MOVE H-R(IDX-1) TO CHOIX
                     PERFORM END-PROGRAM
           END-EVALUATE.
           IF IDX-1 = 0
              GO AVANT-ALL-END.
           IF H-R(IDX-1) = SPACES
              SUBTRACT 1 FROM IDX-1
           END-IF.
           GO AVANT-ALL.

       AVANT-ALL-END.
           EXIT.

       COMPS.
           CALL "6-FUSER" USING LINK-V FUS-RECORD SAVE-KEY.
           EVALUATE EXC-KEY
              WHEN 65 SUBTRACT 1 FROM FUS-COMPETENCE
              WHEN 66 ADD      1 TO   FUS-COMPETENCE
           END-EVALUATE.
           MOVE "Y" TO FUS-YN.
           CALL "6-FUSER" USING LINK-V FUS-RECORD WR-KEY.
           DISPLAY FUS-YN LINE LIN-IDX POSITION 50.
           DISPLAY FUS-COMPETENCE LINE LIN-IDX POSITION 60.

       YN.
           CALL "6-FUSER" USING LINK-V FUS-RECORD SAVE-KEY.
           EVALUATE FUS-YN 
              WHEN SPACES MOVE "Y" TO FUS-YN
                          MOVE 9 TO FUS-COMPETENCE
              WHEN "Y"    MOVE "N" TO FUS-YN
                          MOVE 0 TO FUS-COMPETENCE
              WHEN "N"    MOVE "Y" TO FUS-YN
                          MOVE 9 TO FUS-COMPETENCE
           END-EVALUATE.
           CALL "6-FUSER" USING LINK-V FUS-RECORD WR-KEY.
           DISPLAY FUS-YN LINE LIN-IDX POSITION 50.
           DISPLAY FUS-COMPETENCE LINE LIN-IDX POSITION 60.
                       