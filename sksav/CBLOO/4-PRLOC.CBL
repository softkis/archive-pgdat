      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-PRLOC STRING LOCALITE PERSONNE            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-PRLOC.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

       01  POINT-IDX      PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "PERSON.REC".

       PROCEDURE DIVISION USING LINK-V PR-RECORD.

       START-DISPLAY SECTION.
             
       START-4-PRLOC.
           INITIALIZE LNK-TEXT.
           MOVE 1 TO POINT-IDX.
           STRING PR-PAYS DELIMITED BY " "  INTO LNK-TEXT 
           WITH POINTER POINT-IDX.
           ADD 1 TO POINT-IDX.
           IF PR-PAYS = "D"
           OR PR-PAYS = "F"
              STRING PR-CP5 DELIMITED BY SIZE INTO LNK-TEXT 
              WITH POINTER POINT-IDX
           ELSE
              STRING PR-CP4 DELIMITED BY SIZE INTO LNK-TEXT 
              WITH POINTER POINT-IDX
           END-IF.

           ADD 1 TO POINT-IDX.
           STRING PR-LOCALITE DELIMITED BY SIZE INTO LNK-TEXT 
           WITH POINTER POINT-IDX.
           EXIT PROGRAM.


