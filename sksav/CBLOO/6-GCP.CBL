      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 6-GCP RECHERCHE CODES PARAMETRES            �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    6-GCP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.
           COPY "CODPAR.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "CODPAR.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�

       01  NOT-OPEN     PIC 9 VALUE 0.
       01  ACTION       PIC X.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "CODPAR.LNK".

       PROCEDURE DIVISION USING LINK-V LINK-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON CODPAR.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-6-GCP.
       
           IF NOT-OPEN = 0
              OPEN INPUT CODPAR
              INITIALIZE CP-RECORD 
              MOVE 1 TO NOT-OPEN.

           MOVE LNK-NUM TO CP-CCOL.
           MOVE FR-KEY  TO CP-FIRME.
           PERFORM READ-CP.
           MOVE 0 TO CP-CCOL.
           PERFORM READ-CP.
           MOVE 0 TO CP-FIRME.
           PERFORM READ-CP.
           GO END-PROGRAM.

        READ-CP.
           READ CODPAR INVALID CONTINUE
           NOT INVALID GO END-PROGRAM.

       END-PROGRAM.
           MOVE CP-RECORD TO LINK-RECORD.
           MOVE 0 TO LNK-NUM.
           EXIT PROGRAM.

           