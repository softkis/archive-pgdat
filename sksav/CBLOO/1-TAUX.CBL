      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-TAUX TAUX OFFICIELS                       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-TAUX.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.

       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "TAUX.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "TAUX.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  CHOIX-MAX         PIC 99 VALUE 7.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  ECR-DISPLAY.
           02 HE-Z4 PIC Z(4).
           02 HE-Z2 PIC Z(2).
           02 HE-Z5Z4 PIC ZZZZZ,ZZZZ BLANK WHEN ZERO.
           02 HE-Z2Z2 PIC ZZ,ZZ.
           02 HE-Z6 PIC Z(6).

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON TAUX.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.
      
       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-TAUX.
       
           OPEN I-O   TAUX.

           PERFORM AFFICHAGE-ECRAN.
           PERFORM TAUX-RECENT.
           PERFORM AFFICHAGE-DETAIL.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions
           EVALUATE INDICE-ZONE
               WHEN 1     MOVE 0000000065 TO EXC-KFR (13)
                          MOVE 6600000000 TO EXC-KFR (14)
               WHEN CHOIX-MAX
                          MOVE 0000000005 TO EXC-KFR (1)
                          MOVE 0000080000 TO EXC-KFR (2)
                          MOVE 0052000000 TO EXC-KFR (11)
           END-EVALUATE.

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-1-1 
               WHEN  2 PERFORM AVANT-1-2 
               WHEN  3 PERFORM AVANT-1-3 
               WHEN  4 PERFORM AVANT-1-4 
               WHEN  5 PERFORM AVANT-1-5 
               WHEN  6 PERFORM AVANT-1-6 
               WHEN CHOIX-MAX PERFORM AVANT-DEC
           END-EVALUATE.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 98 GO TRAITEMENT-ECRAN-01.
           EVALUATE INDICE-ZONE
               WHEN  1 PERFORM APRES-1-1 
               WHEN  2 PERFORM APRES-1-2 
               WHEN  3 PERFORM APRES-1-3 
               WHEN  4 PERFORM APRES-1-4 
               WHEN  5 PERFORM APRES-1-5 
               WHEN  6 PERFORM APRES-1-6 
               WHEN CHOIX-MAX PERFORM APRES-DEC
            END-EVALUATE.
            PERFORM ECRAN-1.

           IF INPUT-ERROR NOT = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 52 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 12 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 ADD 1 TO INDICE-ZONE
                WHEN 53 ADD 1 TO INDICE-ZONE.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.
           ACCEPT TAUX-ANNEE
           LINE 3 POSITION 28 SIZE 4
           TAB UPDATE NO BEEP CURSOR  1 HIGH
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.
           ACCEPT TAUX-MOIS  
           LINE 4 POSITION 30 SIZE 2
           TAB UPDATE NO BEEP CURSOR  1 HIGH
           ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.
           MOVE FRANCHISE-HS TO HE-Z5Z4.
           ACCEPT HE-Z5Z4
           LINE 6 POSITION 28 SIZE 10
           TAB UPDATE NO BEEP CURSOR  1 HIGH
           ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z5Z4 REPLACING ALL "." BY ",".
           MOVE HE-Z5Z4 TO FRANCHISE-HS.

       AVANT-1-4.
           MOVE CONGE-ENTREPRENEUR TO HE-Z2Z2.
           ACCEPT HE-Z2Z2
           LINE 8 POSITION 30 SIZE 5
           TAB UPDATE NO BEEP CURSOR  1 HIGH
           ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z2Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z2Z2 TO CONGE-ENTREPRENEUR.

       AVANT-1-5.
           MOVE PRIME-BATIMENT TO HE-Z2Z2.
           ACCEPT HE-Z2Z2
           LINE 9 POSITION 30 SIZE 5
           TAB UPDATE NO BEEP CURSOR  1 HIGH
           ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z2Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z2Z2 TO PRIME-BATIMENT.

       AVANT-1-6.
           MOVE CONGE-PARTIEL TO HE-Z2Z2.
           ACCEPT HE-Z2Z2
           LINE 10 POSITION 30 SIZE 5
           TAB UPDATE NO BEEP CURSOR  1 HIGH
           ON EXCEPTION  EXC-KEY  CONTINUE.
           INSPECT HE-Z2Z2 REPLACING ALL "." BY ",".
           MOVE HE-Z2Z2 TO CONGE-PARTIEL.


      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

           
       APRES-1-1.
           EVALUATE EXC-KEY
           WHEN  65 PERFORM TAUX-PRECEDENT
           WHEN  66 PERFORM TAUX-SUIVANT
           WHEN OTHER READ TAUX INVALID CONTINUE END-READ.
           IF TAUX-ANNEE < 2000
              MOVE 2000 TO TAUX-ANNEE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-2.
           IF TAUX-MOIS < 1
              MOVE 1 TO TAUX-MOIS
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF TAUX-MOIS > 12
              MOVE 12 TO TAUX-MOIS
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF INPUT-ERROR = 0
              READ TAUX INVALID 
              MOVE "  " TO LNK-AREA
              MOVE 13 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              END-READ.
           PERFORM AFFICHAGE-DETAIL.

       APRES-1-3.
           PERFORM DIS-E1-3.
       APRES-1-4.
           PERFORM DIS-E1-4.
       APRES-1-5.
           PERFORM DIS-E1-5.
       APRES-1-6.
           PERFORM DIS-E1-6.

       APRES-DEC.
           EVALUATE EXC-KEY 
            WHEN  5 CALL "0-TODAY" USING TODAY
                    MOVE TODAY-TIME TO TAUX-TIME
                    MOVE LNK-USER TO TAUX-USER
                    WRITE TAUX-RECORD INVALID REWRITE TAUX-RECORD
                    END-WRITE   
                    MOVE 1 TO DECISION
            WHEN 8  DELETE TAUX INVALID CONTINUE END-DELETE
                    PERFORM TAUX-PRECEDENT
                    MOVE 1 TO DECISION
            WHEN 52 COMPUTE DECISION = CHOIX-MAX + 1
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       TAUX-RECENT.
           INITIALIZE TAUX-RECORD NOT-FOUND.
           MOVE 9999 TO TAUX-ANNEE
           PERFORM TAUX-PRECEDENT.

       TAUX-SUIVANT.
           MOVE 0 TO NOT-FOUND .
           START TAUX KEY > TAUX-KEY INVALID KEY
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ TAUX NEXT AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.

       TAUX-PRECEDENT.
           MOVE 0 TO NOT-FOUND.
           START TAUX KEY < TAUX-KEY INVALID KEY
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ TAUX PREVIOUS AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.

       DIS-E1-1.
           DISPLAY TAUX-ANNEE LINE 3 POSITION 28.

       DIS-E1-2.
           MOVE TAUX-MOIS TO HE-Z2.
           DISPLAY HE-Z2 LINE 4 POSITION 30.

       DIS-E1-3.
           MOVE FRANCHISE-HS TO HE-Z5Z4.
           DISPLAY HE-Z5Z4 LINE 6 POSITION 28.
       DIS-E1-4.
           MOVE CONGE-ENTREPRENEUR TO HE-Z2Z2.
           DISPLAY HE-Z2Z2 LINE 8 POSITION 30.
       DIS-E1-5.
           MOVE PRIME-BATIMENT TO HE-Z2Z2.
           DISPLAY HE-Z2Z2 LINE 9 POSITION 30.

       DIS-E1-6.
           MOVE CONGE-PARTIEL TO HE-Z2Z2.
           DISPLAY HE-Z2Z2 LINE 10 POSITION 30.

       AFFICHAGE-ECRAN.
           MOVE 10 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM ECRAN-1 VARYING IDX-1 FROM
               1 BY 1 UNTIL IDX-1 = CHOIX-MAX.

       ECRAN-1.
           EVALUATE IDX-1
               WHEN  1 PERFORM DIS-E1-1 
               WHEN  2 PERFORM DIS-E1-2 
               WHEN  3 PERFORM DIS-E1-3 
               WHEN  4 PERFORM DIS-E1-4 
               WHEN  5 PERFORM DIS-E1-5 
               WHEN  6 PERFORM DIS-E1-6 
           END-EVALUATE.

       END-PROGRAM.
           CLOSE TAUX.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
