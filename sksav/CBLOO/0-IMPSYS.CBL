      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 0-IMPSYS PARAMETRES IMPRIMANTES SYSTEMES    �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-IMPSYS.

      
       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "IMPRSYS.FC".

       DATA DIVISION.

       FILE SECTION.
      *컴컴컴컴컴컴

           COPY "IMPRSYS.FDE".

       WORKING-STORAGE SECTION.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

       01  ECRAN-SUITE.
           02 ECR-S1             PIC 999 VALUE 61.
           02 ECR-S2             PIC 999 VALUE 62.
       01  ECRAN-SUITE-R REDEFINES ECRAN-SUITE.
           02 ECR-S              PIC 999 OCCURS 2.
             
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "V-VAR.CPY".

       01  CHOIX-MAX-V.
           02  CHOIX-MAX-1       PIC 99 VALUE  4. 
           02  CHOIX-MAX-2       PIC 99 VALUE 21.
       01  CHOIX-MAX-R REDEFINES CHOIX-MAX-V.
           02  CHOIX-MAX         PIC 99 OCCURS 2. 

       01  INTER-BIN             PIC 9(4) USAGE IS BINARY VALUE 0.
       01  INTER-MED REDEFINES INTER-BIN.
           02  INTER-MED1        PIC X.
           02  INTER-MED2        PIC X.

       01  COPIE-NAME          PIC X(10).
       01  SAVE-NAME           PIC X(10).

       01   ECR-DISPLAY.
            02 HE-Z2 PIC ZZ.
            02 HEZ3 PIC ZZZ.
            02 HE93 PIC 999.

       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON IMPSYS.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-0-IMPSYS.

           OPEN I-O IMPSYS.

           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.


      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  �       creation window                                 �      
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       TRAITEMENT-ECRAN.
      *----------------- 


           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.


      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052535400 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions


           IF ECRAN-IDX = 1
               MOVE 0000680000 TO EXC-KFR (14) 
               EVALUATE INDICE-ZONE
               WHEN 1     MOVE 0000000022 TO EXC-KFR (1)
                          MOVE 0000080000 TO EXC-KFR (2)
                          MOVE 0000000065 TO EXC-KFR (13)
                          MOVE 6600000000 TO EXC-KFR (14)
               WHEN 2     MOVE 0000000000 TO EXC-KFR (1)
               WHEN 4     MOVE 0100000005 TO EXC-KFR (1)
                          MOVE 0000080000 TO EXC-KFR (2)
                          MOVE 0000000065 TO EXC-KFR (13)
                          MOVE 0052000000 TO EXC-KFR (11)
           END-IF.        

           IF ECRAN-IDX = 2
               MOVE 0067000000 TO EXC-KFR (14) 
               EVALUATE INDICE-ZONE
               WHEN  21   MOVE 0000000005 TO EXC-KFR (1)
                          MOVE 0000080000 TO EXC-KFR (2)
                          MOVE 0052000000 TO EXC-KFR (11)
                          MOVE 0000000065 TO EXC-KFR (13)
                          MOVE 6600000000 TO EXC-KFR (14)
           END-IF.    



           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           IF ECRAN-IDX = 1 
               EVALUATE INDICE-ZONE
               WHEN  1 PERFORM AVANT-1-1 
               WHEN  2 PERFORM AVANT-1-2 
               WHEN  3 PERFORM AVANT-1-3 
               WHEN  4 PERFORM AVANT-DEC.


           IF ECRAN-IDX = 2
               EVALUATE INDICE-ZONE
               WHEN 21 PERFORM AVANT-DEC
               WHEN OTHER PERFORM AVANT-ALL.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN  9 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN.
           IF EXC-KEY = 67 SUBTRACT 1 FROM ECRAN-IDX.
           IF EXC-KEY = 68 ADD 1 TO ECRAN-IDX.
           IF EXC-KEY > 66 
              PERFORM AFFICHAGE-ECRAN
              PERFORM AFFICHAGE-DETAIL
              MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
              INITIALIZE DECISION
              GO TRAITEMENT-ECRAN
           END-IF.
           
           IF ECRAN-IDX = 1
              EVALUATE INDICE-ZONE
              WHEN  1 PERFORM APRES-1-1 
              WHEN  2 PERFORM APRES-1-2 
              WHEN  4 PERFORM APRES-DEC.

           IF ECRAN-IDX = 2
               EVALUATE INDICE-ZONE
               WHEN 21 PERFORM APRES-DEC .

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX)
                 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1-1.          
           ACCEPT IMPS-JOB
             LINE  4 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-2.          
           ACCEPT IMPS-TYPE
             LINE  5 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-1-3.          
           ACCEPT IMPS-DESCR
             LINE  6 POSITION 30 SIZE 40
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-ALL.
           MOVE 1 TO IDX-1.
           PERFORM AVANT-ALL-1 THRU AVANT-ALL-END.
           
       AVANT-ALL-1.
           PERFORM ACCEPT-PARAM.
           IF EXC-KEY = 82 PERFORM END-PROGRAM.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO AVANT-ALL-1.
           IF EXC-KEY = 52 
           AND INDICE-ZONE > 1
              SUBTRACT 1 FROM INDICE-ZONE.
           IF EXC-KEY = 53 ADD 1 TO INDICE-ZONE.
           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM IDX-1
                WHEN 54 MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE
                        MOVE 13 TO IDX-1
                WHEN 13 ADD 1 TO IDX-1
           END-EVALUATE.
           IF IDX-1 >  0 AND IDX-1 < 13 AND  INDICE-ZONE < 21
              GO AVANT-ALL-1.
       AVANT-ALL-END.
           EXIT.

       ACCEPT-PARAM.
           COMPUTE LIN-IDX = INDICE-ZONE + 2.
           COMPUTE COL-IDX = IDX-1 * 4  + 20.
           MOVE IMPS-ESC(INDICE-ZONE, IDX-1) TO INTER-MED2.
           MOVE INTER-BIN TO HE93.
           ACCEPT HE93
             LINE LIN-IDX POSITION COL-IDX SIZE 3 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           MOVE HE93 TO INTER-BIN.
           IF INTER-BIN > 255
              GO ACCEPT-PARAM
           END-IF.
           MOVE INTER-MED2 TO IMPS-ESC(INDICE-ZONE, IDX-1) .
           MOVE INTER-BIN TO HEZ3.
           DISPLAY HEZ3 LINE LIN-IDX POSITION COL-IDX.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1-1.
           EVALUATE EXC-KEY
           WHEN  1 MOVE 200 TO LNK-VAL
                   PERFORM HELP-SCREEN
                   MOVE 1 TO INPUT-ERROR
           WHEN  65 PERFORM PREV-IMPSYS
           WHEN  66 PERFORM NEXT-IMPSYS
           WHEN   5 IF IMPS-JOB NOT = SPACES
                       PERFORM COPIE
                    END-IF
           WHEN OTHER
           READ IMPSYS INVALID 
                INITIALIZE IMPS-REC-DET 
                MOVE 0 TO INTER-BIN
                PERFORM INIT-ESC-SEQ VARYING IDX FROM 1 BY 1
                UNTIL IDX > 240
                END-READ.
           IF IMPS-KEY = SPACES MOVE 1 TO INPUT-ERROR.
           PERFORM DIS-E1-01 THRU DIS-E1-END.

       COPIE.          
           ACCEPT COPIE-NAME
             LINE  4 POSITION 50 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF COPIE-NAME NOT = SPACES
                MOVE COPIE-NAME TO IMPS-JOB
                WRITE IMPS-RECORD INVALID CONTINUE END-WRITE
                PERFORM AFFICHAGE-DETAIL
           END-IF.
           MOVE 0 TO INDICE-ZONE.

       APRES-1-2.
           READ IMPSYS INVALID CONTINUE
                END-READ.


       APRES-DEC.
           EVALUATE EXC-KEY
               WHEN 65 PERFORM PREV-IMPSYS
                       PERFORM AFFICHAGE-DETAIL
                       MOVE 21 TO DECISION
               WHEN 66 PERFORM NEXT-IMPSYS
                       PERFORM AFFICHAGE-DETAIL
                       MOVE 21 TO DECISION
               WHEN  5 REWRITE IMPS-RECORD INVALID WRITE IMPS-RECORD
                       END-REWRITE
                       MOVE 1 TO DECISION ECRAN-IDX 
                       PERFORM AFFICHAGE-ECRAN
               WHEN  8 DELETE IMPSYS INVALID CONTINUE END-DELETE
                       INITIALIZE IMPS-RECORD
                       MOVE 1 TO DECISION ECRAN-IDX 
                       PERFORM AFFICHAGE-ECRAN
           END-EVALUATE.            
           COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX(ECRAN-IDX) 
               MOVE CHOIX-MAX(ECRAN-IDX) TO INDICE-ZONE.

       PREV-IMPSYS.
           START IMPSYS KEY < IMPS-KEY INVALID KEY
                INITIALIZE IMPS-RECORD
                NOT INVALID
           READ IMPSYS PREVIOUS AT END 
                INITIALIZE IMPS-RECORD
                MOVE 1 TO INDICE-ZONE
                END-READ.

       NEXT-IMPSYS.
           START IMPSYS KEY > IMPS-KEY INVALID KEY
                INITIALIZE IMPS-RECORD
                NOT INVALID
           READ IMPSYS NEXT AT END 
                INITIALIZE IMPS-RECORD
                MOVE 1 TO INDICE-ZONE
                END-READ.

       DIS-E1-01.
           DISPLAY IMPS-JOB  LINE  4 POSITION 30.
       DIS-E1-02.
           DISPLAY IMPS-TYPE  LINE  5 POSITION 30.
       DIS-E1-03.
           DISPLAY IMPS-DESCR  LINE 6 POSITION 30.
       DIS-E1-END.
           EXIT.

       DIS-E2-01.
           DISPLAY IMPS-JOB   LINE 24 POSITION 13.
           DISPLAY IMPS-TYPE   LINE 24 POSITION 24.
           DISPLAY IMPS-DESCR  LINE 24 POSITION 35.
           PERFORM DISPLAY-DETAIL VARYING IDX FROM 1 BY 1
           UNTIL IDX > 20.

       DISPLAY-DETAIL.
           COMPUTE LIN-IDX = IDX + 2.
           PERFORM DISPLAY-ESC VARYING IDX-1 FROM 1 BY 1
           UNTIL IDX-1 > 12.

       DISPLAY-ESC.
           COMPUTE COL-IDX = IDX-1 * 4  + 20.
           MOVE IMPS-ESC(IDX, IDX-1) TO INTER-MED2.
           MOVE INTER-BIN TO HEZ3.
           DISPLAY HEZ3 LINE LIN-IDX POSITION COL-IDX.

       INIT-ESC-SEQ .
           MOVE INTER-MED2  TO IMPS-ESC-R(IDX).
       
       AFFICHAGE-ECRAN.
           MOVE ECR-S(ECRAN-IDX) TO LNK-VAL.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           EVALUATE ECRAN-IDX
               WHEN 1 PERFORM DIS-E1-01 THRU DIS-E1-END 
               WHEN 2 PERFORM DIS-E2-01
           END-EVALUATE.


       END-PROGRAM.
           CLOSE IMPSYS.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           
           
      