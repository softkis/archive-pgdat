      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 1-BARCOP COPIER BAREMES DE SALAIRES  !!     �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    1-BARCOP.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "BAREME.FC".
           COPY "BARDEF.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "BAREME.FDE".
           COPY "BARDEF.FDE".

       WORKING-STORAGE SECTION.
      *컴컴컴컴컴컴컴컴컴컴컴�
       01  CHOIX-MAX             PIC 99 VALUE 5.

      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "INDEX.REC".
           COPY "IMPRLOG.REC".
           COPY "V-VAR.CPY".

       01  BAR-DEF               PIC X(10).
       01  BAR-COPY              PIC X(10).
       01  SAVE-BAREME           PIC X(10).
       01  SAVE-PERIODE.
           04 S-ANNEE            PIC 9999.
           04 S-MOIS             PIC 99.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC Z(2).
           02 HE-Z4 PIC Z(4).
           02 HE-Z6 PIC Z(6).
           02 HE-Z2Z2 PIC ZZ,ZZ. 
           02 HE-Z6Z2 PIC Z(6),ZZ. 

       01   MOIS-SOURCE.
            02 ANNEE        PIC 9999.
            02 MOIS         PIC 99.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".

       PROCEDURE DIVISION USING LINK-V.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON BAREME BARDEF.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-1-BARCOP.

           OPEN INPUT BARDEF.
           OPEN INPUT BAREME.

           MOVE LNK-ANNEE TO ANNEE.
           MOVE LNK-MOIS  TO MOIS.
           PERFORM AFFICHAGE-ECRAN .

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > 100.

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           INITIALIZE EXC-KEY-CTL.

      * param둻res g굈굍ales touches de fonctions
      
           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param둻res sp괹ifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 1  MOVE 0029000000 TO EXC-KFR (1)
                   MOVE 0000000065 TO EXC-KFR (13)
                   MOVE 6600000000 TO EXC-KFR (14)
           WHEN 5  MOVE 0000000025 TO EXC-KFR (1)
                   MOVE 0052000000 TO EXC-KFR (11).

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 24 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 PERFORM END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
                WHEN 56 MOVE 65 TO EXC-KEY
                WHEN 58 MOVE 66 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN (EXC-KEY ) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4
           WHEN  5 PERFORM APRES-DEC.

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                            ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.
           ACCEPT BAR-DEF
             LINE  5 POSITION 30 SIZE 10 
             TAB UPDATE NO BEEP CURSOR 1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
             MOVE  BAR-DEF TO BAR-BAREME.

       AVANT-2.
           ACCEPT ANNEE
             LINE  7 POSITION 33 SIZE  4
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3. 
           ACCEPT MOIS  
             LINE  8 POSITION 35 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4.
           ACCEPT BAR-COPY
             LINE 10 POSITION 30 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION EXC-KEY  CONTINUE.

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================
       
       APRES-1.
           EVALUATE EXC-KEY
           WHEN  65 MOVE BAR-BAREME TO BDF-KEY 
                    PERFORM PREV-BDF
                    MOVE BDF-KEY TO BAR-BAREME
           WHEN  66 MOVE BAR-BAREME TO BDF-KEY 
                    PERFORM NEXT-BDF
                    MOVE BDF-KEY TO BAR-BAREME
           WHEN   2 CALL "2-BARDEF" USING LINK-V BDF-RECORD
                    MOVE BDF-KEY TO BAR-BAREME
                    CANCEL "2-BARDEF"
                    PERFORM AFFICHAGE-ECRAN 
           END-EVALUATE.
           MOVE BAR-BAREME TO BAR-DEF.
           PERFORM AFFICHAGE-DETAIL.
           INITIALIZE BAR-RECORD.
           MOVE LNK-ANNEE TO ANNEE.
           MOVE LNK-MOIS  TO MOIS.
           PERFORM BAR-LATEST.
           IF NOT-FOUND = 0
              MOVE BAR-ANNEE TO ANNEE
              MOVE BAR-MOIS  TO MOIS 
           ELSE
              MOVE LNK-ANNEE TO ANNEE
              MOVE LNK-MOIS  TO MOIS
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-2.
           MOVE ANNEE TO BAR-ANNEE-1.
           MOVE MOIS  TO BAR-MOIS-1.
           MOVE 999   TO BAR-GRADE-1.
           PERFORM BAR-LATEST.
           IF NOT-FOUND = 0
           AND BAR-ANNEE = ANNEE
               MOVE BAR-MOIS  TO MOIS 
           ELSE
               MOVE BAR-ANNEE TO ANNEE
               MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-3.
           PERFORM BAR-LATEST.
           IF NOT-FOUND = 0
           AND BAR-ANNEE = ANNEE
           AND BAR-MOIS  = MOIS 
               CONTINUE
           ELSE
              MOVE BAR-ANNEE TO ANNEE
              MOVE BAR-MOIS  TO MOIS 
              MOVE 1 TO INPUT-ERROR
           END-IF.

       APRES-4.
           PERFORM DIS-HE-04.


       APRES-DEC.
            EVALUATE EXC-KEY 
            WHEN  5 IF BAR-COPY NOT = SPACES
                       PERFORM BAR-COPY THRU BAR-COPY-END
                    END-IF
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.
           
       BAR-LATEST.
           MOVE 0 TO NOT-FOUND.
           MOVE BAR-DEF TO SAVE-BAREME BAR-BAREME-1.
           MOVE LNK-ANNEE TO BAR-ANNEE-1.
           MOVE LNK-MOIS  TO BAR-MOIS-1.
           MOVE 999       TO BAR-GRADE-1.
           START BAREME KEY <= BAR-KEY-1 INVALID
                MOVE 1 TO NOT-FOUND
                NOT INVALID 
                   READ BAREME PREVIOUS AT END 
                MOVE 1 TO NOT-FOUND
           END-READ.
           IF SAVE-BAREME NOT = BAR-BAREME
              MOVE 1 TO NOT-FOUND.
           IF NOT-FOUND = 1 
              INITIALIZE BAR-RECORD 
              MOVE SAVE-BAREME TO BAR-BAREME.

       PREV-BDF.
           MOVE 0 TO NOT-FOUND .
           START BARDEF KEY < BDF-KEY INVALID
                MOVE 1 TO NOT-FOUND
                INITIALIZE BDF-REC-DET
                NOT INVALID 
                   READ BARDEF PREVIOUS AT END 
                MOVE 1 TO NOT-FOUND
                INITIALIZE BDF-REC-DET
           END-READ.

       NEXT-BDF.
           MOVE 0 TO NOT-FOUND .
           START BARDEF KEY > BDF-KEY INVALID KEY
                MOVE 1 TO NOT-FOUND
                INITIALIZE BDF-REC-DET
                NOT INVALID 
                   READ BARDEF NEXT AT END 
                MOVE 1 TO NOT-FOUND
                INITIALIZE BDF-REC-DET
           END-READ.

       DIS-HE-01.
           DISPLAY BAR-BAREME LINE  5 POSITION 30.
           MOVE BAR-BAREME TO BDF-KEY.
           READ BARDEF INVALID 
                INITIALIZE BDF-REC-DET
           END-READ.
           DISPLAY BDF-NOM LINE 5 POSITION 45.
       DIS-HE-02.
           MOVE ANNEE TO HE-Z4.
           DISPLAY HE-Z4  LINE  7 POSITION 33.
       DIS-HE-03.
           MOVE MOIS  TO HE-Z2.
           DISPLAY HE-Z2  LINE  8 POSITION 35.
       DIS-HE-04.
           DISPLAY BAR-COPY LINE 10 POSITION 30.
           MOVE BAR-COPY TO BDF-KEY.
           READ BARDEF INVALID 
                INITIALIZE BDF-REC-DET
           END-READ.
           DISPLAY BDF-NOM LINE 10 POSITION 45.
       DIS-HE-END.
           EXIT.

       AFFICHAGE-ECRAN.
           MOVE 1042 TO LNK-VAL.
           MOVE "  " TO LNK-AREA.
           PERFORM WORK-ECRAN.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE BARDEF.
           CLOSE BAREME.
           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".


       BAR-COPY.
           MOVE BAR-COPY TO LNK-TEXT.
           MOVE ANNEE TO BAR-ANNEE-1.
           MOVE MOIS  TO BAR-MOIS-1.
           MOVE SAVE-BAREME TO BAR-BAREME-1.
           MOVE BAR-DATE TO SAVE-PERIODE.
           MOVE BDF-POINTS TO LNK-VAL.
           MOVE 0 TO BAR-GRADE-1.
           START BAREME KEY >= BAR-KEY-1 INVALID
               GO BAR-COPY-END.

       BAR-COPY-1.
           READ BAREME NEXT AT END 
               GO BAR-COPY-END
           END-READ.
           IF BAR-BAREME NOT = SAVE-BAREME
           OR BAR-DATE NOT = SAVE-PERIODE
               GO BAR-COPY-END.
           CALL "4-BARCOP" USING LINK-V BAR-RECORD.
           GO BAR-COPY-1.
       BAR-COPY-END.
           PERFORM END-PROGRAM.
