       IDENTIFICATION DIVISION.
       PROGRAM-ID.     "rmcrl1".
      *
      *    Title:  rmcrl1.cbl
      *        RM/COBOL-85 Relative File Conversion
      *
      *    Copyright (c) 1985-1992 by Liant Software Corp.  All rights reserved.
      *
      *    This file is supplied by Liant Software Corporation as an example only.
      *    Liant Software Corporation has no liability either expressed or implied
      *    as to the completeness or validity of this program.
      *
      * =========
      *
      *    This is the RELATIVE FILE CONVERSION example program
      *    to be used to convert:
      *          RM/COBOL 1.n ----> RM/COBOL-85
      *
      *    This is an example program that must be edited, compiled
      *    using the RM/COBOL-85 RMCOBOL command, and executed using
      *    the RM/COBOL-85 RUNCOBOL command.
      *
      *    1. Edit the maximum record size value at the ^^^^ points
      *       indicated below.
      *
      *    2. Compile the edited source using the RM/COBOL-85 RMCOBOL
      *       command.
      *
      *    3. Use the DOS SET command to indicated the input (1.n) and
      *       output (85) relative files' DOS pathname, filename and
      *       extension as follows:
      *          SET INPUT=<input file's pathname\filename.ext>
      *          SET OUTPUT=<output file's pathname\filename.ext>
      *
      *    4. Execute the object program produced by step 2 to perform
      *       the file conversion.
      *
      *    5. Use the DOS SET command to remove the input and output
      *       files' DOS pathname, filename and extension from the DOS
      *       environment table as follows:
      *          SET INPUT=
      *          SET OUTPUT=
      *
      * ============
      *
      *  VERSION IDENTIFICATION:
      *    $Revision:   5.3  $
      *    $Date:   16 Apr 1992 17:26:14  $
      *    $Author:   MIKE  $
      *    $Logfile:   U:\C85\DEV\UTIL\VCS\RMCRL1.CBV  $
      *
      *  VERSION HISTORY:
      *    $Log:   U:\C85\DEV\UTIL\VCS\RMCRL1.CBV  $
      *    
      *       Rev 5.3   16 Apr 1992 17:26:14   MIKE
      *    Change version number to 5.20.
      *    
      *       Rev 5.2   02 Apr 1992 10:47:28   MIKE
      *    Fixup copyright notices.
      *    
      *       Rev 5.1   26 Mar 1991 19:43:24   RANDY
      *    Update version number.
      *    
      *       Rev 5.0   15 Sep 1990  1:42:44   BILL
      *    No change.
      *    
      *       Rev 1.4   29 Aug 1990 14:17:00   BILL
      *    Change program name to lower case for UNIX program library.
      *    
      *       Rev 1.3   16 Aug 1990  8:55:38   BILL
      *    Change version number to 5.00.
      *    
      *       Rev 1.2   28 Mar 1990  9:47:00   ANTHONY
      *    Added copyright and PVCS keywords.
      *    Changed the title displayed to the standard style.
      *    Updated the question to be OS generic.
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.    RMC.
       OBJECT-COMPUTER.    RMC.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT OUT-FILE ASSIGN TO RANDOM "OUTPUT"
           ORGANIZATION IS RELATIVE ACCESS IS RANDOM
           RELATIVE KEY IS RELKEY.

           SELECT IN-FILE ASSIGN TO RANDOM "INPUT"
           ORGANIZATION IS BINARY SEQUENTIAL
           FILE STATUS IS IN-STATUS.

       DATA DIVISION.
       FILE SECTION.

       FD  OUT-FILE.
       01  OUT-RECORD                  PIC X(1000).
      * >>> Change to maximum record size    ^^^^

       FD  IN-FILE.
       01  IN-RECORD.
           02  IN-RECORD-LENGTH        PIC S9(4) BINARY.
           02  DATA-IN                 PIC X(1000).
      * >>> Change to maximum record size    ^^^^

       WORKING-STORAGE SECTION.
       01  NUMBER-OF-RECORDS           PIC 9(10) VALUE IS ZERO.
       01  DELETED-RECORDS             PIC 9(10) VALUE IS ZERO.

       01  RESPONSE-INDICATOR          PIC X.
           88  AFFIRMATIVE-RESPONSE    VALUES "Y", "y".
           88  NEGATIVE-RESPONSE       VALUES "N", "n".

       01  RELKEY                      PIC 9(10).

       01  IN-STATUS                   PIC X(2).
           88  END-OF-INPUT-FILE       VALUES "10", "04", "97".

       PROCEDURE DIVISION.
       DECLARATIVES.
       IN-FILE-ERROR SECTION.
           USE AFTER STANDARD ERROR PROCEDURE ON IN-FILE.
       A.
           IF NOT END-OF-INPUT-FILE THEN
               DISPLAY "Input file error status ", IN-STATUS,
                       " caused premature termination." LINE 20
               STOP RUN
           END-IF.
       END DECLARATIVES.

       MAIN SECTION.
       START-UP.
           DISPLAY "RM/COBOL 1.n to RM/COBOL-85 Relative file conversion
      -            " - Version 5.20" ERASE LINE 2.
           DISPLAY " You must edit and compile this program and set the"
                       LINE 5.
           DISPLAY " appropriate environment variables before running"
           DISPLAY " this program.  Have you done this? (Y/N) " LINE 7.
           SET NEGATIVE-RESPONSE TO TRUE.
           ACCEPT RESPONSE-INDICATOR UPDATE POSITION 0 TAB.
           IF NOT (AFFIRMATIVE-RESPONSE OR NEGATIVE-RESPONSE) THEN
               GO TO START-UP
           END-IF.
           IF NOT AFFIRMATIVE-RESPONSE THEN
               DISPLAY "Program terminated by negative response."
               STOP RUN
           END-IF.

           SET NEGATIVE-RESPONSE TO TRUE.
           OPEN OUTPUT OUT-FILE WITH LOCK.
           OPEN INPUT IN-FILE WITH LOCK.
           MOVE 1 TO RELKEY.
           DISPLAY "Files opened."               LINE 9.
           DISPLAY "Valid data records found:"   LINE 10.
           DISPLAY "Deleted records noted:"      LINE 12.
           DISPLAY "Record number:"              LINE 14.

           READ IN-FILE.
           PERFORM UNTIL END-OF-INPUT-FILE
               IF IN-RECORD-LENGTH EQUAL 0
                   ADD 1 TO DELETED-RECORDS
                   DISPLAY DELETED-RECORDS LINE 12 POSITION 28 CONVERT
               ELSE
                   WRITE OUT-RECORD FROM DATA-IN
                   ADD 1 TO NUMBER-OF-RECORDS
                   DISPLAY NUMBER-OF-RECORDS LINE 10 POSITION 28 CONVERT
               END-IF
               DISPLAY RELKEY LINE 14 POSITION 28 CONVERT
               ADD 1 TO RELKEY
               READ IN-FILE END-READ
           END-PERFORM.

           CLOSE OUT-FILE.
           CLOSE IN-FILE.
           DISPLAY "Files closed.  Conversion complete." LINE 20.
           STOP RUN.
       END PROGRAM.

